/*
   Wednesday, June 15, 20162:35:43 PM
   User: 
   Server: MKE-PKAMATH
   Database: ComplianceKeeper
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_AssociatesCourses
	(
	Id int NOT NULL IDENTITY (1, 1),
	UserId int NOT NULL,
	CourseId int NOT NULL,
	CompletionDate datetime NULL,
	CourseExpirationDate datetime NULL,
	Deleted bit NOT NULL,
	Completed bit NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_AssociatesCourses SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_AssociatesCourses ON
GO
IF EXISTS(SELECT * FROM dbo.AssociatesCourses)
	 EXEC('INSERT INTO dbo.Tmp_AssociatesCourses (Id, UserId, CourseId, CompletionDate, CourseExpirationDate, Deleted, Completed)
		SELECT Id, UserId, CourseId, CompletionDate, CourseExpirationDate, Deleted, Completed FROM dbo.AssociatesCourses WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AssociatesCourses OFF
GO
DROP TABLE dbo.AssociatesCourses
GO
EXECUTE sp_rename N'dbo.Tmp_AssociatesCourses', N'AssociatesCourses', 'OBJECT' 
GO
COMMIT

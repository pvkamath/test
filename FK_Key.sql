USE [Symphony]
GO

ALTER TABLE [dbo].[NetworkSharedTrainingPrograms]  WITH CHECK ADD  CONSTRAINT [FK_NetworkSharedTrainingPrograms_TrainingProgram__Id] FOREIGN KEY([TrainingProgramId])
REFERENCES [dbo].[TrainingProgram] ([ID])
GO

ALTER TABLE [dbo].[NetworkSharedTrainingPrograms] CHECK CONSTRAINT [FK_NetworkSharedTrainingPrograms_TrainingProgram__Id]
GO


USE [StagingComplianceKeeper]
GO

/****** Object:  View [dbo].[BAKvAssociatesCoursesCKUserId]    Script Date: 6/7/2016 4:27:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[BAKvAssociatesCoursesCKUserId]
AS
SELECT     ACs.Id, ACs.UserId, Us.Email, Us.Ssn, Cs.OwnerCompanyId AS CompanyId, Cs.Name, ACs.CourseId, Cs.ProviderId, ACs.CompletionDate, 
                      ACs.CourseExpirationDate, ACs.CourseExpirationDate AS CertificateExpirationDate, NULL AS UserSpecRenewalDate, NULL AS CertificateReleased, 
                      ACs.Completed, ACs.Deleted, NULL AS PurchaseDate
FROM         dbo.AssociatesCourses AS ACs LEFT OUTER JOIN
                      dbo.Courses AS Cs ON Cs.CourseId = ACs.CourseId LEFT OUTER JOIN
                      dbo.Associates AS Us ON Us.UserId = ACs.UserId
UNION
SELECT     UCs.ID,
                          (SELECT     UserId
                            FROM          dbo.Associates
                            WHERE      (Email LIKE Us.UserName) AND (UserStatus = '1')) AS Expr1, Us.UserName, Us.SSN, Us.CompanyID, Cs.Name, UCs.CourseID, 
                      0 AS Expr2, UCs.CompletionDate, UCs.CourseExpirationDate, Cs.CkRenewalDate,
                          (SELECT     CkRenewalDate
                            FROM          dbo.TPUsersCoursesDates
                            WHERE      (UserCourseId = UCs.ID)) AS Expr4, UCs.CertificateReleased, UCs.Completed, 0 AS Expr3, UCs.PurchaseDate
FROM         TrainingPro2.dbo.UsersCourses AS UCs LEFT OUTER JOIN
                      TrainingPro2.dbo.Courses AS Cs ON Cs.CourseID = UCs.CourseID LEFT OUTER JOIN
                      TrainingPro2.dbo.Users AS Us ON Us.UserID = UCs.UserID
WHERE     (UCs.CourseStatus = 1)

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[33] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 3
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 5
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BAKvAssociatesCoursesCKUserId'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BAKvAssociatesCoursesCKUserId'
GO


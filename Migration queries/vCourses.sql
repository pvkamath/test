USE [StagingComplianceKeeper]
GO

/****** Object:  View [dbo].[vCourses]    Script Date: 6/7/2016 3:01:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vCourses]
AS
SELECT     CourseId, Name, Number, ProviderId, StateId, TypeId, OwnerCompanyId, ContEdHours, ExpirationDate, StateApproved, NonApproved, Deleted, InstructorName, 
                      InstructorEducationLevel, AccreditationTypeID
FROM         dbo.Courses




GO


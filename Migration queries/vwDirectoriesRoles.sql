USE [StagingComplianceKeeper]
GO

/****** Object:  View [dbo].[vwDirectoriesRoles]    Script Date: 6/7/2016 4:28:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwDirectoriesRoles]
AS
SELECT     dbo.afxSecurityDirectories.DirectoryName, dbo.afxSecurityDirectories.ParentID, dbo.afxSecurityRoles.RoleName, 
                      dbo.afxSecurityRoles.AccessLevel
FROM         dbo.afxSecurityDirectories INNER JOIN
                      dbo.afxSecurityDirectoriesRolesX ON dbo.afxSecurityDirectories.DirectoryID = dbo.afxSecurityDirectoriesRolesX.DirectoryID INNER JOIN
                      dbo.afxSecurityRoles ON dbo.afxSecurityDirectoriesRolesX.RoleID = dbo.afxSecurityRoles.RoleID

GO


<%
Response.Clear
Dim objError
Dim sDescription

Set objError = Server.GetLastError()

sErrorMessage = "ComplianceKeeper 500 Site Error<br>" & request.servervariables("SCRIPT_NAME") & "<br>" & request.servervariables("QUERY_STRING")

If Len(CStr(objError.ASPCode)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>IIS Error Number: " & objError.ASPCode
end if

If Len(CStr(objError.Number)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>COM Error Number: " & objError.Number
end if

If Len(CStr(objError.Source)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>Error Source: " & objError.Source
end if

If Len(CStr(objError.File)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>File Name: " & objError.File
end if

If Len(CStr(objError.Line)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>Line Number: " & objError.Line
end if

If Len(CStr(objError.Description)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>Brief Description: " & objError.Description
	sDescription = objError.Description
end if

If Len(CStr(objError.ASPDescription)) > 0 Then
	sErrorMessage = sErrorMessage & "<BR>Full Description: " & objError.ASPDescription
end if

set objError = nothing

call LogError(sErrorMessage) 'located below

strMailServerAddress		= application("MAILSERVER")
	
'Create an instance of the Jmail object and set it's initial values
Set objJMail = server.CreateObject("JMail.Message")
objJMail.ContentType = "text/html"
objJMail.Silent = true	

objJmail.Body = sErrorMessage

'set the Jmail values with the information collected
objJmail.AddRecipient "kchambers@xigroup.com"
objJmail.From = "compliancekeeper@xigroup.com"
objJmail.FromName = "compliancekeeper@xigroup.com"
objJmail.Subject = "complianceKeeper 500 Site Error"
	
'mail it
'objJmail.Send(strMailServerAddress)
objJmail.Close	

set objMail = nothing

%>
<!-- include virtual = "/admin/includes/functions.asp" ---->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- Site Developed by XIGroup -->
<!-- www.xigroup.com -->
<html><head>
<title>ComplianceKeeper | Data Tracking Solutions</title>
<meta NAME="Xchange Interactive Group" Content="XIG">
<meta NAME="Title" Content="Compliance Keeper: Manage Mortgage Licensing � Mortgage License Tracking � Mortgage Licensing Data Solutions">
<meta NAME="Keywords" Content="compliancekeeper, Data tracking solutions, compliance manager, system security solutions, privacy security, secure client login, compliancekeeper com, send data, streamline data, customized distribution">
<meta NAME="Description" Content="ComplianceKeeper is a Data Tracking Solutions.  A Comprehensive Solution for Your Compliance Manager.">
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=iso-8859-1">
<LINK REL="stylesheet" TYPE="text/css" HREF="/includes/style.css">
<script language="JavaScript" src="/includes/common.js"></script>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>

<script type="text/javascript" src="/includes/styleswitcher.js"></script>
<script language="JavaScript">
<!--
function changeImages() {
	if (document.images) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}
-->
</script>
 
</HEAD>
<BODY topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">

<center>
<table width="812" height="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td rowspan="5" width="6" background="/media/images/shadow.gif"><img src="/media/images/spacer.gif" width="6" height="25" alt=""></td>
		<!-- <td rowspan="5" width="1" bgcolor="#cccccc"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td> -->
		<td valign="top"><img src="/media/images/hometopbanner.gif" width="800" height="126" alt="ComplianceKeeper.com"></td>
		<!-- <td rowspan="5" width="1" bgcolor="#cccccc"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td> -->
		<td rowspan="5" width="6" background="/media/images/shadowr.gif"><img src="/media/images/spacer.gif" width="6" height="25" alt=""></td>
	</tr>
	<tr>
		<td valign="top"><img src="/media/images/homeimagebanner.jpg" width="800" height="122" alt=""></td>
	</tr>
	<tr>
		<td valign="top" height="100%">
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="100%" valign="top" class="bckHomeGradient">
						<table width="100%" cellpadding="15" cellspacing="0" border="0">
							<tr>
								<td valign="top">
									<p class="sectionTitleBlack"><b>A Comprehensive Solution for Your Compliance Manager</b></p>
								</td>
							</tr>
							<tr>
								<td align="center">
									<br>
									<P align ="center" class=MsoNormal><SPAN style="FONT-SIZE: 10pt; FONT-FAMILY: Arial; FONT-Weight: bold">
									There was a problem loading the web page.<br>Please hit the back button and try again.<br>An email has been sent to the site administrators.<br><br>
									<%= sDescription %>
									</span>
								</td>
							</tr>
						</table>
					</td>						
				</tr>
			</table>
		</td>
	</tr>
</table>
</center>
</BODY>
</body>
</html>

<% 
sub LogError(p_sError)
	dim oFSO 'as object
	dim oFileObj 'as object
	dim sErrorFilePath 'as string
	
	sErrorFilePath = Application("sAbsWebroot") & "error.log"
		
	set oFSO = CreateObject ("Scripting.FileSystemObject")
	
	'Either Create or open the existing error file
	if not oFSO.FileExists(sErrorFilePath) then	
		set oFileObj =  oFSO.CreateTextFile(sErrorFilePath, false)
	else
		set oFileObj =  oFSO.OpenTextFile(sErrorFilePath, 8)
	end if

	oFileObj.WriteLine(now & " - " & p_sError & vbcrlf & vbcrlf)
	
	oFileObj.Close
	
	set oFSO = nothing
	set oFileObj = nothing
end sub
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/Blog.Class.asp" --------------->

<%
on error resume next

dim oBlog 'as object
dim iPostID 'as integer
dim sMode 'as string
dim bSuccessful 'as boolean

iPostID = GetFormElementAndScrub("PostID")
sMode = GetFormElementAndScrub("Mode")

set oBlog = New Blog
oBlog.Connectionstring = application("sDataSourceName")

bSuccessful = oBlog.DeletePost(iPostID)

set oBlog = nothing

if bSuccessful then
	response.redirect("/Admin/Blogs/Members/ListPosts.asp?MemberID=" & Session("ListPostMemberID") & "&PostStatusID=" & session("ListPostPostStatusID") & "&SortBy=" & session("ListPostSortBy") & "&page_number=" & Session("ListPostCurPage"))
else 
	response.redirect("/admin/error.asp?message=The Post was not deleted successfully.")
end if
%>

<!-- #include virtual = "/includes/functions.asp" --------------->
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/Blog.Class.asp" --------------->

<%
on error resume next

dim oBlog 'as object
dim iBlogID 'as integer
dim iPostID 'as integer
dim iCommentID 'as integer
dim iCurPage 'as integer

iBlogID = GetFormElementAndScrub("BlogID")
iPostID = GetFormElementAndScrub("PostID")
iCommentID  = GetFormElementAndScrub("CommentID")
iCurPage = GetFormElementAndScrub("page_number")

set oBlog = New Blog
oBlog.Connectionstring = application("sDataSourceName")

'Delete Comment
if (oBlog.DeleteComment(iCommentID)) then
	response.redirect("/Admin/Blogs/Members/ListComments.asp?BlogID=" & iBlogID & "&PostID=" & iPostID & "&page_number=" & iCurPage)
else
	response.redirect("/admin/error.asp?message=The Comment was not deleted successfully.")
end if

set oBlog = nothing
%>

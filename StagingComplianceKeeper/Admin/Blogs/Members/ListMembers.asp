<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Member.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Portfolio.Class.asp" --------------------------------------------------->

<%
dim sAction 'as string
dim sSearchUsername, sSearchEmail, sSearchLastName 'as string
dim iSearchMemberType, iSearchMemberStatus, iMaxRecs, iCurPage 'as integer
dim oMember, rs 'as object
dim iPageCount, iCount 'as integer
dim sClass 'as string
dim sName 'as string
dim oBlog, oPortfolio 'as object

sAction = GetFormElementAndScrub("Action")

if Ucase(sAction) = "NEWSEARCH" then
	'Get Search values
	sSearchUsername = GetFormElementAndScrub("SearchUsername")
	sSearchEmail = GetFormElementAndScrub("SearchEmail")
	sSearchLastName = GetFormElementAndScrub("SearchLastName")
	iSearchMemberType = GetFormElementAndScrub("MemberType")
	iSearchMemberStatus = GetFormElementAndScrub("StatusID")
	iMaxRecs = GetFormElementAndScrub("MaxRecs")
	iCurPage = GetFormElementAndScrub("page_number")
	
	'Reset Session Vars
	Session("UserSearchUsername") = sSearchUsername
	Session("UserSearchEmail") = sSearchEmail
	Session("UserSearchLastName") = sSearchLastName
	Session("UserSearchMemberType") = iSearchMemberType
	Session("UserSearchMemberStatus") = iSearchMemberStatus
	Session("UserSearchMaxRecs") = iMaxRecs
	Session("UserCurPage") = iCurPage
else
	'Get Search values
	sSearchUsername = GetFormElementAndScrub("SearchUsername")
	if (sSearchUsername = "") then
		if (trim(Session("UserSearchUsername")) <> "") then
			sSearchUsername = Session("UserSearchUsername")
		end if
	else
		Session("UserSearchUsername") = sSearchUsername
	end if

	sSearchEmail = GetFormElementAndScrub("SearchEmail")
	if (sSearchEmail = "") then
		if (trim(Session("UserSearchEmail")) <> "") then
			sSearchEmail = Session("UserSearchEmail")
		end if
	else
		Session("UserSearchEmail") = sSearchEmail
	end if

	sSearchLastName = GetFormElementAndScrub("SearchLastName")
	if (sSearchLastName = "") then
		if (trim(Session("UserSearchLastName")) <> "") then
			sSearchLastName = Session("UserSearchLastName")
		end if
	else
		Session("UserSearchLastName") = sSearchLastName
	end if

	iSearchMemberType = GetFormElementAndScrub("MemberType")
	if (iSearchMemberType = "") then
		if (trim(Session("UserSearchMemberType")) <> "") then
			iSearchMemberType = Session("UserSearchMemberType")
		end if
	else
		Session("UserSearchMemberType") = iSearchMemberType
	end if

	iSearchMemberStatus = GetFormElementAndScrub("StatusID")
	if (iSearchMemberStatus = "") then
		if (trim(Session("UserSearchMemberStatus")) <> "") then
			iSearchMemberStatus = Session("UserSearchMemberStatus")
		end if
	else
		Session("UserSearchMemberStatus") = iSearchMemberStatus
	end if
	
	iMaxRecs = GetFormElementAndScrub("MaxRecs")
	if (iMaxRecs = "") then
		if (trim(Session("UserSearchMaxRecs")) <> "") then
			iMaxRecs = Session("UserSearchMaxRecs")
		end if
	else
		Session("UserSearchMaxRecs") = iMaxRecs
	end if

	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("UserCurPage")) <> "") then
			iCurPage = Session("UserCurPage")
		end if
	else
		Session("UserCurPage") = iCurPage
	end if
end if

set oMember = New Member
oMember.connectionString = application("sDataSourceName")

'Add values to search if not empty
if sSearchUsername <> "" then
	oMember.Username = sSearchUsername
end if

if sSearchEmail <> "" then
	oMember.Email = sSearchEmail
end if

if sSearchLastname <> "" then
	oMember.Lastname = sSearchLastname
end if

if iSearchMemberType <> "" then
	oMember.MemberTypeID = iSearchMemberType
else
	'if status ID is blank (ALL), only show the Active and Suspended members
	oMember.AddMemberType(1) 'Illustrator
	oMember.AddMemberType(2) 'Photographer
	oMember.AddMemberType(3) 'Illustrator
	oMember.AddMemberType(4) 'Photographer	
end if

if iSearchMemberStatus <> "" then
	oMember.AddStatusType(iSearchMemberStatus)
else
	'if status ID is blank (ALL), only show the Active and Suspended members
	oMember.AddStatusType(1) 'Active Members
	oMember.AddStatusType(2) 'Suspended Members
end if

set rs = oMember.SearchMembers("username")

set oBlog = New Blog
oBlog.connectionString = application("sDataSourceName")
	
set oPortfolio = New Portfolio
oPortfolio.connectionString = application("sDataSourceName")	

If iCurPage = "" Then iCurPage = 1
If iMaxRecs = "" Then iMaxRecs = 25
%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Blogs</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>

<table bgcolor="EaEaEa" cellpadding="4" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Username &nbsp;</td>	
		<td class="reportHeaderText">&nbsp; Name &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Blog &nbsp;</td>
	</tr>
	
<%
if not rs.eof then
	'Set the number of records displayed on a page
	rs.PageSize = iMaxRecs
	rs.Cachesize = iMaxRecs
	iPageCount = rs.PageCount
	
	'determine which search page the user has requested.
	If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
	If clng(iCurPage) <= 0 Then iCurPage = 1
			
	'Set the beginning record to be display on the page
	rs.AbsolutePage = iCurPage		
		
	iCount = 0	
	do while (iCount < rs.PageSize) AND (not rs.eof)
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
			
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td>" & rs("Username") & "</td>" & vbcrlf)
		
		'if Company, show company name, if Member, show fullname
		if cint(rs("MemberTypeID")) = 5 then
			sName = rs("CompanyName")
		else
			if rs("MidInitial") <> "" then
				sName = rs("LastName") & ", " & rs("FirstName") & " " & rs("MidInitial")
			else
				sName = rs("LastName") & ", " & rs("FirstName")
			end if
		end if
		
		response.write("<td>" & sName & "</td>" & vbcrlf)
		
		if oBlog.MemberHasBlog(rs("MemberID")) then
			response.write("<td align=""center""><a href=""/Admin/Blogs/Members/ListPosts.asp?MemberID=" & rs("MemberID") & """>Edit</a></td>" & vbcrlf)
		else
			response.write("<td align=""center"">N/A</td>" & vbcrlf)		
		end if
		
		response.write("</tr>" & vbcrlf)
			
		rs.MoveNext
		iCount = iCount + 1		
	loop

	if sClass = "row2" then
		sClass = "row1"
	else
		sClass = "row2"
	end if		
		
	response.write("<tr class=""" & sClass & """>" & vbcrlf)
	response.write("<td colspan=""7"">" & vbcrlf)

	'Display the proper Next and/or Previous page links to view any records not contained on the present page
	if iCurPage > 1 then
		response.write("<a href=""/Admin/Blogs/Members/ListMembers.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
	end if
	if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
		response.write("&nbsp;|&nbsp;")
	end if 
	if trim(iCurPage) <> trim(iPageCount) then
		response.write("<a href=""/Admin/Blogs/Members/ListMembers.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
	end if
		
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)	
	response.write("</table>" & vbcrlf)
	
	'display Page number
	response.write("<table width=""100%"">" & vbcrlf)	
	response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
	response.write("<td align=""center""><br>" & vbcrlf)	
	response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)		
else
	response.write("<tr><td colspan=""6"">There are currently no members that matched your search criteria.</td></tr>" & vbcrlf)
end if
%>	
	
</table>



<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oBlog = nothing
set oMember = nothing
set oPortfolio = nothing
%>

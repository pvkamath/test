<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Members</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="ListMembers.asp" method="POST">
<input type="hidden" name="action" value="NEWSEARCH">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Username:</b></td>
		<td><input  type="text" name="SearchUsername" value="" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>Email:</b></td>
		<td><input  type="text" name="SearchEmail" value="" maxlength="100"></td>				
	</tr>
	<tr>			
		<td><b>Last Name:</b></td>	
		<td><input  type="text" name="SearchLastName" value="" maxlength="50"></td>				
	</tr>	
	<tr>			
		<td><b>Membership Type:</b></td>	
		<td><% call DisplayMemberTypesDropDown("",0,false,"",false,false) 'located in /includes/functions.asp %></td>				
	</tr>			
	<tr>			
		<td><b>Member Status:</b></td>	
		<td><% call DisplayMemberStatusTypesDropDown("",0,false) 'located in /includes/Fuctions.asp %></td>				
	</tr>		
	<tr>
		<td><b>Records Per Page:</b></td>	
		<td>
			<select name="MaxRecs">
				<option value="25">25</option>
				<option value="50">50</option>
				<option value="75">75</option>
				<option value="100">100</option>												
			</select>
		</td>					
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>	
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Member.Class.asp" --------------------------------------------------->

<%
dim iBlogID 'as integer
dim iMemberID 'as integer
dim iNumberOfBlogPosts 'as integer
dim oBlog, oMember 'as object
dim sBlogTitle 'as string
dim iTotalPostComments 'as integer
dim rs 'as object
dim sUserName 'as string
dim iBlogMemberID 'as integer
dim sMemberName 'as string
dim sTitle, sPostDate, sExpirationDate, sText 'as string
dim iPostStatusID 'as integer
dim iPostID 'as integer
dim sDate 'as string

iPostID = GetFormElementAndScrub("PostID")
iBlogID = GetFormElementAndScrub("BlogID")
	
set oBlog = New Blog
oBlog.connectionString = application("sDataSourceName")
	
'Get Blog Info
if not (oBlog.GetBlogByBlogID(iBlogID)) then
	if not (oBlog.GetBlogByMemberName(sMemberName)) then
		if not (oBlog.GetBlogByMember(iBlogMemberID)) then		
			response.redirect("/admin/error.asp?message=The Blog could not be retrieved.")
		end if 
	end if
end if
	
iBlogID = oBlog.BlogID
sBlogTitle = oBlog.Title	
iMemberID = oBlog.MemberID
iNumberOfBlogPosts = oBlog.NumberOfBlogPosts

'Get Post Info
if not (oBlog.GetPostByPostID(iPostID)) then
	response.redirect("/admin/error.asp?message=The Post could not be retrieved.")
end if		

sTitle = oBlog.PostTitle
sPostDate = oBlog.PostDate
sExpirationDate = oBlog.PostExpirationDate
iPostStatusID = oBlog.PostStatusID
sText = oBlog.PostText
sDate = datepart("M",sPostDate) & "." & datepart("D",sPostDate) & "." & right(datepart("YYYY",sPostDate),2)

iTotalPostComments = oBlog.TotalPostComments(iPostID)
	
'Get the Username of the Blog's owner
set oMember = New Member
oMember.connectionString = application("sDataSourceName")
	
'Get Member info
if not (oMember.GetMember(iMemberID)) then
	response.redirect("/admin/error.asp?message=The member could not be found in the system.")
end if	
	
sUserName = oMember.Username
	
set oMember = nothing
set oBlog = nothing
%>

<table border="0" cellpadding="0" cellspacing="0" width=100%>
	<tr>
		<td>
			<table width=100% border=0 cellpadding=4 cellspacing=0>
				<tr>
					<td>
						<span class="subtitle"><%= sBlogTitle %></span>
						<table>
							<tr><td>Created By: <%= sUserName %></td></tr>
						</table>	
					</td>
				</tr>							
			</table>	
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%">		
				<tr>
					<td>
						<table cellpadding="2" cellspacing="2">			
							<tr>
								<td>
									<span class="subtitle2">
										<%= sDate %><br>
										<%= sTitle %>
									</span>
								</td>
							</tr>
<!--										
										<tr>
											<td>
												Posted: <%= sDate %>
											</td>											
										</tr>
-->										
							<tr>
								<td>
									<%= sText %>
								</td>
							</tr>
							<tr>
								<td align="left" class="blueBold">
									<a href="/Admin/Blogs/Members/ListComments.asp?BlogID=<%=iBlogID%>&PostID=<%=iPostID%>" class="bluebold">comments (<%= iTotalPostComments %>)</a>
								</td>
							</tr>						
						</table>
					</td>
				</tr>
			</table>
			<a href="javascript:history.back()">Return to Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
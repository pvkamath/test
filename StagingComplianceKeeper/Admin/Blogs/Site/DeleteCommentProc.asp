<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------->
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/Blog.Class.asp" --------------->

<%
on error resume next

dim oBlog 'as object
dim iBlogID 'as integer
dim iPostID 'as integer
dim iCommentID 'as integer
dim iCurPage 'as integer

iMemberID = 0
iBlogID = ScrubForSql(request("BlogID"))
iPostID = ScrubForSql(request("PostID"))
iCommentID  = ScrubForSql(request("CommentID"))
iCurPage = ScrubForSql(request("page_number"))

set oBlog = New Blog
oBlog.Connectionstring = application("sDataSourceName")

'Make sure this is the member's blog
'if not oBlog.IsBlogOwner(iMemberID,iBlogID) then
'	response.redirect("/Admin/error.asp?message=You are not allowed to access this Blog.")
'end if

'Delete Comment
if (oBlog.DeleteComment(iCommentID)) then
	response.redirect("/Admin/Blogs/Site/ListComments.asp?BlogID=" & iBlogID & "&PostID=" & iPostID & "&page_number=" & iCurPage)
else
	response.redirect("/Admin/error.asp?message=The Comment was not deleted successfully.")
end if

set oBlog = nothing
%>

<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" -------------------------------------------------------->

<%
	dim sBlogTitle 'as string
	dim sPostTitle 'as string
	dim iBlogID,iMemberID,iPostID 'as integer
	dim iTotalPostComments 'as integer
	dim rs 'as object
	dim iCurPage, iMaxRecs,iCount,iPageCount 'as integer
	dim oBlog 'as object
	dim sGoToLastPage 'as string
	dim sDate, sHours, sMinutes, sAMPM 'as string
	
	iMemberID = 0
	iBlogID = GetFormElementAndScrub("BlogID")
	iPostID = GetFormElementAndScrub("PostID")
	iCurPage = GetFormElementAndScrub("page_number")
	
	set oBlog = New Blog
	oBlog.connectionString = application("sDataSourceName")	
	
	'Make sure this is the member's blog
	if not oBlog.IsBlogOwner(iMemberID,iBlogID) then
		response.redirect("/Admin/error.asp?message=You are not allowed to access this Blog.")
	end if

	'Get Blog Title
	if not (oBlog.GetPostByPostID(iPostID)) then
		response.redirect("/Admmin/error.asp?message=The Blog could not be retrieved.")
	end if

	sBlogTitle = oBlog.Title
	sPostTitle = oBlog.PostTitle
	
	'Get the total Comments
	iTotalPostComments = oBlog.TotalPostComments(iPostID)	

	'Get Comments
	oBlog.PostID = iPostID
	set rs = oBlog.SearchComments("")		

	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 5
%>

<script TYPE="text/javascript">
	function ConfirmDelete(p_iCommentID)
	{
		if (confirm("Are you sure you wish to delete this Comment?"))
			window.location.href = '/Admin/Blogs/Site/DeleteCommentProc.asp?BlogID=<%=iBlogID%>&PostID=<%=iPostID%>&CommentID=' + p_iCommentID + '&page_number=<%=iCurPage%>';
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Site Blog</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<a href="/Admin/Blogs/Site/ListPosts.asp?BlogID=<%= iBlogID %>&Page_Number=<%=Session("CurPage")%>&PostStatusID=<%=Session("PostStatusID")%>&SortBy=<%= Session("SortBy") %>">Return to Blog Posts Screen</a>		
		</td>
	</tr>
</table>
<br>

<table width="100%">		
	<tr>
		<td>
			<table cellpadding="2" cellspacing="2" width="100%">
				<tr>
					<td><span class="subtitle"><%= sPostTitle %></span></td>
				</tr>
				<tr>
					<td><b><%= iTotalPostComments %>&nbsp;Comments</b></td>
				</tr>	
				<tr>
					<td>
						<hr>
					</td>
				</tr>
<%
if not rs.eof then
	'Set the number of records displayed on a page
	rs.PageSize = iMaxRecs
	rs.CacheSize = iMaxRecs
	iPageCount = rs.PageCount
	
	'Determine which search page the user has requested
	if UCase(sGoToLastPage) = "YES" then	
		iCurPage = iPageCount
	else
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
	end if
		
	'Set the beginning record to be displayed on the page
	rs.AbsolutePage = iCurPage
	
	iCount = 0
	do while (iCount < rs.PageSize) and (not rs.eof)
		'Due this to remove the seconds from date
		sDate = formatdatetime(rs("postdate"),2)
		sHours = datepart("H",rs("PostDate"))
		sMinutes = datepart("N",rs("PostDate"))

		if cint(sMinutes) < 10 then
			sMinutes = "0" & sMinutes
		end if		

		if cint(sHours) > 11 then
			sAMPM = "PM"
		else
			sAMPM = "AM"
		end if		

		if cint(sHours) > 12 then
			sHours = sHours - 12
		end if				
		
		sDate = sDate & " " & sHours & ":" & sMinutes & " " & sAMPM		
	
		response.write("<tr><td>" & vbcrlf)
		response.write("<b>" & rs("FirstName") & " " & rs("LastName") & "</b><br>")
		response.write(sDate & "<br><br>" & vbcrlf)
		response.write(rs("Comment") & "<br><br>" & vbcrlf)
		response.write("<a href=""Javascript:ConfirmDelete(" & rs("CommentID") & ")"">Delete</a>" & vbcrlf)
		response.write("</td></tr>" & vbcrlf)
		response.write("<tr><td><hr></td></tr>" & vbcrlf)
		rs.MoveNext
		iCount = iCount + 1		
	loop
	
	response.write("<tr><td>" & vbcrlf)

	'Display the proper Next and/or Previous page links to view any records not contained on the present page
	if iCurPage > 1 then
		response.write("<a href=""/Admin/Blogs/Site/ListComments.asp?BlogID=" & iBlogID & "&PostID=" & iPostID & "&page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
	end if
	if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
		response.write("&nbsp;|&nbsp;")
	end if 
	if trim(iCurPage) <> trim(iPageCount) then
		response.write("<a href=""/Admin/Blogs/Site/ListComments.asp?BlogID=" & iBlogID & "&PostID=" & iPostID & "&page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
	end if

	response.write("</td></tr>" & vbcrlf)	
	response.write("</table>" & vbcrlf)
	
	'display Page number
	response.write("<table width=""100%"">" & vbcrlf)	
	response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
	response.write("<td align=""center"" colspan=""4""><br>" & vbcrlf)	
	response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)		
else
	response.write("<tr><td>There are currently no comments for this post.</td></tr>" & vbcrlf)
end if
%>				
			</table>
		</td>
	</tr>
</table>



<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%	
	set rs = nothing
	set oBlog = nothing
%>
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" -------------------------------------------------------->

<%
	dim iMemberID 'as integer
	dim iBlogID,iPostID 'as integer
	dim oBlog 'as object
	dim sBlogTitle 'as string
	dim sDate, sHours, sMinutes, sAMPM 'as string
	dim iCurPage, iMaxRecs,iCount,iPageCount 'as integer
	dim sSortBy 'as string
	dim iPostStatusID 'as integer
	dim iTotalPostComments 'as integer
	dim rs 'as object
	dim sClass 'as string
	
	iMemberID = 0
'	iBlogID = GetFormElementAndScrub("BlogID")
	iCurPage = GetFormElementAndScrub("page_number")
	iPostStatusID = GetFormElementAndScrub("PostStatusID")
	sSortBy = GetFormElementAndScrub("SortBy")
	
	set oBlog = New Blog
	oBlog.connectionString = application("sDataSourceName")
	
	'Get Blog info
	if not (oBlog.GetBlogByMember(iMemberID)) then
		response.redirect("/admin/error.asp?message=The Blog could not be retrieved.")
	end if
			
	sBlogTitle = oBlog.Title
	iBlogID = oBlog.BlogID
	
	'Get Posts
	oBlog.BlogID = iBlogID
	oBlog.PostStatusID = iPostStatusID
	set rs = oBlog.SearchPosts(sSortBy)
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 5
	
	'create sessions to store page info
	Session("CurPage") = iCurPage
	session("SortBy") = sSortBy
	session("PostStatusID") = iPostStatusID
%>

<script language="Javascript" src="/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript">
function ChangeSort(OBJ)
{
	window.location.href = '/Admin/Blogs/Site/ListPosts.asp?BlogID=<%=iBlogID%>&PostStatusID=<%=iPostStatusID%>&SortBy=' + OBJ[OBJ.selectedIndex].value;
}

function ChangeStatus(OBJ)
{
	window.location.href = '/Admin/Blogs/Site/ListPosts.asp?BlogID=<%=iBlogID%>&PostStatusID=' + OBJ[OBJ.selectedIndex].value + '&SortBy=<%=sSortBy%>';
}

function ConfirmDelete(p_iPostID,p_sPostTitle)
{
	if (confirm("Are you sure you wish to delete the following Post and all related Comments:\n  " + p_sPostTitle))
		window.location.href = '/Admin/Blogs/Site/ModBlogPostProc.asp?BlogID=<%=iBlogID%>&PostID=' + p_iPostID + '&Mode=DELETE';
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Site Blog</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table width="100%">		
	<tr>
		<td>
			<span class="subtitle">Posts</span>
		</td>
	</tr>
	<tr>
		<td>
			<table cellspacing="2" cellpadding="2">
				<tr>
					<td colspan="2">
						<a href="/Admin/Blogs/Site/ModBlogPost.asp?BlogID=<%=iBlogID%>">Create New Post</a><br>											
					</td>
				</tr>
				<tr>
					<td>
						Sort By:
					</td>
					<td>
						<select onChange="ChangeSort(this)">
							<option value="PostDate DESC" <% if Ucase(sSortBy) = "POSTDATE" then response.write(" SELECTED") end if %>>Post Date</option>
							<option value="PostTitle" <% if Ucase(sSortBy) = "POSTTITLE" then response.write(" SELECTED") end if %>>Title</option>
						</select>											
					</td>											
				</tr>
				<tr>
					<td>
						View:
					</td>
					<td>
						<% call DisplayPostStatusTypesDropDown(iPostStatusID,0,false,"onChange=""Javascript:ChangeStatus(this)""") 'located in /includes/Fuctions.asp %>
					</td>											
				</tr>										
			</table>
		</td>
	</tr>
	<tr>
		<td>
			-- Click on the title of the post to edit it<br>							
			-- To view comments, click on the number of comments
			<table bgcolor="EaEaEa" cellpadding="4" cellspacing="2" width="100%">
				<tr class="reportHeaderColor1">
					<td class="reportHeaderText">Date</td>
					<td class="reportHeaderText">Post Title</td>
					<td class="reportHeaderText">Comments</td>
					<td class="reportHeaderText">Delete</td>
				</tr>
<%
if not rs.eof then
	'Set the number of records displayed on a page
	rs.PageSize = iMaxRecs
	rs.CacheSize = iMaxRecs
	iPageCount = rs.PageCount
		
	'Determine which search page the user has requested
	if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
	if clng(iCurPage) <= 0 then iCurPage = 1
		
	'Set the beginning record to be displayed on the page
	rs.AbsolutePage = iCurPage
		
	iCount = 0
	do while (iCount < rs.PageSize) and (not rs.eof)
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if
		
		'Due this to remove the seconds from date
		sDate = formatdatetime(rs("postdate"),2)
		sHours = datepart("H",rs("PostDate"))
		sMinutes = datepart("N",rs("PostDate"))

		if cint(sMinutes) < 10 then
			sMinutes = "0" & sMinutes
		end if		

		if cint(sHours) > 11 then
			sAMPM = "PM"
		else
			sAMPM = "AM"
		end if		

		if cint(sHours) > 12 then
			sHours = sHours - 12
		end if				
		
		sDate = sDate & " " & sHours & ":" & sMinutes & " " & sAMPM
		
		'Get the total Comments
		iTotalPostComments = oBlog.TotalPostComments(rs("PostID"))	
		
		Response.Write("<tr class=""" & sClass & """>" & vbCrLf)
		response.write("<td width=""125"">" & sDate & "</td>" & vbcrlf)
		response.write("<td><a href=""/Admin/Blogs/Site/ModBlogPost.asp?BlogID=" & iBlogID & "&PostID=" & rs("PostID") & """>" & rs("PostTitle") & "</td>" & vbcrlf)
		
		'Comments
		response.write("<td align=""center""  width=""75"">")
		if clng(iTotalPostComments) > 0 then
			response.write("<a href=""/Admin/Blogs/Site/ListComments.asp?BlogID=" & iBlogID & "&PostID=" & rs("PostID") & """>" & iTotalPostComments & "</a>")
		else
			response.write(iTotalPostComments)
		end if
		response.write("</td>" & vbcrlf)
		
		response.write("<td align=""center""  width=""50""><a href=""javascript:ConfirmDelete(" & rs("PostID") & ",'" & rs("PostTitle") & "')"">Delete</a></td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)
		
		rs.MoveNext
		iCount = iCount + 1
	loop
		
	if sClass = "row2" then
		sClass = "row1"
	else
		sClass = "row2"
	end if		
		
	response.write("<tr class=""" & sClass & """>" & vbcrlf)
	response.write("<td colspan=""4"">" & vbcrlf)

	'Display the proper Next and/or Previous page links to view any records not contained on the present page
	if iCurPage > 1 then
		response.write("<a href=""/Admin/Blogs/Site/ListPosts.asp?BlogID=" & iBlogID & "&page_number=" & iCurPage-1 & "&PostStatusID=" & iPostStatusID & "&SortBy=" & sSortBy & """>Previous</a>" & vbcrlf)
	end if
	if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
		response.write("&nbsp;|&nbsp;")
	end if 
	if trim(iCurPage) <> trim(iPageCount) then
		response.write("<a href=""/Admin/Blogs/Site/ListPosts.asp?BlogID=" & iBlogID & "&page_number=" & iCurPage+1 & "&PostStatusID=" & iPostStatusID & "&SortBy=" & sSortBy & """>Next</a>" & vbcrlf)
	end if

	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)	
	response.write("</table>" & vbcrlf)
	
	'display Page number
	response.write("<table width=""100%"">" & vbcrlf)	
	response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
	response.write("<td align=""center"" colspan=""4""><br>" & vbcrlf)	
	response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)		
%>					
<%
else
	response.write("<tr><td colspan=""4"">There are currently no posts.</td></tr>" & vbcrlf)
end if
%>					
			</table>
		</td>
	</tr>
</table>


<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%	
	set rs = nothing
	set oBlog = nothing
%>
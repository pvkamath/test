<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Portfolio.Class.asp" ---------------------->

<%
dim iMemberID 'as integer
dim iPortfolioID 'as integer
dim sMode 'as string
dim oPortfolio 'as object
	
iMemberID = 0
	
set oPortfolio = new Portfolio
oPortfolio.ConnectionString = application("sDataSourceName")
oPortfolio.VocalErrors = application("bVocalErrors")
	
if oPortfolio.GetPortfolioByMember(iMemberId) then
	sMode = "EDIT"
else
	sMode = "ADD"
end if 
%>

<script language="Javascript" src="/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//-->
	function Validate(FORM)
	{   
		return (checkString(FORM.ThumbnailPath,"Thumbnail"))
	}
	
	function ChangeLogoImage()
	{	
		document.frm1.ThumbnailImgSample.src = document.frm1.ThumbnailPath.value
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Blog Image</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table width="100%">		
	<tr>
		<td>
			<form name="frm1" action="/Admin/Blogs/Site/ModBlogImageProc.asp" method="post" enctype="multipart/Form-Data" onSubmit="return Validate(this)">
			<input type="hidden" name="PortfolioID" value="<%= oPortfolio.PortfolioId %>">
			<input type="hidden" name="mode" value="<%= sMode %>">
			<table cellpadding="4" cellspacing="2">
				<tr>
					<td><b>Portfolio Thumbnail: </b></td>
					<td><input type="file" name="ThumbnailPath" onPropertyChange="ChangeLogoImage()"></td>
				</tr>
				<tr>
					<td></td>
					<td>
					<% if oPortfolio.ThumbnailPath <> "" then %>
						<img src="<% = oPortfolio.ThumbnailPath %>" name="ThumbnailImgSample" id="ThumbnailImgSample">
					<% else %>
						<img src="/media/images/clear.gif" name="ThumbnailImgSample" id="ThumbnailImgSample">
					<% end if %>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="image" src="/media/images/btn_submit.gif">
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>

<%
set oPortfolio = nothing
%>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
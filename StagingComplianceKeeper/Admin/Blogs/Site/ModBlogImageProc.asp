<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/Portfolio.Class.asp" ---------------------->
<!-- #include virtual = "/admin/includes/gfxSpex.asp" ------------------------>


<%
'on error resume next

dim iPortfolioId 'as integer
dim sMode 'as string
dim iMemberID 'as integer
dim sDisplayMode 'as string
dim bAllowComments 'as boolean


'Create the fileup object that will allow us to upload files
dim oFileUp
set oFileUp = server.CreateObject("softartisans.fileup")


sMode = ScrubForSql(oFileUp.Form("mode"))
iMemberID = 0
iPortfolioId = ScrubForSQL(oFileUp.Form("PortfolioId"))
bAllowComments = ScrubForSql(oFileUp.Form("AllowComments"))


'Load the passed portfolio.
set oPortfolio = new Portfolio
oPortfolio.ConnectionString = application("sDataSourceName")
oPortfolio.VocalErrors = application("bVocalErrors")

if iPortfolioId <> "" then

	if not oPortfolio.GetPortfolioByPortfolioId(iPortfolioId) then 
		'Failed to load	
		response.redirect("/admin/error.asp?message=Unable to load the specified Portfolio.")
	end if
end if 


'Determine if we got new images that we'll be working with, or that images
'have been deleted.
dim bChangeThumbnailPath
dim iStrPos
if oFileUp.Form("ThumbnailPath").UserFileName <> "" then

	'Save a working copy of the image to the directory.  We'll use this to
	'get the image dimensions later.
	oFileUp.Form("ThumbnailPath").SaveAs(application("sAbsWebroot") & "usermedia\images\uploads\portfoliothumbs\current." & right(oFileUp.Form("ThumbnailPath").UserFileName, 3))

	bChangeThumbnailPath = true
	iStrPos = instrrev(oFileUp.Form("ThumbnailPath").UserFileName, "\")
	if iStrPos = 0 then
		iStrPos = 1
	end if
	'oPortfolio.ThumbnailPath = ScrubForSql(mid(oFileUp.Form("ThumbnailPath").UserFileName, iStrPos))
	oPortfolio.ThumbnailPath = application("sAbsWebroot") & "usermedia\images\uploads\portfoliothumbs\current." & right(oFileUp.Form("ThumbnailPath").UserFileName, 3)
else
	if ucase(oFileUp.Form("DeleteThumbnail")) = "ON" then
		oPortfolio.ThumbnailPath = "" 
	end if
	bChangeThumbnailPath = false
end if


'Define the maximimum dimensions for the image.
dim iMaxHeight, iMaxWidth
iMaxHeight = 400
iMaxWidth = 170


'Check the image size for the Portfolio thumbnail image
if bChangeThumbnailPath then

	'Pull the File Dimensions.  We use the gfxSpex function from gfxSpex.asp.
	dim bGotDimensions
	dim iActualWidth, iActualHeight, iColorDepth, sImgType
	bGotDimensions = gfxSpex(oFileUp.Form("ThumbnailPath"), iActualWidth, iActualHeight, iColorDepth, sImgType)
	if not bGotDimensions then

		Response.redirect("/error.asp?message=Cannot retrieve image dimensions.")
		Response.End
		
	else

		if iActualHeight > iMaxHeight then
			Response.Redirect("/admin/error.asp?message=Image height exceeds the allowed dimensions.")
			Response.End
		end if

		if iActualWidth > iMaxWidth then
			Response.Redirect("/admin/error.asp?message=Image width exceeds allowed dimensions.")
			Response.End
		end if
		
	end if 
	
	'Verify that the image file is within the size limit
	if oFileUp.Form("ThumbnailPath").TotalBytes > 60000 then
		
		Response.Redirect("/error.asp?message=Image size exceeds 60k file size limit.")
		Response.End			
		
	end if 
	
end if


'Define the paths we'll use to store the files.  We also store this information
'in the DB.
dim sThumbnailUploadPath, sThumbnailExtension
dim sDynThumnailPath

sThumbnailUploadPath = application("sAbsMediaUploadPath") & "PortfolioThumbs\"
sDynThumbnailPath = application("sDynMediaUploadPath") & "PortfolioThumbs/"
sThumbnailExtension = mid(oFileUp.Form("ThumbnailPath").UserFileName, instrrev(oFileUp.Form("ThumbnailPath").UserFileName, ".") + 1)


'Set the portfolio properties.
oPortfolio.MemberId = iMemberId
oPortfolio.Title = "Site Blog"
oPortfolio.AllowComments = false

if oPortfolio.SavePortfolio = 0 then

	Response.Redirect("/admin/error.asp?message=Unable to save Portfolio.")

end if 


'We have a portfolio Id, so go ahead and set the thumbnail path then resave.
if bChangeThumbnailPath then
	oPortfolio.ThumbnailPath = sDynThumbnailPath & oPortfolio.PortfolioId & "thumb." & sThumbnailExtension

	if oPortfolio.SavePortfolio = 0 then

		Response.Redirect("/admin/error.asp?message=Unable to save Portfolio.")

	end if 

end if


'If everything is still going smoothly, we can go ahead and upload the file.  
if bChangeThumbnailPath then
	oFileUp.Form("ThumbnailPath").SaveAs sThumbnailUploadPath & oPortfolio.PortfolioId & "thumb." & sThumbnailExtension
	if err <> 0 then
		Response.Redirect("/admin/error.asp?message=Save Failed: " & err.Description)
		Response.End
	end if
end if


'Set appropriate display mode
if Ucase(sMode) = "ADD" then
	sDisplayMode = "created"
elseif Ucase(sMode) = "EDIT" then
	sDisplayMode = "edited"
end if

if oPortfolio.PortfolioId > 0 then
	response.redirect("/admin/blogs/site/ModBlogImageConfirmation.asp")
else 
	response.redirect("/admin/error.asp?message=The Portfolio was not " & sDisplayMode & " successfully.")
end if
%>

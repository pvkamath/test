<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" -------------------------------------------------------->

<%
	dim iMemberID 'as integer
	dim iBlogID,iPostID 'as integer
	dim sMode 'as string
	dim oBlog 'as object
	dim sDisplayMode 'as string
	dim sBlogTitle 'as string
	dim sTitle 'as string
	dim iPostStatusID 'as integer
	dim sPostDate, sPostHours, sPostMinutes, sPostAMPM 'as string
	dim sExpirationDate, sExpirationHours, sExpirationMinutes, sExpirationAMPM 'as string
	dim bDisableExpirationDateTime 'as boolean
	dim bImage 'TextFX option
	dim i 'as integer
	dim sText 'as string
	
	 'TextFX Options
	 bImage = true	
	
	iMemberID = 0
	iBlogID = GetFormElementAndScrub("BlogID")
	iPostID = GetFormElementAndScrub("PostID")
	
	set oBlog = New Blog
	oBlog.connectionString = application("sDataSourceName")

	'Make sure this is the member's blog
	if not oBlog.IsBlogOwner(iMemberID,iBlogID) then
		response.redirect("/Admin/error.asp?message=You are not allowed to access this Blog.")
	end if

	'Get Blog Title
	if not (oBlog.GetBlogByBlogID(iBlogID)) then
		response.redirect("/Admin/error.asp?message=The Blog could not be retrieved.")
	end if
			
	sBlogTitle = oBlog.Title
	
	if trim(iPostID) <> "" then
		'Make sure this is the member's blog
		
		sMode = "EDIT"
		sDisplayMode = "Modify"
		
		'Get Post Info
		if not (oBlog.GetPostByPostID(iPostID)) then
			response.redirect("/Admin/error.asp?message=The Post could not be retrieved.")
		end if		
		
		sTitle = oBlog.PostTitle
		sPostDate = oBlog.PostDate
		sExpirationDate = oBlog.PostExpirationDate
		iPostStatusID = oBlog.PostStatusID
		sText = oBlog.PostText
				
		'Format Post Date
		sPostHours = datepart("H",sPostDate)
		sPostMinutes = datepart("N",sPostDate)
		sPostDate = formatdatetime(sPostDate,2)
		
		if cint(sPostMinutes) < 10 then
			sPostMinutes = "0" & sPostMinutes
		end if		

		if cint(sPostHours) > 11 then
			sPostAMPM = "PM"
		else
			sPostAMPM = "AM"
		end if		

		if cint(sPostHours) > 12 then
			sPostHours = sPostHours - 12
		elseif cint(sPostHours) = 0 then
			sPostHours = 12			
		end if	
		
		'if Expiration Date, is not null
		if not isnull(sExpirationDate) then
			'Format Expiration Date
			sExpirationHours = datepart("H",sExpirationDate)
			
			sExpirationMinutes = datepart("N",sExpirationDate)
			sExpirationDate = formatdatetime(sExpirationDate,2)
		
			if cint(sExpirationMinutes) < 10 then
				sExpirationMinutes = "0" & sExpirationMinutes
			end if		

			if cint(sExpirationHours) > 11 then
				sExpirationAMPM = "PM"
			else
				sExpirationAMPM = "AM"
			end if		

			if cint(sExpirationHours) > 12 then
				sExpirationHours = sExpirationHours - 12
			elseif cint(sExpirationHours) = 0 then
				sExpirationHours = 12
			end if
		else
			'Expiration Time
			sExpirationHours = "12"
			sExpirationMinutes = "00"
			sExpirationAMPM = "AM"
			bDisableExpirationDateTime = true
		end if
	else
		sMode = "ADD"
		sDisplayMode = "Create"
				
		'Get Current Hours, Minutes, and AM/PM setting
		sPostDate = date()
		sPostHours = datepart("H",now())
		sPostMinutes = datepart("N",now())

		if cint(sPostMinutes) < 10 then
			sPostMinutes = "0" & sPostMinutes
		end if

		if cint(sPostHours) > 11 then
			sPostAMPM = "PM"
		else
			sPostAMPM = "AM"
		end if

		if cint(sPostHours) > 12 then
			sPostHours = sPostHours - 12
		elseif cint(sPostHours) = 0 then
			sPostHours = 12			
		end if
		
		'Expiration Time
		sExpirationHours = "12"
		sExpirationMinutes = "00"
		sExpirationAMPM = "AM"
		bDisableExpirationDateTime = true
	end if 
	
	set oBlog = nothing
%>

<script language="JavaScript" type="text/javascript" src="/includes/rte/html2xhtml.js"></script>
<script language="JavaScript" type="text/javascript" src="/includes/rte/richtext.js"></script>

<script language="Javascript" src="/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript">
	function Validate(FORM)
	{   
		var sText;
		
		if (!checkString(FORM.Title,"Title") || (!checkString(FORM.PostStatusID,"Status")) || !checkIsDate(FORM.PostDate, "The Post Date you entered is not a valid date.", false))
			return false;
			
		if (!isWhitespace(FORM.ExpirationDate.value))
		{			
			if (!checkIsDate(FORM.ExpirationDate, "The Expiration Date you entered is not a valid date.", false))
				return false;
				
			//make sure Posting Date is before the Expiration Date
			if (!checkStartAndEndDate(FORM.PostDate, FORM.ExpirationDate, "The Expiration Date is earlier than the Post Date."))	{						
				FORM.PostDate.focus();
				return false;
			}					
		}
			
		//make sure the field is not empty
		FORM.Text.value = Text.document.body.innerHTML;
			
		if (FORM.Text.value == "") 
		{
			alert("Text is a required Field!");
			return false;
		}		
		
		return true;
	}
	
	function EnableDisableExpirationTime(FORM,p_sMode)
	{
		if (p_sMode == "FOCUS")
		{
			FORM.ExpirationHours.disabled = false;
			FORM.ExpirationMinutes.disabled = false;
			FORM.ExpirationAMPM.disabled = false;								
		}
		else
		{
			var sExpirationDate = FORM.ExpirationDate.value;
		
			if (sExpirationDate == '')
			{
				FORM.ExpirationHours.disabled = true;
				FORM.ExpirationMinutes.disabled = true;
				FORM.ExpirationAMPM.disabled = true;						
			}
			else
			{
				FORM.ExpirationHours.disabled = false;
				FORM.ExpirationMinutes.disabled = false;
				FORM.ExpirationAMPM.disabled = false;						
			}		
		}
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Site Blog</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table width="100%">		
	<tr>
		<td>
			<span class="subtitle"><%= sDisplayMode %> Post</span>
		</td>
	</tr>
	<tr>
		<td>
			<form name="frm1" action="/Admin/Blogs/Site/ModBlogPostProc.asp" method="post" onSubmit="return Validate(this)">
			<input type="hidden" name="BlogID" value="<%= iBlogID %>">
			<input type="hidden" name="PostID" value="<%= iPostID %>">			
			<input type="hidden" name="mode" value="<%= sMode %>">
			<table cellpadding="4" cellspacing="2">
				<tr>
					<td><b>Title: </b></td>
					<td><input type="text" name="Title" value="<%= sTitle %>" size="30" maxlength="100"></td>
				</tr>
				<tr>
					<td><b>Status: </b></td>
					<td><% call DisplayPostStatusTypesDropDown(iPostStatusID,1,false,"") 'located in /includes/Fuctions.asp %></td>
				</tr>
				<tr>
					<td><b>Post Date: </b></td>
					<td>
						<input name="PostDate" id="PostDate" value="<%= sPostDate %>" size="10" maxlength="10">
										
						<SELECT name="PostHours">
						<% For i = 1 To 12 %>
							<Option value="<%=i%>" <% If CStr(i) = trim(sPostHours) Then response.write(" selected") %>><%=i%></Option>
						<% Next %>
						</SELECT>

						<SELECT name="PostMinutes">
						<%
						i = 0
						Do While i <= 59
							if i < 10 then i = "0" & i
						%>
							<Option value="<%=i%>" <% If cstr(i) = trim(sPostMinutes) Then response.write(" selected")%>><%=i%></Option>	
						<%
							i = i + 1
						Loop
						%>
						</SELECT>

						<SELECT name="PostAMPM">
						<%If sPostAMPM = "AM" Then%>
							<Option value="am" selected>AM</Option>
							<Option value="pm">PM</Option>
						<%Else%>
							<Option value="am">AM</Option>
							<Option value="pm" selected>PM</Option>
						<%End If%>
						</SELECT>
					</td>
				</tr>
				<tr>
					<td><b>Expiration Date: </b><br>(optional)</td>
					<td>
						<input name="ExpirationDate" id="ExpirationDate" value="<%= sExpirationDate %>" size="10" maxlength="10" onFocus="Javascript:EnableDisableExpirationTime(this.form,'FOCUS')" onBlur="Javascript:EnableDisableExpirationTime(this.form,'BLUR')">

						<SELECT name="ExpirationHours" <% if bDisableExpirationDateTime then response.write(" DISABLED") end if %>>
						<% For i = 1 To 12 %>
							<Option value="<%=i%>" <% If CStr(i) = trim(sExpirationHours) Then response.write(" selected") %>><%=i%></Option>
						<% Next %>
						</SELECT>

						<SELECT name="ExpirationMinutes" <% if bDisableExpirationDateTime then response.write(" DISABLED") end if %>>
						<%
						i = 0
						Do While i <= 59
							if i < 10 then i = "0" & i
						%>
							<Option value="<%=i%>" <% If cstr(i) = trim(sExpirationMinutes) Then response.write(" selected")%>><%=i%></Option>	
						<%
							i = i + 1
						Loop
						%>
						</SELECT>

						<SELECT name="ExpirationAMPM" <% if bDisableExpirationDateTime then response.write(" DISABLED") end if %>>
						<%If sExpirationAMPM = "AM" Then%>
							<Option value="am" selected>AM</Option>
							<Option value="pm">PM</Option>
						<%Else%>
							<Option value="am">AM</Option>
							<Option value="pm" selected>PM</Option>
						<%End If%>
						</SELECT>
					</td>
				</tr>																						
				<tr>
					<td colspan="2">
						<!--<input type="hidden" name="Text"><!-- include virtual="/TextFX1.0/PostFormat.asp"-->
		<script language="JavaScript" type="text/javascript">
		initRTE("/includes/rte/images/", "/includes/rte/", "/includes/rte/", true, false);
		</script>
		<noscript><textarea name="Text" rows="7" cols="60"><% = sText %></textarea></noscript>
<script language="JavaScript" type="text/javascript">
<!--
//Usage: writeRichText(fieldname, html, width, height, buttons)
writeRichText('Text', '<% = RTESafe(sText) %>', 520, 200, true, false);
//-->
</script>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
						<img src="/admin/media/images/blue_bttn_cancel.gif" border="0" onclick="javascript:history.go(-2)">
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>


<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
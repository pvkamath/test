<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->

<%
	dim sMode 'as string
	dim sMessage 'as string
	dim iBlogID 'as integer
	dim sPgeTitle 'as string
	
	iBlogID = GetFormElementAndScrub("BlogID")
	sMode = GetFormElementAndScrub("Mode")
	
	if Ucase(sMode) = "ADD" then
		' Set Page Title:
		sPgeTitle = "Create Post"					
		sMessage = "Your Post was created successfully. "
	elseif Ucase(sMode) = "EDIT" then
		' Set Page Title:
		sPgeTitle = "Modify Post"		
		sMessage = "Your Post was modified successfully."		
	elseif Ucase(sMode) = "DELETE" then
		' Set Page Title:
		sPgeTitle = "Delete Post"				
		sMessage = "Your Post was deleted successfully."		
	end if 

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Site Blog</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table width="100%">		
	<tr>
		<td>
			<span class="subtitle"><%= sPgeTitle %></span>
		</td>
	</tr>
	<tr>
		<td>
			<br>
			<%= sMessage %>
		</td>
	</tr>
	<tr>
		<td>
			<br>
			<a href="/Admin/Blogs/Site/ListPosts.asp?BlogID=<%= iBlogID %>&Page_Number=<%=Session("CurPage")%>&PostStatusID=<%=Session("PostStatusID")%>&SortBy=<%= Session("SortBy") %>">View Posts</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" -------------------------------------------------------->

<%
'on error resume next

dim oBlog 'as object
dim iBlogID 'as integer
dim iPostID 'as integer
dim sMode 'as string
dim sTitle 'as string
dim sText 'as string
dim sPostDate,sExpirationDate 'as string
dim iMemberID 'as integer
dim iPostStatusID 'as integer
dim sDisplayMode 'as string
dim sPostHours,sPostMinutes,sPostAMPM 'as string
dim sExpirationHours,sExpirationMinutes,sExpirationAMPM 'as string
dim bSuccessful 'as boolean

iMemberID = 0
iBlogID = GetFormElementAndScrub("BlogID")
iPostID = GetFormElementAndScrub("PostID")
sMode = GetFormElementAndScrub("Mode")
sTitle = GetFormElementAndScrub("Title")
sText  = GetFormElementAndScrub("Text")
iPostStatusID = GetFormElementAndScrub("PostStatusID")
sPostDate = GetFormElementAndScrub("PostDate")
sPostHours = GetFormElementAndScrub("PostHours")
sPostMinutes = GetFormElementAndScrub("PostMinutes")
sPostAMPM = GetFormElementAndScrub("PostAMPM")
sPostDate = sPostDate & " " & sPostHours & ":" & sPostMinutes & " " & sPostAMPM
sExpirationDate = GetFormElementAndScrub("ExpirationDate")
sExpirationHours = GetFormElementAndScrub("ExpirationHours")
sExpirationMinutes = GetFormElementAndScrub("ExpirationMinutes")
sExpirationAMPM = GetFormElementAndScrub("ExpirationAMPM")
if trim(sExpirationDate) <> "" then
	sExpirationDate = sExpirationDate & " " & sExpirationHours & ":" & sExpirationMinutes & " " & sExpirationAMPM
end if

set oBlog = New Blog
oBlog.Connectionstring = application("sDataSourceName")

'Make sure this is the member's blog
if not oBlog.IsBlogOwner(iMemberID,iBlogID) then
	response.redirect("/admin/error.asp?message=You are not allowed to access this Blog.")
end if

if Ucase(sMode) = "ADD" then
	oBlog.MemberID = iMemberID
	oBlog.BlogID = iBlogID
	oBlog.PostStatusID = iPostStatusID
	oBlog.PostTitle = sTitle
	oBlog.PostText = sText
	oBlog.PostDate = sPostDate
	oBlog.PostExpirationDate = sExpirationDate
	iPostID = oBlog.SavePost()
	
	sDisplayMode = "created"
elseif Ucase(sMode) = "EDIT" then
	oBlog.PostID = iPostID
	oBlog.BlogID = iBlogID
	oBlog.PostStatusID = iPostStatusID	
	oBlog.PostTitle = sTitle
	oBlog.PostText = sText
	oBlog.PostDate = sPostDate
	oBlog.PostExpirationDate = sExpirationDate	
	iPostID = oBlog.SavePost()
	
	sDisplayMode = "edited"
elseif Ucase(sMode) = "DELETE" then
	bSuccessful = oBlog.DeletePost(iPostID)
end if


'Update the blog's RSS feed
set oBlog = nothing
set oBlog = new Blog
oBlog.ConnectionString = application("sDataSourceName")
oBlog.BlogId = iBlogId
bFeedSuccessful = oBlog.GetBlogByBlogId(iBlogId)
'bFeedSuccessful = oBlog.SaveFeed


set oBlog = nothing

if clng(iPostID) > 0 or bSuccessful then
	response.redirect("/Admin/Blogs/Site/ModBlogPostConfirmation.asp?BlogID=" & iBlogID & "&mode=" & sMode)
else 
	response.redirect("/Admin/error.asp?message=The Post was not " & sDisplayMode & " successfully.")
end if
%>

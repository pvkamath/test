<!-- #include virtual = "/admin/CalendarFX/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/CalendarFX/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/CalendarFX/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	
	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Please wait . . ."

	Dim iEventID, sEvent, iSectionID, dStartDate, dEndDate, sHour, sMinute, sAMPM
	Dim eHour, eMinute, eAMPM, sLocation, sOrganization, sPhone, iEventTypeID
	Dim sURL, sContact, sEmail, sInfo, iActive, sAction, word
	Dim objConn, rs, sSQL, strSQLConnectionString
	dim tmpDateEnd, sMode

	iEventID = request("eventid")
	bActive = request("active")

	If bActive = 1 Then 
		bActive = 0
	Else
	 	 bActive = 1
	End If
	
	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	sSQL =  "UPDATE afxEvents SET Active = '" & bActive  & "' " & _
						 "Where EventID = '" & iEventID & "'"
	
	objConn.Execute(sSQL)
	response.redirect "list.asp?"

	Set rs = Nothing
	objConn.Close
	Set objConn = Nothing

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

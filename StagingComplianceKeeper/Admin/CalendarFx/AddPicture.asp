<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Edit a Picture"

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ChangeSampleImage()
		{
			document.ImgSample.src = document.frm.WhatFile.value		
		}	
		
		function ConfirmDelete() {
		if ( window.confirm("Are you sure you want to permanantly delete this image?"))
			return true;
		else
			return false;
		}
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<%
					'Get event name
					Dim iEventID, strSQLConnectionString, sSQL, rs, objConn, sImageEdit
					sImageEdit = " Add "
					
					strSQLConnectionString = application("sDataSourceName")
					
					Set objConn = Server.CreateObject("ADODB.Connection")
					objConn.Open strSQLConnectionString
					
					Set rs = Server.CreateObject("ADODB.Recordset")
					
					iEventID = request("eventid")
					
					sSQL = "Select Event,  ImgPath From afxEvents Where EventID = '" & iEventID & "'"
					
					rs.Open sSQL, objConn, 3, 3
					
					If rs("ImgPath") <> "" Then sImageEdit = " Change " 
%>

<!-- title line	 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 <tr>
        	<td valign="top" class="activeTitle"><%=sImageEdit%> a Picture<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
<!-- content here -->

				
				<td height="100%" valign="top"> 
					<table cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td colspan="2">Would you like to <%=sImageEdit%> a picture for "<%=rs("Event")%>" ?</td>							
						</tr>
						<tr>
							<td colspan="2"><%If rs("ImgPath") <> "" Then%><img src="<%=Application("sDynWebroot")%>usermedia/images/calendarFX/<%=rs("ImgPath")%>" alt="xig" border="0"><%End If%>&nbsp;&nbsp;<img src="/admin/login/media/images/spacer.gif" name=ImgSample  border="0">							
							<%if sImageEdit <> " Add " Then %>								
									<br>
							<FORM action="DeletePictureProc.asp" method="Post" name="frm1" id="frm1">
								<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
								<input type="submit" name="delete" id="delete" value="Delete Image"  onClick="Javascript:return ConfirmDelete();">
							</FORM>								
							<%End If%>
							</td>
						</tr>
							<FORM action="AddPictureProc.asp" method="Post" name="frm" id="frm" enctype="multipart/Form-Data">
							<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
						<tr>
							<td colspan="2"><input type="File" id="WhatFile" name="WhatFile" size="30" onPropertyChange="ChangeSampleImage()"></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;<br>
							<input type="image" src="/admin/calendarfx/media/images/btn_submit.gif" border="0" name="submit" value="Submit">
							&nbsp;&nbsp;
							<a href="list.asp"><img src="/admin/calendarfx/media/images/btn_skip.gif" border="0" name="skip" value="skip"></a>
							</td>
						</tr>
					</table>
					</FORM>					
				</td>
<!-- end content -->				
			</tr>    
   		</table>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

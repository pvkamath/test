<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Edit a Picture"

	Dim objFileUp, sMode, iEventID
	
	set objFileUp = server.CreateObject("softartisans.fileup")
	
	'Set the default path to store uploaded files.
	'This should be set immediately after creating an instance. 

	objFileUP.Path = Application("sAbsUserMediaPath") & "calendarFX/"
	iEventID = objFileUp.Form("eventid")
	'response.write iEventID

	Dim objConn, sSQL, strSQLConnectionString

	strSQLConnectionString = application("sDataSourceName")
	
	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString


If objFileUp.IsEmpty Then

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function backtoAddpicture(){
			window.location = "Addpicture.asp?eventid=<%=iEventID%>"
		}
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
	<table width="300" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Invalid filename. Please go back and try again.</td>
		</tr>
		<tr>
			<td><BR><input type="button" name="back" id="back" value="Back" onClick="backtoAddpicture()">
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
				<p>&nbsp;</p></td>
		</tr>
	</table>
<%
Else
	'Upload file
	
	'iEventID = objFileUp.Form("eventid")
			
	'Get the name and path of the file to be uploaded
	FilePath = objFileUp.UserFileName
	
	'Breakdown uploaded file path and get the original filename
	FileArray = split(FilePath, "\")
	FilePathSpace = ubound(FileArray)
	FileName = FileArray(FilePathSpace)
	
	'Breakdown original file name and get the file extension
	FileNameArray = Split(FileName, ".")
	FileNameSpace = UBound(FileNameArray)
	FileNameExt = FileNameArray(FileNameSpace)
	
	'Change file name
	NewFileName = "evt" & iEventID & "." & FileNameExt
	
	'Upload the image file
	objFileUp.SaveAs NewFileName
	
	'Update image path in DB
	sSQL = "Update afxEvents SET ImgPath = '" & NewFileName & "' WHERE EventID ='" & iEventID & "'"
	
	objConn.Execute(sSQL)
	
	Set objConn = Nothing
	set objFileUp = nothing
	Response.redirect "list.asp"
End If
%>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
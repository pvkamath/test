<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Confirm Event Deletion"

	Dim sSQL, rs, objConn, strSQLConnectionString
	Dim iEventID, id, aEventID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("delevent")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top" class="activeTitle">Confirm Delete<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content here -->
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="300" valign="top">
		<%	If isnull(iEventID) or iEventID = "" Then	%>	
			<table width="95%" border="0">
				<tr>
					<td>You have not selected an event to be deleted.<BR>Please press the back button and try again.</td>
				</tr>
				<tr>
					<td align="center"><BR><a href="#" onClick="history.go(-1)">
						<img src="/admin/calendarfx/media/images/btn_back.gif" border="0" name="clear" value="clear"></a></td>
				</tr>
			</table>
		<%	Else	%>
			<table width="100%" border="0" cellpadding="4">
				<tr>
					<td colspan="2"><B>The following event(s) will be deleted if you press Delete:</B></td>
				</tr>
				<form method=post action="DelEventProc.asp" id=form1 name=form1>
		<%
				aEventID = split(iEventID, ",")

				For each id in aEventID

					sSQL = "Select Event From afxEvents Where EventID = '" & id & "'"
					rs.Open sSQL, objConn, 3, 1
		%>
				<tr>
					<td width="15%"><b>Event Name:</b></td>
					<td width="85%"><%=rs("Event")%></td>
				</tr>
		<%
					rs.Close
				Next
		%>
				<tr>
					<td colspan="2"><BR><input type="image" value="Delete" border=0 alt="delete" id="delete" name="delete" src="/admin/calendarfx/media/images/btn_delete.gif">&nbsp;&nbsp;
					<a href="#" onClick="history.go(-1)"><img src="/admin/calendarfx/media/images/btn_cancel.gif" width="91" height="27" alt="Cancel" border="0"></a>
					</td>
				</tr>
				<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
			</form>	
			</table>
		<%
			End If

			Set rs = Nothing
			Set objConn = Nothing
		%>
		</td>
<!-- end content -->				
	</tr>
</table>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

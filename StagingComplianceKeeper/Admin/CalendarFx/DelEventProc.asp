<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Delete Events"

	Dim sSQL, rs, objConn, strSQLConnectionString
	Dim iEventID, id, aEventID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("eventid")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top" class="activeTitle">Delete  Event<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content here -->
<table width="90%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td height="300" valign="top">
<%
aEventID = split(iEventID, ",")

For each id in aEventID

	'Delete event
	sSQL = "Delete From afxEvents Where EventID = '" & id & "'; " & _
		   "Delete From afxEventsEventTypeX Where EventID = '" & id & "'; " & _
		   "Delete From afxEventDisplayDateX Where EventID = '" & id & "'; " & _
		   "DELETE FROM afxWeeklyRecurrenceEvents WHERE EventID = '" & id & "'; " & _
		   "DELETE FROM afxMonthlyRecurrenceEvents WHERE EventID = '" & id & "' "
	objConn.Execute (sSQL)	
Next

Set objConn = Nothing
%>
			<table width="500" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><span>Event(s) Deleted Successfully</span></td>
				</tr>
				<!--<tr>
					<td ALIGN="center">&nbsp;</td>
				</tr>
				<tr>
					<td ALIGN="center"><a href="List.asp?keywords=">Return to Event List</a></td>
				</tr>-->
			</table>
		</td>
<!-- end content -->
	</tr>
        
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Confirm Event Type Deletion"

	Dim sSQL, rs, objConn, strSQLConnectionString
	Dim iEventTypeID, id, aEventTypeID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventTypeID = Request("deleventtypeid")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 <tr>
        	<td valign="top" class="activeTitle">Delete Event Type<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
<!-- content here -->			
		<td height="300" valign="top">
		<%	If isnull(iEventTypeID) or iEventTypeID = "" Then	%>	
			<table width="95%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>You have not select a event type to delete. Please press the back button and try again.</td>
				</tr>
				<tr>
					<td><BR><a href="#" onClick="history.go(-1)">
						<img src="/admin/calendarfx/media/images/btn_back.gif" border="0" name="clear" value="clear"></a></td>
				</tr>
			</table>
		<%	Else
				aEventTypeID = split(iEventTypeID, ",")
				EventExistFlag = False
				
				For each id in aEventTypeID
					sSQL = "SELECT * From afxEventsEventTypeX Where EventTypeID = '" & id & "'"
					rs.Open sSQL, objConn, 3, 1
					
					If NOT rs.EOF Then
						EventExistFlag = True
						rs.Close
						Exit For
					End If
					
					rs.Close
				Next
				
				If EventExistFlag Then
		%>
					<table width="95%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td>One or more of the event types you chose cannot be deleted until all events associated with these event types have been deleted.</td>
						</tr>
						<tr>
							<td><BR><a href="#" onClick="history.go(-1)">
								<img src="/admin/calendarfx/media/images/btn_back.gif" border="0" name="clear" value="clear"></a></td>
						</tr>   
					</table>
					
		<%		Else	%>
					<table width="100%" border="0" cellpadding="4" cellspacing="0">
						<tr>
							<td colspan="2"><B>The following event type(s) will be deleted if you press Delete:</B></td>
						</tr>
						<form method=post action="EventTypeProc.asp" id=form1 name=form1>
						<input type="hidden" name="action" id="action" value="delete">
						<input type="hidden" name="deleventtypeid" id="deleventtypeid" value="<%=iEventTypeID%>">
									
		<%			For each id in aEventTypeID
						sSQL = "Select * From afxEventType Where EventTypeID = '" & id & "'"
						rs.Open sSQL, objConn, 3, 1
		%>
						<tr>
							<td width="23%"><b>Event Type:</b></td>
							<td width="77%"><%=rs("EventType")%></td>
						</tr>
		<%
						rs.Close
					Next
		%>
						<tr>
							<td colspan="2"><BR><input type="image" value="Delete" border=0 alt="delete" id="Submit" name="Submit" src="/admin/calendarfx/media/images/btn_delete.gif">&nbsp;&nbsp;
							<a href="#" onClick="history.go(-1)"><img src="/admin/calendarfx/media/images/btn_cancel.gif" width="91" height="27" alt="Cancel" border="0"></a>
							</td>
						</tr>
					</form>	
					</table>
		<%
				End If
			End If
			
			Set rs = Nothing
			Set objConn = Nothing
		%>
		</td>
<!-- end content -->				
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

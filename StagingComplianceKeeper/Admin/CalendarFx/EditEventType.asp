<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = True			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Edit Event Type"

	Dim objConn, rs, sSQL, iEventTypeID, strSQLConnectionString, sEventType

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventTypeID = Request("eventtypeid")

	sSQL = "Select * From afxEventType Where EventTypeID = '" & iEventTypeID & "'"

	rs.Open sSQL, objConn, 3, 1

	sEventType = replace(rs("EventType"), """", "&quot;")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ValidateForm(frm) {
			if (frm.eventtype.value == "") {
				alert("Please enter a event type.");
				frm.eventtype.focus();
				return false;
				}
			else
				return true;
		}

//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top" class="activeTitle">Edit Event Type <%PrintNameOfSection(1)%><br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<table width="90%" cellpadding="0" cellspacing="0" border="0">
	<tr>
<!-- content here -->			
		<td height="300" valign="top">
		<FORM action="EventTypeProc.asp" method="Post" name="frm" id="frm" onSubmit="return ValidateForm(document.frm)">
			<input type="hidden" name="action" id="action" value="edit">
			<input type="hidden" name="eventtypeid" id="eventtypeid" value="<%=iEventTypeID%>">
			<table width="565" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="bold">Event Type:</td>
					<td width="80%"><input type="text" name="eventtype" id="eventtype" value="<%=sEventType%>" size="50" maxlength=50></td>
				</tr>
				
				<tr>
					<td colspan="2">&nbsp;<br>
						<input type="image" src="/admin/calendarfx/media/images/btn_submit.gif" border="0" name="submit" value="Submit">
						&nbsp;&nbsp;
						<a href="addEventType.asp"><img src="/admin/calendarfx/media/images/btn_cancel.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</FORM>				
		</td>
<!-- end content -->					
	</tr>            
</table>
<%
rs.Close
Set rs = Nothing
%>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

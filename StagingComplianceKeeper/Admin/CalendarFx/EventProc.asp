<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/RecurDateMethods.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Please wait . . ."

	Dim iEventID, sEvent, iSectionID, dStartDate, dEndDate, sHour, sMinute, sAMPM
	Dim eHour, eMinute, eAMPM, sLocation, sOrganization, sPhone, iEventTypeID
	Dim sURL, sContact, sEmail, sInfo, iActive, sAction, word
	Dim objConn, rs, sSQL, strSQLConnectionString
	dim tmpDateEnd, sMode
	Dim iCourseID, sCity, iStateID

	sEvent = replace(request("event"), "'", "''")
	iSectionID = request("SectionID")
	dStartDate = request("startdate")
	dEndDate = trim(request("enddate"))
	'if end date is blank, then it's an on-going event
	if dEndDate = "" then
		dEndDate = "1/1/2020"
	end if
	sHour = request("shour")
	sMinute = request("sminute")
	sAMPM = request("sampm")
	eHour = request("ehour")
	eMinute = request("eminute")
	eAMPM = request("eampm")
	iEventTypeID = request("EventTypeID")
	iCourseID = replace(request("CourseID"), "'", "''")
	sCity = replace(request("City"), "'", "''")
	iStateID = replace(request("State"), "'", "''")
	sLocation = replace(request("location"), "'", "''")
	sOrganization = replace(request("organization"), "'", "''")
	sMode = request("mode")
	'sPhone = request("phone")
	'sURL = replace(request("url"), "'", "''")
	'sContact = replace(request("contact"), "'", "''")
	'sEmail = request("email")
	sInfo = replace(request("info"), "'", "''")
	iActive = request("active")
	sAction = request("editmode")
	sRecurrenceType = replace(request("RecurrenceType"), "'", "''")
	iWeekCycle = replace(request("WeekCycle"), "'", "''")
	sWeekDays = replace(request("WeekDays"), "'", "''")
	iMonthCycle = replace(request("MonthCycle"), "'", "''")
	iMonthOccurrence = replace(request("MonthOccurrence"), "'", "''")
	sMonthWeekDayOfOccurrence = replace(request("MonthWeekDayOfOccurrence"), "'", "''")
	iEventSpan = replace(request("EventSpan"), "'", "''")
	dStartDate = trim(dStartDate) 

	if len(sHour) <> 0 then	
		tmpDateEnd =  sHour & ":" & sMinute & " " & UCase(sAMPM)
		
		if ucase(trim(tmpDateEnd)) = ucase("12:00 am") then
			tmpDateEnd = "00:00:01"
		end if
		
		dStartDate = dStartDate & " " & tmpDateEnd
	end if
	
	dEndDate = trim(dEndDate) 
	
	if len(eHour) <> 0 then
		tmpDateEnd = eHour & ":" & eMinute & " " & UCase(eAMPM)

		if ucase(trim(tmpDateEnd)) = ucase("12:00 am") then
			tmpDateEnd = "00:00:01"
		end if

		dEndDate = dEndDate & " "  & tmpDateEnd
	end if

	
	If iActive = "on" Then
		iActive = "1"
	Else
		iActive = "0"
	End If

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	Select Case UCase(sAction)
		Case "ADD"
			'Add new event
			sSQL = "INSERT INTO afxEvents (StartDate, EndDate, Event, City, Location, " & _
					"Phone, WebAddress, Contact, Email, Directions, Organization, Active, SectionID " 
			
			'if Course ID is set	
			if trim(iCourseID) <> "" then
				sSQL = sSQL & ",CourseID"
			end if
			
			'if stateID is set
			if trim(iStateID) <> "" then
				sSQL = sSQL & ",StateID"
			end if					
			
			sSQL = sSQL & ") VALUES ('" & dStartDate & "', '" & dEndDate & "', '" & _
					sEvent & "', '" & sCity & "','" & sLocation & "', '" & _
					sPhone & "', '" & sURL & "', '" & sContact & "', '" & sEmail & "', '" & sInfo & "', '" & sOrganization & "', '" & _
					iActive & "', '" & iSectionID & "'"
			
			'if Course ID is set	
			if trim(iCourseID) <> "" then
				sSQL = sSQL & ",'" & iCourseID & "'"
			end if
			
			'if stateID is set
			if trim(iStateID) <> "" then
				sSQL = sSQL & ",'" & iStateID & "'"
			end if			
			
			sSQL = sSQL & ")"

			objConn.Execute(sSQL)
			
			'Get the max event id which is the new event id
			sSQL = "Select max(EventID) From afxEvents"
			
			rs.Open sSQL, objConn, 3, 1	
			
			if not rs.EOF then
				iEventID = rs(0)

				sSQL = "INSERT INTO afxEventsEventTypeX (EventID, EventTypeID) " & _
						"VALUES ('" & iEventID & "', '" & iEventTypeID & "')"
				objConn.Execute(sSQL)
			end if
			rs.Close
		Case "EDIT"
			
			'if session("access_level") >= 10 then
				iEventID = Request("eventid")
				
				sSQL = "UPDATE afxEvents " & _
					   "SET StartDate = '" & dStartDate & "', " & _
					   "	EndDate = '" & dEndDate & "', " & _
					   "	Event = '" & sEvent & "', "
					   
				if trim(iCourseID) <> "" then
					sSQL = sSQL & "	CourseID = '" & iCourseID & "', "
				end if
				
				if trim(iStateID) <> "" then
					sSQL = sSQL & "	StateID = '" & iStateID & "', " 
				end if 
				
				sSQL = sSQL & "	City = '" & sCity & "', " & _
	  					      "	Location = '" & sLocation & "', " & _
					   		  "	Phone = '" & sPhone & "', " & _
					   		  "	WebAddress = '" & sURL & "', " & _
					   		  "	Contact = '" & sContact & "', " & _
					   		  "	Organization = '" & sOrganization & "', " & _
					   		  "	Email = '" & sEmail & "', " & _
					   		  "	Directions = '" & sInfo & "', " & _
					   		  "	Active = '" & iActive & "', " & _
					   		  "	SectionID = '" & iSectionID & "' " & _
					   		  "Where EventID = '" & iEventID & "'"
				
				objConn.Execute(sSQL)
				
				sSQL = "UPDATE afxEventsEventTypeX SET EventTypeID = '" & iEventTypeID & "' " & _
						"WHERE EventID = '" & iEventID & "'"
				
				objConn.Execute(sSQL)
	End Select

	'Delete display date Xreference table entries
	sSQL = "DELETE FROM afxEventDisplayDateX WHERE EventID = '" & iEventID & "'; "
	objConn.Execute(sSQL)	

	'Add display date to Xreference table for regular events
	if trim(sRecurrenceType) = "" then
		sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
			   "VALUES ('" & iEventID & "', '" & dStartDate & "','" & dEndDate & "')"
			   
		objConn.Execute(sSQL)	
	end if
	
	'Process the Recurrence options
	'delete old options, if any
	sSQL = "DELETE FROM afxWeeklyRecurrenceEvents WHERE EventID = '" & iEventID & "'; " & _
		   "DELETE FROM afxMonthlyRecurrenceEvents WHERE EventID = '" & iEventID & "' "
	objConn.Execute(sSQL)
	
	if Ucase(sRecurrenceType) = "WEEKLY" AND iWeekCycle <> "" AND sWeekDays <> "" then
		'Retrieve all the dates this event will display
		sDateString = ComputeWeeklyRecurDates(dStartDate, dEndDate, iWeekCycle, sWeekDays)
		aRecurDates = split(sDateString, ", ")
		
		for each sRecurDate in aRecurDates
			'Since reccurrence events display on multiple days, the start and end date for a
			'given occurrence should be the same
			sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
				   "VALUES ('" & iEventID & "', '" & sRecurDate & "','" & dateadd("d", iEventSpan-1, sRecurDate) & "')"
	   
			objConn.Execute(sSQL)	
		next

		sSQL = "INSERT INTO afxWeeklyRecurrenceEvents(EventID, WeekCycle, Weekdays, EventSpan) " & _
			   "VALUES ('" & iEventID & "', '" & iWeekCycle & "', '" & sWeekDays & "','" & iEventSpan & "')"
			   
		objConn.Execute(sSQL)			 
	end if
	
	if Ucase(sRecurrenceType) = "MONTHLY" AND iMonthCycle <> "" AND iMonthOccurrence <> "" and sMonthWeekDayOfOccurrence <> "" then
		'Retrieve all the dates this event will display
		sDateString = ComputeMonthlyRecurDates(dStartDate, dEndDate, iMonthOccurrence, iMonthCycle, sMonthWeekDayOfOccurrence)
		aRecurDates = split(sDateString, ", ")
		
		for each sRecurDate in aRecurDates
			'Since reccurrence events display on multiple days, the start and end date for a
			'given occurrence should be the same		
			sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
				   "VALUES ('" & iEventID & "', '" & sRecurDate & "','" & dateadd("d", iEventSpan-1, sRecurDate) & "')"
	   
			objConn.Execute(sSQL)	
		next
			
		sSQL = "INSERT INTO afxMonthlyRecurrenceEvents(EventID, Occurrence, MonthCycle, WeekDayOfOccurrence, EventSpan) " & _
			   "VALUES ('" & iEventID & "', '" & iMonthOccurrence & "', '" & iMonthCycle & "', '" & sMonthWeekDayOfOccurrence & "','" & iEventSpan & "')"
			   
		objConn.Execute(sSQL)			 
	end if
	
	response.redirect "AddPicture.asp?eventid=" & iEventID
	Set rs = Nothing

	objConn.Close
	Set objConn = Nothing

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

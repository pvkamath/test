<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""

	Dim iSectionID, iEventTypeID, sEventType, sAction, EventTypeIDArray, id, iDelEventTypeID
	Dim sSQL, objConn, strSQLConnectionString

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	sAction = Request("action")
	iSectionID = CInt(Request("SectionID"))
	iEventTypeID = CInt(Request("EventTypeID"))
	sEventType = trim(Replace(Request("EventType"), "'", "''"))
	
	sSQL = "SELECT EventType from afxEventType where EventType='" &  sEventType & "'"
		Set rsEventType = Server.CreateObject("ADODB.Recordset")							
		rsEventType.Open sSQL, objConn, 3, 1
		
		If Not rsEventType.EOF And sAction <> "delete" Then
			'List available locations
			Do While Not rsEventType.EOF
				if  UCase(rsEventType("EventType")) =  UCase(sEventType) Then			
					response.redirect "addEventType.asp?SectionID=" & iSectionID & "&message=" & sEventType & "&action=" & sAction
				End If
				rsLocation.MoveNext
			Loop
		End If
	
	Select Case sAction
		Case "add"
			
			'If iSectionID <> 0 Then
				sSQL = "INSERT INTO afxEventType (EventType) VALUES ('" & sEventType & "')"
				objConn.Execute(sSQL)
			'End If
			
		Case "edit"
			
			sSQL = "UPDATE afxEventType SET EventType = '" & sEventType & "' Where EventTypeID = '" & iEventTypeID & "'"
			objConn.Execute(sSQL)
			
		Case "delete"
			
			iDelEventTypeID = Request("DelEventTypeID")
			
			EventTypeIDArray = Split(iDelEventTypeID, ",")
			
			For each id in EventTypeIDArray
				sSQL = "DELETE From afxEventType Where EventTypeID = '" & id & "'"
				objConn.Execute(sSQL)
			Next
	End Select

	objConn.Close
	Set objConn = Nothing

	response.redirect "addEventType.asp?SectionID=" & iSectionID

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

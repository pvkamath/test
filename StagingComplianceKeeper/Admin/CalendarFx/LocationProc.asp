<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""

	Dim iSectionID, iLocationID, sLocationName, sAction, LocationIDArray, id, iDelLocationID
	Dim sSQL, objConn, strSQLConnectionString, rsLocation

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	sAction = Request("action")
	iSectionID = CInt(Request("SectionID"))
	iLocationID = CInt(Request("LocationID"))
	sLocationName = trim(Replace(Request("locationname"), "'", "''"))
	sLocationOldName = trim(Replace(Request("locationOldName"), "'", "''"))
	
		sSQL = "SELECT name from afxCalendarLocations where name='" &  sLocationName & "'"
		Set rsLocation = Server.CreateObject("ADODB.Recordset")							
		rsLocation.Open sSQL, objConn, 3, 1
		
		If Not rsLocation.EOF Then
			'List available locations
			Do While Not rsLocation.EOF
				if  UCase(rsLocation("Name")) =  UCase(sLocationName) Then			
					response.redirect "addlocation.asp?SectionID=" & iSectionID & "&message=" & sLocationName & "&action=" & sAction
				End If
				rsLocation.MoveNext
			Loop
		End If
		
	Select Case sAction
		Case "add"
		
			'If iSectionID <> 0 Then
				sSQL = "INSERT INTO afxCalendarLocations (Name) VALUES ('" & sLocationName & "')"
				objConn.Execute(sSQL)
			'End If
			
		Case "edit"
					
			sSQL = "UPDATE afxCalendarLocations SET Name = '" & sLocationName & "' Where ID = '" & iLocationID & "'"
			objConn.Execute(sSQL)
			
			sSQL = "UPDATE afxEvents SET Location = '" & sLocationName & "' Where Location = '" & sLocationOldName & "'"
			objConn.Execute(sSQL)
			
		Case "delete"
		
			iDelLocationID = Request("dellocationid")
			
			LocationIDArray = Split(iDelLocationID, ",")
			
			For each id in LocationIDArray
				sSQL = "DELETE From afxCalendarLocations Where ID = '" & id & "'"
				objConn.Execute(sSQL)
			Next
	End Select

	objConn.Close
	Set objConn = Nothing

	response.redirect "addlocation.asp?SectionID=" & iSectionID

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

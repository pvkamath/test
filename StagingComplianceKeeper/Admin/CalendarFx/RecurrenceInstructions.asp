<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/style.css"">
<table width="100%">
	<tr>
		<th align="center" width="100%">INSTRUCTIONS</th>
	</tr>
	<tr>
		<td><br>Recurrence events will only display on the days that match the recurrence options between the Start and End dates.<br><br></td>
	</tr>
	<tr>
		<td><b>Weekly Recurrence</b></td>
	</tr>
	<tr>
		<td>
			<ol>
				<li>Type in the frequency in the <i>Recur Every _ week(s)</i> box (1 is for every week, 2 is for every other week, 3 is for every three weeks, etc.)</li>
				<li>Select the day(s) of the week.</li>
				<li>Type in the number of days the event will span in the <i>The event spans _ days</i> box.</li>
				<li>Click OK</li>
			</ol>
		</td>
	</tr>
	<tr>
		<td><b>Monthly Recurrence</b></td>
	</tr>
	<tr>
		<td>
			<ol>
				<li>Select which occurrence (first, second, third, fourth, or last occurrence of the month).</li>
				<li>Select which day of the week.</li>
				<li>Type in the frequency in the <i>of every _ month(s)</i> box (1 is for every month, 2 is for every other month, 3 is for every three months, etc.)</li>
				<li>Type in the number of days the event will span in the <i>The event spans _ days</i> box.</li>
				<li>Click OK</li>
			</ol>
		</td>
	</tr>	
</table>
</body>
</html>

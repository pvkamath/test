<%
iMonthCycle = request("MonthCycle")
iMonthOccurrence = request("MonthOccurrence")
sMonthWeekDayOfOccurrence = request("MonthWeekDayOfOccurrence")
iEventSpan = request("EventSpan")

'if recurrence set, enable remove button
if iMonthCycle <> "" then
	sRemoveBtn = ""
else
	sRemoveBtn = "disabled"
end if
%>

<html>

<script language="JavaScript" src="/admin/includes/FormCheck2.js"></script>
<script language="JavaScript">
function ValidateForm(FORM)
{
	//Check Recurrence Field
	if (!isDigit(FORM.MonthCycle.value) || (FORM.MonthCycle.value < 1)) 
	{
		alert('The recurrence field must be a number greater than 0.');
		FORM.MonthCycle.focus();
		return false;
	}
	//Check Event Span
	if (!isDigit(FORM.EventSpan.value) || (FORM.EventSpan.value < 1)) 
	{
		alert('The event span must be a number greater than 0.');
		FORM.EventSpan.focus();
		return false;
	}	
	
	return true;
}

function SubmitValues(FORM)
{
	if (ValidateForm(FORM))
	{
		window.parent.opener.document.frm.RecurrenceType.value = "MONTHLY"
		window.parent.opener.document.frm.MonthCycle.value = FORM.MonthCycle.value;
		window.parent.opener.document.frm.MonthOccurrence.value = FORM.MonthOccurrence.value;		
		window.parent.opener.document.frm.MonthWeekDayOfOccurrence.value = FORM.MonthWeekDayOfOccurrence.value;				
		window.parent.opener.document.frm.EventSpan.value = FORM.EventSpan.value;
		window.parent.opener.document.frm.RecurrenceLink.value = "recurrence.asp?RecurrenceType=MONTHLY&MonthCycle=" + FORM.MonthCycle.value + "&MonthOccurrence=" + FORM.MonthOccurrence.value + "&MonthWeekDayOfOccurrence=" + FORM.MonthWeekDayOfOccurrence.value + "&EventSpan=" + FORM.EventSpan.value
		window.parent.opener.document.frm.WeekCycle.value = '';
		window.parent.opener.document.frm.WeekDays.value = '';				
		window.parent.close();
	}
}

function RemoveValues(FORM)
{
	window.parent.opener.document.frm.RecurrenceType.value = '';
	window.parent.opener.document.frm.WeekCycle.value = '';
	window.parent.opener.document.frm.WeekDays.value = '';		
	window.parent.opener.document.frm.MonthCycle.value = '';
	window.parent.opener.document.frm.MonthOccurrence.value = '';		
	window.parent.opener.document.frm.MonthWeekDayOfOccurrence.value = '';	
	window.parent.opener.document.frm.EventSpan.value = '';			
	window.parent.opener.document.frm.RecurrenceLink.value = 'recurrence.asp';
	window.parent.close();
}
</script>
<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/style.css"">
<body>

<form name="frm1">
<table align="center">
	<tr>
		<th align="center">Monthly Recurrence</th>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td nowrap>The &nbsp;<select name="MonthOccurrence">
						<option value="1" <% if iMonthOccurrence = 1 then response.write("selected") end if %>>First</option>
						<option value="2" <% if iMonthOccurrence = 2 then response.write("selected") end if %>>Second</option>
						<option value="3" <% if iMonthOccurrence = 3 then response.write("selected") end if %>>Third</option>
						<option value="4" <% if iMonthOccurrence = 4 then response.write("selected") end if %>>Fourth</option>						
						<option value="5" <% if iMonthOccurrence = 5 then response.write("selected") end if %>>Last</option>												
					  </select>
					  <select name="MonthWeekDayOfOccurrence">
					  	<option value="Sunday" <% if ucase(sMonthWeekDayOfOccurrence) = "SUNDAY" then response.write("selected") end if %>>Sunday</option>
					  	<option value="Monday" <% if ucase(sMonthWeekDayOfOccurrence) = "MONDAY" then response.write("selected") end if %>>Monday</option>
					  	<option value="Tuesday" <% if ucase(sMonthWeekDayOfOccurrence) = "TUESDAY" then response.write("selected") end if %>>Tuesday</option>
					  	<option value="Sunday" <% if ucase(sMonthWeekDayOfOccurrence) = "WEDNESDAY" then response.write("selected") end if %>>Wednesday</option>
					  	<option value="Thursday" <% if ucase(sMonthWeekDayOfOccurrence) = "THURSDAY" then response.write("selected") end if %>>Thursday</option>
					  	<option value="Friday" <% if ucase(sMonthWeekDayOfOccurrence) = "FRIDAY" then response.write("selected") end if %>>Friday</option>
					  	<option value="Saturday" <% if ucase(sMonthWeekDayOfOccurrence) = "SATURDAY" then response.write("selected") end if %>>Saturday</option>																																				
					  </select>&nbsp;
					  of every&nbsp;<input name="MonthCycle" type="text" value="<%= iMonthCycle %>" size="2" maxlength="2">&nbsp;month(s)
		</td>
	</tr>
	<tr>
		<td>
			The event spans&nbsp;<input type="text" size="3" maxlength="3" name="EventSpan" value="<%=iEventSpan%>">&nbsp;days
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td align="center"><input type="button" name="okBtn" value="OK" onClick="SubmitValues(this.form)">&nbsp;<input type="button" name="cancelBtn" value="Cancel" onClick="javascript:window.parent.close()">&nbsp;<input type="button" name="removeBtn" value="Remove" <%= sRemoveBtn %> onClick="RemoveValues(this.form)"></td>
	</tr>
</table>
</form>

</body>
</html>

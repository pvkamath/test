<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" --------------------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
'	checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""

	Dim iEventID
	Dim rs, objConn, strSQLConnectionString, searching

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("eventid")
	sEventType = request("EventType")
	
	'sSQL = "SELECT *, ET.EventType " & _
			'"FROM afxEvents E, afxEventType ET, afxEventsEventTypeX EET " & _
			'"WHERE E.EventID = EET.EventID AND EET. EventTypeID = ET.EventTypeID " & _
			'"AND E.EventID =  '" & iEventID & "'"
			
	'sSQL = "SELECT * FROM afxEvents WHERE EventID = '" & iEventID & "'"

	If sEventType <> "" Then
	sSQL = "SELECT *, ET.EventType FROM afxEvents AS E " & _
			"INNER JOIN afxEventsEventTypeX AS EET ON (E.EventID = EET.EventID) " & _
			"INNER JOIN afxEventType AS ET ON (EET. EventTypeID = ET.EventTypeID) " & _
			"LEFT OUTER JOIN States as S on (E.StateID = S.StateID) " & _
			"WHERE E.EventID =  '" & iEventID & "'"
	Else 
		sSQL = "Select * From afxEvents as E " & _
			"LEFT OUTER JOIN States as S on (E.StateID = S.StateID) " & _
			"WHERE E.EventID =  '" & iEventID & "'"
	End If
	
	rs.Open sSQL, objConn, 3, 1
	
	set searching = server.CreateObject("NewsFXV2.Searching")
		dim sDirections
		if isnull(rs("directions")) then
			sDirections = " " 
		else
			sDirections = rs("directions")
		end if

		if (not isnull(rs("CourseID")) and trim(rs("CourseID")) <> "") then
			'Get the course name
			dim oCourseObj, courseRS
			
			set oCourseObj = New Course
			oCourseObj.ConnectionString = application("sDataSourceName")
			oCourseObj.CourseID = rs("CourseID")
			set courseRS = oCourseObj.GetCourse()
			
			if not rs.eof then
				sCourseName = courseRS("Name")
			end if
			
			set courseRS = nothing
			set oCourseObj = nothing
		end if
		
		sCity = rs("City")
		sState = rs("State")
				
		if (trim(sCity) <> "" or trim(sState) <> "") then
			if (trim(sCity) <> "" and trim(sState) <> "") then
				sLocation = sCity & ", " & sState
			elseif (trim(sCity) <> "") then
				sLocation = sCity
			else
				sLocation = sState
			end if
		else
			sLocation = ""
		end if		
		
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<!--title line	 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 <tr>
        	<td class="activeTitle">View Event <br><img src="/admin/login/media/images/spacer.gif" width="10" height="10"><br><a href="#" onClick="history.go(-1)" class="back">Back</a><br>
			<img src="/admin/login/media/images/spacer.gif" width="10" height="10"></td>
	</tr>
</table>
<!-- content here -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td rowspan="6">&nbsp;</td>
					<td align="right" nowrap>Event Title</td>
					<td rowspan="6">&nbsp;</td>
					<td rowspan="6">&nbsp;</td>
					<td width="100%"><%=rs("Event")%></td>
				</tr>
				<% If trim(rs("ImgPath")) <> "" Then %>				
				<tr>
					<td align="right" nowrap>Image</td>
					<td class="event" valign="top"><img src="<%=Application("sDynWebroot")%>usermedia/images/calendarfx/<%=rs("ImgPath")%>" alt="xig" border="0">
						<br></td>
				</tr>
				<%End If%>			
				<% if trim(sCourseName) <> "" then %>								
				<tr>
					<td align="right" nowrap>Course</td>
					<td valign="top">
						<%
						Response.Write(sCourseName)
						%>
					</td>
				</tr>			
				<%End If%>		
				<% if trim(sLocation) <> "" then %>								
				<tr>
					<td align="right" nowrap>Location</td>
					<td valign="top">
						<%
						'Dim rsSection, sSection
						
						'set rsSection = server.CreateObject("ADODB.Recordset")
						
						'sSQL = "Select Name From NavMenuItem Where ID = " & rs("SectionID")
						'rsSection.Open sSQL, objConn, 3, 1
						
						'if not rsSection.EOF then
							'sSection = rsSection("Name")
						'else
							'sSection = ""
						'end if
						'rsSection.Close 
						'set rsSection = nothing
						
						Response.Write(sLocation)
						%>
					</td>
				</tr>		
				<% end if %>					
				<% if trim(sDirections) <> "" then	%>					
				<tr>
					<td align="right" nowrap>General Information</td>
					<td class="event" valign="top"><%= searching.TruncateLength( sDirections, 100000)%></td>
				</tr>
				<%End If%>								
				<tr>
					<td align="right" nowrap>Date and Time</td>
					<td valign="top">
						<%
						Dim sDateArray, sEventStartDate, sEventTime, sTime, sTimeStart, sTimeEnd 
						
						'Compute and Display Start Date
						sDateArray = Split(rs("StartDate"), " ")
								
						sEventStartDate = sDateArray(0)
						'sEventMonth = Month(sEventDate)
						'sEventDay = Day(rs("StartDate"))
						'sEventYear = Year(sEventDate)
						
						'Must set a check for when the time is equal to 12AM,
						'because the Database does not store the time value for 12AM
						if (UBound(sDateArray) > 0) then
							sEventTime = sDateArray(1)
							sTime = Split(sEventTime, ":")
							sTimeStart = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
						else
							'sTimeStart = "12:00 AM"
							sTimeStart = ""
						end if
						
						response.write FormatDateTime(sEventStartDate, 1) & " " & sTimeStart
						
						'Compute and Display End Date
						sDateArray = Split(rs("EndDate"), " ")
								
						sEventEndDate = sDateArray(0)
						'sEventMonth = Month(sEventDate)
						'sEventDay = Day(rs("StartDate"))
						'sEventYear = Year(sEventDate)
						
						'Must set a check for when the time is equal to 12AM,
						'because the Database does not store the time value for 12AM
						if (UBound(sDateArray) > 0) then
							sEventTime = sDateArray(1)
							sTime = Split(sEventTime, ":")
							sTimeEnd = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
						else
							'sTimeEnd = "12:00 AM"
							sTimeEnd = ""
						end if
						
						
						If sEventEndDate = sEventStartDate Then
							if len(sTimeEnd) <> 0 then 
								response.write " - " & sTimeEnd
							end if
						Else
							if sEventEndDate = "1/1/2020" then
								response.write " to On-Going"
							else
								response.write " to <BR>" & FormatDateTime(sEventEndDate, 1) & " " & sTimeEnd
							end if
						End If
						%>
					</td>
				</tr>
				<% if trim(sEventType) <> "" then %>								
				<tr>
					<td align="right" nowrap>Event Type</td>
					<td valign="top"><% If sEventType <> "" Then 
					 		Response.Write(rs("EventType"))
						End If
					%></td>
				</tr>	
				<% end if %>							
			</table>		
		</td>		
	</tr>
</table>
<%
rs.Close
Set rs = Nothing
set searching = nothing
%>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

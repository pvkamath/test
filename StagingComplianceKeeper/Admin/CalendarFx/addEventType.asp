<!-- #include virtual = "/admin/calendarfx/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/calendarfx/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/calendarfx/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
'	checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Add a New Event Type"

	Dim strSQLConnectionString, objConn, sSQL, rsEventType
	Dim iSectionID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	iSectionID = Request("SectionID")
	sMessage = Request("message")
	sAction = request("action")

	if iSectionID = "" then
		iSectionID = 0
	else
		iSectionID = cint(iSectionID)
	end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function LocationUpdate(frm) {
			var SecID = frm.SectionID.options[frm.SectionID.selectedIndex].value;
			location.href="addlocation.asp?SectionID=" + SecID
		}

		function ValidateForm(frm) {
			if (frm.eventtype.value == "") {
				alert("Please enter a event type.");
				frm.eventtype.focus();
				return false;
				}
			//else if (frm.SectionID.selectedIndex == 0) {
				//alert("Please select a valid Program.");
				//frm.SectionID.focus();
				//return false;	
			//}
			else
				return true;
		}

		function ClearCheckbox(frm2) {
			for (i = 0; i < frm2.deleventtypeid.length; i++) {
				 frm2.deleventtypeid[i].checked = false;
			}
		   return true;
		}

		function Clear(frm){
			frm.eventtype.value = "";
			frm.eventtype.focus();
		}


//--></script>

<script language="javascript" src="/admin/calendarfx/includes/FormCheck2.js"></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<!-- title line -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top"><span class="activeTitle">Add an Event Type</span>
			<p>Type the new event type in the text box and then click <b>Add</b> to add an event type.<br>
			Click <b>Clear</b> if you would like to clear the text box and retype the event type.<br>
			To edit an event type click <b>Edit</b>.  To delete an event type select the corresponding boxes<br>
			in the delete column and then click <b>Delete</b>.</p>
			<p><b>You may not delete an event type until ALL events of this type are deleted.</b></p>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content here -->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="top"> 
			<FORM action="EventTypeProc.asp" Method="Post" name="frm" id="frm" onSubmit="return ValidateForm(document.frm);">
			<input type="hidden" name="action" id="action" value="add">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
<!-- begin content left column -->
					<td valign="top">
						<table height="130" border="0">
							<tr>
								<td colspan="2">Would you like to add a new event type?</td>
							</tr>
							<% If sMessage <> "" And sAction = "add" Then%>
							<tr>
								<td colspan="2">Event Type <b><%=sMessage%></b> already existed.</td>
							</tr>
							<%End If %>							
							<tr>
								<td>Event Type</td>
								<td><input type="text" name="eventtype" id="eventtype" size="20" maxlength="50"></td>
							</tr>
							<tr>
								<td colspan="2">
									<input type="image" src="/admin/calendarfx/media/images/btn_add.gif" border="0" name="submit" value="Submit">
									&nbsp;&nbsp;
									<a href="#" onClick="javascript:Clear(document.frm)">
									<img src="/admin/calendarfx/media/images/btn_clear.gif" border="0" name="clear" value="clear"></a>
								</td>
							</tr>
						</table>
					</td>
<!-- end content left column, begin content right column -->
					<td nowrap bgcolor="AliceBlue" valign="top"><b>Available Event Types:</b>
						<%
						'If iSectionID <> 0 Then								
							'Get available event types
							sSQL = "Select * From afxEventType Order by EventTypeID"
							
							Set rsEventType = Server.CreateObject("ADODB.Recordset")
							
							rsEventType.Open sSQL, objConn, 3, 1
							
							If Not rsEventType.EOF Then
								'List available event types
								Do While Not rsEventType.EOF
									If rsEventType("EventTypeID") <> 0 Then
										response.write ("<br>" & rsEventType("EventType"))
									End If
									rsEventType.MoveNext
								Loop
							Else
								response.write("<BR>No event type is available.")
							End If
						'Else
							'response.write("<BR>No Locations are available.")
						'End If
						%>
					</td>
<!-- end content right column -->
				</tr>
			</table> 
			</FORM>					
		</td>
<!-- end content -->					
	</tr>
	<tr>
		<td colspan="2">
<!-- edit location table -->
			<table width="100%" bgcolor="E6E6E6" cellpadding="2" cellspacing="2" border="0">
			<FORM action="DelEventTypeConfirm.asp" Method="Post" name="frm2" id="frm2">
				<% If sMessage <> "" And sAction = "edit" Then%>
					<tr>
						<td colspan="3">Event Type <b><%=sMessage%></b> already existed.</td>
					</tr>
				<%End If %>
				<tr class="reportHeaderColor1">
					<td class="reportHeaderText" height="10" colspan="3">Existing Event Types</td>
				</tr>
				<tr class="reportHeaderColor1b">
					<td width="34%" class="reportHeaderText">Edit</td>
					<td width="33%" class="reportHeaderText">Delete</td>
					<td width="34%" class="reportHeaderText">Event Type</td>					
				</tr>
					<%
					'if recordset not empty, move pointer back to the first record 
					if not rsEventType.BOF then
						rsEventType.MoveFirst
					end if
					
					If Not rsEventType.EOF Then
						Do While Not rsEventType.EOF
					
				If rsEventType("EventTypeID") <> 0 Then %>
				<tr class="row2">
					<td class="rowText" align="center"><a href="EditEventType.asp?EventTypeID=<%=rsEventType("EventTypeID")%>">Edit</a></td>
					<td align="center"><input type="checkbox" name="deleventtypeid" value="<%=rsEventType("EventTypeID")%>"</td>
					<td class="rowText" align="center"><b><%=rsEventType("EventType")%></b></td>
					<%
				End If
							rsEventType.MoveNext
						Loop
					End If
					rsEventType.Close
					%>
				</tr>
			</table>
			<table width="100%" cellpadding="2" cellspacing="2" border="0">
				<tr>
					<td colspan="4">&nbsp;<br>
						<input type="image" src="/admin/calendarfx/media/images/btn_delete.gif" border="0" name="delete" value="Submit">
						&nbsp;&nbsp;
						<a href="#" onClick="javascript:ClearCheckbox(document.frm2)">
						<img src="/admin/calendarfx/media/images/btn_clear.gif" border="0" name="clear" value="clear"></a>
					</td>
				</tr>
				</FORM>
			</table>
<!-- end edit Section table -->
		</td>
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
function GetDayOfWeek(p_sDate)
	dim iDay
	
	iDay = datepart("w", p_sDate)
	
	select case(iDay)
		case 1
			GetDayOfWeek = "SUNDAY"
		case 2
			GetDayOfWeek = "MONDAY"				
		case 3
			GetDayOfWeek = "TUESDAY"				
		case 4
			GetDayOfWeek = "WEDNESDAY"				
		case 5
			GetDayOfWeek = "THURSDAY"				
		case 6
			GetDayOfWeek = "FRIDAY"				
		case 7
			GetDayOfWeek = "SATURDAY"
		case default
			GetDayOfWeek = ""
	end select
end function

function ComputeWeeklyRecurDates(p_dStartDate, p_dEndDate, p_iWeekCycle, p_sRecurWeekDays)
	dim dStartDate, dEndDate, sDateString, dStartOfWeek, dEndOfWeek, dCurDate, sDayOfWeek
	dim aSelectedWeekDays, bDayFound, i
	
	aRecurWeekDays = split(p_sRecurWeekDays, ", ")
	dStartDate = formatdatetime(p_dStartDate,2)
	dEndDate = formatdatetime(p_dEndDate,2)
	sDateString = ""

	'do while start date hasn't reached the end date
	do while (datediff("d",dStartDate, dEndDate) >= 0)
		'compute the start date and end of week dates for the current week
		dStartOfWeek = dStartDate
		dEndOfWeek = dateadd("d", 6, dStartOfWeek)
		dCurDate = dStartOfWeek
		
		'if end of week is past end date of event, set end of week to end date of event
		if (datediff("d", dEndOfWeek, dEndDate) < 0) then
			dEndOfWeek = dEndDate
		end if
		
		'do while current date hasn't reached the end of the week
		do while (datediff("d",dCurDate, dEndOfWeek) >= 0)
			sDayOfWeek = GetDayOfWeek(dCurDate)
			
			i = 0
			bDayFound = false
			do while i <= UBound(aRecurWeekDays) and (not bDayFound)
				if (uCase(aRecurWeekDays(i)) = ucase(sDayOfWeek)) then
					if (trim(sDateString) = "") then
						sDateString = dCurDate
					else
						sDateString = sDateString & ", " & dCurDate
					end if
					bFound = true
				end if
				i = i + 1
			loop
			dCurDate = dateadd("d", 1, dCurDate)
		loop
		dStartDate = dateadd("ww", p_iWeekCycle, dStartDate)
	loop
	ComputeWeeklyRecurDates = sDateString
end function

function ComputeMonthlyRecurDates(p_dStartDate, p_dEndDate, p_iOccurrence, p_iMonthCycle, p_sWeekDayOfOccurrence)
	dim dCurMonth, dStartDate, dEndDate, dStartOfCurMonth, dEndOfCurMonth, dCurDate
	dim sDateString, iCurMonthDayOccurrence
	
	dStartDate = p_dStartDate
	dEndDate = p_dEndDate
	dCurMonth = dateserial(year(dStartDate), month(dStartDate), 1)
	sDateString = ""
	
	'do while start month hasn't reached the end date
	do while (datediff("m",dCurMonth, dEndDate) >= 0)
		dCurDate = dCurMonth
		dEndOfCurMonth = (dateadd("m", 1, dCurDate) - 1)

		'loop through month until end is reached or the number of occurrences of a day is reached
		iCurMonthDayOccurrence = 0
		dOccurrenceDate = ""
		do while ((datediff("d",dCurDate,  dEndOfCurMonth) >= 0) and (iCurMonthDayOccurrence < cint(p_iOccurrence)))
			if GetDayOfWeek(dCurDate) = ucase(p_sWeekDayOfOccurrence) then
				iCurMonthDayOccurrence = iCurMonthDayOccurrence + 1
				dOccurrenceDate = dCurDate
			end if			
			dCurDate = dateadd("d", 1, dCurDate)
		loop
		
		'if an occurrence was found and the date hasn't passed yet, add to date string
		if trim(dOccurrenceDate) <> "" and (datediff("d", dStartDate, dOccurrenceDate) >= 0) then
			if (trim(sDateString) = "") then
				sDateString = dOccurrenceDate
			else
				sDateString = sDateString & ", " & dOccurrenceDate
			end if
		end if
		dCurMonth = dateadd("m", p_iMonthCycle, dCurMonth)
	loop
	
	ComputeMonthlyRecurDates = sDateString	
end function
%>
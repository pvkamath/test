<html>

<%
sRecurrenceType = trim(request("RecurrenceType"))

if Ucase(sRecurrenceType) = "WEEKLY" then
	sRecurrenceLink = "RecurrenceWeekly.asp?WeekCycle=" & request("WeekCycle") & "&WeekDays=" & request("WeekDays") & "&EventSpan=" & request("eventSpan")
elseif Ucase(sRecurrenceType) = "MONTHLY" then
	sRecurrenceLink = "RecurrenceMonthly.asp?MonthCycle=" & request("MonthCycle") & "&MonthOccurrence=" & request("MonthOccurrence") & "&MonthWeekDayOfOccurrence=" & request("MonthWeekDayOfOccurrence") & "&EventSpan=" & request("eventSpan")
else
	sRecurrenceLink = "RecurrenceInstructions.asp"	
end if
%>

<script language="javascript">
function ChangeFrame(p_sType)
{
	if (p_sType == "WEEKLY")
	{
		window.frames["bottom"].location.href = "RecurrenceWeekly.asp";
	}
	else if (p_sType == "MONTHLY")
	{
		window.frames["bottom"].location.href = "RecurrenceMonthly.asp";
	}
	else
	{
		window.frames["bottom"].location.href = "RecurrenceInstructions.asp";
	}
	
	
}
</script>

<frameset rows="75,*">
	<frame name="top" src="RecurrenceSelect.asp?RecurrenceType=<%= sRecurrenceType %>"  scrolling=no noresize marginwidth=0 marginheight=0>
	<frame name="bottom" src="<%= sRecurrenceLink %>">
</frameset>
</html>
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->

<%
dim oRs
dim oCompany

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCompany = new Company

oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
'oCompany.UseWithCms = true

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchCompanyName") 
	Session.Contents.Remove("SearchCompanyCity") 
	Session.Contents.Remove("SearchCompanyState") 
	Session.Contents.Remove("CurPage") 
else
	oCompany.Name = ScrubForSQL(request("CompanyName"))
	if (oCompany.Name = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCompanyName")) <> "") then
			oCompany.Name = session("SearchCompanyName")
		end if
	else
		session("SearchCompanyName") = oCompany.Name
	end if
	
	oCompany.City = ScrubForSQL(request("CompanyCity"))
	if (oCompany.City = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCompanyCity")) <> "") then
			oCompany.City = session("SearchCompanyCity")
		end if
	else	
		session("SearchCompanyCity") = oCompany.City
	end if
	
	oCompany.StateId = ScrubForSQL(request("State"))
	if (oCompany.StateId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCompanyState")) <> "") then
			oCompany.StateId = session("SearchCompanyState")
		end if
	else	
		session("SearchCompanyState") = oCompany.StateId
	end if

	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if
	
end if

    oCompany.Active = true
	
	set oRs = oCompany.SearchCompanies()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20

%>

<script language="JavaScript">
function ConfirmDelete(p_iCompanyId,p_sCompanyName)
{
	if (confirm("Are you sure you wish to delete the following Company:\n  " + p_sCompanyName + "\n\nAll information will be deleted."))
		window.location.href = 'deletecompany.asp?CompanyId=' + p_iCompanyId;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Companies</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="CompanyList.asp" method="POST">
<input type="hidden" name="action" value="SEARCH">
<input type="hidden" name="page_number" value="1">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Company Name:</b></td>
		<td><input  type="text" name="CompanyName" value="<%= oCompany.Name %>" maxlength="100"></td>				
	</tr>
	
	<tr>
		<td><b>Company City:</b></td>
		<td><input type="text" name="CompanyCity" value="<% = oCompany.City %>" maxlength="20"></td>
	</tr>
	
	<tr>
		<td><b>Company State:</b></td>
		<td><% call DisplayStatesDropDown(oCompany.StateId,0,"State") 'located in functions.asp %></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>
</table>
</form>

<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Company Name &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Company Logo &nbsp;</td>
		<td class="reportHeaderText">&nbsp; TrainingPro Link &nbsp;</td>
		<!--<td class="reportHeaderText">&nbsp; Branches &nbsp;</td>-->
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>		
	</tr>
<%
	if not (oRs.BOF and oRs.EOF) then
	
		set oCompany = nothing
		set oCompany = new Company
		oCompany.ConnectionString = application("sDataSourceName")
		oCompany.TpConnectionString = application("sTpDataSourceName")
		oCompany.VocalErrors = application("bVocalErrors")
		
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		'Set the beginning record to be displayed on the page
		oRs.AbsolutePage = iCurPage
		
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
			if oCompany.LoadCompanyById(oRs("CompanyID")) <> 0 then
		
				if sClass = "row2" then
					sClass = "row1"
				else
					sClass = "row2"
				end if

				Response.Write("<tr class=""" & sClass & """>" & vbCrLf)

				Response.Write("<td><a href=""modcompany.asp?companyid=" & oCompany.CompanyId & """>" & oCompany.Name & "</a></td>" & vbCrLf)

				if oCompany.LogoFileName <> "" then 
					Response.Write("<td><a href=""modlogo.asp?companyid=" & oCompany.CompanyId & """>Edit</a></td>" & vbCrLf)
				else
					Response.Write("<td><a href=""modlogo.asp?companyid=" & oCompany.CompanyId & """>Add</a></td>" & vbCrLf)
				end if 
				
				if oCompany.TpLinkCompanyId <> "" then
					Response.Write("<td>" & oCompany.LookupTpCompanyName(oCompany.TpLinkCompanyId) & "</td>")
				else
					Response.Write("<td></td>")
				end if
				
				'if oCompany.CompanyId > 0 then 
				'	Response.Write("<td><a href=""modtplink.asp?companyid=" & oCompany.CompanyId & """>")
				'	if oCompany.UseWithCms then
				'		Response.Write("ON")
				'	else
				'		Response.Write("OFF")
				'	end if 
				'	Response.Write("</a></td>" & vbCrLf)
				'else
				'	Response.Write("<td></td>" & vbCrLf)
				'end if
	
				'Response.Write("<td><a href=""branchlist.asp?company=" & oCompany.CompanyId & """>Edit</a></td>" & vbCrLf)

				Response.Write("<td><a href=""javascript:ConfirmDelete(" & oCompany.CompanyId & ",'" & oCompany.Name & "')"">Delete</a></td>" & vbCrLf)

				Response.Write("</tr>" & vbCrLf)

			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
		
		loop		
		
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
		
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td colspan=""4"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""CompanyList.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""CompanyList.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if

		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)	
		response.write("</table>" & vbcrlf)
	
		'display Page number
		response.write("<table width=""100%"">" & vbcrlf)	
		response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)	
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)		
	else
		response.write("<tr><td colspan=""4"">There are currently no Companies that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oCompany = nothing
set oRs = nothing
%>
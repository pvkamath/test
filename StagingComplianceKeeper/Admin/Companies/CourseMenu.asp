<%
option explicit
on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->

<%
dim iCourseID
dim oCourseObj
dim iCourseType
dim rs
dim bShowLessonLink, bShowTestLink, bShowCertificateLink, bShowCourseDatesLink, bShowCrossCertificationLink
dim sLink

iCourseID = ScrubForSQL(request("CourseID"))

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")
oCourseObj.CourseID = iCourseID
set rs = oCourseObj.GetCourse()

iCourseType = cint(rs("TypeID"))

'Determine what links to display
select case cint(iCourseType)
	case 1 'Online
		bShowLessonLink = true
		bShowTestLink = true
		bShowCertificateLink = true
		bShowCrossCertificationLink = true		
		bShowCourseDatesLink = false
	case 2 'HomeStudy
		bShowLessonLink = false
		bShowTestLink = false
		bShowCertificateLink = true	
		bShowCrossCertificationLink = false		
		bShowCourseDatesLink = false		
	case 3 'Live
		bShowLessonLink = false
		bShowTestLink = false
		bShowCertificateLink = true
		bShowCrossCertificationLink = false	
		bShowCourseDatesLink = true
	case else
		response.redirect("/Admin/error.asp?message=Course Type is not set.")
end select
%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= trim(request("message")) %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td><a href="ModCourse.asp?CourseID=<%=iCourseID%>">Edit Course</a></td>
	</tr>
<%
if bShowLessonLink then
	if oCourseObj.HasLessons(iCourseID) then
		sLink = "<a href=""Lessons/LessonList.asp?CourseID=" & iCourseID & """>Edit Lessons</a>"
	else
		sLink = "<a href=""Lessons/ModLesson.asp?CourseID=" & iCourseID & """>Add Lessons</a>"
	end if
%>
	<tr>
		<td><%= sLink %></td>
	</tr>
<%
end if
%>
<%
if bShowCertificateLink then
	if oCourseObj.HasCertificate(iCourseID) then
		sLink = "<a href=""/admin/courses/ModCertificate.asp?CourseID=" & iCourseID & """>Edit Certificate</a>"
	else
		sLink = "<a href=""/admin/courses/ModCertificate.asp?CourseID=" & iCourseID & """>Add Certificate</a>"
	end if
%>
	<tr>
		<td><%= sLink %></td>
	</tr>
<%
end if
%>
<%
if bShowCrossCertificationLink then
	if oCourseObj.HasCrossCertifications(iCourseID,false) then
		sLink = "<a href=""ModCrossCerts.asp?CourseID=" & iCourseID & """>Edit Cross Certifications</a>"
	else
		sLink = "<a href=""ModCrossCerts.asp?CourseID=" & iCourseID & """>Add Cross Certifications</a>"
	end if
%>
	<tr>
		<td><%= sLink %></td>
	</tr>
<%
end if
%>
<%
if bShowCourseDatesLink then
	if oCourseObj.HasCalendarEvents(iCourseID) then
		sLink = "<a target=_parent href=""/Admin/CalendarFX/List.asp?EventType=Live Course"">Edit Course Dates</a>"
	else
		sLink = "<a target=_parent href=""/Admin/CalendarFX/EditEvent.asp?editmode=Add&mode=coming"">Add Course Date</a>"
	end if
%>
	<tr>
		<td><%= sLink %></td>
	</tr>
<%
end if
%>
	<tr>
		<td>
			<a href="CourseList.asp">Return to Course Listing</a>
		</td>
	</tr>
</table>


<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oCourseObj = nothing
%>
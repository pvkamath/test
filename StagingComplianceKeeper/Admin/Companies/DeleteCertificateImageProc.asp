<%
option explicit
on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->

<%
dim sMode
dim oCourseObj
dim iCourseID
dim sImageType
dim bSuccess

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

sMode = trim(request("Mode"))
iCourseID = ScrubForSQL(request("CourseID"))
sImageType = trim(request("ImageType"))

oCourseObj.CourseID = iCourseID
bSuccess = oCourseObj.DeleteCertificateImage(sImageType)

set oCourseObj = nothing

if bSuccess then
	response.redirect("ModCertificateImages.asp?CourseID=" & iCourseID & "&mode=Edit")
else
	response.redirect("/admin/error.asp?message=The Certificate was not Deleted successfully.")
end if
%>
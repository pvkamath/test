<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->

<%
dim oCourseObj
dim iCourseID
dim rs
dim bHasCertificate
dim sMode
dim sCourseName, sState
dim sText
dim sExpirationDate
dim iReleaseType
dim sTextBlock1, sTextBlock2
dim sSignerName, sSignerPosition

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

iCourseID = ScrubForSQL(request("CourseID"))

bHasCertificate = oCourseObj.HasCertificate(iCourseID)

'Set Mode
if bHasCertificate then
	sMode = "Edit"
	
	'retrieve certificate
	oCourseObj.CourseID = iCourseID
	set rs = oCourseObj.GetCertificate()
	
	if not rs.eof then
		sExpirationDate = rs("ExpirationDate")
		iReleaseType = rs("ReleaseType")
		sTextBlock1 = rs("TextBlock1")
		sText = rs("TextBlock2")
		sSignerName = rs("SignerName")
		sSignerPosition = rs("SignerPosition")
	else
		response.redirect("/admin/error.asp?message=The Certificate Information could not be retrieved.")	
	end if
else
	sMode = "Add"
	
	'defaults
	sTextBlock1 = "This will certify that" 
	sSignerName = application("DefaultSignerName")
	sSignerPosition = application("DefaultSignerPosition")
end if

'Retrieve CourseName and State
oCourseObj.CourseID = iCourseID
set rs = oCourseObj.GetCourse()

if not rs.eof then
	sCourseName = rs("Name")
	sState = rs("State")
else
	set rs = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The Course Information could not be retrieved.")	
end if

%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">
{
	function ConfirmDelete()
	{
		if (confirm("Are you sure you wish to delete this Certificate?"))
			window.location.href = 'ModCertificateProc.asp?CourseID=<%= iCourseID %>&mode=Delete';
	}

	function Validate(FORM)
	{
		if (!checkIsDate(FORM.ExpirationDate,"Please enter a valid Expiration Date."))
			return false;		
		
		if (!checkString(FORM.CertificateReleaseType,"Release Type"))
			return false;

		FORM.Text.value = idContent.document.body.innerHTML;

		if (FORM.Text.value == "") 
		{
			alert("Body Text is a required Field!");
			//frm.Text.focus();
			return false;
		}
		
		if (!checkString(FORM.SignerName,"Name of Signer"))
			return false;
		
		if (!checkString(FORM.SignerPosition,"Position of Signer"))
			return false;		
		
		return true;
	}
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%=sMode%>&nbsp;Certificate</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<b><%=sCourseName%></b>
		</td>
	</tr>	
<% if Ucase(sMode) = "EDIT" then %>
	<tr>
		<td>
			<br>
			<a href="PreviewCertificate.asp?CourseID=<%=iCourseID%>" target=_blank>Preview Certificate</a><br>
			<a href="ModCertificateImages.asp?CourseID=<%=iCourseID%>&mode=<%= sMode %>"><%= sMode %> Certificate Images</a>
		</td>
	</tr>
<% end if %>	
</table>
<br>

<form name="frm1" action="ModCertificateProc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="mode" value="<%= sMode %>">
<input type="hidden" name="CourseID" value="<%=iCourseID%>">
<table cellpadding="4" cellspacing="4">
	<tr>		
		<td><b>Expiration Date:</b></td>
		<td><input type="textbox" name="ExpirationDate" value="<%= sExpirationDate%>"  maxlength="15">&nbsp;<a href="javascript:show_calendar('frm1.ExpirationDate');" onmouseover="window.status='Click to choose the Expiration Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 hspace="0" vspace="0" alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frm1.ExpirationDate');" onmouseover="window.status='Click to choose the Post date.';return true;" onmouseout="window.status='';return true;">Select an Expiration Date</a></td>
	</tr>
	<tr>
		<td><b>Release Type:</b></td>
		<td><% call DisplayReleaseTypeDropDown(iReleaseType,1) %></td>
	</tr>	
	<tr>
		<td><b>Body Text:</b></td>
		<td>
			<%= sTextBlock1 %> <u>First Name</u> <u>Last Name</u> [of <u>Company</u>]
			<input type="hidden" name="TextBlock1" value="<%= sTextBlock1 %>">
		</td>
	</tr>	
	<tr>
		<td valign="top"><b>Body Text Cont.:</b></td>
		<td>
		<%
		'if Add, set the default text
		if UCase(sMode) = "ADD" then
			sText= "has taken and completed our program of studies for X hours of " & sCourseName & " the course(s) entitled:" & _
				   "<ol><li>Course 1 - ( X CE Hours)</li><li>Course 2 - ( X CE Hours)</li></ol>" & _
				   "The Provider and above courses are approved by the State of " & sState & " Department of ___________ " & _
				   "and satisfies the continuing education requirements for mortgage professionals under Chapter __, " & sState & " Statutes."
		end if
		%>
			<input type="hidden" name="Text"><!-- #include virtual="/admin/TextFX1.0/CertificateFormat.asp"--> <br>
		</td>		
	</tr>	
	<tr>
		<td><b>Name of Signer:</b></td>
		<td>
			<input type="text" name="SignerName" value="<%= sSignerName %>" maxlength="50">
		</td>
	</tr>	
	<tr>
		<td><b>Position of Signer:</b></td>
		<td>
			<input type="text" name="SignerPosition" value="<%= sSignerPosition %>" maxlength="50">
		</td>
	</tr>			
<% if UCase(sMode) = "ADD" then %>
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="CourseList.asp"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>	
<% elseif UCase(sMode) = "EDIT" then %>
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:onClick=ConfirmDelete()"><img src="/admin/media/images/btn_delete.gif" border=0></a>
		</td>
	</tr>	
	<tr>
		<td align="center" colspan="2">
			<a href="CourseList.asp"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>		
<% end if %>		
</table>
</form>


<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oCourseObj = nothing
set rs = nothing
%>
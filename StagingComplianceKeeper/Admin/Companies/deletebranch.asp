<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------------------------------------>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Delete a Company Branch</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>

<%

dim iCompanyId, iBranchId
iCompanyId = ScrubForSQL(request("CompanyId"))
iBranchId = ScrubForSQL(request("BranchId"))

dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")


'Check for a valid CompanyID.
if iCompanyId <> "" and iBranchId <> "" then

	'We need to have a Company loaded before we can get at the branch.
	if oCompany.LoadId(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("<b>Failed to load the passed CompanyID: " & iCompanyId & "</b>")
		Response.End
		
	end if
	
	if oCompany.Branches.GetSpecificBranch(iBranchId) = 0 then
	
		'No branch.  Fail.
		Response.Write("<b>Failed to load the passed BranchID: " & iBranchId & "</b>")
		Response.End
		
	end if

	if oCompany.Branches.DeleteBranch(iBranchId) = 0 then
		
		'The delete failed.  
		Response.Write("<b>Failed to delete the Company.</b>")
		Response.End
		
	end if

else

	'No CompanyID/BranchID.  Die.
	Response.Write("<b>Incomplete Parameters</b>")
	Response.End	

end if

%>

<b>Successfully deleted the Branch "<% = oCompany.Branches.BranchName %>".</b>

<p>
<a href="BranchList.asp?company=<% = oCompany.CompanyId %>">Return to Branch Listing</a>
<br>

<%
set oCompany = nothing
%>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

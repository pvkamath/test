<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------------------------------------>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Delete a Company</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>

<%

dim iCompanyId
iCompanyId = ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")


'Check for a valid PLPID.
if iCompanyId <> "" then

	'We need to have a PLP loaded before we can delete it.
	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("<b>Failed to load the passed CompanyID: " & iCompanyId & "</b>")
		Response.End
		
	end if

	if oCompany.DeleteCompany() = 0 then
		
		'The delete failed.  
		Response.Write("<b>Failed to delete the Company.</b>")
		Response.End
		
	end if

end if

%>

<b>Successfully deleted the Company "<% = oCompany.Name %>".</b>

<%
set oCompany = nothing
%>

<p>
<a href="CompanyList.asp">Return to Company Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

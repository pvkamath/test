<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------------------------------------>

<%

dim iCompanyId, iBranchId
iCompanyId = ScrubForSQL(request("Company"))
iBranchId = ScrubForSQL(request("BranchId"))


dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
bNew = true
if iCompanyId <> "" then

	if oCompany.LoadId(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.End
		
	end if
	
else

	'We NEED a company ID for obvious reasons.
	Response.Write("Error: Company ID is required.")
	Response.End
	
end if 

if iBranchId <> "" then

	bNew = false

	if oCompany.Branches.GetSpecificBranch(iBranchId) = 0 then
		
		'The load was unsuccessful.  Die.
		Response.Write("Failed to load the passed BranchID: " & iBranchId)
		Response.End
		
	end if

end if


'We need a connectionstring.
oCompany.ConnectionString = application("sDataSourceName")


'If this is a new Branch we need to save it before it will have a unique ID.
dim iNewBranchId
if bNew then

	iNewBranchId = oCompany.Branches.AddUpdateBranch(null, _
		oCompany.CompanyId, _
		ScrubForSQL(request("Name")), _
		ScrubForSQL(request("Password")))

	if iNewBranchId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Branch.  Stop.
		Response.Write("Error: Failed to save new Branch.")
		Response.End

	end if
	
else

	iNewBranchId = oCompany.Branches.AddUpdateBranch( _
		oCompany.Branches.BranchId, _
		oCompany.Branches.BranchCompanyId, _
		ScrubForSQL(request("Name")), _
		ScrubForSQL(request("Password")))

	if iNewBranchId = 0 then

		'The save failed.  Die.
		Response.Write("Error: Failed to save existing Branch.")
		Response.End

	elseif clng(iNewBranchId) <> clng(iBranchId) then
	
		'WTF?  This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Branch object.
		Response.Write("Error: Unexpected results while saving existing Branch.")
		Response.End
		
	end if

end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add a Company Branch</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Company Branch "<% = ScrubForSQL(request("Name")) %>" was successfully created.</b>
<%
else
%>
<b>The Company Branch "<% = ScrubForSQL(request("Name")) %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="BranchList.asp?company=<% = iCompanyId %>">Return to Branch Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
%>
<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------------------------------------>

<%

dim iCompanyId
iCompanyId = ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
dim sMode
bNew = true
sMode = "Add"
if iCompanyId <> "" then

	bNew = false
	sMode = "Edit"
	
	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.End
		
	end if

end if


'Assign the PLP properties based on our passed parameters
oCompany.Name = ScrubForSQL(request("Name"))
oCompany.LegalName = ScrubForSql(request("LegalName"))
if request("Active") = "1" then 
	oCompany.Active = true
else
	oCompany.Active = false
end if 
oCompany.TpLinkCompanyId = ScrubForSql(request("TpCompany"))
oCompany.PricingPlanId = ScrubForSql(request("PricingPlanId"))
oCompany.IncorpStateId = ScrubForSQL(request("IncorpStateId"))
oCompany.IncorpDate = ScrubForSQL(request("IncorpDate"))
'oCompany.FiscalYearEnd = ScrubForSQL(request("FiscalYearEnd"))
oCompany.OrgTypeId = ScrubForSQL(request("OrgTypeId"))
oCompany.Ein = ScrubForSql(request("Ein"))
oCompany.Phone = ScrubForSQL(request("Phone"))
oCompany.PhoneExt = ScrubForSQL(request("PhoneExt"))
oCompany.Phone2 = ScrubForSQL(request("Phone2"))
oCompany.PhoneExt2 = ScrubForSQL(request("PhoneExt2"))
oCompany.Phone3 = ScrubForSQL(request("Phone3"))
oCompany.PhoneExt3 = ScrubForSQL(request("PhoneExt3"))
oCompany.Fax = ScrubForSQL(request("Fax"))
oCompany.Website = ScrubForSQL(request("Website"))
oCompany.Email = ScrubForSql(request("Email"))
oCompany.Address = ScrubForSQL(request("Address"))
oCompany.Address2 = ScrubForSQL(request("Address2"))
oCompany.City = ScrubForSQL(request("City"))
oCompany.StateId = ScrubForSQL(request("StateId"))
oCompany.Zipcode = ScrubForSQL(request("Zipcode"))
oCompany.ZipcodeExt = ScrubForSql(request("ZipcodeExt"))
oCompany.MailingAddress = ScrubForSQL(request("MailingAddress"))
oCompany.MailingAddress2 = ScrubForSQL(request("MailingAddress2"))
oCompany.MailingCity = ScrubForSQL(request("MailingCity"))
oCompany.MailingStateId = ScrubForSQL(request("MailingStateId"))
oCompany.MailingZipcode = ScrubForSQL(request("MailingZipcode"))
oCompany.MainContactName = ScrubForSQL(request("MainContactName"))
oCompany.MainContactPhone = ScrubForSQL(request("MainContactPhone"))
oCompany.MainContactPhoneExt = ScrubForSql(request("MainContactPhoneExt"))
oCompany.MainContactEmail = ScrubForSQL(request("MainContactEmail"))
oCompany.MainContactFax = ScrubForSQL(request("MainContactFax"))


'We need a connectionstring too.
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")


'If this is a new Company we need to save it before it will have a unique ID.
dim iNewCompanyId
if oCompany.CompanyId = "" then

	iNewCompanyId = oCompany.SaveCompany

	if iNewCompanyId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Company.  Stop.
		Response.Write("Error: Failed to save new Company.")
		Response.End

	end if
	
else

	iNewCompanyId = oCompany.SaveCompany

	if iNewCompanyId = 0 then

		'The save failed.  Die.
		Response.Write("Error: Failed to save existing Company.")
		Response.End

	elseif CINT(iNewCompanyId) <> CINT(iCompanyId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		Response.Write("Error: Unexpected results while saving existing Company.")
		Response.End
		
	end if

end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add a Company</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Company "<% = oCompany.Name %>" was successfully created.</b>
<%
else
%>
<b>The Company "<% = oCompany.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="CompanyList.asp">Return to Company Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
%>
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------------------------------------>


<%

dim iCompanyId
iCompanyId = ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

sMode = "Add"
if iCompanyId <> "" then

	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.end
		
	elseif oCompany.LogoFileName <> "" then
	
		sMode = "Edit"
		
	end if
	
else

	Response.Write("Valid Company required.")
	Response.End
	
end if


%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.LogoFile, "File Name")
		   )
			return false;

		return true;
	}
	
	function ChangeLogoImage()
	{	
		document.LogoForm.LogoImgSample.src = document.LogoForm.LogoFile.value
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a Company Logo</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="LogoForm" action="modlogoproc.asp" method="POST" enctype="multipart/Form-Data" onSubmit="return Validate(this)">
<%
'Pass the CompanyID on to the next step so we know we're working with an existing
'Company.
if iCompanyId <> "" then
%>
<input type="hidden" value="<% = iCompanyId %>" name="CompanyId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>Logo File: </b>
		</td>
		<td align="left" valign="top">
			<input type="file" name="LogoFile" onPropertyChange="ChangeLogoImage()">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			&nbsp;
		</td>
		<td align="left" valign="top">
			<%
			if oCompany.LogoFileName <> "" then
			%>
			<img src="/usermedia/images/uploads/AssocLogos/<% = oCompany.LogoFileName %>" name="LogoImgSample" id="LogoImgSample">
			<%
			else
			%>
			<img src="../media/images/clear.gif" name="LogoImgSample" id="LogoImgSample">
			<%
			end if
			%>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif" id=image1 name=image1>&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


<%
set saRS = nothing
set oCompany = nothing
%>
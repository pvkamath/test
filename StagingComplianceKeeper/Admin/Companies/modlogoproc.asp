<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/gfxSpex.asp" ----------------------------------------------------->

<%

'Create the Fileup object that will allow us to upload files
dim oFileUp
set oFileUp = server.CreateObject("softartisans.fileup")

dim iCompanyId
iCompanyId = ScrubForSQL(oFileUp.Form("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
dim sMode
bNew = true
sMode = "Add"
if iCompanyId <> "" then

	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.
		Response.Write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.End

	else
	
		bNew = false
		sMode = "Edit"
		
	end if
	
else

	Response.Write("Failed to load the passed Company.")
	Response.End

end if



'Determine if we got new images that we'll be doing things with.
if oFileUp.Form("LogoFile").UserFileName <> "" then

	oCompany.LogoFileName = "cologo" & oCompany.CompanyId & ScrubForSQL(mid(oFileUp.Form("LogoFile").UserFileName, instrrev(oFileUp.Form("LogoFile").UserFileName, "\") + 1))

else
	
	'We need a file to do anything remotely useful.  Die.
	Response.Write("Error: Failed to retrieve a file.")
	Response.End

end if


'Well define some maximums for our image size here.
dim iMaxHeight, iMaxWidth
iMaxHeight = 500
iMaxWidth = 750

'Pull the File Dimensions.  We use the gfxSpex function from gfxSpex.asp.
dim bGotDimensions
dim iActualWidth, iActualHeight, iColorDepth, sImgType
bGotDimensions = gfxSpex(oFileUp.Form("LogoFile"), iActualWidth, iActualHeight, iColorDepth, sImgType)
if not bGotDimensions then
	'Response.Write("Error: Cannot retrieve image dimensions.<BR>")
	'Response.End
end if 

'The gfxSpex file generates a Permission denied error when it tries
'to get the size of the temp file on the server.  This does not 
'affect our results so we'll just clear it here and move on with
'our lives.
if err.number <> 0 then
	err.Clear
end if

if iActualHeight > iMaxHeight then
	Response.Write("Error: Image height exceeds allowed dimensions.<br>")
	Response.End
end if

if iActualWidth > iMaxWidth then
	Response.Write("Error: Image width exceeds allowed dimensions.<br>")
	Response.End
end if

'Define the paths we'll use to store the files.  We also store this information in the DB.
dim sFlagUploadPath, sFlagExtension
dim sDynFlagPath

sFlagUploadPath = application("sAbsMediaUploadPath") & "\AssocLogos\"
sDynFlagPath = application("sDynMediaUploadPath") & "AssocLogos/"
sFlagExtension = mid(oFileUp.Form("LogoFile").UserFileName, instrrev(oFileUp.Form("LogoFile").UserFileName, ".") + 1)


'Save the file
oFileUp.Form("LogoFile").SaveAs sFlagUploadPath & oCompany.LogoFileName '& "flag." & sFlagExtension
if err <> 0 then
	Response.Write("Error: Save Failed:" & err.Description)
	Response.End
end if



'Save the company data
if oCompany.SaveCompany = 0 then

	'The save failed.  Die.
	Response.Write("Error: Failed to save Company data.")
	Response.End
		
end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a Company Logo</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Logo was successfully created.</b>
<%
else
%>
<b>The Logo was successfully updated.</b>
<%
end if
%>
<p>
<a href="CompanyList.asp">Return to Company Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
%>
<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->

<%

dim iCompanyId
iCompanyId = ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  Stop.
	Response.Write("Failed to load the passed CompanyID: " & iCompanyId)
	Response.End
		
end if


'Assign the PLP properties based on our passed parameters
if oCompany.UseWithCms then
	oCompany.UseWithCms = false
else
	oCompany.UseWithCms = true
end if


if oCompany.SaveCompany = 0 then

	'The save failed.  Die.
	Response.Write("Error: Failed to save existing Company.")
	Response.End
		
end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Activate ComplianceKeeper Link</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if oCompany.UseWithCms then
%>
<b>The ComplianceKeeper link was successfully activated.</b>
<%
else
%>
<b>The ComplianceKeeper link was successfully disabled.</b>
<%
end if
%>
<p>
<a href="CompanyList.asp">Return to Company Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
%>
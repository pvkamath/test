<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->


<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function showArchivedMenus(p_iMenuRow){

		var bVisible = 0;
		//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
		var sEval = "if (ArchivedMenuRow" + p_iMenuRow + ".className == 'RowHidden'){bVisible = 1}"
		//alert(pageRow1.className);
		//alert(sEval);
		eval(sEval);
			if (bVisible == 1){
				eval("ArchivedMenuRow" + p_iMenuRow + ".className = 'RowVisible'");
			}else{
				eval("ArchivedMenuRow" + p_iMenuRow + ".className = 'RowHidden'");
			}
	}
		
		
		
//--></script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><span class="activetitle">Archived Content</span><br>
			<img src="/admin/login/media/images/clear.gif" width="10" height="15" border="0" hspace="0" vspace="0"></td>
	</tr>
</table>

<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
	


call createArchivedMenu("",0)

'###################################################################
'Name:		createArchiveMenu
'Purpose:	To create the side menu for content manager
'			This menu will be dynamic based on the database tables.
'Input :	None
'Output:	Number of items found in the passed menu
'
'###################################################################
sub createArchivedMenu(p_iMenu, p_iLayer)
dim sSQL 'as string
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim i 'as integer
dim sName, sText 'as string 
dim iNavItemID, inavMenuPopout, iNavMenu, iMenuOrder 'as integer
dim sKeyword 'as string
dim sClass 'as string
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean
dim iMenuCount 'as integer
dim sReturn 'as string
dim bShowName 'as boolean

	'check passed data
	if not isNumeric(p_iMenu) then
		p_iMenu = 1
	end if
	if not isNumeric(p_iLayer) then
		p_iLayer = 0
	end if
	
	
	'initialize variables
	iMenuCount = 0
	bShowName = false
		
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	rsMenu.ActiveConnection = oDBConn
 
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3

	'select back then navMenuItem table ordered by navMenu with navMenu
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	
	rsMenu.ActiveConnection = Nothing
	
	if p_iMenu = 1 then
		sClass = "RowVisible"
	else
		sClass = "RowHidden"
	end if	
	
	
	if not rsMenu.EOF then
		'BEGINNING OF EACH MENU ITEM TABLE
		Response.Write("<table cellpadding=""0"" cellspacing=""0""><tr id=""ArchivedMenuRow" & p_iMenu & """ class=""" & sClass & """><td>" & vbCrLF)
		
		do while not rsMenu.EOF
			iNavItemID = rsMenu.Fields("id").Value
			sName = rsMenu.Fields("name").Value
			sText = rsMenu.Fields("text").Value
			iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
			iNavMenu = rsMenu.Fields("navMenu").Value
			iMenuOrder = rsMenu.Fields("menuOrder").Value
			
			'print out folder or page 
			if iNavMenuPopout > 0 then
				
				
				'indent item
				for i = 1 to p_iLayer 
					Response.Write("&nbsp;&nbsp;")
				next				

				Response.Write("<a href=""javascript:showArchivedMenus('" & iNavMenuPopout & "');"">")
				
				Response.Write("	<img src=""/admin/media/images/icon_folder.gif""  border=""0"">&nbsp;")
			
				bShowName = true
			else
				
				sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"
				
				set	rsKeyword.ActiveConnection = oDBConn
				rsKeyword.Open sSQL
				set rsKeyword.ActiveConnection = Nothing
			
				
				'need this code to point the reference to the keyword
				if not rsKeyword.EOF then
					sKeyword = rsKeyword.Fields("keyword").Value
				
					iMenuCount = iMenuCount + 1
					'indent item
					for i = 1 to p_iLayer 
						Response.Write("&nbsp;&nbsp;")
					next				
					Response.Write("<A href=""contentArchivedList.asp?keywords=" & sKeyword & """>")
					Response.Write("<img src=""/admin/media/images/icon_page.gif"" align=""absmiddle"" border=""0"" hspace=""0"" vspace=""0"">&nbsp;&nbsp;")
					bShowName = true
				else
					sKeyword = ""
					bShowName = false
				end if
				
				rsKeyword.Close

				
			end if	
				
			if bShowName then	
				Response.Write( sName )
				Response.Write("</a>")
				Response.Write("<br><img src=""/admin/login/media/images/clear.gif"" width=""10"" height=""4"" border=""0""><br>" & vbCrLF)
			end if
			
			'if menu has popout then query database for menu items
			if iNavMenuPopout > 0 then
				call createArchivedMenu(iNavMenuPopout,p_iLayer + 1)
			end if

			rsMenu.MoveNext
			
		loop
		
		'ENDING OF EVERY MENU ITEM TABLE
		Response.Write("</td></tr></table>" & vbCrLF)

	end if
		
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing

end sub




	
'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
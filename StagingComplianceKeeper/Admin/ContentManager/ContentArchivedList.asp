<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<script language="vbscript" runat="server">
dim oContentAdmin 'as object
dim rsContentList 'as object
dim sHeadline 'as string
dim iContentID 'as integer
dim dPostingDate 'as date
dim sStatus 'as string
dim bListContent 'as boolean
dim sKeyword 'as string
dim i 'as integer
dim bPassedDataIsGood 'as boolean
</script>
<%
	bPassedDataIsGood = true

	sKeywords = getFormElement("keywords")

	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword passed to it<br>")	
		bPassedDataIsGood = false	
	end if

	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")
	
		oContentAdmin.Keywords = sKeywords

		bListContent = oContentAdmin.getContentList("Archived","")
		if not bListContent then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("Error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
	end if
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>

<span class="archivedtitle">Archived Content Listing for <%= sKeywords%></span><br>
<img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br><a href="javascript:window.history.back();" class="back">Back</a>
<form name="frmdelete" method="post" action="deleteContent.asp">
<input type="hidden" name="keywords" value="<%= sKeywords %>">
<input type="hidden" name="status" value="Archived">
<table bgcolor="eaeaea" width="100%" cellpadding="2" cellspacing="2">
<%	
	if bListContent and bPassedDataIsGood then

		set rsContentList = oContentAdmin.ContentListRS
		rsContentList.Open

%>	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; View &nbsp;</td>
		<td class="reportHeaderText">&nbsp; HeadLine &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Posting Date &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Status &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>
	</tr>
<%
	while not rsContentList.EOF
		iContentID = rsContentList.fields("NewsItemID").Value
		dPostingDate = rsContentList.fields("PostingDate").Value
		sHeadline = rsContentList.fields("headline").Value
		sStatus = rsContentList.fields("Status").Value
		
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if
%>
	<tr class="<%= sClass%>">
		<td class="rowText" align="center"><a href="viewContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>"><img src="/admin/media/images/icon_view.gif" border="0"></a></td>
		<td class="rowText" align="center"><%= sHeadLine %></td>
		<td class="rowText" align="center"><%= dPostingDate %></td>
		<td class="rowText" align="center"><%= sStatus %></td>
		<td align="center"><input type="checkbox" name="deleteItem" value="<%= iContentID %>"></td>
	</tr>
<%
			rsContentList.MoveNext
		wend
	rsContentList.Close
	set rsContentList = Nothing
%>
	<tr>
		<td colspan="4">&nbsp;</td>
		<td align="center"><input type="image" name="delete" src="/admin/media/images/bttn_delete.gif"></td>
	</tr>
<%
	
	else
		response.write("<tr><td class=""row2"">" & oContentAdmin.Message & "</td></tr>")
	end if
%>
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	set oContentAdmin = Nothing
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<script language="vbscript" runat="server">
dim oContentAdmin 'as object
dim rsContentList 'as object
dim sHeadline 'as string
dim iContentID 'as integer
dim dPostingDate 'as date
dim sStatus 'as string
dim bListContent 'as boolean
dim sKeyword 'as string
dim i 'as integer
dim bPassedDataIsGood 'as boolean
dim bHasPending 'as boolean
dim iPendingContentID 'as integer
dim oSecurity 'as object
dim bHasAuthorAccessLevel 'as boolean
dim bHasEditAccessLevel 'as boolean
dim bHasAdminAccessLevel 'as boolean
dim iUserID 'as integer
</script>
<%
	bPassedDataIsGood = true

	iUserID = session("User_ID")
	sKeywords = getFormElement("keywords")

	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword passed to it<br>")	
		bPassedDataIsGood = false	
	end if

	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")
	
		oContentAdmin.Keywords = sKeywords

		bListContent = oContentAdmin.getContentList("Active","")
		if not bListContent then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("Error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
	end if
	
	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,5)
	bHasEditAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,10)
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)


	set oSecurity = Nothing
	'###############################################################################
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<table width="100%" bgcolor="FFFFFF" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<span class="activetitle">Content Listing for <%= sKeywords%></span><br>
			<img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
			<form name="frmdelete" method="post" action="deleteContent.asp">
			<input type="hidden" name="keywords" value="<%= sKeywords %>">
			<input type="hidden" name="status" value=""></td>
	</tr>
</table>
<table width="100%" bgcolor="" cellpadding="2" cellspacing="0">
	<tr>
		<td colspan=""><%
					'check if this keyword has content 
					'if the keyword allows multiple then always allow add content
					if oContentAdmin.isMultipleKeyword(sKeywords) then
						if bHasAuthorAccessLevel then 'this access level is set in the security block near the top of the page
			%>
			<a href="changeContent.asp?keywords=<%= sKeywords%>">Add Content</a>
			<%
						end if
						'Check to see if pending content exist  
						if oContentAdmin.hasPendingKeywordContent(sKeywords) then
			%>
			<a href="contentPendingList.asp?keywords=<%= sKeywords%>">Check Pending Content</a>
			<%
						end if
					else
						if not oContentAdmin.hasKeywordContent(sKeywords) then
							if bHasAuthorAccessLevel then 'this access level is set in the security block near the top of the page
			%>
			<a href="changeContent.asp?keywords=<%= sKeywords%>">Add Content</a>
			<%			
							end if
						end if
						'Check to see if pending content exist  
						if oContentAdmin.hasPendingKeywordContent(sKeywords) then
			%>
			<a href="contentPendingList.asp?keywords=<%= sKeywords%>">Check Pending Content</a>
			<%
						end if
					end if
			%></td>
	</tr>
	<%	
		if bListContent and bPassedDataIsGood then
	
			set rsContentList = oContentAdmin.ContentListRS
			rsContentList.Open
	
	%>
</table>
<table width="100%" bgcolor="E6E6E6" cellpadding="2" cellspacing="3">
	<tr>
<%
		if bHasEditAccessLevel then 'this access level is set in the security block near the top of the page
%>
		<td class="reportHeaderColor1">&nbsp; Edit &nbsp;</td>
<%
		end if
%>
		<td class="reportHeaderColor1">&nbsp; View &nbsp;</td>
		<td class="reportHeaderColor1">&nbsp; HeadLine &nbsp;</td>
		<td class="reportHeaderColor1">&nbsp; Posting Date &nbsp;</td>
<%
		if bHasAdminAccessLevel then
%>
		<td class="reportHeaderColor1">&nbsp; Delete &nbsp;</td>
<%
		end if
%>
		<td class="reportHeaderColor1">&nbsp; Has Pending &nbsp;</td>
	</tr>
<%
	while not rsContentList.EOF
		iContentID = rsContentList.fields("NewsItemID").Value
		dPostingDate = rsContentList.fields("PostingDate").Value
		sHeadline = rsContentList.fields("headline").Value
		sStatus = rsContentList.fields("Status").Value
		bhasPending = oContentAdmin.hasPending(iContentID)
		iPendingContentID = oContentAdmin.ContentID

		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if


%>
	<tr class="<%= sClass%>">
<%
		if bHasEditAccessLevel then 'this access level is set in the security block near the top of the page
%>
		<td class="rowText" align="center"><%	if not bhasPending then	%><a href="changeContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>"><img src="/admin/media/images/icon_edit.gif" width="14" height="14" border="0" vspace="0" hspace="0"></a><% end if %></td>
<%
		end if
%>
		<td class="rowText" align="center"><a href="viewContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>"><img src="/admin/media/images/icon_view.gif" border="0"></a></td>
		<td class="rowText" align="center"><%= sHeadLine %></td>
		<td class="rowText" align="center"><%= dPostingDate %></td>
<%
		if bHasAdminAccessLevel then
%>
		<td align="center"><input type="checkbox" name="deleteItem" value="<%= iContentID %>"></td>
<%
		end if
%>
<%
		if bhasPending then		
%>
		<td><a href="contentPendingList.asp?contentid=<%= iPendingContentID %>&keywords=<%= sKeywords %>">Check</a></td>
<%
		else
%>
		<td>&nbsp;</td>
<%
		end if
%>
	</tr>
<%
			rsContentList.MoveNext
		wend
	rsContentList.Close
	set rsContentList = Nothing

		if bHasAdminAccessLevel then
%>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="center"><input type="image" name="delete" src="/admin/media/images/bttn_delete.gif" width="53" height="15"></td>
		<td>&nbsp;</td>
	</tr>
<%
		end if
	else
		response.write("<tr><td class=""row2"">" & oContentAdmin.Message & "</td></tr>")
	end if
%>
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------

	set oContentAdmin = Nothing
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
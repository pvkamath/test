<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<script language="vbscript" runat="server">
dim oContentAdmin 'as object
dim rsContentList 'as object
dim sHeadline 'as string
dim iContentID 'as integer
dim dPostingDate 'as date
dim sStatus 'as string
dim bListContent 'as boolean
dim sKeyword 'as string
dim i 'as integer
dim bPassedDataIsGood 'as boolean
dim sOldContentHeadline 'as String
dim bOldContentExist 'as boolean
dim iRevision 'as integer
dim bHasPending 'as boolean
dim oSecurity 'as object
dim bHasAuthorAccessLevel 'as boolean
dim bHasEditAccessLevel 'as boolean
dim bHasAdminAccessLevel 'as boolean
dim iUserID 'as integer
</script>
<%
	bPassedDataIsGood = true

	sKeywords = getFormElement("keywords")

	iUserID = session("User_ID")

	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword passed to it<br>")	
		bPassedDataIsGood = false	
	end if

	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")
	
		oContentAdmin.Keywords = sKeywords

		bListContent = oContentAdmin.getContentList("Pending","")
		if not bListContent then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("Error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
	end if
	
	
	'###############################################################################
	'security code block
	'iUserID must be set for this to work
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,5)
	bHasEditAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,10)
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)

	set oSecurity = Nothing
	'###############################################################################

	
	
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
%>
<script language="Javascript">
	window.parent.side.location.reload();
</script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><span class="pendingtitle">Pending Content Listing for <%= sKeywords%></span><br>
			<img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br><a href="javascript:window.history.back();" class="back">Back</a><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
			<form name="frmdelete" method="post" action="deleteContent.asp">
			<input type="hidden" name="keywords" value="<%= sKeywords %>">
			<input type="hidden" name="status" value="Pending">
		</td>
	</tr>
</table>
<table width="100%" bgcolor="EAEAEA" cellpadding="2" cellspacing="2">
	<tr>
		<td colspan="6">
<%
		'check if this keyword has content 
		'if the keyword allows multiple then always allow add content
		if oContentAdmin.isMultipleKeyword(sKeywords) then
			if bHasAuthorAccessLevel then 'this access level is set in the security block near the top of the page
%>
			<a href="changeContent.asp?keywords=<%= sKeywords%>">Add Content</a><br>
<%
			end if
		else
			if not oContentAdmin.hasKeywordContent(sKeywords) then
				if bHasAuthorAccessLevel then 'this access level is set in the security block near the top of the page
%>
			<a href="changeContent.asp?keywords=<%= sKeywords%>">Add Content</a><br>
<%			
				end if
			end if
		end if
%>
		</td>
	</tr>
<%	
	if bListContent and bPassedDataIsGood then

		set rsContentList = oContentAdmin.ContentListRS
		rsContentList.Open

%>	<tr class="reportHeaderColor1">
<%
		if bHasEditAccessLevel then 'this access level is set in the security block near the top of the page
%>
		<td class="reportHeaderText">&nbsp; Edit &nbsp;</td>
<%
		end if
%>
		<td class="reportHeaderText">&nbsp; View &nbsp;</td>
		<td class="reportHeaderText">&nbsp; New HeadLine &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Old Headline &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Posting Date &nbsp;</td>
<%
		if bHasAdminAccessLevel or InStr(1, sKeywords, session("User_CompanyId") & "conews") then
%>	
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>
		<td class="reportHeaderText">&nbsp;</td>
<%
		end if
%>
	</tr>
<%
		while not rsContentList.EOF
			iContentID = rsContentList.fields("NewsItemID").Value
			dPostingDate = rsContentList.fields("PostingDate").Value
			sHeadline = rsContentList.fields("headline").Value
			sStatus = rsContentList.fields("Status").Value
			iRevision = trim(rsContentList.fields("Revision").Value)
			bHasPending = oContentAdmin.hasPending(iContentID)
			
			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if
			
			
			bOldContentExist = oContentAdmin.GetContent(iRevision)

			if bOldContentExist then
				oContentAdmin.ContentRS.Open
				sOldContentHeadline = oContentAdmin.ContentRS.Fields("Headline").Value
				oContentAdmin.ContentRS.Close
			else
				sOldContentHeadline = "No Previous Headline"
			end if
%>
	<tr class="<%= sClass%>">
<%
			if bHasEditAccessLevel then 'this access level is set in the security block near the top of the page
%>
		<td align="center">
<%
				if not bHasPending then 
%>
			<a href="changeContent.asp?status=Pending&contentid=<%= iContentID %>&keywords=<%= sKeywords %>"><img src="/admin/media/images/icon_edit.gif" border="0"></a>
<%		
				end if
%>
		</td>
<%
			end if
%>
		<td align="center"><a href="viewContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>"><img src="/admin/media/images/icon_view.gif" border="0"></a></td>
		<td class="rowText" align="center"><%= sHeadLine %></td>
		<td class="rowText" align="center"><%= sOldContentHeadline %></td>
		<td class="rowText" align="center"><%= dPostingDate %></td>
<%
			if bHasAdminAccessLevel then
%>		
		<td align="center"><input type="checkbox" name="deleteItem" value="<%= iContentID %>"></td>
		<td class="rowText" align="center"><a href="viewPendingContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>">&nbsp; Approve Pending &nbsp;</a></td>
<%

			elseif InStr(1, sKeywords, session("User_CompanyId") & "conews") then
%>				
		<td align="center"><input type="checkbox" name="deleteItem" value="<%= iContentID %>"></td>
		<td class="rowText" align="center"><a href="viewPendingContent.asp?contentid=<%= iContentID %>&keywords=<%= sKeywords %>">&nbsp; Approve Pending &nbsp;</a></td>
					
<%			
			end if
%>
	</tr>
<%
			rsContentList.MoveNext
		wend
		rsContentList.Close
		set rsContentList = Nothing

		if bHasAdminAccessLevel then
%>
	<tr>
		<td colspan="5">&nbsp;</td>
		<td align="center"><input type="image" name="delete" src="/admin/media/images/bttn_delete.gif"></td>
		<td>&nbsp;</td>
	</tr>
<%
		end if
	else
		response.write("<tr><td class=""row2"">" & oContentAdmin.Message & "</td></tr>")
	end if
%>
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	set oContentAdmin = Nothing
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<html>
<head>

<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->

<script TYPE="text/javascript"><!--
	function showMenus(p_iMenuRow){

		var bVisible = 0;
		//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
		var sEval = "if (menuRow" + p_iMenuRow + ".className == 'RowHidden'){bVisible = 1}"
		//alert(pageRow1.className);
		//alert(sEval);
		eval(sEval);
			if (bVisible == 1){
				eval("menuRow" + p_iMenuRow + ".className = 'RowVisible'");
			}else{
				eval("menuRow" + p_iMenuRow + ".className = 'RowHidden'");
			}
	}
//-->	
</script>
</head>
<body bgcolor="#FFFFFF">
<table width="150" height="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td rowspan="5"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="5" width="1" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td width="100%"><a target="displayarea" href="InsertKeyword.asp">Add Keywords</a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="6" border="0"></td>
	</tr>
	<tr>
		<td><a target="displayarea" href="ActiveContent.asp" class="status">Active Pages</a></span>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
	</tr>
	<tr>
		<td>
			<%
						call createMenu("", 0) 'function routine can be found at the bottom
			%>
		</td>
	</tr>
	<tr>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
	</tr>
	<tr>
		<!-- dotted line -->
		<td colspan="3" background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="120" height="1" alt="" border="0"></td>
	</tr>
	<tr>
		<td rowspan="0"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br>
			<a target="displayarea" href="PendingCheck.asp" class="status">Pending Edits</a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="0" width="1" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<!-- dotted line -->
		<td colspan="3" background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="120" height="1" alt="" border="0"></td>
	</tr>
	<tr>
		<td rowspan="0"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td valign="top"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br>
			<a target="displayarea" href="ArchivedFolders.asp" class="status">Archived Pages</a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="0" width="1" height="100%" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
</table>
</body>
</html>
<script language="vbscript" runat="server">
'###################################################################
'Name:		createMenu
'Purpose:	To create the side menu for content manager
'			This menu will be dynamic based on the database tables.
'Input :	None
'Output:	Number of items found in the passed menu
'
'###################################################################
sub createMenu(p_iMenu, p_iLayer)
dim sSQL 'as string
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim i 'as integer
dim sName, sText 'as string 
dim iNavItemID, inavMenuPopout, iNavMenu, iMenuOrder 'as integer
dim sKeyword 'as string
dim sClass 'as string
dim oSecurity 'as object
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean
dim iMenuCount 'as integer
dim sReturn 'as string
dim bCheckSecurity 'as boolean
dim bHasAccess 'as boolean

	if not isNumeric(p_iMenu) then
		p_iMenu = 1
	end if
	if not isNumeric(p_iLayer) then
		p_iLayer = 0
	end if

	iMenuCount = 0
	
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	rsMenu.ActiveConnection = oDBConn
 
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3

	'select back then navMenuItem table ordered by navMenu with navMenu
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	
	rsMenu.ActiveConnection = Nothing
	
	if p_iMenu = 1 then
		sClass = "RowVisible"
	else
		sClass = "RowHidden"
	end if	
	
	
	if not rsMenu.EOF then
		'BEGINNING OF EACH MENU ITEM TABLE
		Response.Write("<table cellpadding=""0"" cellspacing=""0""><tr id=""menuRow" & p_iMenu & """ class=""" & sClass & """><td>" & vbCrLF)
		
		do while not rsMenu.EOF
			iNavItemID = rsMenu.Fields("id").Value
			sName = rsMenu.Fields("name").Value
			sText = rsMenu.Fields("text").Value
			iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
			iNavMenu = rsMenu.Fields("navMenu").Value
			iMenuOrder = rsMenu.Fields("menuOrder").Value
			
			bCheckSecurity = checkSecurity(iNavMenuPopout)
			
			
			'print out folder or page 
			if iNavMenuPopout > 0 and bCheckSecurity then
				
				
				'indent item
				for i = 1 to p_iLayer 
					Response.Write("&nbsp;&nbsp;")
				next				

				if bCheckSecurity then
					Response.Write("<a href=""javascript:showMenus('" & iNavMenuPopout & "');"">")
				end if				
				Response.Write("	<img src=""/admin/media/images/icon_folder.gif""  border=""0"">&nbsp;")
			else
				
				sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"
				
				set	rsKeyword.ActiveConnection = oDBConn
				rsKeyword.Open sSQL
				set rsKeyword.ActiveConnection = Nothing
			
				
				'need this code to point the reference to the keyword
				if not rsKeyword.EOF then
					sKeyword = rsKeyword.Fields("keyword").Value
				
					'###############################################################################
					'security code block
					'purpose is to set the boolean so that it can be used throughout the page
					oSecurity.UserID = session("User_id")
	
					bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeyword,5)

					if bHasAuthorAccessLevel then
						bHasAccess = true
					else
						bHasAccess = false
					end if
					'###############################################################################
	
					if bHasAccess then	
						iMenuCount = iMenuCount + 1
						'indent item
						for i = 1 to p_iLayer 
							Response.Write("&nbsp;&nbsp;")
						next				
						Response.Write("<A target=""displayarea"" href=""/admin/contentManager/contentList.asp?keywords=" & sKeyword & """>")
					end if
				else
					sKeyword = ""
				end if
				
				rsKeyword.Close

				if bHasAccess then
					Response.Write("<img src=""/admin/media/images/icon_page.gif"" align=""absmiddle"" border=""0"" hspace=""0"" vspace=""0"">&nbsp;&nbsp;")
				end if
			end if	
				
			'Check that the 
			if (iNavMenuPopout > 0  and bCheckSecurity)  or (bHasAccess) then
				Response.Write( sName )
				
				if bCheckSecurity then
				Response.Write("</a>")
				end if
				Response.Write("<br><img src=""/admin/login/media/images/clear.gif"" width=""10"" height=""4"" border=""0""><br>" & vbCrLF)
				
			end if
			
			
			'if menu has popout then query database for menu items
			if iNavMenuPopout > 0 then
				call createMenu(iNavMenuPopout,p_iLayer + 1)
			end if

			rsMenu.MoveNext
			
		loop
		
		'ENDING OF EVERY MENU ITEM TABLE
		Response.Write("</td></tr></table>" & vbCrLF)

	end if
		
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing

end sub



'###################################################################################
'Action: This function recursively checks for access to anything in the passed menu
'Output: Boolean True if user has access
'###################################################################################
function checkSecurity(p_iMenu)
dim bReturn 'as boolean
dim sSQL 'as string
dim iNavItemID, iNavMenuPopout 'as integer
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim oSecurity 'as object
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean

	bReturn = false

	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	set rsMenu.ActiveConnection = oDBConn
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3
	
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	rsMenu.ActiveConnection = Nothing
	
	do while not rsMenu.EOF
		iNavItemID = rsMenu.Fields("id").Value
		iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
		
		if iNavMenuPopout = 0 then
			'select back the keyword for this menu item
			sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"

			set rsKeyword.ActiveConnection = oDBConn
			
			rsKeyword.Open sSQL			
		
			set rsKeyword.ActiveConnection = Nothing
			if not rsKeyword.EOF then
				sKeyword = rsKeyword.Fields("keyword").Value	
				'###############################################################################
				'security code block
				'check if user has any access to this item
				oSecurity.UserID = session("User_id")
	
				bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeyword,5)
				if bHasAuthorAccessLevel then
					bHasAccess = true
					checkSecurity = bHasAccess
					exit function
				end if
				
				'###############################################################################
			end if
			rsKeyword.Close
		else	
			bReturn = checkSecurity(iNavMenuPopout)	'function found at the bottom of the page
		end if
		
		'exit loop if a they have access to anything
		if bReturn then
			exit do
		end if
		rsMenu.MoveNext
	loop

	checkSecurity = bReturn
	
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing	
end function
</script>
<html>
<head>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/login/includes/style.css">
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
</head>
<body bgcolor="#FFFFFF" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

<!-- code from TopHtml.asp -->

<%
dim iSecurityUserID 'as integer
dim iLocation 'as integer
dim sPagePath 'as string
dim bHasDirectoryAccess 'as boolean

iSecurityUserID = session("User_ID")

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(iSecurityUserID, sPagePath)'security.asp

sCurrentSection = trim(Ucase(request("CurrentSection")))

%>

<table width="165" height="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td rowspan="2"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="2" width="100%"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="2" width="1" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<!-- code from ActiveContent.asp -->
	<script TYPE="text/javascript"><!--
		function showMenus(p_iMenuRow){
	
			var bVisible = 0;
			//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
			var sEval = "if (menuRow" + p_iMenuRow + ".className == 'RowHidden'){bVisible = 1}"
			//alert(pageRow1.className);
			//alert(sEval);
			eval(sEval);
				if (bVisible == 1){
					eval("menuRow" + p_iMenuRow + ".className = 'RowVisible'");
				}else{
					eval("menuRow" + p_iMenuRow + ".className = 'RowHidden'");
				}
		}
	//-->	
	</script>

	<% if sCurrentSection = "ACTIVE" then %>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><a href="framenav.asp" class="cmNav"><nobr>Active Content</nobr></a><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
				</tr>
				<tr>
					<td valign="top" nowrap><% call createMenu("", 0) 'function routine can be found at the bottom %><br></td>
				</tr>
			</table>
		</td>
	</tr>
	<% else %>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><a href="framenav.asp?CurrentSection=ACTIVE" class="cmNav"><nobr>Active Content</nobr></a><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>	
	<% end if %>		
	<tr>
		<!-- dotted line -->
		<td colspan="4" background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="10" height="1" alt="" border="0"></td>
	</tr>	
	<% if sCurrentSection = "PENDING" then %>
	<tr>
		<td rowspan="3"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br>
			<a href="framenav.asp" class="cmNav"><nobr>Pending Edits</nobr></a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="3"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td width="1" rowspan="3" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td>
			<table border="0" bgcolor="" cellpadding="0" cellspacing="0">
				<tr>
					<td>Keyword</td>
					<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
					<td align="right">Count</td>
				</tr>
				<tr>
					<td colspan="3"><img src="/admin/login/media/images/clear.gif" width="10" height="5" border="0"></td>
				</tr>
				<%
					dim oContentAdmin 'as object
					dim iCount 'as integer
					dim bKeywordsExist 'as string
					dim rsKeywords 'as object
					dim sKeyword 'as string
					dim sClass 'as string
					dim iPendingCount 'as integer

					iPendingCount = 0	
					set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp	
					oContentAdmin.ConnectionString = Application("sDataSourceName")	
					bKeywordsExist = oContentAdmin.getKeywords()
					if bKeywordsExist then
						set rsKeywords	= oContentAdmin.KeywordsRS
						rsKeywords.Open
					end if

					do while not rsKeywords.EOF 
						sKeyword = rsKeywords("keyword")
						iCount = oContentAdmin.countPendingContent(sKeyword)
								
						if iCount > 0 then
							iPendingCount = iPendingCount + 1
				
							if sClass = "rowColor2" then
								sClass = "rowColor1"
							else
								sClass = "rowColor2"
							end if
				%>
				<tr>
					<td valign="top" nowrap class="<%= sClass%>"><a href="contentPendingList.asp?keywords=<%= sKeyword %>" target="displayarea"><img src="/admin/media/images/icon_page_pending.gif" align="absmiddle" border="0" hspace="0" vspace="0">&nbsp;&nbsp;<%= sKeyword %></a><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"></td>
					<td>&nbsp;</td>
					<td valign="top" class="<%= sClass%>"><%=	iCount%></td>
				</tr>
				<%			
					end if
						
					rsKeywords.Movenext
				loop
			
			
				if iPendingCount = 0 then
				%>
			
				<tr class="reportHeaderColor1">
					<td class="reportHeaderText" colspan="3">&nbsp; No Pending Articles &nbsp;</td>
				</tr>
				<% end if %>
			</table>
				<% set oContentAdmin = Nothing 	%>
		</td>
	</tr>
	<tr>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="15" border="0"></td>
	</tr>
	<% else %>
	<tr>
		<td rowspan="0"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br>
			<a href="framenav.asp?CurrentSection=PENDING" class="cmNav"><nobr>Pending Edits</nobr></a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
			<td rowspan="0"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="0" width="1" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<% end if %>		
	<tr>
		<!-- dotted line -->
		<td colspan="4" background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="10" height="1" alt="" border="0"></td>
	</tr>	
	<% if sCurrentSection = "ARCHIVE" then %>	
	<tr>
		<td rowspan="2"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td valign="top"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br><a href="FrameNav.asp?" class="cmNav"><nobr>Archived Content</nobr></a><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="2"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td width="1" background="/admin/login/media/images/dots-vertical.gif" rowspan="2"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td valign="top" height="100%" nowrap><% call createArchivedMenu("", 0) 'function routine can be found at the bottom %></td>
	</tr>
	<% else %>
	<tr>
		<td rowspan="2"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td valign="top"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"><br>
			<a href="FrameNav.asp?CurrentSection=ARCHIVE" class="cmNav"><nobr>Archived Content</nobr></a>
			<br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="2"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
		<td rowspan="2" width="1" background="/admin/login/media/images/dots-vertical.gif"><img src="/admin/login/media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td valign="top" height="100%"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0"></td>
	</tr>
	<% end if %>			
	
</table>
</body>
</html>

<!-- code from ActiveContent.asp -->

<%
'###################################################################
'Name:		createMenu
'Purpose:	To create the side menu for content manager
'			This menu will be dynamic based on the database tables.
'Input :	None
'Output:	Number of items found in the passed menu
'
'###################################################################
sub createMenu(p_iMenu, p_iLayer)
dim sSQL 'as string
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim i 'as integer
dim sName, sText 'as string 
dim iNavItemID, inavMenuPopout, iNavMenu, iMenuOrder 'as integer
dim sKeyword 'as string
dim sClass 'as string
dim oSecurity 'as object
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean
dim iMenuCount 'as integer
dim sReturn 'as string
dim bCheckSecurity 'as boolean
dim bHasAccess 'as boolean

	if not isNumeric(p_iMenu) then
		p_iMenu = 1
	end if
	if not isNumeric(p_iLayer) then
		p_iLayer = 0
	end if

	iMenuCount = 0
	
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	rsMenu.ActiveConnection = oDBConn
 
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3

	'select back then navMenuItem table ordered by navMenu with navMenu
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	
	rsMenu.ActiveConnection = Nothing
	
	if p_iMenu = 1 then
		sClass = "RowVisible"
	else
		sClass = "RowHidden"
	end if	
	
	
	if not rsMenu.EOF then
		'BEGINNING OF EACH MENU ITEM TABLE
		Response.Write("<table cellpadding=""0"" cellspacing=""0""><tr id=""menuRow" & p_iMenu & """ class=""" & sClass & """><td nowrap>" & vbCrLF)
		
		do while not rsMenu.EOF
			iNavItemID = rsMenu.Fields("id").Value
			sName = rsMenu.Fields("name").Value
			sText = rsMenu.Fields("text").Value
			iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
			iNavMenu = rsMenu.Fields("navMenu").Value
			iMenuOrder = rsMenu.Fields("menuOrder").Value
			
			bCheckSecurity = checkSecurity(iNavMenuPopout)
			
			'print out folder or page 
			if iNavMenuPopout > 0 and bCheckSecurity then
				
				
				'indent item
				for i = 1 to p_iLayer 
					Response.Write("&nbsp;&nbsp;")
				next				

				if bCheckSecurity then
					Response.Write("<a href=""javascript:showMenus('" & iNavMenuPopout & "');"">")
				end if				
				Response.Write("<img src=""/admin/media/images/icon_folder.gif""  border=""0"">&nbsp;")
			else
				
				sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"
				
				set	rsKeyword.ActiveConnection = oDBConn
				rsKeyword.Open sSQL
				set rsKeyword.ActiveConnection = Nothing
			
				
				'need this code to point the reference to the keyword
				if not rsKeyword.EOF then
					sKeyword = rsKeyword.Fields("keyword").Value
				
					'###############################################################################
					'security code block
					'purpose is to set the boolean so that it can be used throughout the page
					oSecurity.UserID = session("User_id")
	
					bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeyword,5)

					if bHasAuthorAccessLevel then
						bHasAccess = true
					else
						bHasAccess = false
					end if
					'###############################################################################
	
					if bHasAccess then	
						iMenuCount = iMenuCount + 1
						'indent item
						for i = 1 to p_iLayer 
							Response.Write("&nbsp;&nbsp;")
						next				
						Response.Write("<A target=""displayarea"" href=""/admin/contentManager/contentList.asp?keywords=" & sKeyword & """>")

					end if
				else
					sKeyword = ""
				end if
				
				rsKeyword.Close

				if bHasAccess then
					Response.Write("<img src=""/admin/media/images/icon_page.gif"" align=""absmiddle"" border=""0"" hspace=""0"" vspace=""0"">&nbsp;&nbsp;")
				end if
			end if	
				
			'Check that the 
			if (iNavMenuPopout > 0  and bCheckSecurity)  or (bHasAccess) then
				Response.Write( sText )
				
				if bCheckSecurity then
				Response.Write("</a>")
				end if
				Response.Write("<br><img src=""/admin/login/media/images/clear.gif"" width=""10"" height=""4"" border=""0""><br>" & vbCrLF)
				
			end if
			
			
			'if menu has popout then query database for menu items
			if iNavMenuPopout > 0 then
				call createMenu(iNavMenuPopout,p_iLayer + 1)
			end if

			rsMenu.MoveNext
			
		loop
		
		'ENDING OF EVERY MENU ITEM TABLE
		Response.Write("</td></tr></table>" & vbCrLF)

	end if
		
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing

end sub



'###################################################################################
'Action: This function recursively checks for access to anything in the passed menu
'Output: Boolean True if user has access
'###################################################################################
function checkSecurity(p_iMenu)
dim bReturn 'as boolean
dim sSQL 'as string
dim iNavItemID, iNavMenuPopout 'as integer
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim oSecurity 'as object
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean

	bReturn = false

	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	set rsMenu.ActiveConnection = oDBConn
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3
	
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	rsMenu.ActiveConnection = Nothing

	do while not rsMenu.EOF
		iNavItemID = rsMenu.Fields("id").Value
		iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
		
		if iNavMenuPopout = 0 then
			'select back the keyword for this menu item
			sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"

			set rsKeyword.ActiveConnection = oDBConn
			
			rsKeyword.Open sSQL			
		
			set rsKeyword.ActiveConnection = Nothing
			if not rsKeyword.EOF then
				sKeyword = rsKeyword.Fields("keyword").Value	
				'###############################################################################
				'security code block
				'check if user has any access to this item
				oSecurity.UserID = session("User_id")
	
				bHasAuthorAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeyword,5)
				if bHasAuthorAccessLevel then
					bHasAccess = true
					checkSecurity = bHasAccess
					exit function
				else
				end if
				
				'###############################################################################
			end if
			rsKeyword.Close
		else	
			bReturn = checkSecurity(iNavMenuPopout)	'function found at the bottom of the page
		end if
		
		'exit loop if a they have access to anything
		if bReturn then
			exit do
		end if
		rsMenu.MoveNext
	loop

	checkSecurity = bReturn
	
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing	
end function
%>


<!-- code from ArchivedFolders.asp  -->

<%

'call createArchivedMenu("",0)

'###################################################################
'Name:		createArchiveMenu
'Purpose:	To create the side menu for content manager
'			This menu will be dynamic based on the database tables.
'Input :	None
'Output:	Number of items found in the passed menu
'
'###################################################################
sub createArchivedMenu(p_iMenu, p_iLayer)
dim sSQL 'as string
dim oDBConn 'as object
dim rsMenu, rsKeyword 'as object
dim i 'as integer
dim sName, sText 'as string 
dim iNavItemID, inavMenuPopout, iNavMenu, iMenuOrder 'as integer
dim sKeyword 'as string
dim sClass 'as string
dim bHasAuthorAccessLevel ,	bHasEditAccessLevel, bHasAdminAccessLevel 'as boolean
dim iMenuCount 'as integer
dim sReturn 'as string
dim bShowName 'as boolean

	'check passed data
	if not isNumeric(p_iMenu) then
		p_iMenu = 1
	end if
	if not isNumeric(p_iLayer) then
		p_iLayer = 0
	end if
	
	
	'initialize variables
	iMenuCount = 0
	bShowName = false
		
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsMenu = server.CreateObject("ADODB.Recordset")
	set rsKeyword = server.CreateObject("ADODB.Recordset")

	oDBConn.ConnectionString = Application("sDataSourceName")
	oDBConn.Open
	
	rsMenu.ActiveConnection = oDBConn
 
	rsMenu.CursorLocation = 3
	rsKeyword.CursorLocation = 3

	'select back then navMenuItem table ordered by navMenu with navMenu
	sSQL = "SELECT id, name, Text, navMenuPopout, navMenu, menuOrder " & _
			"FROM navMenuItem " & _
			"WHERE (navMenu = " & p_iMenu & ") " & _
			"ORDER BY menuOrder"

	rsMenu.Open sSQL
	
	rsMenu.ActiveConnection = Nothing
	
	if p_iMenu = 1 then
		sClass = "RowVisible"
	else
		sClass = "RowHidden"
	end if	
	
	
	if not rsMenu.EOF then
		'BEGINNING OF EACH MENU ITEM TABLE
		Response.Write("<table border=""0"" cellpadding=""0"" cellspacing=""0""><tr id=""ArchivedMenuRow" & p_iMenu & """ class=""" & sClass & """><td nowrap>" & vbCrLF)
		
		do while not rsMenu.EOF
			iNavItemID = rsMenu.Fields("id").Value
			sName = rsMenu.Fields("name").Value
			sText = rsMenu.Fields("text").Value
			iNavMenuPopout = rsMenu.Fields("navMenuPopout").Value
			iNavMenu = rsMenu.Fields("navMenu").Value
			iMenuOrder = rsMenu.Fields("menuOrder").Value
			
			'print out folder or page 
			if iNavMenuPopout > 0 then
				
				
				'indent item
				for i = 1 to p_iLayer 
					Response.Write("&nbsp;&nbsp;")
				next				

				Response.Write("<a href=""javascript:showArchivedMenus('" & iNavMenuPopout & "');"">")
				
				Response.Write("<img src=""/admin/media/images/icon_folder.gif""  border=""0"">&nbsp;")
			
				bShowName = true
			else
				
				sSQL = "SELECT ak.keyword " & _
						"FROM navMenuItem nmi, afxKeywordsMenuX akmx, afxKeywords ak " & _
						"WHERE nmi.id = " & iNavItemID & " " & _
						"AND akmx.menuSectionID = nmi.id " & _
						"AND ak.Keywordid = akmx.keywordID " & _
						"ORDER BY nmi.menuOrder"
				
				set	rsKeyword.ActiveConnection = oDBConn
				rsKeyword.Open sSQL
				set rsKeyword.ActiveConnection = Nothing
				
				'check whether the keyword ends in "conews" 
				'get the preceding number, which is the company ID
				'if the user's access level is 4, and their company ID matches the
				'keyword company ID, then display the entry.   
				
				'during submission, we need to do the same verification to make
				'sure that the user has a matching company if access is 4.
			
				
				'need this code to point the reference to the keyword
				if not rsKeyword.EOF then
					sKeyword = rsKeyword.Fields("keyword").Value
				
					iMenuCount = iMenuCount + 1
					'indent item
					for i = 1 to p_iLayer 
						Response.Write("&nbsp;&nbsp;")
					next				
					Response.Write("<A href=""contentArchivedList.asp?keywords=" & sKeyword & """ target=""displayarea"">")
					Response.Write("<img src=""/admin/media/images/icon_page_archived.gif"" align=""absmiddle"" border=""0"" hspace=""0"" vspace=""0"">&nbsp;&nbsp;")
					bShowName = true
				else
					sKeyword = ""
					bShowName = false
				end if
				
				rsKeyword.Close

				
			end if	
				
			if bShowName then	
				Response.Write(sText )
				Response.Write("</a>")
				Response.Write("<br><img src=""/admin/login/media/images/clear.gif"" width=""10"" height=""4"" border=""0""><br>" & vbCrLF)
			end if
			
			'if menu has popout then query database for menu items
			if iNavMenuPopout > 0 then
				call createArchivedMenu(iNavMenuPopout,p_iLayer + 1)
			end if

			rsMenu.MoveNext
			
		loop
		
		'ENDING OF EVERY MENU ITEM TABLE
		Response.Write("</td></tr></table>" & vbCrLF)

	end if
		
	rsMenu.Close
	oDBConn.Close
	
	set rsKeyword = Nothing
	set rsMenu = Nothing
	set oDBConn = Nothing
	set oSecurity = Nothing

end sub

%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"

	dim rsChildren, oContentAdmin
	set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
	oContentAdmin.ConnectionString = Application("sDataSourceName")
	set rsChildren = oContentAdmin.GetNavMenuItemChildren(1)

' -------------- END INITIALIZATION CONTENT -------------------------------------------
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ValidateForm(oFrm) {
			if (oFrm.keyword.value == "") {
				alert("Please enter a distinct keyword.");
				oFrm.keyword.focus();
				return false;
			}
			return true;
		}
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>	<form action="InsertKeywordProc.asp" method="post" onSubmit="javascript:return ValidateForm(this);">
	<table>
<%		if request("err") = "1" then %>
		
		<tr>
			<td colspan="2">The keyword you have entered already exists in the database.  Please enter another keyword.</td>
		</tr>
<%		end if %>
		<tr>
			<td>Keyword:</td>
			<td><input type="text" name="keyword" size="50" maxlength="100" value="<%=request("keyword")%>"></td>
		</tr>
		<tr>
			<td>Title:</td>
			<td><input type="text" name="title" size="50" maxlength="100" value="<%=request("title")%>"></td>
		</tr>
		<tr>
			<td>Image:</td>
			<td><input type="text" name="image" size="50" maxlength="100" value="<%=request("image")%>"></td>
		</tr>
		<tr>
			<td>Parent:</td>
			<td><select name="navMenuItem">
					<option value="0">Root</option>
					<% call DisplayNavigation(rsChildren, 1, request("navMenuItem")) 'function routine can be found at the bottom%>				
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="submit" value="Submit"></td>
		</tr>
	</table>
	</form>
<%

set oContentAdmin = Nothing
set rsChildren = Nothing
			
Sub DisplayNavigation(p_rs, p_sLayer, p_nNavMenuItem)

	if p_nNavMenuItem = "" then
		p_nNavMenuItem = 0
	else
		p_nNavMenuItem = cint(p_nNavMenuItem)
	end if
	
	Dim sSpaces, i, rsChildren
	sSpaces = ""
	
	for i = 1 to p_sLayer
		sSpaces = sSpaces & "--"
	next
	
	do while not p_rs.eof
		response.write("<option value=""" & p_rs("id") & """")
		if p_rs("id") = p_nNavMenuItem then
			response.write (" selected")
		end if
		
		response.write(">" & sSpaces & p_rs("name") & "</option>")

		if p_rs("navMenuPopout") <> 0 then
			dim oContentAdmin
			set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
			oContentAdmin.ConnectionString = Application("sDataSourceName")

			set rsChildren = oContentAdmin.GetNavMenuItemChildren(p_rs("navMenuPopout"))
			
			'get subnav
			if not rsChildren.eof then			
				DisplayNavigation rsChildren, p_sLayer + 1, p_nNavMenuItem
			end if
			
			set oContentAdmin = Nothing
			set rsChildren = Nothing
		end if
		p_rs.MoveNext
	loop
End Sub



	'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

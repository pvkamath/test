<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"

	dim oContentAdmin 'as object
	dim sKeyword 'as string
	dim sTitle 'as string
	dim sImage 'as string
	dim nNavMenuItem 'as integer
	
	set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
	oContentAdmin.ConnectionString = Application("sDataSourceName")

	sKeyword = request("keyword")
	sTitle = request("title")
	if sTitle = "" then
		sTitle = sKeyword
	end if
	sImage = request("image")
	nNavMenuItem = cint(request("navMenuItem"))

	if oContentAdmin.IsExistingKeyword(sKeyword) then
		set oContentAdmin = Nothing
		response.redirect("InsertKeyword.asp?keyword=" & Server.UrlEncode(sKeyword) & "&title=" & Server.UrlEncode(sTitle) & "&image=" & Server.UrlEncode(sImage) & "&navMenuItem=" & nNavMenuItem & "&err=1")
	else
		oContentAdmin.InsertKeyword sKeyword, sTitle, sImage, nNavMenuItem
		set oContentAdmin = Nothing
		response.redirect("InsertKeyword.asp")
	end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>	
			
			

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<script language="vbscript" runat="server">
dim oContentAdmin 'as object
dim iCount 'as integer
dim bKeywordsExist 'as string
dim rsKeywords 'as object
dim sKeyword 'as string
dim sClass 'as string
dim iPendingCount 'as integer
</script>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 

	iPendingCount = 0
	
	set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
	
	oContentAdmin.ConnectionString = Application("sDataSourceName")
	
	bKeywordsExist = oContentAdmin.getKeywords()

	if bKeywordsExist then
		set rsKeywords	= oContentAdmin.KeywordsRS
		rsKeywords.Open
	end if

%>
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td><span class="pendingtitle">Pending Edits Listing</span><br>
				<img src="/admin/login/media/images/clear.gif" width="10" height="15" border="0" hspace="0" vspace="0"></td>
		</tr>
	</table>
	<table  bgcolor="e6e6e6">
		<tr class="reportHeaderColor1">
			<td class="reportHeaderText">&nbsp; Keyword &nbsp;</td>
			<td class="reportHeaderText">&nbsp; Count &nbsp;</td>
		</tr>
<%
	do while not rsKeywords.EOF 
		sKeyword = rsKeywords("keyword")
		iCount = oContentAdmin.countPendingContent(sKeyword)
				
		if iCount > 0 then
			iPendingCount = iPendingCount + 1

			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if
%>
		<tr>
			<td nowrap class="<%= sClass%>"><a href="contentPendingList.asp?keywords=<%= sKeyword %>"><%= sKeyword %></a></td>
			<td nowrap class="<%= sClass%>"><%=	iCount%></td>
		</tr>
<%			
		end if
			
		rsKeywords.Movenext
	loop


	if iPendingCount = 0 then
%>

		<tr class="reportHeaderColor1">
			<td class="reportHeaderText" colspan="2">&nbsp; No Pending Articles &nbsp;</td>
		</tr>
<%
	end if
%>
	</table>
<%
		
	set oContentAdmin = Nothing

	'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

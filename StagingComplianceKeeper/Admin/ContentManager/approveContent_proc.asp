<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.asp" --------------------------------------------------->

<script language="vbscript" runat="server">
dim iContentID 'as integer
dim oContentAdmin 'as object
dim rsContent 'as object
dim bContentApproved 'as boolean
dim bPassedDataIsGood 'as boolean
dim sKeywords 'as string	
dim bRetrievedContent 'as boolean
dim sImagePath 'as string
dim sImageName 'as string
dim oFileSystem 'as object
dim sImageFullPath 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasAdminAccessLevel 'as boolean
</script>
<%
	Set oFileSystem = CreateObject("Scripting.FileSystemObject")	

	bPassedDataIsGood = true
	bDataReturned = false
	
	iContentID = getFormElement("contentid")
	sKeywords = getFormElement("keywords")
	iUserID = session("User_ID")

	if not isNumeric(iContentID) then
		Response.Write("This page needs a Content ID passed to it<br>")	
		bPassedDataIsGood = false
	end if
	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword<br>")	
		bPassedDataIsGood = false
	end if
	
	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)
	
	if not bHasAdminAccessLevel then

		if InStr(1, sKeywords, session("User_CompanyId") & "conews") then
		
			bHasAdminAccessLevel = true
		
		else
	
			Response.Write("Please contact administrator.  User needs Admin access to approve pending<br>")
			Response.Write(oSecurity.ErrorDescription)
			Response.End
			
		end if
		
	end if
	
	set oSecurity = Nothing
	'###############################################################################



	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")
			
		bContentApproved = oContentAdmin.approveContent(iContentID)
			
		'Need to retrieve and move the image to the correct folder
		'#########################################################
		bRetrievedContent = oContentAdmin.GetContent(iContentID)
		
		if bRetrievedContent then
			set rsContent = oContentAdmin.ContentRS
			rsContent.Open
		
			if not rsContent.EOF then
				sImagePath = rsContent.fields("mediaPath").Value
				if isNull(sImagePath) then
					sImagePath = ""
				end if
				sImageName = rsContent.fields("mediaName").Value
				if isNull(sImageName) then
					sImageName = ""
				end if
			
				if len(sImageName) > 0 then
					
					sOldImageFullPath = server.MapPath(sImagePath & sCurrentImageName)					
					sNewImageFullPath = Application("sAbsContentImageFolder") & sImageName					

					'change the path to the media path of the approved image 
					if oFileSystem.FileExists(sImageFullPath) then
						
						sImageName = replace(sCurrentImageName ,iOldContentID ,iContentID)

						set fImage = oFileSystem.GetFile(sImageFullPath)
		
						'Move the image to the default content manager folder
						fImage.Move = sNewImageFullPath
					
						'Make the change to the image entry in the database
						oContentAdmin.MediaPath = Application("sAbsContentImageFolder")
						oContentAdmin.MediaName = sImageName

						bChangedImage =  oContentAdmin.updateMedia("I")
					
						'error checking
						if not bChangedImage then
							Response.Write("message " & oContentAdmin.ErrorDescription & "<br>")
							Response.Write("error " & oContentAdmin.Message & "<br>")
						end if					
					
					end if
				end if
			end if
		end if
		'#########################################################
		
		if not bContentApproved  then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
	end if

	set oContentAdmin = Nothing			
	Set oFileSystem = Nothing

	Response.Redirect("contentPendingList.asp?keywords=" & sKeywords)
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.asp" --------------------------------------------------->

<script language="vbscript" runat="server">
dim iContentID 'as integer
dim oContentAdmin 'as object
dim bContentArchived 'as boolean
dim bPassedDataIsGood 'as boolean
dim sKeywords 'as string	
dim sStatus 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasAdminAccessLevel 'as boolean
</script>
<%
	Set oFileSystem = CreateObject("Scripting.FileSystemObject")	

	bContentArchived = false
	bPassedDataIsGood = true
	bDataReturned = false
	
	iUserID = session("User_ID")
	iContentID = getFormElement("contentid")
	sKeywords = getFormElement("keywords")
	sStatus = getFormElement("status")
	
	if not isNumeric(iContentID) then
		Response.Write("This page needs a Content ID passed to it<br>")	
		bPassedDataIsGood = false
	end if
	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword<br>")	
		bPassedDataIsGood = false
	end if
	
	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)
	
	if not bHasAdminAccessLevel then
		Response.Write("Please contact administrator.  User needs Admin access to Archive content<br>")
		Response.Write(oSecurity.ErrorDescription)
		Response.End
	end if
	
	set oSecurity = Nothing
	'###############################################################################
	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")
			
		bContentArchived = oContentAdmin.archiveContent(iContentID)
			
		if not bContentApproved  then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
	end if

	set oContentAdmin = Nothing			

	Response.Redirect("content" & sStatus & "List.asp?keywords=" & sKeywords)
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td>

<script language="vbscript" runat="server">
dim iContentID 'as integer
dim oContentAdmin 'as object
dim rsContent 'as object
dim sBodyText 'as string
dim sHeadline 'as string
dim dPostingDate 'as date
dim bAllowEdit 'as boolean
dim bPassedDataIsGood 'as boolean
dim sKeywords 'as string	
dim sText 'as string
dim hasPending 'as boolean
dim bAdd 'as boolean
dim sStatus 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasEditAccessLevel 'as boolean
</script>
<%
	bPassedDataIsGood = true
	bAllowEdit = false
	bAdd = false
	sImageName = ""
	sImagePath = ""
	
	iUserID = session("User_ID")
	
	iContentID = getFormElement("contentid")
	cID = iContentID
	sKeywords = getFormElement("keywords")
	sStatus = getFormElement("status")
	
	if not isNumeric(iContentID) then
		bAdd = true
	end if
	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword<br>")	
		bPassedDataIsGood = false
	end if
	
	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasEditAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,10)
	
	if not bHasEditAccessLevel then
		Response.Write("Please contact administrator.  User needs edit access<br>")
		Response.Write(oSecurity.ErrorDescription)
		Response.End
	end if
	
	set oSecurity = Nothing
	'###############################################################################


	
	if bPassedDataIsGood and not bAdd then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")

		bHasPending = oContentAdmin.hasPending(iContentID)
		
		if bHasPending then
			bAllowEdit = oContentAdmin.getContent(oContentAdmin.ContentID)
		else
			bAllowEdit = oContentAdmin.getContent(iContentID)
		end if
			
		if not bAllowEdit  then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
			
		if bAllowEdit then
			set rsContent = oContentAdmin.ContentRS
			rsContent.Open
			
			if not rsContent.EOF then
				iContentID = rsContent.fields("NewsItemID").Value
				dPostingDate = rsContent.fields("PostingDate").Value
				sHeadline = rsContent.fields("headline").Value
				sText = replace(rsContent.fields("BodyText").Value,"''","""")
				sImagePath = rsContent.fields("mediaPath").Value
				if isNull(sImagePath) then
					sImagePath = ""
				end if
				sImageName = rsContent.fields("mediaName").Value
				if isNull(sImageName) then
					sImageName = ""
				end if
				bAllowEdit = true
			end if
			
			rsContent.Close
			set rsContent = Nothing
		end if
	end if

%>

<script language="Javascript" src="/admin/includes/cookies.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}

		// Enter Javascript Validation and Functions below: ----------------------


function timeoutRedirect() {
	self.location.href = "../login/timeoutContent.asp?text=" + idContent.document.body.innerHTML +
	         "&status=" + document.forms.frmChangeContent.status.value +
	         "&ContentID=" + ((document.forms.frmChangeContent.ContentID.value) ? document.forms.frmChangeContent.ContentID.value : "0") +
	         "&imageName=" + document.forms.frmChangeContent.imageName.value +
	         "&imagePath=" + document.forms.frmChangeContent.imagePath.value +
	         "&Keywords=" + document.forms.frmChangeContent.Keywords.value +
	         "&headline=" + document.forms.frmChangeContent.headline.value +
	         "&postingdate=" + document.forms.frmChangeContent.postingdate.value;
}

function getLastEntry() {
	var entryText = getCookie("Text");
	if (entryText) {
		idContent.document.body.innerHTML = entryText;
	}
	var headlineText = getCookie("Headline");
	if (headlineText) {
		document.forms.frmChangeContent.headline.value = headlineText;
	}
	var postDate = getCookie("PostingDate");
	if (postDate) {
		document.forms.frmChangeContent.postingdate.value = postDate;
	}
}

<%
if bPassedDataIsGood then
%>
		function ValidateForm(frm) {
			if (frm.headline.value == "") {
				alert("Headline is a required Field!");
				frm.headline.focus();
				return false;
			}
			
			frm.Text.value = idContent.document.body.innerHTML;

			if (frm.Text.value == "") 
			{
				alert("Body Text is a required Field!");
				//frm.Text.focus();
				return false;
			}
			
			if (frm.EditHTML.checked) 
			{
				alert("Please un-check the 'Edit HTML' checkbox");
				frm.EditHTML.focus();
				return false;
			}
			
			//Make sure the Text value is not over 100,000 chars, if gets far over this limit it will cause a stack overflow
			sText = frm.Text.value;
	
			if (sText.length > 100000)
			{
				alert("Section Content has " + sText.length + " chars.\nThe maximum length is 100,000 chars.");
				return false;
			}
		}
<%
end if
%>

function RefreshImageNew(p_sImageName,p_sImagePath){ 
//document.imagepath.src = "/media/images/spacer.gif" 
//document.imagepath.src = "/usermedia/images/uploads/" + p_sImageName 

	document.image.src = p_sImagePath + p_sImageName 

	if (document.image.src == '/admin/login/media/images/spacer.gif')
	{
		document.frmChangeContent.imagePath.value = ''
		document.frmChangeContent.imageName.value = ''
	}
	else
	{
		
		document.frmChangeContent.imagePath.value = p_sImagePath
		document.frmChangeContent.imageName.value = p_sImageName
	}
}

//--></script>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 

	if bPassedDataIsGood then

		if not isdate(dPostingDate) then
			dPostingDate = date
		end if
%>
<form name="frmChangeContent" id="frmChangeContent" method="post" action="changeContent_proc.asp" onSubmit="return ValidateForm(this);">

<input type="hidden" name="status" value="<%= sStatus%>">
<input type="hidden" name="ContentID" value="<%= iContentID%>">
<input type="hidden" name="imageName" value="<%= sImageName %>">
<input type="hidden" name="imagePath" value="<%= sImagePath %>">
<input type="hidden" name="Keywords" value="<%= sKeywords%>">
<span class="activetitle">
<%
		if bAllowEdit then
%>
		Modify 
<%
		else
%>
		Add
<%
		end if
%>
		Content for <%= sKeywords%></span><br>
		<img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
		<a href="javascript:window.history.back();" class="back">Back</a><br><img src="/admin/login/media/images/clear.gif" width="10" height="25" border="0" hspace="0" vspace="0"></td>
	</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr>
		<td colspan="3">&nbsp;</td>
		<td><script TYPE="text/javascript">
				var cookie = getCookie("ContentID");
				//document.write("cookie: " + cookie);
				//document.write("<br>contentid: <% = iContentID %>");
				if ((cookie == "<% = iContentID %>") || ((cookie == "0") && (!"<% = iContentID %>"))) {
					document.write("<a href=\"#\" onClick=\"getLastEntry();\"><img src=\"/admin/login/media/images/bttn-restore.gif\" border=\"0\" hspace=\"0\" vspace=\"0\"></a><br>");
					document.write("<img src=\"/admin/login/media/images/clear.gif\" width=\"10\" height=\"10\" border=\"0\" hspace=\"0\" vspace=\"0\"><br>");
					document.write("When your session has timed out, clicking the <br>above link will retrieve the entry that was lost.<br>");
					document.write("<img src=\"/admin/login/media/images/clear.gif\" width=\"10\" height=\"30\" border=\"0\" hspace=\"0\" vspace=\"0\"><br>");
				} 
			</script></td>
	</tr>
	<tr>
		<td rowspan="5">&nbsp;</td>
		<td align="right" class="labelText">HeadLine</td>
		<td rowspan="5">&nbsp;</td>
		<td><input type="textbox" name="headline" value="<%= sHeadline%>" maxlength="5000">&nbsp;&nbsp;&nbsp;<%if not bAdd then%><a href="archiveContent_proc.asp?keywords=<%= sKeywords%>&contentid=<%= iContentID%>"><img src="/admin/login/media/images/icon-archive.gif" border=0 hspace="0" vspace="0" alt="Archive Content" align="absmiddle"></a><%end if%><%if not bAdd then%>&nbsp;&nbsp;<a href="archiveContent_proc.asp?keywords=<%= sKeywords%>&contentid=<%= iContentID%>">Archive Content</a><%end if%></td>
		
	</tr>
	<tr>
		<td align="right" class="labelText">Posting Date</td>
		<td><input type="textbox" name="postingdate" value="<%= dPostingDate%>"  maxlength="30">&nbsp;<a href="javascript:show_calendar('frmChangeContent.postingdate');" onmouseover="window.status='Click to choose the Post date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 hspace="0" vspace="0" alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frmChangeContent.postingdate');" onmouseover="window.status='Click to choose the Post date.';return true;" onmouseout="window.status='';return true;">Select a Posting Date</a></td>
	</tr>
	<!--
	<tr>
		<td align="right">Image</td>
<% 
		'need to put the default spacer image in if no image exist for this content
		'if len(sImageName) = 0 then
			'sImageName = "spacer.gif"
			'sImagePath = "/admin/media/images/"
		'end if
%>
		<td><img name="image" id="image" src="<%= sImagePath%><%= sImageName %>"><br>
			<input type="image" src="/admin/login/media/images/bttn-upload-image.gif" width="124" height="25" name="Upload Image" value="Upload Image" onClick="OpenImageWindow('<%= iContentID %>','imagepath');">
			&nbsp;
			<input type="image" src="/admin/login/media/images/bttn-remove-image.gif" width="130" height="25" name="Remove Image" value="Remove Image" onClick="RefreshImageNew('spacer.gif','/admin/media/images/')">
		</td>
	</tr>
	-->
	<tr>
		<td align="right" class="labelText" nowrap>Body Text.<br>Enter the body<br>
			of the content here.<br>
		</td>
		<td><input type="hidden" name="Text"><!-- #include virtual="/admin/TextFX1.0/Format.asp"--> <br>
			<img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="image" src="/admin/login/media/images/bttn-submit.gif" value="Submit"><br>
			<img src="/admin/login/media/images/clear.gif" width="10" height="40" border="0" hspace="0" vspace="0"></td>
	</tr>
</table>
<%	
	end if
'-------------------- END PAGE CONTENT ----------------------------------------------------------

'if bAllowEdit and bPassedDataIsGood then
%>
<script language=javascript>
		setTimeout('timeoutRedirect();', <% = (Session.Timeout * 60000) + 30000 %>);
		setTimeout('InsertText();', 1000);
</script>
<%
'end if
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
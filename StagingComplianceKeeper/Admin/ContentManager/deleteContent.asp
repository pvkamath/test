<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.asp" --------------------------------------------------->

<script language="vbscript" runat="server">
dim oContentAdmin 'as object
dim iCount 'as integer
dim bContentDeleted 'as boolean
dim sKeywords 'as string
dim sStatus 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasAdminAccessLevel 'as boolean
</script>
<%
	sKeywords = Request.Form("keywords")
	sStatus = Request.Form("status")

	iUserID = session("User_ID")

	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)
	
	if not bHasAdminAccessLevel then
		Response.Write("Please contact administrator.  User needs Admin access to delete content<br>")
		Response.Write(oSecurity.ErrorDescription)
		Response.End
	end if
	
	set oSecurity = Nothing
	'###############################################################################



	set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
	oContentAdmin.ConnectionString = Application("sDataSourceName")

	iCount = 0
	for each iContentID in Request.Form("deleteItem")

		bContentDeleted = oContentAdmin.deleteContent(iContentID)

		if not bContentDeleted then
			Response.Write("Error " & oContentAdmin.ErrorDescription)
		end if
		
		iCount = iCount + 1
	next

	'Cleanup
	set oContentAdmin = Nothing
	
	if bContentDeleted then
		Response.Redirect("Content" & sStatus & "List.asp?keywords=" & sKeywords)
	end if
%>
<!-- #include virtual = "/admin/includes/misc.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.asp" --------------------------------------------------->

<script language="vbscript" runat="server">
dim iContentID 'as Integer 
dim sHeadline 'as string
dim sBodyText 'as string
dim dPostingDate 'as date
dim oContentAdmin 'as Object
dim rsContent 'as Object
dim sKeywords 'as String
dim bChangedContent 'as Boolean
dim bChangedImage 'as Boolean
dim iSourceID 'as integer
dim iSiteID 'as integer
dim iUserID 'as integer
dim sImageName 'as string
dim sImagePath 'as string
dim sCurrentImageName 'as string
dim oFileSystem 'as object
dim sImageFullPath 'as string
dim fImage 'as file
dim iOldContentID 'as integer
dim bDeletedMedia 'as boolean
dim oSecurity 'as object
dim bHasEditAccessLevel 'as boolean
dim sAbstract 'as string
</script>
<%

bChangedContent = false
bChangedImage = false
iSourceID = 1
iSiteID = 1
iUserID = session("User_ID")

iContentID = getFormElement("ContentID")

sHeadline = getFormElement("headline")
dPostingDate = getFormElement("postingdate")
sBodyText = getFormElement("Text")
sKeywords = getFormElement("keywords")
sStatus = getFormElement("status")
sImageName = getFormElement("ImageName")
sImagePath = getFormElement("ImagePath")
sAbstract = getFormElement("Abstract")'here for future expansion

	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasEditAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,10)
	
	if not bHasEditAccessLevel then
		Response.Write("Please contact administrator.  User needs edit access<br>")
		Response.Write(oSecurity.ErrorDescription)
		Response.End
	end if
	
	set oSecurity = Nothing
	'###############################################################################

	set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
	Set oFileSystem = Server.CreateObject("Scripting.FileSystemObject")	

	oContentAdmin.ConnectionString = Application("sDataSourceName")
	
	oContentAdmin.Headline = sHeadline
	oContentAdmin.PostingDate = dPostingDate
	oContentAdmin.WrittenDate = date
	oContentAdmin.BodyText = sBodyText
	oContentAdmin.Source = iSourceID
	oContentAdmin.Abstract = sAbstract'"Abstract"
	oContentAdmin.SiteID = iSiteID
	oContentAdmin.AuthorID = iUserID
	'need to add keyword

	oContentAdmin.Keywords = sKeywords
	

	if isNumeric(iContentID) then

		iOldContentID = iContentID

		bChangedContent = oContentAdmin.modContent(iContentID)
		
		'Retrieve the new content id
		iContentID = oContentAdmin.ContentID
		
		sBodyText = ProcessBodyText(sBodyText, iContentID, iOldContentID, Application("sDataSourceName"))'function can be found below
		sBodyText = changeAbsolutePath(sBodyText)'function can be found below
		sBodyText = changeAnchor(sBodyText)'function can be found below

		oContentAdmin.BodyText = sBodyText
		bUpdateBodyText = oContentAdmin.updateBodyText()
		
		if bUpdateBodyText <> "true" then
			Response.Write("updateText " & oContentAdmin.ErrorDescription & "<br>")
			Response.End
		end if
		
		'Store current Image Name in a variable
		sCurrentImageName = stripImageQueryString(sImageName)'sImageName
		
		if len(sCurrentImageName) > 0 then

			sImageFullPath = server.MapPath(sImagePath & sCurrentImageName)

			'Rename Current Image to contain the content id
			if oFileSystem.FileExists(sImageFullPath) then
				sImageName = replace(sCurrentImageName ,iOldContentID ,iContentID)

				if sImageName <> sCurrentImageName then
					oFileSystem.CopyFile sImageFullPath, server.MapPath(sImagePath & sImageName)
'					fImage.Name = sImageName
				end if
			
			end if		
		
			oContentAdmin.MediaPath = sImagePath
			oContentAdmin.MediaName = sImageName

			bChangedImage =  oContentAdmin.updateMedia("I")
		
			'error checking
			if not bChangedImage then
				Response.Write("message " & oContentAdmin.ErrorDescription & "<br>")
				Response.Write("error " & oContentAdmin.Message & "<br>")
				Response.End
			end if




		end if
	else
		bChangedContent = oContentAdmin.addContent()

		'Retrieve the new content id
		iContentID = oContentAdmin.ContentID

		sBodyText = ProcessBodyText(sBodyText, iContentID, "", Application("sDataSourceName"))'function can be found below
		sBodyText = changeAbsolutePath(sBodyText)'function can be found below
		sBodyText = changeAnchor(sBodyText)'function can be found below

		oContentAdmin.BodyText = sBodyText
		bUpdateBodyText = oContentAdmin.updateBodyText()

		'Store current Image Name in a variable
		sCurrentImageName = stripImageQueryString(sImageName)'sImageName
		
		if len(sCurrentImageName) > 0 then
			sImageFullPath = server.MapPath(sImagePath & sCurrentImageName)
		
			'Rename Current Image to contain the content id
			if oFileSystem.FileExists(sImageFullPath) then
				set fImage = oFileSystem.GetFile(sImageFullPath)
		
				sImageName = replace(sCurrentImageName,"NEW",iContentID)
		

				fImage.Name = sImageName
			end if
		
			'Need to change the image name to contain content id instead of NEW
			oContentAdmin.MediaName = sImageName
			oContentAdmin.MediaPath = sImagePath

			bChangedImage =  oContentAdmin.updateMedia("I")

			'error checking
			if not bChangedImage then
				Response.Write("this message " & oContentAdmin.ErrorDescription & "<br>")
				Response.Write("error " & oContentAdmin.Message & "<br>")
			end if
		end if
	end if

	if not bChangedContent or not bChangedImage then
		if len(oContentAdmin.ErrorDescription) > 0 then
			Response.Write ("Error " & oContentAdmin.ErrorDescription & "<br>")
		end if
	end if
	
	set oContentAdmin = Nothing
	set oFileSystem = Nothing
	

	if bChangedContent then
		Response.Redirect("Content" & sStatus & "List.asp?keywords=" & sKeywords)
	end if
	
'##################################################################
'Purpose: 
'##################################################################
Function ProcessBodyText(sBodyText,p_iContentID,p_iOldContentID, p_sConnectionString)
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    Dim sFullPath, sImagePath, sImageName
    dim sCurrentImageName 'as string
    dim sStringToReplace 'as string
    dim oFileSytem 'as object
    dim oContentAdmin 'as object
    dim bDeleteResult 'as boolean
    dim bIsNew 'as boolean
    
    'create instance of RegExp object
    Set objRegEx = New RegExp 
	Set oFS = Server.CreateObject("Scripting.FileSystemObject")
    set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp
    
    'Delete all image references from the database for contentid
    oContentAdmin.ConnectionString = p_sConnectionString
    oContentAdmin.ContentID = p_iContentID
    bDeleteResult = oContentAdmin.DeleteMedia(false)

    'find first matches
    objRegEx.Global = True
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
	'objRegEx.Pattern = "/usermedia/images/[^\""]+"
	objRegEx.Pattern = "/usermedia/images/[^\""]+"
	
	'create the collection of matches
    Set Matches = objRegEx.Execute(sBodyText) 
    
    'print out all matches
    For Each Match in Matches
        sFullPath = Match.value
        'Response.write(sFullPath) & "<br>" 
		
		'Get the image 
		sImageName = getImage(sFullPath)
		
		'Get the image path
		sImagePath = getPath(sFullPath)
		
		'if the image name has a new in it then
		if instr(sImageName,"NEW") > 0 then
			sStringToReplace = "NEW"
			bIsNew = true
		else
			sStringToReplace = p_iOldContentID
			bIsNew = false
		end if	

		'Store current Image Name in a variable
		sCurrentImageName = sImageName
		if len(sCurrentImageName) > 0 then
			
			sImageFullPath = server.MapPath(sImagePath & sCurrentImageName)
		
			'Rename Current Image to contain the content id
			if oFS.FileExists(sImageFullPath) then
				'set fImage = oFS.GetFile(sImageFullPath)
		
				sImageName = replace( sCurrentImageName, sStringToReplace, p_iContentID)
				'fImage.Name = sImageName

				'copy file with new file name
				oFileSystem.CopyFile sImageFullPath, server.MapPath(sImagePath & sImageName)

				'delete the old file if it contains new
				if bIsNew then
					oFileSystem.Deletefile sImageFullPath
				end if

				'Replace the text with the new image name 
				sBodyText = replace(sBodyText , sImagePath & sCurrentImageName, sImagePath & sImageName )

			end if
		
			oContentAdmin.ConnectionString = p_sConnectionString
			oContentAdmin.ContentID = p_iContentID
		
			'Need to change the image name to contain content id instead of NEW
			oContentAdmin.MediaName = sImageName
			oContentAdmin.MediaPath = sImagePath

			bChangedImage =  oContentAdmin.updateMedia("T")

			'error checking
			if not bChangedImage then
				Response.Write("message " & oContentAdmin.ErrorDescription & "<br>")
				Response.Write("error " & oContentAdmin.Message & "<br>")
			end if
		end if		
		
    Next

	'Return Body Text 
	ProcessBodyText = sBodyText
	
    Set objRegEx = Nothing
	Set oFS = Nothing
	set oContentAdmin = Nothing
End Function 	


'##################################################################
'Purpose: Return the image name for the passed path
'##################################################################
function getImage(p_sFilePath)
'pull out the image name 
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    'create instance of RegExp object
    Set objRegEx = New RegExp 

    'find first matches
    objRegEx.Global = False
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
'    objRegEx.Pattern = "\""/usermedia/images/(.*)\"""
	'response.write strPhase
	'objRegEx.Pattern = "CMNEW_(.)*?"
	objRegEx.Pattern = "CM.*_.*"
    'create the collection of matches
    Set Matches = objRegEx.Execute(p_sFilePath) 

    'print out all matches
    For Each Match in Matches
        getImage = Match.value
    Next
    
end function


'##################################################################
'Purpose: Returns the image path
'##################################################################
function getPath(p_sFilePath)
'pull out the file path 
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    'create instance of RegExp object
    Set objRegEx = New RegExp 

    'find first matches
    objRegEx.Global = False
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
	objRegEx.Pattern = "^/usermedia/.*[^" & getImage(p_sFilePath) & "]"
    'create the collection of matches
    Set Matches = objRegEx.Execute(p_sFilePath) 

    'print out all matches
    For Each Match in Matches
        'Response.write(Match.value)
        getPath = Match.value
    Next

end function


'##################################################################
'Purpose: Strips out the dynamic query string at the end of the file name 
'##################################################################
function stripImageQueryString(p_sFile)
'pull out the image name 
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    'create instance of RegExp object
    Set objRegEx = New RegExp 

    'find first matches
    objRegEx.Global = False
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
'    objRegEx.Pattern = "\""/usermedia/images/(.*)\"""
	'response.write strPhase
	'objRegEx.Pattern = "CMNEW_(.)*?"
	objRegEx.Pattern = "CM.*_.*[^?1234567890.]"
    'create the collection of matches
    Set Matches = objRegEx.Execute(p_sFile) 

    'print out all matches
    For Each Match in Matches
        stripImageQueryString = Match.value
    Next
    
end function


'##################################################################
'Purpose: Changes the absolute path to a relative path
'##################################################################
Function changeAbsolutePath(sBodyText)
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    dim sStringToReplace 'as string
    dim bDeleteResult 'as boolean
    dim bIsNew 'as boolean
    
    'create instance of RegExp object
    Set objRegEx = New RegExp 
    
    'find first matches
    objRegEx.Global = True
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
	'objRegEx.Pattern = "/usermedia/images/[^\""]+"
	objRegEx.Pattern = "src=\""http://.*?/usermedia/"
	
	'create the collection of matches
    Set Matches = objRegEx.Execute(sBodyText) 
    
    'print out all matches
    For Each Match in Matches
        'sFullPath = Match.value
		'Replace the text with as a relative path 

		sBodyText = replace(sBodyText , Match, "src=""/usermedia/" )
		
    Next

	'Return Body Text 
	changeAbsolutePath = sBodyText


    Set objRegEx = Nothing
End Function 

'##################################################################
'Purpose: Makes sure an anchor is properly handled
'##################################################################
Function changeAnchor(sBodyText)
    'create variables
    Dim objRegEx, Match, Matches, StrReturnStr
    dim sStringToReplace 'as string
    dim bDeleteResult 'as boolean
    dim bIsNew 'as boolean
    
    'create instance of RegExp object
    Set objRegEx = New RegExp 
    
    'find first matches
    objRegEx.Global = True
    'set case insensitive
    objRegEx.IgnoreCase = True
    'set the pattern
	'objRegEx.Pattern = "/usermedia/images/[^\""]+"
	objRegEx.Pattern = "href=\""http://.*?#"
	
	'create the collection of matches
    Set Matches = objRegEx.Execute(sBodyText) 
    
    'print out all matches
    For Each Match in Matches
        'sFullPath = Match.value
		'Replace the text with as a relative path 

		sBodyText = replace(sBodyText , Match, "href=""#" )
		
    Next

	'Return Body Text 
	changeAnchor = sBodyText


    Set objRegEx = Nothing
End Function 
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/directoryCheck.asp" -------------------------------------------->

<script language="vbscript" runat="server">
dim iContentID 'as integer
dim oContentAdmin 'as object
dim rsContent 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
dim bPassedDataIsGood 'as boolean
dim sKeywords 'as string	
dim sText 'as string
dim sHeadline 'as string
dim sImagePath 'as string
dim sImageName 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasAdminAccessLevel 'as boolean
</script>
<%
	bPassedDataIsGood = true
	bDataReturned = false
	
	iContentID = getFormElement("contentid")
	sKeywords = getFormElement("keywords")
	iUserID = session("User_ID")

	if not isNumeric(iContentID) then
		Response.Write("This page needs a Content ID passed to it<br>")	
		bPassedDataIsGood = false
	end if
	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword<br>")	
		bPassedDataIsGood = false
	end if

	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oSecurity = New Security 'Found in include admin\security\security.Class.asp

	oSecurity.ConnectionString = Application("sDataSourceName")

	oSecurity.UserID = iUserID
	bHasAdminAccessLevel = oSecurity.UserHasKeywordNameAccess(sKeywords,15)
	
	set oSecurity = Nothing
	'###############################################################################

	

	
	if bPassedDataIsGood then
		set oContentAdmin = New ContentAdmin 'Found in include contentManager.Class.asp

		oContentAdmin.ConnectionString = Application("sDataSourceName")

		bDataReturned = oContentAdmin.getContent(iContentID)
			
		if not bDataReturned  then
			if len(oContentAdmin.ErrorDescription) > 0 then
				Response.Write ("error " & oContentAdmin.ErrorDescription & "<br>")
			end if
		end if
			
		if bDataReturned then

			set rsContent = oContentAdmin.ContentRS
			rsContent.Open
			
			if not rsContent.EOF then
				iContentID = rsContent.fields("NewsItemID").Value
				sStatus = rsContent.fields("Status").Value
				sHeadline = rsContent.fields("headline").Value
				sText = replace(rsContent.fields("BodyText").Value,"''","""")
				sImagePath = rsContent.fields("mediaPath").Value
				if isNull(sImagePath) then
					sImagePath = ""
				end if
				sImageName = rsContent.fields("mediaName").Value
				if isNull(sImageName) then
					sImageName = ""
				end if				
				
				
				bDataReturned = true
			end if
			
			rsContent.Close
			set rsContent = Nothing

		end if
	end if
	
	select case Ucase(sStatus)
		case "ACTIVE"
			sTitleClass =  "activeTitle"
			sTitle = "View Active Content for " & sKeywords
		case  "ARCHIVED"
			sTitleClass =  "archivedTitle"
			sTitle = "View Archived Content for " & sKeywords
		case "PENDING"
			sTitleClass =  "pendingTitle"
			sTitle = "View Pending Content for " & sKeywords			
	end select
		
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 

	if bDataReturned and bPassedDataIsGood then
		'hide form if user does not have admin access level
		if bHasAdminAccessLevel then
%>
<form name="frmApproveContent" method="post" action="approveContent_proc.asp">
<input type="hidden" name="ContentID" value="<%= iContentID%>">
<input type="hidden" name="Keywords" value="<%= sKeywords%>">
<%
		end if
%>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><span class="<%= sTitleClass %>"><%= sTitle %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
			<a href="javascript:window.history.back();" class="back">Back</a><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"></td>
	</tr>
	<tr>
		<!-- dotted line -->
		<td background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="10" height="1" alt="" border="0"></td>
	</tr>
	<tr>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr>
		<td rowspan="5">&nbsp;</td>
		<td align="right" class="labelText">Status:</td>
		<td rowspan="5">&nbsp;</td>
		<td width="100%" class="elementText"><%= sStatus%></td>
	</tr>
	<tr>
		<td align="right" class="labelText">Headline:</td>
		<td class="elementText"><%= sHeadline%></td>
	</tr>
	<tr>
		<td align="right" class="labelText">Image:</td>
<%		if len(sImageName) > 0 then %>
		<td class="elementText"><img name="image" id="image" src="<%= sImagePath%><%= sImageName %>"></td>
<%		end if %>
	</tr>
	<tr>
		<td align="right" class="labelText" nowrap>Body Text:</td>
		<td class="elementText"><%= sText%></td>
	</tr>
<%

		if ucase(trim(sStatus)) = "PENDING" and bHasAdminAccessLevel then
%>
	
	<tr>
		<td>&nbsp;</td>
		<td colspan="4"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
			<input type="image" src="/admin/login/media/images/bttn-approve.gif" value="Approve" id=submit1 name=submit1></td>
	</tr>
<%
		end if
%>

</table>
<%
		'hide form if user does not have admin access level
		if bHasAdminAccessLevel then
%>
</form>
<%	
		end if
	end if
'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

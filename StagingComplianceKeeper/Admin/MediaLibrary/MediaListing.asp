<!-- #include virtual = "/admin/MediaLibrary/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/MediaLibrary/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/MediaLibrary/includes/shell.asp" --------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Calendar Admin"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<%
Set bc = Server.CreateObject("MSWC.BrowserType") 
strBrowser = bc.browser
set bc = nothing
%>

<script language="javascript1.1" src="includes/FormCheck2.js"></script>
<script TYPE="text/javascript">
		
function Validate(FORM){

	if (!checkString(FORM.fileName,"File")){
		return false;
	}
	return true;
}
		
function DeleteMedia(fileName){
	var bDelete = confirm("Delete " + fileName + "?")
	if (bDelete){
		window.location = "deleteProc.asp?File=" + fileName
	}else{
	}
}

function OpenLinkWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=600,height=100,left=50,top=50')
} 
		
//--></script>

<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/MediaLibrary/includes/Style_ie.css">	
<br>
<table width="100%" height="100%" border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td align="center" valign="top"><span class="tabtitle">Media Library</span>
		
			<form action="UploadProc.asp" method="POST" id="frmUpload" name="frmUpload" enctype="multipart/Form-Data" onSubmit="return Validate(this)">
				<div>Upload New Media:</div>
				<input type=file name="fileName" size="50"><BR><BR>
				<input type=submit value=Submit>
			</form>
			<div><B>Files currently on the server:</B></div>
			<table cellpadding="5" cellspacing="0" border="0" align="center">

		<%
			Dim objFilesys, demofolder, fil, filecoll, filist 
			Dim arrFiles()
			Set objFilesys = CreateObject("Scripting.FileSystemObject") 

			Set objFolder = objFilesys.GetFolder(Application("sAbsWebroot") & "usermedia\MediaLibrary\")  
			Set objFilecoll = objFolder.Files 
			i = 0
			j = objFileColl.Count
			redim arrFiles(j-1)
			For Each fil in objFilecoll    
			    arrFiles(i) = fil.name    
				i = i + 1
			Next  
			
			LineColor = "white"
			
			for i = 0 to uBound(arrFiles)

				if LineColor = "#c0c0c0" then
					LineColor = "#CCCCCC"
				else
					LineColor = "#c0c0c0"
				end if
			%>				
				<tr <%= LineColor%>>
					<td><a target="_blank" href="<%=Application("sDynWebroot")%>usermedia/MediaLibrary/<%=arrFiles(i)%>">	<%=arrFiles(i)%></a>&nbsp;&nbsp;&nbsp;</td>
					<td><a href="javascript:OpenLinkWin('ViewLink.asp?file=<%=arrFiles(i)%>','ViewLink');">View Link</a></td>
					<td><a href="javascript:DeleteMedia('<%=server.urlencode(arrFiles(i))%>')">Delete</a></td>
				</tr>	
		<%		
			
			next 
			
		%>		
		</table>	
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/admin/MediaLibrary/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/MediaLibrary/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/MediaLibrary/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Media Library"
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/MediaLibrary/includes/Style_ie.css">

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<%
Set bc = Server.CreateObject("MSWC.BrowserType") 
strBrowser = bc.browser
set bc = nothing
%>
<br>
<table width="100%" height="100%" border=0 cellpadding=0 cellspacing=0 slign="center">
	<tr>
		<td valign="top" align="center"><span class="tabtitle">Upload Media</span>
			<div><img src="<%= application("sDynWebroot")%>Media/Images/spacer.gif" width="432" height="20"></div>
			<div class=normal>File <B><%=FilePath%></b> uploaded successfully</div><br>
			<div><a href="MediaListing.asp">Return to Media Library</a></div>
		</td>
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%	'---------------------------------------------------------------------------------------------
	Sub PrintShellHeader(p_bShowMenu, p_sSideMenuSelected)
		if p_bShowMenu then

				%><!-- #include virtual="/admin/MediaLibrary/includes/designShellHeader.asp" ---------><%
		end if
		
	End Sub
	
	'---------------------------------------------------------------------------------------------
	Sub PrintShellFooter(p_bShowMenu)
		dim sPagePath
		sPagePath = UCase(Request.ServerVariables("PATH_INFO"))

			%>
			<!-- #include virtual="/admin/MediaLibrary/includes/designShellFooter.asp" --------->
			<%
	End Sub

	'----------------------------------------------------------------------------------------------

	Sub PrintSecondaryNav(p_sFileName)
		p_sFileName = LCase(p_sFileName)
		p_sFileName = Replace(p_sFileName, " ", "_")
		if right(p_sFileName, len(".asp")) = ".asp" then p_sFileName = left(p_sFileName, len(p_sFileName) - len(".asp"))

	End Sub

	'---------------------------------------------------------------------------------------------
	' display links for edit event on the top of page 
	Sub PrintShellFunctionLink()
		dim sImg, sStartTd, sEndTd
		
		response.write "<tr>"
		response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href=EditEvent.asp?editmode=Add&mode=coming class=calendarNav>Add Event</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
		response.write "</tr>"
		response.write "<tr>"
		response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
		response.write "</tr>"
		
		if session("access_level") >= 10 then
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='list.asp?mode=coming' class=calendarNav>List Active Events</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='list.asp?mode=history' class=calendarNav>List Past Events</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
		
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='addlocation.asp' class=calendarNav>Add/Edit Location</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td height=100% valign=top nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='addeventtype.asp' class=calendarNav>Add/Edit Event Type</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			end if
	End Sub
	
	'---------------------------------------------------------------------------------------------
	' display links for edit event on the top of page 
	Sub PrintShellFunctionLinkYearRound()
		dim sImg, sStartTd, sEndTd
		
		response.write "<tr>"
		response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href=EditEvent.asp?editmode=Add&mode=coming class=calendarNav>Add Event</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
		response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
		response.write "</tr>"
		response.write "<tr>"
		response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
		response.write "</tr>"
		
		if session("access_level") >= 10 then
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='list.asp?mode=coming' class=calendarNav>List Active Events</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='list.asp?mode=history' class=calendarNav>List Past Events</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='addlocation.asp' class=calendarNav>Add/Edit Location</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			response.write "<tr>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td nowrap><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0><br><a href='addeventtype.asp' class=calendarNav>Add/Edit Event Type</a><br><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td><img src=/admin/login/media/images/clear.gif width=10 height=10 border=0></td>"
			response.write "<td width=1 background=/admin/login/media/images/dots-vertical.gif><img src=/admin/login/media/images/clear.gif width=1 height=10 alt= border=0></td>"
			response.write "</tr>"
			response.write "<tr>"
			response.write "<td colspan=4 background=/admin/login/media/images/dots-horizontal.gif><img src=/admin/login/media/images/clear.gif width=10 height=1 alt= border=0></td>"
			response.write "</tr>"
			
			end if
	End Sub
	
	'---------------------------------------------------------------------------------------------
	' print the name of section 
	Sub PrintNameOfSection(index) 
		Response.Write "-- AddFX StreetWise"
	End Sub
%>

<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->

<%
dim iSecurityUserID 'as integer
dim iLocation 'as integer
dim sPagePath 'as string
dim bHasDirectoryAccess 'as boolean

iSecurityUserID = session("User_ID")

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(iSecurityUserID, sPagePath)'security.asp
%>
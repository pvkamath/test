//*******************************************************************************
//TEXT FORMATTING CODE														
//  08/01/01 - Sean Stansell												
//	Adds HTML tags around selected text, so that							
//	when text is displayed it will have formatting.							
//	Functions:																
//		MakeLink - Adds <a> tags, prompts user for							
//					address of link (supports http://						
//					and mailto: (easily configurable						
//					for others												
//				-Parameters													
//					-sLinkType (String)										
//						-Type of link (W for Web,							
//						  M for mailto:)									
//		FormatText - Adds configurable tags									
//				-Parameters													
//					-sTag (String)											
//						-Tag to add (Without <>,							
//							i.e.-'p' for <P>)								
//		EnableFormatting - Enables formatting								
//						(lets you allow formatting							
//						 on certain parts of page)							
//		DisableFormatting - Disables formatting								
//*******************************************************************************
	var browser
	var bFormattingEnabled = false //boolean that determines whether the user is in a section of the page that is allowed to be formatted

	function whatBrowser() {	
		var name = navigator.appName
		if (name == "Microsoft Internet Explorer")
			browser = "IE";
		else
			browser = "NOT IE"
	}
	
	function MakeLink(sLinkType){  //sLinkType should be 'W' for a standard Web link or 'M' for a mailto: link
		//this script only works in IE, so check for that
		whatBrowser()
		if (browser == "IE"){
			//Create a TextRange from the selected text	
			var oSelection = document.selection.createRange();
			//Make sure user has selected text and not a button or control
			if(document.selection.type == "Text"){	
				//check the boolean to see if formatting is enabled
				if (bFormattingEnabled==true){
					//Set up link based on link type
					switch(sLinkType){
						case 'W':
							//pop up a prompt for the address to link to
							var URL = prompt("Enter the address of the link", "http://")
							//If the user hit Cancel, stop the function
							if (URL==null)
								return false;
							//Convert the address to uppercase for comparison
							URL = URL.toUpperCase()
							//Check to see if the address begins with http://
							//if not, add it
							if (URL.substr(0,7)!="HTTP://"){
								URL = "HTTP://" + URL
							}
							//add the tags with the address						
							break;
						case 'M':
							//pop up a prompt for the address to link to
							var URL = prompt("Enter the email address of the link", "Type email address here")
							//If the user hit Cancel, stop the function
							if (URL==null)
								return false;								
							//Convert the address to uppercase for comparison
							URL = URL.toUpperCase()
							//Check to see if the address begins with mailto:
							//if not, add it
							if (URL.substr(0,7)!="MAILTO:"){
								URL = "MAILTO:" + URL
							}												
							break;
						default:
							var URL = prompt("Enter the address of the link", "http://")
							//Convert the address to uppercase for comparison
							URL = URL.toUpperCase()
							//Check to see if the address begins with http://
							//if not, add it
							if (URL.substr(0,7)!="HTTP://"){
								URL = "HTTP://" + URL
							}
							//add the tags with the address							
							break;
					}
					var sSelectedText = "<A HREF="+ URL +" target=_blank>" + oSelection.text +"</A>"
					oSelection.text = sSelectedText
				}else{	//IF THE FORMATTING IS DISABLED
					alert("You cannot format this item");
				}				
			}
			else{	//IF THE USER HAS NOT SELECTED ANY TEXT	
				alert("You have not selected any text")
			}
		}
		else{	//IF THE USER IS NOT IN INTERNET EXPLORER
			alert("This is only available in Internet Explorer")
		}
	}	

	function FormatText(sTag){	//sTag is any standard web tag
		//this script only works in IE, so check for that
		whatBrowser()
		if (browser == "IE"){
			//Create a TextRange from the selected text	
			var oSelection = document.selection.createRange();
			//Make sure user has selected text and not a button or control
			if(document.selection.type == "Text"){	
				//check the boolean to see if formatting is enabled			
				if (bFormattingEnabled==true){
					var sSelectedText = "<"+sTag+">" + oSelection.text +"</"+sTag+">"
					oSelection.text = sSelectedText
				}else{	//IF THE FORMATTING IS DISABLED
					alert("You cannot format this item");
				}				
			}
			else{	//IF THE USER HAS NOT SELECTED ANY TEXT	
				alert("You have not selected any text")
			}
		}
		else{	//IF THE USER IS NOT IN INTERNET EXPLORER
			alert("This is only available in Internet Explorer")
		}
	}

	function InsertText(sTag){	//sTag is any standard web tag
		//this script only works in IE, so check for that
		whatBrowser()
		if (browser == "IE"){
			//Create a TextRange from the selected text	
			var oSelection = document.selection.createRange();
			//Make sure user has selected text and not a button or control
			if(document.selection.type == "Text"){	
				//check the boolean to see if formatting is enabled			
				if (bFormattingEnabled==true){
					var sSelectedText = "<"+sTag+">" + oSelection.text
					oSelection.text = sSelectedText
				}else{	//IF THE FORMATTING IS DISABLED
					alert("You cannot format this item");
				}				
			}
			else{	//IF THE USER HAS NOT SELECTED ANY TEXT	
				alert("You have not selected any text")
			}
		}
		else{	//IF THE USER IS NOT IN INTERNET EXPLORER
			alert("This is only available in Internet Explorer")
		}
	}

	function EnableFormatting(){	//changes the bFormattingEnabled boolean to True so that the functions will run
		bFormattingEnabled = true		
	}

	function DisableFormatting(){	//changes the bFormattingEnabled boolean to False so that the functions will not run
		if (window.event.srcElement.name != "BodyText")
			bFormattingEnabled = false;
		//alert(bFormattingEnabled);
	}	
//END TEXT FORMATTING CODE





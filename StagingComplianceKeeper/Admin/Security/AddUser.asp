<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">

<SCRIPT LANGUAGE="JavaScript1.1" SRC="/includes/FormCheck2.js"></SCRIPT>
<SCRIPT>
<!--
	// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
	function InitializePage() {
	//	preloadImages();
	}
	
	function ValidateForm(form)
	{   
		return (
		  checkString(form.elements["firstname"],"First Name") &&
	      checkString(form.elements["lastname"],"Last Name") &&
	      checkString(form.elements["username"],"Username") &&
	      checkString(form.elements["AccessLevel"],"Access Level") 
	   )
	}

	function ValidatePassword() 
	// Expects fields password and confirm_pw in a form called "frm"
	{
		pw1 = document.frm.password.value;
		pw2 = document.frm.confirm_pw.value;
	
		if (document.frm.password.value.length == 0)
		{
			alert("\nPasswords cannot be left blank.  Please enter a password.")
			document.frm.password.focus();
			document.frm.password.select();
			return false;
		}
		
		if (pw1 != pw2) 
		{
			alert ("\nPasswords do not match. Please re-enter your password.")
			document.frm.confirm_pw.focus();
			document.frm.confirm_pw.select();
			return false;
		}
		
		else return true;
	}
//-->	
</script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 

mode = request("mode")


if mode = "A" then
	Lastname = request("LN")
	Firstname = request("FN")
end if
%>

<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">Add a User</font></CENTER>
	</TD>
</TR>
</table>
<table width="500" border="0" align="center">
	<form method=post name="frm" action="AddUserProc.asp" onSubmit="JavaScript: return ValidatePassword()">
	<tr>
		<td align="right"><b>First Name: </b></td>
		<td><input type="text" name="firstname" value="<%=firstname%>" maxlength="75"></td>
	</tr>
	<tr>
		<td align="right"><b>Last Name: </b></td>
		<td><input type="text" name="lastname" value="<%=lastname%>" maxlength="75"></td>
	</tr>
	<tr>
		<td align="right"><b>Username: </b></td>
		<td><input type="text" name="username" maxlength="50"></td>
	</tr>
	<tr>
		<td align="right"><b>Password: </b></td>
		<td><input type="password" name="password" maxlength="50"></td>
	</tr>
	<tr>
		<td align="right"><b>Confirm Password: </b></td>
		<td><input type="password" name="confirm_pw" maxlength="50"></td>
	</tr>		
	<tr>
		<td align="right"><b>Access Level: </b></td>
		<td><select name="AccessLevel">
				<Option value="">Please Select</option>
				<Option value="5">Writer</option>
				<Option value="10">Editor</option>		
				<Option value="15">Administrator</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="submit" name="Submit" value="Submit" onClick="return ValidateForm(this.form)"></td>
	</tr>
	</form>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

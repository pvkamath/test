<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
function HandleChar(SomeValue)
	SomeValue = replace(SomeValue, """", "''")
	HandleChar = replace(SomeValue, "'", "''")
end function

Dim Username, Password, Firstname, Lastname, AccessLevel
dim rs
dim News, User
dim sSQL
dim objConn

set news = server.CreateObject("NewsFxV2.Admin")
set rs = server.CreateObject("ADODB.REcordset")
set objConn = server.CreateObject("ADODB.Connection")
set u = server.CreateObject("UserFX.Users")

objConn.ConnectionString = Application("sDataSourceName")
objConn.Open

u.ConnectionString = Application("sDataSourceName")

Username = request("username")
Username = HandleChar(Username)
Password = request("password")
Password = HandleChar(Password)
Firstname = request("firstname")
Firstname = HandleChar(FirstName)
Lastname = request("lastname")
Lastname = HandleChar(Lastname)
AccessLevel = request("AccessLevel")
AccessLevel = HandleChar(AccessLevel)

'Check to see if form fields are completed.  If not, stop processing
if username = "" or isnull(username) then
	Response.Write("No username given.  Please use the Back button on your browser to go back and add a username")
	Response.End
end if

if lastname = "" or isnull(lastname) then
	Response.Write("No last name given.  Please use the Back button on your browser to go back and add a last name")
	Response.End
end if

if firstname = "" or isnull(firstname) then
	Response.Write("No first name given.  Please use the Back button on your browser to go back and add a first name")
	Response.End
end if

if password = "" or isnull(password) then
	Response.Write("No password given.  Please use the Back button on your browser to go back and add a password")
	Response.End
end if

if AccessLevel = "" or isnull(AccessLevel) then
	Response.Write("No Access Level given.  Please use the Back button on your browser to go back and add a Access Level")
	Response.End
end if


'Check to see if username already exsists
sSQL = "Select * from afxUsersLogin where UserName = '" & Username & "'"
set rs = objConn.Execute(sSQL)
if not rs.EOF then
	Response.Write("Username already exists.  Please use the Back button on your browser to go back and try a different username.")
	Response.End
end if

rs.Close

'Set the Properties
u.ParentID = request("ParentID")
u.HasKids = request("HasKids")
u.CompanyName = request("CompanyName") 
u.LastName = LastName
u.FirstName = FirstName
u.Address1 = request("Address1") 
u.Address2 = request("Address2") 
u.City = request("City") 
u.State = request("State") 
u.Zip = request("Zip") 
u.Phone = request("Phone") 
u.Mobile = request("Mobile") 
u.Fax = request("Fax") 
u.Email = request("Email") 
u.WebSite = request("Website") 
u.Value1 = request("Val1") 
u.Value2 = request("Val2") 
u.Value3 = request("Val3") 
'now() is a VB function that returns the current date and time
u.UserDate = now()
'Makes the User and Active User.  Use 0 to make it InActive
u.Active = 1

'Execute the method and return error information to the IfError variable
'IfError = u.AddUser
		
'if IfError = "Error" then
'	Response.Write("Errors occurred while adding user.")
'	Response.End
'end if
%>
<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">User Added</font></CENTER><BR>
	</TD>
</TR>
</table>
<%
	'insert into afxUsers table - compatibility w/ model db - modified 2/13/2003 jgo
	sSQL = " INSERT INTO afxUsers (FirstName, LastName) " &_
		   " VALUES ('" & firstName & "', '" & lastName & "')"
	objConn.Execute(sSQL)

	'get iUserID
	dim iUserID
	sSQL = " SELECT MAX(userID) AS userID FROM afxUsers "
	rs.Open sSQL, objConn, 3, 1
	
	iUserID = rs.Fields("userID").Value
	
	rs.Close

	sSQL = " Insert into afxUsersLogin(UserID, firstName, lastName ,UserName, Password, AccessLevel) " & _
			" VALUES(" & iUserID & ", '" & firstName & "','" & lastName & "', '" & Username & "', '" & Password & "', '" & _
			AccessLevel & "')"
	
	set rs = objConn.Execute(sSQL)
%>
<table width="500" border="0" align="center">
	<tr>
		<td align="center"><%Response.Write("User " & request("UserName") & " added successfully")%></td>
	</tr>
</table>
<br>
<center><A HREF="ListUsers.asp">Return to User List</A></center>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
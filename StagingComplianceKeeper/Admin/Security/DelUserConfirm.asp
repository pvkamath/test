<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<%

%>
<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">Confirm Delete</font></CENTER><BR>
	</TD>
</TR>
</table>
<%
dim rs
dim U
dim sSQL
dim objConn

set rs = server.CreateObject("ADODB.REcordset")
set objConn = server.CreateObject("ADODB.Connection")
set u = server.CreateObject("UserFX.Users")

objConn.ConnectionString = Application("sDataSourceName")
objConn.Open

u.ConnectionString = Application("sDataSourceName")


	If request("Delete").count = 0 then
%>	
		<table width="500" border="0" align="center">
			<tr>
				<td><%Response.Write "You did not select any users to delete.  <BR>Please press the back button and try again."%></td>
			</tr>
		</table>
<%
		
	Else
%>
<table width="500" border="0" align="center">
<form method=post action="DelUserProc.asp">
	<tr>
		<td align="center"><B><%Response.Write("The following users will be deleted if you press Delete:")%></B></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
<%		
		For i=1 To request("Delete").count
			UserID = request("Delete")(i)
			u.UserID = cint(UserID)
			'sSQL = "Select * from afxUsersInfo UI, afxUsersLogin UL where UI.UserID = UL.UserID and UI.UserID = " & cint(UserID)
			sSQL = "SELECT * FROM afxUsersLogin UL WHERE UserID = " & cint(UserID)
			set rs = objConn.Execute(sSQL)
			%>
	<tr>
		<td align="center"><%=rs("Lastname") & ", " & rs("Firstname")%><input type="hidden" name="Delete" value="<%=UserID%>"></td>
	</tr>
			<%
		Next

%>
	<tr>
		<td align="center"><BR><input type="Submit" value="Delete"><BR></td>
	</tr>
</form>
<form method=post action="ListUsers.asp">
	<tr>
		<td align="center"><input type="Submit" value="Cancel" id=Submit1 name=Submit1><BR></td>
	</tr>
</form>
	
</table>
<%
	end if

%>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
	dim oSecurity 'as object
	dim sError 'as string
	dim sClass 'as string
	dim iDirectoryID 'as integer
	dim sDirectoryName 'as string
	dim rsDirectoy 'as object
	dim rsPage 'as object
	dim iRoleID 'as integer
	dim bIsPageInRole 'as boolean
	dim bIsDirectoryInRole 'as boolean
	dim sChecked 'as string
</script>
<%


' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	bGetDirectories = oSecurity.ListDirectories()
	
	if bGetDirectories then
		set rsDirectories = oSecurity.ReturnedRecordset
	else
		sError = oSecurity.ErrorDescription
		Response.Write sError
		Response.end
	end if

	' Set Page Title:
	sPgeTitle = "List Directories"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		
		function showPages(p_iDirectoryID){
			var bVisible = 0;
			//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
			var sEval = "if (pageRow" + p_iDirectoryID + ".className == 'RowHidden'){bVisible = 1}"
			//alert(pageRow1.className);
			//alert(sEval);
			eval(sEval);
				if (bVisible == 1){
					eval("pageRow" + p_iDirectoryID + ".className = 'RowVisible'");
				}else{
					eval("pageRow" + p_iDirectoryID + ".className = 'RowHidden'");
				}
		}


		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle">Directory Listing</span>
<p><a href="EditDirectory.asp">Add New Directory</a>
	<table bgcolor="DDDDDD" cellpadding=2 cellspacing=2 width=400 style="table-layout: fixed;">
		<tr>
			<td class="ReportHeaderCell" align="center">Edit</td>
			<td class="ReportHeaderCell" align="left">Directory Name</td>
			<td class="ReportHeaderCell" align="left">Directory Path</td>
		</tr>
	<%
		do while not rsDirectories.EOF
			iDirectoryID = rsDirectories.fields("DirectoryID").Value
			sDirectoryName = rsDirectories.fields("DirectoryName").Value
			
			if sClass = "ReportCell1" then
				sClass = "ReportCell2"
			else
				sClass = "ReportCell1"
			end if
	
			oSecurity.RoleID = iRoleID
			oSecurity.DirectoryID = iDirectoryID
			bIsDirectoryInRole = oSecurity.IsDirectoryInRole()
	
	%>
		<tr class="<%= sClass%>">
			<td align="center"><a href="EditDirectory.asp?DirectoryID=<%=iDirectoryID%>"><img src="/admin/media/images/icon_edit.gif" border="0"></a></td>
			<td align="left"><a href="javascript:showPages('<%=iDirectoryID%>');"><img src="/admin/media/images/icon_folder.gif" border="0"></a>&nbsp;<%= sDirectoryName %></td>
			<td align="left"><%=rsDirectories("path")%></td>
		</tr>
		<TR id="pageRow<%=iDirectoryID%>" class="RowHidden">
	<%
				'Get the pages in this directory
				'oSecurity.clearProperties
				
				oSecurity.DirectoryID = iDirectoryID
				bGotPages = oSecurity.GetPagesInDirectory()
				if bGotPages then
					set rsPage = oSecurity.ReturnedRecordset

					Response.Write("<td colspan=""3"">")
					do while not rsPage.eof
						iPageID = rsPage.fields("PageID").Value
						sPageName = rsPage.fields("PageName").Value

						oSecurity.RoleID = iRoleID
						oSecurity.PageID = iPageID
						bIsPageInRole = oSecurity.IsPageInRole()
						
						
						if bIsPageInRole then
							sChecked = "CHECKED"
						else	
							sChecked = ""
						end if
						
						Response.Write("<input type=""checkbox"" name=""addPage" & iDirectoryID & """ value=""" & iPageID & """ " & sChecked & ">&nbsp;&nbsp;&nbsp;&nbsp;<img src=""/admin/media/images/icon_page.gif"" border=""0"">&nbsp;" &  sPageName & "<br>")
						
						rsPage.movenext
					loop
					rsPage.close
					Response.Write("</td>")
					
				else
					Response.Write("<td colspan=""3"">No pages</td>")
				end if	
				
	%>
		</TR>
	<%
			rsDirectories.MoveNext
		loop

		rsDirectories.Close
		set rsDirectories = Nothing
		
		set oSecurity = Nothing
	%>
	</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
dim oSecurity 'as object
dim rs 'as object
dim sGroupName 'as string
dim iGroupID 'as integer
dim sClass 'as string
dim i 'as integer
dim bGotGroups 'as integer
dim sError 'as string
dim bGotUsers 'as boolean
dim rsUsers 'as object
dim sUsers 'as string
dim bGotRoles 'as boolean
dim rsRoles 'as object
dim sRoles 'as string
</script>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

set oSecurity = new Security 'found in security.class.asp
oSecurity.ConnectionString = Application("sDataSourceName")
bGotGroups = oSecurity.GetGroups()
if bGotGroups then
	set rs = oSecurity.ReturnedRecordset
	rs.Open
else
	sError = oSecurity.ErrorDescription
end if


	
	' Set Page Title:
	sPgeTitle = "Group Added"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		
		function showUsers(p_iGroupID){
			var bVisible = 0;
			var sEval = "if (userRow" + p_iGroupID + ".className == 'RowHidden'){bVisible = 1}"
	
			eval(sEval);
				if (bVisible == 1){
					eval("userRow" + p_iGroupID + ".className = 'RowVisible'");
				}else{
					eval("userRow" + p_iGroupID + ".className = 'RowHidden'");
				}
		}	
		
		function showRoles(p_iGroupID){
			var bVisible = 0;
			var sEval = "if (roleRow" + p_iGroupID + ".className == 'RowHidden'){bVisible = 1}"
	
			eval(sEval);
				if (bVisible == 1){
					eval("roleRow" + p_iGroupID + ".className = 'RowVisible'");
				}else{
					eval("roleRow" + p_iGroupID + ".className = 'RowHidden'");
				}
		}	

		
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle">Group List</span>
<table bgcolor="DDDDDD">
	<tr colspan="6">
		<td><a href="editGroup.asp">Add Group</a><br></td>
	</tr>
<tr>
		<td class="ReportHeaderCell">&nbsp;Edit&nbsp;</td>
		<td class="ReportHeaderCell">Group Name</td>
		<td class="ReportHeaderCell">Users</td>
		<td class="ReportHeaderCell">Roles</td>
		<td class="ReportHeaderCell">Delete</td>
	</tr>
<%
	while not rs.EOF
		iGroupID = rs.fields("GroupID").Value
		sGroupName = rs.fields("GroupName").Value

		if sClass = "ReportCell1" then
			sClass = "ReportCell2"
		else
			sClass = "ReportCell1"
		end if
%>
	<tr class="<%= sClass%>" class="RowHidden">
		<td>
			<a href="editGroup.asp?g=<%=iGroupID%>"><img src="/admin/media/images/icon_edit.gif" border="0"></a>
		</td>
		<td><%= sGroupName %></td>
		<td><a href="javascript:showUsers('<%=iGroupID%>');">Users</a></td>
		<td><a href="javascript:showRoles('<%=iGroupID%>');">Roles</a></td>
		<td><input type="checkbox" id=checkbox1 name=checkbox1></td>
	</tr>
<%	
	'Get the users assigned to the the group
	'oSecurity.clearProperties
	oSecurity.GroupID = iGroupID
	bGotUsers = oSecurity.GetUsersInGroup()

	if bGotUsers then
		set rsUsers = oSecurity.ReturnedRecordset
		rsUsers.open
		i = 0
		do while not rsUsers.eof
			if i = 0 then
				sUsers = rsUsers.fields("UserName")
			else
				sUsers = sUsers & ", " & rsUsers.fields("UserName")
			end if
			i = i + 1
			rsUsers.movenext
		loop
		rsUsers.close
	else
		sUsers = "No Users"
	end if		
%>
	<TR id="userRow<%=iGroupID%>"  class="RowHidden">
		<td colspan=5>Users assigned:  <%=sUsers%></td>
		<td><a href="UserGroupAssignments.asp?groupid=<%= iGroupID %>">Edit User assignments</a></td>
	</TR>
<%
	'Get the roles assigned to the the group
	'oSecurity.clearProperties
	oSecurity.GroupID = iGroupID
	bGotRoles = oSecurity.GetRolesInGroup()

	if bGotRoles then
		set rsRoles = oSecurity.ReturnedRecordset
		rsRoles.open
		i = 0
		do while not rsRoles.eof
			if i = 0 then
				sRoles = rsRoles.fields("RoleName")
			else
				sRoles = sRoles & ", " & rsRoles.fields("RoleName")
			end if
			i = i + 1
			rsRoles.movenext
		loop
		rsRoles.close
	else
		sRoles = "No Roles"
	end if		
%>
	<TR id="roleRow<%=iGroupID%>"  class="RowHidden">
		<td colspan=5>Roles assigned:  <%=sRoles%></td>
		<td><a href="RoleGroupAssignments.asp?groupid=<%= iGroupID %>">Edit Role assignments</a></td>
	</TR>
<%



			rs.MoveNext
		wend

	rs.Close
	set rs = Nothing


%>
</table>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

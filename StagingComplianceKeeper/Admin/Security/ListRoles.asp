<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
	dim oSecurity 'as object
	dim rs 'as object
	dim sRoleName 'as string
	dim iRoleID 'as integer
	dim sClass 'as string
	dim i 'as integer
	dim bGotRoles 'as integer
	dim sError 'as string
	dim bGotKeywords 'as boolean
	dim rsKeywords 'as object
	dim sKeywords 'as string
</script>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

set oSecurity = new Security 'found in security.class.asp
oSecurity.ConnectionString = Application("sDataSourceName")
bGotRoles = oSecurity.GetRoles()
if bGotRoles then
	set rs = oSecurity.ReturnedRecordset
	rs.cursorLocation = 3
	rs.Open
else
	sError = oSecurity.ErrorDescription
	Response.Write sError
	Response.end
end if

	' Set Page Title:
	sPgeTitle = "Role Listing"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		
		function showPages(p_iRoleID){
			var bVisible = 0;
			//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
			var sEval = "if (pageRow" + p_iRoleID + ".className == 'RowHidden'){bVisible = 1}"
			//alert(pageRow1.className);
			//alert(sEval);
			eval(sEval);
				if (bVisible == 1){
					eval("pageRow" + p_iRoleID + ".className = 'RowVisible'");
				}else{
					eval("pageRow" + p_iRoleID + ".className = 'RowHidden'");
				}
		}


		function showDirectories(p_iRoleID){
			var bVisible = 0;
			//var sEval = "if (pageRow" + p_iRoleID + ".className = 'RowVisible') {pageRow" + p_iRoleID + ".className = 'RowHidden'}else{pageRow" + p_iRoleID + ".className = 'RowVisible'}"
			var sEval = "if (directoryRow" + p_iRoleID + ".className == 'RowHidden'){bVisible = 1}"
			//alert(pageRow1.className);
			//alert(sEval);
			eval(sEval);
				if (bVisible == 1){
					eval("directoryRow" + p_iRoleID + ".className = 'RowVisible'");
				}else{
					eval("directoryRow" + p_iRoleID + ".className = 'RowHidden'");
				}
		}
		
		function showKeywords(p_iRoleID){
			var bVisible = 0;
			var sEval = "if (keywordRow" + p_iRoleID + ".className == 'RowHidden'){bVisible = 1}"
	
			eval(sEval);
				if (bVisible == 1){
					eval("keywordRow" + p_iRoleID + ".className = 'RowVisible'");
				}else{
					eval("keywordRow" + p_iRoleID + ".className = 'RowHidden'");
				}
		}		
		
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle">Role List</span>
<table bgcolor="DDDDDD" cellpadding=2 cellspacing=2 width=600 style="table-layout: fixed;">
	<tr colspan="6">
		<td><a href="editRole.asp">Add Role</a><br></td>
	</tr>
<tr>
		<td class="ReportHeaderCell" width=25>&nbsp;Edit&nbsp;</td>
		<td class="ReportHeaderCell" width=100>Role Name</td>
		<td class="ReportHeaderCell" width=50>Access Level</td>
		<td class="ReportHeaderCell" width=50>Pages</td>	
		<td class="ReportHeaderCell" width=50>Folders</td>
		<td class="ReportHeaderCell" width=50>Sections</td>			
		<td class="ReportHeaderCell" width=50>Delete</td>
	</tr>
<%
	do while not rs.EOF
		iRoleID = rs.fields("RoleID").Value
		sRoleName = rs.fields("RoleName").Value
		iAccessLevel = rs.fields("AccessLevel").Value

		if sClass = "ReportCell1" then
			sClass = "ReportCell2"
		else
			sClass = "ReportCell1"
		end if
%>
	<tr class="<%= sClass%>">
		<td>
			<a href="editRole.asp?roleID=<%=iRoleID%>"><img src="/admin/media/images/icon_edit.gif" border="0"></a>
		</td>
		<td><%= sRoleName %></td>
		<td><%= iAccessLevel %></td>	
		<td><a href="javascript:showPages('<%=iRoleID%>');"><img src="/admin/security/media/images/icon_page.gif" border="0"></a></td>	
		<td><a href="javascript:showDirectories('<%=iRoleID%>');"><img src="/admin/security/media/images/icon_folder.gif" border="0"></a></td>	
		<td><a href="javascript:showKeywords('<%=iRoleID%>');"><img src="/admin/security/media/images/icon_keyword.gif" border="0"></a></td>	
		<td><input type="checkbox" id=checkbox1 name=checkbox1></td>
	</tr>
<%
			'Get the pages assigned to the group
			'oSecurity.clearProperties
			oSecurity.RoleID = iRoleID
			bGotPages = oSecurity.GetPagesInRole
			if bGotPages then
				set rsPage = oSecurity.ReturnedRecordset
				rsPage.open
				i = 0
				do while not rsPage.eof
					if i = 0 then
						sPages = rsPage.fields("PageName")
					else
						sPages = sPages & ", " & rsPage.fields("PageName")
					end if
					i = i + 1
					rsPage.movenext
				loop
				rsPage.close
			else
				sPages = "No pages"
			end if	
%>
	<TR id="pageRow<%=iRoleID%>" class="RowHidden">
		<td colspan=5>Pages assigned:  <%=sPages%></td>
		<td>&nbsp;</td>
		<td><a href="RoleAssignments.asp?roleid=<%= iRoleID %>">Edit assignments</a></td>
	</TR>
<%
			'Get the directories assigned to the group
			'oSecurity.clearProperties
			oSecurity.RoleID = iRoleID
			bGotDirectories = oSecurity.GetDirectoriesInRole
			if bGotDirectories then
				set rsDirectory = oSecurity.ReturnedRecordset
				rsDirectory.open
				i = 0
				do while not rsDirectory.eof
					if i = 0 then
						sDirectories = rsDirectory.fields("DirectoryName")
					else
						sDirectories = sDirectories & ", " & rsDirectory.fields("DirectoryName")
					end if
					i = i + 1
					rsDirectory.movenext
				loop
				rsDirectory.close
			else
				sDirectories = "No Directories"
			end if	
%>
	<TR id="directoryRow<%=iRoleID%>" class="RowHidden">

		<td colspan=5>Directories assigned:  <%=sDirectories%></td>
		<td>&nbsp;</td>
		<td><a href="RoleAssignments.asp?roleid=<%= iRoleID %>">Edit assignments</a></td>
	</TR>
<%
	'Get the keywords assigned to the the role
	'oSecurity.clearProperties
	oSecurity.RoleID = iRoleID
	bGotKeywords = oSecurity.GetKeywordsInRole()


	if bGotKeywords then
		set rsKeywords = oSecurity.ReturnedRecordset
		rsKeywords.open
		i = 0
		do while not rsKeywords.eof
			if i = 0 then
				sKeywords = rsKeywords.fields("Keyword")
			else
				sKeywords = sKeywords & ", " & rsKeywords.fields("Keyword")
			end if
			i = i + 1
			rsKeywords.movenext
		loop
		rsKeywords.close
	else
		sKeywords = "No Keywords"
	end if		
%>
	<TR id="keywordRow<%=iRoleID%>" class="RowHidden">
		<td colspan=5>Keywords assigned:  <%=sKeywords%></td>
		<td>&nbsp;</td>
		<td><a href="KeywordRoleAssignments.asp?roleid=<%= iRoleID %>">Edit Keyword assignments</a></td>
	</TR>
<%
			rs.MoveNext
		loop

	rs.Close
	set rs = Nothing
%>

</table>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>

<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">Modify/Delete Users</font></CENTER>
	</TD>
</TR>
</table>
<%
dim objDBConn
dim rs
dim sSQL

set objDBConn = server.CreateObject("ADODB.Connection")
set rs = server.CreateObject("ADODB.Recordset")

objDBConn.ConnectionString = Application("sDataSourceName")
objDBConn.Open

'sSQL = "select * from afxUsersLogin UL, afxUserInfo UI" _ 
'		& " where UI.UserID = UL.UserID AND UL.SiteID = '1' AND UL.AccessLevel is not null " _
'		& " order by UI.Lastname, UI.Firstname" 

sSQL = "SELECT * FROM afxUsersLogin"
		
set rs = objDBConn.Execute(sSQL)

%>

<form method=post name = frm action="DelUserConfirm.asp">
<table width="500" cellpadding="5" cellspacing="0" border="0" bgcolor="#CCCCCC" align="center">
	<tr>
		<td class=headerR><a href="AddUser.asp">Add New</a></td>
		<td class=header>Edit</td>
		<td class=header>User</td>
		<td class=header>Access Level</td>
		<td class=header>Delete</td>
	</tr>
<%
do while not rs.EOF
%>
	<%if LineColor = "white" then%>
	<tr bgcolor="#CCCCCC">
	<%LineColor = "#CCCCCC"%>
	<%else%>
	<tr bgcolor="white">
	<%LineColor = "white"%>
	<%end if%>
		<td>&nbsp;</td>
		<td><a href="modUser.asp?uid=<%=rs("UserID")%>">Edit</td>
		<td><%=rs("Lastname") & ", " & rs("firstname")%></td>
		<td><%=rs("AccessLevel")%></td>
		<td><input type="checkbox" name="delete" value="<%=rs("UserID")%>"></td>
	</tr>
<%
rs.MoveNext
loop
%>
	<%if LineColor = "white" then%>
	<tr bgcolor="#CCCCCC">
	<%else%>
	<tr bgcolor="white">
	<%end if%>
		<td colspan=5 align="right"><input type="Submit" value="Delete"></td>
	</tr>	
	</form>
</table>
<BR>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
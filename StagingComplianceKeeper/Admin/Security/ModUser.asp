<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">

<SCRIPT LANGUAGE="JavaScript1.1" SRC="/includes/FormCheck2.js"></SCRIPT>
<script language="Javascript">
	// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
	function InitializePage() {
	//	preloadImages();
	}
	// Enter Javascript Validation and Functions below: ----------------------
		
	function ValidateForm(form)
	{   
		return (
		  checkString(form.elements["firstname"],"First Name") &&
	      checkString(form.elements["lastname"],"Last Name") &&
	      checkString(form.elements["username"],"Username") &&
	      checkString(form.elements["AccessLevel"],"Access Level") 
	   )
	}

	function ValidatePassword() 
	// Expects fields password and confirm_pw in a form called "frm"
	{
		pw1 = document.frm.password.value;
		pw2 = document.frm.confirm_pw.value;
	
		if (document.frm.password.value.length == 0)
		{
			alert("\nPasswords cannot be left blank.  Please enter a password.")
			document.frm.password.focus();
			document.frm.password.select();
			return false;
		}
		
		if (pw1 != pw2) 
		{
			alert ("\nPasswords do not match. Please re-enter your password.")
			document.frm.confirm_pw.focus();
			document.frm.confirm_pw.select();
			return false;
		}
		
		else return true;
	}
</script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<%
dim UserID
dim rs
dim objConn
dim sSQL

UserID = request("uid")
if userID = "" or isnull(UserID) then
	userID = 0
end if

set objConn = server.CreateObject("ADODB.COnnection")
set rs = server.CreateObject("ADODB.Recordset")

objConn.ConnectionString = Application("sDataSourceName")
objConn.Open

'sSQL = "Select * from afxUsersInfo UI, afxUsersLogin UL Where ui.userID = ul.userID and UI.UserID = " & cint(UserID)
sSQL = "Select * from afxUsersLogin  WHERE UserID = " & cint(UserID)
set rs = objConn.Execute(sSQL)

%>

<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">Modify a User</font></CENTER><BR>
	</TD>
</TR>
</table>
<%
if not rs.EOF then
%>
<table width="500" border="0" align="center">
	<form method=post name = frm action="ModUserProc.asp?uid=<%=UserID%>" onSubmit="JavaScript:return ValidatePassword()">
	<tr>
		<td ALIGN="right"><B>First Name:&nbsp;</B></td>
		<td><input type="text" name="firstname" value="<%=rs("FirstName")%>" maxlength="75"></td>
	</tr>
	<tr>
		<td ALIGN="right"><B>Last Name:&nbsp;</B></td>
		<td><input type="text" name="lastname" value="<%=rs("LastName")%>" maxlength="75"></td>
	</tr>
	<tr>
		<td ALIGN="right"><B>Username:&nbsp;</B></td>
		<td><input type="text" name="username" value="<%=rs("UserName")%>" maxlength="50"></td>
	</tr>
	<tr>
		<td ALIGN="right"><B>Password:&nbsp;</B></td>
		<td><input type="password" name="password" value="<%=rs("Password")%>" maxlength="50"></td>
	</tr>
	<tr>
		<td ALIGN="right"><B>Confirm Password:&nbsp;</B></td>
		<td><input type="password" name="confirm_pw" value="<%=rs("Password")%>" maxlength="50"></td>
	</tr>		
	<tr>
		<td ALIGN="right"><B>Access Level:&nbsp;</B></td>
		<td><select name="AccessLevel">
				<%
				dim iAccessLevel 'as integer
				dim sWriterSelected, sEditorSelected, sAdministratorSelected 'as string
				
				sWriterSelect = ""
				sEditorSelect = ""
				sAdministratorSelect = ""
				
				iAccessLevel = rs.Fields("AccessLevel").Value
									
				if isnumeric(iAccessLevel) then
					select case iAccessLevel
						case 5
							sWriterSelected = "selected"
						case 10
							sEditorSelected = "selected"
						case 15
							sAdministratorSelected = "selected"
						case else
					end select
				end if
				%>
				<Option value="5" <%= sWriterSelected %>>Writer</option>
				<Option value="10" <%= sEditorSelected %>>Editor</option>
				<Option value="15" <%= sAdministratorSelected %>>Administrator</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="submit" name="Submit" value="Submit" onClick="return ValidateForm(this.form)"></td>
	</tr>	
	</form>
</table>

<%
else
Response.Write("No user found")
end if
%>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
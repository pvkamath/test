<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<%

function HandleChar(SomeValue)
	SomeValue = replace(SomeValue, """", "''")
	HandleChar = replace(SomeValue, "'", "''")
end function

Dim Username, Password, Firstname, Lastname, AccessLevel
dim sSQL
dim objConn

set objConn = server.CreateObject("ADODB.Connection")

objConn.ConnectionString = Application("sDataSourceName")
objConn.Open


Username = request("username")
Username = HandleChar(Username)
Password = request("password")
Password = HandleChar(Password)
Firstname = request("firstname")
Firstname = HandleChar(FirstName)
Lastname = request("lastname")
Lastname = HandleChar(Lastname)
AccessLevel = request("AccessLevel")
AccessLevel = HandleChar(AccessLevel)
UserID = request("uid")

if username = "" or isnull(username) then
	Response.Write("No username given.  Please use the Back button on your browser to go back and add a username")
	Response.End
end if

if lastname = "" or isnull(lastname) then
	Response.Write("No last name given.  Please use the Back button on your browser to go back and add a last name")
	Response.End
end if

if firstname = "" or isnull(firstname) then
	Response.Write("No first name given.  Please use the Back button on your browser to go back and add a first name")
	Response.End
end if

if password = "" or isnull(password) then
	Response.Write("No password given.  Please use the Back button on your browser to go back and add a password")
	Response.End
end if

if AccessLevel = "" or isnull(AccessLevel) then
	Response.Write("No Access Level given.  Please use the Back button on your browser to go back and add a Access Level")
	Response.End
end if


%>
<table width="500" height="25" cellpadding="5" cellspacing="2" border="0" bgcolor="" align="center"> 
<TR>
	<TD>
		<CENTER><font class="title">User Modified</font></CENTER><BR>
	</TD>
</TR>
</table>
<%
	'update afxUsers table - modified 2.13.2003 - jgo
	sSQL = " Update afxUsers SET LastName = '" & Lastname & "', " & _
			"FirstName = '"& Firstname &"'" & _
			"WHERE UserID = " & cint(UserID)
	objConn.Execute(sSQL)


	sSQL = " Update afxUsersLogin SET LastName = '" & Lastname & "', " & _
			"FirstName = '"& Firstname &"', " & _
			"Username = '" & Username & "', " & _
			"Password = '" & Password & "', " & _
			"Accesslevel = '" & AccessLevel & "' " & _
			"WHERE UserID = " & cint(UserID)


	objConn.Execute(sSQL)
%>
<table width="500" border="0" align="center">
	<tr>
		<td ALIGN="center"><%Response.Write("User " & UserName & " updated successfully")%></td>
	</tr>
</table>
<br>
<center><A HREF="ListUsers.asp">Return to User List</A></center>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
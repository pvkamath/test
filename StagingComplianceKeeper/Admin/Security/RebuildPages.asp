<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
	dim oSecurity 'as object
	dim rs 'as object
	dim sRoleName 'as string
	dim iRoleID 'as integer
	dim sClass 'as string
	dim i 'as integer
	dim bAddDirectory 'as boolean
	dim sError 'as string
</script>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

set oSecurity = new Security 'found in security.class.asp
oSecurity.ConnectionString = Application("sDataSourceName")

'oSecurity.DirectoryName = "test2"
'oSecurity.DirectoryParentID = 1
'bAddDirectory = oSecurity.AddDirectory()
'if bAddDirectory then
'	Response.write("directory " & oSecurity.DirectoryID & "<br>")
'else
'	Response.Write("error " & oSecurity.ErrorDescription & "<br>")
'end if

'bDeleteDirectory = oSecurity.DeleteDirectory(11)
'if bDeleteDirectory then
	'Response.write("directory deleted<br>")
'else
	'Response.Write("error " & oSecurity.ErrorDescription & "<br>")
'end if

'oSecurity.DirectoryID = 10
'iGetParentDirectory = oSecurity.GetParentDirectory()
'Response.write("directory " & iGetParentDirectory & " <br>")
'Response.Write("error " & oSecurity.ErrorDescription & "<br>")


'oSecurity.DirectoryID = 1
'bGetChildren = oSecurity.getChildren()
'if bGetChildren then
'	
	'set rs = oSecurity.ReturnedRecordset
'	do while not rs.eof
'		Response.Write("child " & rs.Fields("directoryID").Value & " name " & rs.Fields("directoryName").Value & "<br>") 
'		rs.Movenext
'	loop
'	Response.write("children returned<br>")
'else
'	Response.Write("error " & oSecurity.ErrorDescription & "<br>")
'end if



'oSecurity.DirectoryID = 1
'bGetPagesInDirectory = oSecurity.GetPagesInDirectory()
'if bGetPagesInDirectory then
'	
'	set rs = oSecurity.ReturnedRecordset
'	do while not rs.eof
'		Response.Write("page " & rs.Fields("pageID").Value & " name " & rs.Fields("pageName").Value & "<br>") 
'		rs.Movenext
'	loop
'	Response.write("pages returned<br>")
'else
'	Response.Write("error " & oSecurity.ErrorDescription & "<br>")
'end if



'oSecurity.DirectoryParentID = 0
'bListDirectories = oSecurity.ListDirectories()
'if bListDirectories then
'	
'	set rs = oSecurity.ReturnedRecordset
'	do while not rs.eof
'		Response.Write("directory " & rs.Fields("directoryID").Value & " name " & rs.Fields("directoryName").Value & " parentid " & rs.Fields("parentid").value & "<br>") 
'		rs.Movenext
'	loop
'	Response.write("directories returned<br>")
'else
'	Response.Write("error " & oSecurity.ErrorDescription & "<br>")
'end if


'oSecurity.PageID = 1
'iPage = oSecurity.GetDirectoryForPage()
'Response.write("page " & iPage & " <br>")
'Response.Write("error " & oSecurity.ErrorDescription & "<br>")

oSecurity.directoryID = 12
bRebuilt = oSecurity.RebuildPages()
if bRebuilt then
	Response.write("rebuilt<br>")
else
Response.Write("error " & oSecurity.ErrorDescription & "<br>")
end if
%>
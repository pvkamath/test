<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
	dim oSecurity 'as object
	dim sError 'as string
	dim sClass 'as string
	dim iGroupID 'as integer
	dim sChecked 'as string
	dim bGotRoles 'as boolean
	dim rsRoles 'as object
	dim iRoleID 'as integer
	dim sRoleName 'as string
	dim bIsRoleInGroup 'as boolean
</script>
<%
'retrieve variables
iGroupID = GetFormElement("groupID") 

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable



set oSecurity = new Security 'found in security.class.asp
oSecurity.ConnectionString = Application("sDataSourceName")

oSecurity.sort = "rolename"
bGotRoles = oSecurity.ListAllRoles()
if bGotRoles then
	set rsRoles = oSecurity.ReturnedRecordset
	rsRoles.cursorLocation = 3
	rsRoles.Open
else
	sError = oSecurity.ErrorDescription
	Response.Write sError
	Response.end
end if

	' Set Page Title:
	sPgeTitle = "Adjust Role Group Assignments"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle">User List</span>
<form name="RoleGroupAssignment" method="post" action="RoleGroupAssignmentsProc.asp">

<table bgcolor="DDDDDD" cellpadding=2 cellspacing=2 width=300 style="table-layout: fixed;">
<tr>
		<td class="ReportHeaderCell">Role Name</td>
</tr>
<%

	do while not rsRoles.EOF
		iRoleID = rsRoles.fields("roleID").Value
		sRoleName = rsRoles.fields("RoleName").Value

		if sClass = "ReportCell1" then
			sClass = "ReportCell2"
		else
			sClass = "ReportCell1"
		end if
		
		
		oSecurity.GroupID = iGroupID
		oSecurity.RoleID = iRoleID
		bIsRoleInGroup = oSecurity.IsRoleInGroup()
		
		if bIsRoleInGroup then
			sChecked = "CHECKED"
		else
			sChecked = ""
		end if
%>
	<tr class="<%= sClass%>">
		<td align="left">
			<input type="checkbox" id="addUser" name="addRole" value="<%=iRoleID%>"<%= sChecked%>>&nbsp;&nbsp;<%= sRoleName %>
		</td>
	</tr>
<%
		

		
		
		rsRoles.MoveNext
	loop

	rsRoles.Close
	set rsRoles = Nothing
%>
	<tr>
		<td><input type="submit" name="submit" value="submit">
	</tr>
</table>
<input type="hidden" name="groupID" value="<%= iGroupID%>">
</form>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

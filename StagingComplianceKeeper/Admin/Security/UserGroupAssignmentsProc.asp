<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<script language="vbscript" runat="server">
dim oSecurity 'as object
dim iGroupID 'as integer
dim sError 'as string
dim bDeleteUsersFromGroup 'as boolean
'dim iUserID 'as integer needed to comment out because security has the same variable
dim bAddUserToGroup 'as boolean
</script>
<%
'initialize 
set oSecurity = new Security 'found in security.class.asp
oSecurity.ConnectionString = Application("sDataSourceName")


'retrieve variables
iGroupID = GetFormElement("groupID") 

oSecurity.GroupID = iGroupID

'remove all cross references to the Group
bDeleteUsersFromGroup = oSecurity.DeleteUsersFromGroup()

if not bDeleteUsersFromGroup then
'error handling
	Response.Write(oSecurity.ErrorDescription)
	Response.End
end if

'add all the user to the group that have been selected  
for each iUserID in Request.Form("addUser")
	
	oSecurity.UserID = iUserID
	bAddUserToGroup = oSecurity.AddUserToGroup()	

	if not bAddUserToGroup then
	'error handling
		Response.Write(oSecurity.ErrorDescription)
		Response.End
	end if

next

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable



	
	' Set Page Title:
	sPgeTitle = "Adjust User Group Assignments"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {

				setTimeout("window.location='ListGroups.asp'", 5000);


		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
Users adjusted.  You will be redirected back to the List Groups page.

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

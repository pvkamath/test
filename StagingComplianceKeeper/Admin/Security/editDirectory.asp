<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	dim iDirectoryID, sDirectoryName, iParentID, sPath, bDirectoryReturned
	dim oSecurity
	dim sMode	'as string
	
	sMode = "ADD"
	' Set Page Title:
	sPgeTitle = "Home"
	
	'Try to get the directoryID
	iDirectoryID = GetFormElement("DirectoryID")
	if iDirectoryID = "" then
		iDirectoryID = 0
	end if

	set oSecurity = new Security 'found in security.class.asp
	oSecurity.connectionstring = Application("sDataSourceName")
	
	'If a directoryID was passed, get the current info for the directory
	'if not, prepare to add a record
	if not (checkEmpty(iDirectoryID)) AND (isnumeric(iDirectoryID)) then
		oSecurity.DirectoryID = iDirectoryID
		bDirectoryReturned = oSecurity.ListDirectories()
		if bDirectoryReturned then
			set rs = server.CreateObject("ADODB.Recordset")
			set rs = oSecurity.ReturnedRecordset

			if not rs.eof then
				sMode = "MOD"
				sDirectoryName = rs.fields("DirectoryName").value
				iParentID = rs.fields("ParentID").value
				sPath = rs.fields("path").value
			end if
			rs.close
			set rs = nothing
		end if
	end if
	
	'get rs for parent dropdown
	dim rsChildren
	oSecurity.DirectoryID = null
	oSecurity.DirectoryParentID = 0
	bGetDirectories = oSecurity.ListDirectories()
	
	if bGetDirectories then
		set rsChildren = oSecurity.ReturnedRecordset
	end if


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function ValidateForm(oFrm){
			if (oFrm.DirectoryName.value == "") {
				alert("Please enter a name for this directory");
				oFrm.DirectoryName.focus();
				return false;
			}
			if (oFrm.Path.value == "") {
				alert("Please enter a path for this directory");
				oFrm.Path.focus();
				return false;
			}
			return true;
		}
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle"><%if sMode = "ADD" then response.Write("Add Directory") else response.Write("Edit Directory")%></span>
<form name="frmAddDir" id="frmAddDir" method="post" action="editDirectoryProc.asp" onSubmit="return ValidateForm(this);">
<input type=hidden name="DirectoryID" value="<%=iDirectoryID%>" ID="DirectoryID">
<table cellspacing="0">
	<tr>
		<td align="right" class="ReportCell3" width="100">Directory Name</td>
		<td><input type="textbox" name="DirectoryName" value="<%= sDirectoryName%>" size="50" maxlength="100"></td>
	</tr>
	<tr>
		<td align="right"  class="ReportCell3" width="100">Path</td>
		<td><input type="textbox" name="Path" value="<%= sPath%>" size="50" maxlength="500"></td>
	</tr>
	<tr>
		<td align="right"  class="ReportCell3" width="100">Parent</td>
		<td><select name="ParentID">
				<option value="0">Root</option>
				
				<% call GenerateParentDropdown(rsChildren, 1, iParentID)%>
			</select>		
		</td>
	</tr>
	<tr>
		<td align="left" colspan="3"><input type="submit" value="submit" id=submit1 name=submit1></td>
	</tr>
	
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------

	set oSecurity = Nothing
	set rsChildren = NOthing
	
	Sub GenerateParentDropdown(p_rs, p_sLayer, p_iParentID)
	
		dim oSecurity, bGetDirectories
		
		if p_iParentID = "" then
			p_iParentID = 0
		else
			p_iParentID = cint(p_iParentID)
		end if
		
		Dim sSpaces, i, rsChildren
		sSpaces = ""
		
		for i = 1 to p_sLayer
			sSpaces = sSpaces & "--"
		next
		
		do while not p_rs.eof
			response.write("<option value=""" & p_rs("directoryID") & """")
			if p_rs("directoryID") = p_iParentID then
				response.write (" selected")
			end if
			
			response.write(">" & sSpaces & p_rs("DirectoryName") & "</option>")
	
			set oSecurity = new Security 'found in security.class.asp
			oSecurity.ConnectionString = Application("sDataSourceName")
			oSecurity.DirectoryParentID = p_rs("directoryID")
			bGetDirectories = oSecurity.ListDirectories()
			'get subnav
			if bGetDirectories then			
				set rsChildren = oSecurity.ReturnedRecordset
				GenerateParentDropdown rsChildren, p_sLayer + 1, p_iParentID
			end if
			
			set oSecurity = Nothing
			set rsChildren = Nothing
			p_rs.MoveNext
		loop
	End Sub

	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

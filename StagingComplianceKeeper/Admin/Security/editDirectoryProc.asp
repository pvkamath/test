<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

	dim oSecurity, sDirectoryName, iDirectoryID, sPath, iParentID, sMode, bSuccess
	
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	'Determine if you are adding or modifying a group (Check the GroupID)
	iDirectoryID = getFormElement("DirectoryID")
	sDirectoryName = getFormElement("DirectoryName")
	sPath = GetFormElement("Path")
	iParentID = GetFormElement("parentID")
	
	if iDirectoryID <> 0 and not CheckEmpty(iDirectoryID) then
		sMode = "EDIT"
	else
		sMode = "ADD"
	end if
	
	'Set up the class to add or edit
	oSecurity.DirectoryName = sDirectoryName
	oSecurity.DirectoryPath = sPath
	oSecurity.DirectoryParentID = iParentID
	
	if sMode = "EDIT" then
		oSecurity.DirectoryID = iDirectoryID
		bSuccess = oSecurity.ModifyDirectory()
		if bSuccess then
		else
			sError = oSecurity.errorDescription
		end if
	else
		bSuccess = oSecurity.AddDirectory()
		if bSuccess then
			iDirectoryID = oSecurity.DirectoryID
		else
			sError = oSecurity.errorDescription
		end if
	end if

	
	' Set Page Title:
	sPgeTitle = "Directory Added"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
			setTimeout("window.location='ListDirectories.asp'", 5000);
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
Directory Name: <%=oSecurity.DirectoryName%><BR>
Directory Path: <%=oSecurity.DirectoryPath%><BR>
Error: <%=sError%>
<BR><BR>
Directory Added.  You will be redirected back to the List Directories page.


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------

	set oSecurity = Nothing
	
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

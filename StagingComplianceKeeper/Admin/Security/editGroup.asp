<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	dim iGroupID, sGroupName, bGroupsReturned
	
	' Set Page Title:
	sPgeTitle = "Home"
'Try to get the GroupID
iGroupID = GetFormElement("g")

'If a groupID was passed, get the current info for the group
'if not, prepare to add a record
if not (checkEmpty(iGroupID)) AND (isnumeric(iGroupID)) then
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.connectionstring = Application("sDataSourceName")
	oSecurity.GroupID = iGroupID
	bGroupsReturned = oSecurity.GetGroups()
	if bGroupsReturned then
		set rs = server.CreateObject("ADODB.Recordset")
		set rs = oSecurity.ReturnedRecordset
		rs.Open
			if not rs.eof then
				sGroupName = rs.fields("GroupName").value
			end if
		rs.close
		set rs = nothing
	else
		iGroupID = 0
		sGroupName = ""
	end if
else
	iGroupID = 0
	sGroupName = ""
end if	

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function ValidateForm(form){
			return true;
		}
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle"><%if GroupID = 0 then response.Write("Add Group") else response.Write("Edit Group")%></span>
<form name="frmAddGroup" id="frmAddGroup" method="post" action="editGroupProc.asp" onSubmit="return ValidateForm(this);">
<input type=hidden name=GroupID value="<%=iGroupID%>" ID="Hidden1">
<table cellspacing="0">
	<tr>
		<td align="right"  class="ReportCell3">Group Name</td>
		<td width="20" class="ReportCell3">&nbsp;</td>
		<td>
			<input type="textbox" name="groupName" value="<%= sGroupName%>" maxlength="500">
		</td>
	</tr>
	<tr>
		<td align="center" colspan="3"><input type="submit" value="submit" id=submit1 name=submit1></td>
	</tr>
	
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

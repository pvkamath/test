<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = false			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Editing Role"
	
	dim iRoleID 'as integer
	dim oSecurity 'as object
	dim bRoleReturned 'as boolean
	dim sRole 'as string
	dim iAccessLevel 'as integer
	
	'get roleid if it was passed in
	iRoleID = GetFormElement("roleID")
	if iRoleID = "" then iRoleID = 0
	
	if not (checkEmpty(iRoleID)) AND (isnumeric(iRoleID)) then
		set oSecurity = new Security 'found in security.class.asp
		oSecurity.connectionstring = Application("sDataSourceName")
		oSecurity.RoleID = iRoleID
		bRoleReturned = oSecurity.GetRoles()
		if bRoleReturned then
			set rs = server.CreateObject("ADODB.Recordset")
			set rs = oSecurity.ReturnedRecordset
			rs.Open
			if not rs.eof then
				sRole = rs.fields("RoleName").value
				iAccessLevel = rs.fields("AccessLevel").value
			end if
			rs.close
			set rs = nothing
		end if
	end if	
	
	set oSecurity = Nothing
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function ValidateForm(form){
			return true;
		}
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
<span class="tabtitle"><% if iRoleID = 0 then response.write "Add " else response.write "Edit " %>Role</span>
<form name="frmAddGroup" id="frmAddGroup" method="post" action="editRoleProc.asp" onSubmit="return ValidateForm(this);">
<input type="hidden" name="RoleID" value="<%=iRoleID%>">
<table cellspacing="0">
	<tr>
		<td align="right"  class="ReportCell3">Role Name</td>
		<td width="20" class="ReportCell3">&nbsp;</td>
		<td>
			<input type="textbox" name="RoleName" value="<%= sRole%>" maxlength="500">
		</td>
	</tr>
	<tr>
		<td align="right"  class="ReportCell3">Access Level (integer)</td>
		<td width="20" class="ReportCell3">&nbsp;</td>
		<td>
			<input type="textbox" name="AccessLevel" value="<%= iAccessLevel%>" maxlength="3" size=5>
		</td>
	</tr>	
	<tr>
		<td align="center" colspan="3"><input type="submit" value="submit" id=submit1 name=submit1></td>
	</tr>
	
</table>
</form>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

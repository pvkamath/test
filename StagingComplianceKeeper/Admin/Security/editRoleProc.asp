<!-- #include virtual = "/admin/security/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/security/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = False			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = false				' Template Constant for Admin section
	const ROLLOVER_IMAGES = 1			' 1 for image rollovers, 0 for none	
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SHOW_PRO_MENU = FALSE					'Determines whether or not to show the PROFESSIONAL menu
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state
												'PROFESSIONAL = 1
												'OVERVIEW = 2
												'PROFILES = 3
												'PRICES = 4
												'INFO = 5
	
	const MENU_NUMBER = 2
	dim sPgeTitle							' Template Variable

	dim oSecurity
	dim iRoleID, sRoleName, iAccessLevel
	dim sMode, bSuccess
	
	set oSecurity = new Security 'found in security.class.asp
	oSecurity.ConnectionString = Application("sDataSourceName")
	
	'Determine if you are adding or modifying a role (get RoleID)
	iRoleID = GetFormElement("RoleID")
	sRoleName = getFormElement("RoleName")
	iAccessLevel = getFormElement("AccessLevel")
	
	if iRoleID <> 0 and not CheckEmpty(iRoleID) then
		sMode = "EDIT"
	else
		sMode = "ADD"
	end if
	
	'Set up the class to add or edit

	oSecurity.RoleName = sRoleName
	oSecurity.AccessLevel = iAccessLevel

	if sMode = "EDIT" then
		oSecurity.RoleID = iRoleID
		bSuccess = oSecurity.ModifyRole()
		if bSuccess then
			iRoleID = oSecurity.RoleID
		else
			sError = oSecurity.errorDescription
		end if
	else
		bSuccess = oSecurity.AddRole()
		if bSuccess then
			iRoleID = oSecurity.RoleID
		else
			sError = oSecurity.errorDescription
		end if
	end if

	set oSecurity = Nothing

	
	' Set Page Title:
	sPgeTitle = "Role " & sMode & "ed"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<LINK REL="stylesheet" TYPE="text/css" HREF="/admin/security/includes/Style_ie.css">
<script TYPE="text/javascript"><!--



		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
			setTimeout("window.location='ListRoles.asp'", 5000);
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>
RoleName: <%=sRoleName%><BR>
RoleID: <%=iRoleID%><BR>
Error: <%=sError%>
<BR><BR>
Role <%=sMode%>ed.  You will be redirected back to the List Roles page.


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------

	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

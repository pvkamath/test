<%
'===============================================================================================================
' Class: HandyADO
' Description: Got tired of putting this code in every component.
' Create Date: 10/8/2001
' Current Version: 1.0
' Author(s): Mike Joseph
' References to other DLL's:  ADO
' ----------------------
' Properties:
'	- No Properties
'
' Methods:
'	- GetDisconnectedRecordset(p_sDataSource, p_sSQL, p_bUpdateable)
'	- UpdateDisconnectedRecordset(p_sDataSourceName, p_objRS)
'	- PrintRecordset(p_rs)
'	- GetMaxId(p_sTable, p_sIdField)
'	- GetSingleValue(p_sDSN, p_sTable, p_sGetField, p_sSearchField, p_sFieldValue)
'	- RecordExists(p_sDSN, p_sTable, p_sField, p_sFieldValue)
'	- ExecuteSQL(p_sDSN, p_sSQL)
'===============================================================================================================
Class HandyADO
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	' None Defined

	' PUBLIC METHODS ===========================================================================================
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Will return a disconnected recordset given an SQL String
	' Pre-conditions: ConnectionString Property must be set
	' Inputs: 
	'	Property Inputs -- None
	'	Parameter Inputs:
	'		- p_sDataSource: Connection String
	'		- p_sSQL: SQL Query to Run
	'		- p_bUpdateable: True to return an updateable recordset, false to return a static one.
	' Outputs: Returns a disconnected Recordset containing the results of the query
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetDisconnectedRecordset(p_sDataSource, p_sSQL, p_bUpdateable)
		dim bError, rs
		bError = False

		set rs = CreateObject("ADODB.Recordset")

		if isEmpty(p_sDataSource) or p_sDataSource = "" then bError = True
		
		if not bError then
			dim objDBConn
			set objDBConn = CreateObject("ADODB.Connection")
			'response.write p_sDataSource
			objDBConn.Open p_sDataSource
			rs.CursorLocation = adUseClient

'			response.write p_sSQL & "<br><br>"
			if p_bUpdateable then
				
				rs.Open p_sSQL, objDBConn, adOpenDynamic, adLockBatchOptimistic
			else
				rs.Open p_sSQL, objDBConn, adOpenStatic, adLockReadOnly
			end if
			set rs.ActiveConnection = Nothing

			objDBConn.Close
			set objDBConn = Nothing
		end if

		set GetDisconnectedRecordset = rs
		set rs = Nothing
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Will update an Updateable Disconnected Recordset
	' Pre-conditions: ConnectionString Property must be set
	' Inputs: 
	'	Property Inputs -- ConnectionString
	'	Parameter Inputs -- p_objRS -- Updateable Disconnected Recordset (adOpenDynamic, adLockBatchOptimistic)
	' Outputs: Returns true or false
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function UpdateDisconnectedRecordset(p_sDataSourceName, p_objRS)
		dim bRetVal
		bRetVal = True

		if isEmpty(p_sDataSourceName) or p_sDataSourceName = "" then bRetVal = False
		
		' wscript.echo "Got Here"

		if bRetVal then
			dim objDBConn
			set objDBConn = CreateObject("ADODB.Connection")
			objDBConn.Open p_sDataSourceName

			p_objRS.ActiveConnection = objDBConn
			p_objRS.UpdateBatch
			set p_objRS.ActiveConnection = Nothing

			objDBConn.Close
			set objDBConn = Nothing
		end if

		UpdateDisconnectedRecordset = bRetVal
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	'	Property Inputs -- 
	'	Parameter Inputs -- 
	' Outputs: Integer of how many files were unzipped or -1 for error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetMaxId(p_sDSN, p_sTable, p_sIdField)

		dim sRetVal
		dim sSQL, objDBConn, rs

		' Validate Params:
		sRetVal = 0
		if isEmpty(p_sTable) or p_sTable = "" then sRetVal = -1
		if isEmpty(p_sIdField) or p_sIdField = "" then sRetVal = -1

		if sRetVal <> -1 then
			' Create Connection Objects:
			set objDBConn = CreateObject("ADODB.Connection")
			objDBConn.Open p_sDSN

			' Create a ClientSide Recordset:
			set rs = CreateObject("ADODB.Recordset")
			rs.CursorLocation = adUseClient

			' Get the MaxID
			sSQL = "SELECT max(" & p_sIdField & ") as maxid FROM " & p_sTable
			rs.Open sSQL, objDBConn, adOpenStatic, adLockReadOnly
			
			' If results were found, set the value:
			if not rs.EOF and not isNull(rs("maxid")) then sRetVal = cint(rs("maxid"))
		end if

		GetMaxId = sRetVal
		
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	'	Property Inputs -- 
	'	Parameter Inputs -- 
	' Outputs: Integer of how many files were unzipped or -1 for error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RecordExists(p_sDSN, p_sTable, p_sField, p_sFieldValue)
		dim sSQL, rs, bRetVal
		bRetVal = False
		sSQL = "SELECT " & p_sField & " FROM " & p_sTable & " WHERE " & p_sField & " = '" & ScrubForSQL(p_sFieldValue) & "'"
		set rs = GetDisconnectedRecordset(p_sDSN, sSQL, False)
		if not rs.EOF then bRetVal = True

		rs.Close
		set rs = Nothing

		RecordExists = bRetVal
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------

	Public Function GetSingleValue(p_sDSN, p_sTable, p_sGetField, p_sSearchField, p_sFieldValue)
		dim sSQL, rs, sRetVal
		sRetVal = ""
		sSQL = "SELECT " & p_sGetField & " FROM " & p_sTable & " WHERE " & p_sSearchField & " = '" & ScrubForSQL(p_sFieldValue) & "'"
'		response.write ssql
'		response.end
		set rs = GetDisconnectedRecordset(p_sDSN, sSQL, False)
		if not rs.EOF then sRetVal = rs(p_sGetField)

		rs.Close
		set rs = Nothing

		GetSingleValue = sRetVal
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ExecuteSQL(p_sDSN, p_sSQL)
		dim objDBConn
		set objDBConn = CreateObject("ADODB.Connection")
		objDBConn.Open p_sDSN
		objDBConn.Execute p_sSQL
		objDBConn.Close
		set objDBConn = Nothing
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------

	Public Sub PrintRecordset(p_rs, p_sHeaderClass, p_sRow1Class, p_sRow2Class)
		dim sCurrentStyle, i
		
		response.write "<table border=""1"">"

		' Header Row:
		response.write "<tr>"
		for i = 0 to p_rs.Fields.Count - 1
			response.write "<td class=""rptHdrCell"">" & p_rs(i).Name & "</td>"
		next
		response.write "</tr>"
			
		' Data:
		sCurrentStyle = p_sRow1Class
		do until p_rs.EOF

			' Alternate Row Style:
			if sCurrentStyle = p_sRow1Class then
				sCurrentStyle = p_sRow2Class
			else
				sCurrentStyle = p_sRow1Class
			end if

			' Print Row:
			response.write "<tr>"
			for i = 0 to p_rs.Fields.Count - 1 
				response.write "<td class=""" & sCurrentStyle & """>" & p_rs(i) & "</td>"
			next
			response.write "</tr>"
			
			p_rs.MoveNext
		Loop
		
		' Fin.
		response.write("</table>")

	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Added: 1/30/2002 by MJ
	Public Sub PrepareRecordsetForPage(p_nRecordsPerPage, p_nPage, p_rs)
		if isNumeric(p_nRecordsPerPage) then 
			p_nRecordsPerPage = Cint(p_nRecordsPerPage)
		else
			p_nRecordsPerPage = 25
		end if
		
		if isNumeric(p_nPage) then
			p_nPage = Cint(p_nPage)

			' Make sure the page isn't negative:
			if p_nPage < 1 then p_nPage = 1
		else
			p_nPage = 1
		end if

		' Make sure a recordset was passed in:
		if isObject(p_rs) then

			' Set the Page Size:
			p_rs.PageSize = p_nRecordsPerPage
			'p_rs.CacheSize = p_rs.PageSize

			dim nPageCount, nRecordCount

			' Cache some values from the rs:
			nPageCount = p_rs.PageCount
			nRecordCount = p_rs.RecordCount

			' Make sure the current page isn't larger than the actual # of pages:
			if p_nPage > nPageCount then p_nPage = nPageCount

			' Move the Recordset to the Appropriate page:
			if nRecordCount > 0 then	p_rs.AbsolutePage = p_nPage
		end if
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ScrubForSQL(p_sText)
		if isNull(p_sText) or isEmpty(p_sText) then
			ScrubForSQL = ""
		else
			ScrubForSQL = Replace(p_sText, "'", "''")
		end if
	End Function

	' PRIVATE METHODS ==========================================================================================
	Private Sub Class_Initialize()
		' Enter code you want to run on Object Startup here.
	End Sub

	Private Sub Class_Terminate()
		' Enter code you want to run on Object Termination here.
	End Sub

	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined

	'===========================================================================================================
End Class
%>
<%
	' ----------- Place Environment Variables Here -----------------------------------
	
'	option explicit
'	Response.Buffer = False		' False for Debug Mode
	' on error resume next
	
	CONST vbBlank = ""
	
	if Application("sDataSourceName") <> "" then
		dim g_objDBConn
		set g_objDBConn = Server.CreateObject("ADODB.Connection")
		
		g_objDBConn.Open Application("sDataSourceName")
	end if
	
	' ================================================================================
	sub CheckSecurityAdmin()
		'dim sTmp
		'if session("user_id") = "" then
		'	sTmp = "Your session has expired or you have not yet logged on.<br>Please log on to the system again.<br><br>Thank you."
		'	session("sErrMsg") = ""
		'	response.redirect("/admin/default.asp?msg=" & Server.URLEncode(sTmp))
		'end if
		
		if session("Access_Level") <> "" then
			if not isNumeric(session("Access_Level")) then
				session.Abandon
				server.transfer("/admin/default.asp")
				'Response.Write session("Access_Level") & " Not numeric"
			else
'				if session("Access_Level") < 15 then
				if session("Access_Level") <> 1 then
					session.Abandon
					server.transfer("/admin/default.asp")
					'Response.Write session("Access_Level") & " < 15"
				end if					
			end if	
		end if	

		
	end sub

	function CheckEmpty(p_sString)
		dim bRetVal
		
		bRetVal = true
		
		if isnull(p_sString) OR p_sString = "" then
			bRetVal = false
		else
			bRetVal = true
		end if
		
		CheckEmpty = bRetValue
	
	end function
		
	sub CheckSecurityInfo(p_nLevel)
		'dim sTmp
		'if session("user_id") = "" then
		'	sTmp = "Your session has expired or you have not yet logged on.<br>Please log on to the system again.<br><br>Thank you."
		'	session("sErrMsg") = ""
		'	response.redirect("/admin/default.asp?msg=" & Server.URLEncode(sTmp))
		'end if
		'if session("Access_Level") > p_nLevel then
		'	Response.Redirect("/main/sign.asp")
		'end if
		
		if session("Access_Level") <> "" then
			if not isNumeric(session("Access_Level")) then
				session.Abandon
				server.transfer("/main/sign.asp")
				'Response.Write session("Access_Level") & " Not numeric"
			else
				if session("Access_Level") < 4 then
					session.Abandon
					server.transfer("/main/sign.asp")
					'Response.Write session("Access_Level") & " < 15"
				end if					
			end if	
		else
			session.Abandon
			server.transfer("/main/sign.asp")
		end if	
		
	end sub
	
	' ================================================================================
	Function ScrubForSQL(p_sText)
		if not isNull(p_sText) then
			ScrubForSQL = replace(p_sText, "'", "''")
		else
			ScrubForSQL = ""
		end if
	End Function
	' --------------------------------------------------------------------------------
	Function CheckForNullCell(p_sText)
		if isNull(p_sText) or p_sText = "" then
			CheckForNullCell = "&nbsp;"
		else
			CheckForNullCell = p_sText
		end if
	End Function
	' --------------------------------------------------------------------------------
	Function ScrubForJavascriptString(p_sText)
		if not isNull(p_sText) then
			ScrubForJavascriptString = replace(p_sText, "'", "\'")
		else
			ScrubForJavascriptString = ""
		end if
	End Function
	
	' --------------------------------------------------------------------
	Sub PrintArray(vec,lo,hi)
	  '== Simply print out an array from the lo bound to the hi bound.
	  Dim i
	  For i = lo to hi
	    Response.Write vec(i) & "<BR>"
	  Next
	End Sub  'PrintArray
	' --------------------------------------------------------------------
	
	function BackButton(p_sText)
		response.write("<a href=""JavaScript:history.back();""><img align=""middle"" border=""0"" src=""/media/images/backarrow.gif"" width=""51"" height=""63"">Go Back")
		if p_sText <> "" then response.write(" to " & p_sText)
		response.write("</a>")
	end function
	
	' --------------------------------------------------------------------------------

	' --------------------------------------------------------------------
	
	function DecimalToPercentage(p_nDecimal)
		dim nDecimal
		
		nDecimal = CDbl(p_nDecimal)
		
		DecimalToPercentage = nDecimal * 100
	end function
	
	'---------------------------------------------------------------------
	
	function PercentageToDecimal(p_nPercentage)
		
		dim nPercentage
		
		nPercentage = CDbl(p_nPercentage)
		
		PercentageToDecimal = Round(nPercentage / 100, 5)
		
	end function
	
	' --------------------------------------------------------------------	
	' Places commas into a number:
	Function PlaceCommasInNumber(p_nValue) 
		if isNull(p_nValue) then
			PlaceCommasInNumber = ""
		else
			dim sTempString, nDecimalPosition, nCommaPlace, sDecimal, bDecimalExists
			
			' Convert to String and round the number to the nearest penny:
			sTempString = CStr(Round(p_nValue, 2))
			
			' Find the position of the Decimal Place:
			bDecimalExists = False
			if InStr(sTempString, ".") then bDecimalExists = True
			
			nDecimalPosition = (len(sTempString) - InStr(sTempString, ".")) + 1
			
			' Save the decimal place in a variable:
			if bDecimalExists then
				sDecimal = right(sTempString, nDecimalPosition)
				sTempString = left(sTempString, len(sTempString) - nDecimalPosition)
				
				' Place a trailing zero on the end if is a decimal like .5
				if len(sDecimal) = 2 then sDecimal = sDecimal & "0"
			end if
			
			' Get the Length of the Value passed in:
			nCommaPlace = len(sTempString)
			
			' Find the position of where the first comma should be (from the dec place):
			nCommaPlace = nCommaPlace - 4
			
			' If we haven't reached the end of the string:
			do while nCommaPlace >= 0
				
				' Add a comma by splitting the string at the comma position and placing a comma in:
				sTempString = left(sTempString, nCommaPlace + 1) & "," & right(sTempString, (len(sTempString) - nCommaPlace) - 1)
				
				' Find where the next comma should be:
				nCommaPlace = nCommaPlace - 3
			Loop
			
			' Put the decimal place back on and return
			PlaceCommasInNumber = sTempString & sDecimal
		end if
End Function	
	
	' --------------------------------------------------------------------
	
	function DisplayAsMoney(p_nNumber)
		
		dim sRetVal
		
		sRetVal = CStr(p_nNumber)
		
		' If there are no decimial places
		if InStr(sRetVal, ".") = 0 then
			' Add two trailing zeros:
			sRetVal = sRetVal & ".00"
			
		' If there are decimal places
		else
			if InStrRev(sRetVal, ".") < 2 then
				' Add one trailing zero:
				sRetVal = sRetVal & "0"
			elseif InStrRev(sRevVal, ".") > 2 then
				' Round off to 2 decimal places:
				sRetVal = CStr(Round(CSng(sRetVal), 2))
			end if
		end if
		
		if left(sRetVal, 1) = "." then
			' Add a Leading Zero:
			sRetVal = "0" & sRetVal
		end if
		
		' Return:
		DisplayAsMoney = sRetVal
		
	end function
	
	function FormatDate8Digits(p_sDate)
		dim sMonth, sDay, sYear
		sMonth = month(p_sDate)
		sDay = day(p_sDate)
		sYear = year(p_sDate)
		
		if len(sMonth) = 1 then
			sMonth = "0" & sMonth
		end if
		if len(sDay) = 1 then
			sDay = "0" & sDay
		end if
		
		FormatDate8Digits = sMonth & "/" & sDay & "/" & sYear
	end function

		Function PrintSiteContentTextFile(p_nSiteContentId)
		dim sPath, objFS, objTextFile, sRetVal
		sPath = application("sAbsWebroot") & "text\section\" & p_nSiteContentId & ".txt"
		
		' response.write(sPath)
		set objFS = Server.CreateObject("Scripting.FileSystemObject")
		set objTextFile = objFS.OpenTextFile(sPath, 1, True)
		
		' response.write(objTextFile.AtEndOfStream)
		if not objTextFile.AtEndOfStream then sRetVal = objTextFile.ReadAll
		
		' response.write("RetVal = " & sRetVal)
		
		objTextFile.Close
		set objTextFile = Nothing
		set objFS = Nothing
		
		PrintSiteContentTextFile = sRetVal
	End Function
	
	sub PrintContent(p_nSectionId)
		dim sPagePath, sSQL, rs_tmp
		sPagePath = Request.ServerVariables("PATH_INFO")
		
		sSQL = "select * from site_section where id = " & p_nSectionId _
			& " and UPPER(path) = '"& UCase(sPagePath) & "'"
		' response.write(sSQL)
		set rs_tmp = objDBConn.Execute(sSQL)
		
		'response.write(vbCrLf & "<br>Path = " & rs_tmp("path") & ", PathInfo = " & Request.ServerVariables("PATH_INFO") & "<br>" & vbCrLf)
		'response.end
		
		if not rs_tmp.eof then
			response.write PrintSiteContentTextFile(p_nSectionId)
		else
			response.redirect("error.asp?errmsg=" & Server.URLEncode("This page has been called illegally."))
		end if
		
		rs_tmp.Close
		set rs_tmp = Nothing
	end sub

	Function PrintTextFile(p_sFilePath)
		dim objFS, objTextStream, sRetVal, sFileName
		set objFS = Server.CreateObject("Scripting.FileSystemObject")

		sRetVal = ""
		sFileName = Server.MapPath(p_sFilePath)
		if objFS.FileExists(sFileName) then
			' response.write sFileName

			set objTextStream = objFS.OpenTextFile(sFileName, 1)
			
			if not objTextStream.AtEndOfStream then	sRetVal = objTextStream.ReadAll

			objTextStream.Close
			set objTextStream = Nothing
		end if
		
		set objFS = Nothing

		PrintTextFile = sRetVal
	End Function

	' ---------------------------------------------------------------------------------------------------------
	Sub Error(p_sErrorMessage)
		'CleanUpObjects
		response.redirect(Application("sDefaultErrorPage") & "?msg=" & Server.URLEncode(p_sErrorMessage))
	End Sub

	' ---------------------------------------------------------------------------------------------------------
	Sub PassFormElementsThrough()
		dim x

		response.write(vbCrLf)
		For Each x in Request.Form
			response.write("<input type=""hidden"" id=""" & x & """ name=""" & x & """ value=""" & Request.Form(x) & """>" & vbCrLf)
		Next

		For Each x in Request.QueryString
			response.write("<input type=""hidden"" id=""" & x & """ name=""" & x & """ value=""" & Request.QueryString(x) & """>" & vbCrLf)
		Next
	End Sub

	' ---------------------------------------------------------------------------------------------------------
	Function GetFormElement(p_sName)
		dim sRetVal

		sRetVal = Trim(Request.Form(p_sName))
		if sRetVal = "" then sRetVal = Trim(Request.QueryString(p_sName))

		GetFormElement = sRetVal
	End Function

	' ---------------------------------------------------------------------------------------------------------
	' Inputs:
	'	- p_sKeywords -- Comma delimited string of Keywords to filter by
	'	- p_nNumArticles -- Number of Articles to retrieve
	'	- p_nArticleLength -- How many characters to preview before printing a <more> button.
	Sub PrintVerticalNewsBlock(p_sKeywords, p_nNumArticles, p_nArticleLength, p_sHeadingText)
		dim sRetVal
		sRetVal = ""

		' Validate Conditions:
		if p_sKeywords = "" or isEmpty(p_sKeywords) then 
			sRetVal = "You must specify at least one keyword."
		elseif not isNumeric(p_nNumArticles) then
			sRetVal = "Number of Articles must be a number."
		elseif cint(p_nNumArticles) <= 0 then
			sRetVal = "You must specify a positive number of articles."
		elseif not isNumeric(p_nArticleLength) then
			sRetVal = "Length of Articles must be a number."
		elseif cint(p_nArticleLength) <= 0 then
			sRetVal = "You must specify a positive number for the length of articles."
		end if


		if sRetVal = "" then
			dim sKeyword, nCount
			dim objSearch, rsNews

			' Setup Objects:
			set objSearch = Server.CreateObject("NewsFxV2.Searching")
			set rsNews = server.CreateObject("ADODB.Recordset")
			
			' Set the ConnectionString for the search component:
			objSearch.ConnectionString = application("sDataSourceName")
			objSearch.SiteID = application("siteID")
			objSearch.MaxRecords = p_nNumArticles
			
			' Populate aKeywords for searching:
			if isArray(p_sKeywords) then
				dim aKeywords()
				redim preserve aKeywords(UBound(p_sKeywords))

				nCount = 0
				do while nCount < UBound(p_sKeywords)
					aKeywords(nCount) = Trim(p_sKeywords(nCount))
					nCount = nCount + 1
				Loop
			else
				' dim aKeywords
				aKeywords = Split(p_sKeywords, ",")

				nCount = 0
				do while nCount < UBound(aKeywords)
					aKeywords(nCount) = Trim(aKeywords(nCount))
					nCount = nCount + 1
				Loop
			end if

			' Add each Keyword into the search conditions
			dim nTmp
			For each sKeyword in aKeywords
				nTmp = FindKeywordId(sKeyword, "NULL")
				' response.write(nTmp)
				if nTmp <> "" then objSearch.AddKeywordIDSearch nTmp
			Next
			
			' Run the Search:
			set rsNews = objSearch.RunSearch
			
			' If results were found:
			if not (rsNews.EOF or rsNews.BOF) Then
				
				' All this gets info for pictures (not being used... just for the future additions)
				MediaPath = rsNews("MediaPath")
				NewsItemID = rsNews("NewsItemID")
				Headline = rsNews("Headline")
				AltTag = rsNews("AltTag")
				Caption = rsNews("Caption")
				Credit = rsNews("Credit")								
				bodyID = rsNews("BodyID")
				objSearch.BodyID = bodyID
				set rsBody = objSearch.getbodytext
				Body = rsBody("BodyText")
				' Layout = rsNews("MediaName")			Ignoring  
				
				%><table border="0" cellpadding="2" cellspacing="0" width="270"><%
				if p_sHeadingText <> "" then response.write("<tr><td class=""newsfx_preview_head"">" & p_sHeadingText & "</td></tr>")
				
				do while not rsNews.EOF
					bodyID = rsNews("BodyId")
					objSearch.BodyID = bodyID
					set rsBody = objSearch.getbodytext
					Body = rsBody("BodyText")

					%><td valign="top" align="left" width="463" class="article">
						<div class="newsfx_preview_headline">
							<a href="<%=application("sDynWebRoot")%>article/article.asp?NewsID=<%=rsNews("NewsItemID")%>&sID=<%=sID%>"><%= rsNews("Headline")%></a>
						</div>
						<div class="newsfx_preview_info">
							<%= FormatDateTime(rsNews("PostingDate"), 2) %>
						</div>
						<div class="newsfx_preview_text">
							<% 
								nTmpLength = p_nArticleLength
								' response.write nTmpLength
								response.write objSearch.StoryLength(Body,nTmpLength) & "..."%>
							<a href="<%=application("sDynWebRoot")%>article/article.asp?NewsID=<%=rsNews("NewsItemID")%>&sID=<%=sID%>"><img src="<%=Application("sDynWebroot")%>media/images/button_more.gif" width="30" height="10" alt="Click Here for More Information" align="absmiddle" border="0"></a>
					</td></tr><%

					rsBody.Close
					set rsBody = Nothing

					rsNews.MoveNext
				Loop	
				%></table><%
			end if


			' Destroy Objects:
			set rsNews = Nothing
			set objSearch = Nothing
		end if
	End Sub

	' --------------------------------------------------------------------------------------------
	Function FindKeywordId(p_sKeyword, p_sKWType)
		dim sRetVal
		dim objDBConn, sSQL, rs
		
		' Validate Params:
		if p_sKeyword = "" or isEmpty(p_sKeyword) then 
			sRetVal = "You must specify a keyword."
		elseif not isEmpty(p_sKWType) then
			if p_sKWType <> "NULL" and p_sKWType <> "" and not isNull(p_sKWType) then
				if not isNumeric(p_sKWType) then sRetVal = "If a keyword type is specified, it must be numeric."
			end if
		end if

		if len(sRetVal) = 0 then
			' Open Connections:
			set objDBConn = Server.CreateObject("ADODB.Connection")
			objDBConn.Open Application("sDataSourceName")
			set rs = Server.CreateObject("ADODB.Recordset")
			rs.CursorLocation = adUseClient
			
			' Get the id from afxKeywords:
			sSQL = "SELECT keywordID as id FROM afxKeywords " _
				& "WHERE keyword = '" & ScrubForSQL(p_sKeyword) & "' " _
				& "and keywordType "

			if p_sKWType = "" or isNull(p_sKWType) or UCase(p_sKWType) = "NULL" then
				sSQL = sSQL & "is null"
			else
				sSQL = sSQL & " = " & p_sKWType
			end if

			' Create Disconnected RS and close DBConnection
			rs.Open sSQL, objDBConn, adOpenStatic, adLockReadOnly
			set rs.ActiveConnection = Nothing
			objDBConn.Close
			set objDBConn = Nothing

			' Get the ID into a variable:
			if rs.EOF then
				sRetVal = ""
			else
				sRetVal = rs("id")
			end if
			
			rs.Close
			set rs = Nothing
		end if

		FindKeywordId = sRetVal
		
	End Function
	
	Function FindKeyword(p_intKeywordID)
		if isNumeric(p_intKeywordID) then
			sSQL = "SELECT KeywordID, Keyword FROM afxKeywords where KeywordID = " & P_intKeywordID
			set objConn = server.CreateObject("ADODB.COnnection")
			objConn.connectionstring = application("sDataSourceName")
			objConn.open
			set rs = server.CreateObject("ADODB.Recordset")					
			with rs
				.CursorLocation = adUseClient
				.ActiveConnection = objConn
				.Open sSQL,,3,3
			end with
			if not rs.eof then
				FindKeyword = rs("Keyword")
			else
				FindKeyword = "None"	
			end if

			rs.close
			set rs = nothing
			objConn.close
			set objConn = nothing	
		end if

	end function	
	
	function getImage(p_intNewsItemID, p_strAspectRatio)
	
		sSQL = "Select * from afxNewsMedia where NewsItemID = " & p_intNewsItemID & " AND MediaName = '" & p_strAspectRatio & "'"
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		set rs = server.CreateObject("ADODB.Recordset")
		with rs
			.cursorlocation = aduseclient
			.cursortype = adForwardOnly
			.locktype = adLockReadOnly
			.open sSQL, objConn
		end with
		
		if not rs.eof then
			getImage = array(cstr(rs("MediaPath")), cstr(rs("AltTag")))
		else
			getImage = array("None", 0)
		end if	
	
		rs.close
		set rs = nothing
		objConn.Close
		set objConn = nothing
	
	end function
	
	function getImages(p_intNewsItemID)

		dim i

		
		sSQL = "Select * from afxNewsMedia where NewsItemID = " & p_intNewsItemID & " ORDER BY MediaID asc "
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		set rs = server.CreateObject("ADODB.Recordset")
		with rs
			.cursorlocation = aduseclient
			.cursortype = adForwardOnly
			.locktype = adLockReadOnly
			.open sSQL, objConn
		end with

		intRecordCount = rs.recordcount

		i = 0
		if rs.eof then
			getImages = false
			exit function
		else
			redim strAltTag(intRecordCount-1)
			redim strMediaPath(intRecordCount-1)
			redim strCaption(intRecordCount-1)
			do while not rs.eof
				strAltTag(i) = rs("AltTag")
				strMediaPath(i) = rs("MediaPath")
				strCaption(i) = rs("Caption")
				i = i + 1
				rs.movenext
			loop
			getImages = "1"	
		end if	
	
		rs.close
		set rs = nothing
		objConn.Close
		set objConn = nothing		
		
	end function	
	
	
	function CheckBrowser()
		Set bc = Server.CreateObject("MSWC.BrowserType") 
		CheckBrowser = bc.browser
		set bc = nothing
	end function
	
	
%>
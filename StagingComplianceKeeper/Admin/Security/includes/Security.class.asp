<%
'===============================================================================================================
' Class: Security 1.0
' Description: An object used to provide security functions
' Create Date: 6/4/2002
' Author(s): Sean Stansell
'
' ---------------------------------------------------------------------
' Properties: 
'	- ConnectionString - String
'	- ErrorDescription - String
'	- ReturnedRecordset - ADO Recordset
'   - Connection - ADO Connection (PRIVATE)
'	- UserID - Integer
'   - UserName - String
'   - GroupID - Integer
'   - GroupName - String
'	- DirectoryID - Integer
'	- DirectoryName - String
'   - DirectoryParentID - String
'   - RoleID - Integer
'   - RoleName - String
'   - AccessLevel - Integer
'	- PageID - Integer
'	- PageName - String
'	- PageDirectoryID - Integer
'   - KeywordID - Integer
'	- Sort - String
' Methods:
'	- ClearProperties
'	- ClearErrorDescription
'	- CheckRequiredProperties (PRIVATE)
'	- AddDirectory
'	- AddDirectoryToRole
'	- AddGroup
'	- AddKeywordToRole
'	- AddPageToRole
'	- AddRole
'	- AddRoleToGroup
'   - AddUserToGroup
'	- DeleteDirectory
'	- DeleteGroup
'	- DeleteRole
'	- DeleteRolesFromGroup
'	- DeleteRolesFromPages
'	- DeleteRolesFromDirectories
'	- DeleteRolesFromKeywords
'	- DeleteUsersFromGroups
'	- GetAccessLevel
'	- GetChildren
'	- GetDirectoriesInRole
'	- GetDirectoryForPage
'	- GetGroups
'	- GetKeywordsInRole
'	- GetPagesInRole
'	- GetPagesInDirectory
'	- GetParentDirectory
'	- GetRoles
'   - GetRolesInGroup
'	- GetUsersInGroup
'	- IsPageInRole
'	- IsDirectoryInRole
'	- IsKeywordInRole
'	- IsRoleInGroup
'	- ListAllKeywords
'	- ListAllPages
'	- ListDirectories
'	- ListAllRoles
'   - ModifyGroup
'	- ModifyRole
'	- RebuildPages
'	- RemoveUserFromGroup
'	- RemoveRoleFromGroup
'	- RemovePageFromRole
'	- RemoveDirectoryFromRole
'	- RemoveKeywordFromRole
'   - UserHasKeywordAccess
'	- UserHasDirectoryAccess
'	- UserHasDirectoryNameAccess
'	- UserHasPageAccess
'	- getKeywordID
'	- setConnection (PRIVATE)
'	- checkEmpty (PRIVATE)
'===============================================================================================================

Class Security
%>
<script language="vbscript" runat="server">
'Declare global variables
	Private g_sConnectionString 'as string
	Private g_sErrorDescription ' as String
	Private g_oReturnedRecordset ' as ADODB.Recordset
	Private g_oConnection ' as ADODB.Connection
	Private g_iUserID ' as integer
	Private g_sUserName ' as string
	Private g_iGroupID ' as integer
	Private g_sGroupName ' as string
	Private g_iDirectoryID ' as integer
	Private g_sDirectoryName ' as string
	Private g_iDirectoryParentID ' as integer
	Private g_iRoleID ' as integer
	Private g_sRoleName ' as string
	Private g_iAccessLevel ' as integer
	Private g_iPageID ' as integer
	Private g_sPageName 'as string
	Private g_iPageDirectoryID 'as integer
	Private g_iKeywordID 'as integer
	Private g_sSort ' as string
	Private g_objHandyADO
</script>
<%

'============================================================
' GENERAL METHODS
'============================================================	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Clears all properties EXCEPT ConnectionString
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: None
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: All properties except ConnectionString are null
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ClearProperties()
		'check the objects to see if they need to be closed before destroying
		if isObject(g_oReturnedRecordset) then
			g_oReturnedRecordset.Close
			set g_oReturnedRecordset = nothing
		end if
		if isObject(g_oConnection) then
			g_oConnection.Close
			set g_oConnection = nothing
		end if	
		'Clear out the non-object properties
		g_sErrorDescription = null
		g_iUserID = null
		g_sUserName = null
		g_iGroupID = null
		g_sGroupName = null
		g_iDirectoryID = null
		g_sDirectoryName = null
		g_iDirectoryParentID = null
		g_iRoleID = null
		g_sRoleName = null
		g_iAccessLevel = null
		g_iPageID = null
		g_sPageName = null
		g_iPageDirectoryID = null
		g_iKeywordID = null	
		g_sSort = null	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Clears the ErrorDescription property
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: None
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  
	' Post-conditions: ErrorDescription property is an empty string
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ClearErrorDescription()
		g_sErrorDescription = ""
	End Function
	
'============================================================
' GROUPS METHODS
'============================================================
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Adds a group to the afxSecurityGroups table
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupName
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs, iRetID
		dim bReturn
		
		bReturn = false

		'Check the GroupName property and return an error if it is not set
		
		if not checkEmpty(g_sGroupName) then 
			g_sErrorDescription = "Group Name not set."
			AddGroup = false
			exit function	
		end if				
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 
			set rs = server.CreateObject("ADODB.Recordset")
			rs.cursorlocation = 3
			'Check to see if a group with the given name exists
			sSQL = " SELECT * from afxSecurityGroups where groupName = '" & g_sGroupName & "'"
			rs.open sSQL, g_oConnection
			if not rs.eof then
				g_sErrorDescription = "Group with the name """ & g_sGroupName & """ already exists"
				AddGroup = false
				exit function
			end if			
			rs.close
			
			'Insert the record
			sSQL = " ap_addGroup '" & g_sGroupName & "'"
			
			rs.Open sSQL, g_oConnection
			if not rs.EOF then
				iRetID = rs.Fields(0).Value
			else
				g_sErrorDescription = "Identity not returned"
			end if	
		else 'Return an error
			g_sErrorDescription = err.Description
			AddGroup = false
			exit function			
		end if

		'If there was an error inserting, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddGroup = false
			exit function
		end if	

		g_iGroupID = iRetID		
		AddGroup = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Deletes a group from the afxSecurityGroups, afxSecurityGroupsRolesX, and afxUsersGroupsX tables
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function DeleteGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			DeleteGroup = false
			exit function	
		end if				
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			'Delete from the afxUsersGroupsX table
			sSQL = " Delete from afxUsersGroupsX where GroupID = " & g_iGroupID
			call g_oConnection.Execute(sSQL)
			'Delete from the afxSecurityGroupsRolesX table
			sSQL = " Delete from afxSecurityGroupsRolesX where GroupID = " & g_iGroupID
			call g_oConnection.Execute(sSQL)
			'Delete from the afxSecurityGroups table
			sSQL = " Delete from afxSecurityGroups where GroupID = " & g_iGroupID
			call g_oConnection.Execute(sSQL)						

		else 'Return an error
			g_sErrorDescription = err.Description
			DeleteGroup = false
			exit function			
		end if

		'If there was an error deleting, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			DeleteGroup = false
			exit function
		end if	

		DeleteGroup = true	
	End Function	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Modifies a group (updates the GroupName)
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID, GroupName
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ModifyGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			ModifyGroup = false
			exit function	
		end if				
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			if g_sGroupName <> "" and not isnull(g_sGroupName) then 'if the GroupName property is set, update the property
				'Update the GroupName
				sSQL = " UPDATE afxSecurityGroups SET GroupName = '"& g_sGroupName &"' where GroupID = " & g_iGroupID
				call g_oConnection.Execute(sSQL)
			else
				g_sErrorDescription = "Group not updated because GroupName was not set."	
			end if
		else 'Return an error
			g_sErrorDescription = err.Description
			ModifyGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			ModifyGroup = false
			exit function
		end if	

		ModifyGroup = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the groups in the database 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: Sort
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetGroups()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the group list
				sSQL = " SELECT * FROM afxSecurityGroups "
				if g_iGroupID <> "" and not isNull(g_iGroupID) then sSQL = sSQL & " WHERE GroupID = " & g_iGroupID
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	
				'set rs = g_objHandyADO.GetDisconnectedRecordset(g_oConnection, sSQL, false)
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					'rs.ActiveConnection = nothing
					set g_oReturnedRecordset = rs
				else
					GetGroups = false
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetGroups = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetGroups = false
			exit function
		end if	

		GetGroups = true	
	End Function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Adds a user to a group
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID, UserID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddUserToGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			AddUserToGroup = false
			exit function	
		end if	
		'Check the UserID property and return an error if it is not set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID not set."
			AddUserToGroup = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Add the record
			sSQL = " INSERT INTO afxUsersGroupsX (UserID, GroupID) VALUES(" & g_iUserID & ", " & g_iGroupID & ") "
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			AddUserToGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddUserToGroup = false
			exit function
		end if	

		AddUserToGroup = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Removes a user from a group
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID, UserID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RemoveUserFromGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			RemoveUserFromGroup = false
			exit function	
		end if	
		'Check the UserID property and return an error if it is not set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID not set."
			RemoveUserFromGroup = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			sSQL = " DELETE FROM afxUsersGroupsX WHERE UserID = " & g_iUserID & " AND GroupID = " & g_iGroupID
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			RemoveUserFromGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			RemoveUserFromGroup = false
			exit function
		end if	

		RemoveUserFromGroup = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Adds a role to a group
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID, RoleID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddRoleToGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			AddRoleToGroup = false
			exit function	
		end if	
		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			AddRoleToGroup = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			'Update the GroupName
			sSQL = " INSERT INTO afxSecurityGroupsRolesX (RoleID, GroupID) VALUES(" & g_iRoleID & ", " & g_iGroupID & ") "
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			AddRoleToGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddRoleToGroup = false
			exit function
		end if	

		AddRoleToGroup = true	
	End Function	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Removes a role from a group
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID, RoleID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RemoveRoleFromGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			RemoveRoleFromGroup = false
			exit function	
		end if	
		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			RemoveRoleFromGroup = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			sSQL = " DELETE FROM afxSecurityGroupsRolesX WHERE RoleID = " & g_iRoleID & " AND GroupID = " & g_iGroupID
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			RemoveRoleFromGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			RemoveRoleFromGroup = false
			exit function
		end if	

		RemoveRoleFromGroup = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the roles for the specified group 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID(REQUIRED), Sort (OPTIONAL)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetRolesInGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			GetRolesInGroup = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the role list
				sSQL =	" SELECT g.GroupID, g.GroupName, r.RoleID, r.RoleName, r.AccessLevel " & _
						" FROM afxSecurityGroupsRolesX grx, " & _
						"		afxSecurityGroups g, " & _
						"		afxSecurityRoles r " & _
						" WHERE g.GroupID = grx.GroupID " & _
						" AND r.RoleID = grx.RoleID " & _
						" AND g.GroupID = " & g_iGroupID
						
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	

				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
				else
					GetRolesInGroup = false
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetRolesInGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetRolesInGroup = false
			exit function
		end if	

		GetRolesInGroup = true	
	End Function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the users for the specified group 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: GroupID(REQUIRED), Sort (OPTIONAL)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetUsersInGroup()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs

		'Check the GroupID property and return an error if it is not set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID not set."
			GetUsersInGroup = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the role list
				sSQL =	" SELECT g.GroupID, g.GroupName, u.UserID, u.UserName " & _
						" FROM afxUsersGroupsX gux, " & _
						"		afxSecurityGroups g, " & _
						"		afxUsersLogin u " & _
						" WHERE g.GroupID = " & g_iGroupID & _
						" AND g.GroupID = gux.GroupID " & _
						" AND u.UserID = gux.UserID " 
						
						
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	

				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
				else
					GetUsersInGroup = false
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetUsersInGroup = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetUsersInGroup = false
			exit function
		end if	

		GetUsersInGroup = true	
	End Function
'============================================================
' ROLE METHODS
'============================================================

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Adds a role to the afxSecurityRoles table
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleName, AccessLevel
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs, iRetID
		dim bReturn
		
		bReturn = false

		'Check the RoleName property and return an error if it is not set
		
		if not checkEmpty(g_sRoleName) then 
			g_sErrorDescription = "Role Name not set."
			AddRole = false
			exit function	
		end if	
		'Check the AccessLevel property and return an error if it is not set
		
		if not checkEmpty(g_iAccessLevel) then 
			g_sErrorDescription = "Access Level not set."
			AddRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 
			set rs = server.CreateObject("ADODB.Recordset")
			rs.cursorlocation = 3
			'Check to see if a group with the given name exists
			sSQL = " SELECT * from afxSecurityRoles where RoleName = '" & g_sRoleName & "'"
			rs.open sSQL, g_oConnection
			if not rs.eof then
				g_sErrorDescription = "Role with the name """ & g_sRoleName & """ already exists"
				AddRole = false
				exit function
			end if			
			rs.close
			
			'Insert the record
			sSQL = " ap_addRole '" & g_sRoleName & "', " & g_iAccessLevel
			
			rs.Open sSQL, g_oConnection
			if not rs.EOF then
				iRetID = rs.Fields(0).Value
			else
				g_sErrorDescription = "Identity not returned"
			end if	
		else 'Return an error
			g_sErrorDescription = err.Description
			AddRole = false
			exit function			
		end if

		'If there was an error inserting, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddRole = false
			exit function
		end if	

		g_iRoleID = iRetID		
		AddRole = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the Roles in the database 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: Sort (Optional)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the Roles.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetRoles()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the Role list
				sSQL = " SELECT * FROM afxSecurityRoles "
				if g_iRoleID <> "" and not isNull(g_iRoleID) then sSQL = sSQL & " WHERE RoleID = " & g_iRoleID
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
				else
					GetRoles = false
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetRoles = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetRoles = false
			exit function
		end if	

		GetRoles = true	
	End Function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Deletes a Role from the afxSecurityRoles, afxSecurityKeywordsRolesX, and afxSecurityDirectoriesRolesX tables
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function DeleteRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			DeleteRole = false
			exit function	
		end if				
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			'Delete from the afxUsersRolesX table
			sSQL = " Delete from afxUsersRolesX where RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)
			'Delete from the afxSecurityRolesRolesX table
			sSQL = " Delete from afxSecurityRolesRolesX where RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)
			'Delete from the afxSecurityRoles table
			sSQL = " Delete from afxSecurityRoles where RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)						

		else 'Return an error
			g_sErrorDescription = err.Description
			DeleteRole = false
			exit function			
		end if

		'If there was an error deleting, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			DeleteRole = false
			exit function
		end if	

		DeleteRole = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Modifies a Role (updates the RoleName and AccessLevel, if provided)
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, RoleName (optional), AccessLevel (optional)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ModifyRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			ModifyRole = false
			exit function	
		end if				

		'Check the RoleName and AccessLevel properties and return an error both are not set
		if not checkEmpty(g_sRoleName) AND not checkEmpty(g_iAccessLevel)then
			g_sErrorDescription = "Neither RoleName or AccessLevel is set."
			ModifyRole = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Updating
				'Start the update statement
				sSQL = " UPDATE afxSecurityRoles " &_
					   " SET RoleName = '" & g_sRoleName &"', " &_
					   " AccessLevel = " & g_iAccessLevel & " " &_
					   " WHERE RoleID = " & g_iRoleID
'				response.write sSQL
				call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			ModifyRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			ModifyRole = false
			exit function
		end if	

		ModifyRole = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Gets the access level for a given RoleID
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID
	'	Parameter Inputs: None
	' Outputs: Integer - AccessLevel of given RoleID.  Returns 0 if no AccessLevel was found.  Returns -1 on error.  Use the ErrorDescription property to see the error
	' Post-conditions: 
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetAccessLevel()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		dim iReturn
		
		iReturn = 0
		
		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			GetAccessLevel = -1
			exit function	
		end if

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 
			'Get the accessLevel
			sSQL = " SELECT AccessLevel FROM afxSecurityRoles WHERE RoleID = " & g_iAccessLevel
			set rs = server.CreateObject("ADODB.Recordset")
			rs.open sSQL, g_oConnection
			if not rs.eof then 'If a record was found, set the return value to the value of the record
				if not checkEmpty(rs.fields(0).value) then 'if the field is not empty
					iReturn = rs.fields(0).value
				else 'if the field is empty
					iReturn = 0
				end if
			else 'If no record was found set the return value to 0
				iReturn = 0
			end if
			rs.close
			set rs = nothing
		else
			g_sErrorDescription = err.Description
			GetAccessLevel = -1
			exit function		
		end if	
		
		GetAccessLevel = iReturn
				
	end function			

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the Pages for the specified Role 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID(REQUIRED), Sort (OPTIONAL)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the Pages.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetPagesInRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			GetPagesInRole = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the role list
				sSQL =	" SELECT r.RoleID, r.RoleName, p.PageID, p.PageName, p.PageDirectoryID, r.AccessLevel " & _
						" FROM afxSecurityPagesRolesX rpx, " & _
						"		afxSecurityRoles r, " & _
						"		afxSecurityPages p " & _
						" WHERE r.RoleID = rpx.RoleID " & _
						" AND p.PageID = rpx.PageID " & _
						" AND r.RoleID = " & g_iRoleID
						
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	

				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
				else
					GetPagesInRole = false
					g_sErrorDescription = "No pages found for RoleID " & g_iRoleID
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetPagesInRole = false
			exit function			
		end if

		'If there was an error getting the recordset, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetPagesInRole = false
			exit function
		end if	

		GetPagesInRole = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the Directories for the specified Role 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID(REQUIRED), Sort (OPTIONAL)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the Roles.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetDirectoriesInRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			GetDirectoriesInRole = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the role list
				sSQL =	" SELECT r.RoleID, r.RoleName, d.DirectoryID, d.DirectoryName, d.ParentID, r.AccessLevel " & _
						" FROM afxSecurityDirectoriesRolesX rdx, " & _
						"		afxSecurityRoles r, " & _
						"		afxSecurityDirectories d " & _
						" WHERE r.RoleID = rdx.RoleID " & _
						" AND d.DirectoryID = rdx.DirectoryID " & _
						" AND r.RoleID = " & g_iRoleID
						
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	

				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
				else
					GetDirectoriesInRole = false
					g_sErrorDescription = "No Directories found for RoleID " & g_iRoleID
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetDirectoriesInRole = false
			exit function			
		end if

		'If there was an error getting the recordset, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetDirectoriesInRole = false
			exit function
		end if	

		GetDirectoriesInRole = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Lists all of the Keywords for the specified Role 
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID(REQUIRED), Sort (OPTIONAL)
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the Roles.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetKeywordsInRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			GetKeywordsInRole = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the role list
				sSQL =	" SELECT r.RoleID, r.RoleName, k.KeywordID, k.Keyword, k.KeywordType, r.AccessLevel " & _
						" FROM afxKeywordsRolesX rkx, " & _
						"		afxSecurityRoles r, " & _
						"		afxKeywords k " & _
						" WHERE r.RoleID = rkx.RoleID " & _
						" AND k.KeywordID = rkx.KeywordID " & _
						" AND r.RoleID = " & g_iRoleID
						
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	

				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				
				
				rs.open sSQL
				if not rs.EOF then
					set g_oReturnedRecordset = rs
					bReturn = True
				else
					GetKeywordsInRole = false
					g_sErrorDescription = "No Keywords found for RoleID " & g_iRoleID
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			GetKeywordsInRole = false
			exit function			
		end if

		'If there was an error getting the recordset, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			GetKeywordsInRole = false
			exit function
		end if	

		GetKeywordsInRole = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Adds a Page to a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, PageID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddPageToRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			AddPageToRole = false
			exit function	
		end if	
		'Check the PageID property and return an error if it is not set
		if not checkEmpty(g_iPageID) then
			g_sErrorDescription = "PageID not set."
			AddPageToRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Add the page
			sSQL = " INSERT INTO afxSecurityPagesRolesX (PageID, RoleID) VALUES(" & g_iPageID & ", " & g_iRoleID & ") "
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			AddPageToRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddPageToRole = false
			exit function
		end if	

		AddPageToRole = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Adds a Directory to a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, DirectoryID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddDirectoryToRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			AddDirectoryToRole = false
			exit function	
		end if	
		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID not set."
			AddDirectoryToRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Add the Directory
			sSQL = " INSERT INTO afxSecurityDirectoriesRolesX (DirectoryID, RoleID) VALUES(" & g_iDirectoryID & ", " & g_iRoleID & ") "
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			AddDirectoryToRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddDirectoryToRole = false
			exit function
		end if	

		AddDirectoryToRole = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Adds a Keyword to a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, KeywordID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function AddKeywordToRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			AddKeywordToRole = false
			exit function	
		end if	
		'Check the KeywordID property and return an error if it is not set
		if not checkEmpty(g_iKeywordID) then
			g_sErrorDescription = "KeywordID not set."
			AddKeywordToRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Add the Keyword
			sSQL = " INSERT INTO afxKeywordsRolesX (KeywordID, RoleID) VALUES(" & g_iKeywordID & ", " & g_iRoleID & ") "
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			AddKeywordToRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			AddKeywordToRole = false
			exit function
		end if	

		AddKeywordToRole = true	
	End Function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Removes a Page from a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, PageID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RemovePageFromRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			RemovePageFromRole = false
			exit function	
		end if	
		'Check the PageID property and return an error if it is not set
		if not checkEmpty(g_iPageID) then
			g_sErrorDescription = "PageID not set."
			RemovePageFromRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			sSQL = " DELETE FROM afxSecurityPagesRolesX WHERE PageID = " & g_iPageID & " AND RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			RemovePageFromRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			RemovePageFromRole = false
			exit function
		end if	

		RemovePageFromRole = true	
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Removes a Directory from a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, DirectoryID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RemoveDirectoryFromRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			RemoveDirectoryFromRole = false
			exit function	
		end if	
		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID not set."
			RemoveDirectoryFromRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			sSQL = " DELETE FROM afxSecurityDirectoriesRolesX WHERE DirectoryID = " & g_iDirectoryID & " AND RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			RemoveDirectoryFromRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			RemoveDirectoryFromRole = false
			exit function
		end if	

		RemoveDirectoryFromRole = true	
	End Function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Description:	Removes a Keyword from a Role
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: RoleID, KeywordID
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function RemoveKeywordFromRole()
		On Error Resume Next	
		
		dim sSQL, bConnectionOpened
		dim bReturn
		
		bReturn = false

		'Check the RoleID property and return an error if it is not set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID not set."
			RemoveKeywordFromRole = false
			exit function	
		end if	
		'Check the KeywordID property and return an error if it is not set
		if not checkEmpty(g_iKeywordID) then
			g_sErrorDescription = "KeywordID not set."
			RemoveKeywordFromRole = false
			exit function	
		end if						
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Start Deleting
			sSQL = " DELETE FROM afxKeywordsRolesX WHERE KeywordID = " & g_iKeywordID & " AND RoleID = " & g_iRoleID
			call g_oConnection.Execute(sSQL)
		else 'Return an error
			g_sErrorDescription = err.Description
			RemoveKeywordFromRole = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			RemoveKeywordFromRole = false
			exit function
		end if	

		RemoveKeywordFromRole = true	
	End Function	
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: addDirectory
	' Description:	Adds a record to the afxSecurityDirectories table with the given DirectoryName and DirectoryParentID.  
	'				Sets the DirectoryID property equal to the DirectoryID of the newly added record.
	' Pre-conditions:
	'	Property Inputs: directoryName, directoryParentID
	'	Parameter Inputs: None 
	' Inputs: 
	' Returns = Boolean
	' --------------------------------------------------------------------------------------------------------------------------------
	public function addDirectory()
		On Error Resume Next	

		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsAddDirectory 'as recordset
		
		bReturn = false

		'Check the Directory Name property and return an error if it is not set
		if not checkEmpty(g_sDirectoryName) then
			g_sErrorDescription = "DirectoryName is not set."
			addDirectory = false
			exit function	
		end if	
		
		'Check the DirectoryParentID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryParentID) then
			g_sErrorDescription = "DirectoryParentID is not set."
			addDirectory = false
			exit function	
		end if	

		'Initialize record set
		set rsAddDirectory = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		set rsAddDirectory.ActiveConnection = g_oConnection
		
		sSQL = "ap_addDirectory '" & g_sDirectoryName & "', " & g_iDirectoryParentID & ",'/admin/" & (g_sDirectoryName) & "'"
		
		rsAddDirectory.Open sSQL
		
		if not rsAddDirectory.EOF then
			bReturn = true
			'set directory id with returned directory id
			g_iDirectoryID = rsAddDirectory.Fields(0).Value
		else'error
			g_iDirectoryID = "" 'Make sure directory id is blank
			g_sErrorDescription = "DirectoryParentID is not set."
			addDirectory = false
		end if
		rsAddDirectory.Close		

		addDirectory = bReturn
		
		set rsAddDirectory = Nothing

	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: deleteDirectory()
	' Description:	
	'				Deletes records from the afxSecurityDirectories, afxSecurityDirectoriesRolesX, afxSecurityPages, and afxSecurityPagesRolesX tables with the given DirectoryID.  
	'				WARNING:  THIS DELETES THE RECORDS OF ALL PAGES ASSOCIATED WITH THE DIRECTORY, AND REMOVES ALL REFERENCES TO THOSE PAGES FROM ALL ROLES.
	' Pre-conditions: 
	'	Property Inputs: 
	'	Parameter Inputs: p_iDirectoryID 
	' Inputs: 
	' Returns :
	'				Boolean  True if directory was successfully deleted, False if errors occurred.  
	'				Check the ErrorDescription property for the contents of the error.
	' --------------------------------------------------------------------------------------------------------------------------------
	public function deleteDirectory(p_iDirectoryID)
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsPage 'as object
		dim iPageID 'as integer
		
		bReturn = false
		
		'initialize object
		set rsPage = server.CreateObject("ADODB.Recordset")

		'Check the DirectoryID parameter and return an error if it is not set
		if not checkEmpty(p_iDirectoryID) then
			g_sErrorDescription = "DirectoryID parameter is not set."
			deleteDirectory = false
			exit function	
		end if	

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then

			set rsPage.ActiveConnection = g_oConnection
		
			'select back all the security pages that exist for this directory
			sSQL = "SELECT * FROM afxSecurityPages WHERE pageDirectoryID = " & p_iDirectoryID
			
			rsPage.Open sSQL
			
			do while not rsPage.EOF
				
				iPageID = rsPage.Fields("pageID").Value
				
				'delete all references from afxSecurityPagesRolesX
				sSQL = "DELETE FROM afxSecurityPagesRolesX WHERE pageID = " & iPageID
				call g_oConnection.Execute(sSQL)
				
				rsPage.MoveNext
			loop
			
			rsPage.Close
			
			sSQL = "DELETE FROM afxSecurityPages WHERE pageDirectoryID = " & p_iDirectoryID
			call g_oConnection.Execute(sSQL)
			
			sSQL = "DELETE FROM afxSecurityDirectoriesRolesX WHERE directoryID = " & p_iDirectoryID
			call g_oConnection.Execute(sSQL)
			
			sSQL = "DELETE FROM afxSecurityDirectories WHERE directoryID = " & p_iDirectoryID
			call g_oConnection.Execute(sSQL)
			
			if err.number <> 0 then
				g_sErrorDescription = err.Description
				bReturn = false
			else
				bReturn = true
			end if
		else
			bReturn = false	
		end if
		
		deleteDirectory = bReturn	
		
		set rsPage = Nothing
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:			GetParentDirectory
	' Description:	Queries the afxSecurityDirectories table for the given DirectoryID.
	'		
	'		
	' Pre-conditions: 
	'	Property Inputs: DirectoryID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :
	'			Integer  The DirectoryID of the parent directory or 0 if the parent directory is not returned or errors occurred.  
	'			Check the ErrorDescription property for the contents of the error.				
	' --------------------------------------------------------------------------------------------------------------------------------
	public function GetParentDirectory()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim iReturn 'as integer
		dim rsDirectory 'as object
		
		iReturn = 0
		
		'initialize object
		set rsDirectory = server.CreateObject("ADODB.Recordset")

		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			getParentDirectory = 0
			exit function	
		end if	

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
			set rsDirectory.ActiveConnection = g_oConnection
		
			'select the parentid of the directory
			sSQL = "SELECT parentID FROM afxSecurityDirectories WHERE DirectoryID = " &  g_iDirectoryID
			
			rsDirectory.Open sSQL
			
			if err.number <> 0 then

				'error handling
				g_sErrorDescription = err.Description
				GetParentDirectory = 0
				set rsDirectory = Nothing
				exit function
			else
				if not rsDirectory.EOF then
					'sent parentid to the return variable
					iReturn = rsDirectory.fields("parentID").Value
				else
					g_sErrorDescription = "No data was found for the passed directory"
					GetParentDirectory = 0
					rsDirectory.Close
					set rsDirectory = Nothing
					exit function
				end if			
			end if
		else
			'error handling
			GetParentDirectory = 0
			set rsDirectory = Nothing
			exit function
		end if
		
		'Return value
		GetParentDirectory = iReturn
		
		'Cleanup
		set rsDirectory = Nothing		

	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		getChildren()	
	' Description:	Queries afxSecurityDirectories table for all of the Directories whose ParentID is equal to 
	'				the given DirectoryID.  Creates a recordset in the ReturnedRecordset property containing the 
	'				following fields:  DirectoryID, DirectoryName.
	'		
	' Pre-conditions: 
	'	Property Inputs:	DirectoryID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :
	'			Boolean  True if the list was successfully fetched AND contains results, 
	'			False if errors occurred or no records were fetched.  
	'			Check the ErrorDescription property for the contents of the error.  
	'			Access the results through the ReturnedRecordset property.
	'			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getChildren()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsDirectory 'as object
		
		bReturn = false
		
		'initialize object
		set rsDirectory = server.CreateObject("ADODB.Recordset")

		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			getChildren = false
			exit function	
		end if	

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsDirectory.ActiveConnection = g_oConnection
			rsDirectory.CursorLocation = 3
			
			'select back the directories for which the set directory id is the parent id
			sSQL = "SELECT directoryID, directoryName FROM afxSecurityDirectories WHERE parentID = " &  g_iDirectoryID
			
			rsDirectory.Open sSQL 
			
			set rsDirectory.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				getChildren = false
				set rsDirectory = Nothing
				exit function
			else
				if rsDirectory.EOF then
					bReturn = false
					g_sErrorDescription = "No Directories were found with the passed directory as a parent directory" 
				else
					set g_oReturnedRecordset = rsDirectory
					bReturn = true
				end if
			end if
		else
			'error handling
			getChildren = false
			set rsDirectory = Nothing
			exit function
		end if
		
		'Return value
		getChildren = bReturn
		
		'Cleanup
		set rsDirectory = Nothing	
	end function
	


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		GetPagesInDirectory
	' Description:	Queries afxSecurityPages table for all of the Pages whose PageDirectoryID 
	'				is equal to the given DirectoryID.  Creates a recordset in the ReturnedRecordset 
	'				property containing the following fields:  PageID, PageName.
	'		
	' Pre-conditions: 
	'	Property Inputs: DirectoryID 
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :		Boolean  True if the list was successfully fetched AND contains results, 
	'				False if errors occurred or no records were fetched.  Check the 
	'				ErrorDescription property for the contents of the error.  Access the 
	'				results through the ReturnedRecordset property.
	'			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function GetPagesInDirectory()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsDirectory 'as object
		
		bReturn = false
		
		'initialize object
		set rsDirectory = server.CreateObject("ADODB.Recordset")

		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			GetPagesInDirectory = false
			exit function	
		end if	

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsDirectory.ActiveConnection = g_oConnection
			rsDirectory.CursorLocation = 3
		
			'select all the pages that have the current directory
			sSQL = "SELECT  PageID, PageName FROM afxSecurityPages WHERE PageDirectoryID = " &  g_iDirectoryID
			
			rsDirectory.Open sSQL 
			
			set rsDirectory.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				GetPagesInDirectory = false
				set rsDirectory = Nothing
				exit function
			else
				if rsDirectory.EOF then
					bReturn = false
					g_sErrorDescription = "No pages were found with the passed directory as their directory" 
				else
					set g_oReturnedRecordset = rsDirectory
					bReturn = true
				end if
			end if
		else
			'error handling
			GetPagesInDirectory = false
			set rsDirectory = Nothing
			exit function
		end if
		
		'Return value
		GetPagesInDirectory = bReturn
		
		'Cleanup
		set rsDirectory = Nothing		
	end function	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	ListDirectories	
	' Description:		Queries afxSecurityDirectories table for all of the Directories.  
	'					If the optional DirectoryParentID is set it filters the results 
	'					to only include folders whose ParentID equals the DirectoryParentID.  
	'					Otherwise queries for all directories.  Creates a recordset in the 
	'					ReturnedRecordset property containing the following fields:  
	'					DirectoryID, DirectoryName, ParentID.	
	'		
	' Pre-conditions: 
	'	Optional Property Inputs: DirectoryParentID 
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :			Boolean  True if the list was successfully fetched AND contains results, 
	'					False if errors occurred or no records were fetched.  Check the ErrorDescription 
	'					property for the contents of the error.  Access the results through the 
	'					ReturnedRecordset property.
	'			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function ListDirectories()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsDirectory 'as object
		
		bReturn = false
		
		'initialize object
		set rsDirectory = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsDirectory.ActiveConnection = g_oConnection
			rsDirectory.CursorLocation = 3
		
			
			'select all the directories back with all their information
			sSQL = "SELECT  DirectoryID, DirectoryName, ParentID, path " & _
					"FROM afxSecurityDirectories"

			'select back just those directories that have the passed parent directory id
			if checkEmpty(g_iDirectoryParentID) then
					sSQL = sSQL & " WHERE ParentID = " &  g_iDirectoryParentID
			end if

			rsDirectory.Open sSQL 
			
			set rsDirectory.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				ListDirectories = false
				set rsDirectory = Nothing
				exit function
			else
				if rsDirectory.EOF then
					bReturn = false
					g_sErrorDescription = "No directories were found with the passed directory as their parent directory" 
				else
					set g_oReturnedRecordset = rsDirectory
					bReturn = true
				end if
			end if
		else
			'error handling
			ListDirectories = false
			set rsDirectory = Nothing
			exit function
		end if
		
		'Return value
		ListDirectories = bReturn
		
		'Cleanup
		set rsDirectory = Nothing		
	end function	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				GetDirectoryForPage
	' Description:		Queries the afxSecurityPages table for the given PageID.
	'		
	' Pre-conditions: 
	'	Property Inputs: PageID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :			Integer  The DirectoryID of the pages directory or 0 if the 
	'					pages directory is not returned or errors occurred.  Check 
	'					the ErrorDescription property for the contents of the error.
	'			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function GetDirectoryForPage()	
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim iReturn 'as integer
		dim rsPage 'as object
		
		iReturn = 0
		
		'initialize object
		set rsPage = server.CreateObject("ADODB.Recordset")

		'Check the DirectoryID property and return an error if it is not set
		if not checkEmpty(g_iPageID) then
			g_sErrorDescription = "PageID property is not set."
			GetDirectoryForPage = 0
			exit function	
		end if	

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsPage.ActiveConnection = g_oConnection
			rsPage.CursorLocation = 3
		
			'select the pageDirectoryID for the pass page
			sSQL = "SELECT pageDirectoryID FROM afxSecurityPages WHERE PageID = " &  g_iPageID

			rsPage.Open sSQL 
			
			set rsPage.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				GetDirectoryForPage = 0
				set rsPage = Nothing
				exit function
			else
				if rsPage.EOF then
					iReturn = 0
					g_sErrorDescription = "No data was found for the passed directoryID" 
				else
					iReturn = rsPage.Fields("parentDirectoryID").Value
				end if
			end if
		else
			'error handling
			GetDirectoryForPage = 0
			set rsPage = Nothing
			exit function
		end if
		
		'Return value
		GetDirectoryForPage = iReturn
		
		'Cleanup
		set rsPage = Nothing
	end function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				RebuildPages
	' Description:		Drops all of the pages associated with the given DirectoryID.  
	'					Then uses the File System Object to find all of the pages in 
	'					the given directory and adds them to the table.
	'		
	' Pre-conditions: 
	'	Property Inputs:	DirectoryID 
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :			Boolean  True if pages table was successfully rebuilt, False if errors occurred.  
	'					Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function RebuildPages()	
	'on error resume next
	
	dim bReturn 'as boolean
	dim sSQL 'as string
	dim rsDirectory 'as object
	dim oFS 'as object
	dim bConnectionOpened 'as boolean
	dim sPath 'as string
	dim oFolder 'as object
	dim collFile 'as object
	dim oFile' as object
	dim rsTmp 'as object
	dim rsPage 'as object
	dim Init 'as boolean
	
		'initialize variables
		set oFS = server.CreateObject("scripting.FileSystemObject")
		set rsDirectory = server.CreateObject("ADODB.Recordset")
		set rsPage = server.CreateObject("ADODB.Recordset")
		set rsTmp = server.CreateObject("ADODB.Recordset")
		
	

		bReturn = false
		
		'check if the directoryid is set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			RebuildPages = false
			set oFS = Nothing
			set rsDirectory = Nothing
			set rsTmp = Nothing
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
		set rsPage.ActiveConnection = g_oConnection
		set rsDirectory.ActiveConnection = g_oConnection
		set rsTmp.ActiveConnection = g_oConnection	
	
		rsDirectory.CursorLocation = 3
		rsPage.CursorLocation = 3
		
			'get the directory path for the directoryid
			sSQL = "SELECT DirectoryID, DirectoryName, ParentID, Path " & _
					"FROM afxSecurityDirectories " & _
					"WHERE directoryID = " & g_iDirectoryID
			
			rsDirectory.Open sSQL
		
			if not rsDirectory.EOF then
				sPath = server.MapPath(rsDirectory.Fields("Path").Value)

				if oFS.FolderExists(sPath) then
	
					Set oFolder = oFS.GetFolder(sPath)  
					Set collFile = oFolder.Files 
					
					'drop all the pages associated with that directory that are orphaned
					sSQL = "SELECT pageID, pageName FROM afxSecurityPages WHERE pageDirectoryID = " & rsDirectory("DirectoryID")
					rsPage.Open sSQL				

					' For each Record:
					do while not rsPage.EOF
										
						bInIt = False
						for each oFile in collFile
							if LCase(oFile.Name) = LCase(rsPage("pagename")) then 
								bInIt = True
								exit for
							end if
						next
						
						if not bInIt then
							' Delete the record:
							sSQL = "delete from afxSecurityPages where pageID = " & rsPage("pageID")
							g_oConnection.Execute(sSQL)
							sSQL = "delete from afxSecurityPagesRolesX where pageID = " & rsPage("pageID")
							g_oConnection.Execute(sSQL)
						end if
										
						' Next Record:
						rsPage.MoveNext
					Loop
					rsPage.Close
				
			
				'use the file system object to fill all the pages in that directory

					For Each oFile in collFile
						' Check for Existence in the pages table:
						sSQL = "SELECT pageName FROM afxSecurityPages WHERE UPPER(pageName) = '" & UCase(oFile.Name) & "'"
					   
						rsTmp.Open sSQL
					   
						if rsTmp.EOF then
							sSQL = "INSERT INTO afxSecurityPages(PageDirectoryID,pageName) " & _
									"VALUES (" & g_iDirectoryID & ",'" & oFile.name & "')"
					    
							g_oConnection.Execute(sSQL)
						end if
						rsTmp.Close
					    
					Next   

				end if
			
				if err.number <> 0 then
					'error handling
					g_sErrorDescription = err.Description
					RebuildPages = false
					set rsDirectory = Nothing
					set rsPage = Nothing
					set rsTmp = Nothing
					exit function
				else
					bReturn = true
				end if
			end if
			rsDirectory.Close
		else
			'error handling
			RebuildPages = false
			set oFS = Nothing
			set rsDirectory = Nothing
			set rsTmp = Nothing
			exit function
		end if
		
		'Return data
		RebuildPages = bReturn
		
		'cleanup
		set oFS = Nothing
		set rsDirectory = Nothing
		set rsTmp = Nothing
	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		ListAllPages		
	' Description:	Queries afxSecurityPages table for all of the Pages.  
	'				Creates a recordset in the ReturnedRecordset property 
	'				containing the following fields:  PageID, PageName, PageDirectoryID.  
	'				The recordset is sorted by PageDirectoryID by default.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the list was successfully fetched AND contains results, 
	'			False if errors occurred or no records were fetched.  
	'			Check the ErrorDescription property for the contents of the error.  
	'			Access the results through the ReturnedRecordset property.		
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function ListAllPages()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsPages 'as object
		
		bReturn = false
		
		'initialize object
		set rsPages = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsPages.ActiveConnection = g_oConnection
			rsPages.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT PageID, PageDirectoryID, PageName FROM afxSecurityPages"
			
			rsPages.Open sSQL 
			
			set rsPages.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				ListAllPages = false
				set rsPages = Nothing
				exit function
			else
				if rsPages.EOF then
					bReturn = false
					g_sErrorDescription = "No Pages were found" 
				else
					set g_oReturnedRecordset = rsPages
					bReturn = true
				end if
			end if
		else
			'error handling
			ListAllPages = false
			set rsPages = Nothing
			exit function
		end if
		
		'Return value
		ListAllPages = bReturn
		
		'Cleanup
		set rsPages = Nothing		
	end function




	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	IsPageInRole	
	' Description: Check the afxSecurityPagesRolesX if the passed page has the passed role.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	RoleID, PageID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'			Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsPageInRole()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsPages 'as object
		
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			IsPageInRole = false
			exit function	
		end if	
		'check if the pageid is set
		if not checkEmpty(g_iPageID) then
			g_sErrorDescription = "PageID property is not set."
			IsPageInRole = false
			exit function	
		end if	
		
		'initialize object
		set rsPages = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsPages.ActiveConnection = g_oConnection
			rsPages.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT roleID FROM afxSecurityPagesRolesX WHERE roleID = " & g_iRoleID & " AND pageID = " & g_iPageID
			rsPages.Open sSQL 
			
			set rsPages.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				IsPageInRole = false
				set rsPages = Nothing
				exit function
			else
				if rsPages.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsPages.Close
		else
			'error handling
			IsPageInRole = false
			set rsPages = Nothing
			exit function
		end if
		
		'Return value
		IsPageInRole = bReturn
		
		'Cleanup
		set rsPages = Nothing		
	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	IsDirectoryInRole	
	' Description: Check the afxSecurityDirectoriesRolesX if the passed page has the passed role.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	DirectoryID, PageID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'			Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsDirectoryInRole()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsDirectories 'as object
		
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			IsDirectoryInRole = false
			exit function	
		end if	
		'check if the directoryid is set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			IsDirectoryInRole = false
			exit function	
		end if	
		
		'initialize object
		set rsDirectories = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsDirectories.ActiveConnection = g_oConnection
			rsDirectories.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT roleID FROM afxSecurityDirectoriesRolesX WHERE roleID = " & g_iRoleID & " AND directoryID = " & g_iDirectoryID
			rsDirectories.Open sSQL 
			
			set rsDirectories.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				IsDirectoryInRole = false
				set rsDirectories = Nothing
				exit function
			else
				if rsDirectories.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsDirectories.Close
		else
			'error handling
			IsDirectoryInRole = false
			set rsDirectories = Nothing
			exit function
		end if
		
		'Return value
		IsDirectoryInRole = bReturn
		
		'Cleanup
		set rsDirectories = Nothing		
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	IsKeywordInRole	
	' Description: Check the afxKeywordsRolesX if the passed page has the passed role.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	KeywordID, PageID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'			Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsKeywordInRole()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsKeywords 'as object
		
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			IsKeywordInRole = false
			exit function	
		end if	
		'check if the DirectoryID is set
		if not checkEmpty(g_iKeywordID) then
			g_sErrorDescription = "KeywordID property is not set."
			IsKeywordInRole = false
			exit function	
		end if	
		
		
		'initialize object
		set rsKeywords = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsKeywords.ActiveConnection = g_oConnection
			rsKeywords.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT roleID FROM afxKeywordsRolesX WHERE roleID = " & g_iRoleID & " AND keywordID = " & g_iKeywordID

			rsKeywords.Open sSQL 
			
			set rsKeywords.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				IsKeywordInRole = false
				set rsKeywords = Nothing
				exit function
			else
				if rsKeywords.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsKeywords.Close
		else
			'error handling
			IsKeywordInRole = false
			set rsKeywords = Nothing
			exit function
		end if
		
		'Return value
		IsKeywordInRole = bReturn
		
		'Cleanup
		set rsKeywords = Nothing		
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		DeleteRolesFromPages
	' Description: Delete all the pages for the passed role afxSecurityPagesRolesX.
	'		
	' Pre-conditions: 
	'	Property Inputs:	roleID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	: Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'				Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function DeleteRolesFromPages()
	On Error Resume Next	
	dim sSQL 'as string
	dim bConnectionOpened 'as boolean
	dim bReturn 'as boolean
	
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			DeleteRolesFromPages = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
	
			sSQL = "DELETE FROM afxSecurityPagesRolesX WHERE roleID = " & g_iRoleID

			g_oConnection.Execute(sSQL)

		end if
	
		if err.number <> 0 then
		'error handling
			g_sErrorDescription = err.Description
			bReturn = false
		else
			bReturn = true
		end if
		
		DeleteRolesFromPages = bReturn
	
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		DeleteRolesFromDirectories	
	' Description:	Delete all the directories for the passed role afxSecurityDirectoriesRolesX.
	'		
	' Pre-conditions: 
	'	Property Inputs:	roleID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	: Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'				Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function DeleteRolesFromDirectories()
	On Error Resume Next	
	dim sSQL 'as string
	dim bConnectionOpened 'as boolean
	dim bReturn 'as boolean
	
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			DeleteRolesFromDirectories = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
	
			sSQL = "DELETE FROM afxSecurityDirectoriesRolesX WHERE roleID = " & g_iRoleID

			g_oConnection.Execute(sSQL)

		end if
	
		if err.number <> 0 then
		'error handling
			g_sErrorDescription = err.Description
			bReturn = false
		else
			bReturn = true
		end if

		DeleteRolesFromDirectories = bReturn
	
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		DeleteRolesFromKeywords	
	' Description:	Delete all the keywords for the passed role from afxKeywordsRolesX.
	'		
	' Pre-conditions: 
	'	Property Inputs:	roleID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	 Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'				Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function DeleteRolesFromKeywords()
	On Error Resume Next	
	dim sSQL 'as string
	dim bConnectionOpened 'as boolean
	dim bReturn 'as boolean
	
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			DeleteRolesFromKeywords = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
	
			sSQL = "DELETE FROM afxKeywordsRolesX WHERE roleID = " & g_iRoleID

			g_oConnection.Execute(sSQL)

		end if
	
		if err.number <> 0 then
		'error handling
			g_sErrorDescription = err.Description
			bReturn = false
		else
			bReturn = true
		end if
		
		DeleteRolesFromKeywords = bReturn
	
	end function




	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	ListAllKeywords
	' Description: Lists all of the keywords
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: Sort
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ListAllKeywords()
		'On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		dim bReturn 'as boolean
		
		bReturn = false
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the keywords list
				sSQL = " SELECT * FROM afxKeywords "
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	
				'set rs = g_objHandyADO.GetDisconnectedRecordset(g_oConnection, sSQL, false)
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					bReturn = True
					'rs.ActiveConnection = nothing
					set g_oReturnedRecordset = rs
				else
					bReturn = false
					g_sErrorDescription = "No Keywords were found"
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			ListAllKeywords = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			ListAllKeywords = false
			exit function
		end if	

		ListAllKeywords = bReturn	
	End Function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	ListAllUsers
	' Description: Lists all of the users
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: Sort
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ListAllUsers()
		'On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		dim bReturn 'as boolean
		
		bReturn = false
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the keywords list
				sSQL = " SELECT * FROM afxUsersLogin "
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	
				'set rs = g_objHandyADO.GetDisconnectedRecordset(g_oConnection, sSQL, false)
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					bReturn = True
					'rs.ActiveConnection = nothing
					set g_oReturnedRecordset = rs
				else
					bReturn = false
					g_sErrorDescription = "No Users were found"
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			ListAllUsers = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			ListAllUsers = false
			exit function
		end if	

		ListAllUsers = bReturn	
	End Function


	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	IsUserInGroup	
	' Description: Check the afxUsersGroupX if the passed User has the passed Group.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	userID, GroupID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'			Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsUserInGroup()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsUsers 'as object
		
		bReturn = false
		
		'check if the userid is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID property is not set."
			IsUserInGroup = false
			exit function	
		end if	
		'check if the GroupID is set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID property is not set."
			IsUserInGroup = false
			exit function	
		end if	
		
		
		'initialize object
		set rsUsers = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsUsers.ActiveConnection = g_oConnection
			rsUsers.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT userID FROM afxUsersGroupsX WHERE userID = " & g_iUserID & " AND GroupID = " & g_iGroupID

			rsUsers.Open sSQL 
			
			set rsUsers.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				IsUserInGroup = false
				set rsUsers = Nothing
				exit function
			else
				if rsUsers.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsUsers.Close
		else
			'error handling
			IsUserInGroup = false
			set rsUsers = Nothing
			exit function
		end if
		
		'Return value
		IsUserInGroup = bReturn
		
		'Cleanup
		set rsUsers = Nothing		
	end function	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		DeleteUsersFromGroup	
	' Description:	Delete all the Users for the passed Group from afxUsersGroupsX.
	'		
	' Pre-conditions:
	'	Property Inputs:	groupId
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	 Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'				Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function DeleteUsersFromGroup()
	On Error Resume Next	
	dim sSQL 'as string
	dim bConnectionOpened 'as boolean
	dim bReturn 'as boolean
	
		bReturn = false
		
		'check if the groupid is set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID property is not set."
			DeleteUsersFromGroup = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
	
			sSQL = "DELETE FROM afxUsersGroupsX WHERE groupID = " & g_iGroupID

			g_oConnection.Execute(sSQL)

		end if
	
		if err.number <> 0 then
		'error handling
			g_sErrorDescription = err.Description
			bReturn = false
		else
			bReturn = true
		end if
		
		DeleteUsersFromGroup = bReturn
	
	end function	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	ListAllRoles
	' Description: Lists all of the roles
	' Pre-conditions: None
	' Inputs: 
	'	Optional  Property Inputs: Sort
	'	Parameter Inputs: None
	' Outputs: Boolean - True if successful, False if error.  Use the ErrorDescription property to see the error.  On success
	'						fills the ReturnRecordset property with a recordset of all the groups.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function ListAllRoles()
		'On Error Resume Next	
		
		dim sSQL, bConnectionOpened, rs
		dim bReturn 'as boolean
		
		bReturn = false
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the keywords list
				sSQL = " SELECT * FROM afxSecurityRoles "
				if g_sSort <> "" and not isnull(g_sSort) then
					sSQL = sSQL & " ORDER BY " & g_sSort
				end if	
				'set rs = g_objHandyADO.GetDisconnectedRecordset(g_oConnection, sSQL, false)
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					bReturn = True
					'rs.ActiveConnection = nothing
					set g_oReturnedRecordset = rs
				else
					bReturn = false
					g_sErrorDescription = "No Roles were found"
					rs.Close
					set rs = nothing
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			ListAllRoles = false
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			ListAllRoles = false
			exit function
		end if	

		ListAllRoles = bReturn	
	End Function	
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	IsRoleInGroup	
	' Description: Check the afxSecurityGroupsRolesX if the passed User has the passed Group.	
	'		
	' Pre-conditions: 
	'	Property Inputs:	roleID, GroupID
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'			Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsRoleInGroup()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsRoles 'as object
		
		bReturn = false
		
		'check if the roleid is set
		if not checkEmpty(g_iRoleID) then
			g_sErrorDescription = "RoleID property is not set."
			IsRoleInGroup = false
			exit function	
		end if	
		'check if the GroupID is set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID property is not set."
			IsRoleInGroup = false
			exit function	
		end if	
		
		
		'initialize object
		set rsRoles = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsRoles.ActiveConnection = g_oConnection
			rsRoles.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT roleID FROM afxSecurityGroupsRolesX WHERE roleID = " & g_iRoleID & " AND GroupID = " & g_iGroupID

			rsRoles.Open sSQL 
			
			set rsRoles.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				IsRoleInGroup = false
				set rsRoles = Nothing
				exit function
			else
				if rsRoles.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsRoles.Close
		else
			'error handling
			IsRoleInGroup = false
			set rsRoles = Nothing
			exit function
		end if
		
		'Return value
		IsRoleInGroup = bReturn
		
		'Cleanup
		set rsRoles = Nothing		
	end function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		DeleteRolesFromGroup	
	' Description:	Delete all the Roles for the passed Group from afxUsersGroupsX.
	'		
	' Pre-conditions:
	'	Property Inputs:	groupId
	'	Parameter Inputs: 
	' Inputs: 
	' Returns :	 Boolean  True if the page is in the role, False if errors occurred or  the page is not in the role.  
	'				Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function DeleteRolesFromGroup()
	On Error Resume Next	
	dim sSQL 'as string
	dim bConnectionOpened 'as boolean
	dim bReturn 'as boolean
	
		bReturn = false
		
		'check if the groupid is set
		if not checkEmpty(g_iGroupID) then
			g_sErrorDescription = "GroupID property is not set."
			DeleteRolesFromGroup = false
			exit function	
		end if	
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
	
			sSQL = "DELETE FROM afxSecurityGroupsRolesX WHERE groupID = " & g_iGroupID

			g_oConnection.Execute(sSQL)

		end if
	
		if err.number <> 0 then
		'error handling
			g_sErrorDescription = err.Description
			bReturn = false
		else
			bReturn = true
		end if
		
		DeleteRolesFromGroup = bReturn
	
	end function
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		UserHasKeywordAccess
	' Description: Checks these tables afxUsersGroupsX, afxSecurityGroupsRolesX, afxKeywordsRolesX , 
	'				afxSecurityRoles for if the userID has access to the keywordID.  
	'				Retrieves the accesslevel for the passed data .  
	'				If passed minimum access level is greater then or equal to the access 
	'				level the true is returned.
	'		
	' Pre-conditions: 
	'	Property Inputs:	userID, KeywordID, connectionString
	'	Parameter Inputs:	MinimumAccessLevel
	' Inputs: 
	' Returns : Boolean  True if the user has access, False if errors occurred or  the user does not have access. 
	'					Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function UserHasKeywordAccess(p_iMinimumAccessLevel)
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsUsers 'as object
		dim iAccessLevel 'as integer
		
		bReturn = false
		
		'check if the MinimumAccessLevel is set
		if not checkEmpty(p_iMinimumAccessLevel) then
			g_sErrorDescription = "MinimumAccessLevel parameter is not set."
			UserHasKeywordAccess = false
			exit function	
		end if	
		'check if the userid is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID property is not set."
			UserHasKeywordAccess = false
			exit function	
		end if	
		'check if the KeywordID is set
		if not checkEmpty(g_iKeywordID) then
			g_sErrorDescription = "KeywordID property is not set."
			UserHasKeywordAccess = false
			exit function	
		end if	
		
		'initialize object
		set rsUsers = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsUsers.ActiveConnection = g_oConnection
			rsUsers.CursorLocation = 3
			
			'pull back all the pages in the database

			sSQL =	"SELECT distinct(AUGX.UserID ) , ASR.AccessLevel " & _
					"FROM	afxUsersGroupsX AUGX INNER JOIN " & _ 
					"		afxSecurityGroupsRolesX ASGRX ON (AUGX.UserID = " & g_iUserID & " AND AUGX.GroupID = ASGRX.GroupID) INNER JOIN " & _
					"		afxKeywordsRolesX AKRX ON (ASGRX.RoleID = AKRX.RoleID AND AKRX.KeywordID = " & g_iKeywordID & ")INNER JOIN " & _
					"		afxSecurityRoles ASR ON (ASR.RoleID = ASGRX.RoleID) " & _
					"ORDER BY accessLevel DESC"
			'Response.Write(sSQL)
			'Response.End

			rsUsers.Open sSQL 
			
			set rsUsers.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				UserHasKeywordAccess = false
				set rsUsers = Nothing
				exit function
			else
				if rsUsers.EOF then
					bReturn = false
				else
					iAccessLevel = rsUsers.Fields("AccessLevel").Value
					if cint(iAccessLevel) >= cint(p_iMinimumAccessLevel) then

						bReturn = true
					end if
				end if
			end if
			
			rsUsers.Close
		else
			'error handling
			UserHasKeywordAccess = false
			set rsUsers = Nothing
			exit function
		end if
		
		'Return value
		UserHasKeywordAccess = bReturn
		
		'Cleanup
		set rsUsers = Nothing		
	end function	
	
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		UserHasDirectoryAccess
	' Description: :  Checks these tables afxUsersGroupsX, afxSecurityGroupsRolesX, afxSecurityDirectoriesRolesX  
	'					for if the userID has access to the directoryID.
	'		
	' Pre-conditions: 
	'	Property Inputs:	userID, DirectoryID, connectionString
	'	Parameter Inputs: 
	' Inputs: 
	' Returns : Boolean  True if the user has access, False if errors occurred or  the user does not have access. 
	'					Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function UserHasDirectoryAccess()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsUsers 'as object
		
		bReturn = false
		
		'check if the userid is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID property is not set."
			UserHasDirectoryAccess = false
			exit function	
		end if	
		'check if the DirectoryID is set
		if not checkEmpty(g_iDirectoryID) then
			g_sErrorDescription = "DirectoryID property is not set."
			UserHasDirectoryAccess = false
			exit function	
		end if	
		
		'initialize object
		set rsUsers = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsUsers.ActiveConnection = g_oConnection
			rsUsers.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT distinct(AUGX.UserID) " & _
					"FROM 	afxUsersGroupsX AUGX INNER JOIN " & _
					"		afxSecurityGroupsRolesX ASGRX ON (AUGX.UserID = " & g_iUserID & " AND AUGX.GroupID = ASGRX.GroupID) INNER JOIN " & _
					"		afxSecurityDirectoriesRolesX ASDRX ON (ASGRX.RoleID = ASDRX.RoleID AND ASDRX.DirectoryID = " & g_iDirectoryID & ")"
			rsUsers.Open sSQL 
			'Response.Write(sSql)

			set rsUsers.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				UserHasDirectoryAccess = false
				set rsUsers = Nothing
				exit function
			else
				if rsUsers.EOF then

					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsUsers.Close
		else
			'error handling
			UserHasDirectoryAccess = false
			set rsUsers = Nothing
			exit function
		end if
		
		'Return value
		UserHasDirectoryAccess = bReturn
		
		'Cleanup
		set rsUsers = Nothing		
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		UserHasPageAccess
	' Description: :  Checks these tables afxUsersGroupsX, afxSecurityGroupsRolesX, afxSecurityDirectoriesRolesX  
	'					for if the userID has access to the directoryID.
	'		
	' Pre-conditions: 
	'	Property Inputs:	userID, DirectoryID, connectionString
	'	Parameter Inputs: 
	' Inputs: 
	' Returns : Boolean  True if the user has access, False if errors occurred or  the user does not have access. 
	'					Check the ErrorDescription property for the contents of the error.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function UserHasPageAccess()
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim bReturn 'as boolean
		dim rsUsers 'as object
		
		bReturn = false
		
		'check if the userid is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID property is not set."
			UserHasPageAccess = false
			exit function	
		end if	
		'check if the PageName is set
		if not checkEmpty(g_sPageName) then
			g_sErrorDescription = "PageName property is not set."
			UserHasPageAccess = false
			exit function	
		end if	
		
		'initialize object
		set rsUsers = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsUsers.ActiveConnection = g_oConnection
			rsUsers.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT DISTINCT AUGX.UserID " & _
					"FROM 	afxUsersGroupsX AUGX INNER JOIN " & _
					"		afxSecurityGroupsRolesX ASGRX ON AUGX.UserID = " & g_iUserID & " AND AUGX.GroupID = ASGRX.GroupID INNER JOIN " & _
					"		afxSecurityPagesRolesX ASPRX ON ASGRX.RoleID = ASPRX.RoleID INNER JOIN " & _
					"		afxSecurityPages ASP ON ASPRX.PageID = ASP.PageID AND ASP.PageName = '" & g_sPageName & "'"
			'Response.Write(sSql)
			rsUsers.Open sSQL 
			
			set rsUsers.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				UserHasPageAccess = false
				set rsUsers = Nothing
				exit function
			else
				if rsUsers.EOF then
					bReturn = false
				else
					bReturn = true
				end if
			end if
			
			rsUsers.Close
		else
			'error handling
			UserHasPageAccess = false
			set rsUsers = Nothing
			exit function
		end if
		
		'Return value
		UserHasPageAccess = bReturn
		
		'Cleanup
		set rsUsers = Nothing		
	end function
	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	getKeywordID()
	' Description: Query the database for keyword id that matches the keyword name passed.
	'		
	' Pre-conditions: 
	'	Property Inputs:	
	'	Parameter Inputs: keywordName
	' Inputs: 
	' Returns : keywordID as integer or 0 If none was found.
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getKeywordID(p_sKeywordName)
		On Error Resume Next	
		dim sSQL 'as string
		dim bConnectionOpened 'as boolean
		dim iReturn 'as integer
		dim rsKeyword 'as object
		
		iReturn = 0
		
		'check if the Keyword is set
		if not checkEmpty(p_sKeywordName) then
			g_sErrorDescription = "Keyword Parameter is not set."
			getKeywordID = false
			exit function	
		end if	
				
		'initialize object
		set rsKeyword = server.CreateObject("ADODB.Recordset")

		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
			set rsKeyword.ActiveConnection = g_oConnection
			rsKeyword.CursorLocation = 3
			
			'pull back all the pages in the database
			sSQL = "SELECT keywordID FROM afxKeywords WHERE keyword = '" & p_sKeywordName & "'"
			
			rsKeyword.Open sSQL 
			
			set rsKeyword.ActiveConnection = Nothing
			
			if err.number <> 0 then
				'error handling
				g_sErrorDescription = err.Description
				getKeywordID = 0
				set rsKeyword = Nothing
				exit function
			else
				if rsKeyword.EOF then
					iReturn = 0
				else
					iReturn = rsKeyword.Fields("keywordID").Value
				end if
			end if
			
			rsKeyword.Close
		else
			'error handling
			getKeywordID = 0
			set rsKeyword = Nothing
			exit function
		end if
		
		'Return value
		getKeywordID = iReturn
		
		'Cleanup
		set rsKeyword = Nothing		
	end function	
	
	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	UserHasKeywordNameAccess
	' Description: 
	'		
	' Pre-conditions: 
	'	Property Inputs: userID	
	'	Parameter Inputs: keywordName, p_iMinimumAccessLevel
	' Inputs: 
	' Returns : 
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	function UserHasKeywordNameAccess(p_sKeywordName, p_iMinimumAccessLevel)
	dim iKeywordID 'as integer
	dim bReturn 'as boolean
		bReturn = false

		'check if the Keyword is set
		if not checkEmpty(p_sKeywordName) then
			g_sErrorDescription = "Keyword Parameter is not set."
			UserHasKeywordNameAccess = false
			exit function	
		end if	
		'check if the UserID is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID Property is not set."
			
			UserHasKeywordNameAccess = false
			exit function	
		end if	

		g_iKeywordID = getKeywordID(p_sKeywordName)

		bReturn = UserHasKeywordAccess(p_iMinimumAccessLevel)
		
		UserHasKeywordNameAccess = bReturn
	
	end function
	


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:	UserHasDirectoryNameAccess
	' Description: 
	'		
	' Pre-conditions: 
	'	Property Inputs: userID	
	'	Parameter Inputs: sPath
	' Inputs: 
	' Returns : 
	'					
	' --------------------------------------------------------------------------------------------------------------------------------

	function UserHasDirectoryNameAccess(p_sPath)
	dim bReturn 'as boolean
	dim aPath 'as array
	dim oFS 'as object
	dim fFile
	dim sParentFolderName 'as object
	dim oFolder 'as object
	dim sDirectoryName 'as string
	dim sSQL 'as string
	dim rsDirectory 'as object
	dim bConnectionOpened 'as boolean
	dim iDirectoryID 'as integer
	
		set rsDirectory = server.CreateObject("ADODB.Recordset")
		
		'check if the UserID is set
		if not checkEmpty(g_iUserID) then
			g_sErrorDescription = "UserID Property is not set."
			UserHasDirectoryNameAccess = false
			exit function	
		end if
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()
		
		if bConnectionOpened then
		
'			p_sPath = server.Mappath(replace(p_sPath,"/","\"))
		
'			set oFS = server.CreateObject("Scripting.FileSystemObject")

			bReturn = false
		
			if len(p_sPath) = 0 or isNull(p_sPath) then
				bReturn = false
			else
				
'				sParentFolderName = oFS.GetParentFolderName(p_sPath)
				
'				set oFolder = oFS.GetFolder(sParentFolderName)
				
'				sDirectoryName = oFolder.Name
				
				set rsDirectory.ActiveConnection = g_oConnection
				rsDirectory.CursorLocation = 3
				
'				sSQL = "SELECT directoryID " & _
'						"FROM afxSecurityDirectories  " & _
'						"WHERE directoryName = '" & sDirectoryName & "'"
						
				sSQL = "SELECT directoryID " & _
						"FROM afxSecurityDirectories  " & _
						"WHERE path = '" & p_sPath & "'"						

				rsDirectory.Open sSQL

				if rsDirectory.eof then
					bReturn = false
				else
					
					g_iDirectoryID = rsDirectory.Fields("directoryID").Value
				
					
					bReturn = UserHasDirectoryAccess()
				
				end if
			end if
		
			if err.number <> 0 then
				bReturn = false
			end if


		end if
		UserHasDirectoryNameAccess = bReturn

	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Description: Returns the roleID for the role name passed in
	' Pre-conditions: None
	' Inputs: 
	'	Property Inputs: None
	'	Parameter Inputs: p_sRoleName
	' Outputs: RoleID.  Use the ErrorDescription property to see the error.
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Function GetRoleIDForRole(p_sRoleName)
		On Error Resume Next
		
		dim sSQL, bConnectionOpened, rs
		
		'Open a connection to the DB
		bConnectionOpened = setConnection()

		GetRoleIDForRole = 0
		
		'if the connection opened successfully
		if bConnectionOpened then 'Get the group list
				sSQL = " SELECT * FROM afxSecurityRoles WHERE RoleName = '" & ScrubForSQL(p_sRoleName) & "'"
				set rs = server.CreateObject("ADODB.Recordset")
				rs.ActiveConnection = g_oConnection
				rs.open sSQL
				if not rs.EOF then
					'rs.ActiveConnection = nothing
					GetRoleIDForRole = rs.Fields("RoleID").Value
				end if	
				rs.Close
				set rs = nothing
		else 'Return an error
			g_sErrorDescription = err.Description
			exit function			
		end if

		'If there was an error updating, report it and exit
		if err.Number <> 0 then 
			g_sErrorDescription = err.Description
			exit function
		end if
	End Function

	' PRIVATE METHODS ==========================================================================================
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				setConnection
	' Description:		Set The connection if one does not exist
	' Inputs:			none
	' Output:			Boolean - True if connection successfully opened, false if error
	' --------------------------------------------------------------------------------------------------------------------------------
	private function setConnection()
		if not isObject(g_oConnection) then
			set g_oConnection = server.CreateObject("ADODB.Connection")
			if len(g_sConnectionString) = 0 then
				g_sErrorDescription = "No Connection String was supplied"
			else
				g_oConnection.ConnectionString = g_sConnectionString
			end if
			g_oConnection.Open
			if err.number <> 0 then
				g_sErrorDescription = err.Description
				setConnection = false
				exit function
			end if
		end if
		setConnection = true
	end function	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: checkEmpty
	' Description: Checks to see if the given item is null
	' Pre-conditions: 
	' Inputs: 
	' Returns = Boolean
	' --------------------------------------------------------------------------------------------------------------------------------
	private function checkEmpty(p_sItem)
		dim bReturn
		bReturn = ""
		
		if len(p_sItem) = 0 or p_sItem = "" or isnull(p_sItem) then
			bReturn = false
		else
			bReturn = true	
		end if
		
		checkEmpty = bReturn
	
	end function

	
	' Runs automatically at Object Instansiation:
	Private Sub Class_Initialize()
		set g_objHandyADO = New HandyADO
	End Sub
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Runs automatically at Object Destruction:
	Private Sub Class_Terminate()
		set g_objHandyADO = Nothing
	End Sub

' 	PROPERTY DEFINITION ======================================================================================

	'ConnectionString
	Public Property Let ConnectionString(p_sText)
		g_sConnectionString = p_sText
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	'ErrorDescription
	Public Property Let ErrorDescription(p_sText)
		g_sErrorDescription = p_sText
	End Property

	Public Property Get ErrorDescription()
		ErrorDescription = g_sErrorDescription
	End Property

	'ReturnedRecordset
	Public Property Set ReturnedRecordset(p_oRS)
		set g_oReturnedRecordset = p_oRS
	End Property

	Public Property Get ReturnedRecordset()
		set ReturnedRecordset = g_oReturnedRecordset
	End Property

	'Connection
	Public Property Set Connection(p_oConn)
		g_oConnection = p_oConn
	End Property

	Public Property Get Connection()
		Connection = g_oConnection
	End Property

	'UserID
	Public Property Let UserID(p_iID)
		g_iUserID = p_iID
	End Property

	Public Property Get UserID()
		UserID = g_iUserID
	End Property

	'UserName
	Public Property Let UserName(p_sText)
		g_sUserName = p_sText
	End Property

	Public Property Get UserName()
		UserName = g_sUserName
	End Property	

	'GroupID
	Public Property Let GroupID(p_iID)
		g_iGroupID = p_iID
	End Property

	Public Property Get GroupID()
		GroupID = g_iGroupID
	End Property

	'GroupName
	Public Property Let GroupName(p_sText)
		g_sGroupName = p_sText
	End Property

	Public Property Get GroupName()
		GroupName = g_sGroupName
	End Property
	
	'DirectoryID
	Public Property Let DirectoryID(p_iID)
		g_iDirectoryID = p_iID
	End Property

	Public Property Get DirectoryID()
		DirectoryID = g_iDirectoryID
	End Property

	'DirectoryName
	Public Property Let DirectoryName(p_sText)
		g_sDirectoryName = p_sText
	End Property

	Public Property Get DirectoryName()
		DirectoryName = g_sDirectoryName
	End Property
	
	'DirectoryParentID
	Public Property Let DirectoryParentID(p_iID)
		g_iDirectoryParentID = p_iID
	End Property

	Public Property Get DirectoryParentID()
		DirectoryParentID = g_iDirectoryParentID
	End Property	

	'RoleID
	Public Property Let RoleID(p_iID)
		g_iRoleID = p_iID
	End Property

	Public Property Get RoleID()
		RoleID = g_iRoleID
	End Property

	'RoleName
	Public Property Let RoleName(p_sText)
		g_sRoleName = p_sText
	End Property

	Public Property Get RoleName()
		RoleName = g_sRoleName
	End Property
	
	'AccessLevel
	Public Property Let AccessLevel(p_iAccessLevel)
		g_iAccessLevel = p_iAccessLevel
	End Property

	Public Property Get AccessLevel()
		AccessLevel = g_iAccessLevel
	End Property	

	'PageID
	Public Property Let PageID(p_iID)
		g_iPageID = p_iID
	End Property

	Public Property Get PageID()
		PageID = g_iPageID
	End Property

	'PageName
	Public Property Let PageName(p_sText)
		g_sPageName = p_sText
	End Property

	Public Property Get PageName()
		PageName = g_sPageName
	End Property
	
	'PageDirectoryID
	Public Property Let PageDirectoryID(p_iID)
		g_iPageDirectoryID = p_iID
	End Property

	Public Property Get PageDirectoryID()
		PageDirectoryID = g_iPageDirectoryID
	End Property

	'KeywordID
	Public Property Let KeywordID(p_iID)
		g_iKeywordID = p_iID
	End Property

	Public Property Get KeywordID()
		KeywordID = g_iKeywordID
	End Property	

	'Sort
	Public Property Let Sort(p_sFieldName)
		g_sSort = p_sFieldName
	End Property

	Public Property Get Sort()
		Sort = g_sSort
	End Property		
	
	'===========================================================================================================
End Class

%>

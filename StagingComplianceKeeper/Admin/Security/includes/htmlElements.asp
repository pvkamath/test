<%
	'--------------------------------------------------------------------------------------------------
	sub BeginHead(p_sTitle, p_bUsesFormValidation, p_nRefreshTime, p_sRefreshURL)
	
		response.write("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
		response.write("<html>")
		
		' Begin head tag:
		response.write("<head>" & vbCrLf)
		response.write PrintTextFile(Application("sDynWebroot") & "includes/designShellHeadInformation.asp")
		response.write("<title>" & Application("sSiteName") & ": " & p_sTitle & "</title>" & vbCrLf)
		
		' Redirect if refresh url and refresh time are specified parameters
		if p_nRefreshTime <> "" and p_sRefreshURL <> "" then
			' Print Meta Refresh Info
			response.write("<META HTTP-EQUIV=""Refresh"" " &_
							"CONTENT=""" & p_nRefreshTime & ";" &_
							"URL=" & p_sRefreshURL & """>" & vbCrLf)
		end if
		
		response.write("<meta NAME=""Xchange Interactive Group"" Content=""XIG"">" & vbCrLf)
		response.write("<meta HTTP-EQUIV=""Content-Type"" CONTENT=""text/html;CHARSET=iso-8859-1"">" & vbCrLf)
		response.write("<META name=""keywords"" content=""culinary,college,cooking,school,Baltimore,hospitality,chef,restaurant,food,cook,Maryland,career,kitchen,educational,hotel,Ireland,Chylinski,foodservice,baking,baker,pastry,beverage,motel,inn,innkeeping,lodging,employment,cuisine,Epicurean,Europe,gourmet,Prudhomme,Shaefer,Phillips,business,campus,student,dining,management,professional,degree,nutrition,recipe,travel,dinner,horticulture,training""> " & vbCrLf)
		response.write("<META name=""description"" content=""Baltimore International College is a regionally accredited, independent college offering specialized baccalaureate and associate degrees through its School of Culinary Arts and School of Business and Management. The college has an urban campus in Baltimore, MD, and Virginia Park, a suburban, historic campus in Virginia, County Cavan, Ireland."">" & vbCrLf)
		response.write("<style>culinary,college,cooking,school,Baltimore,hospitality,chef,restaurant,food,cook,Maryland,career,kitchen,educational,hotel,Ireland,Chylinski,foodservice,baking,baker,pastry,beverage,motel,inn,innkeeping,lodging,employment,cuisine,Epicurean,Europe,gourmet,Prudhomme,Shaefer,Phillips,business,campus,student,dining,management,professional,degree,nutrition,recipe,travel,dinner,horticulture,training</style>" & vbCrLf)
		response.write("<!--//culinary,college,cooking,school,Baltimore,hospitality,chef,restaurant,food,cook,Maryland,career,kitchen,educational,hotel,Ireland,Chylinski,foodservice,baking,baker,pastry,beverage,motel,inn,innkeeping,lodging,employment,cuisine,Epicurean,Europe,gourmet,Prudhomme,Shaefer,Phillips,business,campus,student,dining,management,professional,degree,nutrition,recipe,travel,dinner,horticulture,training//-->" & vbCrLf)
		
		response.write("<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""" & Application("sDynWebroot") & "includes/")
		if Session("browser") = "IE" then
			response.write Application("sDefaultIEStyle")
		else
			response.write Application("sDefaultNSStyle")
		end if
		response.write(""">" & vbCrLf)
			'<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""" & Application("sDynWebroot") & "includes/XIGStyle.css"">	
		response.write("<script language=""JavaScript"" src=""" & Application("sDynWebroot") & "includes/common.js""></script>" & vbCrLf)
		'Response.Write("<script language=""JavaScript"">var preloadFlag = true;preloadImages;</script>")

		if p_bUsesFormValidation then response.write("<script language=""JavaScript"" src=""" & Application("sStandardFormValidationLibrary") & """></script>")
		
		' Add in Author, Company, etc.
	
	end sub
	
	'--------------------------------------------------------------------------------------------------
	sub FinishHeadBeginBody(bShowBackground, bTrimMargins, bShowMenus)
		response.write("</head>" & vbCrLf)
		response.write("<body")
		if bShowBackground then
			response.write(" bgcolor=""#FFFFFF""")
		end if
		' Print Javascript References:
		response.write(" onLoad=""JavaScript:")
		response.write("InitializePage();")
		'if bShowMenus then response.write("createMenus();")
		if bShowMenus then response.write("return true;")
		response.write("""")
		if bShowMenus then response.write(" onResize=""Javascript:if (isNS4) resizeHandler()""")


		if bShowBackground then
			response.write(" background=""" & Application("sDefaultSiteBackground") & """")
		end if
		
		if bTrimMargins then
			'response.write("  style=""margin-top:0;margin-left:1;margin-right:1"" ")
			response.write(" class=""" & Application("sDefaultBodyStylesheetClass") & """")
			response.write(" topmargin=""0"" rightmargin=""0"" bottommargin=""0"" leftmargin=""0"" marginwidth=""0"" marginheight=""0""")
		end if


		'Check to see if this page needs the "onSelect" tag for the text formatting
		strPageName = Request.ServerVariables("PATH_INFO")
		if (instr(1,strPageName, "AddArticleGen") > 0) or (instr(1,strPageName, "EditArticle") > 0) then
			strOnSelect = "DisableFormatting()"
		else
			strOnSelect = ""
		end if
		
		response.write(" onSelectStart=""" & strOnSelect & """")
		response.write(">")
	end sub
	
	'--------------------------------------------------------------------------------------------------
	sub EndPage()
		if Application("sDataSourceName") <> "" then
			g_objDBConn.Close
			set g_objDBConn = Nothing
		end if
		
		response.write("</body>" & vbCrLf & "</html>")
	end sub
	
	' ------------------------------------------------------------------
	sub PrintMetaTags
		dim sSQL, rs_tmp
		
		' Print Site Description:
		sSQL = "select * from site_setting where UCase(key_name) = 'DESCRIPTION'"
		set rs_tmp = objDBConn.Execute(sSQL)
		if not rs_tmp.eof then
			response.write(vbCrLf & "<meta name=""description"" content=""" & replace(rs_tmp("key_value"), """", vbBlank) & """>" & vbCrLf)
		end if
		rs_tmp.Close
		
		' Print Keywords:
		sSQL = "select * from site_keyword order by keyword"
		set rs_tmp = objDBConn.Execute(sSQL)
		
		response.write("<meta name=""keywords"" content=""")
		
		do while not rs_tmp.eof
			response.write(replace(rs_tmp("keyword"), """", vbBlank))
			rs_tmp.MoveNext
			
			if not rs_tmp.eof then response.write(", ")
		Loop
		
		response.write(""">")
		
		' Deinit Objects:
		rs_tmp.Close
		set rs_tmp = Nothing
	end sub
	

%>
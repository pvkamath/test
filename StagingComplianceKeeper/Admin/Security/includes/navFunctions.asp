<%
' Main Subs:
'	- GenerateMenuParameters(p_objDBConn, p_nNavMenuSite)
'	- PrintNavMenuDefaults(p_objDBConn, p_nNavMenuSite)
'	- PrintCoreJavascript()
'	- PrintOptionalJavascript()
'	- PrintMenuStyle()
'
'==========================================================================================================
'
'	USAGE (within <head> tag): 
'		PrintMenuStyle
'
'	USAGE (within Javascript <script> tags):
'		dim g_aMenuToJSArrayIndex()
'		dim g_nNavMenuCount
'		g_nNavMenuCount = 0
'
'		PrintCoreJavascript
'		PrintNavMenuDefaults g_objDBConn, 1
'		GenerateMenuParameters g_objDBConn, 1
'		PrintOptionalJavascript
'
' ==================== MAIN SUBROUTINES AND FUNCTIONS =====================================================
' These subroutines and functions are the ones that are necessary for use of the navMenu Features
'
' GenerateNavigationMenu() --------------------------------------------------------------------------------
'  Summary: This function will print a navigation menu
'  Preconditions: g_objDBConn must be declared as global db connection.
'  Inputs: p_nSelectId -- The ID of the Section you want selected (pass vbBlank for now... to be implemented)
'		 p_nNavMenuSite -- The ID of the Site we want to display a menu for.
'  Outputs: Will call other subroutines to print the Javascript array to the screen in the order specified
'		  in the database.
' ---------------------------------------------------------------------------------------------------------
' Global Variables:
dim g_aMenuToJSArrayIndex()
dim g_nMenuCount
dim g_intStartImage
dim g_intMenuItems
g_nMenuCount = 0
g_intMenuItems = 5

if SHOW_PRO_MENU then
	g_intStartImage = 1
else
	g_intStartImage = 2
end if

'=================================================================================================================
'======= ADMIN FUNCTIONS === ADMIN FUNCTIONS === ADMIN FUNCTIONS === ADMIN FUNCTIONS === ADMIN FUNCTIONS =========
'=================================================================================================================

Sub UpdateMenuOrder(p_objDBConn, p_sTable, p_nId, p_nMenuOrder)
	
	' Set Defaults:
	if p_sTable = "" or isNull(p_sTable) then p_sTable = "navMenu"
	if p_nMenuOrder = "" or isNull(p_nMenuOrder) then p_nMenuOrder = "NULL"

	dim sSQL
	
	' Don't do anything if no ID was passed in:
	if not isNull(p_nId) and p_nId <> "" then

		sSQL = "UPDATE " & p_sTable & " SET menuOrder = " & p_nMenuOrder & " WHERE " & p_sCompareField & " = " & p_nId
		p_objDBConn.Execute(sSQL)
	end if

End Sub

' ------------------------------------------------------------------------------

' Returns a disconnected recordset (not currently used -- depricated):
Function GetNavElementDependancies(p_objDBConn, p_nId, p_sEntity)

	dim sSQL, rs

	' Create a Client side Recordset
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	if p_nId = "" then p_nId = 0

	select case p_sEntity
		case	"navMenu"
			sSQL = "SELECT id FROM navMenu WHERE parent = " & p_nId
		case "navMenuItem"
			sSQL = "SELECT navMenuPopout as id FROM navMenuItem WHERE id = " & p_nId
		case else
			sSQL = "SELECT id FROM navMenu WHERE 1 = 2"
	end select

	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rs.ActiveConnection = Nothing
	
	set GetNavElementDependancies = rs
	set rs = Nothing
End Function

' ------------------------------------------------------------------------------
' The deletion of a Menu Item will just unlink a layer from the navMenuPopout column.  
' Return a Disconnected Recordset of the records that will be affected by this delete.
' If p_bPerformDelete is true the delete will happen even if dependancies exist.
Function DeleteNavMenuItem(p_objDBConn, p_nId, p_bPerformDelete)
	
	dim sSQL, rs, bMenuItemExists, nCurrentMenuOrder, nNavMenu
	bMenuItemExists = True
	nCurrentMenuOrder = 0
	
	' Check Inputs:
	if p_nId = "" or isNull(p_nId) then p_nId = 0
	if p_bPerformDelete = "" or isNull(p_bPerformDelete) then p_bPerformDelete = False

	' Create Recordset:
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	' Find the menuOrder number for the navMenuItem you want to delete:
	sSQL = "SELECT menuOrder, navMenu FROM navMenuItem WHERE id = " & p_nId
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	if not rs.EOF then
		if not IsNull(rs("menuOrder")) and rs("menuOrder") <> "" then nCurrentMenuOrder = cint(rs("menuOrder"))
		nNavMenu = rs("navMenu")
	end if
	rs.Close

	' Find any dependancies:
	sSQL = "SELECT navMenu.id, navMenu.name " _
		& "FROM navMenu, navMenuItem " _
		& "WHERE navMenu.id = navMenuItem.navMenuPopout " _
		& "AND navMenuItem.id = " & p_nId

	' See if the navMenuItem Exists.  If not, give an empty recordset:
	if not NavMenuItemExists(p_objDBConn, p_nId) then 
		sSQL = sSQL & " AND 1 = 2"
		bMenuItemExists = False
	end if

	' Open Recordset:
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rs.ActiveConnection = Nothing

	' If the Menu Item did exist in the DB:
	if bMenuItemExists then

		' If the function is instructed to perform a DELETE on the menuItem:
		if p_bPerformDelete then

			' Delete the MenuItem:
			sSQL = "DELETE FROM navMenuItem WHERE id = " & p_nId
			p_objDBConn.Execute(sSQL)

			' Update the MenuOrder:
			if nCurrentMenuOrder <> 0 then
				if not rs.EOF then
					sSQL = "UPDATE navMenuItem SET menuOrder = menuOrder - 1 WHERE navMenu = " & nNavMenu & " AND menuOrder > " & nCurrentMenuOrder
					p_objDBConn.Execute(sSQL)
				end if
			end if
		end if
	end if

	' Return the Recordset:
	set DeleteNavMenuItem = rs
	set rs = Nothing

End Function

' ------------------------------------------------------------------------------

' The deletion of a Menu will potentially orphan the 
' Return a Disconnected Recordset of the records that will be affected by this delete.
' If p_bPerformDelete is true and dependancies are found, the delete will not occur.  So if any
' records came back in the recordset, a delete didn't occur.
Function DeleteNavMenu(p_objDBConn, p_nId, p_bPerformDelete)
	
	dim sSQL, rs, bMenuExists, nCurrentMenuOrder, nNavMenuSite
	bMenuExists = True
	nCurrentMenuOrder = 0

	' Check Inputs:
	if p_nId = "" or isNull(p_nId) then p_nId = 0
	if p_bPerformDelete = "" or isNull(p_bPerformDelete) then p_bPerformDelete = False

	' Create Recordset:
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	' Find the Current Menu Order:
	sSQL = "SELECT menuOrder, navMenuSite FROM navMenu WHERE id = " & p_nId
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	if not rs.EOF then
		if not isNull(rs("menuOrder")) and rs("menuOrder") <> "" then nCurrentMenuOrder = cint(rs("menuOrder"))
		nNavMenuSite = rs("navMenuSite")
	end if
	rs.Close

	' Find any dependancies:
	sSQL = "SELECT navMenu.id, navMenu.name, navMenu.parent " _
		& "FROM navMenu " _
		& "WHERE navMenu.parent = " & p_nId

	' See if the navMenuItem Exists.  If not, give an empty recordset:
	if not NavMenuExists(p_objDBConn, p_nId) then 
		sSQL = sSQL & " AND 1 = 2"
		bMenuExists = False
	end if

	' Open Recordset:
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rs.ActiveConnection = Nothing

	' If the navMenu did exist in the DB:
	if bMenuExists then

		' If the function is instructed to perform a DELETE on the menuItem:
		if p_bPerformDelete then

			' If no dependancies were found:
			if rs.EOF then

				' Delete the navMenu:
				sSQL = "DELETE FROM navMenu WHERE id = " & p_nId
				p_objDBConn.Execute(sSQL)

				' Update the menuOrders for the remaining items:
				if nCurrentMenuOrder <> 0 then
					if not rs.EOF then
						sSQL = "UPDATE navMenu SET menuOrder = menuOrder - 1 WHERE " _
							& "menuOrder > " & nCurrentMenuOrder & " " _
							& "AND parent "
						if p_nParent = "NULL" then
							sSQL = sSQL & "IS NULL AND navMenuSite = " & nNavMenuSite
						else
							sSQL = sSQL & "= " & rs("parent")
						end if

						p_objDBConn.Execute(sSQL)
					end if
				end if
			end if
		end if
	end if

	' Return the Recordset:
	set DeleteNavMenu = rs
	set rs = Nothing

End Function

' ------------------------------------------------------------------------------

Sub DeleteNavMenuSite(p_objDBConn, p_nId)

	if p_nId = "" or isNull(p_nId) then p_nId = 0

	dim sSQL, rs
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	' Delete all MenuItems related to the menuitems in the site:
	sSQL = "SELECT * FROM navMenu WHERE navMenuSite = " & p_nId
	
	rs.Open sSQL, p_objDBConn, adOpenDynamic, adLockBatchOptimistic
	do while not rs.EOF

		' Remove any navMenuItems related to the current NavMenu:
		sSQL = "DELETE FROM navMenuItem WHERE navMenu = " & rs("id")
		p_objDBConn.Execute(sSQL)
		
		' Delete the navMenu Instance:
		rs.Delete

		' Next Menu:
		rs.MoveNext
	Loop

	' Make any updates and destroy recordset:
	rs.ActiveConnection = p_objDBConn
	rs.UpdateBatch
	rs.Close
	set rs = Nothing

	' Delete the actual navMenuSite:
	sSQL = "DELETE FROM navMenuSite WHERE id = " & p_nId
	p_objDBConn.Execute(sSQL)

	' Delete any proprietary settings for the navMenuSite:
	'sSQL = ""
	'p_objDBConn.Execute(sSQL)

End Sub

' ------------------------------------------------------------------------------

Function InsertOrUpdateNavMenuSite(p_objDBConn, p_nId, p_sName, p_sURL, p_sDefBack, p_sDefOver, p_sDefLength)

	dim sSQL, rs, sFields, sValues
	dim sRetVal

	' Setup Recordset:
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	if p_nId = "" then
		' Insert:
		sFields = "name, URL, defBack, defOver, defLength"
		sValues = "'" & ScrubForSQL(p_sName) & "', " _
				& "'" & ScrubForSQL(p_sURL) & "', " _
				& "'" & ScrubForSQL(p_sDefBack) & "', " _
				& "'" & ScrubForSQL(p_sDefOver) & "', " _
				& "'" & ScrubForSQL(p_sDefLength) & "'"

		sSQL = "INSERT INTO navMenuSite (" & sFields & ") VALUES (" & sValues & ")"
		sRetVal = "Inserted"

		p_objDBConn.Execute(sSQL)
	else
		
		if NavMenuSiteExists(p_objDBConn, p_nId) then
			' Update:
			sSQL = "UPDATE navMenuSite SET " _
				& "name = '" & ScrubForSQL(p_sName) & "', " _
				& "URL = '" & ScrubForSQL(p_sURL) & "', " _
				& "defBack = '" & ScrubForSQL(p_sDefBack) & "', " _
				& "defOver = '" & ScrubForSQL(p_sDefOver) & "', " _
				& "defLength = '" & ScrubForSQL(p_sDefLength) & "' " _
				& "WHERE id = " & p_nId

			sRetVal = "Updated"

			p_objDBConn.Execute(sSQL)
		else
			sRetVal = "Error"
		end if
	end if

	InsertOrUpdateNavMenuSite = sRetVal
	
End Function

' ------------------------------------------------------------------------------

Function InsertOrUpdateNavMenu(p_objDBConn, p_nId, p_sName, p_nParent, p_nVerticalMenu, p_nPopoutIndicator, _
				p_nMarginLeft, p_nMarginTop, p_nWidth, p_sMouseOverColor, p_sBackgroundColor, _
				p_sBorderStyle, p_sTextStyle, p_nNavMenuSite)

	dim sSQL, rs, sFields, sValues
	dim sRetVal
	dim nMenuOrder
	dim nCurrentParent, nCurrentMenuOrder, nNewMenuOrder, bParentChanging

	' Setup Recordset:
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	' If no parent was passed in, set it to NULL:
	if isNull(p_nParent) or p_nParent = "" then p_nParent = "NULL"

	' If the ID was blank
	if p_nId = "" then

		' Determine New Menu Order:
		sSQL = "SELECT max(menuOrder) as maxMenuOrder FROM navMenu WHERE parent "
		if p_nParent = "NULL" then
			sSQL = sSQL & "IS NULL AND navMenuSite = " & p_nNavMenuSite
		else
			sSQL = sSQL & "= " & p_nParent
		end if
		rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly

		if isNull(rs("maxMenuOrder")) or rs("maxMenuOrder") = "" then
			nMenuOrder = 1
		else
			nMenuOrder = cint(rs("maxMenuOrder")) + 1
		end if

		rs.Close

		' Insert new Menu:
		sFields = "name, parent, verticalMenu, popoutIndicator, marginLeft, marginTop, width, " _
				& "mouseOverColor, backgroundColor, borderStyle, textStyle, navMenuSite, menuOrder"
		
		sValues = "'" & ScrubForSQL(p_sName) & "', " _
				& p_nParent & ", " _
				& "'" & ScrubForSQL(p_nVerticalMenu) & "', " _
				& "'" & ScrubForSQL(p_nPopoutIndicator) & "', " _
				& "'" & ScrubForSQL(p_nMarginLeft) & "', " _
				& "'" & ScrubForSQL(p_nMarginTop) & "', " _
				& "'" & ScrubForSQL(p_nWidth) & "', " _
				& "'" & ScrubForSQL(p_sMouseOverColor) & "', " _
				& "'" & ScrubForSQL(p_sBackgroundColor) & "', " _
				& "'" & ScrubForSQL(p_sBorderStyle) & "', " _
				& "'" & ScrubForSQL(p_sTextStyle) & "', " _
				& "'" & ScrubForSQL(p_nNavMenuSite) & "', " _
				& "'" & ScrubForSQL(nMenuOrder) & "'"

		sSQL = "INSERT INTO navMenu (" & sFields & ") VALUES (" & sValues & ")"
		p_objDBConn.Execute(sSQL)

		' Set the value to return:
		sRetVal = "Inserted"

	else
		' Make sure the menu exists:
		if NavMenuExists(p_objDBConn, p_nId) then
			
			nCurrentMenuOrder = 0

			' Check to see if the parent is changing:
			sSQL = "SELECT parent, menuOrder FROM navMenu WHERE id = " & p_nId
			rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
			nCurrentParent = rs("parent")
			nCurrentMenuOrder = rs("menuOrder")
			rs.Close

			bParentChanging = False
			if isNull(nCurrentParent) or nCurrentParent = "" then
				if p_nParent <> "NULL" then bParentChanging = True
			else
				if p_nParent = "NULL" then
					bParentChanging = True
				elseif cint(p_nParent) <> cint(nCurrentParent) then
					bParentChanging = True
				end if
			end if

			' If it has been determined that the parent is changing:
			if bParentChanging then
				' Update the old parent level
				if not isNull(nCurrentMenuOrder) and nCurrentMenuOrder <> "" then
					if cint(nCurrentMenuOrder) <> 0 then
						sSQL = "UPDATE navMenu SET menuOrder = menuOrder - 1 WHERE menuOrder < " & nCurrentMenuOrder & " AND parent "
						if p_nParent = "NULL" then
							sSQL = sSQL & "IS NULL AND navMenuSite = " & p_nNavMenuSite
						else
							sSQL = sSQL & "= " & p_nParent
						end if

						p_objDBConn.Execute(sSQL)
					end if
				end if

				' Find the new max MenuOrder in the new parent level
				sSQL = "SELECT max(menuOrder) maxMenuOrder FROM navMenu WHERE parent "
				if p_nParent = "NULL" then
					sSQL = sSQL & "IS NULL AND navMenuSite = " & p_nNavMenuSite
				else
					sSQL = sSQL & "= " & p_nParent
				end if

				rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
				nNewMenuOrder = 1
				if not rs.EOF and rs("maxMenuOrder") <> "" then nNewMenuOrder = cint(rs("maxMenuOrder")) + 1
				rs.Close

			end if

			' Update:
			sSQL = "UPDATE navMenu SET " _
				& "name = '" & ScrubForSQL(p_sName) & "', " _
				& "parent = " & p_nParent & ", " _
				& "verticalMenu = '" & ScrubForSQL(p_nVerticalMenu) & "', " _
				& "popoutIndicator = '" & ScrubForSQL(p_nPopoutIndicator) & "', " _
				& "marginLeft = '" & ScrubForSQL(p_nMarginLeft) & "', " _
				& "marginTop = '" & ScrubForSQL(p_nMarginTop) & "', " _
				& "width = '" & ScrubForSQL(p_nWidth) & "', " _
				& "mouseOverColor = '" & ScrubForSQL(p_sMouseOverColor) & "', " _
				& "backgroundColor = '" & ScrubForSQL(p_sBackgroundColor) & "', " _
				& "borderStyle = '" & ScrubForSQL(p_sBorderStyle) & "', " _
				& "textStyle = '" & ScrubForSQL(p_sTextStyle) & "', "

			if bParentChanging then sSQL = sSQL & "menuOrder = '" & ScrubForSQL(nNewMenuOrder) & "', "

			sSQL = sSQL & "navMenuSite = '" & ScrubForSQL(p_nNavMenuSite) & "' " _
				& "WHERE id = " & p_nId
			
			' response.write(sSQL)
			p_objDBConn.Execute(sSQL)
			

			' Set the value to return:
			sRetVal = "Updated"
		else
			' Return an error:
			sRetVal = "Error"
		end if

	end if

	set rs = Nothing
	InsertOrUpdateNavMenu = sRetVal

End Function

' ------------------------------------------------------------------------------

Function InsertOrUpdateNavMenuItem(p_objDBConn, p_nId, p_sName, p_sText, p_sURL, p_sTargetFrame, p_nLength, _
							p_sAdditionalSpacing, p_nNavMenuPopout, p_nNavMenu)

	dim sSQL, rs, sFields, sValues
	dim sRetVal
	dim nMenuOrder

	if p_nLength = "" or isNull(p_nLength) then p_nLength = "default"
	
	' Setup Recordset:
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient

	' If the ID was blank
	if p_nId = "" then

		' Determine New Menu Order:
		sSQL = "SELECT max(menuOrder) as maxMenuOrder FROM navMenuItem WHERE navMenu = " & p_nNavMenu
		rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly

		if isNull(rs("maxMenuOrder")) or rs("maxMenuOrder") = "" then
			nMenuOrder = 1
		else
			nMenuOrder = cint(rs("maxMenuOrder")) + 1
		end if

		' Insert new Menu:
		sFields = "name, Text, URL, TargetFrame, Length, AdditionalSpacing, navMenuPopout, " _
				& "navMenu, menuOrder"
		
		sValues = "'" & ScrubForSQL(p_sName) & "', " _
				& "'" & ScrubForSQL(p_sText) & "', " _
				& "'" & ScrubForSQL(p_sURL) & "', " _
				& "'" & ScrubForSQL(p_sTargetFrame) & "', " _
				& "'" & ScrubForSQL(p_nLength) & "', " _
				& "'" & ScrubForSQL(p_sAdditionalSpacing) & "', " _
				& "'" & ScrubForSQL(p_nNavMenuPopout) & "', " _
				& "'" & ScrubForSQL(p_nNavMenu) & "', " _
				& "'" & ScrubForSQL(nMenuOrder) & "'"

		sSQL = "INSERT INTO navMenuItem (" & sFields & ") VALUES (" & sValues & ")"
		p_objDBConn.Execute(sSQL)
		' response.write(sSQL)

		' Set the value to return:
		sRetVal = "Inserted"

	else
		' Make sure the menu exists:
		if NavMenuItemExists(p_objDBConn, p_nId) then

			' Update:
			sSQL = "UPDATE navMenuItem SET " _
				& "name = '" & ScrubForSQL(p_sName) & "', " _
				& "Text = '" & ScrubForSQL(p_sText) & "', " _
				& "URL = '" & ScrubForSQL(p_sURL) & "', " _
				& "TargetFrame = '" & ScrubForSQL(p_sTargetFrame) & "', " _
				& "Length = '" & ScrubForSQL(p_nLength) & "', " _
				& "AdditionalSpacing = '" & ScrubForSQL(p_sAdditionalSpacing) & "', " _
				& "navMenuPopout = '" & ScrubForSQL(p_nNavMenuPopout) & "', " _
				& "navMenu = '" & ScrubForSQL(p_nNavMenu) & "' " _
				& "WHERE id = " & p_nId
			
			' response.write(sSQL)
			p_objDBConn.Execute(sSQL)

			' Set the value to return:
			sRetVal = "Updated"
		else
			' Return an error:
			sRetVal = "Error"
		end if

	end if

	set rs = Nothing
	InsertOrUpdateNavMenuItem = sRetVal

End Function

' ------------------------------------------------------------------------------
Sub GetChildrenOptions(p_objDBConn, p_nStartId, p_nSelectId, p_sSpaces)
	dim rs, sSQL
	dim sSectionId
	
	set rs = Server.CreateObject("ADODB.Recordset")
			
	sSQL = "SELECT * FROM navMenu WHERE parent = " & p_nStartId & " ORDER BY menuOrder"
	' response.write(sSQL)
	rs.CursorLocation = adUseClient
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rs.ActiveConnection = Nothing
	
	do while not rs.EOF
		sSectionId = rs("id")
		
		response.write("<option value=""" & rs("id") & """")
		if not isNull(p_nSelectId) and p_nSelectId <> "" then
			if cint(sSectionId) = cint(p_nSelectId) then response.write(" selected")
		end if
		response.write(">" & p_sSpaces & rs("name") & "</option>" & vbCrLf)
		
		p_sSpaces = p_sSpaces & "&nbsp;&nbsp;&nbsp;&nbsp;"
		call GetChildrenOptions(p_objDBConn, sSectionId, p_nSelectId, p_sSpaces)
		p_sSpaces = "&nbsp;&nbsp;&nbsp;&nbsp;"
		
		rs.MoveNext
	Loop
	
	rs.Close
	set rs = Nothing
	
End Sub

' ------------------------------------------------------------------------------

Sub GenerateNavMenuSelectBox(p_objDBConn, p_sFieldName, p_nSite, p_nSelId, p_sFormStyle, p_bDisabled)
	dim sSQL, rsTmp
	%><select <%
		if p_bDisabled then
			response.write(" onClick=""Javascript:AlertAboutParent();"" onChange=""Javascript:AlertAboutParent();""")
		else
			response.write(" onChange=""Javascript:CheckParent();""")
		end if
	%> name="<%= p_sFieldName %>" id="<%= p_sFieldName %>" class="<%= p_sFormStyle %>">
	<option value="">Nothing</option>
	<%
	sSQL = "select * from navMenu where parent IS NULL and navMenuSite = " & p_nSite

	set rsTmp = Server.CreateObject("ADODB.Recordset")
	rsTmp.CursorLocation = adUseClient
	rsTmp.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly

	do while not rsTmp.EOF
		response.write("<option value=""" & rsTmp("id") & """")
		if p_nSelId <> "" then
			if cint(p_nSelId) = cint(rsTmp("id")) then response.write(" selected")
		end if
		response.write(">" & rsTmp("name") & "</option>")
		
		call GetChildrenOptions(g_objDBConn, rsTmp("id"), p_nSelId, "&nbsp;&nbsp;&nbsp;&nbsp;")
		rsTmp.MoveNext
	Loop

	rsTmp.Close
	set rsTmp = Nothing
	%>
	</select><%

End Sub

'=================================================================================================================
'=== GENERAL FUNCTIONS === GENERAL FUNCTIONS === GENERAL FUNCTIONS === GENERAL FUNCTIONS === GENERAL FUNCTIONS ===
'=================================================================================================================

Function NavMenuSiteExists(p_objDBConn, p_nNavMenuSiteId)
	dim sSQL, rs, bRetVal

	if p_nNavMenuSiteId = "" then p_nNavMenuSiteId = 0

	sSQL = "SELECT count(*) as numValues FROM navMenuSite WHERE id = " & p_nNavMenuSiteId
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	
	if cint(rs("numValues")) > 0 then
		bRetVal = True
	else
		bRetVal = False
	end if

	rs.Close
	set rs = Nothing
	NavMenuSiteExists = bRetVal

End Function

'------------------------------------------------------------------------------------------------

Function NavMenuExists(p_objDBConn, p_nMenuId)
	dim sSQL, rs, bRetVal

	if p_nMenuId = "" then p_nMenuId = 0

	sSQL = "SELECT count(*) as numValues FROM navMenu WHERE id = " & p_nMenuId
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	
	if cint(rs("numValues")) > 0 then
		bRetVal = True
	else
		bRetVal = False
	end if

	rs.Close
	set rs = Nothing
	NavMenuExists = bRetVal

End Function

'------------------------------------------------------------------------------------------------

Function NavMenuItemExists(p_objDBConn, p_nMenuItemId)
	dim sSQL, rs, bRetVal

	if p_nMenuItemId = "" then p_nMenuItemId = 0

	sSQL = "SELECT count(*) as numValues FROM navMenuItem WHERe id = " & p_nMenuItemId
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	
	if cint(rs("numValues")) > 0 then
		bRetVal = True
	else
		bRetVal = False
	end if

	rs.Close
	set rs = Nothing

	NavMenuItemExists = bRetVal

End Function

'------------------------------------------------------------------------------------------------

Function GetField(p_sEnt, p_sField, p_sContraintField, p_sConstraintValue)
	
	dim bError, sRetVal
	if p_sContraintField = "" or isNull(p_sContraintField) then 
		bError = True
		sRetVal = "No ID was passed in."
	end if

	if not bError then
		dim sSQL, rs
		sSQL = "SELECT " & p_sField & " FROM " & p_sEnt & " where " & p_sContraintField & " = " & p_sConstraintValue
	end if

	GetField = sRetVal

End Function

' ScrubForSQL(p_sText) ------------------------------------------------------------------------------------
'  Summary: Will replace single quotes with two single quotes for SQL Scrubbing
'  Inputs: p_sText -- A String
'  Outputs: A scrubbed string.
' ---------------------------------------------------------------------------------------------------------

Function ScrubForSQL(p_sText)
	if isNull(p_sText) or p_sText = "" then
		ScrubForSQL = ""
	else
		ScrubForSQL = Replace(p_sText, "'", "''")
	end if
End Function

' FormatForJSString(p_sText) ------------------------------------------------------------------------------
'  Summary: Will replace double quotes with backslash-double quotes for Javascript String Scrubbing
'  Inputs: p_sText -- A String
'  Outputs: A scrubbed string.
' ---------------------------------------------------------------------------------------------------------

Function FormatForJSString(p_sText, p_sQuote)
	if isNull(p_sText) or p_sText = "" then
		FormatForJSString = ""
	else
		FormatForJSString = Replace(p_sText, p_sQuote, "\" & p_sQuote)
	end if
End Function


'=================================================================================================================
'============ FRONT END FUNCTIONS === FRONT END FUNCTIONS === FRONT END FUNCTIONS === FRONT END FUNCTIONS ======== 
'=================================================================================================================

Sub GenerateNavigationMenu(p_nSelectId, p_nNavMenuSite)
	
	'response.write("<link rel=""stylesheet"" type=""text/css"" href=""" & Application("sDynWebroot") & "includes/navStyle.css"">")
	response.write("<script language=""JavaScript1.2"">")
	PrintCoreJavascript
	PrintNavMenuDefaults g_objDBConn, p_nNavMenuSite
	GenerateMenuParameters g_objDBConn, p_nNavMenuSite
	PrintOptionalJavascript
	response.write("</script>")

End Sub

' GenerateMenuParameters(p_objDBConn, p_nNavMenuSite) -----------------------------------------------------
'  Summary: This function is the main function to be called from this file.  This function will generate
'			Javascript arrays that will be used with the layer functions obtained from 
'			javascript.internet.com (or Jeff's Friend)
'  Inputs: p_objDBConn -- An Open ADO Connection to a database with access to the navMenu Tables.
'		 p_nNaveMenuSite -- The ID of the Site we want to display a menu for.
'  Outputs: Will call other subroutines to print the Javascript array to the screen in the order specified
'		  in the database.
' ---------------------------------------------------------------------------------------------------------
Sub GenerateMenuParameters(p_objDBConn, p_nNavMenuSite)
	dim sSQL, rsMenus

	' Get Root Menu:
	set rsMenus = Server.CreateObject("ADODB.Recordset")
	rsMenus.CursorLocation = adUseClient
	sSQL = "SELECT * FROM navMenu WHERE parent IS NULL AND navMenuSite = " & p_nNavMenuSite
	rsMenus.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rsMenus.ActiveConnection = Nothing

	' If we don't have an empty recordset:
	if not rsMenus.EOF then

		' Add this ID to the Javascript Array Index
		redim preserve g_aMenuToJSArrayIndex(0)
		g_aMenuToJSArrayIndex(0) = rsMenus("id")

		' Create the Root Menu:
		call GenerateMenu(rsMenus("id"), rsMenus("name"), rsMenus("parent"), rsMenus("verticalMenu"), rsMenus("popoutIndicator"), rsMenus("marginLeft"), rsMenus("marginTop"), rsMenus("width"), rsMenus("mouseOverColor"), rsMenus("backgroundColor"), rsMenus("borderStyle"), rsMenus("textStyle"), p_nNavMenuSite)
		
		' Increment the Menu Counter:
		g_nMenuCount = g_nMenuCount + 1

		' Should only be one Record, but just in case, for each record:
		Do While Not rsMenus.EOF
			
			' Generate all Child Menus of the Root Menu
			call GetChildren(p_objDBConn, rsMenus("id"), False)

			' Next Root Menu:
			rsMenus.MoveNext
		Loop
	end if

	' If we did get a result from the first query, then reset the recordset:
	if not rsMenus.BOF then rsMenus.MoveFirst

	' Reset the Menu Counter:
	g_nMenuCount = 0

	' If we didn't have an empty recordset:
	if not rsMenus.EOF then
		
		' Generate the MenuItems for the Root Menu:
		call GenerateMenuItems(p_objDBConn, rsMenus("id"))

		' Increment the Menu Counter:
		g_nMenuCount = g_nMenuCount + 1

		' For each Root Menu:
		Do While Not rsMenus.EOF
			
			' Get all menu Items for all child menus to the root:
			call GetChildren(p_objDBConn, rsMenus("id"), True)

			' Next Menu:
			rsMenus.MoveNext
		Loop
	end if

	' Close the menu Recordset:
	rsMenus.Close
	set rsMenus = Nothing
End Sub

' PrintNavMenuDefaults(p_objDBConn, p_nNavMenuSite) -------------------------------------------------------
'  Summary: This function will declare javascript variables to specify defaults in the menu
'  Inputs: p_objDBConn -- An Open ADO Connection to a database with access to the navMenu Tables.
'		 p_nNavMenuSite -- The ID of the site you want to generate a Menu For.
'  Outputs: Javascript variable to the screen:
' ---------------------------------------------------------------------------------------------------------
Sub PrintNavMenuDefaults(p_objDBConn, p_nNavMenuSite)
	
	dim sSQL, rsNavOptions, rsMenus, rsMenuItems, sQuote
	
	' Open Database Objects:
	set rsNavOptions = Server.CreateObject("ADODB.Recordset")

	' Get Navigation Options:
	sSQL = "SELECT * FROM navConfigOptions WHERE navMenuSite = " & p_nNavMenuSite
	rsNavOptions.CursorLocation = adUseClient
	rsNavOptions.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	rsNavOptions.ActiveConnection = Nothing

%>

var menu = new Array();

<%	' Print Navigation Variables:
	do while not rsNavOptions.EOF 
		select case rsNavOptions("configKey")
			case "defOver"
				sQuote = """"
			case "defBack"
				sQuote = """"
			case else
				sQuote = ""
		end select

		response.write(rsNavOptions("configKey") & " = " & sQuote & FormatForJSString(rsNavOptions("configValue"), """") & sQuote & ";" & vbCrLf)

		rsNavOptions.MoveNext
	Loop 

End Sub

' ---------------------------------------------------------------------------------------------------------
Sub PrintMenuStyle %>
	<LINK REL="stylesheet" TYPE="text/css" HREF="./includes/navStyle.css">
<%
End Sub

' Get Children(p_objDBConn, p_nStartId, p_bGetChildrenMenuItems) ------------------------------------------
'  Summary: This function will Get either the Child Menus or Child MenuItems of the given menu id.
'  Inputs: p_objDBConn -- An Open ADO Connection to a database with access to the navMenu Tables.
'		 p_nStartId -- The ID of the menu you want to get the children of.
'  Outputs: Will call itself recursively to get the entire set of child menus and menuitems.
' ---------------------------------------------------------------------------------------------------------
Sub GetChildren(p_objDBConn, p_nStartId, p_bGetChildrenMenuItems)
	dim rs, sSQL
	dim sSectionId
	dim x, nCount

	' Get all Menus below the given start id that was passed in:
	
		
	sSQL = "SELECT * FROM navMenu WHERE parent = " & p_nStartId & " ORDER BY menuOrder"
	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	set rs.ActiveConnection = Nothing

	' For Each Menu that was found:
	do while not rs.EOF

		' Generate their menu:
		if not p_bGetChildrenMenuItems then
			call GenerateMenu(rs("id"), rs("name"), rs("parent"), rs("verticalMenu"), rs("popoutIndicator"), rs("marginLeft"), rs("marginTop"), rs("width"), rs("mouseOverColor"), rs("backgroundColor"), rs("borderStyle"), rs("textStyle"), p_nStartId)

			' Add the menu to the JavaScript Array Index:
			redim preserve g_aMenuToJSArrayIndex(UBound(g_aMenuToJSArrayIndex) + 1)
			g_aMenuToJSArrayIndex(UBound(g_aMenuToJSArrayIndex)) = rs("id")
		else
			' Generate their Menu Items:
			call GenerateMenuItems(p_objDBConn, rs("id"))
		end if

		' Increment the MenuCounter:
		g_nMenuCount = g_nMenuCount + 1

		' Get any Child Menus to the current menu (recursively):
		call GetChildren(p_objDBConn, rs("id"), p_bGetChildrenMenuItems)
		
		' Next Menu Found:
		rs.MoveNext
		
	Loop
	
	' Destroy Recordset:
	rs.Close
	set rs = Nothing

End Sub


' FindRootMenuId(p_objDBConn, p_nNavMenuId) --------------------------------------------------------------
'  Summary: Not used yet... experimental
'  Inputs: p_objDBConn -- An Open ADO Connection to a database with access to the navMenu Tables.
'		 p_nNavMenuId -- The ID you want to find the root menu for.
'  Outputs: integer id
' ---------------------------------------------------------------------------------------------------------
function FindRootMenuId(p_objDBConn, p_nNavMenuId)
	dim sSQL, rs_tmp
	
	set rs_tmp = Server.CreateObject("ADODB.Recordset")
	sSQL = "select parent from navMenu where id = " & p_nNavMenuId
	rs_tmp.CursorLocation = adUseClient
	rs_tmp.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly
	
	if not rs_tmp.eof then
		' If the parent isn't null or equal to Site Root (1)
		if rs_tmp("parent") <> "" and rs_tmp("parent") <> "1" and rs_tmp("parent") <> 1 then
			sRetVal = FindRootMenuId(rs_tmp("parent"))
		else
			sRetVal = p_nNavMenuId
		end if
	else
		call ShowError("There was an Error in Generating the Side Menu (FindRootMenuId)")
	end if
	
	rs_tmp.Close
	set rs_tmp = Nothing
	
	FindRootMenuId = sRetVal
	
end function

'------------------------------------------------------------------------------------------------
' Will generate a new array to hold a menu layer:
'------------------------------------------------------------------------------------------------
Sub GenerateMenu(p_nId, p_sName, p_nParent, p_bVerticalMenu, p_sPopOutIndicator, p_nMarginLeft, p_nMarginTop, p_nWidth, p_sMouseOverColor, p_sBackgroundColor, p_sBorderStyle, p_sTextStyle, p_nNavMenuSite)

	dim sMouseOverColor
	dim sBackgroundColor
	dim sWidth

	' Determine MouseOverColor
	if isNull(p_sMouseOverColor) or p_sMouseOverColor = "" then
		sMouseOverColor = "''"
	elseif left(LCase(p_sMouseOverColor), 3) = "def" then
		sMouseOverColor = "defOver"
	else
		sMouseOverColor = "'" & p_sMouseOverColor & "'"
	end if

	' Determine Background Color:
	if isNull(p_sBackgroundColor) or p_sBackgroundColor = "" then
		sBackgroundColor = "''"
		'sBackgroundColor = null
	elseif left(lcase(p_sBackgroundColor), 3) = "def" then
		sBackgroundColor = "defBack"
	else
		sBackgroundColor = "'" & p_sBackgroundColor & "'"
	end if
	
	' Determine Width
	sWidth = p_nWidth
	if isNull(p_nWidth) or p_nWidth = "" then sWidth = "0"

	%>
	menu[<%= g_nMenuCount %>] = new Array();
	menu[<%= g_nMenuCount %>][0] = new Menu(<%= Lcase(p_bVerticalMenu) %>, '<%= p_sPopoutIndicator %>', <%= p_nMarginLeft %>, <%= p_nMarginTop %>, <%= sWidth %>, '0', <%= sMouseOverColor %>, <%= sBackgroundColor %>, '<%= p_sBorderStyle %>', '<%= p_sTextStyle %>');
	<%

End Sub

'------------------------------------------------------------------------------------------------
' Will populate an array defined in GenerateMenu for a given menu ID:
'------------------------------------------------------------------------------------------------
Sub GenerateMenuItems(p_objDBConn, p_nMenuId)
	
	dim nCount
	dim sSQL, rs
	dim sLength
	dim sURL
	if p_nMenuId = "" then p_nMenuId = 0
	
	if SHOW_PRO_MENU then
		sSQL = "SELECT * FROM navMenuItem WHERE navMenu = " & p_nMenuId & " ORDER BY menuOrder"
	else
		sSQL = "SELECT * FROM navMenuItem WHERE id <> 1 AND navMenu = " & p_nMenuId & " ORDER BY menuOrder"
	end if

	set rs = Server.CreateObject("ADODB.Recordset")
	rs.CursorLocation = adUseClient
	rs.Open sSQL, p_objDBConn, adOpenStatic, adLockReadOnly

	nCount = 1
	do while not rs.EOF
		sLength = rs("length")
		if isNull(sLength) or sLength = "" then 
			sLength = 0
		elseif left(lcase(sLength), 3) = "def" then 
			sLength = "defLength"
		end if
		
		sURL = rs("URL")
		if isNull(rs("URL")) or trim(rs("URL")) = "" then sURL = "#"
		
	%>menu[<%= g_nMenuCount %>][<%= nCount %>] = new Item('<%= rs("Text") %>', '<%= sURL %>', '<%= rs("TargetFrame") %>', <%= sLength %>, <%= rs("AdditionalSpacing") %>, <%= GetMenuIndex(rs("navMenuPopout")) %>);
	<%
		' Next Record:
		rs.MoveNext
		nCount = nCount + 1
	Loop

End Sub

'------------------------------------------------------------------------------------------------
' Will find the Array Index that a given MenuID is in.
'------------------------------------------------------------------------------------------------
Function GetMenuIndex(p_nMenuId)

	dim nRetVal, nValue, nMenuId, nCount

	nMenuId = p_nMenuId
	nRetVal = 0

	if nMenuId = "" then nMenuId = "0"
	nMenuId = cint(nMenuId)

	if nMenuId <> 0 then
		nCount = 0
		For Each nValue in g_aMenuToJSArrayIndex
			' response.write(vbCrLf & "nMenuId = " & nMenuId & ", nValue = " & nValue & vbCrLf)

			if cint(nValue) = nMenuId then
				nRetVal = nCount
			end if
			
			nCount = nCount + 1
		Next
	end if
	
	GetMenuIndex = nRetVal
End Function


'=================================================================================================================
'============ CORE JAVASCRIPT FUNCTIONS === CORE JAVASCRIPT FUNCTIONS === CORE JAVASCRIPT FUNCTIONS ==============
'=================================================================================================================
' PrintCoreJavascript() ----------------------------------------------------------------------------------
'  Summary: This function will print javascript functions to the screen that are used to generate the 
'			layer based navigation menus.
'  Inputs: None
'  Outputs: Javascript text to the screen:
' ---------------------------------------------------------------------------------------------------------
Sub PrintCoreJavascript()
%>
	// *** CROSS-BROWSER COMPATIBILITY ***
	var isDOM = (document.getElementById ? true : false);
	var isIE  = (document.all ? true: false);
	var isNS4 = (navigator.appName=='Netscape' && !isDOM ? true : false);
	var isIE4 = ((isIE && !isDOM) ? true : false);
	var isDyn = (isDOM || isIE4 || isNS4);


	//var isDOM = (document.getElementById ? true : false); 
	//var isIE4 = ((document.all && !isDOM) ? true : false);
	//var isNS4 = (document.layers ? true : false);
	var bImageRollovers = true

	function getRef(id)
	{
	 return (isDOM ? document.getElementById(id) :
	  (isIE4 ? document.all[id] : document.layers[id]));
	}

	function getSty(id)
	{
	 return (isNS4 ? getRef(id) : getRef(id).style);
	} 


	// *** MOUSEOVER/OUT CONTROL FUNCTIONS ***

	// Hide timeout.
	var popTimer = 0;
	// Arrays holding highlighted menu items.
	var litNow = new Array();

	function popOver(menuNum, itemNum)
	{
	 clearTimeout(popTimer);
	 	 
	 if (bCharts){
		HideDropDowns(false);
		}	
	//Add code here to check what item you're on and only change images on that one.
	
	if (<%=g_intStartImage%> !=1){
		var imageNum = (itemNum + 1)
	}else{
		var imageNum = itemNum	
	}
	
	
	if (<%=ROLLOVER_IMAGES%> ==1){
		if (isNS4){
			if (menuNum==0){
				var i
				
				changeImages('img_'+imageNum, '/media/images/nav_'+imageNum+'-over.gif')
				
				if (document.layers[0].document.img_1){
					if (imageNum != 1){
						if (<%=MENU_SELECTED%> != 1){
							changeImages('img_1', '/media/images/nav_1.gif')
						}
					}
				}
				if (document.layers[0].document.img_2){
					if (imageNum != 2){
						if (<%=MENU_SELECTED%> != 2){	
							changeImages('img_2', '/media/images/nav_2.gif')
						}
					}
				}
				if (document.layers[0].document.img_3){
					if (imageNum != 3){
						if (<%=MENU_SELECTED%> != 3){	
							changeImages('img_3', '/media/images/nav_3.gif')
						}
					}
				}
				if (document.layers[0].document.img_4){
					if (imageNum != 4){
						if (<%=MENU_SELECTED%> != 4){	
							changeImages('img_4', '/media/images/nav_4.gif')
						}
					}
				}
				if (document.layers[0].document.img_5){
					if (imageNum != 5){
						if (<%=MENU_SELECTED%> != 5){	
							changeImages('img_5', '/media/images/nav_5.gif')
						}
					}
				}
									
			}			
		}else{
			if (menuNum==0){
				
				changeImages('img_'+imageNum, '/media/images/nav_'+imageNum+'-over.gif')
				
				var i
				
				if (document.images.img_1){
					if (imageNum != 1){
						if (<%=MENU_SELECTED%> != 1){
							changeImages('img_1', '/media/images/nav_1.gif')
						}
					}
				}
				if (document.images.img_2){
					if (imageNum != 2){
						if (<%=MENU_SELECTED%> != 2){
							changeImages('img_2', '/media/images/nav_2.gif')
						}
					}
				}
				if (document.images.img_3){
					if (imageNum != 3){
						if (<%=MENU_SELECTED%> != 3){
							changeImages('img_3', '/media/images/nav_3.gif')
						}
					}
				}
				if (document.images.img_4){
					if (imageNum != 4){
						if (<%=MENU_SELECTED%> != 4){
							changeImages('img_4', '/media/images/nav_4.gif')
						}
					}
				}
				if (document.images.img_5){
					if (imageNum != 5){
						if (<%=MENU_SELECTED%> != 5){
							changeImages('img_5', '/media/images/nav_5.gif')
						}
					}
				}						
				//for (i = 1 ; i < 6; i++) {
				//	strImage = 'document.image.img_' + i
				//	if (eval(strImage)){
				//	//alert(strImage);
				//		if (eval(strImage) != 'img_' + itemNum){
				//			changeImages('img_'+i, '/media/images/nav_'+i+'.gif')	
				//		}
				//	}
				//}	
			}	
		}
	}		
	
	 // Hide all other menus & dim old highlighted items, still showing this menu.
	 hideAllBut(menuNum);

	 // Get tree of parent menu items and light them up - global variable!
	 litNow = getTree(menuNum, itemNum);
	 changeCol(true);

	 // Get target menu to show - if it's nonzero, position & show it.
	 targetNum = menu[menuNum][itemNum].target;
	 if (targetNum > 0)
	 {
	  // Get current menu position - menu position plus item position in menu.
	  thisX = parseInt(menu[menuNum][0].ref.left) + parseInt(menu[menuNum][itemNum].ref.left);
	  thisY = parseInt(menu[menuNum][0].ref.top) + parseInt(menu[menuNum][itemNum].ref.top);

	  // Add those to the target's offset to set the target's position, show it.
	  with (menu[targetNum][0].ref)
	  {
	   left = thisX + menu[targetNum][0].x;
	   top = thisY + menu[targetNum][0].y;
	   visibility = 'visible';
	  }
	 }
	}


	function popOut(menuNum, itemNum)
	{
	 // If it's a root menu item that doesn't trigger a popout, hide now, else set timeout
	 // to hide all menus in 1/2 sec... remember, another mouseover clears the timeout.

	 if (bCharts){
		HideDropDowns(true);
		}	

	//THIS IS ANGUS' CODE.  I CHANGED THE FUNCTION IT CALLS - SCS
	//	 if ((menuNum == 0) && !menu[menuNum][itemNum].target) hideAllBut(0);
	//	 else popTimer = setTimeout('ClearAllRollovers(0)', 500);
	 
	 if ((menuNum == 0) && !menu[menuNum][itemNum].target)
	  hideAllBut(0);
	 else
	  popTimer = setTimeout('ClearAllRollovers(0)', 500);
	}

	
	function popClick(menuNum, itemNum)
	{
	 with (menu[menuNum][itemNum])
	 {
	  switch (type)
	  {
	   // A JavaScript function? Eval() it and break out of switch.
	   case 'js:': { eval(href); break }
	   // Otherwise, point to the window if nothing else and navigate.
	   case '': type = 'window';
	   default: if (href) eval(type + '.location.href = "' + href + '"');
	  }
	 }

	 // Whatever happens, hide the menus when clicked.
	 hideAllBut(0);
	}

	function getTree(menuNum, itemNum)
	{
	 // Array index is the menu number. The contents are null (if that menu is not a parent)
	 // or the item number in that menu that is an ancestor (to light it up).
	 itemArray = new Array(menu.length);

	 while(1)
	 {
	  itemArray[menuNum] = itemNum;
	  // If we've reached the top of the hierarchy, return.
	  if (menuNum == 0) break;
	  itemNum = menu[menuNum][0].parentItem;
	  menuNum = menu[menuNum][0].parentMenu;
	 }
	 return itemArray;
	}
	
	// Pass an array and a boolean to specify colour change, true = over colour.
	// N.B: Uses global litNow array which contains items in hierarchy.
	function changeCol(isOver)
	{
	 // Cycle through array searching for items to change.
	 for (count = 0; count < litNow.length; count++)
	 {
	  // If item number is present, change its colour.
	  if (litNow[count])
	  {
	   // Nest two WITH's, the last being more specific to allow item hover colours.
	   with (menu[count][0]) with (menu[count][litNow[count]])
	   {
	    newCol = isOver ? overCol : backCol;

	    // Change the colours of the div/layer background.
	    if (isNS4) ref.bgColor = newCol;
	    else ref.backgroundColor = newCol;
	   }
	  }
	 }
	}
	
	function hideAllBut(menuNum)
	{
	 // Get array of parent menus (item numbers irrelevant, just pass '1').
	 var keepMenus = getTree(menuNum, 1);

	 // ...and work through it, hiding menus that are not its ancestors/itself.
	 for (count = 0; count < menu.length; count++)
	  if (!keepMenus[count] && menu[count]) menu[count][0].ref.visibility = 'hidden';

	 // Dim all the items in litNow array.
	 changeCol(false);
	}

	function ClearAllRollovers(menuNum){
			if (<%=ROLLOVER_IMAGES%> ==1){			
				if (bImageRollovers){
					
					
					for (i = <%=g_intStartImage%> ; i < <%=g_intMenuItems + 1%>; i++) {
						if (<%=MENU_SELECTED%> != i){
							changeImages('img_' + i, '/media/images/nav_' + i + '.gif')
						}else{
							changeImages('img_' + i, '/media/images/nav_' + i + '-on.gif')
						}
					}				
				}
			}		
		hideAllBut(menuNum);
	}

	// *** MENU ARRAY CONSTRUCTION FUNCTIONS ***

	// This is incredibly, incredibly cool. Really. It takes an array of data and names and
	// assigns the values to a specified object -- used to slim down the constructors here.
	function addProps(obj, data, names, addNull)
	{
	 for (i = 0; i < names.length; i++)
	  if(i < data.length || addNull) obj[names[i]] = data[i];
	}

	// Use above function to add our list of arguments to the menu array.
	function Menu()
	{
	 var names = ['isVert', 'popInd', 'x','y', 'width', 'pad', 'overCol', 'backCol',
	  'borderClass', 'textClass', 'parentMenu', 'parentItem', 'ref'];
	 addProps(this, arguments, names, true);
	}

	function Item()
	{
	 var names = ['text', 'href', 'type', 'length', 'spacing', 'target',    'ref'];
	 addProps(this, arguments, names, true);
	}

// *** MAIN MENU SETUP FUNCTION ***

function createMenus()
{
 if (!isDyn) return;

 // Loop through menus, using properties of menu description object, i.e. x, y, width etc...
 for (currMenu = 0; currMenu < menu.length; currMenu++)
 if (menu[currMenu]) with (menu[currMenu][0])
 {
  // Variable for holding HTML for items and positions of next item.
  var str = '', itemX = 0, itemY = 0;

  // In NS4, since borders are assigned to the table rather than layer, increase padding.
  //if (isNS4) pad++;

  // Remember, items start from 1 in the array (0 is menu object itself, above).
  // Also use properties of each item nested in the other with() for construction.
  for (currItem = 1; currItem < menu[currMenu].length; currItem++) with (menu[currMenu][currItem])
  {
   var itemID = 'menu' + currMenu + 'item' + currItem;

   // The width and height of the menu item - dependent on orientation!
   // NS6 disagrees with other browsers as to whether borders increase widths, so fix here.
   var shrink = (borderClass && isDOM && !document.all ? 2 : 0)
   var w = (isVert ? width : length) - shrink;
   var h = (isVert ? length : width) - shrink;

   // Create a div or layer text string with appropriate styles/properties.
   if (isDOM || isIE4)
   {
    str += '<div id="' + itemID + '" style="position: absolute; left: ' + itemX +
     '; top: ' + itemY + '; width: ' + w + '; height: ' + h + '; visibility: inherit; ';
    if (backCol) str += 'background: ' + backCol;
    str += '" ';
   }
   if (isNS4)
   {
    str += '<layer id="' + itemID + '" left="' + itemX + '" top="' + itemY + '" width="' + 
     w + '" height="' + h + '" visibility="inherit" ';
    if (backCol) str += 'bgcolor="' + backCol + '" ';
   }
   if (borderClass) str += 'class="' + borderClass + '" ';
   
   // Add mouseover and click handlers and finish div/layer.
   str += 'onMouseOver="popOver(' + currMenu + ',' + currItem + ')" onMouseOut="popOut(' +
     currMenu + ',' + currItem + ')" onClick="popClick(' + currMenu + ',' + currItem + ')">';



   // Add contents of item...

   if (target > 0)
   {
    // Set target's parents to this menu item.
    menu[target][0].parentMenu = currMenu;
    menu[target][0].parentItem = currItem;

    // Add a popout indicator - before text so it shows up below text in NS4.
    if (popInd)
    {
     if (isNS4) str += '<layer class="' + textClass + '" left="'+ (w - 15) + '" top="' +
      pad + '">' + popInd + '</layer>';
     else str += '<div class="' + textClass + '" style="position: absolute; left: ' + (w - 15) +
      '; top: ' + pad + '">' + popInd + '</div>';
    }
   }

   // For NS4, if a border is assigned, add a spacer to push border out to layer edges.
   // The text layer must completely overlay this table as well for proper click capturing.
   // Add a link both to generate an onClick event and to stop the ugly I-beam text cursor appearing.
   if (isNS4) str += (borderClass ? '<spacer type="block" width="' + (w - 8) + '" height="' +
    (h - 8) + '">' : '') +
    '<layer left="' + pad + '" top="' + pad + '" width="' + (w - (2 * pad)) + '" height="' +
    (h - (2 * pad)) + '"><a class="' + textClass + '" href="#" ' +
    'onClick="popClick(' + currMenu + ',' + currItem + '); return false" ' +
    'onMouseOver="status=\'\'; return true;">' + text + '</a></layer>';

   // IE4+/NS6 is an awful lot easier to work with sometimes.
   else str += '<div class="' + textClass + '" style="position: absolute; left: ' + pad +
    '; top: ' + pad + '; width: ' + (w - (2 * pad)) + '; height: ' + (h - (2 * pad)) +
    '">' + text + '</div>';

   // Finish off item.
   str += (isNS4 ? '</layer>' : '</div>');

   // Move next item position down or across by this item's length and additional spacing.
   // Subtract 1 so borders overlap slightly.
   if (isVert) itemY += length + spacing - 1;
   else itemX += length + spacing - 1;

  // End loop through items and with([menu[currMenu][currItem]).
  }



  // Now, write the menu to the document depending on browser capabilities.
  // N.B: Still using properties of menu[currMenu][0] like 'ref' etc...
   
  // Insert a div tag to the end of the BODY with menu HTML in place for IE4+.
  if (document.all)
  {
   // Give a small width and height to stop IE4 sizing to full body. Thanks to Jeff Blum
   // for pointing this out. Also, thanks to Paul Maden for helping debug this, apparently
   // the width must be a miniumum of 3 for it to work in IE4.
   document.body.insertAdjacentHTML('beforeEnd', '<div id="menu' + currMenu + 'div" ' +
    'style="position: absolute; width: 3; height: 3; visibility: hidden; z-index: 1000">' +
     str + '</div>');
   ref = getSty('menu' + currMenu + 'div');
  }
  // In NS6+ or other DOM but non-IE browsers, create a new DIV node and add text...
  else if (isDOM)
  {
   var newDiv = document.createElement('div');
   document.body.appendChild(newDiv);
   newDiv.innerHTML = str;
   ref = newDiv.style;
    
   ref.position = 'absolute';
   ref.visibility = 'hidden';
  }
  // In NS4, create a reference to a new layer and write the items to it.
  else if (isNS4)
  {
   ref = new Layer(0);
   ref.document.write(str);
   ref.document.close();
  }

  // Chuck some positions in here. Only really relevant for root menu.
  ref.left = x;
  ref.top = y;
  // Set the default cursor for the menu to be the hand (or 'pointer' if you're the W3C or
  // Mozilla Project and just trying to be difficult :)...
  if (!isNS4) ref.cursor = (document.all ? 'hand' : 'pointer');
  // IE has z-indices assigned already, IE4 doesn't like them done here for some reason.
  if (!document.all) ref.zIndex = 1000;

  // Now items have been written, loop through them again to set up references.
  for (currItem = 1; currItem < menu[currMenu].length; currItem++)
  {
   itemName = 'menu' + currMenu + 'item' + currItem;
   if (isDOM || isIE4) menu[currMenu][currItem].ref = getSty(itemName);
   if (isNS4)
   {
    menu[currMenu][currItem].ref = ref.document[itemName];
    // Also capture clicks on that item layer's document...
    with (ref.document[itemName])
    {
     document.captureEvents(Event.CLICK);
     document.onclick = new Function('popClick(' + currMenu + ', ' + currItem + ')');
    }
   }
  }

 // End loop through menus and with (menu[currMenu][0]).
 }

 // *** CENTRING FUNCTION ***  Uncomment this to centre menus.
 //positionMenu()

 // Show the root menu now that's all over. Phew!
 menu[0][0].ref.visibility = 'visible';
 	//Change the selected image to the "ON" state (Selected image defined in header of each page)
		if (<%=MENU_SELECTED%> != 0){ 
			changeImages('img_<%=MENU_SELECTED%>', '/media/images/nav_<%=MENU_SELECTED%>-on.gif')
		}	

}
// Here, we handle the onLoad and onResize events. If you are combining this script with
// other scripts that use these events (e.g. DHTML Scroller), make sure to combine them!
// A good way to do that is: window.onevent = new Function('function1(); function2();...');
<%if session("browser") <> "IE" then%>
window.onload = createMenus;
<%end if%>
//window.onresize = resizeHandler;

	
<%

End Sub


' PrintOptionalJavascript() -------------------------------------------------------------------------------
'  Summary: This function will print javascript functions to the screen that are used to generate the 
'			layer based navigation menus.
'  Inputs: None
'  Outputs: Javascript text to the screen:
' ---------------------------------------------------------------------------------------------------------
Sub PrintOptionalJavascript

%>

// *** OPTIONAL CODE FROM HERE DOWN ***

// This handles the window resize bug in NS4, and optionally centres your menus. I suggest
// leaving this here as otherwise when you resize NS4 horizontally menus are hidden.

var popOldWidth = window.innerWidth;
function resizeHandler()
{
 // If no menus have been created, nothing to do here...
 if (!menu[0][0].ref) return;
 
 if (isNS4 && popOldWidth != window.innerWidth) location.reload()

 //positionMenu();
}

function positionMenu()
{
 // Uncomment these next lines to *** CENTRE/RIGHT ALIGN YOUR MENU ***
 // You must uncomment the positionMenu() call in the resizeHandler() function right above, and
 // also at the end of the createMenus() function above the menu[] array.
 // Edit this expression to anything you want -- note the menu width is hard-coded :).

 //var winWidth = (document.all ? document.body.clientWidth : window.innerWidth)
 //menu[0][0].ref.left = (winWidth / 2) - 120;
 
 // You can extend this code any way you want. Each menu or item has a 'ref' property that
 // is a reference to its style object, so you can move/resize pretty much anything.
}

// Optional 'coloured item' object you can add to your menu array. Delete this if you aren't
// using it, it's not necessary.

// You can alter most constructors, shifting properties between the default Menu() and Item()
// constructors, this is just an example. I built simple polymorphism into the menus beginning
// with v2.1 by nesting with()'s, just make sure everything gets included at least once
// somewhere. If in doubt, the script uses item properties over menu properties as they're more
// specific. Good luck!

function colItem()
{
 // If you want, add borderClass and textClass in here too. Make sure to add them as
 // parameter to this function above! You can add most parameters of the 'menu' object
 // used in writing menus to the document, such as popout indicators if you want.
 // Alternatively, it could be simpler to just use a bit of JS: menu[x][y].popInd = '...';
 // before creating the menus. Note: 'ref' isn't passed to the function.
 var names = ['text', 'href', 'type', 'length', 'spacing', 'overCol', 'backCol', 'target',
  'ref'];
 addProps(this, arguments, names, true);
}


// End Hide -->


<%

End Sub




%>
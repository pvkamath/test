<script language="vbscript" runat="server">
dim oContentNavAdmin 'as object
dim bNavKeywordsExist 'as boolean
dim rsNavKeywords 'as object
dim sNavKeyword 'as string
dim sNavKeywordType 'as string
dim iNavKeywordCount 'as integer
dim iNavCount 'as integer
</script>
<%
	set oContentNavAdmin = New ContentAdmin

	oContentNavAdmin.ConnectionString = Application("sDataSourceName")
	
	bNavKeywordsExist = oContentNavAdmin.getKeywords()
	

	if bNavKeywordsExist then
		set rsNavKeywords	= oContentNavAdmin.KeywordsRS
		rsNavKeywords.Open
	end if

set oContentNavAdmin = Nothing
if bNavKeywordsExist then
%>
<table>
	<tr>
		<td colspan="2"><span class="tabtitle">Active</span></td>
	</tr>
<%
iNavCount = rsNavKeywords.RecordCount

dim aNavKeywords()
redim aNavKeywords(iNavCount,2)

iNavCount = 1
	do while not rsNavKeywords.Eof
		sNavKeyword = rsNavKeywords.Fields("keyword").Value
		sKeywordType = rsNavKeywords.Fields("keywordtype").Value

		aNavKeywords(iNavCount,0) = sNavKeyword
		aNavKeywords(iNavCount,1) = sNavKeywordType

		iNavCount = iNavCount + 1
		rsNavKeywords.Movenext
	loop
	rsNavKeywords.Close
	set rsNavKeywords = Nothing


	call createList("contentList.asp", aNavKeywords) 'sub routine can be found at the bottom
%>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2"><span class="tabtitle">Pending</span></td>
	</tr>
<%
	call createList("contentPendingList.asp", aNavKeywords) 'sub routine can be found at the bottom
%>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2"><span class="tabtitle">Archived</span></td>
	</tr>
<%
	call createList("contentArchivedList.asp", aNavKeywords) 'sub routine can be found at the bottom
%>
</table>
<%	
end if
%>
<script language="vbscript" runat="server">
sub createList(p_sLink, p_aKeywords)
dim i 'as integer

	for i = 1 to ubound(p_aKeywords) step 1
		Response.Write("<tr>")
		Response.Write("	<td width=""20"">&nbsp;</td>")
		Response.Write("	<td><a href=""" & p_sLink& "?keywords=" &  p_aKeywords(i,0) & """>" & p_aKeywords(i,0) &  "</a></td>")
		Response.Write("</tr>")
	next
end sub
</script>
<!-- #include virtual = "/admin/security/includes/HandyADO.class.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/security.class.asp" --------------------------------------------------->


<%
'if session has expired
'Response.Write("x" & session("User_ID")  & "X")
if trim(session("User_ID")) = "" or not session("Access_Level") = "1" then
	Response.Write("<table><tr><td>Your session has expired.<br><br><a href=""javascript:void(window.parent.location.href='/')"">Please Click Here to log back in.</a></td></tr></table>")
	Response.End
end if

function checkDirectoryAccess(p_iUserID, p_sPath)
dim bHasAccess 'as boolean
dim oDirectorySecurity 'as object



	'###############################################################################
	'security code block
	'purpose is to set the boolean so that it can be used throughout the page
	set oDirectorySecurity = New Security 'Found in include admin\security\security.Class.asp

	oDirectorySecurity.ConnectionString = Application("sDataSourceName")

	oDirectorySecurity.UserID = p_iUserID

	'Response.Write("X" & p_iUserId & "X" & p_sPath & "X")

	bHasAccess = oDirectorySecurity.UserHasDirectoryNameAccess(p_sPath)

	if not bHasAccess then
	Response.Write("<link rel=""stylesheet"" href=""/admin/includes/style_ie.css"">")
	Response.Write("<table><tr><td>You do not have access to this directory.<br>Please contact your administrator for access privileges.<br><a href=""javascript:window.history.back()"">Click Here to go Back</a></td></tr></table>")
	Response.End
	end if

	set oDirectorySecurity = Nothing
	'###############################################################################

	checkDirectoryAccess = bHasAccess
end function

%>
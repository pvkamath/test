<%
'===============================================================================================================
' Class: Content Manager 
' Description: Content Manager Class
' Create Date: 2/15/02
' Current Version: 1.0
' Author(s): Joseph Lascola
' References to other DLL's: newsFx2
' References other classes : HandyADO.Class.asp
' ----------------------
' Properties:
'	- ConnectionString
'	- BodyText, HeadLine, ByLine, PostingDate
'	- WrittenDate, Source, Abstract, Status
'	- Keywords, AuthorID, ContentID, SiteID
'	- ErrorDescription, ContentRS, KeywordsRS, Connection
'	- MediaName, MediaPath
'
' Methods:
'	- approveContent
'	- hasPending
'	- addContent
'	- modContent
'	- isGoodContentCheck (private)
'	- checkEmpty (private)
'	- setConnection (private)
'===============================================================================================================
Class ContentAdmin
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString
	dim g_sBodyText, g_sHeadLine, g_sByLine, g_sPostingDate
	dim g_sWrittenDate, g_sSource, g_sAbstract, g_sStatus
	dim g_sKeywords, g_iAuthorID, g_iContentID, g_iSiteID
	dim g_sErrorDescription
	dim g_bCheckBodyText
	dim g_bCheckKeywords
	dim g_bCheckPostingDate
	dim g_bCheckWrittenDate
	dim g_bCheckSource
	dim g_bCheckAbstract
	dim g_bCheckSiteID
	dim g_bCheckAuthorID
	dim g_oContentRS
	dim g_oKeywordsRS
	dim g_oConnection
	dim g_sMessage
	dim g_sMediaName 
	dim g_sMediaPath

	' PUBLIC METHODS ===========================================================================================
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: approveContent
	' Description: 
	' Pre-conditions: 
	'	Required Properties:
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	public function approveContent(p_iContentID)
	'on error resume next
	dim oNewsAdmin
	dim iOldContentID
	dim rsContent
	
		set oNewsAdmin = server.CreateObject("NewsFXV2.Admin")
		
		oNewsAdmin.ConnectionString = g_sConnectionString
		
		'Check if the news item id was passed and is a number 
		if len(p_iContentID) = 0 or not isnumeric(p_iContentID) then
			g_sErrorDescription = "A Content Id must be passed to this method"
			set oNewsAdmin = Nothing
			set oNewsReport = Nothing
			exit function
		end if
	
		if GetContent(p_iContentID) then
		'Change Contentid status to Active
			oNewsAdmin.NewsItemId = p_iContentID
			
			g_oContentRS.Open
			
			oNewsAdmin.Headline = g_oContentRS.fields("Headline").Value
			oNewsAdmin.ByLine = g_oContentRS.fields("Byline").Value
			oNewsAdmin.PostingDate = g_oContentRS.fields("PostingDate").Value
			oNewsAdmin.WrittenDate = g_oContentRS.fields("WrittenDate").Value
			oNewsAdmin.Source = g_oContentRS.fields("Source").Value
			oNewsAdmin.Abstract = g_oContentRS.fields("Abstract").Value
			oNewsAdmin.Revision = g_oContentRS.fields("Revision").Value
			oNewsAdmin.AuthorID = g_oContentRS.fields("AuthorID").Value
			oNewsAdmin.Status = "Active"
			
			oNewsAdmin.AdminUpdateNewsItem

			iOldContentID = g_oContentRS.fields("Revision").Value
			
			if len(iOldContentID) > 0 then
				bRevisionExist = true
			end if
		
			if bRevisionExist then
			'Change Contentid status to Active
				g_oContentRS.Close
				
				if GetContent(iOldContentID) then

					g_oContentRS.Open
			
					oNewsAdmin.NewsItemId = iOldContentID
					
					oNewsAdmin.Headline = g_oContentRS.fields("Headline").Value
					oNewsAdmin.ByLine = g_oContentRS.fields("Byline").Value
					oNewsAdmin.PostingDate = g_oContentRS.fields("PostingDate").Value
					oNewsAdmin.WrittenDate = g_oContentRS.fields("WrittenDate").Value
					oNewsAdmin.Source = g_oContentRS.fields("Source").Value
					oNewsAdmin.Abstract = g_oContentRS.fields("Abstract").Value
					oNewsAdmin.Revision = g_oContentRS.fields("Revision").Value
					oNewsAdmin.AuthorID = g_oContentRS.fields("AuthorID").Value
			
					oNewsAdmin.Status = "Archived"

					oNewsAdmin.AdminUpdateNewsItem
				end if
			end if
		end if
					
		set oNewsAdmin = Nothing
	end function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: addContent
	' Description: Insert content with pending status 
	' Pre-conditions: 
	'	Required Properties:
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	public function addContent()
	on error resume next
		dim oNewsAdmin
		dim i
		dim aKeywordSplit
		dim bReturn

		bReturn = false
		
		set oNewsAdmin = Server.CreateObject("Newsfxv2.Admin")

		oNewsAdmin.ConnectionString = g_sConnectionString

		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			addContent = false
			exit function
		end if
		
		if isGoodContentCheck() then

			'SET VARIABLES
			oNewsAdmin.BodyText = g_sBodyText
			oNewsAdmin.HeadLine = g_sHeadline
			oNewsAdmin.ByLine = g_sByline
			oNewsAdmin.PostingDate = g_sPostingDate
			oNewsAdmin.WrittenDate = g_sWrittenDate
			oNewsAdmin.Source = g_sSource
			oNewsAdmin.Abstract = g_sAbstract
			oNewsAdmin.Status = "Pending"
			oNewsAdmin.SiteID = g_iSiteID
			oNewsAdmin.AuthorID = g_iAuthorID

			'ADD THE CONTENT
			oNewsAdmin.AdminAddNewsItem

			
			'ADD ABILITY FOR AN ARRAY OF KEYWORDS
			aKeywordSplit = split(g_sKeywords,",")
			for i = 0 to  ubound(aKeywordSplit)
				oNewsAdmin.keyword = aKeywordSplit(i)
				oNewsAdmin.AdminAddNewsKeywords
			next

			oNewsAdmin.AdminAddNewsAuthorMatch
			oNewsAdmin.AdminAddNewsBody

			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				addContent = false
				exit function
			else
				bReturn = true
			end if

		end if
		
		set oNewsAdmin = Nothing

		addContent = bReturn
	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: modContent
	' Description: Insert content with pending status and the reference to the old content id(which is the newsfx newsitemid)
	' Pre-conditions: 
	'	Required Properties:
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	public function modContent(p_iContentID)
	on error resume next
		dim oNews
		dim i
		dim aKeywordSplit
		dim bReturn

		bReturn = false
		
		set oNews = Server.CreateObject("Newsfxv2.Admin")

		oNews.ConnectionString = g_sConnectionString

		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			modContent = false
			exit function
		end if

		'Check if the news item id was passed and is a number 
		if len(p_iContentID) = 0 or not isnumeric(p_iContentID) then
			g_sErrorDescription = "A Content Id must be passed to this method"
			set oNews = Nothing
			exit function
		end if
		
		if isGoodContentCheck() then
			'SET VARIABLES
			oNews.BodyText = g_sBodyText
			oNews.HeadLine = g_sHeadline
			oNews.ByLine = g_sByline
			oNews.PostingDate = g_sPostingDate
			oNews.WrittenDate = g_sWrittenDate
			oNews.Source = g_sSource
			oNews.Abstract = g_sAbstract
			oNews.Status = "Pending"
			oNews.SiteID = g_iSiteID
			oNews.Revision = p_iContentID
			oNews.AuthorID = g_iAuthorID

			'ADD THE CONTENT
			oNews.AdminAddNewsItem
			
			'ADD ABILITY FOR AN ARRAY OF KEYWORDS
			aKeywordSplit = split(g_sKeywords,",")
			for i = 0 to  ubound(aKeywordSplit)
				oNews.keyword = aKeywordSplit(i)
				oNews.AdminAddNewsKeywords
			next
		
			oNews.AdminAddNewsAuthorMatch
			oNews.AdminAddNewsBody

			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				modContent = false
				exit function
			else
				bReturn = true
			end if

		end if
		
		set oNews = Nothing
	
		modContent = bReturn
	end function

	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContent
	' Description:		return a recordset for the contentid passed
	' Inputs:			p_iContentID - is the 
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContent(p_iContentId)
	dim bReturn
	dim oContentReport
	
		bReturn = false

		set oContentReport = new ContentReport 'found in contentReport.Class.asp included at the top of the page

		if len(g_sConnectionString) > 0 then

			oContentReport.ConnectionString = g_sConnectionString
		
			if oContentReport.GetContent(p_iContentid) then
				set g_oContentRS = oContentReport.ContentRS

				bReturn = true					
			else
				g_sMessage = oContentReport.Message
				g_sErrorDescription = oContentReport.ErrorDescription
			end if
		else
			g_sErrorDescription = "Not Datasource was supplied"
		end if		
		set oContentReport = Nothing

		getContent = bReturn
	end function
	


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContentList
	' Description:		return the record set for the contentid passed
	' Inputs:			p_iOrderBy - is the number to order the query by
	'					p_sStatus - is the status to pull back ("Archived","Active","Pending")
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContentList(p_sStatus,p_iOrderBy)
	dim bReturn
	dim oContentReport
	
		bReturn = false

		set oContentReport = new ContentReport 'found in contentReport.Class.asp included at the top of the page

		if len(g_sConnectionString) > 0 then

			oContentReport.ConnectionString = g_sConnectionString
			oContentReport.Keywords = g_sKeywords

			call clearContent() 'subroutine found below
			
			if oContentReport.getContentList(p_sStatus,p_iOrderBy) then
				set g_oContentRS = oContentReport.ContentRS
				bReturn = true					
			else
				g_sMessage = oContentReport.Message
				g_sErrorDescription = oContentReport.ErrorDescription
			end if
		else
			g_sErrorDescription = "Not Datasource was supplied"
		end if		
		set oContentReport = Nothing

		getContentList = bReturn
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: hasPending
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	public function hasPending(p_iContentID)
	'on error resume next
	dim sSQL
	dim rsPendingCheck
	dim bReturn
		
		call setConnection()
	
		set rsPendingCheck = Server.CreateObject("ADODB.Recordset")
		set rsPendingCheck.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = "SELECT newsItemID " &_
				"FROM afxNewsItems " &_
				"WHERE revision = " & p_iContentID & " " &_
				"AND status = 'Pending'"

		rsPendingCheck.Open sSQL
		
		if not rsPendingCheck.EOF then
			g_iContentID = rsPendingCheck.Fields("newsItemID").Value

			bReturn = true
		end if
		
		hasPending = bReturn
	
		rsPendingCheck.Close
		set rsPendingCheck = Nothing
	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getKeywords
	' Description:		sets a recordset with the keywords
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getKeywords()
	'on error resume next
	dim sSQL
	dim rsKeywords
	dim bReturn
		
		call setConnection()
	
		set rsKeywords = Server.CreateObject("ADODB.Recordset")
		set rsKeywords.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = "SELECT keyword, KeywordType " &_
				"FROM afxKeywords" 

		rsKeywords.Open sSQL,,3,3
		
		if not rsKeywords.EOF then
			set g_oKeywordsRS = rsKeywords

			bReturn = true
		end if
		
		getKeywords = bReturn
		
		rsKeywords.Close
		set rsKeywords = Nothing
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:			addMedia	
	' Description:	Associates some sort of media with a content id
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function addMedia(p_sMediaType)
	on error resume next
	dim bReturn
	dim sMessageDescription 
	
		bReturn = false

		set oNewsAdmin = Server.CreateObject("Newsfxv2.Admin")
		
		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			addMedia = false
			set oNewsAdmin = Nothing
			exit function
		end if
		
		oNewsAdmin.ConnectionString = g_sConnectionString	

		if len(p_sMediaType) = 0 then
			p_sMediaType = "I"
		end if

		sMessage = sMessage & checkEmpty(g_iContentID,"ContentID")
		sMessage = sMessage & checkEmpty(g_sMediaName,"MediaName")
		sMessage = sMessage & checkEmpty(g_sMediaPath,"MediaPath")			
		
		if len(sMessage) > 0 then
			g_sMessage = sMessage
			set oNewsAdmin = Nothing
			exit function	
		end if

		oNewsAdmin.NewsItemID = g_iContentID
		oNewsAdmin.MediaType = p_sMediaType
		oNewsAdmin.MediaPath = g_sMediaPath
		oNewsAdmin.MediaName = g_sMediaName
		
		if oNewsAdmin.AdminAddNewsMedia() = "No Errors" then
			bReturn = true
		end if
		
		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			addMedia = false
			set oNewsAdmin = Nothing
			exit function
		end if
		
		set oNewsAdmin = Nothing
		
		addMedia = bReturn
	end function
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				clearContent
	' Description:		Clears all properties except the connetionstring
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub clearContent()
		g_sBodyText = "" 
		g_sHeadLine = "" 
		g_sByLine = "" 
		g_sPostingDate = ""
		g_sWrittenDate = "" 
		g_sSource = "" 
		g_sAbstract = "" 
		g_sStatus = ""
		g_sKeywords = "" 
		g_iAuthorID = "" 
		g_iContentID = "" 
		g_iSiteID = ""
		g_sErrorDescription = ""
		g_sMessage = ""

		if isObject(g_oContentRS) then
			g_oContentRS.Close
			set g_oContentRS = nothing
		end if
		if isObject(g_oKeywordsRS) then
			g_oKeywordsRS.Close
			set g_oKeywordsRS = nothing
		end if
	
	end sub

	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				isGoodContentCheck
	' Description:		This function checks if the content required is supplied 
	' Inputs:			none
	' Output:			True	-	If the context exist
	'					False	-	If the required content does not exist
	' Error Handling :	
	' --------------------------------------------------------------------------------------------------------------------------------
	private function isGoodContentCheck()
	dim sErrorDescription
	dim bReturn	
		
		bReturn = false 'Initialize return variable
	
		'THIS SECTION SET THE ERROR DESCRIPTION IF ANY OF THESE REQUIRES FIELDS ARE EMPTY
		if g_bCheckBodyText then
			sErrorDescription = sErrorDescription & checkEmpty(g_sBodyText,"Body Text")
		end if
		if g_bCheckKeywords then
			sErrorDescription = sErrorDescription & checkEmpty(g_sKeywords,"Keyword(s)")
		end if
		if g_bCheckPostingDate then
			sErrorDescription = sErrorDescription & checkEmpty(g_sPostingDate,"Post Date")
		end if
		if g_bCheckWrittenDate then
			sErrorDescription = sErrorDescription & checkEmpty(g_sWrittenDate,"Written Date")
		end if
		if g_bCheckSource then
			sErrorDescription = sErrorDescription & checkEmpty(g_sSource,"Source")
		end if
		if g_bCheckAbstract then
			sErrorDescription = sErrorDescription & checkEmpty(g_sAbstract,"Abstract")
		end if
		if g_bCheckSiteID then
			sErrorDescription = sErrorDescription & checkEmpty(g_iSiteID,"Site ID")
		end if
		if g_bCheckAuthorID then
			sErrorDescription = sErrorDescription & checkEmpty(g_iAuthorID,"Author ID")
		end if		
		
		if len(sErrorDescription) = 0 then
			bReturn = True
		else
			g_sErrorDescription = sErrorDescription
		end if

		isGoodContentCheck = bReturn

	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: checkEmpty
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	private function checkEmpty(p_sItem,p_sItemDescription)
	dim sReturn
		sReturn = ""
		
		if len(p_sItem) = 0 or p_sItem = "" then
			sReturn = " " & p_sItemDescription & " cannot be empty"
		end if
		
		checkEmpty = sReturn
	
	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				setConnection
	' Description:		Set The connection if one does not exist
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub setConnection()
		if not isObject(g_oConnection) then
			set g_oConnection = server.CreateObject("ADODB.Connection")
			if len(g_sConnectionString) = 0 then
				g_sErrorDescription = "No Connection String was supplied"
			else
				g_oConnection.ConnectionString = g_sConnectionString
			end if
			g_oConnection.Open
		end if
	end sub

	' PRIVATE METHODS ==========================================================================================
	Private Sub Class_Initialize()
		' Enter code you want to run on Object Startup here.
		%>
		<!--#include virtual="/admin/includes/ContentManagerAdminConfig.asp"-->
		<%
	End Sub

	Private Sub Class_Terminate()
		' Enter code you want to run on Object Termination here.
	End Sub

	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined
	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	Public Property Let BodyText(p_sBodyText)
		g_sBodyText = p_sBodyText
	End Property

	Public Property Get BodyText()
		BodyText = g_sBodyText
	End Property

	Public Property Let HeadLine(p_sHeadLine)
		g_sHeadLine = p_sHeadLine
	End Property

	Public Property Get HeadLine()
		HeadLine = g_sHeadLine
	End Property

	Public Property Let ByLine(p_sByLine)
		g_sByLine = p_sByLine
	End Property

	Public Property Get ByLine()
		ByLine = g_sByLine
	End Property

	Public Property Let PostingDate(p_sPostingDate)
		g_sPostingDate = p_sPostingDate
	End Property

	Public Property Get PostingDate()
		PostingDate = g_sPostingDate
	End Property

	Public Property Let WrittenDate(p_sWrittenDate)
		g_sWrittenDate = p_sWrittenDate
	End Property

	Public Property Get WrittenDate()
		WrittenDate = g_sWrittenDate
	End Property

	Public Property Let Source(p_sSource)
		g_sSource = p_sSource
	End Property

	Public Property Get Source()
		Source = g_sSource
	End Property

	Public Property Let Abstract(p_sAbstract)
		g_sAbstract = p_sAbstract
	End Property

	Public Property Get Abstract()
		Abstract = g_sAbstract
	End Property

	Public Property Let Status(p_sStatus)
		g_sStatus = p_sStatus
	End Property

	Public Property Get Status()
		Status = g_sStatus
	End Property

	Public Property Let Keywords(p_sKeywords)
		g_sKeywords = p_sKeywords
	End Property

	Public Property Get Keywords()
		Keywords = g_sKeywords
	End Property

	Public Property Let AuthorID(p_iAuthorID)
		g_iAuthorID = p_iAuthorID
	End Property

	Public Property Get AuthorID()
		AuthorID = g_iAuthorID
	End Property

	Public Property Let ContentID(p_iContentID)
		g_iContentID = p_iContentID
	End Property

	Public Property Get ContentID()
		ContentID = g_iContentID
	End Property

	Public Property Let SiteID(p_iSiteID)
		g_iSiteID = p_iSiteID
	End Property

	Public Property Get SiteID()
		SiteID = g_iSiteID
	End Property

	Public Property Let ErrorDescription(p_sErrorDescription)
		g_sErrorDescription = p_sErrorDescription
	End Property

	Public Property Get ErrorDescription()
		ErrorDescription = g_sErrorDescription
	End Property

	Public Property Let Message(p_sMessage)
		g_sMessage = p_sMessage
	End Property

	Public Property Get Message()
		Message = g_sMessage
	End Property

	Public Property Set ContentRS(p_oContentRS)
		set g_oContentRS = p_oContentRS
	End Property

	Public Property Get ContentRS()
		set ContentRS = g_oContentRS
	End Property	


	Public Property Set KeywordsRS(p_oKeywordsRS)
		set g_oKeywordsRS = p_oKeywordsRS
	End Property

	Public Property Get KeywordsRS()
		set KeywordsRS = g_oKeywordsRS
	End Property	
	
	Private Property Set Connection(p_oConnection)
		set g_oConnection = p_oConnection
	End Property

	Private Property Get Connection()
		set Connection = g_oConnection
	End Property	
	
	Private Property Set MediaName(p_sMediaName)
		set g_sMediaName = p_sMediaName
	End Property

	Private Property Get MediaName()
		set p_sMediaName = g_sMediaName
	End Property		
	

	Private Property Set MediaPath(p_sMediaPath)
		set g_sMediaPath = p_sMediaPath
	End Property

	Private Property Get MediaPath()
		set p_sMediaPath = g_sMediaPath
	End Property

	'===========================================================================================================
End Class
%>
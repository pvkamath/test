<%	
	dim sSQL, sField 'as string
	dim reqRS 'as object
	dim p_objConn 'as object
	
	
	set reqRS = server.createobject("ADODB.Recordset")
	set p_objConn = server.CreateObject("ADODB.Connection")
    	p_objConn.Open application("sDataSourceName")
	

'Set the report Parameters
	dim sState, sReq, sEdit 'as string



sSQL =	"SELECT StateAbbrev as StName, RequirementsText as ReqTxt, LastEdit From StateRequirements"


' -- Output the query as an XML/txt file
	response.ContentType="text/xml"
	response.write("<?xml version='1.0' encoding='ISO-8859-1'?>")
	response.write("<CKStateReqs>")

	reqRS.Open sSQL, p_objConn, 3, 2
	reqRS.MoveFirst
	
' -- Now output the contents of the Recordset
	
	Do While Not (reqRS.EOF) AND Not (reqRS.BOF) 
		

		sState = chr(9) & "<State Name= " 
		
		sState = sState & chr(34) & reqRS.Fields("StName").value & chr(34)
		sState = sState & ">" & chr(13) 
		response.write(sState)


		' -- output the contents
		sReq = chr(9) & chr(9) & "<Requirement_Text>" & chr(13)
		sReq = sReq & "<![CDATA[ "
		sReq = sReq & reqRS.Fields("ReqTxt").value  & "]]>"& chr(13)
		sReq = sReq & "</Requirement_Text>" & chr(13) 
		response.write(sReq)
		

		sEdit = chr(9) & chr(9) & "<Edit_Date> "
		sEdit = sEdit & reqRS.Fields("LastEdit").value & "</Edit_Date>"  & chr(13)
		response.write(sEdit)
			
																								
		sState = chr(9) & "</State>"  & chr(13)
		Response.write(sState)
			
		' -- move to the next course record
		reqRS.MoveNext
	Loop
			
			
		
	
	response.write("</CKStateReqs>")

	reqRS.close
	set reqRS = Nothing
	set p_objConn = Nothing
%>
<%
option explicit
%>

<!-- #include virtual = "/admin/includes/misc.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" -------------------------------------------------->

<%
dim objConn 'as object
dim rs, linkRS 'as object
dim sSQL 'as string
dim sClass 'as string
dim sState 'as string
dim iStateID 'as string
dim sLink 'as string

iStateID = scrubforsql(request("StateID"))

set objConn = New HandyADO

sSQL = "SELECT * FROM States WHERE StateID = '" & iStateID & "' "

set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

if not rs.eof then
	sState = rs("State")
else
	set objConn = nothing
	set rs = nothing
	response.redirect("/admin/error.asp?message=The state could not be found in the system.")
end if

sSQL = "SELECT * FROM StatesLinks " & _
	   "WHERE StateID = '" & iStateID & "' " & _
	   "Order By Sort"

set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
%>

<script language="javascript">
	function ConfirmDelete(p_iLinkID, p_sName)
	{
		if (confirm("Are you sure you want to Delete this link?"))
			window.location.href = '/admin/states/ModLinkProc.asp?mode=delete&LinkID=' + p_iLinkID + '&StateID=<%=iStateID%>&Name=' + p_sName;
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%=sState%> Links</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<br>
			<a href="ModLink.asp?StateID=<%=iStateID%>">Add New Link</a><br>
			<a href="StateList.asp">Return to the State Listing</a>
		</td>
	</tr>
</table>
<br>

<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Sort &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Name &nbsp;</td>		
		<td class="reportHeaderText">&nbsp; View &nbsp;</td>							
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>									
	</tr>
<% 
if not rs.eof then
	do while not rs.eof 	
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if
%>
	<tr class="<%= sClass%>">
		<td>	
<%				
		if (clng(rs.recordcount) > 1) then		
			if cint(rs("Sort")) = 1 then
				'if first item
				response.write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""ModLinkProc.asp?LinkID=" & rs("ID") & "&mode=SORT&Direction=DOWN&StateID=" & iStateID & """><img src=""/admin/media/images/button_arrow_down.gif"" border=""0""></a>")
			elseif cint(rs("Sort")) = clng(rs.recordcount) then
				'if last item
				response.write("<a href=""ModLinkProc.asp?LinkID=" & rs("ID") & "&mode=SORT&Direction=UP&StateID=" & iStateID & """><img src=""/admin/media/images/button_arrow_up.gif"" border=""0""></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
			else
				'if middle item
				response.write("<a href=""ModLinkProc.asp?LinkID=" & rs("ID") & "&mode=SORT&Direction=UP&StateID=" & iStateID & """><img src=""/admin/media/images/button_arrow_up.gif"" border=""0""></a>")
				response.write("&nbsp;&nbsp;")
				response.write("<a href=""ModLinkProc.asp?LinkID=" & rs("ID") & "&mode=SORT&Direction=DOWN&StateID=" & iStateID & """><img src=""/admin/media/images/button_arrow_down.gif"" border=""0""></a>")
			end if
		else
			response.write("&nbsp;")
		end if		
%>
		</td>
		<td align="center">			
			<%= rs("Name") %>
		</td>		
		<td align="center">			
<%
	if cint(rs("LinkType")) = 1 then 'href
		sLink = rs("Link")
	elseif cint(rs("LinkType")) = 2 then 'file
		sLink = application("sDynStatesDocumentsFolder") & rs("Link")
	end if
%>		
			<a href="<%=sLink%>" target="_blank">View</a>
		</td>				
		<td align="center">
			<a href="javascript:void(ConfirmDelete(<%=rs("ID")%>, '<% = rs("Name") %>'))">Delete</a>
		</td>			
	</tr>
<%
		rs.MoveNext
	loop
else
%>
	<tr>
		<td colspan="5">
			There are currently no Links for this state.
		</td>
	</tr>
<%
end if
%>	
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set linkRS = nothing
set objConn = nothing
set rs = nothing
%>
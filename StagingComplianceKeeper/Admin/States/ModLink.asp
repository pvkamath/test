<%
option explicit
on error resume next
%>

<!-- #include virtual = "/admin/includes/misc.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/admin/includes/Functions.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->

<%
dim objConn 'as object
dim rs 'as object
dim iStateID 'as integer
dim sSQL 'as string
dim sMode 'as string
dim sState 'as string
dim iLinkTypeID 'as integer
dim sLink 'as string

'Retrieve the State Information
iStateID = scrubforsql(request("StateID"))
iLinkTypeID = scrubforsql(request("StateLinkType"))

set objConn = New HandyADO

sSQL = "SELECT * FROM States WHERE StateID = '" & iStateID & "' "

set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

if not rs.eof then
	sState = rs("State")
else
	set objConn = nothing
	set rs = nothing
	response.redirect("/admin/error.asp?message=The state could not be found in the system.")
end if

set objConn = nothing
set rs = nothing
%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function LinkTypeChange(FORM)
	{
		var iSelVal = FORM.StateLinkType[FORM.StateLinkType.selectedIndex].value;
		
		window.location.href = 'modLink.asp?StateID=<%=iStateID%>&StateLinkType=' + iSelVal
	}
	
	function Validate(FORM)
	{
		//determine what field should be validated
		var iSelVal = FORM.StateLinkType[FORM.StateLinkType.selectedIndex].value;
		
		//Href field
		if (iSelVal == 1)
			return (checkString(FORM.name,"Name") && checkString(FORM.href,"HREF"))
		else
			return (checkString(FORM.name,"Name") && checkString(FORM.file,"File"))
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add&nbsp;<%= sState %>&nbsp;Link</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="ModLinkProc.asp" method="POST" enctype="multipart/Form-Data" onsubmit="return Validate(this)">
<input type="hidden" name="Mode" value="Add">
<input type="hidden" name="StateID" value="<%=iStateID%>">

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<b>Type of Link:</b>
		</td>
		<td>
			<% call DisplayStateLinkTypesDropDown(iLinkTypeID,1,"onChange='LinkTypeChange(this.form)'") %>
		</td>
	</tr>
	<tr>
		<td>
			<b>Name:</b>
		</td>	
		<td>
			<input type="text" name="name" value="">
		</td>
	</tr>		
<%
if trim(iLinkTypeID) <> "" then 
	if cint(iLinkTypeID) = 1 then
%>
	<tr>
		<td>
			<b>HREF:</b>
		</td>
		<td>
			<input type="text" name="href" value="http://">
		</td>
	</tr>
<%
	end if
	if cint(iLinkTypeID) = 2 then
%>		
	<tr>
		<td>
			<b>File:</b>
		</td>
		<td>
			<input type="file" name="file">
		</td>
	</tr>	
<%
	end if
end if
%>	
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
<%
if trim(iLinkTypeID) <> "" then 
%>			
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
<%
end if
%>			
			<a href="javascript:void(window.location.href='/admin/states/StateList.asp')"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
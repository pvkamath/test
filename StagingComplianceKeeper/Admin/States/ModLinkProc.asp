<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->

<%
dim oFileUp,objconn 'as object
dim iLinkTypeID 'as integer
dim iLinkID 'as integer
dim sName 'as string
dim sHref,sFile 'as string
dim sMode 'as string
dim iStateID 'as integer
dim sStateAbbrev 'as string
dim sSQL 'as string
dim sState 'as string
dim rs 'as object
dim sAction 'as string
dim sLink 'as string
dim sDirection 'as string
dim iNextSort 'as integer

'Create the Fileup object that will allow us to upload files
set oFileUp = server.CreateObject("softartisans.fileup")

set objConn = server.CreateObject("ADODB.Connection")
objConn.ConnectionString = application("sDataSourceName")
objConn.Open

iLinkTypeID = oFileUp.Form("StateLinkType")
sName  = scrubforSql(oFileUp.Form("Name"))
'Catch the name if being passed in from the querystring via the
'delete javascript.
if sName = "" then
	sName = ScrubForSql(request("Name"))
end if
sHref = scrubforSql(oFileUp.Form("HREF"))
iStateID = scrubforSql(oFileUp.Form("StateID"))
sMode = oFileUp.Form("mode")

'if true, then retrieve from request var
if trim(sMode) = "" then
	sMode = request("mode")
	iStateID = scrubforSql(request("StateID"))
	iLinkID = scrubforSql(request("LinkID"))
	sDirection = scrubforSql(request("Direction"))
end if

'Retrieve the State Information
sSQL = "SELECT * FROM States WHERE StateID = '" & iStateID & "' "

set rs = objConn.execute(sSQL)

if not rs.eof then
	sState = rs("State")
	sStateAbbrev = rs("Abbrev")
else
	set objConn = nothing
	set rs = nothing
	set oFileObj = nothing
	response.redirect("/admin/error.asp?message=The state could not be found in the system.")
end if

if UCase(sMode) = "ADD" then
	sAction = "Added"
	
	'Get the Next Sort Value
	sSQL = "SELECT MAX(Sort) FROM StatesLinks WHERE StateID = '" & iStateID & "' "

	set rs = objConn.execute(sSQL)
	
	if isnull(rs(0)) then
		iNextSort = 1
	else
		iNextSort = rs(0) + 1
	end if

	'if HREF Link
	if cint(iLinkTypeID) = 1 then
		sSQL = "INSERT INTO StatesLinks(Name,Link,LinkType,StateID,Sort) " & _
			   "VALUES('" & sName & "','" & sHref & "','" & iLinkTypeID & "','" & iStateID & "','" & iNextSort & "')"
		objConn.execute(sSQL)
		
		if err.number <> 0 then
			set objConn = nothing
			set oFileUp = nothing
			response.redirect("/admin/error.asp?message=The Link was not Added successfully.<br>" & err.description)
		end if
		
		'Update the XML file
		XMLAddLink sStateAbbrev, sHref, sName
	elseif cint(iLinkTypeID) = 2 then
		sFile = ScrubForSQL(mid(oFileUp.Form("file").UserFileName, instrrev(oFileUp.Form("file").UserFileName, "\") + 1))
		
		'save the file to the database
		sSQL = "INSERT INTO StatesLinks(Name,Link,LinkType,StateID,Sort) " & _
			   "VALUES('" & sName & "','" & sFile & "','" & iLinkTypeID & "','" & iStateID & "','" & iNextSort & "')"
		objConn.execute(sSQL)
		
		if err.number <> 0 then
			set objConn = nothing
			set oFileUp = nothing
			response.redirect("/admin/error.asp?message=The Link was not Added successfully.<br>" & err.description)
		end if
				
		'Save the file to the system
		oFileUp.Form("file").SaveAs application("sAbsStatesDocumentsFolder") & sFile	
		
		'Update the XML file
		XMLAddLink sStateAbbrev, sFile, sName
	end if
	

	
elseif UCase(sMode) = "DELETE" then
	sAction = "Deleted"
	
	'pull link type and name
	sSQL = "SELECT * FROM StatesLinks WHERE ID = '" & iLinkID & "' "
	
	set rs = objConn.execute(sSQL)
	
	if rs.eof then
		set objConn = nothing
		set oFileUp = nothing
		response.redirect("/admin/error.asp?message=The Link information could not be retrieved.")	
	end if
	
	iLinkTypeID = rs("LinkType")
	sLink = rs("Link")
	
	Call ReOrderLinks(iLinkID)
	
	sSQL = "DELETE FROM StatesLinks WHERE ID = '" & iLinkID & "' "
	objConn.execute(sSQL)
		
	if err.number <> 0 then
		set objConn = nothing
		set oFileUp = nothing
		response.redirect("/admin/error.asp?message=The Link was not Deleted successfully.<br>" & err.description)
	end if	
	
	'if document, delete file from system
	if cint(iLinkTypeID) = 2 then
		dim oFileSysObj 'as object
		
		'Delete from the system
		set oFileSysObj = CreateObject("Scripting.FileSystemObject") 

		If oFileSysObj.FileExists(application("sAbsStatesDocumentsFolder") & sLink) Then
		   oFileSysObj.DeleteFile application("sAbsStatesDocumentsFolder") & sLink
		End If 	
		
		set oFileSysObj = nothing
	end if
		
	XMLRemoveLink sStateAbbrev, sName, sLink
	
elseif UCase(sMode) = "SORT" then
	call SortLinks(iLinkID, sDirection) 'located below

	response.redirect("/Admin/States/LinkList.asp?StateID=" & iStateID)
end if

set objConn = nothing
set oFileUp = nothing
%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">The link was <%= sAction %> successfully.</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td><a href="LinkList.asp?StateID=<%=iStateID%>">Return to the Link Listing</a></td>	
	</tr>	
	<tr>
		<td><a href="StateList.asp">Return to the State Listing</a></td>	
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
'Resorts the links
Sub SortLinks(p_iLinkID,p_sDirection) 
	dim objConn 'as object
	dim sSQL 'as string
	dim rs 'as object
	dim iOldSort, iNewSort 'as integer
	dim iTmpLinkID 'as integer
	
	set objConn = server.CreateObject("ADODB.Connection")
	objConn.ConnectionString = application("sDataSourceName")
	objConn.Open
				
	'Retrieve old sort value
	sSQL = "SELECT Sort FROM StatesLinks WHERE ID = '" & p_iLinkID & "' "
	set rs = objConn.execute(sSQL)

	if not rs.eof then
		iOldSort = rs("Sort")
				
		'Determine the new sort
		if (Ucase(p_sDirection) = "DOWN") then
			iNewSort = iOldSort + 1
		elseif (Ucase(p_sDirection) = "UP") then
			iNewSort = iOldSort - 1
		end if

		'Get the ID of the Link that is currently set to the New Sort value
		sSQL = "SELECT ID FROM StatesLinks WHERE Sort = '" & iNewSort & "' AND StateID = '" & iStateID & "'"
		set rs = objConn.execute(sSQL)
		
		if not rs.eof then
			iTmpLinkID = rs("ID")
			
			'Change the sort for the first link
			sSQL = "UPDATE StatesLinks " & _
				   "SET Sort = '" & iNewSort & "' " & _
				   "WHERE ID = '" & p_iLinkID & "' "
			objConn.execute(sSQL)
				
			if err.number = 0 then
				'Change the sort for the second lesson
				sSQL = "UPDATE StatesLinks " & _
					   "SET Sort = '" & iOldSort & "' " & _
					   "WHERE ID = '" & iTmpLinkID & "' "
				objConn.execute(sSQL)
			end if
		end if
	end if

	set objConn = nothing
	set rs = nothing
	
	XMLReorder
	
end sub

'Moves Links UP one in the sort list starting from the specified LinkID
Sub ReOrderLinks(p_iLinkID)
	dim objConn 'as object
	dim sSQL 'as string
	dim rs,rs2 'as object
	dim iSort, iNewSort 'as integer

	set objConn = server.CreateObject("ADODB.Connection")
	objConn.ConnectionString = application("sDataSourceName")
	objConn.Open
	
	'Retrieve Sort of the Link
	sSQL = "SELECT Sort FROM StatesLinks WHERE ID = '" & p_iLinkID & "' "
	set rs = objconn.execute(sSQL)
		
	if not rs.eof then
		iSort = rs("Sort")
			
		'Select all Links after this Link
		sSQL = "SELECT ID FROM StatesLinks WHERE Sort > " & iSort & " "
		set rs2 = objconn.execute(sSQL)
		iNewSort = iSort
			
		do while not rs2.EOF
			'change the sort for the current Link
			sSQL = "UPDATE StatesLinks " & _
				   "SET Sort = '" & iNewSort & "' " & _
				   "WHERE ID = '" & rs2("ID") & "' "
			objConn.execute(sSQL)
				
			rs2.MoveNext
			iNewSort = iNewSort + 1
		loop
	end if
		
	set objConn = nothing
	set rs = nothing
	set rs2 = nothing
End Sub


'Removes all links and adds them back in the current preferred order.
sub XMLReorder()

	dim oXML 'Get the XML Parser object
	set oXML = Server.CreateObject("Microsoft.XMLDOM")

	oXML.Async = False 'Do not return asynchronously
	oXML.ValidateOnParse = False 'Do not validate
	
	'Load the document
	oXML.Load(application("sAbsMapXMLPath"))

	'Check for any errors on loading
	if oXML.ParseError.ErrorCode <> 0 then
		Response.Write("Error: Could not load XML data file. " & oXML.ParseError.reason & "<br>" & vbCrLf)
		exit sub
	end if

	'Select the specific state node based on the state abbreviation we were
	'passed initially.
	dim oNode
	set oNode = oXML.selectSingleNode("/country/" & _
		"state[@abbreviation='" & sStateAbbrev & "']/menu")

	if oNode is nothing then
		Response.Write("Error: Unable to locate node.<br>" & vbCrLf)
		exit sub
	end if

	'Find the child nodes and delete them.
	dim oSelection
	set oSelection = oNode.selectNodes("item")
	oSelection.removeAll
	

	'Get the records of the link items and add them back to the XML file
	'in order.
	dim oConn
	dim sSql
	dim oRs
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open	
	
	'Get the links ordered by Sort
	sSql = "SELECT Name, Link FROM StatesLinks " & _
		   "WHERE StateID = '" & iStateID & "'" & _
		   "ORDER BY Sort"
	set oRs = oConn.Execute(sSql)
	
	dim oNewElement
	dim oNewAttribute
	
	do while not oRs.EOF
	
		'Create the new XML element we'll be adding.
		set oNewElement = oXML.CreateElement("item")
		set oNewAttribute = oXML.createAttribute("action")
		oNewElement.setAttributeNode(oNewAttribute)
		set oNewAttribute = oXML.createAttribute("btarget")
		oNewElement.setAttributeNode(oNewAttribute)
	
		'Append it to the XML doc
		oNode.AppendChild(oNewElement)
	
		'Set the properties.
		oNode.LastChild.Text = oRs("Name")
		oNode.LastChild.attributes.getNamedItem("action").value = oRs("Link")
		oNode.LastChild.attributes.getNamedItem("btarget").value = "_self"
		
		oRs.MoveNext
		
	loop

	
	'Save the modified XML doc
	oXML.Save(application("sAbsMapXMLPath"))
	
	set oNewElement = nothing
	set oNewAttribute = nothing	
	
	set oRs = nothing
	set oConn = nothing
	
	set oXML = nothing
	set oNode = nothing
	set oSelection = nothing

end sub


'Removes a specific link from the XML file.  
sub XMLRemoveLink(p_sStateAbbrev, p_sName, p_sAction)

	'Verify parameters
	if p_sStateAbbrev = "" or p_sName = "" then
		Response.Write("Error: Invalid parameters while removing a link.<br>" & vbCrLf)
		Response.Write("p_sStateAbbrev: " & p_sStateAbbrev & "<br>")
		Response.Write("p_sName: " & p_sName)
		exit sub
	end if
	
	
	dim oXML 'Get the XML Parser object
	set oXML = Server.CreateObject("Microsoft.XMLDOM")
	
	oXML.Async = False 'Do not return asynchronously
	oXML.ValidateOnParse = False 'Do not validate
	
	'Load the document
	oXML.Load(application("sAbsMapXMLPath"))
	
	'Check for any errors on loading
	if oXML.ParseError.ErrorCode <> 0 then
		Response.Write("Error: Could not load XML data file. " & oXML.ParseError.reason & "<br>" & vbCrLf)
		exit sub
	end if
	
	'Select the specific state node based on the state abbreviation we were
	'passed initially.
	dim oNode
	set oNode = oXML.selectSingleNode("/country/" & _
		"state[@abbreviation='" & p_sStateAbbrev & "']/menu/item[@action='" & p_sAction & "']")

	if oNode is nothing then
		Response.Write("Error: Unable to locate child node.<br>" & vbCrLf)
		exit sub
	end if
	
	
	'Select the parent node
	dim oParentNode
	set oParentNode = oXML.selectSingleNode("/country/" & _
		"state[@abbreviation='" & p_sStateAbbrev & "']/menu")
		
	if oParentNode is nothing then
		Response.Write("Error: Unable to locate parent node.<br>" & vbCrLf)
		exit sub
	end if
	
	
	'Remove the XML element
	oParentNode.removeChild(oNode)
	
	
	set oParentNode = nothing
	set oNode = nothing

	oXML.Save(application("sAbsMapXMLPath"))
	
end sub


'Updates the XML file to reflect new state links
sub XMLAddLink(p_sStateAbbrev, p_sAction, p_sName)

	'Verify parameters
	if p_sStateAbbrev = "" or p_sAction = "" or p_sName = "" then
		Response.Write("Error: Invalid parameters while adding link.<br>" & vbCrLf)
		exit sub
	end if


	dim oXML 'Get the XML Parser object
	set oXML = Server.CreateObject("Microsoft.XMLDOM")
	
	oXML.Async = False 'Do not return asynchronously
	oXML.ValidateOnParse = False 'Do not validate
	
	'Load the document
	oXML.Load(application("sAbsMapXMLPath"))
	
	'Check for any errors on loading
	if oXML.ParseError.ErrorCode <> 0 then
			Response.Write("Error: Could not load XML data file. " & oXML.ParseError.reason & "<br>" & vbCrLf)
		exit sub
	end if
	
	
	'Select the specific state node based on the state abbreviation we were    
	'passed initially.
	dim oNode
	set oNode = oXML.selectSingleNode("/country/" & _
		"state[@abbreviation='" & p_sStateAbbrev & "']/menu")
	
	if oNode is nothing then
		Response.Write("Error: Unable to locate state node.<br>" & vbCrLf)
		exit sub	
	end if
		
	
	'Create the new XML element we'll be adding.
	dim oNewElement
	set oNewElement = oXML.CreateElement("item")
	dim oNewAttribute
	set oNewAttribute = oXML.createAttribute("action")
	oNewElement.setAttributeNode(oNewAttribute)
	set oNewAttribute = oXML.createAttribute("btarget")
	oNewElement.setAttributeNode(oNewAttribute)
	
	'Append it to the XML doc
	oNode.AppendChild(oNewElement)
	
	'Set the properties.
	oNode.LastChild.Text = p_sName
	oNode.LastChild.attributes.getNamedItem("action").value = p_sAction
	oNode.LastChild.attributes.getNamedItem("btarget").value = "_self"

	
	'Save the modified XML doc
	oXML.Save(application("sAbsMapXMLPath"))
	
end sub


%>
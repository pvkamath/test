<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" -------------->
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/State.Class.asp" -------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"

'Verify that the user has logged in.
'CheckIsLoggedIn()

'Verify directory security
dim bHasDirectoryAccess
dim sPagePath
dim iLocation

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(session("User_ID"), sPagePath) 'security.asp


	
'Configure the administration submenu options
'bAdminSubmenu = false
'sAdminSubmenuType = "BROKERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
'iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))
'
'dim oCompany
'dim saRS
dim sMode
dim iBusinessType
'
'set oCompany = new Company
'oCompany.ConnectionString = application("sDataSourceName")
'oCompany.TpConnectionString = application("sTpDataSourceName")
'oCompany.VocalErrors = application("bVocalErrors")
'
'if iCompanyId <> "" then
'
'	if oCompany.LoadCompanyById(iCompanyId) = 0 then
'	
'		'The load was unsuccessful.  Die.
'		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
'		Response.end
'		
'	end if
'	
'else
'	
'	Response.Write("Company ID required.")
'	Response.End
'	
'end if 



'Load the passed State Requirements
dim oState
dim iStateId
dim sStateAbbrev
sStateAbbrev = ScrubForSql(request("state"))
set oState = new State
oState.ConnectionString = application("sDataSourceName")
oState.VocalErrors = false 'application("bVocalErrors")
oState.StateAbbrev = sStateAbbrev

if sStateAbbrev = "" then 

	Response.Write("This page requires a State reference.")
	Response.End

else

	if oState.LoadStateByAbbrev(sStateAbbrev) = 0 then

		iStateId = GetStateIdByAbbrev(sStateAbbrev)
		oState.StateName = GetStateName(iStateId, 0)
		oState.StateAbbrev = GetStateName(iStateId, 1)
		'oState.CompanyId = iCompanyId
		sMode = "Add"
		
	else
	
		sMode = "Edit"
		
	end if 
	
end if


'Content form variables
dim sText
sText = oState.RequirementsText

%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(frm)
	{
		if ((frm.StateName.value == "") || (frm.StateAbbrev.value == "")) {
			alert("State Name and Abbreviation are required Fields!");
			frm.StateName.focus();
			return false;
		}
			
		frm.Text.value = idContent.document.body.innerHTML;

		if (frm.Text.value == "") 
		{
			alert("Body Text is a required Field!");
			//frm.Text.focus();
			return false;
		}
			
		if (frm.EditHTML.checked) 
		{
			alert("Please un-check the 'Edit HTML' checkbox");
			frm.EditHTML.focus();
			return false;
		}
			
		//Make sure the Text value is not over 100,000 chars, if gets far over this limit it will cause a stack overflow
		sText = frm.Text.value;
	
		if (sText.length > 100000)
		{
			alert("Section Content has " + sText.length + " chars.\nThe maximum length is 100,000 chars.");
			return false;
		}

		return true;
	}

</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- State Guide -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Edit a State Profile</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="StateForm" action="modstateproc.asp" method="POST" onSubmit="return Validate(this);">
<input type="hidden" value="<% = sStateAbbrev %>" name="state">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><input type="text" name="StateName" value="<% = oState.StateName %>" size="30" maxlength="50"> <input type="text" name="StateAbbrev" value="<% = oState.StateAbbrev %>" size="3" maxlength="2"></td>
										</tr>
										<tr>
											<td colspan="2" nowrap>State Requirements:<br>
<input type="hidden" name="Text"><!-- #include virtual="/admin/TextFX1.0/Format.asp" -->
											</td>
										</tr>
										<% 
										'We're removing the following since they're being replaced with a single text content field.
										if 1 = 2 then
										%>
										<tr>
											<td class="newstitle" nowrap>Regulated By: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><textarea name="RegulatedBy" rows="6" cols="28"><% = oState.RegulatedBy %></textarea></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>CE Requirements: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><textarea name="CeRequirements" rows="6" cols="28"><% = oState.CeRequirements %></textarea></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>Licenses: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><textarea name="Licenses" rows="6" cols="28"><% = oState.Licenses %></textarea></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>License Requirements: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><textarea name="LicenseRequirements" rows="6" cols="28"><% = oState.LicenseRequirements %></textarea></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fingerprints: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="Fingerprints" value="<% = oState.Fingerprints %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>Renewals: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><textarea name="Renewals" rows="6" cols="28"><% = oState.Renewals %></textarea></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Website: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="Website" value="<% = oState.Website %>" size="30" maxlength="50"></td>
										</tr>
										<!--
										<tr>
											<td class="newstitle" nowrap>Expire Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
												<input type="text" name="ExpireDate" value="<% = oState.ExpireDate %>" size="10" maxlength="10">
												<a href="javascript:show_calendar('StateForm.IncorpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('StateForm.ExpireDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
											</td>
										</tr>
										-->
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3"><b>Mailing Address</b></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="MailingAddress" value="<% = oState.MailingAddress %>" size="30" maxlength="100"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="MailingAddress2" value="<% = oState.MailingAddress2 %>" size="30" maxlength="100"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="MailingCity" value="<% = oState.MailingCity %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% call DisplayStatesDropDown(oState.MailingStateId,1,"MailingStateId") 'functions.asp %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="MailingZipcode" value="<% = oState.MailingZipcode %>" size="6" maxlength="5"> - <input type="text" name="MailingZipcodeExt" value="<% = oState.MailingZipcodeExt %>" size="5" maxlength="4"></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3"><b>State Contact</b></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactName" value="<% = oState.ContactName %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactPhone" value="<% = oState.ContactPhone %>" size="30" maxlength="50"> Ext. <input type="text" name="ContactPhoneExt" value="<% = oState.ContactPhoneExt %>" size="11" maxlength="10"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactPhone2" value="<% = oState.ContactPhone2 %>" size="30" maxlength="50"> Ext. <input type="text" name="ContactPhone2Ext" value="<% = oState.ContactPhone2Ext %>" size="11" maxlength="10"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactFax" value="<% = oState.ContactFax %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactEmail" value="<% = oState.ContactEmail %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alternate Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactEmail2" value="<% = oState.ContactEmail2 %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactAddress" value="<% = oState.ContactAddress %>" size="30" maxlength="100"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactAddress2" value="<% = oState.ContactAddress2 %>" size="30" maxlength="100"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactCity" value="<% = oState.ContactCity %>" size="30" maxlength="50"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% call DisplayStatesDropDown(oState.ContactStateId,1,"ContactStateId") 'functions.asp %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="ContactZipcode" value="<% = oState.ContactZipcode %>" size="6" maxlength="5"> - <input type="text" name="ContactZipcodeExt" value="<% = oState.ContactZipcodeExt %>" size="5" maxlength="4"></td>
										</tr>
										<%
										end if 
										%>
	<tr>
		<td colspan="2" align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<%

'set saRS = nothing
'set oCompany = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
													' From htmlElements.asp
%>
<script language=javascript>
		setTimeout('InsertText();', 1000);
</script>
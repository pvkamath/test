<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" -------------->
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/State.Class.asp" -------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"

'Verify that the user has logged in.
'CheckIsLoggedIn()

'Verify directory security
dim bHasDirectoryAccess
dim sPagePath
dim iLocation

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(session("User_ID"), sPagePath) 'security.asp


	
'Configure the administration submenu options
'bAdminSubmenu = false
'sAdminSubmenuType = "BROKERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%

	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

'dim oCompany
'dim saRS
dim sMode
dim iBusinessType

'set oCompany = new Company
'oCompany.ConnectionString = application("sDataSourceName")
'oCompany.TpConnectionString = application("sTpDataSourceName")
'oCompany.VocalErrors = application("bVocalErrors")
'
'if iCompanyId <> "" then
'
'	if oCompany.LoadCompanyById(iCompanyId) = 0 then
'	
'		'The load was unsuccessful.  Die.
'		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
'		Response.end
'		
'	end if
'	
'else
'	
'	Response.Write("Company ID required.")
'	Response.End
'	
'end if 



'Load the passed State Requirements
dim oState
dim iStateId
dim sStateAbbrev
sStateAbbrev = ucase(ScrubForSql(request("state")))
set oState = new State
oState.ConnectionString = application("sDataSourceName")
oState.VocalErrors = false 'application("bVocalErrors")
oState.StateAbbrev = sStateAbbrev

if sStateAbbrev = "" then 

	Response.Write("This page requires a State reference.")
	Response.End

else

	if oState.LoadStateByAbbrev(sStateAbbrev) = 0 then

		iStateId = GetStateIdByAbbrev(sStateAbbrev)
		oState.StateName = GetStateName(iStateId, 0)
		oState.StateAbbrev = GetStateName(iStateId, 1)
		'oState.CompanyId = iCompanyId
		sMode = "Add"
		
	else
	
		sMode = "Edit"
		
	end if 
	
end if

'Assign the state properties based on the passed form data
oState.RequirementsText = ScrubForSql(request("Text"))
oState.ContactName = ScrubForSql(request("ContactName"))
oState.ContactPhone = ScrubForSql(request("ContactPhone"))
oState.ContactPhoneExt = ScrubForSql(request("ContactPhoneExt"))
oState.ContactPhone2 = ScrubForSql(request("ContactPhone2"))
oState.ContactPhone2Ext = ScrubForSql(request("ContactPhone2Ext"))
oState.ContactFax = ScrubForSql(request("ContactFax"))
oState.ContactEmail = ScrubForSql(request("ContactEmail"))
oState.ContactEmail2 = ScrubForSql(request("ContactEmail2"))
oState.ContactAddress = ScrubForSql(request("ContactAddress"))
oState.ContactAddress2 = ScrubForSql(request("ContactAddress2"))
oState.ContactCity = ScrubForSql(request("ContactCity"))
oState.ContactStateId = ScrubForSql(request("ContactStateId"))
oState.ContactZipcode = ScrubForSql(request("ContactZipcode"))
oState.ContactZipcodeExt = ScrubForSql(request("ContactZipcodeExt"))
oState.MailingAddress = ScrubForSql(request("MailingAddress"))
oState.MailingAddress2 = ScrubForSql(request("MailingAddress2"))
oState.MailingCity = ScrubForSql(request("MailingCity"))
oState.MailingStateId = ScrubForSql(request("MailingStateId"))
oState.MailingZipcode = ScrubForSql(request("MailingZipcode"))
oState.MailingZipcodeExt = ScrubForSql(request("MailingZipcodeExt"))
oState.RegulatedBy = ScrubForSql(request("RegulatedBy"))
oState.CeRequirements = ScrubForSql(request("CeRequirements"))
oState.Hours = ScrubForSql(request("Hours"))
oState.ExpireDate = ScrubForSql(request("ExpireDate"))
oState.Licenses = ScrubForSql(request("Licenses"))
oState.LicenseRequirements = ScrubForSql(request("LicenseRequirements"))
oState.Fingerprints = ScrubForSql(request("Fingerprints"))
oState.Renewals = ScrubForSql(request("Renewals"))
oState.Website = ScrubForSql(request("Website"))
oState.LastEdit = now()



'If this is a new State record we need to save it before it will have a
'unique ID.
dim iNewStateId

if oState.SaveState = 0 then

	Response.Write("Error: Failed to save the State Requirements record.")
	Response.End

end if 
%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			(!checkString(FORM.Name, "Name") && !checkString(FORM.BranchNum, "Branch Number"))
			|| !checkString(FORM.Address, "Address")
			|| !checkString(FORM.City, "City")
			|| !checkString(FORM.State, "State")
			|| !checkZIPCode(FORM.Zipcode, 0)
		   )
			return false;

		return true;
	}
</script>

			<table width=760  border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- State Guide -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Edit a State Profile</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>
<b>The state record was saved successfully.</b>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
'set oCompany = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
													' From htmlElements.asp
%>
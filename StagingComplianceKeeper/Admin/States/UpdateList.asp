<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/StateUpdate.Class.asp" ------------------------------------------------->


<%
dim rs
dim sSQL
dim sClass
dim oUpdate
dim iSearchStateId
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sAction
'dim sSearchEmail, sSearchLastName, iSearchCompanyId, iSearchProviderID, iSearchBranchID, iSearchUserStatus
'dim bBranchesDropDownDisabled


set oUpdate = new StateUpdate
oUpdate.ConnectionString = application("sDataSourceName")
oUpdate.VocalErrors = application("bVocalErrors")

sAction = trim(request("Action"))

if Ucase(sAction) = "NEW" then
	Session.Contents.Remove("UpdateSearchStateId") 
else
	'Get Search values
	iSearchStateId = ScrubForSql(request("SearchStateId"))
	if (iSearchStateId  = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("UpdateSearchStateId")) <> "") then
			iSearchStateId = session("UpdateSearchStateId")
		end if
	else
		session("UpdateSearchStateId") = iSearchStateId
	end if
	oUpdate.StateId = iSearchStateId
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("UserCurPage")) <> "") then
			iCurPage = Session("UserCurPage")
		end if
	else
		Session("UserCurPage") = iCurPage
	end if

end if	


set rs = oUpdate.SearchUpdates()

If iCurPage = "" Then iCurPage = 1
If iMaxRecs = "" Then iMaxRecs = 20

%>

<script language="JavaScript">

function ConfirmDelete(p_iUpdateID)
{
	if (confirm("Are you sure you wish to delete this update notice?"))
		window.location.href = 'DeleteUpdateProc.asp?UpdateID=' + p_iUpdateID;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">State Requirements Updates</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="UpdateList.asp" method="POST">
<input type="hidden" name="action" value="SEARCH">
<input type="hidden" name="page_number" value="1">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>State:</b></td>
		<td><% call DisplayStatesDropDown(iSearchStateId,0,"SearchStateId") %></td>				
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>
</table>
</form>

<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; State &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Update Date &nbsp;</td>					
		<td class="reportHeaderText" width="125">&nbsp; Delete &nbsp;</td>			
	</tr>
<%
	if not rs.eof then

		'Set the number of records displayed on a page
		rs.PageSize = iMaxRecs
		rs.Cachesize = iMaxRecs
		iPageCount = rs.PageCount
	
		'determine which search page the user has requested.
		If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
		If clng(iCurPage) <= 0 Then iCurPage = 1
			
		'Set the beginning record to be display on the page
		rs.AbsolutePage = iCurPage		
		
		iCount = 0	
		do while (iCount < rs.PageSize) AND (not rs.eof)
		
			if oUpdate.LoadUpdateById(rs("UpdateId")) <> 0 then
		
				if sClass = "row2" then
					sClass = "row1"
				else
					sClass = "row2"
				end if		
			
				response.write("<tr class=""" & sClass & """>" & vbcrlf)
			
				response.write("<td><a href=""ModUpdate.asp?UpdateID=" & oUpdate.UpdateId & """>" & oUpdate.LookupState(oUpdate.StateId) & "</a></td>" & vbcrlf)
				response.write("<td>" & oUpdate.UpdateDate & "</td>" & vbcrlf)
			
				response.write("<td align=""center""><a href=""javascript:ConfirmDelete(" & oUpdate.UpdateId & ");"">Delete</a></td>" & vbcrlf)
			
				response.write("</tr>" & vbcrlf)
			
			end if
			
			rs.MoveNext
			iCount = iCount + 1		
		loop

		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
		
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td colspan=""4"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""UpdateList.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""UpdateList.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if

		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)	
		response.write("</table>" & vbcrlf)
	
		'display Page number
		response.write("<table width=""100%"">" & vbcrlf)	
		response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)	
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)		
	else
		response.write("<tr><td colspan=""4"">There are currently no updates that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>	
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUpdate = nothing
set rs = nothing
%>
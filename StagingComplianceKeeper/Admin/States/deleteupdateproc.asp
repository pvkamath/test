<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/StateUpdate.Class.asp" ------------------------------------------------->

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Delete a State Requirements Update</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>

<%

dim iUpdateId
iUpdateId = ScrubForSql(request("UpdateId"))

dim oUpdate
set oUpdate = new StateUpdate
oUpdate.ConnectionString = application("sDataSourceName")
oUpdate.VocalErrors = application("bVocalErrors")


'Check for a valid Update ID.
if iUpdateId <> "" then

	'We need to have an object loaded before we can delete it.
	if oUpdate.LoadUpdateById(iUpdateId) = 0 then
		
		'The load was unsuccessful
		Response.Write("<b>Failed to load the passed Update ID.</b>")
		Response.End
		
	end if
	
	oUpdate.Deleted = 1

	if oUpdate.SaveUpdate() = 0 then
		
		'The delete failed.  
		Response.Write("<b>Failed to delete the Update.</b>")
		Response.End
		
	end if

end if

%>

<b>Successfully deleted the <% = oUpdate.LookupState(oUpdate.StateId) %> state requirements update.</b>

<%
set oUpdate = nothing
%>

<p>
<a href="UpdateList.asp">Return to Update Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

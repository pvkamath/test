<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/StateUpdate.Class.asp" ------------------------------------------------->


<%

dim iUpdateId
iUpdateId = ScrubForSql(request("UpdateId"))

dim oUpdate
dim sMode

set oUpdate = new StateUpdate
oUpdate.ConnectionString = application("sDataSourceName")
oUpdate.VocalErrors = application("bVocalErrors")

if iUpdateId <> "" then

	if oUpdate.LoadUpdateById(iUpdateId) = 0 then
	
		'The load was unsuccessful.
		Response.write("Failed to load the passed Update ID.")
		Response.end
		
	end if
	
	sMode = "Edit"
	
else

	sMode = "Add"
	
end if


%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.UpdateDate, "Update Date")
			|| !checkString(FORM.StateId, "State")
			|| !checkString(FORM.Text, "Update Notice")
		   )
			return false;

		return true;
	}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a State Requirements Update</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="UpdateForm" action="modupdateproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the UpdateID on to the next step so we know we're working with an existing
'Update.
if iUpdateId <> "" then
%>
<input type="hidden" value="<% = iUpdateId %>" name="UpdateId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>State: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oUpdate.StateId,1,"StateId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Date of Update: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="UpdateDate" value="<% = oUpdate.UpdateDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('UpdateForm.UpdateDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('UpdateForm.UpdateDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Update Notice: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="text" cols="50" rows="5"><% = oUpdate.Text %></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif" id=image1 name=image1>&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


<%
set oUpdate = nothing
%>
<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/StateUpdate.Class.asp" ------------------------------------------------->

<%

dim iUpdateId
iUpdateId = ScrubForSql(request("UpdateId"))

dim oUpdate
dim sMode

set oUpdate = new StateUpdate
oUpdate.ConnectionString = application("sDataSourceName")
oUpdate.VocalErrors = application("bVocalErrors")

if iUpdateId <> "" then

	if oUpdate.LoadUpdateById(iUpdateId) = 0 then
	
		'The load was unsuccessful.
		Response.write("Failed to load the passed Update ID.")
		Response.end
		
	end if
	
	sMode = "Edit"
	
else

	sMode = "Add"
	
end if


'Assign the properties based on our passed parameters
oUpdate.StateId = ScrubForSql(request("StateId"))
oUpdate.UpdateDate = ScrubForSql(request("UpdateDate"))
oUpdate.Text = ScrubForSql(request("Text"))


'Save the update object
if oUpdate.SaveUpdate = 0 then

	Response.Write("Error: Failed to save new State Requirements Update.")
	Response.End

end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a State Requirements Update</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new State Requirements Update for <% = oUpdate.LookupState(oUpdate.StateId) %> was successfully created.</b>
<%
else
%>
<b>The State Requirements Update for "<% = oUpdate.LookupState(oUpdate.StateId) %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="UpdateList.asp">Return to Update Listing</a>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
%>
 <%
 dim bClipboard
dim bFontDecoration
dim bLists
dim bForeColor
dim bLink
dim bImage
dim bStyle
dim bFont
dim bSize 
dim bAlignment
dim bIndents
dim TextFXGif
dim cID

 'TextFX Options
		 bClipboard = True
		 bFontDecoration = True
		 bLists = True
		 bForeColor = True
		 bLink = false
		 bImage = bShowImageUpload
		 bStyle = False 
		 bFont = False 
	     bSize = False 
 		 bAlignment = true 
	     bIndents = true 
	     TextFXGif = "/admin/TextFX1.0/images/"
%>
<STYLE TYPE="text/css">

TABLE#tblCoolbar 
       { 
       background-color:#EAEAEA; padding:1px; color:#EAEAEA; 
       border-width:1px; border-style:solid; 
       border-color:#EAEAEA
       }
.cbtn
       {
       height:18;
       BORDER-LEFT: #EAEAEA 1px solid;
       BORDER-RIGHT: #EAEAEA 1px solid;
       BORDER-TOP: #EAEAEA 1px solid;
       BORDER-BOTTOM: #EAEAEA 1px solid; 
       }

.txtbtn {font-family:trebuchet; font-size:70%; color:menutext;}

</STYLE>

<script language="Javascript">
	function EnableFormatting(){	//changes the bFormattingEnabled boolean to True so that the functions will run
		bFormattingEnabled = true
		//alert(bFormattingEnabled);
	}

	function DisableFormatting(){	//changes the bFormattingEnabled boolean to False so that the functions will not run
		//if (window.event.srcElement.name != "BodyText")
		bFormattingEnabled = false;
		//alert(bFormattingEnabled);
	}	

function button_over(eButton)
       {
       eButton.style.backgroundColor = "#EAEAEA";
       eButton.style.borderColor = "#EAEAEA #EAEAEA #EAEAEA #EAEAEA";
       }

function button_out(eButton)
       {
       eButton.style.backgroundColor = "#EAEAEA";
       eButton.style.borderColor = "#EAEAEA";
       }

function button_down(eButton)
       {
       eButton.style.backgroundColor = "#EAEAEA";
       eButton.style.borderColor = "#EAEAEA #EAEAEA #EAEAEA #EAEAEA";
       }

function button_up(eButton)
       {
       eButton.style.backgroundColor = "#EAEAEA";
       eButton.style.borderColor = "#EAEAEA #EAEAEA #EAEAEA #EAEAEA";
       eButton = null; 
       }

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


var isHTMLMode=false

function document.onreadystatechange()
       {
       //idContent.document.designMode="On"
       }

function cmdExec(cmd,opt) 
       {
       if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
       idContent.document.execCommand(cmd,"",opt);idContent.focus();
       }

function setMode(bMode)
       {
       var sTmp;
       isHTMLMode = bMode;
       if (isHTMLMode){sTmp=idContent.document.body.innerHTML;idContent.document.body.innerText=sTmp;} 
       else {sTmp=idContent.document.body.innerText;idContent.document.body.innerHTML=sTmp;}
       idContent.focus();
       }

function createLink()
       {
       if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
       cmdExec("CreateLink");
       }

function Save() {
	if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}
	
	var sImgTag = idContent.document.body.all.tags("IMG");
	var oImg;
	
	for (var i = sImgTag.length - 1; i >= 0; i--) {
		oImg = sImgTag[i];
		alert("Add your code to Upload local image file here. Image Inserted : " + oImg.src );
	}
	
	alert("Add your code to Save Document here");
	alert("Your Document : " + idContent.document.body.innerHTML);
}

function foreColor()
       {
       var arr = showModalDialog("/Admin/TextFX1.0/selcolor.htm","","font-family:Verdana; font-size:12; dialogWidth:30em; dialogHeight:34em" );
       if (arr != null) cmdExec("ForeColor",arr);      
       }

function insertImage(p_image){

    if (isHTMLMode){alert("Please uncheck 'Edit HTML'");return;}

    //var sImgSrc=prompt("Insert Image File (You can use your local image file) : ", "http://www.aspalliance.com/Yusuf/Article10/sample.jpg");
	var sImgSrc = p_image
    if(sImgSrc!=null) cmdExec("InsertImage",sImgSrc);
}

function OpenImageWindow(imageNum,p_sTextbox){

	//window.open("modHPImageWin.asp?im=" + imageNum, "ImageWindow", "width=500,height=500")
	window.open("/Admin/ContentManager/UploadImageWin.asp?im=" + imageNum + "&textbox=" + p_sTextbox , "ImageWindow", "width=500,height=100")
}	

function RefreshImage(p_imageName){
	document.curImage.src = "/media/images/spacer.gif"
	document.curImage.src = "/usermedia/images/" + p_imageName
}

function InsertText() {
	// replace double quotes with a javascript escaped double quote
	// replace vbcrlf with a new line character
	// replace "about:blank" with an empty string -- for some reason this text editor adds that into the source 
	//		which breaks the image

	document.frames("idContent").document.body.innerHTML = "<%=replace(replace(replace(sText,"""","\"""), vbcrlf, "\n"),"about:blank","")%>";
}

</script>

<table id="tblCoolbar" width=540 cellpadding="0" cellspacing="0">

<tr valign="middle">
	<td colspan=16>
		<%If bStyle Then%>
              <select onchange="cmdExec('formatBlock',this[this.selectedIndex].value);this.selectedIndex=0">
	              <option selected>Style</option>
	              <option value="Normal">Normal</option>
	              <option value="Heading 1">Heading 1</option>
	              <option value="Heading 2">Heading 2</option>
	              <option value="Heading 3">Heading 3</option>
	              <option value="Heading 4">Heading 4</option>
	              <option value="Heading 5">Heading 5</option>
	              <option value="Address">Address</option>
	              <option value="Formatted">Formatted</option>
	              <option value="Definition Term">Definition Term</option>
              </select> 
		<%End If%>
		
		<%If bFont Then%>
              <select onchange="cmdExec('fontname',this[this.selectedIndex].value);">

                     <option selected>Font</option>

                     <option value="Arial">Arial</option>

                     <option value="Arial Black">Arial Black</option>

                     <option value="Arial Narrow">Arial Narrow</option>

                     <option value="Comic Sans MS">Comic Sans MS</option>

                     <option value="Courier New">Courier New</option>

                     <option value="System">System</option>

                     <option value="Tahoma">Tahoma</option>

                     <option value="Times New Roman">Times New Roman</option>

                     <option value="Verdana">Verdana</option>

                     <option value="Wingdings">Wingdings</option>

              </select> 
		<%End If%>
		
		<%If bSize Then%>
              <select onchange="cmdExec('fontsize',this[this.selectedIndex].value);">

                     <option selected>Size</option>

                     <option value="1">1</option>

                     <option value="2">2</option>

                     <option value="3">3</option>

                     <option value="4">4</option>

                     <option value="5">5</option>

                     <option value="6">6</option>

                     <option value="7">7</option>

                     <option value="8">8</option>

                     <option value="10">10</option>

                     <option value="12">12</option>

                     <option value="14">14</option>

              </select> 
		<%End If%>
       </td>
</tr>

<tr>
	<%If bClipboard Then%>
       <td><div class="cbtn" onClick="cmdExec('cut')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Cut.gif" alt="Cut">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('copy')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Copy.gif" alt="Copy">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('paste')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Paste.gif" alt="Paste">

       </div></td>   
	<%End If%>

	<%If bFontdecoration Then%>
       <td><div class="cbtn" onClick="cmdExec('bold')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Bold.gif" alt="Bold">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('italic')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Italic.gif" alt="Italic">

       </div></td>   

       <td><div class="cbtn" onClick="cmdExec('underline')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Under.gif" alt="Underline">

       </div></td>          
	<%End If%>     

	<%If bAlignment Then%>
       <td><div class="cbtn" onClick="cmdExec('justifyleft')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Left.gif" alt="Justify Left">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('justifycenter')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Center.gif" alt="Center">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('justifyright')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="1" vspace=1 align=absmiddle src="<%=TextFXGif%>Right.gif" alt="Justify Right">

       </div></td>
	<%End If%>

	<%If bLists Then%>
       <td><div class="cbtn" onClick="cmdExec('insertorderedlist')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>numlist.GIF" alt="Ordered List">

       </div></td>

       <td><div class="cbtn" onClick="cmdExec('insertunorderedlist')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>bullist.GIF" alt="Unordered List">

       </div></td>
	<%End If%>
	
	<%If bIndents Then%>
       <td><div class="cbtn" onClick="cmdExec('outdent')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>deindent.gif" alt="Decrease Indent">

       </div></td>
	   
       <td><div class="cbtn" onClick="cmdExec('indent')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>inindent.gif" alt="Increase Indent">

       </div></td>
	<%End If%>

	<%If bForecolor Then%>
       <td><div class="cbtn" onClick="foreColor()" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>fgcolor.gif" alt="Forecolor">

       </div></td>                
	<%End If%>
 
	<%If bLink Then%>
       <td><div class="cbtn" onClick="cmdExec('createLink')" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>Link.gif" alt="Link">

       </div></td>   
	<%End If%>
       
	<%If bImage Then%>
       <!--<td><div class="cbtn" onClick="insertImage()" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">
    --><td><div class="cbtn" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">
       <a href="javascript:OpenImageWindow('<%=cID%>','');"><img border="0" hspace="2" vspace=1 align=absmiddle src="<%=TextFXGif%>btn_upload.gif" alt="Image"></a>
       </div></td>          
	<%End If%>

       <!--<td><div class="cbtn" onClick="Save()" onmouseover="button_over(this);" onmouseout="button_out(this);" onmousedown="button_down(this);" onmouseup="button_up(this);">

       <img  hspace="2" vspace=1 align=left align=absmiddle src="<%=TextFXGif%>Save.gif" alt="Save">

       <font class=txtbtn>Save&nbsp;&nbsp;</font>

       </div></td>

       <td width=200></td>-->

</tr>

</table>


<iframe onfocus="EnableFormatting();" onLoad="InsertText();"width="540" name="idContent" id="idContent" height="225">
</iframe> 
<br>
<script language="Javascript">
idContent.document.designMode="On"
</script>

<input type="checkbox" onclick="setMode(this.checked)" name="EditHTML"> Edit HTML

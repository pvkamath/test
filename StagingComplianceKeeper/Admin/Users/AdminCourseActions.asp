<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>

<%
dim iUserID 'as integer
dim iUserCourseID 'as integer
dim sSort 'as string
dim oUserObj 'as object
dim rs 'as object
dim sAction 'as string
dim iCurPage 'as integer
dim iMaxRecs 'as integer
dim sCourseName 'as string
dim iCount, iPageCount 'as integer
dim sClass 'as string

iUserID = scrubforsql(trim(request("UserID")))
iUserCourseID = scrubforsql(trim(request("UserCourseID")))
sAction = trim(request("Action"))
sSort = scrubforSQL(request("Sort"))

if trim(sSort) = "" then
	sSort = "ActionDate Desc"
end if

iCurPage = trim(request("page_number"))
if trim(iCurPage) = "" then
	iCurPage = 1
end if

'restrict display to 50 records
iMaxRecs = 50

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID
set rs = oUserObj.GetCourse(iUserCourseID)

if not rs.eof then
	sCourseName = rs("Name")
else
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The course information could not be retrieved.")
end if

%>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Admin Course Actions</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
			<b><%=sCourseName%></b>
		</td>
	</tr>
	<tr>
		<td>
			<br>
			<a href="/Admin/Users/ModUserCourse.asp?UserID=<%=iUserID%>&UserCourseID=<%=iUserCourseID%>">Return to Course Modify Screen</a>
		</td>
	</tr>	
</table>
<br>
<form name="frm1" action="AdminCourseActions.asp" method="POST">
<input type="hidden" name="UserID" value="<%= iUserID %>">
<input type="hidden" name="UserCourseID" value="<%= iUserCourseID %>">
<input type="hidden" name="action" value="RETRIEVE">
<input type="hidden" name="page_number" value="1">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Sort By:</b></td>
		<td>
			<select name="Sort">			
				<option value="ActionDate Asc" <% if UCase(sSort) = "ACTIONDATE ASC" then response.write("SELECTED") end if %>>Date Ascending</option>			
				<option value="ActionDate Desc" <% if UCase(sSort) = "ACTIONDATE DESC" then response.write("SELECTED") end if %>>Date Descending</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">
		</td>
	</tr>	
</table>
</form>
<% 
if Ucase(sAction) = "RETRIEVE" then
%>
<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Admin &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Action &nbsp;</td>							
		<td class="reportHeaderText">&nbsp; Date &nbsp;</td>									
	</tr>
<%
	set rs = oUserObj.GetAdminCourseActions(iUserCourseID,sSort)
	
	if clng(rs.recordcount) > 0 then
		'Set the number of records displayed on a page
		rs.PageSize = iMaxRecs
		rs.Cachesize = iMaxRecs
		iPageCount = rs.PageCount
	
		'determine which search page the user has requested.
		If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
		If clng(iCurPage) <= 0 Then iCurPage = 1
			
		'Set the beginning record to be display on the page
		rs.AbsolutePage = iCurPage		
		
		iCount = 0	
		do while (iCount < rs.PageSize) AND (not rs.eof)
			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if		
			
			response.write("<tr class=""" & sClass & """>" & vbcrlf)
			response.write("<td valign=""top"" width=""250"">" & rs("Email") & "</td>" & vbcrlf)
			response.write("<td valign=""top"">" & rs("CourseAction") & "</td>" & vbcrlf)
			response.write("<td valign=""top"" align=""center"" nowrap>" & rs("ActionDate") & "</td>" & vbcrlf)
			response.write("</tr>" & vbcrlf)
				
			rs.movenext
			iCount = iCount + 1
		loop
	
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
	
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td colspan=""3"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""AdminCourseActions.asp?UserID=" & iUserID & "&UserCourseID=" & iUserCourseID & "&Action=" & sAction & "&sort=" & sSort & "&page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if

		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""AdminCourseActions.asp?UserID=" & iUserID & "&UserCourseID=" & iUserCourseID & "&Action=" & sAction & "&sort=" & sSort & "&page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if

		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)				
		response.write("</table>" & vbcrlf)
	
		'display Page number
		response.write("<table width=""100%"">" & vbcrlf)	
		response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)	
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)					
	else
%>
	<tr>
		<td colspan="3">There has been no actions performed.</td>
	</tr>
<%
	end if
%>
</table>
<%
end if
%>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
set rs = nothing
%>

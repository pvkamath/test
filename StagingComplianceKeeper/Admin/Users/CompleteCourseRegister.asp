<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>
<!-- #include virtual="/includes/EmailFunctions.asp" ---------------------------------------------------->
<%
dim iUserID, iCurrentUserID
dim sCourses
dim sCrossCertCourses
dim sReferrer

if trim(session("AssignedCourses")) = "" and trim(session("AssignedCrossCerts")) = "" then
	response.redirect("/admin/error.asp?message=Course information could not be found.")
end if

iUserID = trim(request("UserID"))
sCourses = session("AssignedCourses")
sSelectectedCrossCerts = session("AssignedCrossCerts")
sReferrer = trim(request("referrer"))

'if not multiple user assign
if (trim(Session("SelectedUsers")) = "") then	
	Call CourseRegisterConfirmation(iUserID,1,sCourses,sSelectectedCrossCerts,0,"","","","BACKENDREGISTRATION")
else
	for each iCurrentUserID in split(trim(Session("SelectedUsers")), ", ")
		Call CourseRegisterConfirmation(iCurrentUserID,1,sCourses,sSelectectedCrossCerts,0,"","","","BACKENDREGISTRATION")	
	next
end if

'Delete Sessions
Session.Contents.Remove("SelectedUsers") 
Session.Contents.Remove("AssignedCourses") 
Session.Contents.Remove("AssignedCrossCerts") 
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">
				Course Registration is Complete
			</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<br>
		<% if Ucase(sReferrer) = "CROSSCERTLIST" then %>		
			<a href="/Admin/Users/UserCrossCertCourseList.asp?UserID=<%=iUserID%>">Return to Cross Certification Listing</a><br>
		<% end if %>			
		<%
		if (trim(iUserID) <> "") then
		%>
			<a href="UserCourseList.asp?UserID=<%=iUserID%>">Return to User's Course Listing</a>
		<%
		else
		%>
			<a href="UserList.asp">Return to User Listing</a>
		<%
		end if
		%>
		</td>
	</tr>
</table>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
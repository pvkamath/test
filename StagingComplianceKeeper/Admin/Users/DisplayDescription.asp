<%
option explicit
%>

<!-- #include virtual = "/includes/Course.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->

<%

dim iCourseID
dim oCourseObj
dim sCourseName
dim sDescription
dim rs

iCourseID = trim(request("CourseID"))

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

'Retrieve Course info
oCourseObj.CourseID = iCourseID
set rs = oCourseObj.GetCourse()

'Retrieve CourseName
if not rs.eof then
	sCourseName = rs("Name")
	sDescription = FormatTextForHTML(rs("Description"))
else
	set rs = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The Course Information could not be retrieved.")	
	response.end
end if

set oCourseObj = nothing
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title><%= sCourseName %> Description</title>
	<link rel="stylesheet" type="text/css" href="/admin/login/includes/style.css">
</head>

<body>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%=sCourseName%></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<br>
			<%= sDescription %>
		</td>
	</tr>
	<tr>
		<td align="center">
			<br>
			<a href="Javascript:window.close()">Close Window</a>
		</td>
	</tr>
</table>


</body>
</html>

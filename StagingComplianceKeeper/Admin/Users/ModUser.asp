<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->

<%
dim oUserObj 'as object
dim rs 'as object
dim iUserID 'as integer
dim sMode 'as string
dim sLogin
dim sEmail, sPassword, sConfirmPassword 'as string
dim sFirstName, sMid, sLastName 'as string
dim iGroupID, iProviderID, iBranchID 'as integer
dim sCompanyL2IdList
dim sCompanyL3IdList
dim sBranchIdList
dim bBranchesDropDownDisabled 'as integer
dim iScreenRefreshed 'as integer
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo 'as string
dim sCity, sAddress, sZipcode, sPhone, sFax 'as string
dim iStateID, iUserStatus 'as integer
dim bCrossCertEligible, bNewsletter 'as boolean
dim sCrossCertEligibleChecked, sNewsletterChecked 'as string
dim sMarketWareConfirmNo 'as string
dim sNotes 'as string
dim sAllowedToAddUsersChecked, sAllowedToAddUsersDisabled  'as string
dim bAllowedToAddUsers 'as boolean
dim iCompanyId
dim bCConAlertEmails 'as boolean

iUserID = ScrubForSQL(request("UserID"))

iScreenRefreshed = trim(request("ScreenRefresh"))
if trim(iScreenRefreshed) = "" then
	iScreenRefreshed = 0 
end if

if iUserID = "" then
	sMode = "Add"
	
	'if the screen was refreshed, pull the values that were stored on the form
	if (cint(iScreenRefreshed) = 1) then
		sLogin = ScrubForSql(request("Login"))
		sEmail = ScrubForSql(request("Email"))
		sPassword = ScrubForSql(request("Password"))
		sConfirmPassword = ScrubForSql(request("ConfirmPassword"))
		sFirstName = ScrubForSql(request("FirstName"))
		sMid = ScrubForSql(request("MiddleName"))
		sLastName = ScrubForSql(request("LastName"))
		iGroupID = ScrubForSql(request("GroupID"))
		iCompanyId = ScrubForSql(request("Company"))
		'iBranchId = ScrubForSql(request("BranchId"))
		sBranchIdList = ScrubForSql(request("BranchId"))
		sBranchIdList = replace(request("BranchIdList"), ", ", ",")
		sCompanyL2IdList = ScrubForSql(request("CompanyL2Id"))
		sCompanyL2IdList = replace(request("CompanyL2IdList"), ", ", ",")
		sCompanyL3IdList = ScrubForSql(request("CompanyL3Id"))
		sCompanyL3IdList = replace(request("CompanyL3IdList"), ", ", ",")
		iProviderID = ScrubForSql(request("Provider"))
		sSSN = ScrubForSql(request("SSN"))
		sCity = ScrubForSql(request("City"))
		iStateID = ScrubForSql(request("State"))
		sAddress = ScrubForSql(request("Address"))
		sZipcode = ScrubForSql(request("Zipcode"))
		sPhone = ScrubForSql(request("Phone"))
		sFax = ScrubForSql(request("Fax"))
		iUserStatus = ScrubForSql(request("UserStatus"))
		sNotes = ScrubForSql(request("Notes"))
		
		'Set Value for CConAlertEmails
		if Ucase(trim(request("CConAlertEmails"))) = "ON" then
			bCConAlertEmails = true
		else
			bCConAlertEmails = false
		end if
	else

		iGroupID = ""
		bCConAlertEmails = true
	end if

else
	sMode = "Edit"
	
	'if the screen was refreshed, pull the values that were stored on the form
	if (cint(iScreenRefreshed) = 1) then
		sLogin = ScrubForSql(request("Login"))
		sEmail = ScrubForSql(request("Email"))
		sPassword = ScrubForSql(request("Password"))
		sConfirmPassword = ScrubForSql(request("ConfirmPassword"))
		sFirstName = ScrubForSql(request("FirstName"))
		sMid = ScrubForSql(request("MiddleName"))
		sLastName = ScrubForSql(request("LastName"))
		iGroupID = ScrubForSql(request("GroupID"))
		iCompanyId = ScrubForSql(request("Company"))
		'iBranchId = ScrubForSql(request("BranchId"))
		sBranchIdList = ScrubForSql(request("BranchId"))
		sBranchIdList = replace(request("BranchIdList"), ", ", ",")
		sCompanyL2IdList = ScrubForSql(request("CompanyL2Id"))
		sCompanyL2IdList = replace(request("CompanyL2IdList"), ", ", ",")
		sCompanyL3IdList = ScrubForSql(request("CompanyL3Id"))
		sCompanyL3IdList = replace(request("CompanyL3IdList"), ", ", ",")
		iProviderId = ScrubForSql(request("Provider"))
		sCity = ScrubForSql(request("City"))
		iStateID = ScrubForSql(request("State"))
		sAddress = ScrubForSql(request("Address"))
		sZipcode = ScrubForSql(request("Zipcode"))
		sPhone = ScrubForSql(request("Phone"))
		sFax = ScrubForSql(request("Fax"))
		iUserStatus = ScrubForSql(request("UserStatus"))
		sNotes = ScrubForSql(request("Notes"))

		'Set Value for CConAlertEmails
		if Ucase(trim(request("CConAlertEmails"))) = "ON" then
			bCConAlertEmails = true
		else
			bCConAlertEmails = false
		end if
	else	

		'Pull Info from db
		set oUserObj = New User
		oUserObj.ConnectionString = application("sDataSourceName")
		oUserObj.UserID = iUserID
		set rs = oUserObj.GetUser()
		
		sLogin = trim(rs("UserName"))
		sEmail = trim(rs("Email"))
		sPassword = trim(rs("Password"))
		sFirstName = trim(rs("FirstName"))
		sMid = trim(rs("MiddleName"))
		sLastName = trim(rs("LastName"))
		iGroupID = trim(rs("GroupID"))
		iCompanyId = trim(rs("CompanyID"))
		'iBranchId = trim(rs("BranchId"))
		sBranchIdList = oUserObj.GetBranchIdListString
		sCompanyL2IdList = oUserObj.GetCompanyL2IdListString
		sCompanyL3IdList = oUserObj.GetCompanyL3IdListString
		iProviderId = trim(rs("ProviderId"))
		sCity = trim(rs("City"))
		iStateID = trim(rs("StateID"))
		sAddress = trim(rs("Address"))
		sZipcode = trim(rs("Zipcode"))
		sPhone = trim(rs("Phone"))
		sFax = trim(rs("Fax"))
		iUserStatus = trim(rs("UserStatus"))
		sNotes = trim(rs("Notes"))
		bCConAlertEmails = trim(rs("CConAlertEmails"))
		
		set oUserObj = nothing

		sConfirmPassword = sPassword
	
	end if
	
end if

%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">

function CompanyChange()
{
	document.frm1.BranchId.value = '';	
	document.frm1.Provider.value = '';
									
	RefreshScreen();
}

function StatusChange()
{
	if (document.frm1.GroupID.value == '20')
	{
		document.frm1.BranchId.value = '';
	}
	
	RefreshScreen(); 
}

function RefreshScreen()
{
	document.frm1.ScreenRefresh.value = 1;
	document.frm1.action = "ModUser.asp";
	
	//EnableFields();
	
	document.frm1.submit();	
}

function Validate(FORM)
{   	
	if(!checkEmail(FORM.Email) || !checkString(FORM.Login, "Login") || !checkString(FORM.Password,"Password") || !checkString(FORM.ConfirmPassword,"Confirm Password"))
		return false;
		
	if (FORM.Password.value != FORM.ConfirmPassword.value)
	{
		alert("The Password and Confirm Password fields do not match.")
		FORM.Password.focus();
		return false;
	}
	
	if (!checkString(FORM.FirstName,"First Name") || !checkString(FORM.LastName,"Last Name") || !checkString(FORM.GroupID,"User Type"))
		return false;

	//if the UserType/GroupID is Company Admin, the Company field must be selected
	if ((FORM.GroupID.selectedIndex == 4) && (FORM.Provider.selectedIndex == 0))
	{
		alert("The Provider field must be set since this user is a Provider Admin.");
		FORM.GroupID.focus();
		return false;
	}
	
	//check address
	if (!checkString(FORM.Address,"Address"))
		return false;
	
	//if zipcode is not blank, make sure it's in the proper format
	if (FORM.Zipcode.value != "")
	{
		if (!checkZIPCode(FORM.Zipcode))
			return false;
	}
	
	//if phone # is not blank, make sure it's in the proper format
	if (!checkUSPhone(FORM.Phone))
		return false;
	
	//if fax # is not blank, make sure it's in the proper format
	if (FORM.Fax.value != "")
	{
		if (!checkUSPhone(FORM.Fax))
			return false;
	}	

	return true;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%=sMode%>&nbsp;User</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="ModUserProc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<input type="hidden" name="Mode" value="<%=sMode%>">
<% if UCase(sMode) = "EDIT" then %>
<input type="hidden" name="OldLogin" value="<%=sLogin%>">
<% end if %>

<input type="hidden" name="ScreenRefresh" value="">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td><b>* Login:</b></td>
		<td><input type="text" name="Login" value="<% = sLogin %>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>* Password:</b></td>
		<td><input type="password" name="Password" value="<%=sPassword%>" size="30" maxlength="20"> 
		<a href="showpassword.asp?userid=<% = iUserId %>">View Password</a>
		</td>				
	</tr>
	<tr>
		<td><b>* Confirm Password:</b></td>
		<td><input type="password" name="ConfirmPassword" value="<%=sConfirmPassword%>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>* First Name:</b></td>
		<td><input type="text" name="FirstName" value="<%=sFirstName%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>Middle Name:</b></td>
		<td><input type="text" name="MiddleName" value="<%=sMid%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>* Last Name:</b></td>
		<td><input type="text" name="LastName" value="<%=sLastName%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>* Email:</b></td>
		<td><input type="text" name="Email" value="<%=sEmail%>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>* User Status:</b></td>
		<td><% call DisplayUserStatusDropDown(iUserStatus,1) %></td>				
	</tr>		
	<tr>
		<td><b>* User Type:</b></td>
		<td><% call DisplayUserTypeDropDown(iGroupID,1) %></td>				
	</tr>	
	<tr>
		<td><b>Company: </b></td>
		<td><% call DisplayCompaniesDropDown(iCompanyId, 1) %></td>
	</tr>
	<tr>
		<td valign="top"><b><% = session("CompanyL2Name") %> </b></td>
		<td>
		<%
		if iGroupId = "" then
			call DisplayCompanyL2sListBox(0, iCompanyId, 1, 1, "CompanyL2Id")
		elseif iGroupId = 3 or iGroupId = 4 then
			call DisplayCompanyL2sListbox(sCompanyL2IdList, iCompanyId, false, 2, "CompanyL2Id")
		else
			call DisplayCompanyL2sListBox(0, iCompanyId, 1, 1, "CompanyL2Id")
		end if
		%>
		</td>
	</tr>
	<tr>	
		<td valign="top"><b><% = session("CompanyL3Name") %> </b></td>
		<td>
		<%
		if iGroupId = "" then
			call DisplayCompanyL3sListBox(0, iCompanyId, 1, 1, "CompanyL3Id")
		elseif iGroupId = 5 or iGroupId = 6 then
			call DisplayCompanyL3sListbox(sCompanyL3IdList, iCompanyId, false, 2, "CompanyL3Id")
		else
			call DisplayCompanyL3sListBox(0, iCompanyId, 1, 1, "CompanyL3Id")
		end if
		%>
		</td>
	</tr>
	<tr>
		<td><b>Branch: </b></td>
		<td>
		<%
		if iGroupId = "" then
			call DisplayBranchesListBox(0, iCompanyId, 1, 1, "BranchId")
		elseif iGroupId = 10 or iGroupId = 11 then
			call DisplayBranchesListBox(sBranchIdList, iCompanyId, false, 2, "BranchId")
		else
			call DisplayBranchesListBox(0, iCompanyId, 1, 1, "BranchId")
		end if
		%>
		</td>
	</tr>
	<tr>
		<td><b>Provider: </b></td>
		<td>
		<%
		if iGroupId = "" then
			call DisplayProvidersDropdown(0, iCompanyId, 1, 2, 0, "")
		elseif iGroupId <> 30 then
			call DisplayProvidersDropdown(0, iCompanyId, 1, 2, 0, "")
		else
			call DisplayProvidersDropdown(iProviderId, iCompanyId, 0, 1, 0, "")
		end if
		%>
	<tr>
	<tr>
		<td><b>* Address:</b></td>
		<td><input type="text" name="Address" value="<%=sAddress%>" size="30" maxlength="100"></td>				
	</tr>				
	<tr>
		<td><b>City:</b></td>
		<td><input type="text" name="City" value="<%=sCity%>" size="30" maxlength="50"></td>				
	</tr>	
	<tr>
		<td><b>State:</b></td>
		<td><% call DisplayStatesDropDown(iStateID,1,"State") %></td>
	</tr>
	<tr>
		<td><b>Zipcode:</b></td>
		<td><input type="text" name="Zipcode" value="<%=sZipcode%>" size="10" maxlength="10"></td>				
	</tr>
	<tr>
		<td><b>* Phone No.:</b></td>
		<td><input type="text" name="Phone" value="<%=sPhone%>" size="30" maxlength="20"></td>				
	</tr>
	<tr>
		<td><b>Fax No.:</b></td>
		<td><input type="text" name="Fax" value="<%=sFax%>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>CC on Alerts Emails:</b></td>
		<td><input type="checkbox" name="CConAlertEmails" <% if bCConAlertEmails then response.write(" CHECKED") end if %>></td>				
	</tr>	
	<tr>
		<td><b>Notes:</b></td>
		<td valign="middle">
			<textarea name="notes" cols="50" rows="5" maxlength="1000"><%=sNotes%></textarea>
		</td>
	</tr>			
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
%>
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" -------------------------------------------------------->

<%
dim sCourseName 'as string
dim oUserObj, oCourseObj 'as object
dim rs 'as object
dim iUserID 'as integer
dim iUserCourseID, iCourseID 'as integer
dim sCourseExpirationDate, sCertificateExpirationDate 'as string
dim sActiveChecked 'as string
dim bActive 'as boolean
dim bCertificateReleased 'as boolean
dim sCertificateReleasedChecked 'as string
dim testRS 'as object
dim bHasCertificate 'as boolean
dim iSearchCurExp, iSearchStatus 'as integer
dim IsCrossCert 'as integer

iUserID = trim(request("UserID"))
iUserCourseID = trim(request("UserCourseID"))
iSearchCurExp = trim(request("CurrentExpired"))
iSearchStatus = trim(request("Status"))
IsCrossCert = trim(request("IsCrossCert"))

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID

set rs = oUserObj.GetCourse(iUserCourseID)

iCourseID = rs("CourseID")
sCourseName = rs("Name")
sCourseExpirationDate = rs("CourseExpirationDate")
sCertificateExpirationDate = rs("CertificateExpirationDate")
bActive = rs("Active")

if bActive then
	sActiveChecked = "CHECKED"
else
	sActiveChecked = ""
end if

bCertificateReleased = rs("CertificateReleased")

if bActive then
	sCertificateReleasedChecked = "CHECKED"
else
	sCertificateReleasedChecked = ""
end if

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")
bHasCertificate =  oCourseObj.HasCertificate(iCourseID)
set oCourseObj = nothing

'Get the Test and Quizzes, if any
set testRS = oUserObj.GetTests(iUserCourseID,"","")
%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">
function Validate(FORM)
{
	var sFullElementName;
	
	if (!checkIsDate(FORM.CourseExpirationDate,"Please enter a valid Course Expiration Date."))
		return false;
<% if bHasCertificate then %>
	if (!checkIsDate(FORM.CertificateExpirationDate,"Please enter a valid Certificate Expiration Date."))
		return false;
<% end if %>		

	//Validate Test and Quiz attempts
	//if the item has a value(optional), then make sure it's an integer
	for (i = 0; i < FORM.elements.length; i++)
	{
		//Get Name
		sFullElementName = FORM.elements[i].name;	
		
		//determine if element is a Test/Quiz
		sTemp = sFullElementName.substr(0,4);
		
		if (sTemp.toUpperCase() == "TEST")
		{
			if (!isWhitespace(FORM.elements[i].value))
			{
				//if not empty, make sure the value is an integer greater or equal than 0
				if ((!isInteger(FORM.elements[i].value)) || (FORM.elements[i].value < 0))
				{
					alert('All Test and Quiz attempt values must be integers greater or equal to zero.\nIf you want unlimited attempts, leave the value blank.');
					return false;
				}		
			}
		}
	}

	return true;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sCourseName %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="ModUserCourseProc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="UserID" value="<%= iUserID %>">
<input type="hidden" name="UserCourseID" value="<%= iUserCourseID %>">
<input type="hidden" name="mode" value="EDIT">
<input type="hidden" name="CurrentExpired" value="<%= iSearchCurExp %>">
<input type="hidden" name="Status" value="<%= iSearchStatus %>">
<input type="hidden" name="IsCrossCert" value="<%=IsCrossCert%>">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td><b>* Course Expiration Date:</b></td>
		<td><input type="textbox" name="CourseExpirationDate" value="<%= sCourseExpirationDate%>"  maxlength="30">&nbsp;<a href="javascript:show_calendar('frm1.CourseExpirationDate');" onmouseover="window.status='Click to choose the Expiration Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 hspace="0" vspace="0" alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frm1.CourseExpirationDate');" onmouseover="window.status='Click to choose the Expiration date.';return true;" onmouseout="window.status='';return true;">Select an Expiration Date</a></td>
	</tr>
	<tr>
		<td><b>* Active:</b></td>
		<td><input type="checkbox" name="Active" <%= sActiveChecked %>></td>				
	</tr>
<% if bHasCertificate then %>
	<tr>
		<td><b>* Certificate Expiration Date:</b></td>
		<td><input type="textbox" name="CertificateExpirationDate" value="<%= sCertificateExpirationDate%>"  maxlength="30">&nbsp;<a href="javascript:show_calendar('frm1.CertificateExpirationDate');" onmouseover="window.status='Click to choose the Expiration Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 hspace="0" vspace="0" alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frm1.CourseExpirationDate');" onmouseover="window.status='Click to choose the Expiration date.';return true;" onmouseout="window.status='';return true;">Select an Expiration Date</a></td>
	</tr>	
	<tr>
		<td><b>* Certificate Released:</b></td>
		<td><input type="checkbox" name="CertificateReleased" <%= sCertificateReleasedChecked %>></td>				
	</tr>
<% end if %>	
<% if not testRS.EOF then %>
	<tr>
		<td colspan="2">
			<table cellpadding="4" cellspacing="4">
				<tr>
					<td colspan="2"><b>Tests And Quizzes</b></td>
				</tr>
				<tr>
					<td>
						<u>Name</u>
					</td>
					<td>
						<u>Attempts Left</u>
					</td>
				</tr>
<%
		do while not testRS.EOF
%>				
				<tr>
					<td><%= testRS("Name") %></td>
					<td><input type="Text" name ="TestAttemptsLeft<%= testRS("ID") %>" value="<%= testRS("AttemptsLeft") %>" size="3" maxlength="3">&nbsp;leave blank for unlimited</td>
				</tr>
<%
			testRS.MoveNext
		loop
%>
			</table>
		</td>
	</tr>
<% end if %>
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
set rs = nothing
set testRS = nothing
%>
<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim iUserID 'as integer
dim iUserCourseID 'as integer
dim oUserObj 'as object
dim bSuccess 'as boolean
dim sCourseExpirationDate, sCertificateExpirationDate 'as string
dim iActive, iCertificateReleased 'as integer
dim sMode 'as string
dim sMessage 'as string
dim iSearchCurExp, iSearchStatus 'as integer
dim sElement 'as string
dim iUserTestID 'as integer
dim iAttemptsLeft 'as integer
dim iIsCrossCert 'as integer

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

iUserID = ScrubForSQL(request("UserID"))
sMode = trim(request("Mode"))
iUserCourseID = ScrubForSQL(request("UserCourseID"))
sCourseExpirationDate = ScrubForSQL(request("CourseExpirationDate"))
sCertificateExpirationDate = ScrubForSQL(request("CertificateExpirationDate"))
iSearchCurExp = trim(request("CurrentExpired"))
iSearchStatus = trim(request("Status"))
iIsCrossCert = trim(request("IsCrossCert"))

if (UCase(sMode) = "EDIT") then
	if (trim(request("Active")) = "on") then
		iActive = 1
	else
		iActive = 0
	end if

	if (trim(request("CertificateReleased")) = "on") then
		iCertificateReleased = 1
	else
		iCertificateReleased = 0
	end if

	'update user course info
	oUserObj.UserID = iUserID
	bSuccess = oUserObj.EditUserCourse(iUserCourseID,sCourseExpirationDate,iActive,sCertificateExpirationDate,iCertificateReleased)

	if bSuccess then
		'Edit the Test and Quiz attempts, if any
		for each sElement in request.form()
			if (left(UCase(sElement),16) = "TESTATTEMPTSLEFT") then		
				'pull the UserTestID from the sting
				iUserTestID = mid(sElement,17)
				iAttemptsLeft = request.form(sElement)
				
				bSuccess = oUserObj.EditTestAttempts(iUserTestID,iAttemptsLeft)
				
				if not bSuccess then
					response.redirect("/admin/error.asp?message=The Test/Quiz attempts were not Edited successfully.")		
				end if
			end if
		next 
		sMessage = "The Course was Edited successfully."
	else
		response.redirect("/admin/error.asp?message=The course was not Edited successfully")
	end if
elseif (UCase(sMode) = "DELETE") then
	bSuccess = oUserObj.DeleteCourse(iUserCourseID)
	
	if bSuccess then
		sMessage = "The Course was Deleted successfully."
	else
		response.redirect("/admin/error.asp?message=The Course was not Deleted successfully.")		
	end if
end if

set oUserObj = nothing
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->


<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMessage %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
		<% if trim(iIsCrossCert) = "1" then %>
			<a href="UserCrossCertCourseList.asp?UserID=<%=iUserID%>&CurrentExpired=<%=iSearchCurExp%>&Status=<%=iSearchStatus%>">Return to Cross Certification Listing</a><br>		
		<% end if %>
			<a href="UserCourseList.asp?UserID=<%=iUserID%>&CurrentExpired=<%=iSearchCurExp%>&Status=<%=iSearchStatus%>">Return to User's Course Listing</a><br>		
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

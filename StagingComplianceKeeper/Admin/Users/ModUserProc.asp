<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim iUserID
dim sMode
dim oUserObj
dim rs
dim sLogin
dim sEmail, sOldLogin, sPassword, sConfirmPassword
dim sFirstName, sMid, sLastName
dim iGroupID, iProviderID, iBranchID, iCompanyId
dim sCompanyL2IdList
dim sCompanyL3IdList
dim sBranchIdList
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo
dim sCity, sAddress, sZipcode, sPhone, sFax
dim iStateID, iUserStatus, iCrossCertEligible, iNewsletter, iAllowedToAddUsers, iCConAlertEmails
dim sMarketWareConfirmNo
dim sNotes
dim sMessage
dim bSuccess

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

sMode = trim(request("Mode"))
iUserID = ScrubForSQL(request("UserID"))
sEmail = ScrubForSQL(request("Email"))
sLogin = ScrubForSql(request("Login"))
sOldLogin = ScrubForSQL(request("OldLogin"))
sPassword = ScrubForSQL(request("Password"))
sConfirmPassword = ScrubForSQL(request("ConfirmPassword"))
sFirstName = ScrubForSQL(request("FirstName"))
sMid = ScrubForSQL(request("MiddleName"))
sLastName = ScrubForSQL(request("LastName"))
iGroupID = ScrubForSQL(request("GroupID"))
iCompanyId = ScrubForSql(request("Company"))
'iBranchId = ScrubForSql(request("BranchId"))
sBranchIdList = ScrubForSql(request("BranchId"))
iProviderId = ScrubForSql(request("Provider"))

if iGroupId = "" then
	iGroupId = 0
end if

sCompanyL2IdList = ScrubForSql(request("CompanyL2Id"))
if (iGroupId = 3 or iGroupId = 4) and sCompanyL2IdList = "" then
	Response.Write("This operation requires a valid " & session("CompanyL2Name") & " selection.")
	Response.End
end if

sCompanyL3IdList = ScrubForSql(request("CompanyL3Id"))
if (iGroupId = 5 or iGroupId = 6) and sCompanyL3IdList = "" then
	Response.Write("This operation requires a valid " & session("CompanyL3Name") & " selection.")
	Response.End
end if

if (iGroupId = 10 or iGroupId = 11) and sBranchIdList = "" then
	Response.Write("This operation requires a valid Branch selection.")
	Response.End
end if
iProviderId = ScrubForSql(request("Provider"))
if iGroupId = 20 and iProviderId = "" then
	Response.Write("This operation requires a valid Provider selection.")
	Response.End
end if 

sCity = ScrubForSQL(request("City"))
iStateID = ScrubForSQL(request("State"))
sAddress = ScrubForSQL(request("Address"))
sZipcode = ScrubForSQL(request("Zipcode"))
sPhone = ScrubForSQL(request("Phone"))
sFax = ScrubForSQL(request("Fax"))
iUserStatus = ScrubForSQL(request("UserStatus"))
sNotes = ScrubForSQL(request("Notes"))

if Ucase(trim(request("CConAlertEmails"))) = "ON" then
	iCConAlertEmails = 1
else
	iCConAlertEmails = 0
end if

if UCase(sMode) = "ADD" then
	'Make Sure this login is not in the system
	if (oUserObj.LoginExists(sLogin)) then
		response.redirect("/admin/error.asp?message=The Login - " & sLogin & " - is being used by another user.  Please choose another login.")		
	end if
	
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MiddleName = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.CompanyL2IdList = sCompanyL2IdList
	oUserObj.CompanyL3IdList = sCompanyL3IdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.CConAlertEmails = iCConAlertEmails	
	oUserObj.Notes = sNotes
	
	iUserID = oUserObj.AddUser()
	
	if cint(iUserID) > 0 then
		sMessage = sFirstName & " " & sLastName & " has been Added successfully."
	else
		response.redirect("/admin/error.asp?message=The User was not Added successfully.")		
	end if
elseif UCase(sMode) = "EDIT" then
	'if login has changed, Make Sure this login is not in the system
	if (trim(sLogin) <> trim(sOldLogin)) and (oUserObj.LoginExists(sLogin)) then
		response.redirect("/admin/error.asp?message=The Login - " & sLogin & " - is being used by another user.  Please choose another login.")		
	end if
	
	oUserObj.UserID = iUserID
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MiddleName = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.CompanyL2IdList = sCompanyL2IdList
	oUserObj.CompanyL3IdList = sCompanyL3IdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.CConAlertEmails = iCConAlertEmails	
	oUserObj.Notes = sNotes
	
	bSuccess = oUserObj.EditUser()
	
	if bSuccess then
		sMessage = sFirstName & " " & sLastName & " has been Edited successfully."
	else
		response.redirect("/admin/error.asp?message=The User was not Edited successfully.")		
	end if
elseif UCase(sMode) = "DELETE" then
	oUserObj.UserID = iUserID
	oUserObj.UserStatus = 3 'deleted status
	bSuccess = oUserObj.ChangeStatus()
	
	if bSuccess then
		response.redirect("UserList.asp?&page_number=" & Session("UserCurPage"))
	else
		response.redirect("/admin/error.asp?message=The User was not Deleted successfully.")		
	end if
end if

%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->


<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMessage %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
%>

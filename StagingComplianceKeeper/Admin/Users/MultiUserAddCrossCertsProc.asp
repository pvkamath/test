<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim oUserObj 'as object
dim rs 'as object
dim iCurrentCrossCertID 'as integer
dim sCoreCourses 'as string
dim sCrossCertCourses 'as string
dim bSuccess 'as boolean
dim iCurrentUserID 'as integer
dim iCoreCourseID 'as integer
dim objConn 'as object
dim sSQL 'as string
dim iUserCourseID 'as integer
dim iCourseID 'as integer
dim bFirstUser 'as boolean

iCoreCourseID = ScrubForSQL(request("CourseID"))
sCoreCourses = trim(request("CoreCourses"))
sCrossCertCourses = trim(request("CrossCertCourses"))

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

set objConn = New HandyADO

'add cross certs
if sCrossCertCourses <> "" then
	bFirstUser = true
	for each iCurrentUserID in split(trim(Session("SelectedUsers")), ", ")
		oUserObj.UserID = iCurrentUserID

		'get the User's Course ID for this course
		sSQL = "SELECT MAX(ID) AS UserCourseID FROM UsersCourses WHERE UserID = '" & iCurrentUserID & "' AND CourseID = '" & iCoreCourseID & "' "
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

		if not rs.eof then		
			iUserCourseID = rs("UserCourseID")
			
			for each iCurrentCrossCertID in split(sCrossCertCourses, ", ")
				bSuccess = oUserObj.AddCrossCertCourse(iUserCourseID,iCurrentCrossCertID,0)		

				if bSuccess then
					'Only construct application var once
					if bFirstUser then
						set rs = oUserObj.GetCourse(iUserCourseID)
						iCourseID = rs("CourseID")
						rs.Close
			
						if trim(session("AssignedCrossCerts")) = "" then
							session("AssignedCrossCerts") = iCourseID & "/" & iCurrentCrossCertID
						else
							session("AssignedCrossCerts") = session("AssignedCrossCerts") & ", " & iCourseID & "/" & iCurrentCrossCertID
						end if
					end if
				else
					set objConn = nothing
					set oUserObj = nothing
					response.redirect("/admin/error.asp?message=The Cross Certified Course could not be Added.")
				end if
			next
		else
			set objConn = nothing
			set oUserObj = nothing		
			response.redirect("/admin/error.asp?message=The Cross Certified Course could not be Added.")		
		end if
		bFirstUser = false
	next
end if

if (sCoreCourses <> "") then 
	'there are still more courses to cross certify, redirect to Add cross cert screen
	response.redirect("MultiUserAddCrossCerts.asp?CoreCourses=" & sCoreCourses)
end if

set rs = nothing
set objConn = nothing
set oUserObj = nothing
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">The Cross Certifications have been completed</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>	
			<form name="frm1" action="CompleteCourseRegister.asp" method="POST">
				<input type="submit" name="completebutton" value="Complete Registration">										
			</form>		
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


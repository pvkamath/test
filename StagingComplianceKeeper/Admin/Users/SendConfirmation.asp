<%
option explicit
on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/EmailFunctions.asp" --------------------------------------------------->
<%
dim iUserID 'as integer
dim oUserObj 'as object
dim rs 'as object
dim sFullName 'as string

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

iUserID = ScrubForSQL(request("UserID"))

oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()

if trim(rs("MiddleName")) <> "" then
	sFullName = rs("FirstName") & " " & rs("MiddleName") & ". " & rs("LastName")
else
	sFullName = rs("FirstName") & " " & rs("LastName")
end if			

Call SendConfirmationEmail(iUserID)

set rs = nothing
set oUserObj = nothing
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->


<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">The Confirmation Email was sent successfully to <%= sFullName %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

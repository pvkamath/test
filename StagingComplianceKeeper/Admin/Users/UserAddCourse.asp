<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Search.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>

<%
dim rs
dim sName
dim sSQL
dim sClass
dim oSearchObj, oUserObj
dim iCourseID
dim sSearchCourseName
dim iSearchCourseProgram, iSearchState 
dim sAction
dim iUserID

iUserID = trim(request("UserID"))
sAction = Ucase(trim(request("Action")))

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()
if not rs.eof then
	sName = rs("FirstName") & " " & rs("LastName")
	set oUserObj = nothing
else
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The User Information could not be retrieved.")
end if

if sAction = "SEARCH" then
	set oSearchObj = New Search
	oSearchObj.ConnectionString = application("sDataSourceName")

	sSearchCourseName = ScrubForSQL(request("CourseName"))
	iSearchCourseProgram = ScrubForSQL(request("CourseProgram"))
	iSearchState = ScrubForSQL(request("State"))

	'add necessary search fields
	if sSearchCourseName <> "" then
		oSearchObj.CourseName = sSearchCourseName
	end if
	
	if iSearchCourseProgram <> "" then
		oSearchObj.CourseProgram = iSearchCourseProgram	
	end if
	
	if iSearchState <> "" then
		oSearchObj.State = iSearchState	
	end if

	'only search if one of the search button was clicked

	oSearchObj.CourseType = 1 'only online courses
	set rs = oSearchObj.SearchCourses()
	set oSearchObj = nothing
end if

%>

<script language="JavaScript" src="/admin/includes/FormCheck2.js"></script>
<script language="JavaScript">
function OpenDescription(p_iCourseID)
{
	window.open("DisplayDescription.asp?CourseID=" + p_iCourseID,"DescWin","width=400, height=200,toolbars=no,scrollbars=yes");
}

function Validate(FORM)
{
	var bFound = false;
	var iCourseCnt = FORM.Courses.length;
	
	//Make sure at least one course is selected
	
	//if the iCourseCnt var is not an integer, there is only 1 course in the list
	//the length value is undefined if there is only one course
	if (!isInteger(iCourseCnt)){
		if (FORM.Courses.checked)
		{
			bFound = true;
		}
	}
	else	
	{		
		for (var i = 0; i < iCourseCnt; i++) {
			if (FORM.Courses[i].checked)
			{
				bFound = true;
			}
		}
	}
	
	if (bFound)
	{
		return true;
	}
	else
	{
		alert("You must select a Course to Add.");
		return false;
	}
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add Courses</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<b><%=sName%></b>
		</td>
	</tr>	
	<tr>
		<td>
			<br>
			<a href="/Admin/Users/UserCourseList.asp?UserID=<%=iUserID%>">Go to User Course Listing</a><br>
			<a href="/Admin/Users/UserList.asp">Go to User Listing</a>
		</td>
	</tr>	
</table>
<br>

<form name="SearchFrm" action="UserAddCourse.asp" method="POST">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<input type="hidden" name="action" value="SEARCH">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Course Name:</b></td>
		<td><input  type="text" name="CourseName" value="<%=sSearchCourseName%>" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>Program:</b></td>
		<td>
			<% call DisplayProgramDropDown(iSearchCourseProgram,0) 'located in functions.asp %>
		</td>				
	</tr>
	<tr>		
		<td><b>State:</b></td>
		<td>
			<% call DisplayStatesDropDown(iSearchState,0,1) 'located in functions.asp %>
		</td>
	</tr>		
	<tr>
		<td colspan="2" align="center">
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>
</table>
</form>

<form name="AddFrm" action="UserAddCourseProc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText" width="25">&nbsp;</td>
		<td class="reportHeaderText">&nbsp; Course Name &nbsp;</td>
		<td class="reportHeaderText" width="100">&nbsp; Description &nbsp;</td>					
		<td class="reportHeaderText">&nbsp; Program &nbsp;</td>				
		<td class="reportHeaderText">&nbsp; State &nbsp;</td>			
	</tr>
<%
if sAction = "SEARCH" then
	if not rs.eof then
		do while not rs.eof
			iCourseID = rs("CourseID")
			
			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if		
			
			response.write("<tr class=""" & sClass & """>" & vbcrlf)
			response.write("<td align=""center""><input type=""checkbox"" name=""Courses"" value=""" & iCourseID & """></td>" & vbcrlf)
			response.write("<td>" & rs("Name") & "</td>" & vbcrlf)
			response.write("<td align=""center""><a href=""javascript:OpenDescription('" & iCourseID & "');"">View</a></td>" & vbcrlf)
			response.write("<td>" & rs("ProgramName") & "</td>" & vbcrlf)
			response.write("<td>" & rs("State") & "</td>" & vbcrlf)
			response.write("</tr>" & vbcrlf)
			rs.MoveNext
		loop
%>
	<tr class="row2">
		<td colspan="5"><input type="image" src="/admin/media/images/blue_bttn_add.gif"></td>
	</tr>
<%
	else
		response.write("<tr><td colspan=""5"">There are currently no courses that matched your search criteria.</td></tr>" & vbcrlf)
	end if
end if
%>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
%>
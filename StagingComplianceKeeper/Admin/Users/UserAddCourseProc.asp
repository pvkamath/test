<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim iUserID
dim oUserObj, oCourseObj
dim rs
dim iCurrentCourseID, iCurrentID, sCurrentCourse, aCurrentCourse
dim sCoursesWithCrossCerts, sCourses
dim sName
dim aCourses
dim bSuccess
dim bFirst
dim iID

iUserID = ScrubForSQL(request("UserID"))
sCourses = trim(request("Courses"))

'if no courses, then display error
if sCourses = "" then
	response.redirect("/admin/error.asp?message=There were no courses to Add.")
end if

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

'Get the User's Name
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()

if not rs.eof then
	sName = rs("FirstName") & " " & rs("LastName")
else
	set oUserObj = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The User Information could not be retrieved.")
end if

aCourses = split(sCourses, ", ")

bFirst = true
for each iCurrentCourseID in aCourses
	iID = oUserObj.AddCourse(iCurrentCourseID)
	
	if cint(iID) < 1 then
		set oUserObj = nothing
		set oCourseObj = nothing
		response.redirect("/admin/error.asp?message=The Course(s) were not Added succesfully.")	
	else
		'check to see if this course has cross certs
		if oCourseObj.HasCrossCertifications(iCurrentCourseID) then
			if bFirst then
				sCoursesWithCrossCerts = iID & "/" & iCurrentCourseID
				bFirst = false
			else
				sCoursesWithCrossCerts = sCoursesWithCrossCerts & ", " & iID & "/" & iCurrentCourseID
			end if
		end if
	end if
next
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<script language="JavaScript" src="/admin/includes/FormCheck2.js"></script>
<script language="JavaScript">
function CheckCoreCourses(FORM)
{
	//make sure at least one item is picked
	var bFound = false;
	var iCourseCnt = FORM.CoreCourses.length;
	
	//Make sure at least one course is selected
	
	//if the iCourseCnt var is not an integer, there is only 1 course in the list
	//the length value is undefined if there is only one course
	if (!isInteger(iCourseCnt)){
		if (FORM.CoreCourses.checked)
		{
			bFound = true;
		}
	}
	else	
	{		
		for (var i = 0; i < iCourseCnt; i++) {
			if (FORM.CoreCourses[i].checked)
			{
				bFound = true;
			}
		}
	}
	
	if (bFound)
	{
		return true;
	}
	else
	{
		alert("You must select a Course to Cross Certify.");
		return false;
	}
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Courses Added successfully for <%= sName %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<% if trim(sCoursesWithCrossCerts) <> "" then %>
<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			The following list of Courses can be Cross Certified.<br>
			If you wish to Cross Certify, check the box of the cross certification course and click Cross Certify.
		</td>
	</tr>
	<tr>
		<td>
			<form name="frm1" action="UserAddCrossCerts.asp" method="POST" onSubmit="return CheckCoreCourses(this)">
			<input type="hidden" name="UserID" value="<%=iUserID%>">
			<table>
			<%
				for each sCurrentCourse in split(sCoursesWithCrossCerts, ", ")
					'split out ID of the user course and CourseID of the course
					aCurrentCourse = split(sCurrentCourse,"/")
					iCurrentID = aCurrentCourse(0)					
					iCurrentCourseID = aCurrentCourse(1)
					oCourseObj.CourseID = iCurrentCourseID
					set rs = oCourseObj.GetCourse()
					
					if not rs.eof then
						response.write("<tr>" & vbcrlf)
						response.write("<td><input type=""checkbox"" name=""CoreCourses"" value=""" & iCurrentID & """></td>" & vbcrlf)
						response.write("<td>" & rs("Name") & "</td>" & vbcrlf)
						response.write("</tr>" & vbcrlf)						
					end if
				next
			%>
				<tr>
					<td colspan="2">
						<br>
						<input type="submit" name="submit1" value="Cross Certify">
					</td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<% end if %>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
set oCourseObj = nothing
%>

<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>

<%
dim rs 'as object
dim sName 'as string
dim sSQL 'as string
dim sClass 'as string
dim oCourseObj, oUserObj 'as object
dim iCourseID, iID 'as integer
dim iUserID 'as integer
dim sCoreCourses 'as string
dim aCoreCourses 'as array
dim bFirst 'as boolean
dim i 'as integer
dim sCourseName 'as string
dim sReferrer 'as string

iUserID = trim(request("UserID"))
sCoreCourses = trim(request("CoreCourses"))
sReferrer = trim(request("referrer"))

'Get the name of the User
set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()
if not rs.eof then
	sName = rs("FirstName") & " " & rs("LastName")
else
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The User Information could not be retrieved.")
end if

if trim(sCoreCourses) = "" then
	response.redirect("/admin/error.asp?message=The Cross Certification Info. could not be retrieved.")
end if

'Get the next course to cross certify
aCoreCourses = split(sCoreCourses, ", ")
iID = aCoreCourses(0)

'reconstruct Core Courses string, without the current course
if cint(uBound(aCoreCourses)) > 0 then
	bFirst = true
	for i = 1 to ubound(aCoreCourses)
		if bFirst then
			sCoreCourses = aCoreCourses(i)
			bfirst = false
		else
			sCoreCourses = sCoreCourses & ", " & aCoreCourses(i)
		end if
	next
else
	sCoreCourses = ""
end if

'Get the name of the course
set rs = oUserObj.GetCourse(iID)
iCourseID = rs("CourseID")
sCourseName = rs("Name")

'get Cross Certifications
set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")
oCourseObj.CourseID = iCourseID

set rs = oCourseObj.GetAllCrossCertifications()

if cint(rs.recordcount) < 1 then
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The Cross Certification Info. could not be retrieved.")
end if

set oUserObj = nothing
set oCourseObj = nothing
%>

<script language="JavaScript" src="/admin/includes/FormCheck2.js"></script>
<script language="JavaScript">
function CheckCrossCerts(FORM)
{
	//make sure at least one item is picked
	var bFound = false;
	var iCourseCnt = FORM.CrossCertCourses.length;
	
	//Make sure at least one course is selected
	
	//if the iCourseCnt var is not an integer, there is only 1 course in the list
	//the length value is undefined if there is only one course
	if (!isInteger(iCourseCnt)){
		if (FORM.CrossCertCourses.checked)
		{
			bFound = true;
		}
	}
	else	
	{		
		for (var i = 0; i < iCourseCnt; i++) {
			if (FORM.CrossCertCourses[i].checked)
			{
				bFound = true;
			}
		}
	}
	
	if (bFound)
	{
		return true;
	}
	else
	{
		alert("You must select a Course to Cross Certify.");
		return false;
	}
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add Cross Certifications</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<b><%=sName%></b>
		</td>
	</tr>	
	<tr>
		<td>
			<br>
	<% if Ucase(sReferrer) = "CROSSCERTLIST" then %>			
			<a href="/Admin/Users/UserCrossCertCourseList.asp?UserID=<%=iUserID%>&CoreUserCourseID=<%=iID%>">Return to Cross Certification Listing</a><br>
	<% end if %>			
			<a href="UserCourseList.asp?UserID=<%=iUserID%>">Return to User's Course Listing</a><br>				
			<a href="/Admin/Users/UserList.asp">Return to User Listing</a>
		</td>
	</tr>	
</table>
<br>

<form name="frm1" action="UserAddCrossCertsProc.asp" method="POST" onSubmit="return CheckCrossCerts(this)">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<input type="hidden" name="UserCourseID" value="<%=iID%>">
<input type="hidden" name="CoreCourses" value="<%= sCoreCourses %>">
<input type="hidden" name="referrer" value="<%=sReferrer%>">
<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<b>Core Course:</b>&nbsp;<%= sCourseName %><br>
		</td>
	</tr>
	<tr>
		<td>
			<u>Available Cross Certifications:</u>
		</td>
	</tr>
<%
do while not rs.eof
	response.write("<tr>" & vbcrlf)
	response.write("<td>")
	response.write("&nbsp;&nbsp;<input type=""checkbox"" name=""CrossCertCourses"" value=""" & rs("CourseID") & """>&nbsp;&nbsp;")
	response.write(rs("Name"))
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)
	rs.MoveNext
loop
%>
	<tr>
		<td align="center">
			<br>
			<input type="submit" name="submit1" value="Cross Certify">
			&nbsp;&nbsp;
		<% if trim(sCoreCourses) <> "" then 'if more courses, skip to next Course %>
			<input type="button" name="skipbutton" value="Skip" onClick="javascript:window.location.href='UserAddCrossCerts.asp?UserID=<%=iUserID%>&CoreCourses=<%=sCoreCourses%>&referrer=<%=sReferrer%>'">
		<% else 'if no more courses, go to proc page %>
			<input type="button" name="skipbutton" value="Skip" onClick="document.frm1.CrossCertCourses.value='';document.frm1.submit()">
		<% end if %>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
%>
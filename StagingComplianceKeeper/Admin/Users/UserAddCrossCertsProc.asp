<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim iUserID 'as integer
dim oUserObj 'as object
dim rs 'as object
dim iID, iCurrentCrossCertID 'as integer
dim sCoreCourses 'as string
dim sCrossCertCourses 'as string
dim sName 'as string
dim bSuccess 'as boolean
dim sReferrer 'as string

iUserID = ScrubForSQL(request("UserID"))
iID = ScrubForSQL(request("UserCourseID"))
sCoreCourses = trim(request("CoreCourses"))
sCrossCertCourses = trim(request("CrossCertCourses"))
sReferrer = trim(request("Referrer"))

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

'Get the User's Name
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()

if not rs.eof then
	sName = rs("FirstName") & " " & rs("LastName")
else
	set oUserObj = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The User Information could not be retrieved.")
end if

'add Cross Certs
if sCrossCertCourses <> "" then
	for each iCurrentCrossCertID in split(sCrossCertCourses, ", ")
		bSuccess = oUserObj.AddCrossCertCourse(iID,iCurrentCrossCertID)
		
		if not bSuccess then
			response.redirect("/admin/error.asp?message=The Cross Certified Course could not be Added.")
		end if
	next
end if

if (sCoreCourses <> "") then 
	'there are still more courses to cross certify, redirect to Add cross cert screen
	response.redirect("UserAddCrossCerts.asp?UserID=" & iUserID & "&CoreCourses=" & sCoreCourses)
end if

set oUserObj = nothing
%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">The Cross Certifications have been completed for <%= sName %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>	
	<% if Ucase(sReferrer) = "CROSSCERTLIST" then %>
			<a href="/Admin/Users/UserCrossCertCourseList.asp?UserID=<%=iUserID%>">Return to Cross Certification Listing</a><br>
	<% end if %>
			<a href="UserCourseList.asp?UserID=<%=iUserID%>">Return to User's Course Listing</a><br>		
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


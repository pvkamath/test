<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>

<%
dim rs 'as object
dim sName 'as string
dim sSQL 'as string
dim sClass 'as string
dim oUserObj, oCourseObj 'as object
dim iUserID, iCourseID, iID 'as integer
dim iSearchCurExp, iSearchStatus 'as integer
dim iReloaded 'as integer
dim sLink 'as string

iUserID = trim(request("UserID"))
iSearchCurExp = trim(request("CurrentExpired"))
iSearchStatus = trim(request("Status"))
iReloaded = trim(request("Reloaded"))

'set default if blank and the screen was not reloaded(dropdowns changed)
if iReloaded <> "1" then
	if iSearchCurExp = "" then
		iSearchCurExp = 1
	end if

	if iSearchStatus = "" then
		iSearchStatus = 1
	end if
end if

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()
if not rs.eof then
	sName = rs("FirstName") & " " & rs("LastName")	
else
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The User Information could not be retrieved.")
end if

set rs = oUserObj.GetCourses(iSearchCurExp,iSearchStatus,0,"") 'set to 0 to show only Core Courses
%>

<script language="JavaScript">
function ConfirmDelete(p_iID,p_sName)
{
	if (confirm("Are you sure you wish to delete " + p_sName + "?"))
		window.location.href = 'ModUserCourseProc.asp?UserID=<%= iUserID %>&UserCourseID=' + p_iID + '&mode=Delete&CurrentExpired=<%=iSearchCurExp%>&Status=<%=iSearchStatus%>';
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Assigned Courses</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
	<tr>
		<td>
			<b><%=sName%></b>
		</td>
	</tr>	
	<tr>
		<td>
			<br>
			<a href="/Admin/Users/UserAddCourse.asp?UserID=<%=iUserID%>">Add Courses</a><br>
			<a href="/Admin/Users/UserList.asp">Return to User Listing</a>
		</td>
	</tr>	
</table>
<br>

<form name="frm1" action="UserCourseList.asp" method="POST">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<input type="hidden" name="reloaded" value="">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>View:</b></td>
		<td><% call DisplayCurrentExpiredDropDown(iSearchCurExp,"onChange='document.frm1.reloaded.value=1;document.frm1.submit()'",1) %></td>				
	</tr>
	<tr>
		<td><b>View:</b></td>	
		<td><% call DisplayStatusDropDown(iSearchStatus,"onChange='document.frm1.reloaded.value=1;document.frm1.submit()'",1) %></td>				
	</tr>	
</table>
</form>

<b>Courses</b>
<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Course &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Cross Certifications &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Completion Date &nbsp;</td>				
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>			
	</tr>
<%
if not rs.eof then
	do while not rs.eof
		iID = rs("ID")
		iCourseID = rs("CourseID")
			
		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
			
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		
		'Course Name
		response.write("<td><a href=""ModUserCourse.asp?UserID=" & iUserID & "&UserCourseID=" & rs("ID") & "&CurrentExpired=" & iSearchCurExp & "&Status=" & iSearchStatus & """>" & rs("Name") & "</a></td>" & vbcrlf)
		
		'Cross Certs
		'Check to see if this course has been cross certified by the user
		if (oUserObj.HasCrossCertifications(iUserID,iID)) then
			sLink = "<a href=""UserCrossCertCourseList.asp?UserID=" & rs("UserID") & "&CoreUserCourseID=" & rs("ID") & """>Edit</a>"
		else
			'check to see if course has Courses that can be cross certified
			set oCourseObj = New Course
			oCourseObj.ConnectionString = application("sDataSourceName")			
			
			if (oCourseObj.HasCrossCertifications(iCourseID)) then
				sLink = "<a href=""UserAddCrossCerts.asp?UserID=" & iUserID & "&CoreCourses=" & iID & """>Add</a>"
			else
				sLink = "N/A"
			end if
			
			set oCourseObj = nothing
		end if
		response.write("<td align=""center"">" & sLink & "</td>" & vbcrlf)
		
		'Completion Date
		response.write("<td align=""center"">")
		if (trim(rs("CompletionDate")) <> "" and not isnull(rs("CompletionDate"))) then
			response.write(rs("CompletionDate"))
		else
			response.write("Not Completed")
		end if
		response.write("</td>" & vbcrlf)
		
		'Delete
		response.write("<td align=""center""><a href=""javascript:ConfirmDelete(" & iID & ",'" & rs("Name") & "');"">Delete</a></td>" & vbcrlf)				
		
		response.write("</tr>" & vbcrlf)

		rs.MoveNext
	loop
else
%>
	<tr>
		<td colspan="6">No Results</td>
	</tr>
<%
end if
%>	
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
set oUserObj = nothing
%>
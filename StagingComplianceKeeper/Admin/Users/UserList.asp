<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/Search.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>

<%
dim rs
dim sSQL
dim sClass
dim oSearchObj, oUserObj
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sAction
dim sSearchEmail, sSearchLastName, iSearchCompanyId, iSearchProviderID, iSearchBranchID, iSearchUserStatus
dim bBranchesDropDownDisabled
dim sLink

set oSearchObj = New Search
oSearchObj.ConnectionString = application("sDataSourceName")

sAction = trim(request("Action"))

if Ucase(sAction) = "NEW" then
	Session.Contents.Remove("UserSearchEmail") 
	Session.Contents.Remove("UserSearchLastName")
	Session.Contents.Remove("UserSearchCompanyID") 
	Session.Contents.Remove("UserSearchUserStatus") 
else
	'Get Search values
	sSearchEmail = ScrubForSQL(request("SearchEmail"))
	if (sSearchEmail = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchEmail")) <> "") then
			sSearchEmail = Session("UserSearchEmail")
		end if
	else
		Session("UserSearchEmail") = sSearchEmail
	end if

	sSearchLastName = ScrubForSQL(request("SearchLastName"))
	if (sSearchLastName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchLastName")) <> "") then
			sSearchLastName = Session("UserSearchLastName")
		end if
	else
		Session("UserSearchLastName") = sSearchLastName
	end if
	
	iSearchCompanyID = ScrubForSQL(request("Company"))
	if (iSearchCompanyID = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchCompanyID")) <> "") then
			iSearchCompanyID = Session("UserSearchCompanyID")
		end if
	else
		Session("UserSearchCompanyID") = iSearchCompanyID
	end if

	iSearchBranchID = ScrubForSQL(request("Branch"))
	if (iSearchBranchID = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchBranchID")) <> "") then
			iSearchBranchID = Session("UserSearchBranchID")
		end if
	else
		Session("UserSearchBranchID") = iSearchBranchID
	end if

	iSearchUserStatus = ScrubForSQL(request("UserStatus"))
	if (iSearchUserStatus = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchUserStatus")) <> "") then
			iSearchUserStatus = Session("UserSearchUserStatus")
		end if
	else
		Session("UserSearchUserStatus") = iSearchUserStatus
	end if
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("UserCurPage")) <> "") then
			iCurPage = Session("UserCurPage")
		end if
	else
		Session("UserCurPage") = iCurPage
	end if
	
	'Add values to search if not empty
	if sSearchEmail <> "" then
		oSearchObj.Email = sSearchEmail
	end if
	
	if sSearchLastName <> "" then
		oSearchObj.LastName = sSearchLastName
	end if
	
'	if iSearchProviderID <> "" then
'		oSearchObj.ProviderID = iSearchProviderID
'	end if
	
	if iSearchCompanyID <> "" then
		oSearchObj.CompanyId = iSearchCompanyId
	end if 
	
	if iSearchUserStatus <> "" then
		oSearchObj.UserStatus = iSearchUserStatus
	end if
end if	

set rs = oSearchObj.SearchUsers()

set oSearchObj = nothing

If iCurPage = "" Then iCurPage = 1
If iMaxRecs = "" Then iMaxRecs = 50

%>

<script language="JavaScript">

function ConfirmDelete(p_iUserID,p_sName)
{
	if (confirm("Are you sure you wish to delete " + p_sName + "?"))
		window.location.href = 'ModUserProc.asp?UserID=' + p_iUserID + '&mode=Delete';
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Users</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="UserList.asp" method="POST">
<input type="hidden" name="action" value="SEARCH">
<input type="hidden" name="page_number" value="1">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Email:</b></td>
		<td><input  type="text" name="SearchEmail" value="<%=sSearchEmail%>" maxlength="100"></td>				
	</tr>
	<tr>			
		<td><b>Last Name:</b></td>	
		<td><input  type="text" name="SearchLastName" value="<%=sSearchLastName%>" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>Company:</b></td>
		<td><% call DisplayCompaniesDropDown(iSearchCompanyID,0) %></td>				
	</tr>
	<tr>			
		<td><b>User Status:</b></td>	
		<td><% call DisplayUserStatusDropDown(iSearchUserStatus,0) %></td>				
	</tr>			
	<tr>
		<td colspan="2" align="center">
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>
</table>
</form>

<table bgcolor="EaEaEa" cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Name &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Login &nbsp;</td>		
		<td class="reportHeaderText">&nbsp; Email &nbsp;</td>		
		<td class="reportHeaderText">&nbsp; Last Login &nbsp;</td>	
		<td class="reportHeaderText">&nbsp; User Type &nbsp;</td>		
		<td class="reportHeaderText" width="65">&nbsp; Delete &nbsp;</td>			
	</tr>
<%
	if not rs.eof then
		set oUserObj = New User
		oUserObj.ConnectionString = application("sDataSourceName")	
		
		'Set the number of records displayed on a page
		rs.PageSize = iMaxRecs
		rs.Cachesize = iMaxRecs
		iPageCount = rs.PageCount
	
		'determine which search page the user has requested.
		If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
		If clng(iCurPage) <= 0 Then iCurPage = 1
			
		'Set the beginning record to be display on the page
		rs.AbsolutePage = iCurPage		
		
		iCount = 0	
		do while (iCount < rs.PageSize) AND (not rs.eof)
			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if		
			
			response.write("<tr class=""" & sClass & """>" & vbcrlf)
			
			response.write("<td><a href=""ModUser.asp?UserID=" & rs("UserID") & """>" & rs("FirstName") & " " & rs("LastName") & "</a></td>" & vbcrlf)
			response.write("<td>" & rs("UserName") & "</td>" & vbcrlf)
			response.write("<td>" & rs("Email") & "</td>" & vbcrlf)
			response.write("<td nowrap>" & rs("LastLoginDate") & "</td>" & vbcrlf)
			Response.Write("<td>" & rs("GroupName") & "</td>" & vbCrLf)
			
			response.write("<td align=""center""><a href=""javascript:ConfirmDelete(" & rs("UserID") & ",'" & rs("FirstName") & " " & rs("LastName") & "');"">Delete</a></td>" & vbcrlf)
			
			response.write("</tr>" & vbcrlf)
			
			rs.MoveNext
			iCount = iCount + 1		
		loop

		if sClass = "row2" then
			sClass = "row1"
		else
			sClass = "row2"
		end if		
		
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td colspan=""6"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""UserList.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""UserList.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if

		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)	
		response.write("</table>" & vbcrlf)
	
		'display Page number
		response.write("<table width=""100%"">" & vbcrlf)	
		response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		response.write("<td align=""center"" colspan=""6""><br>" & vbcrlf)	
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)		
	else
		response.write("<tr><td colspan=""6"">There are currently no users that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>	
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oSearchObj = nothing
set oUserObj = nothing
set rs = nothing
%>
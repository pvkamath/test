<%
option explicit
on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->

<%
dim iCourseID 'as integer
dim oCourseObj,oUserObj 'as object
dim rs 'as object
dim sBackgroundImage, sStampImage, sSignatureImage 'as string
dim sTextBlock1, sTextBlock2 'as string
dim sSignerName, sSignerPosition 'as string
dim iCertificateID 'as integer
dim oPDF 'as object
dim oDoc 'as object
dim oPage 'as object
dim oDest 'as object
dim oTimesFont, oTimesFontBold 'as object
dim oCanvas 'as object
dim oImage 'as object
dim oParam 'as object
dim oTable 'as object
dim sTempText 'as string
dim sUserText 'as string
dim bCertificateReleased 'as boolean
dim iUserCourseID 'as integer
dim iUserID 'as integer
dim sName 'as string
dim sCompanyName 'as string
dim sCompanyNameOnCert 'as string
dim sCompletionDate 'as string
dim sCertificateExpirationDate 'as string
dim bSuccess 'as boolean
dim iXCord, iYCord 'as integer

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

iUserCourseID = ScrubForSQL(request("UserCourseID"))
iUserID = ScrubForSQL(request("UserID"))

'Get User Info
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()

if not rs.eof then
	if trim(rs("MiddleName")) <> "" then
		sName = rs("FirstName") & " " & rs("MiddleName") & ". " & rs("LastName")
	else
		sName = rs("FirstName") & " " & rs("LastName")
	end if		
	
	sCompanyName = rs("CompanyName")
	if isnull(sCompanyName) then
		sCompanyName = ""
	end if	
		
	sCompanyNameOnCert = rs("CertCompanyName")
	if isnull(sCompanyNameOnCert) then
		sCompanyNameOnCert = ""
	end if	
	
	sUserText = sName
	
	'Add company info
	if trim(sCompanyNameOnCert) <> "" then
		sUserText = sUserText & " of " & sCompanyNameOnCert	
	elseif trim(sCompanyName) <> "" then
		sUserText = sUserText & " of " & sCompanyName
	end if
else
	set rs = nothing
	set oCourseObj = nothing
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The User Info. could not be retrieved.")
end if

'Get course Info
set rs = oUserObj.GetCourse(iUserCourseID)

iCourseID = rs("CourseID")
sCompletionDate = rs("CompletionDate")
sCertificateExpirationDate = rs("CertificateExpirationDate")
bCertificateReleased = rs("CertificateReleased")

'make sure the certificate has been released
if not bCertificateReleased then
	set rs = nothing
	set oCourseObj = nothing
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The Certificate has not been released.")
end if

'get certificate info
oCourseObj.CourseID = iCourseID

set rs = oCourseObj.GetCertificate()

if not rs.eof then
	iCertificateID = rs("CertificateID")
	sTextBlock1 = trim(rs("TextBlock1"))
	sTextBlock2 = trim(rs("TextBlock2"))
	sSignerName = trim(rs("SignerName"))
	sSignerPosition = trim(rs("SignerPosition"))
	sBackgroundImage = trim(rs("BackgroundImage"))	
	sStampImage = trim(rs("StampImage"))
	sSignatureImage = trim(rs("SignatureImage"))
else
	set rs = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The Certifacte could not be retrieved.")
end if

set rs = nothing
set oCourseObj = nothing
set oUserObj = nothing

'Create PDF
Set oPDF = Server.CreateObject("Persits.PDFManager")
oPDF.RegKey = "IWez17RlvL6PIQc213KacCXF1eD2xzmp+C8FGLDD04ptU96y5Mi8KtfWC9XKP4Nl6qpjPqlOSwG3"
Set oDoc = oPDF.CreateDocument

Set oPage = oDoc.Pages.Add(792,612)

Set oDest = oDoc.CreateDest(oPage, "Fit=True")
oDoc.OpenAction = oDest

Set oTimesFont = oDoc.Fonts("Times-Roman")
Set oTimesFontBold = oDoc.Fonts("Times-Bold")
set oCanvas = oPage.Canvas

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_logo.gif") )
Set oParam = oPdf.CreateParam

oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 525
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_header_1.gif") )
oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 475
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam


if sBackgroundImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sBackgroundImage) )
	oParam("x") = (oPage.Width - oImage.Width) / 2
	oParam("y") = (450 - 275) + ((275 - oImage.Height) / 2)
	oParam("ScaleX") = oImage.ResolutionX / 72
	oParam("ScaleY") = oImage.ResolutionY / 72
	oPage.Background.DrawImage oImage, oParam
end if

set oTable = oDoc.CreateTable("rows=3; cols=2; cellspacing=1; cellpadding=2; height=435; width=680; border=1;bordercolor=white;")
oTable.Font = oTimesFont

With oTable.Rows(1)
	.Height = "275"
	.Cells(1).ColSpan = 2
	.Cells(1).AddText sTextBlock1 & " <b>" & sUserText & "</b> " & FormatTextForHTML(sTextBlock2), "alignment=left;color=black;size=14;html=true;"
end with

With oTable.Rows(2)
	.Height = "120"
	.Cells(1).width = 400
	.Cells(2).width = 280		

if sStampImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sStampimage) )
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(1).Canvas.DrawImage oImage, oParam
else
	.Cells(1).AddText "", "alignment=left;color=black;html=true;"
end if

if sSignatureImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sSignatureImage) )
else
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & Application("DefaultSignatureImage")) )
end if
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(2).Canvas.DrawImage oImage, oParam
end with

With oTable.Rows(3)
	.Height = "40"
	.Cells(1).width = 400	
	.Cells(2).width = 280		
	
	.Cells(1).AddText "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br><b>" & "Authentication Number :1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid & "</b>", "alignment=left;color=black;size=12;html=true;"
	.Cells(2).AddText "<font color=""#295385"" style=""font-size:16pt;"" face=""Verdana, Arial, helvetica""><b>" & sSignerName & "</b></font><br><b>" & sSignerPosition & "</b>", "alignment=right;color=black;size=12;html=true;"
end with

oCanvas.DrawTable oTable, "x=54;y=450"

'check if expired
if (datediff("d",date(),sCertificateExpirationDate) < 0) then
	for iYCord = 570 to 0 step -120
		for iXCord = 50 to 750 step 200
			oPage.Background.DrawText "EXPIRED", "x=" & iXCord & "; y=" & iYCord & "; size=20; angle=0; color=red;", oTimesFont
		next
	next
end if

'oCanvas.DrawText sTextBlock1 & " <b>First Name Last Name of Company</b> " & FormatTextForHTML(sTextBlock2), "x=54; y=450; html=true; width=684; size=14; alignment=left; rendering=0", oTimesFont

'oCanvas.DrawText "Issue Date: " & formatdatetime(date(),2), "x=54; y=350; size=12; alignment=left; rendering=0", oTimesFont

' Send directly to browser, do not save a temporary copy on disk
oDoc.SaveHttp "attachment;filename=certificate.pdf"

set oTable = nothing
set oPDF = nothing
set oDoc = nothing
set oPage = nothing
set oDest = nothing
set oTimesFont = nothing
Set oTimesFontBold = nothing
set oImage = nothing
%>

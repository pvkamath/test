<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->
<%
dim iUserID
dim sMode
dim oUserObj
dim rs
dim sLogin
dim sEmail, sOldEmail, sPassword, sConfirmPassword
dim sFirstName, sMid, sLastName
dim iGroupID, iProviderID, iBranchID, iCompanyId
dim sBranchIdList
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo
dim sCity, sAddress, sZipcode, sPhone, sFax
dim iStateID, iUserStatus, iCrossCertEligible, iNewsletter, iAllowedToAddUsers
dim sMarketWareConfirmNo
dim sNotes
dim sMessage
dim bSuccess

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

sMode = trim(request("Mode"))
iUserID = ScrubForSQL(request("UserID"))
sEmail = ScrubForSQL(request("Email"))
sLogin = ScrubForSql(request("Login"))
sOldEmail = ScrubForSQL(request("OldEmail"))
sPassword = ScrubForSQL(request("Password"))
sConfirmPassword = ScrubForSQL(request("ConfirmPassword"))
sFirstName = ScrubForSQL(request("FirstName"))
sMid = ScrubForSQL(request("MidInitial"))
sLastName = ScrubForSQL(request("LastName"))
iGroupID = ScrubForSQL(request("GroupID"))
iCompanyId = ScrubForSql(request("Company"))
'iBranchId = ScrubForSql(request("BranchId"))
sBranchIdList = ScrubForSql(request("BranchId"))
iProviderId = ScrubForSql(request("Provider"))

if iGroupId = 10 and sBranchIdList = "" then
	HandleError("This operation requires a valid Branch selection.")
end if
iProviderId = ScrubForSql(request("Provider"))
if iGroupId = 20 and iProviderId = "" then
	HandleError("This operation requires a valid Provider selection.")
end if 

sCity = ScrubForSQL(request("City"))
iStateID = ScrubForSQL(request("State"))
sAddress = ScrubForSQL(request("Address"))
sZipcode = ScrubForSQL(request("Zipcode"))
sPhone = ScrubForSQL(request("Phone"))
sFax = ScrubForSQL(request("Fax"))
iUserStatus = ScrubForSQL(request("UserStatus"))
sNotes = ScrubForSQL(request("Notes"))


if UCase(sMode) = "ADD" then
	'Make Sure this Email Address is not in the system
	if (oUserObj.EmailExists(sEmail)) then
		response.redirect("/admin/error.asp?message=The email address - " & sEmail & " - is being used by another user.  Please choose another email address.")		
	end if
	
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MidInitial = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.Notes = sNotes
	
	iUserID = oUserObj.AddUser()
	
	if cint(iUserID) > 0 then
		sMessage = sFirstName & " " & sLastName & " has been Added successfully."
	else
		response.redirect("/admin/error.asp?message=The User was not Added successfully.")		
	end if
elseif UCase(sMode) = "EDIT" then
	'if email has changed, Make Sure this Email Address is not in the system
	if (trim(sEmail) <> trim(sOldEmail)) and (oUserObj.EmailExists(sEmail)) then
		response.redirect("/admin/error.asp?message=The email address - " & sEmail & " - is being used by another user.  Please choose another email address.")		
	end if
	
	oUserObj.UserID = iUserID
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MidInitial = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.Notes = sNotes
	
	bSuccess = oUserObj.EditUser()
	
	if bSuccess then
		sMessage = sFirstName & " " & sLastName & " has been Edited successfully."
	else
		response.redirect("/admin/error.asp?message=The User was not Edited successfully.")		
	end if
elseif UCase(sMode) = "DELETE" then
	oUserObj.UserID = iUserID
	oUserObj.UserStatus = 3 'deleted status
	bSuccess = oUserObj.ChangeStatus()
	
	if bSuccess then
		response.redirect("UserList.asp?&page_number=" & Session("UserCurPage"))
	else
		response.redirect("/admin/error.asp?message=The User was not Deleted successfully.")		
	end if
end if

%>

<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->


<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMessage %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
%>

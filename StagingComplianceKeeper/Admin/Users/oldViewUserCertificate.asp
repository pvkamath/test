<%
option explicit
on error resume next
%>
<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" ----------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ---------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->

<%
dim sName 'as string
dim iCourseID 'as integer
dim oCourseObj, oUserObj 'as object
dim rs 'as object
dim sBackgroundImage, sStampImage, sSignatureImage 'as string
dim sTextBlock1, sTextBlock2 'as string
dim sSignerName, sSignerPosition 'as string
dim iCertificateID 'as integer
dim iUserID 'as integer
dim sCompanyName 'as string

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

set oCourseObj = New Course
oCourseObj.ConnectionString = application("sDataSourceName")

iCourseID = ScrubForSQL(request("CourseID"))
iUserID = ScrubForSQL(request("UserID"))

'Get User Info
oUserObj.UserID = iUserID
set rs = oUserObj.GetUser()

if not rs.eof then
	if trim(rs("MidInitial")) <> "" then
		sName = rs("FirstName") & " " & rs("MidInitial") & ". " & rs("LastName")
	else
		sName = rs("FirstName") & " " & rs("LastName")
	end if		
	
	sCompanyName = rs("CompanyName")

	if isnull(sCompanyName) then
		sCompanyName = ""
	end if	
else
	set rs = nothing
	set oCourseObj = nothing
	set oUserObj = nothing
	response.redirect("/admin/error.asp?message=The User Info. could not be retrieved.")
end if

'Get Certificate info
oCourseObj.CourseID = iCourseID

set rs = oCourseObj.GetCertificate()

if not rs.eof then
	iCertificateID = rs("CertificateID")
	sTextBlock1 = trim(rs("TextBlock1"))
	sTextBlock2 = trim(rs("TextBlock2"))
	sSignerName = trim(rs("SignerName"))
	sSignerPosition = trim(rs("SignerPosition"))
	sBackgroundImage = trim(rs("BackgroundImage"))	
	sStampImage = trim(rs("StampImage"))
	sSignatureImage = trim(rs("SignatureImage"))
else
	set rs = nothing
	set oCourseObj = nothing
	response.redirect("/admin/error.asp?message=The Certifacte could not be retrieved.")
end if

set rs = nothing
set oCourseObj = nothing
set oUserObj = nothing
%>

<style>
table{
	
}
font{
font-size:13pt;
font-family: "Times New Roman", Times, serif;
font-weight:bold;
letter-spacing : 2px;

}
</style>

<table border="1" height="100%" width="99%" cellpadding="0" cellspacing="0" align="center" bordercolor="Black">
	<tr>
		<td valign="top">
			<table border="0" cellpadding="0" width="100%" cellspacing="0">
				<tr>
				    <td width="100%" background="/media/images/certificate_bg_top.gif" height="150" valign="top">
						<p align="center"><img border="0" src="/media/images/trans.gif" width="100" height="25"><br>
						<img border="0" src="/media/images/certificate_logo.gif" width="556" height="90">
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" colspan="2">
						<p align="center"><img border="0" src="/media/images/certificate_header_1.gif" width="532" height="45">
					</td>
				</tr>
				<tr>
					<td valign="top">
						<table border="0" align="center" height="560" border="0" width="90%" style="background-position : center;background-repeat : no-repeat;background-attachment : fixed;background-color :transparent;" <% if sBackgroundImage <> "" then %> background="<%=Application("sDynCertificateImageFolder")%><%= sBackgroundImage %>" <% end if %>>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td valign="top">
									<div align="justify">
									<b><font face="Verdana, Arial, helvetica">		
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=sTextBlock1%>&nbsp;<font color="#295385"  style="font-size:14pt;"><b><%= sName %></font>
									<% if trim(sCompanyName) <> "" then %>
									of <font color="#295385"  style="font-size:14pt;" face="Verdana, Arial, helvetica"><B><%= sCompanyName %></B></font>
									<% end if %>
									</font>	</b>
									<b><font face="Verdana, Arial, helvetica"><%=sTextBlock2%></font></b>
									</div>		
								</td>
							</tr>		
							<tr>
								<td>
									<table align="center" border="0" width="100%">
										<tr>
											<td align="left" valign="bottom">
											<% if sStampImage <> "" then %>
												<img src="<%=Application("sDynCertificateImageFolder")%><%=sStampimage%>"> <br>
											<% end if %>
												<b><font face="Verdana, Arial, helvetica" size="2">Issue Date:<%=formatdatetime(date(),2)%></font></b><br>
												<b>Authentication Number :1000DOI<%=month(now())%><%=day(now())%><%=year(now())%>UD<%=iUserID%>CCD<%=iCertificateid%>	
											</td>
											<td align="right" valign="bottom">
												<table>
													<tr>
														<td>
														<%if sSignatureImage <> "" then %>
															<img src="<%=Application("sDynCertificateImageFolder")%><%= sSignatureImage %>"> 
														<%else%>
															<img src="<%=Application("sDynCertificateImageFolder")%><%=Application("DefaultSignatureImage")%>"> 
														<%end if%>
															<span style="text-decoration: overline">            
															<font color="#295385" style="font-size:16pt;" face="Verdana, Arial, helvetica"><b><br> 
															<%= sSignerName %><br></span></font></b>
															<font face="Verdana, Arial, helvetica" size="3"><b>
															<%= sSignerPosition %><br></font></b>
														</td>
													</tr>
												</table>
											</td>
										</tr>														
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<p align="center"><b><font face="Verdana, Arial, helvetica" size="2">Please maintain for your records</p>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
</table>
<br>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->
<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->

<%
dim oUserObj 'as object
dim rs 'as object
dim iUserID 'as integer
dim sMode 'as string
dim sLogin
dim sEmail, sPassword, sConfirmPassword 'as string
dim sFirstName, sMid, sLastName 'as string
dim iGroupID, iProviderID, iBranchID 'as integer
dim sBranchIdList
dim bBranchesDropDownDisabled 'as integer
dim iScreenRefreshed 'as integer
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo 'as string
dim sCity, sAddress, sZipcode, sPhone, sFax 'as string
dim iStateID, iUserStatus 'as integer
dim bCrossCertEligible, bNewsletter 'as boolean
dim sCrossCertEligibleChecked, sNewsletterChecked 'as string
dim sMarketWareConfirmNo 'as string
dim sNotes 'as string
dim sAllowedToAddUsersChecked, sAllowedToAddUsersDisabled  'as string
dim bAllowedToAddUsers 'as boolean
dim iCompanyId

iUserID = ScrubForSQL(request("UserID"))

iScreenRefreshed = trim(request("ScreenRefresh"))
if trim(iScreenRefreshed) = "" then
	iScreenRefreshed = 0 
end if

if iUserID = "" then

	Response.Write("Invalid UserID.")
	Response.End

else
	sMode = "Edit"
	
	'Pull Info from db
	set oUserObj = New User
	oUserObj.ConnectionString = application("sDataSourceName")
	oUserObj.UserID = iUserID
	set rs = oUserObj.GetUser()
		
	sLogin = trim(rs("UserName"))
	sEmail = trim(rs("Email"))
	sPassword = trim(rs("Password"))
	sFirstName = trim(rs("FirstName"))
	sMid = trim(rs("MiddleName"))
	sLastName = trim(rs("LastName"))
	iGroupID = trim(rs("GroupID"))
	iCompanyId = trim(rs("CompanyID"))
	'iBranchId = trim(rs("BranchId"))
	sBranchIdList = oUserObj.GetBranchIdListString
	iProviderId = trim(rs("ProviderId"))
	sCity = trim(rs("City"))
	iStateID = trim(rs("StateID"))
	sAddress = trim(rs("Address"))
	sZipcode = trim(rs("Zipcode"))
	sPhone = trim(rs("Phone"))
	sFax = trim(rs("Fax"))
	iUserStatus = trim(rs("UserStatus"))
	sNotes = trim(rs("Notes"))
		
	set oUserObj = nothing

	sConfirmPassword = sPassword
	
end if

%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">

function CompanyChange()
{
	document.frm1.BranchId.value = '';	
	document.frm1.Provider.value = '';
									
	RefreshScreen();
}

function StatusChange()
{
	if (document.frm1.GroupID.value == '20')
	{
		document.frm1.BranchId.value = '';
	}
	
	RefreshScreen(); 
}

function RefreshScreen()
{
	document.frm1.ScreenRefresh.value = 1;
	document.frm1.action = "ModUser.asp";
	
	//EnableFields();
	
	document.frm1.submit();	
}

function Validate(FORM)
{   	
	if(!checkEmail(FORM.Email) || !checkString(FORM.Login, "Login") || !checkString(FORM.Password,"Password") || !checkString(FORM.ConfirmPassword,"Confirm Password"))
		return false;
		
	if (FORM.Password.value != FORM.ConfirmPassword.value)
	{
		alert("The Password and Confirm Password fields do not match.")
		FORM.Password.focus();
		return false;
	}
	
	if (!checkString(FORM.FirstName,"First Name") || !checkString(FORM.LastName,"Last Name") || !checkString(FORM.GroupID,"User Type"))
		return false;

	//if the UserType/GroupID is Company Admin, the Company field must be selected
	if ((FORM.GroupID.selectedIndex == 4) && (FORM.Provider.selectedIndex == 0))
	{
		alert("The Provider field must be set since this user is a Provider Admin.");
		FORM.GroupID.focus();
		return false;
	}
	
	//check address
	if (!checkString(FORM.Address,"Address"))
		return false;
	
	//if zipcode is not blank, make sure it's in the proper format
	if (FORM.Zipcode.value != "")
	{
		if (!checkZIPCode(FORM.Zipcode))
			return false;
	}
	
	//if phone # is not blank, make sure it's in the proper format
	if (!checkUSPhone(FORM.Phone))
		return false;
	
	//if fax # is not blank, make sure it's in the proper format
	if (FORM.Fax.value != "")
	{
		if (!checkUSPhone(FORM.Fax))
			return false;
	}	

	return true;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">View User Password</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Login:</b></td>
		<td><% = sLogin %></td>				
	</tr>
	<tr>
		<td><b>Password:</b></td>
		<td><% = sPassword %></td>				
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
%>
function OpenNewWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=720,height=420,left=50,top=50')
} 
function OpenChildWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=740,height=450,left=50,top=50')
}
function OpenIndexWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=740,height=550,left=50,top=50')
}
function OpenTinyWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=10,height=10,left=0,top=0')
}
function OpenFundFactWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=825,height=520,left=50,top=50')
}
function OpenFundModWin(url, win) {
	popupWin = window.open(url, win, 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=750,height=540,left=50,top=50')
}
function preloadImages() {
	if (document.images) {
		grn_nav_home_over = newImage("/media/images/grn_nav_home-over.gif");
		grn_nav_open_over = newImage("/media/images/grn_nav_open-over.gif");
		grn_nav_access_over = newImage("/media/images/grn_nav_access-over.gif");
		grn_nav_prof_over = newImage("/media/images/grn_nav_prof-over.gif");
		grn_nav_contact_over = newImage("/media/images/grn_nav_contact-over.gif");
		nav_prof_over = newImage("/media/images/nav_5-over.gif");
		nav_overview_over = newImage("/media/images/nav_1-over.gif");
		nav_profiles_over = newImage("/media/images/nav_2-over.gif");
		nav_prices_over = newImage("/media/images/nav_3-over.gif");
		nav_info_over = newImage("/media/images/nav_4-over.gif");
		preloadFlag = true;
	}
}

function OpenHelpWin(url, win) {
	popupWin = window.open(url, win, 'menubar=0,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=500,height=450,left=50,top=50')
}
function OpenInstructions(p_nKeywordID) {
	popupWin = window.open('/main/instructions.asp?keywordID='+ p_nKeywordID, 'Instructions', 'menubar=0,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=500,height=450,left=50,top=50')
}

function OpenGlossary() {
	window.open('/main/glossary.asp','glossary','HEIGHT=550,WIDTH=450,scrollbars=1')
}	

<%
' DisplayBranchesDropDown(p_iSelectedValue,p_iCompanyID,p_iDisplayType)
' DisplayBusinessTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayCompaniesDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayCourseDropDown(p_iSelectedValue,p_sCourseType,p_bDisabled,p_iDisplayType)
' DisplayCrossCertCourseDropDown(p_sDropDownName,p_iSelectedValue,p_oRS,p_sCourseIDs,p_iDisplayType)
' DisplayCourseTypesDropDown(p_iSelectedValue)
' DisplayCrossCertificationTypesDropDown(p_iSelectedValue,p_iDisplayType, p_bCrossCertTypeDisabled)
' DisplayCurrentExpiredDropDown(p_iSelectedValue,p_sJSEvents)
' DisplayLessonSectionsDropDown(p_iSelectedValue,p_iLessonID,p_bDisabled,p_iDisplayType)
' DisplayProgramDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayReleaseTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayStatesDropDown(p_iSelectedValue,p_iDisplayType,p_iRestricted)
' DisplayStatusDropDown(p_iSelectedValue,p_sJSEvents)
' DisplaySupplementCourseDropDown(p_iSelectedValue,p_iCourseID,p_iDisplayType)
' DisplayTestTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayTimeTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayUserStatusDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayUserTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayVerificationMethodDropDown(p_iSelectedValue,p_iDisplayType)
' ValueInDelString(p_sValue,p_sSearchString)
'

' Name : DisplayBranchesDropDown
' Purpose : displays a drop down box filled with all existing branch names
' Inputs : p_sDropdownName
'		 : p_iSelectedValue - the value the drop down should be preselected to
'	     : p_iCompanyID = the company ID of the Branches to retrieve
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 
'						    2 is for Assigning on the User screen, 3 is for straight selection
' Outputs : prints the drop down to the screen
Sub DisplayBranchesDropDown(p_sDropdownName,p_iSelectedValue,p_iCompanyID,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	
	set objConn = New HandyADO

	if p_iCompanyId >= 0 then
		sSQL = "SELECT CBs.BranchID, CBs.BranchNum, CBs.Name " & _
			   "FROM CompanyBranches AS CBs " & _
			   "LEFT JOIN BranchInfo AS BI " & _
			   "ON (BI.BranchId = CBs.BranchId) " & _
			   "WHERE CBs.CompanyID = '" & p_iCompanyID & "' " & _
			   "ORDER BY BI.BranchNum"
		set rs = objConn.GetDisconnectedRecordset(application("sTpDataSourceName"), sSQL, false)
	else
		sSQL = "SELECT BranchID, BranchNum, Name FROM CompanyBranches " & _
			   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
			   "ORDER BY BranchNum"
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	

	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if

	if p_sDropdownName <> "" then
		Response.Write("<select name=""" & p_sDropdownName & """ " & sDisabled & ">" & vbCrLf)
	else
		response.write("<select name=""Branch"" " & sDisabled & ">" & vbcrlf)
	end if 

	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif not (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">No Branch</option>" & vbcrlf)
	end if

	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("BranchID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if

		response.write("<option value=""" & rs("BranchID") & """ " & sSelected & ">Branch " & rs("BranchNum") & "</option>" & vbcrlf)
		rs.MoveNext
	loop

	response.write("</select>" & vbcrlf)

	set rs = nothing
	set objConn = nothing

End Sub


' Name: DisplayBranchAssociatesDropDown
' Purpose: Displays a dropdown box filled with all existing associates for a given branch.
' Inputs: p_sDropdownName - Name of the dropdown form element
'		  p_iBranchId - Unique ID of the branch to work with
'		  p_iSelectedValue - the value the dropdown box should be preselected to
'		  p_iDisplayType - 0 for search, 1 for assignment
'		  p_bDisabled - Determines if the dropdown should be disabled
' Outputs: Prints the dropdown to the screen
sub DisplayBranchAssociatesDropDown(p_sDropdownName, p_iBranchId, p_iSelectedValue, p_iDisplayType, p_bDisabled)

	dim oConn
	dim oRs
	dim sSql
	dim sSelected
	dim sDisabled

	if p_iBranchId = "" or not isnumeric(p_iBranchId) then
		exit sub
	end if 

	set oConn = server.CreateObject("ADODB.Connection")

	if p_iBranchId < 0 then

		oConn.ConnectionString = application("sDataSourceName")
		oConn.Open
		
		sSql = "SELECT * FROM Associates " & _
			   "WHERE BranchId = '" & g_iBranchId & "'"
	
		set oRs = oConn.Execute(sSql)
					 
	elseif p_iBranchid >= 0 then

		oConn.ConnectionString = application("sTpDataSourceName")
		oConn.Open
	
		sSql = "SELECT * FROM Users " & _
			   "LEFT JOIN UsersInfo AS UI " & _
			   "ON (UI.UserId = Users.UserId) " & _
			   "WHERE BranchId = '" & g_iBranchId & "'"
			   
		set oRs = oConn.Execute(sSql)

	end if 
	
	
	if p_sDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if 
	
	
	'Start the dropdownbox, and use a custom name if we were given one.
	if not p_sDropdownName = "" then
		Response.Write("<select name=""" & p_sDropdownName & """" & sDisabled & ">" & vbCrLf)
	else
		Response.Write("<select name=""BranchAssociateId""" & sDisabled & ">" & vbCrLf)
	end if
	
	
	if (cint(p_iDisplayType) = 0) then
		Response.Write("<option value="""">ALL</option>" & vbCrLf)
	else
		Response.Write("<option value="""">Please Select a Branch Associate</option>" & vbCrLf)
	end if
	
	
	do while not oRs.EOF
		
		if (trim(p_iSelectedValue) = trim(oRs("UserId"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if 
		
		
		Response.Write("<option value=""" & oRs("UserId") & """ " & sSelected & ">" & oRs("LastName") & ", " & oRs("FirstName") & "</option>" & vbCrLf)
		
		oRs.MoveNext
		
	loop
	
	Response.Write("</select>" & vbCrLf)
	
	set oRs = nothing
	oConn.Close
	set oConn = nothing
	
end sub


' Name : DisplayBusinessTypeDropDown
' Purpose : displays a drop down box filled with all existing Business Types
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens
' Outputs : prints the drop down to the screen
Sub DisplayBusinessTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM BusinessTypes"
	
	if trim(p_iSelectedValue) <> "" then
		sSQL = sSQL & "WHERE BusinessTypeID = '" & p_iSelectedValue & "' "
	end if

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""BusinessType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select a Business Type</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("BusinessTypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("BusinessTypeID") & """ " & sSelected & ">" & rs("BusinessType") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name : DisplayCompaniesDropDown
' Purpose : displays a drop down box filled with all existing company names
' Preconditions: Assumes the existance of the CreateBranchList script via the
'	CreateBranchesJavascript sub.
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen, 3 is for Searching on the User List, 4 is for Searching on the Company Branch list.
' Outputs : prints the drop down to the screen
sub DisplayCompaniesDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT CompanyID, Name FROM Companies "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""Company"" ")
	
	'determine which method to use
	if (cint(p_iDisplayType) = 3) then
		response.write("onChange=""CompanyChange();"">" & vbcrlf)
	elseif (cint(p_iDisplayType) = 4) then
		Response.write(">" & vbCrLf)	
	else
		response.write("onLoad=""UpdateBranchList(this.value)"" onChange=""UpdateBranchList(this.value);"">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) or (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Company</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("CompanyID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("CompanyID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayProvidersDropDown
' Purpose : displays a drop down box filled with all existing provider names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
sub DisplayProvidersDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT ProviderID, Name FROM Providers "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""Provider"">" & vbCrLf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Provider</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ProviderId"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ProviderID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name 	  : DisplayCourseDropDown
' Purpose : displays the course dropdown
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayCourseDropDown(p_iSelectedValue,p_oRS,p_sJSEvents,p_bDisabled,p_iDisplayType)
	dim rs
	dim sSelected
	dim sSQL
	dim bDisplayValue
	dim iCurrentCourseID, iPreviousCourseID
	dim sDisabled
	
	set rs = p_oRS
	
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	response.write("<select name=""CourseID"" " & sDisabled & " " & p_sJSEvents & ">" & vbcrlf)	
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select The Course</option>" & vbcrlf)	
	end if
	
	do while not rs.eof		
		iCurrentCourseID = rs("CourseID")
		if (trim(iCurrentCourseID) <> trim(iPreviousCourseID)) then
			if (trim(p_iSelectedValue) = trim(rs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CourseID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		end if
		iPreviousCourseID = iCurrentCourseID
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
End Sub

' Name 	  : DisplayCrossCertCourseDropDown
' Purpose : displays the cross cert course dropdown
' Inputs  : p_sDropDownName - the name of the dropdown
'		  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_oRS - the recordset to populate the dropdown 
'		  : p_sCourseIDs - the courseIDs the dropdown should filter out, optional
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayCrossCertCourseDropDown(p_sDropDownName,p_iSelectedValue,p_oRS,p_sCourseIDs,p_iDisplayType)
	dim rs
	dim sSelected
	dim sSQL
	dim iProgramID
	dim bDisplayValue
	
	set rs = p_oRS
	
	response.write("<select name=""" & p_sDropDownName & """>" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Course</option>")	
	end if
	
	do while not rs.eof		
		'true, if the dropdown should screen out Courses
		if (trim(p_sCourseIDs) <> "") then
			if (ValueInDelString(rs("CourseID"), p_sCourseIDs)) then  'located in this file
				bDisplayValue = false
			else
				bDisplayValue = true			
			end if 
		else
			bDisplayValue = true
		end if
		
		'display the course if set to true
		if bDisplayValue then
			if (trim(p_iSelectedValue) = trim(rs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CourseID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		end if
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
End Sub

' Name : DisplayCrossCertificationTypesDropDown
' Purpose : displays the course types drop down
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
'		    p_bCrossCertTypeDisabled - whether or not the dropdown should be disabled
' Outputs : prints the drop down to the screen
Sub DisplayCrossCertificationTypesDropDown(p_iSelectedValue,p_iDisplayType, p_bCrossCertTypeDisabled)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled

	set objConn = New HandyADO

	sSQL = "SELECT * FROM CrossCertificationTypes"

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	'Determin if dropdown should be disabled
	if (p_bCrossCertTypeDisabled) then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	response.write("<select name=""CrossCertType"" " & sDisabled & ">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name : DisplayCourseTypesDropDown
' Purpose : displays the course types drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	       p_iSearchType - 0 for searching, 1 for assigning
' Outputs : prints the drop down to the screen
sub DisplayCourseTypesDropDown(p_iSelectedValue, p_iSearchType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM CourseTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CourseType"">" & vbcrlf)
	
	if p_iSearchType = 0 then 
		response.write("<option value="""">ALL</option>")
	end if 
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("typeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayCurrentExpiredDropDown
' Purpose : displays the Current/Expired drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		 : p_iDisplayType - 0 is for displaying the All option, 1 is for not showing the All Option
' Outputs : prints the drop down to the screen
sub DisplayCurrentExpiredDropDown(p_iSelectedValue,p_sJSEvents,p_iDisplayType)
	dim sSelected
	
	response.write("<select name=""CurrentExpired"" " & p_sJSEvents & ">" & vbcrlf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>")
	end if
	
	if trim(p_iSelectedValue) = "1" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if
	response.write("<option value=""1"" " & sSelected & ">Current</option>" & vbcrlf)

	if trim(p_iSelectedValue) = "0" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if	
	response.write("<option value=""0"" " & sSelected & ">Expired</option>" & vbcrlf)
		
	response.write("</select>" & vbcrlf)
end sub

' Name : DisplayLessonSectionsDropDown
' Purpose : displays the Lesson Sections drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		   p_iLessonID - the Lesson ID of the lesson 
'		 : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayLessonSectionsDropDown(p_iSelectedValue,p_iLessonID,p_bDisabled,p_iDisplayType)
	dim oCourseObj
	dim rs
	dim sSelected
	dim sSQL
	
	set oCourseObj = New Course
	oCourseObj.ConnectionString = application("sDataSourceName")
	
	'Retrieve Course Sections
	oCourseObj.LessonID = iLessonID
	set rs = oCourseObj.GetAllLessonSections()

	if p_bDisabled then
		response.write("<select name=""LessonSectionID"" DISABLED>" & vbcrlf)
	else
		response.write("<select name=""LessonSectionID"">" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Section</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("SectionID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("SectionID") & """ " & sSelected & ">" & rs("Title") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set oCourseObj = nothing
end sub

' Name : DisplayProgramDropDown
' Purpose : displays the programs drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayProgramDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM Programs Order By ProgramID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CourseProgram"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Program</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ProgramID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ProgramID") & """ " & sSelected & ">" & rs("name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayReleaseTypeDropDown
' Purpose : displays the certificate release type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayReleaseTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM CertificateReleaseTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CertificateReleaseType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Release Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name: GetBranchName
' Purpose: Returns the full branch name given the branch ID
' Inputs: p_iBranchId
' Returns: Branch name
Function GetBranchName(p_iBranchId)

	if not (isNumeric(p_iBranchId)) or p_iBranchId = "" then
		
		GetBranchName = ""
		exit function
		
	end if
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = new HandyADO
	
	sSql = "SELECT Name FROM CompanyBranches WHERE BranchID = '" & p_iBranchID & "'"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	GetBranchName = oRs("Name")
	
	set oRs = nothing
	set oConn = nothing

End Function

' Name: GetStateName
' Purpose: Returns the full state name given the state ID
' Inputs: p_iStateId - state id
'		  p_bAbbrev - return abreviation instead of full name
' Returns: State name
Function GetStateName(p_iStateId, p_bAbbrev)

	if not (IsNumeric(p_iStateId)) or p_iStateId = "" then
	
		GetStateName = ""
		exit function
	
	end if

	dim oConn
	dim oRs
	dim sSql
	
	set oConn = new HandyADO
	
	sSql = "SELECT State, Abbrev FROM States WHERE StateID = '" & p_iStateID & "'"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if p_bAbbrev then
		GetStateName = oRs("Abbrev")
	else
		GetStateName = oRs("State")
	end if
	
	set oRs = nothing
	set oConn = nothing	

End Function

' Name : DisplayStatesDropDown
' Purpose : displays the states drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning, 2 is for state code assignment dropdown, 
'							3 for Front End Calendar. 4 for the Billing Info screen (option values is the entire name of state)
'		 : p_sFormName - Name of the dropdown box placed
' Outputs : prints the drop down to the screen
Sub  DisplayStatesDropDown(p_iSelectedValue,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	
	'Name the dropdown box
	dim sFormName
	if p_sFormName = "" then
		if (cint(p_iDisplayType) = 3) then
			sFormName = "SearchState"
		else
			sFormName = "State"
		end if 
	else
		sFormName = p_sFormName
	end if 
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM " & application("TrainingProDBName") & ".dbo.States "
	
	sSQL = sSQL & "	Order By State "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if (cint(p_iDisplayType) = 3) then
		response.write("<select name=""" & sFormName & """ onchange=""this.form.submit();"">" & vbcrlf)
	else
		response.write("<select name=""" & sFormName & """>" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	elseif (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">All States</option>")		
'	else
'		response.write("<option value="""">Please Select The State</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("StateID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if

		if p_iDisplayType = 2 then	
			Response.Write("<option value=""" & rs("Abbrev") & """ " & sSelected & ">" & rs("State") & "</option>" & vbCrLf)
		elseif p_iDisplayType = 4 then
			Response.Write("<option value=""" & rs("State") & """ " & sSelected & ">" & rs("State") & "</option>" & vbCrLf)
		else
			response.write("<option value=""" & rs("StateID") & """ " & sSelected & ">" & rs("state") & "</option>" & vbcrlf)
		end if
		rs.MoveNext
	loop

	response.write("</select>" & vbcrlf)

	set rs = nothing
	set objConn = nothing
end sub

' Name: DisplayOrgTypesDropDown
' Purpose: Displays the org types list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayOrgTypesDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "OrgType"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM OrganizationTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("OrganizationTypeId") & """")
		
		if oRs("OrganizationTypeId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("OrganizationType") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name: DisplayLicenseStatusDropDown
' Purpose: Displays the license statuses list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayLicenseStatusDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "LicenseStatus"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM LicenseStatusTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("LicenseStatusID") & """")
		
		if oRs("LicenseStatusId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("LicenseStatus") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name: DisplayLicensingTypeDropDown
' Purpose: Displays the licensing type list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayLicensingTypeDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "LicensingType"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM LicensingTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("LicensingTypeId") & """")
		
		if oRs("LicensingTypeId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("LicensingType") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name : DisplayStatusDropDown
' Purpose : displays the Status drop down
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		  : p_iDisplayType - 0 is for displaying the All option, 1 is for not showing the All Option
' Outputs : prints the drop down to the screen
sub DisplayStatusDropDown(p_iSelectedValue,p_sJSEvents,p_iDisplayType)
	dim sSelected
	
	response.write("<select name=""Status"" " & p_sJSEvents & ">" & vbcrlf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>")
	end if
	
	if trim(p_iSelectedValue) = "1" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if
	response.write("<option value=""1"" " & sSelected & ">Active</option>" & vbcrlf)

	if trim(p_iSelectedValue) = "0" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if	
	response.write("<option value=""0"" " & sSelected & ">Disabled</option>" & vbcrlf)
		
	response.write("</select>" & vbcrlf)
end sub

' Name : DisplayTestTypeDropDown
' Purpose : displays the test types drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayTestTypeDropDown(p_iSelectedValue,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM TestTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if p_bDisabled then
		response.write("<select name=""TestType"" DISABLED>" & vbcrlf)
	else
		response.write("<select name=""TestType""")
		
		if (cint(p_iDisplayType) <> 0) then
			response.write(" onChange='EnableDisableSections(this.form)'")
		end if
		
		response.write(">" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayTimeTypeDropDown
' Purpose : displays the Time Type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayTimeTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM TimeTypes "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if (cint(p_iDisplayType) = 1) then
		sEvent = "onChange=EnableDisableTimedMinutes(this.form)"
	end if
	
	response.write("<select name=""TimeType"" " & sEvent & ">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name: DisplayUsersDropdown
' Purpose: Creates a dropdown to select from a list of a company's users
' Inputs: p_iSelectedValue, p_iCompanyId, p_iBranchId,
'		  p_iDisplayType: 0 for search
' Outputs: Prints the dropdown to the screen
sub DisplayUsersDropdown(p_iSelectedValue, p_iCompanyId, p_iBranchId, _
	p_iDisplayType)
	
	dim oConn
	dim oRs
	dim sSql
	
	dim bFirstClause
	bFirstClause = true
	
	dim sSelected 'Holds text to select an option
	
	sSql = "SELECT UserID, FirstName, LastName, UserName FROM Users "
	
	'Add Company search clause
	if (p_iCompanyId <> "") and (p_iCompanyId <> 0) then 
		
		if bFirstClause then
			bFirstClause = false
			sSql = sSql & " WHERE "
		else
			sSql = sSql & " AND "
		end if
			
		sSql = sSql & "CompanyID = '" & p_iCompanyId & "'" 
		
	end if
	
	'Add Branch search clause
	if (p_iBranchId <> "") and (p_iBranchId <> 0) then
	
		if bFirstClause then
			bFirstClause = false
			sSql = sSql & " WHERE "
		else
			sSql = sSql & " AND "
		end if
		
		sSql = sSql & "BranchID = '" & p_iBranchId & "'"
	
	end if
	
	sSql = sSql & " ORDER BY FirstName"
	
	set oConn = new HandyADO
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

	Response.Write("<select name=""Users"">" & vbCrLf)
	
	if p_iDisplayType = 0 then
		Response.Write("<option value="""">All Users</option>" & vbCrLf)
	end if
	
	do while not oRs.EOF
	
		if IsNumeric(p_iSelectedValue) then
			if oRs("UserID") = CINT(p_iSelectedValue) then
				sSelected = "SELECTED"
			end if
		else
			sSelected = ""
		end if
	
		Response.Write("<option value=""" & oRs("UserID") & """ " & _
			sSelected & ">" & oRs("FirstName") & " " & oRs("LastName") & _
			": " & oRs("UserName") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	Response.Write("</select>" & vbCrLf)
	
	set oConn = nothing
	set oRs = nothing
	
end sub

' Name : DisplayUserStatusDropDown
' Purpose : displays the User Status drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayUserStatusDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM UserStatusTypes WHERE Display = 1 ORDER BY StatusID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""UserStatus"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
'		response.write("<option value="""">Please Select The User Status</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("StatusID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("StatusID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayUserTypeDropDown
' Purpose : displays the user Type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayUserTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM afxSecurityGroups ORDER BY GroupID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""GroupID"" onChange=""StatusChange()"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select The User Type</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("GroupID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("GroupID") & """ " & sSelected & ">" & rs("GroupName") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name: WriteBranchesDropdownJavascript 
' Purpose: Writes a javascript that allows a dropdown box to be changed with
'	the passed parameter - this can be used with a Companies dropdown box to
'	allow a dynamic Branches dropdown box that changes according to the
'	selected company. 
' Inputs: p_sFormName
' Outputs: Writes javascript to the file
sub WriteBranchesDropdownJavascript(p_sFormName)

	dim oConn
	dim oRs
	dim sSql
	
	if p_sFormName = "" then
		exit sub
	end if

	set oConn = new HandyADO
	
	sSql = "SELECT * FROM CompanyBranches ORDER BY CompanyID ASC"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if not (oRs.EOF and oRs.BOF) then

		Response.Write("<script langauge=""JavaScript"">" & vbCrLf)
		Response.Write("function UpdateBranchList(company) {" & vbCrLf)

		dim iCompanyId
		dim bFirstCompany
		dim iOptionNum
		bFirstCompany = true
		iOptionNum = 1
				
			do while not oRs.EOF
			
				if oRs("CompanyID") <> iCompanyId then
					iCompanyId = oRs("CompanyID")
					
					if not bFirstCompany then
						Response.Write("	}" & vbCrLf)
					end if 
					bFirstCompany = false
					
					Response.Write("	if (company == " & iCompanyId & ") {" & vbCrLf)
					Response.Write("		document." & p_sFormName & ".Branch.length = 0;" & vbCrLf)
					
					iOptionNum = 0
									
				end if
				
				Response.Write("		document." & p_sFormName & ".Branch.options[" & iOptionNum & "] = new Option('" & oRs("Name") & "','" & oRs("BranchID") & "');" & vbCrLf)
			
				iOptionNum = iOptionNum + 1
				oRs.MoveNext
			
			loop
			
		Response.Write("	} else {" & vbCrLf)
		Response.Write("		document." & p_sFormName & ".Branch.length = 0;" & vbCrLf)
		Response.Write("	}" & vbCrLf)
		Response.Write("}" & vbCrLf)
		Response.Write("</script>" & vbCrLf)
		
	end if

end sub


' Name: DisplayCouponTypesDropDown
' Purpose: Displays a dropdown box with %/$ coupon type options.  Exciting.
' Inputs: p_iDisplayType - 0 for search, 1 for assignment.
' Outputs: Prints the aforementioned dropdown box.
sub DisplayCouponTypesDropDown(p_sSelect, p_iDisplayType)

	Response.Write("<select name=""CouponType"">" & vbCrLf)
	
	if p_iDisplayType = 0 then
		Response.Write("<option value="""">ALL</option>" & vbCrLf)
	end if
	
	dim sSelected
	
	if p_sSelect = "$" then sSelected = "SELECTED" else sSelected = ""
	Response.Write("<option value=""$""" & sSelected & ">$</option>" & vbCrLf)
	if p_sSelect = "%" then sSelected = "SELECTED" else sSelected = ""
	Response.Write("<option value=""%""" & sSelected & ">%</option>" & vbCrLf)
	
	Response.Write("</select>" & vbCrLf)		

end sub


' Name : DisplayPLPTypesDropDown
' Purpose : displays a drop down box filled with all existing PLPsTypes names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayPLPTypesDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT * FROM PLPTypes "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""PLPType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name: DisplayCandidateStatusDropdown(p_iSelectedValue)
' Purpose: Display the select dropdown box for candidate credit status types in the database.
' Inputs: p_iSelectedValue - value to preselect
'		  p_iDisplayType - 0 - default
'						   1 - display ALL option.
'						   2 - display "Select" first option
' Outputs: prints dropdown to screen
sub DisplayCreditStatusDropdown(p_iSelectedValue, p_iDisplayType)

	dim oConn
	dim oRs
	dim sSql
	dim sSelected
	
	set oConn = new HandyADO
	
	sSql = "SELECT CreditStatus, CreditStatusID FROM CreditStatusTypes"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	Response.Write("<select name=""CreditStatus"">" & vbCrLf)
	
	if cint(p_iDisplayType) = 1 then
		Response.Write("<option value="""">ALL</option>")
	elseif cint(p_iDisplayType) = 2 then
		Response.Write("<option value="""">Select a Credit Status</option>")
	end if
	
	do while not oRs.EOF
	
		if trim(p_iSelectedValue) = trim(oRs("CreditStatusID")) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		Response.Write("<option value=""" & oRs("CreditStatusID") & """ " & sSelected & ">" & oRs("CreditStatus") & "</option>" & vbCrLf)
		
		oRs.MoveNext
		
	loop

	Response.Write("</select>" & vbCrLf)
	
	set oRs = nothing
	set oConn = nothing

end sub

' DisplayVerificationMethodDropDown(p_iSelectedValue,p_iDisplayType)
' Name : DisplayVerificationMethodDropDown
' Purpose : displays the verification method drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_sUserType - the type of user the drop down should be filtered for, also used in the drop down name
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayVerificationMethodDropDown(p_iSelectedValue,p_sUserType,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM VerificationMethods " & _
			"WHERE UserType = 'All' or UserType = '" & p_sUserType & "' " & _
			"ORDER BY ID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""" & p_sUserType & "UserVerificationID"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Verification Method</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ID") & """ " & sSelected & ">" & rs("method") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : ValueInDelString
' Purpose : determines if a value is contained in a string of comma delimited elements
' Inputs : p_sValue - the value to check for
'		 : p_sElementString - string of elements delimited by commas
' Outputs : true if the value was in the string, false if not
function ValueInDelString(p_sValue,p_sElementString)
	dim sCurrentValue
	dim aElementArray
	
	aElementArray = split(p_sElementString, ", ")
	
	for each sCurrentValue in aElementArray
		if (trim(sCurrentValue) = trim(p_sValue)) then
			ValueInDelString = true
			exit function
		end if
	next
	
	ValueInDelString = false
end function
%>

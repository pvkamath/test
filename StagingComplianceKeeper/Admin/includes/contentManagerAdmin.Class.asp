<%
'===============================================================================================================
' Class: Content Manager 
' Description: Content Manager Class
' Create Date: 2/15/02
' Current Version: 1.0
' Author(s): Joseph Lascola
' References to other DLL's: newsFx2
' ----------------------
' Properties:
'	- ConnectionString
'	- BodyText, HeadLine, ByLine, PostingDate
'	- WrittenDate, Source, Abstract, Status
'	- Keywords, AuthorID, ContentID, SiteID
'	- ErrorDescription, Message, ContentRS, ContentListRS, KeywordsRS, Connection, MediaRS
'	- MediaName, MediaPath, MediaID
'	- PageName
'
' Methods:
'	- addContent
'	- approveContent
'	- archiveContent
'	- checkEmpty (private)
'	- clearContent
'	- deleteContent
'	- deleteMedia
'	- deleteMediaDB
'	- getContent
'	- getContentList
'	- getMedia
'	- GetNavMenuItemChildren
'	- getKeywords
'	- hasMedia
'	- hasPending
'	- hasKeywordContent
'	- hasPendingKeywordContent
'	- InsertKeyword
'	- isGoodContentCheck (private)
'	- IsExistingKeyword
'	- modContent
'	- setConnection (private)
'   - updateMedia


'===============================================================================================================
Class ContentAdmin
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString
	dim g_sBodyText, g_sHeadLine, g_sByLine, g_sPostingDate
	dim g_sWrittenDate, g_sSource, g_sAbstract, g_sStatus
	dim g_sKeywords, g_iAuthorID, g_iContentID, g_iSiteID
	dim g_sErrorDescription
	dim g_bCheckBodyText
	dim g_bCheckKeywords
	dim g_bCheckPostingDate
	dim g_bCheckWrittenDate
	dim g_bCheckSource
	dim g_bCheckAbstract
	dim g_bCheckSiteID
	dim g_bCheckAuthorID
	dim g_oContentRS, g_oContentListRS
	dim g_oKeywordsRS
	dim g_oMediaRS
	dim g_oConnection
	dim g_sMessage
	dim g_sMediaName 
	dim g_sMediaPath
	dim g_iMediaID
	dim g_sPageName

	' PUBLIC METHODS ===========================================================================================
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					approveContent
	' Description:			Changes the status of the content from pending to approved
	'						also moves appropriate files.
	' Pre-conditions: 
	'	Required Properties: ConnectionString
	'						 
	' Inputs:				 ContentID - The content id to be approved
	' --------------------------------------------------------------------------------------------------------------------------------
	public function approveContent(p_iContentID)
	on error resume next
	dim oNewsAdmin
	dim iOldContentID
	dim rsContent
	dim bReturn
	
		set oNewsAdmin = server.CreateObject("NewsFXV2.Admin")
		
		oNewsAdmin.ConnectionString = g_sConnectionString
		
		'Check if the news item id was passed and is a number 
		if len(p_iContentID) = 0 or not isnumeric(p_iContentID) then
			g_sErrorDescription = "A Content Id must be passed to this method"
			set oNewsAdmin = Nothing
			set oNewsReport = Nothing
			exit function
		end if
	
		if GetContent(p_iContentID) then
		'Change Contentid status to Active
			oNewsAdmin.NewsItemId = p_iContentID
			
			g_oContentRS.Open
			
			oNewsAdmin.Headline = g_oContentRS.fields("Headline").Value
			oNewsAdmin.ByLine = g_oContentRS.fields("Byline").Value
			oNewsAdmin.PostingDate = g_oContentRS.fields("PostingDate").Value
			oNewsAdmin.WrittenDate = g_oContentRS.fields("WrittenDate").Value
			oNewsAdmin.Source = g_oContentRS.fields("Source").Value
			oNewsAdmin.Abstract = g_oContentRS.fields("Abstract").Value
			oNewsAdmin.Revision = ""'g_oContentRS.fields("Revision").Value
			oNewsAdmin.AuthorID = g_oContentRS.fields("AuthorID").Value
			oNewsAdmin.Status = "Active"
			
			oNewsAdmin.AdminUpdateNewsItem

			iOldContentID = g_oContentRS.fields("Revision").Value
			
			if len(iOldContentID) > 0 then
				bRevisionExist = true
			end if
		
			if bRevisionExist then
			
				g_oContentRS.Close
				
				if GetContent(iOldContentID) then
				'Change Old Content id status to Archived
					g_oContentRS.Open
			
					oNewsAdmin.NewsItemId = iOldContentID
					
					oNewsAdmin.Headline = g_oContentRS.fields("Headline").Value
					oNewsAdmin.ByLine = g_oContentRS.fields("Byline").Value
					oNewsAdmin.PostingDate = g_oContentRS.fields("PostingDate").Value
					oNewsAdmin.WrittenDate = g_oContentRS.fields("WrittenDate").Value
					oNewsAdmin.Source = g_oContentRS.fields("Source").Value
					oNewsAdmin.Abstract = g_oContentRS.fields("Abstract").Value
					oNewsAdmin.Revision = g_oContentRS.fields("Revision").Value
					oNewsAdmin.AuthorID = g_oContentRS.fields("AuthorID").Value
			
					oNewsAdmin.Status = "Archived"

					oNewsAdmin.AdminUpdateNewsItem
				end if
			end if
		end if
					
		set oNewsAdmin = Nothing
	end function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					addContent
	' Description:			Insert content with pending status 
	' Pre-conditions: 
	'	Required Properties:ConnectionString
	'						Properties set in contentManagerAdminConfig.asp
	' Inputs:				None
	' Outputs:				ContentID variable is set with the content id of the newly added item
	'	Returns:			True - If function ran successfully without errors
	'						False - If function did not run successfully without errors
	' --------------------------------------------------------------------------------------------------------------------------------
	public function addContent()
	on error resume next
		dim oNewsAdmin
		dim i
		dim aKeywordSplit
		dim bReturn

		bReturn = false
		
		set oNewsAdmin = Server.CreateObject("Newsfxv2.Admin")

		oNewsAdmin.ConnectionString = g_sConnectionString

		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			addContent = false
			exit function
		end if
		
		if isGoodContentCheck() then

			'SET VARIABLES
			oNewsAdmin.BodyText = g_sBodyText
			oNewsAdmin.HeadLine = g_sHeadline
			oNewsAdmin.ByLine = g_sByline
			oNewsAdmin.PostingDate = g_sPostingDate
			oNewsAdmin.WrittenDate = g_sWrittenDate
			oNewsAdmin.Source = g_sSource
			oNewsAdmin.Abstract = g_sAbstract
			oNewsAdmin.Status = "Pending"
			oNewsAdmin.SiteID = g_iSiteID
			oNewsAdmin.AuthorID = g_iAuthorID

			'ADD THE CONTENT
			oNewsAdmin.AdminAddNewsItem

			
			'ADD ABILITY FOR AN ARRAY OF KEYWORDS
			aKeywordSplit = split(g_sKeywords,",")
			for i = 0 to  ubound(aKeywordSplit)
				oNewsAdmin.keyword = aKeywordSplit(i)
				oNewsAdmin.AdminAddNewsKeywords
			next

			oNewsAdmin.AdminAddNewsAuthorMatch
			oNewsAdmin.AdminAddNewsBody

			g_iContentID = oNewsAdmin.NewsItemID

			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				addContent = false
				exit function
			else
				bReturn = true
			end if

		end if
		
		set oNewsAdmin = Nothing

		addContent = bReturn
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					updateBodyText
	' Description:			Updates the bodytext for a certain content id
	'
	' Pre-conditions: 
	'	Required Properties: ConnectionString
	'						 ContentID - The content id to be approved
	'						 g_sBodyText
	' Inputs:				 None
	' Output:				 True - Successful
	'						 False - Unsuccessful						
	' --------------------------------------------------------------------------------------------------------------------------------
	public function updateBodyText()
	on error resume next
	dim bReturn 'as boolean
	dim sSQL 'as string
	
		call setConnection()
	
		bReturn = false

		'Check that data exist for required fields
		sMessage = sMessage & checkEmpty(g_iContentID,"ContentID")
		sMessage = sMessage & checkEmpty(g_sBodyText,"BodyText")
	
		if len(sMessage) = 0 then
			sSQL = "UPDATE afxNewsBody " & _
					"SET bodytext = '" & replace(g_sBodyText,"'","''") & "' " & _
					"WHERE newsItemID = " & g_iContentID
	
			g_oConnection.Execute sSQL
		
			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				updateBodyText = false
				exit function
			end if
			
			bReturn = true
		else
			g_sMessage = sMessage
		end if		
	
		updateBodyText = bReturn
	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					modContent
	' Description:			Insert content with pending status and add the reference to the old content id(which is the newsfx newsitemid)
	' Pre-conditions: 
	'	Required Properties:ConnectionString
	'						Properties set in contentManagerAdminConfig.asp
	' Inputs:				p_iContentID - This is the content id of the item to be  
	' Outputs:				ContentID variable is set with the content id of the newly added item			
	'	Returns:			True - If function ran successfully without errors
	'						False - If function did not run successfully without errors
	' --------------------------------------------------------------------------------------------------------------------------------
	public function modContent(p_iContentID)
	on error resume next
		dim oNews
		dim i
		dim aKeywordSplit
		dim bReturn
		dim iParentContentID

		bReturn = false
		
		set oNews = Server.CreateObject("Newsfxv2.Admin")

		oNews.ConnectionString = g_sConnectionString

		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			modContent = false
			exit function
		end if

		'Check if the news item id was passed and is a number 
		if len(p_iContentID) = 0 or not isnumeric(p_iContentID) then
			g_sErrorDescription = "A Content Id must be passed to this method"
			set oNews = Nothing
			exit function
		end if
		
		if isGoodContentCheck() then
		
			'check to see that the current content does not have any other revisions		
			if getContent(p_iContentID) then
				g_oContentRS.Open
				
				iParentContentID = trim(g_oContentRS.fields("Revision").Value)
			else
			'give an error if there is no record for the passed content id	
				g_sErrorDescription = "The content id must be in the database"
			end if
					
		
			'SET VARIABLES
			oNews.BodyText = g_sBodyText
			oNews.HeadLine = g_sHeadline
			oNews.ByLine = g_sByline
			oNews.PostingDate = g_sPostingDate
			oNews.WrittenDate = g_sWrittenDate
			oNews.Source = g_sSource
			oNews.Abstract = g_sAbstract
			oNews.Status = "Pending"
			oNews.SiteID = g_iSiteID
			
			oNews.AuthorID = g_iAuthorID
			
			'The purpose of this statement is to allow only one edit on an article
			if len(iParentContentID) > 0 then
			'if the revision field is not an integer then we must put update this news article
				oNews.NewsItemID = p_iContentID
				oNews.Revision = iParentContentID
				'MODIFY THE CONTENT
				oNews.AdminUpdateNewsItem		
				oNews.AdminUpdateNewsBody
			
				g_iContentID = oNews.NewsItemID
			
			else
			'if the revision field is empty then add a new article with a reference to the passed contentid
				oNews.Revision = p_iContentID
				
				'ADD THE CONTENT
				oNews.AdminAddNewsItem
			
				'ADD ABILITY FOR AN ARRAY OF KEYWORDS
				aKeywordSplit = split(g_sKeywords,",")
				for i = 0 to  ubound(aKeywordSplit)
					oNews.keyword = aKeywordSplit(i)
					oNews.AdminAddNewsKeywords
				next
		
				oNews.AdminAddNewsAuthorMatch
				'oNews.AdminUpdateNewsBody
				oNews.AdminAddNewsBody
				Response.Write("ssql " & oNews.BodyText & "<br>")
				Response.Write("item " & oNews.NewsItemID & "<br>")
				
				
				g_iContentID = oNews.NewsItemID
			end if


			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				modContent = false
				exit function
			else
				bReturn = true
			end if

		end if
		
		set oNews = Nothing

		modContent = bReturn
	end function

	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContent
	' Description:		return a recordset for the contentid passed
	'					This function uses the contentReport Class to retrieve the content
	' Inputs:			p_iContentID 
	' Output:			none
	'	Returns:			True - If function ran successfully without errors
	'						False - If function did not run successfully without errors
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContent(p_iContentId)
	dim bReturn
	dim oContentReport
	
		bReturn = false

		set oContentReport = new ContentReport 'found in contentReport.Class.asp included at the top of the page

		if len(g_sConnectionString) > 0 then

			oContentReport.ConnectionString = g_sConnectionString
		
			if oContentReport.GetContent(p_iContentid) then
				set g_oContentRS = oContentReport.ContentRS

				bReturn = true					
			else
				g_sMessage = oContentReport.Message
				g_sErrorDescription = oContentReport.ErrorDescription
			end if
		else
			g_sErrorDescription = "No Datasource was supplied"
		end if		
		set oContentReport = Nothing

		getContent = bReturn
	end function
	


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContentList
	' Description:		return the record set for the contentid passed
	' Inputs:			p_iOrderBy - is the number to order the query by
	'					p_sStatus - is the status to pull back ("Archived","Active","Pending")
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContentList(p_sStatus,p_iOrderBy)
	dim bReturn
	dim oContentReport
	
		bReturn = false

		set oContentReport = new ContentReport 'found in contentReport.Class.asp included at the top of the page

		if len(g_sConnectionString) > 0 then

			oContentReport.ConnectionString = g_sConnectionString
			oContentReport.Keywords = g_sKeywords

			call clearContent() 'subroutine found below
			
			if oContentReport.getContentList(p_sStatus,p_iOrderBy) then
				set g_oContentListRS = oContentReport.ContentListRS
		
				bReturn = true					
			else
				g_sMessage = oContentReport.Message
				g_sErrorDescription = oContentReport.ErrorDescription
			end if
		else
			g_sErrorDescription = "Not Datasource was supplied"
		end if		
		set oContentReport = Nothing

		getContentList = bReturn
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: isMultipleKeyword
	' Description:		Checks if the passed keyword allows multiple content
	' Pre-conditions: 
	' Inputs:			p_sKeyword - This is the keyword that we want to check for content.
	' Outputs:			True - If keyword allows multiple
	'					False - If keyword does not allow multiple
	' --------------------------------------------------------------------------------------------------------------------------------
	public function isMultipleKeyword(p_sKeyword)
	on error resume next
	dim bReturn
	dim rsCheck
	dim sSQL
		
		bReturn = false
		
		call setConnection()
	
		set rsCheck = Server.CreateObject("ADODB.Recordset")
		set rsCheck.ActiveConnection = g_oConnection
		
		sSQL = "SELECT keywordtype " & _
				"FROM afxKeywords " & _
				"WHERE keyword = '" & p_sKeyword & "'"	

		rsCheck.Open sSQL
		

		if not rsCheck.EOF then

			if trim(rsCheck("keywordtype")) = "M" then
				bReturn = true
			end if			
			rsCheck.Close
			
		end if

		isMultipleKeyword = bReturn
		
		set rsCheck = Nothing

	end function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: hasKeywordContent
	' Description:		Checks if the passed keyword has content nonarchived content.
	' Pre-conditions: 
	' Inputs:			p_sKeyword - This is the keyword that we want to check for content.
	' Outputs:			True - If the keyword has nonarchived content
	'					False - If the keyword has no nonarchived content
	' --------------------------------------------------------------------------------------------------------------------------------
	public function hasKeywordContent(p_sKeyword)
	'on error resume next
	dim bReturn
	dim rsCheck
		
		bReturn = false
		
		call setConnection()
	
		set rsCheck = Server.CreateObject("ADODB.Recordset")
		set rsCheck.ActiveConnection = g_oConnection

		sSQL = "SELECT ni.newsItemID , k.keywordtype " & _
				"FROM afxKeywords k INNER JOIN " & _
				"afxNewsKeywordsX nkx ON (k.keyword = '" & p_sKeyword & "' AND nkx.KeywordID = k.KeywordID) INNER JOIN " & _
				"afxNewsItems ni ON (nkx.NewsItemID = ni.NewsItemID AND ni.status <> 'Archived')"

		rsCheck.Open sSQL
		
		if not rsCheck.EOF then
			
			if rsCheck("keywordtype") <> "M" then
				bReturn = true
			end if
			
			rsCheck.Close
		end if
		
		hasKeywordContent = bReturn

		set rsCheck = Nothing
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: countPendingContent
	' Description:		Checks if the passed keyword has a pending article
	' Pre-conditions: 
	' Inputs:			p_sKeyword - Keyword to do this check on
	' Outputs:			Returns the number of pending content for the passed keyword
	'					Returns zero if none are found
	' --------------------------------------------------------------------------------------------------------------------------------
	public function countPendingContent(p_sKeyword)
	on error resume next
	dim sSQL
	dim rsPendingCheck
	dim iReturn
		
		call setConnection()
	
		set rsPendingCheck = Server.CreateObject("ADODB.Recordset")
		set rsPendingCheck.ActiveConnection = g_oConnection
	
		iReturn = 0
	
		sSQL = "SELECT count(ni.newsItemID) as contentCount " & _
				"FROM afxKeywords k INNER JOIN " & _
				"afxNewsKeywordsX nkx ON (k.keyword = '" & p_sKeyword & "' AND nkx.KeywordID = k.KeywordID) INNER JOIN " & _
				"afxNewsItems ni ON (nkx.NewsItemID = ni.NewsItemID AND ni.status = 'Pending')"

		rsPendingCheck.Open sSQL
		
		if not rsPendingCheck.EOF then
			iReturn = rsPendingCheck("contentCount")
		end if
	
		rsPendingCheck.Close
		set rsPendingCheck = Nothing

		countPendingContent = iReturn
		
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: hasPendingKeywordContent
	' Description:		Checks if the passed keyword has a pending article
	' Pre-conditions: 
	' Inputs:			p_sKeyword - 
	' Outputs:			True - If pending content exist
	'					False - If pending content does not exist
	'					
	' --------------------------------------------------------------------------------------------------------------------------------
	public function hasPendingKeywordContent(p_sKeyword)
	on error resume next
	dim sSQL
	dim rsPendingCheck
	dim bReturn
		
		call setConnection()
	
		set rsPendingCheck = Server.CreateObject("ADODB.Recordset")
		set rsPendingCheck.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = "SELECT ni.newsItemID , k.keywordtype " & _
				"FROM afxKeywords k INNER JOIN " & _
				"afxNewsKeywordsX nkx ON (k.keyword = '" & p_sKeyword & "' AND nkx.KeywordID = k.KeywordID) INNER JOIN " & _
				"afxNewsItems ni ON (nkx.NewsItemID = ni.NewsItemID AND ni.status = 'Pending')"

		rsPendingCheck.Open sSQL
		
		if not rsPendingCheck.EOF then
			bReturn = true
		end if
		
		hasPendingKeywordContent = bReturn
	
		rsPendingCheck.Close
		set rsPendingCheck = Nothing
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: hasPending
	' Description:		Checks if the passed Contentid has a pending article
	' Pre-conditions: 
	' Inputs:			p_iContentId - Function checks if this content id has a pending article
	' Outputs:			True - If pending content exist
	'					False - If pending content does not exist
	'					Sets Contentid if pending exist with the pending contents id
	' --------------------------------------------------------------------------------------------------------------------------------
	public function hasPending(p_iContentID)
	on error resume next
	dim sSQL
	dim rsPendingCheck
	dim bReturn
		
		call setConnection()
	
		set rsPendingCheck = Server.CreateObject("ADODB.Recordset")
		set rsPendingCheck.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = "SELECT newsItemID " &_
				"FROM afxNewsItems " &_
				"WHERE revision = " & p_iContentID & " " &_
				"AND status = 'Pending'"

		rsPendingCheck.Open sSQL
		
		if not rsPendingCheck.EOF then
			g_iContentID = rsPendingCheck.Fields("newsItemID").Value

			bReturn = true
		end if
		
		hasPending = bReturn
	
		rsPendingCheck.Close
		set rsPendingCheck = Nothing
	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getKeywords
	' Description:		sets a recordset with the keywords
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getKeywords()
	'on error resume next
	dim sSQL
	dim rsKeywords
	dim bReturn
		
		call setConnection()
	
		set rsKeywords = Server.CreateObject("ADODB.Recordset")
		set rsKeywords.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = "SELECT keyword, KeywordType " &_
				"FROM afxKeywords" 

		rsKeywords.Open sSQL,,3,3
		
		if not rsKeywords.EOF then
			set g_oKeywordsRS = rsKeywords

			bReturn = true
		end if
		
		getKeywords = bReturn
		
		rsKeywords.Close
		set rsKeywords = Nothing
	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:			updateMedia	
	' Description:	Associates some sort of media with a content id
	' Required:		ConnectionString
	'				ContentID
	'				MediaName
	'				MediaPath
	' Inputs:		p_sMediaType - a media type for the afxNewsMedia table (I for image is the default)
	' Output:		True 
	'				False	
	' --------------------------------------------------------------------------------------------------------------------------------
	public function updateMedia(p_sMediaType)
	on error resume next
	dim bReturn
	dim sMessage
	dim sObjectReturn
	
		bReturn = false

		set oNewsAdmin = Server.CreateObject("Newsfxv2.Admin")
		
		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			updateMedia = false
			set oNewsAdmin = Nothing
			exit function
		end if

		oNewsAdmin.ConnectionString = g_sConnectionString	

		if len(p_sMediaType) = 0 then
			p_sMediaType = "I"
		end if

		'Check that data exist for required fields
		sMessage = sMessage & checkEmpty(g_iContentID,"ContentID")
		sMessage = sMessage & checkEmpty(g_sMediaName,"MediaName")
		sMessage = sMessage & checkEmpty(g_sMediaPath,"MediaPath")			
		
		if len(sMessage) > 0 then
			g_sMessage = sMessage
			updateMedia = false
			set oNewsAdmin = Nothing
			exit function	
		end if

		'Set required fields for news object
		oNewsAdmin.NewsItemID = g_iContentID
		oNewsAdmin.MediaType = p_sMediaType
		oNewsAdmin.MediaPath = g_sMediaPath
		oNewsAdmin.MediaName = g_sMediaName
		
		sObjectReturn = oNewsAdmin.AdminAddNewsMedia()

		'check for errors from newsFX
		if trim(sObjectReturn) = "No Errors" then
			bReturn = true
		else
			g_sErrorDescription = sObjectReturn
			set oNewsAdmin = Nothing
			exit function
		end if
		
		'error handling
		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			updateMedia = false
			set oNewsAdmin = Nothing
			exit function
		end if
				
		set oNewsAdmin = Nothing
		updateMedia = bReturn
	end function
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:			deleteContent
	' Description:	Delete the content id that is passed to the function
	' Required Properties:ConnectionString
	' Inputs:		p_iContentID - this is the content id that will be deleted
	' Output:		deleteContent returns 
	'					True - If the content is deleted with no errors
	'					False - If the content is not deleted without errors	
	' --------------------------------------------------------------------------------------------------------------------------------
	public function deleteContent(p_iContentID)
	on error resume next
	dim bReturn 'as boolean
	dim oNewsAdmin 'as object
	dim sObjectReturn 'as string
	dim bDeleteMedia 'as boolean
	
		bReturn = false

		set oNewsAdmin = Server.CreateObject("Newsfxv2.Admin")		
		
		oNewsAdmin.ConnectionString = g_sConnectionString
		
		'error checking
		if err.Number <> 0 then
			g_sErrorDescription = err.Description
			exit function
		end if

		if isNumeric(cint(p_iContentID)) then

			g_iContentID = p_iContentID

			bDeleteMedia = deleteMedia(true)
		
			oNewsAdmin.NewsItemId = p_iContentID

			'error checking
			if err.Number <> 0 then
				g_sErrorDescription = err.Description
				set oNewsAdmin = Nothing
				exit function
			end if
			
			sObjectReturn = oNewsAdmin.AdminDeleteNewsItem
		
			'error checking
			if trim(sObjectReturn) = "No Errors" then
				bReturn = True
			else
				g_sErrorDescription = sObjectReturn
			end if
		end if
	
		deleteContent = bReturn
		
		set oNewsAdmin = Nothing		
	end function


	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:			hasMedia	
	' Description:	Check if there is media for the current contentid
	' Required:		ConnectionString
	'				g_iContentID
	' Inputs:			
	' Output:		True - If media exist
	'				False - If there is no media
	'				g_iMediaID - Is set if media is found	
	' --------------------------------------------------------------------------------------------------------------------------------
	private function hasMedia()
	dim rsMedia
	dim sSQL
	dim bReturn 
	
		bReturn = false
	
		call setConnection()
	
		set rsMedia = Server.CreateObject("ADODB.Recordset")
		set rsMedia.ActiveConnection = g_oConnection
		
		sSQL = "SELECT mediaID " & _
				"FROM afxNewsMedia " & _
				"WHERE newsItemID = " & g_iContentID

		rsMedia.Open sSQL	
	
		if not rsMedia.EOF then
			g_iMediaID = rsMedia.Fields("mediaID").Value
			bReturn = true
		end if

		hasMedia = bReturn
		
		set rsMedia = Nothing

	end function


	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getMedia()
	' Description:		This function returns a recordset of all the media for the content id
	' Pre-conditions:	g_iContentID is a Required field
	'					ConnectionString
	' Inputs:			None
	' Outputs:			MediaRS recordset
	'					True - If successful
	'					False - If not succesful
	' Returns:		
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getMedia()
	dim bReturn
	dim oContentReport
	dim tmpContentID
		bReturn = false

		set oContentReport = new ContentReport 'found in contentReport.Class.asp included at the top of the page

		oContentReport.ConnectionString = g_sConnectionString
		oContentReport.ContentID = g_iContentID
		tmpContentID = g_iContentID

		call clearContent() 'subroutine found below
		g_iContentID = tmpContentID	
		bReturn = oContentReport.getMedia()

		if bReturn then
			set g_oMediaRS = oContentReport.MediaRS
		else
			g_sMessage = oContentReport.Message
			g_sErrorDescription = oContentReport.ErrorDescription
		end if

		set oContentReport = Nothing

		getMedia = bReturn

	end function
		
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				deleteMedia()
	' Description:		This function deletes all the media for a given content id from the server
	' Pre-conditions:	g_iContentID is a Required field
	'					ConnectionString
	' Inputs:			p_bDeleteMediaFiles- true to delete the files off the server. false to leave the files.
	' Outputs:			true - succesful
	'					false - unsuccessful
	' Returns:		
	' --------------------------------------------------------------------------------------------------------------------------------
	public function deleteMedia(p_bDeleteMediaFiles)
	'on error resume next
	dim bMedia 'as boolean
	dim sSQL 'as string
	dim oFileSystem 'as object
	dim sImageFullPath 'as string
	dim sImagePath 'as string
	dim sImageName 'as string
	dim bDeleteMediaDB 'as boolean
	dim bReturn 'as boolean


		bReturn = false
		
		if not p_bDeleteMediaFiles and len(p_bDeleteMediaFiles) > 0 then
			p_bDeleteMediaFiles = false		
		end if

		call setConnection()

		Set oFileSystem = CreateObject("Scripting.FileSystemObject")

		bMedia = getMedia()


		if bMedia  then
			bDeleteMediaDB =  deleteMediaDB()
			' need to check if media file should be deleted
			' a media file should not be deleted if this is a modification of content
			' because this should be checked against the database after content is modified
			if p_bDeleteMediaFiles then

				do while not g_oMediaRS.EOF
					sImagePath = g_oMediaRS.Fields("MediaPath").Value
					sImageName = g_oMediaRS.Fields("MediaName").Value				
					
					sImageFullPath = server.MapPath(sImagePath & sImageName)


					if oFileSystem.FileExists(sImageFullPath) then
						oFileSystem.DeleteFile(sImageFullPath)
					end if
			
					g_oMediaRS.Movenext
				loop	
			end if
			
			bReturn = True
		end if

		set oFileSystem = Nothing
		
		deleteMedia = bReturn
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				deleteMediaDB()
	' Description:		Deletes the media from the database.
	' Pre-conditions:	g_oConnection be set
	'					g_iContentID
	' Inputs:			nothing
	' Outputs:			nothing
	' Returns:			True 
	'					False
	' --------------------------------------------------------------------------------------------------------------------------------
	public function deleteMediaDB()	
	'on error resume next
	dim sSQL 'as string
	dim bReturn
	
		bReturn = false		

		'check the content id 
		if len(g_iContentID) > 0 then	

			'Delete the all media for the content id
			sSQL = "DELETE FROM afxNewsMedia WHERE newsItemID = " & g_iContentID
			g_oConnection.Execute sSQL
			
			if err.number > 0 then
				g_sErrorDescription = err.Description
				deleteMediaDB = false							
				exit function
			end if
			
			bReturn = true
		else
		'return an message 
			g_sMessage = "Content ID must be set"
		end if

		deleteMediaDB = bReturn
	end function
		
	
	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				clearContent
	' Description:		Clears all properties except the connetionstring
		' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub clearContent()
		g_sBodyText = "" 
		g_sHeadLine = "" 
		g_sByLine = "" 
		g_sPostingDate = ""
		g_sWrittenDate = "" 
		g_sSource = "" 
		g_sAbstract = "" 
		g_sStatus = ""
		g_sKeywords = "" 
		g_iAuthorID = "" 
		g_iContentID = "" 
		g_iSiteID = ""
		g_sErrorDescription = ""
		g_sMessage = ""

		if isObject(g_oContentRS) then
			g_oContentRS.Close
			set g_oContentRS = nothing
		end if
		if isObject(g_oKeywordsRS) then
			g_oKeywordsRS.Close
			set g_oKeywordsRS = nothing
		end if
	
	end sub

	
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				isGoodContentCheck
	' Description:		This function checks if the content required is supplied 
	' Inputs:			none
	' Output:			True	-	If the context exist
	'					False	-	If the required content does not exist
	' Error Handling :	
	' --------------------------------------------------------------------------------------------------------------------------------
	private function isGoodContentCheck()
	dim sErrorDescription
	dim bReturn	
		
		bReturn = false 'Initialize return variable
	
		'THIS SECTION SET THE ERROR DESCRIPTION IF ANY OF THESE REQUIRES FIELDS ARE EMPTY
		if g_bCheckBodyText then
			sErrorDescription = sErrorDescription & checkEmpty(g_sBodyText,"Body Text")
		end if
		if g_bCheckKeywords then
			sErrorDescription = sErrorDescription & checkEmpty(g_sKeywords,"Keyword(s)")
		end if
		if g_bCheckPostingDate then
			sErrorDescription = sErrorDescription & checkEmpty(g_sPostingDate,"Post Date")
		end if
		if g_bCheckWrittenDate then
			sErrorDescription = sErrorDescription & checkEmpty(g_sWrittenDate,"Written Date")
		end if
		if g_bCheckSource then
			sErrorDescription = sErrorDescription & checkEmpty(g_sSource,"Source")
		end if
		if g_bCheckAbstract then
			sErrorDescription = sErrorDescription & checkEmpty(g_sAbstract,"Abstract")
		end if
		if g_bCheckSiteID then
			sErrorDescription = sErrorDescription & checkEmpty(g_iSiteID,"Site ID")
		end if
		if g_bCheckAuthorID then
			sErrorDescription = sErrorDescription & checkEmpty(g_iAuthorID,"Author ID")
		end if		
		
		if len(sErrorDescription) = 0 then
			bReturn = True
		else
			g_sErrorDescription = sErrorDescription
		end if

		isGoodContentCheck = bReturn

	end function
	

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name: checkEmpty
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	' --------------------------------------------------------------------------------------------------------------------------------
	private function checkEmpty(p_sItem,p_sItemDescription)
	dim sReturn
		sReturn = ""
		
		if len(p_sItem) = 0 or p_sItem = "" then
			sReturn = " " & p_sItemDescription & " cannot be empty"
		end if
		
		checkEmpty = sReturn
	
	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				setConnection
	' Description:		Set The connection if one does not exist
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub setConnection()
		if not isObject(g_oConnection) then
			set g_oConnection = server.CreateObject("ADODB.Connection")
			if len(g_sConnectionString) = 0 then
				g_sErrorDescription = "No Connection String was supplied"
			else
				g_oConnection.ConnectionString = g_sConnectionString
			end if
			g_oConnection.Open
		end if
	end sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					archiveContent
	' Description:			Change a content item to archived.
	'						Remove reference to that content from any pending article
	'						
	' Pre-conditions: 
	'	Required Properties: ConnectionString
	'						 
	' Inputs:				 ContentID - The content id to be archived
	' --------------------------------------------------------------------------------------------------------------------------------
	public function archiveContent(p_iContentID)
	on error resume next
	dim oNewsAdmin
	dim bReturn
	dim sSQL
	dim oDBConn 'as object
	
		bReturn = false
	
		set oNewsAdmin = server.CreateObject("NewsFXV2.Admin")
		set oDBConn = server.CreateObject("ADODB.Connection")
		
		oDBConn.ConnectionString = g_sConnectionString
		
		oNewsAdmin.ConnectionString = g_sConnectionString
		
		'Check if the news item id was passed and is a number 
		if len(p_iContentID) = 0 or not isnumeric(p_iContentID) then
			g_sErrorDescription = "A Content Id must be passed to this method"
			set oNewsAdmin = Nothing
			set oNewsReport = Nothing
			exit function
		end if
	
		if GetContent(p_iContentID) then
		'Change Contentid status to Active
			oNewsAdmin.NewsItemId = p_iContentID
			
			g_oContentRS.Open
			
			oNewsAdmin.Headline = g_oContentRS.fields("Headline").Value
			oNewsAdmin.ByLine = g_oContentRS.fields("Byline").Value
			oNewsAdmin.PostingDate = g_oContentRS.fields("PostingDate").Value
			oNewsAdmin.WrittenDate = g_oContentRS.fields("WrittenDate").Value
			oNewsAdmin.Source = g_oContentRS.fields("Source").Value
			oNewsAdmin.Abstract = g_oContentRS.fields("Abstract").Value
			oNewsAdmin.Revision = ""
			oNewsAdmin.AuthorID = g_oContentRS.fields("AuthorID").Value
			oNewsAdmin.Status = "Archived"
			
			oNewsAdmin.AdminUpdateNewsItem
		
			'remove the passed content id as a active article for anypending articles
			sSQL = "UPDATE afxNewsItems SET revision = '' WHERE newsItemID = " & p_iContentID
			oDBConn.Execute sSQL

			if err.number = 0 then 
				bReturn = true
			end if
		end if
		
		archiveContent = bReturn
		
		set oDBConn = Nothing			
		set oNewsAdmin = Nothing
	end function
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				IsExistingKeyword
	' Description:		returns true if keyword already exists; false if not.
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function IsExistingKeyword(p_sKeyword)

		dim sSQL
		dim rsKeywords
		dim bReturn
		
		call setConnection()
	
		set rsKeywords = Server.CreateObject("ADODB.Recordset")
		set rsKeywords.ActiveConnection = g_oConnection
	
		bReturn = false
	
		sSQL = " SELECT * " &_
			   " FROM afxKeywords " &_
			   " WHERE keyword = '" & p_sKeyword & "'"

		rsKeywords.Open sSQL,,3,3
		
		if not rsKeywords.EOF then
			bReturn = true
		end if
		
		rsKeywords.Close
		set rsKeywords = Nothing
		
		IsExistingKeyword = bReturn
		
	end function
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				GetNavMenuItemChildren
	' Description:		Returns recordset of navmenuitems that are children of a navMenu
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function GetNavMenuItemChildren(p_nNavMenu)
		'on error resume next
		dim sSQL, rs
		
		call setConnection()

		set rs = Server.CreateObject("ADODB.Recordset")
		set rs.ActiveConnection = g_oConnection
	
		'insert into afxKeywords table
		sSQL = " SELECT * FROM navMenuItem WHERE navMenu = " & p_nNavMenu
'		response.write sSQL
		rs.Open sSQL
		
		set GetNavMenuItemChildren = rs
		
	end function 
		
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				InsertKeyword
	' Description:		Inserts keyword into afxKeywords, afxKeywordsMenuX, navMenu (if needed), navMenuItem
	' Inputs:			
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function InsertKeyword(p_sKeyword, p_sText, p_sImage, p_nNavMenuItem)
		'on error resume next
		dim sSQL, rs, nKeywordID, nNavMenuPopout, nNavMenu, nNavMenuItem
		
		call setConnection()

		set rs = Server.CreateObject("ADODB.Recordset")
		set rs.ActiveConnection = g_oConnection
	
		'insert into afxKeywords table
		sSQL = " INSERT INTO afxKeywords " &_
			   " (Keyword, KeywordType) " &_
			   " VALUES ('" & ScrubForSQL(p_sKeyword) & "', 'M')"
		rs.Open sSQL
		
		'get keywordID
		sSQL = " SELECT MAX(keywordID) AS keywordID FROM afxKeywords "
		rs.Open sSQL,,3,3
		nKeywordID = rs("keywordID")
		rs.Close
		
		'p_nNavMenuItem is the parent menu of this new keyword
		
		'if p_nNavMenu <> 0 check to see whether navMenu exists
		if p_nNavMenuItem <> 0 then		'its a submenu of something other than the root
			sSQL = " SELECT navMenuPopout, navMenu FROM navMenuItem WHERE id = " & p_nNavMenuItem
			rs.Open sSQL,,3,3
			nNavMenuPopout = rs("navMenuPopout")
			nNavMenu = rs("navMenu")

			rs.Close
			if nNavMenuPopout = 0 then

				'need to create navMenu
'				sSQL = " INSERT INTO navMenu (name, parent, menuOrder) " &_
'					   " VALUES ('" & ScrubForSQL(p_sKeyword) & "', " & nNavMenu & ", '(SELECT (COUNT(*) + 1) FROM navMenu WHERE parent = " & nNavMenu & ")')"
				sSQL = " ap_addNavMenu " & p_nNavMenuItem & ", " & nNavMenu 
				rs.Open sSQL

				sSQL = " SELECT MAX(id) AS id FROM navMenu "
				rs.Open sSQL,,3,3
				nNavMenuPopout = rs("id")
				rs.Close
				
				'update navmenuitem with popout menu
				sSQL = " UPDATE navMenuItem SET navMenuPopout = " & nNavMenuPopout & " WHERE id = " & p_nNavMenuItem
				rs.Open sSQL
			end if
		else
			nNavMenuPopout = 1
		end if
		
		'insert into navmenuitem table
		sSQL = " ap_addNavMenuItem '" & ScrubForSQL(p_sKeyword) & "', '" & ScrubForSQL(p_sText) & "', '" & ScrubForSQL(p_sImage) & "', " & nNavMenuPopout
		rs.Open sSQL
		
		'grab id
		nNavMenuItem = rs("id")
		rs.Close
		
		'insert into afxKeywordsMenuX
		sSQL = " INSERT INTO afxKeywordsMenuX (KeywordID, MenuSectionID) " &_
			   " VALUES (" & nKeywordID & ", " & nNavMenuItem & ")"
		rs.Open sSQL

		set rs = Nothing
	end function	
	
	
	' PRIVATE METHODS ==========================================================================================
	Private Sub Class_Initialize()
		' Enter code you want to run on Object Startup here.
		%>
		<!--#include virtual="/admin/includes/ContentManagerAdminConfig.asp"-->
		<%
	End Sub

	Private Sub Class_Terminate()
		' Enter code you want to run on Object Termination here.
	End Sub

	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined
	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	Public Property Let BodyText(p_sBodyText)
		g_sBodyText = p_sBodyText
	End Property

	Public Property Get BodyText()
		BodyText = g_sBodyText
	End Property

	Public Property Let HeadLine(p_sHeadLine)
		g_sHeadLine = p_sHeadLine
	End Property

	Public Property Get HeadLine()
		HeadLine = g_sHeadLine
	End Property

	Public Property Let ByLine(p_sByLine)
		g_sByLine = p_sByLine
	End Property

	Public Property Get ByLine()
		ByLine = g_sByLine
	End Property

	Public Property Let PostingDate(p_sPostingDate)
		g_sPostingDate = p_sPostingDate
	End Property

	Public Property Get PostingDate()
		PostingDate = g_sPostingDate
	End Property

	Public Property Let WrittenDate(p_sWrittenDate)
		g_sWrittenDate = p_sWrittenDate
	End Property

	Public Property Get WrittenDate()
		WrittenDate = g_sWrittenDate
	End Property

	Public Property Let Source(p_sSource)
		g_sSource = p_sSource
	End Property

	Public Property Get Source()
		Source = g_sSource
	End Property

	Public Property Let Abstract(p_sAbstract)
		g_sAbstract = p_sAbstract
	End Property

	Public Property Get Abstract()
		Abstract = g_sAbstract
	End Property

	Public Property Let Status(p_sStatus)
		g_sStatus = p_sStatus
	End Property

	Public Property Get Status()
		Status = g_sStatus
	End Property

	Public Property Let Keywords(p_sKeywords)
		g_sKeywords = p_sKeywords
	End Property

	Public Property Get Keywords()
		Keywords = g_sKeywords
	End Property

	Public Property Let AuthorID(p_iAuthorID)
		g_iAuthorID = p_iAuthorID
	End Property

	Public Property Get AuthorID()
		AuthorID = g_iAuthorID
	End Property

	Public Property Let ContentID(p_iContentID)
		g_iContentID = p_iContentID
	End Property

	Public Property Get ContentID()
		ContentID = g_iContentID
	End Property

	Public Property Let SiteID(p_iSiteID)
		g_iSiteID = p_iSiteID
	End Property

	Public Property Get SiteID()
		SiteID = g_iSiteID
	End Property

	Public Property Let ErrorDescription(p_sErrorDescription)
		g_sErrorDescription = p_sErrorDescription
	End Property

	Public Property Get ErrorDescription()
		ErrorDescription = g_sErrorDescription
	End Property

	Public Property Let Message(p_sMessage)
		g_sMessage = p_sMessage
	End Property

	Public Property Get Message()
		Message = g_sMessage
	End Property

	Public Property Set ContentRS(p_oContentRS)
		set g_oContentRS = p_oContentRS
	End Property

	Public Property Get ContentRS()
		set ContentRS = g_oContentRS
	End Property	

	Public Property Set ContentListRS(p_oContentListRS)
		set g_oContentListRS = p_oContentListRS
	End Property

	Public Property Get ContentListRS()
		set ContentListRS = g_oContentListRS
	End Property

	Public Property Set KeywordsRS(p_oKeywordsRS)
		set g_oKeywordsRS = p_oKeywordsRS
	End Property

	Public Property Get KeywordsRS()
		set KeywordsRS = g_oKeywordsRS
	End Property	

	Public Property Set MediaRS(p_oMediaRS)
		set g_oMediaRS = p_oMediaRS
	End Property

	Public Property Get MediaRS()
		set MediaRS = g_oMediaRS
	End Property
	
	Private Property Set Connection(p_oConnection)
		set g_oConnection = p_oConnection
	End Property

	Private Property Get Connection()
		set Connection = g_oConnection
	End Property	
	
	Public Property Let MediaName(p_sMediaName)
		g_sMediaName = p_sMediaName
	End Property

	Public Property Get MediaName()
		p_sMediaName = g_sMediaName
	End Property		
	
	Public Property Let MediaPath(p_sMediaPath)
		g_sMediaPath = p_sMediaPath
	End Property

	Public Property Get MediaPath()
		MediaPath = g_sMediaPath
	End Property

	Public Property Let MediaID(p_iMediaID)
		g_iMediaID = p_iMediaID
	End Property

	Public Property Get MediaID()
		MediaID = g_iMediaID
	End Property

	Public Property Let PageName(p_sPageName)
		g_sPageName = p_sPageName
	End Property

	Public Property Get PageName()
		PageName = g_sPageName
	End Property


	'===========================================================================================================
End Class
%>
<script language=javascript>
	var preloadFlag = true;
//	preloadImages();
	
	//var img_1 = new Image();

<!-- // Hide

// *** COMMON CROSS-BROWSER COMPATIBILITY CODE ***

var isDOM = document.getElementById?1:0;
var isIE = document.all?1:0;
var isNS4 = (navigator.appName=='Netscape' && !isDOM)?1:0;
var isIE4 = (isIE && !isDOM)?1:0;
var isDyn = (isDOM||isIE||isNS4);

function ClearAllRollovers(){
	var MENU_SELECTED = <%=MENU_SELECTED%>;
	
	var aryImgs = new Array(9);
	aryImgs[1] = "/admin/media/images/nav-home.gif";
	aryImgs[2] = "/admin/media/images/nav-cm.gif";
	aryImgs[3] = "/admin/media/images/nav-inventory.gif";
	aryImgs[4] = "/admin/media/images/nav-logout.gif";
	aryImgs[5] = "/admin/media/images/nav-help.gif";
	aryImgs[6] = "/admin/media/images/nav-coupons.gif";
	aryImgs[7] = "/admin/media/images/nav-ads.gif";
	aryImgs[8] = "/admin/media/images/nav-calendar.gif";
	aryImgs[9] = "/admin/media/images/nav-form-manager.gif";
	
	var aryImgsOver = new Array(9);
	aryImgs[1] = "/admin/media/images/nav-home-h.gif";
	aryImgs[2] = "/admin/media/images/nav-cm-h.gif";
	aryImgs[3] = "/admin/media/images/nav-inventory-h.gif";
	aryImgs[4] = "/admin/media/images/nav-logout-h.gif";
	aryImgs[5] = "/admin/media/images/nav-help-h.gif";
	aryImgs[6] = "/admin/media/images/nav-coupons-h.gif";
	aryImgs[7] = "/admin/media/images/nav-ads-h.gif";
	aryImgs[8] = "/admin/media/images/nav-calendar-h.gif";
	aryImgs[9] = "/admin/media/images/nav-form-manager-h.gif";

	for (var i = 1; i <= 4; i++) {
		if (MENU_SELECTED != i){
			changeImages('img_' + i, aryImgs[i])
		}else{
			changeImages('img_' + i, aryImgsOver[i])
		}
	}				
}
function getRef(id, par)
{
 par = !par ? document : (par.navigator?par.document:par);
 return (isIE ? par.all[id] :
  (isDOM ? (par.getElementById?par:par.ownerDocument).getElementById(id) :
  par.layers[id]));
}

function getSty(id, par)
{
 return (isNS4 ? getRef(id, par) : getRef(id, par).style)
}

if (!window.LayerObj) var LayerObj = new Function('id', 'par',
 'this.ref=getRef(id, par); this.sty=getSty(id, par); return this');
function getLyr(id, par) { return new LayerObj(id, par) }

function LyrFn(fn, fc)
{
 LayerObj.prototype[fn] = new Function('var a=arguments,p=a[0]; with (this) { '+fc+' }');
}
LyrFn('x','if (!isNaN(p)) sty.left=p; else return parseInt(sty.left)');
LyrFn('y','if (!isNaN(p)) sty.top=p; else return parseInt(sty.top)');
LyrFn('vis','sty.visibility=p');
LyrFn('bgColor','if (isNS4) sty.bgColor=(p?p:null); ' +
 'else sty.background=p?p:"transparent"');
LyrFn('bgImage','if (isNS4) sty.background.src=(p?p:null); ' + 
 'else sty.background=(p?"url("+p+")":"") ');
LyrFn('clip','if (isNS4) with(sty.clip) { left=a[0]; top=a[1]; right=a[2]; bottom=a[3] } ' +
 'else sty.clip="rect("+a[1]+"px "+a[2]+"px "+a[3]+"px "+a[0]+"px)" ');
LyrFn('write','if (isNS4) with (ref.document) {write(p);close()} else ref.innerHTML=p');
LyrFn('alpha','var f=ref.filters; if (f) {' +
 'if (sty.filter.indexOf("alpha")==-1) sty.filter+="alpha()"; ' +
 'if (f.length&&f.alpha) f.alpha.opacity=p } else if (isDOM) sty.MozOpacity=(p/100)');


function setLyr(lVis, docW, par)
{
 if (!setLyr.seq) setLyr.seq=0;
 if (!docW) docW=0;
 var obj = (!par ? (isNS4 ? window : document.body) :
  (!isNS4 && par.navigator ? par.document.body : par));
 var newID='_js_layer_'+setLyr.seq++;

 if (isIE&&!window.opera) obj.insertAdjacentHTML('beforeEnd', '<div id="'+newID+
  '" style="position:absolute"></div>');
 else if (isDOM)
 {
  var newL=document.createElement('div');
  obj.appendChild(newL);
  newL.id=newID; newL.style.position='absolute';
 }
 else if (isNS4)
 {
  var newL=new Layer(docW, obj);
  newID=newL.id;
 }

 var lObj=getLyr(newID, par);
 with (lObj.sty) { visibility=lVis; left=0; top=0; width=docW }
 return lObj;
}


if (!window.page) var page = { win: window, minW: 0, minH: 0, MS: isIE&&!window.opera }

page.winW=function()
 { with (this) return Math.max(minW, MS?win.document.body.clientWidth:win.innerWidth) }
page.winH=function()
 { with (this) return Math.max(minH, MS?win.document.body.clientHeight:win.innerHeight) }

page.scrollX=function()
 { with (this) return MS?win.document.body.scrollLeft:win.pageXOffset }
page.scrollY=function()
 { with (this) return MS?win.document.body.scrollTop:win.pageYOffset }



// *** MOUSE EVENT CONTROL FUNCTIONS ***


// Most of these are passed the relevant 'menu Name' and 'item Number'.
// The 'with (this)' means it uses the properties and functions of the current menu object.
function popOver(mN, iN) { with (this)
{
 // Cancel any pending menu hides from a previous mouseout.
 clearTimeout(hideTimer);
	var imageNum = iN;
	var MENU_SELECTED = <%=MENU_SELECTED%>;
	var sRolloverSource;



 // Set the 'over' variables to point to this item.
 overM = mN;
 overI = iN;
 // A quick reference to this item.
 var thisI = menu[mN][iN];

 // Call the 'onMouseOver' event if it exists, and the item number is 1 or more.
 if (iN && this.onmouseover) this.onmouseover();


 // Remember what was lit last time, and compute a new hierarchy.
 litOld = litNow;
 litNow = new Array();
 var litM = mN, litI = iN;
 while(1)
 {
  litNow[litM] = litI;
  // If we've reached the top of the hierarchy, exit loop.
  if (litM == 'root') break;
  // Otherwise repeat with this menu's parent.
  litI = menu[litM][0].parentItem;
  litM = menu[litM][0].parentMenu;
 }

 // If the two arrays are the same, return... no use hiding/lighting otherwise.
 var same = true;
 for (var z in menu) if (litNow[z] != litOld[z]) same = false;
 if (same) return;



 // Cycle through menu array, lighting and hiding menus as necessary.
 for (thisM in menu) with (menu[thisM][0])
 {
  // Doesn't exist yet? Keep rollin'...
  if (!lyr) continue;

  // The number of this menu's item that is to be lit, undefined if none.
  litI = litNow[thisM];
  oldI = litOld[thisM];

  // If it's lit now and wasn't before, highlight...
  if (litI && (litI != oldI)) changeCol(thisM, litI, true);

  // If this item was lit but another is now, dim the old item.
  if (oldI && (oldI != litI)) changeCol(thisM, oldI, false);

  // Make sure if it's lit, it's shown, and set the visNow flag.
  if (litI && !visNow && (thisM != 'root'))
  {
   showMenu(thisM);
   visNow = true;
  }

  // If this menu has no items from the current hierarchy in it, and is currently
  // onscreen, call the hide function.
  if (isNaN(litI) && visNow)
  {
   hideMenu(thisM);
   visNow = false;
  }
 }


 // Get target menu to show - if we've got one, position & show it.
 clearTimeout(showTimer);
 nextMenu = '';
 if (thisI.type == 'sm:')
 {
  // The target menu and the layer object of the current menu itself (not this item).
  var targ = thisI.href, lyrM = menu[mN][0].lyr;

  // Add current menu/item positions to the target's offset to set its position, show it.
  with (menu[targ][0])
  {
   // If the target menu has not been created yet, return as we can't do anything.
   if (!lyr || !lyr.ref) return;

   // If the offset is a number add this item's position else just calculate position.
   lyr.x(eval(offX) + (typeof(offX)=='number' ? lyrM.x() + thisI.lyr.x() : 0));
   lyr.y(eval(offY) + (typeof(offY)=='number' ? lyrM.y() + thisI.lyr.y() : 0));

   // Either show immediately or after a delay if set. Set nextMenu to the impending show.
   showStr = myName + '.showMenu("' + targ + '"); ' +
    myName + '.menu.' + targ + '[0].visNow = true';
   nextMenu = targ;
   if (showDelay) showTimer = setTimeout(showStr, showDelay);
   else eval(showStr);
  }
 }
}}


function popChangeCol(mN, iN, isOver) { with (this.menu[mN][iN])
{
 // Swap the background colour/image depending on highlight state.
 // If it's got a period in it, call it an image, otherwise it must be a colour.
 var col = isOver ? overCol : outCol;
 if (col.indexOf('.') == -1) lyr.bgColor(col);
 else lyr.bgImage(col);

 // Test for other style changes, we can skip them if not needed.
 var doFX = ((overClass != outClass) || (outBorder != overBorder));

 // In Netscape 4, rewrite layer contents (causes a little flickering)...
 if (doFX && isNS4) lyr.write(this.getHTML(mN, iN, isOver));

 // ...otherwise manipulate the DOM tree for IE/NS6+ (faster than rewriting contents).
 else if (doFX) with (lyr)
 {
  ref.className = (isOver ? overBorder : outBorder);
  var chl = (isDOM ? ref.childNodes : ref.children)
  if (chl) for (var i = 0; i < chl.length; i++)
   chl[i].className = (isOver ? overClass : outClass);
 }

 // Alpha filtering activated? Might as well set that then too...
 // Weirdly it has to be done after the border change, another random Mozilla bug...
 if (typeof(outAlpha)=='number') lyr.alpha(isOver ? overAlpha : outAlpha);
}}


function popOut(mN, iN) { with (this)
{
 // Sometimes, across frames, overs and outs can get confused. If this is called before
 // the relevant over command, return...
 if ((mN != overM) || (iN != overI)) return;
 // Evaluate the onmouseout event, if any.
 if (this.onmouseout) this.onmouseout();

 var thisI = menu[mN][iN];

 // Stop showing another menu if this item isn't pointing to the same one.
 if (thisI.href != nextMenu) clearTimeout(showTimer);

 if (hideDelay)
 {
  var delay = ((mN == 'root') && (thisI.type != 'sm:')) ? 50 : hideDelay;
  hideTimer = setTimeout(myName + '.over("root", 0)', delay);
 }

 // Clear the 'over' variables.
 overM = 'root';
 overI = 0;
}}


function popClick(evt) { with (this)
{
 // If the moused over item number isn't 0, activate it!
 if (overI)
 {
  // Evaluate the onclick event, if any.
  if (this.onclick) this.onclick();

  var thisI = menu[overM][overI];

  with (thisI) switch (type)
  {
   // Targeting another popout? Clicking will get you nowhere...
   case 'sm:': return;
   // A JavaScript function? Eval() it and break out of switch.
   case 'js:': { eval(href); break }
   // Otherwise, point to the window if nothing else and navigate.
   case '': type = 'window';
   default: if (href) eval(type + '.location.href = "' + href + '"');
  }
 }

 // Whatever happens, hide the menus when clicked.
 over('root', 0);
}}


function popClearLyr(wN) { with (this)
{
 // Pass a window name on unload. Any menus in that window have their layer objects
 // deleted, for re-creation later.
 for (mN in menu) with (menu[mN][0]) if (par == wN) lyr = null;
}}



// *** MENU OBJECT CONSTRUCTION FUNCTIONS ***

// This takes arrays of data and names and assigns the values to a specified object.
function addProps(obj, data, names, addNull)
{
 for (var i = 0; i < names.length; i++) if(i < data.length || addNull) obj[names[i]] = data[i];
}

function ItemStyle()
{
 // Like the other constructors, this passes a list of property names that correspond to the list
 // of arguments. Feel free to add more than the first 4 to the addItem() command if you want.
 var names = ['len', 'spacing', 'popInd', 'popPos', 'pad', 'outCol', 'overCol', 'outClass',
  'overClass', 'outBorder', 'overBorder', 'outAlpha', 'overAlpha'];
 addProps(this, arguments, names, true);
}

function popStartMenu(mName) { with (this)
{
 // Create a new array within the menu object if none exists already, and a new menu object within.
 if (!menu[mName]) { menu[mName] = new Array(); menu[mName][0] = new Object(); }

 // Set this as the active menu to which new items are added, and clean out existing items.
 actMenu = mName;
 menu[mName].length = 1;
 nextItem = 1;
 // A quick reference to the current menu descriptor -- array index 0, 1+ are items.
 var aM = menu[mName][0];

 // Not all of these are actually passed to the constructor -- the last few are null.
 // N.B: I pass 'isVert' twice so the first parameter (the menu name) is overwritten & ignored.
 var names = ['isVert', 'isVert', 'offX','offY', 'width', 'itemSty', 'par', 'parentMenu',
  'parentItem', 'visNow', 'menuW', 'menuH'];
 addProps(aM, arguments, names, true);

 // Reuse old layers if we can, no use creating new ones every time the menus refresh.
 if (!aM.lyr) aM.lyr = null;
}}

function popAddItem() { with (this)
{
 // Add these properties onto a new 'active Item' at the end of the active menu.
 var aI = menu[actMenu][nextItem++] = new Object();

 // Add function parameters to object. Again, the last three are undefined, set later.
 var names = ['text', 'href', 'type', 'itemSty', 'len', 'spacing', 'popInd', 'popPos',
  'pad', 'outCol', 'overCol', 'outClass', 'overClass', 'outBorder', 'overBorder',
  'outAlpha', 'overAlpha', 'iW', 'iH', 'lyr'];
 addProps(aI, arguments, names, true);

 // Find an applicable itemSty -- either passed to this item or the menu[0] object.
 var iSty = (arguments[3] ? arguments[3] : menu[actMenu][0].itemSty);
 // Loop through its properties, add them if they don't already exist (overridden e.g. length).
 for (prop in iSty) if (aI[prop] == window.UnDeFiNeD) aI[prop] = iSty[prop];

 // In NS4, since borders are assigned to the contents rather than the layer, increase padding.
 if (aI.outBorder)
 {
  if (isNS4) aI.pad++;
 }
}}



// *** MAIN MENU CREATION/UPDATE FUNCTIONS ***

// Returns the inner HTML of an item, used for menu generation, and highlights in NS4.
function popGetHTML(mN, iN, isOver) { with (this)
{
 var itemStr = '';
 with (menu[mN][iN])
 {
  var textClass = (isOver ? overClass : outClass);

  // If we're supposed to add a popout indicator, add it before text so it appears below in NS4.
  // Note the (iW - 15): the position is hardcoded at 15px from the item's right edge.
  if ((type == 'sm:') && popInd)
  {
   if (isNS4) itemStr += '<layer class="' + textClass + '" left="'+ ((popPos+iW) % iW) +
    '" top="' + pad + '">' + popInd + '</layer>';
   else itemStr += '<div class="' + textClass + '" style="position: absolute; left: ' +
    ((popPos+iW) % iW) + '; top: ' + pad + '">' + popInd + '</div>';
  }

  if (isNS4) itemStr += (outBorder ? '<span class="' + (isOver ? overBorder : outBorder) +
   '"><spacer type="block" width="' + (iW - 8) + '" height="' + (iH - 8) + '"></span>' : '') +
   '<layer left="' + pad + '" top="' + pad + '" width="' + (iW - (2 * pad)) + '" height="' +
   (iH - (2 * pad)) + '"><a class="' + textClass + '" href="#" ' +
   'onClick="return false" onMouseOver="status=\'\'; ' + myName + '.over(\'' + mN + '\',' +
   iN + '); return true">' + text + '</a></layer>';

  // IE4+/NS6 is an awful lot easier to work with sometimes.
  else itemStr += '<div class="' + textClass + '" style="position: absolute; left: ' + pad +
   '; top: ' + pad + '; width: ' + (iW - (2 * pad)) + '; height: ' + (iH - (2 * pad)) +
   '">' + text + '</div>';
 }
 return itemStr;
}}


// The main menu creation/update routine. The first parameter is 'true' if you want the script
// to use document.write() to create the menus. Second parameter is optionally the name of one
// menu only to update rather then create al menus.
function popUpdate(docWrite, upMN) { with (this)
{
 // 'isDyn' (set at the very top of the script) signifies a DHTML-capable browser.
 if (!isDyn) return;

 // Loop through menus, using properties of menu description object (array index 0)...
 for (mN in menu) with (menu[mN][0])
 {
  // If we're updating one specific menu, only run the code for that.
  if (upMN && (upMN != mN)) continue;

  // Variable for holding HTML for items and positions of next item.
  var str = '', iX = 0, iY = 0;

  // Remember, items start from 1 in the array (0 is menu object itself, above).
  // Also use properties of each item nested in the other with() for construction.
  for (var iN = 1; iN < menu[mN].length; iN++) with (menu[mN][iN])
  {
   // An ID for divs/layers contained within the menu.
   var itemID = myName + '_' + mN + '_' + iN;

   // Now is a good time to assign another menu's parent to this if we've got a popout item.
   var targM = menu[href];
   if (targM && (type == 'sm:'))
   {
    targM[0].parentMenu = mN;
    targM[0].parentItem = iN;
   }

   // NS6 and Opera disagree with IE and NS4 as to whether borders increase div widths, so we
   // subtract some pixels here to go with the old 'loose' specification. Swap this around if you
   // want your documents to conform with the new specs.
   var shrink = (outBorder && ((isDOM && !isIE) || window.opera) ? 2 : 0)
   // The actual dimensions of the items, store as item properties so they can be accessed later.
   iW = (isVert ? width : len) - shrink;
   iH = (isVert ? len : width) - shrink;

   // Have we been given a background image? It'll have a period in its name if so...
   var isImg = (outCol.indexOf('.') != -1) ? true : false;

   // Create a div or layer text string with appropriate styles/properties.
   // OK, OK, I know this is a little obtuse in syntax, but it's small...
   // At the end we set the alpha transparency (if specified) and cursor to be a hand
   // if it's not a 'sm:' item or blank -- those items get a regular cursor
   if (isDOM || isIE4)
   {
    str += '<div id="' + itemID + '" ' + (outBorder ? 'class="' + outBorder + '" ' : '') +
     'style="position: absolute; left: ' + iX + '; top: ' + iY + '; width: ' + iW + '; height: ' +
     iH + '; background: ' + (isImg ? 'url('+outCol+')' : outCol) +
     ((typeof(outAlpha)=='number') ? '; filter: alpha(opacity='+ outAlpha + '); -moz-opacity: ' +
      (outAlpha/100) : '') +
     '; cursor: ' + ((type!='sm:' && href) ? (isIE ? 'hand' : 'pointer') : 'default') + '" ';
   }
   else if (isNS4)
   {
    // NS4's borders must be assigned within the layer so they stay when content is replaced.
    str += '<layer id="' + itemID + '" left="' + iX + '" top="' + iY + '" width="' +
     iW + '" height="' + iH + '" ' + (outCol ? (isImg ? 'background="' : 'bgcolor="') +
     outCol + '" ' : '');
   }

   // Add mouseover and click handlers, contents, and finish div/layer.
   str += 'onMouseOver="' + myName + '.over(\'' + mN + '\',' + iN + ')" ' +
     'onMouseOut="' + myName + '.out(\'' + mN + '\',' + iN + ')">' +
     getHTML(mN, iN, false) + (isNS4 ? '</layer>' : '</div>');

   // Move next item position down or across by this item's length and additional spacing.
   // Subtract 1 so borders overlap slightly.
   var spc = (outBorder ? 1 : 0)
   if (isVert) iY += len + spacing - spc;
   else iX += len + spacing - spc;

  // End loop through items and with(menu[mN][iN]).
  }


  // Record the last item position as the dimensions of this menu.
  // N.B: Still using properties of menu[mN][0]...
  menuW = isVert ? width : iX;
  menuH = isVert ? iY : width;



  // The parent frame for this menu, if any.
  var eP = eval(par);


  // Do not ask me why Opera makes me set a timeout now rather than later, or in fact have
  // to set a timeout at all to get references to the divs we are about to create.
  // But, it makes the cross-frame version of the script actually work, so there's a benefit.
  setTimeout(myName + '.setupRef("' + mN + '")', 50);
  
  // For Fast creation mode (default for IE, NS6, Opera), write the menus to the document now.
  if (docWrite)
  {
   // Find the right target frame.
   var targFr = (eP && eP.navigator ? eP : window);
   targFr.document.write('<div id="' + myName + '_' + mN + '_Div" style="position: absolute; ' +
    'visibility: hidden; width: ' + menuW + '; height: ' + menuH + '; z-index: 1000">' +
    str + '</div>');
  }
  else
  {
    if (!lyr) lyr = setLyr('hidden', menuW, eP);
   else if (isIE4) setTimeout(myName + '.menu.' + mN + '[0].lyr.sty.width=' + (menuW+2), 50);

   // Give it a high Z-index, and write its content.
   with (lyr) { sty.zIndex = 1000; write(str) }
  }

 // End loop through menus and with (menu[mN][0]).
 }
}}


function popSetupRef(mN) { with (this) with (menu[mN][0])
{
 // Get a reference to a div, only needed for Fast creation mode.
 if (!lyr || !lyr.ref) lyr = getLyr(myName + '_' + mN + '_Div', eval(par));

 // Loop through menu items again to set up individual references.
 for (var i = 1; i < menu[mN].length; i++)
  menu[mN][i].lyr = getLyr(myName + '_' + mN + '_' + i, lyr.ref);

 // Position and show the root menu once written. Phew!
 if (mN == 'root')
 {
  position();
  lyr.vis('visible');
 }
}}


function popPosition(wN) { with (this)
{
 // Pass this a window name to position the absolute menus in that window, otherwise all are.
 for (mN in menu) if (!wN || menu[mN][0].par == wN) with (menu[mN][0])
 {
  // If the menu hasn't been created, and there's no layer reference, loop.
  if (!lyr || !lyr.ref) continue;

  // If either of the offsets is a string or its the root menu, position it.
  if (typeof(offX)!='number' || mN=='root') lyr.x(eval(offX));
  if (typeof(offY)!='number' || mN=='root') lyr.y(eval(offY));
 }
}}


// *** POPUP MENU MAIN OBJECT CONSTRUCTOR ***

function PopupMenu(myName)
{
 // These are the properties of any PopupMenu objects you create.
 this.myName = myName;

 // Manage what gets lit and shown when.
 this.showTimer = 0;
 this.hideTimer = 0;
 this.showDelay = 50;
 this.hideDelay = 1;
 this.showMenu = '';

 // 'menu': the main data store, contains subarrays for each menu e.g. pMenu.menu['root'][];
 this.menu =  new Array();
 // litNow and litOld arrays control what items get lit in the hierarchy.
 this.litNow = new Array();
 this.litOld = new Array();

 // The item the mouse is currently over. Used by click processor to help NS4.
 this.overM = 'root';
 this.overI = 0;

 // The active menu and next item to which addItem() will assign its results. startMenu() sets
 // these automatically, set them manually if you want to call addItem() yourself, e.g.:
 // actMenu = 'root'; nextItem = 5; addItem('Another root menu item'...);
 this.actMenu = '';
 this.nextItem = 1;

 // Functions to create and manage the menu.
 this.over = popOver;
 this.changeCol = popChangeCol;
 this.out = popOut;
 this.click = popClick;
 this.clearLyr = popClearLyr;
 this.startMenu = popStartMenu;
 this.addItem = popAddItem;
 this.getHTML = popGetHTML;
 this.update = popUpdate;
 this.setupRef = popSetupRef;
 this.position = popPosition;
 
 // Default show and hide functions, overridden in the example script by the clipping routine.
 this.showMenu = function(mName) { this.menu[mName][0].lyr.vis('visible') }
 this.hideMenu = function(mName) { this.menu[mName][0].lyr.vis('hidden') }
}

var hBar = new ItemStyle(110, 41, '', 0, 0, '', '', 'itemText', 'itemText', '', '',
 null, null);

// The 'sub Menu' items: 20px long, 0px spacing, a 'greater than' sign for a popout indicator
// (you may wish to use an image tag?), the popout indicator is positioned 15px from the right
// edge of the item, items have 3px padding, some colours, it uses 'itemText' as the dimmed text
// class but 'itemHover' when it is moused over, and 'itemBorder' as the border stylesheet class.
var subM = new ItemStyle(20, 0, '&gt;', -15, 0, '#000000', '#666666', 'itemText', 'itemHover', '', '', null, null);

var productMenu = new ItemStyle(90, 0, '&gt;', -15, 0, '#78B0E9', '#78B0E9', 'itemText', 'itemHover', '', '', null, null);


// The purplish 'crazy' style, same length but extra 1px spacing (to show up the fancy border) and
// different colours/text and less padding. They also have translucency set -- these items
// are 80% opaque when dim and 90% when highlighted.
var crazy = new ItemStyle(22, 1, '&gt;', -15, 2, '#666699', '#CC6600', 'crazyText', 'crazyHover', 'crazyBorder', 'crazyBorderOver', 80, 90);


var pMenu = new PopupMenu('pMenu');
with (pMenu)
{

// The 'root' menu is horizontal, positioned at (x = 112, y = 51) and is 26px high, and items
// by default use the colours and dimensions in the 'hBar' ItemStyle defined above.
//startMenu('root', false, 359, 129, 26, hBar);
startMenu('root', false, 'Math.max(0,page.winW()/2 - pMenu.menu.root[0].menuW/2)', '48', 24, hBar); // Centres & floats.

//Do Not use the true height of the images.  It seems to increase the horizontal spacing of these links.  Set the height to 0.
//The height of these horizontally aligned links is controlled above by the startMenu parameters. 

//EXAMPLE:
//addItem('ITEM TEXT', 'Action Target', 'Action', 'defaultItemStyle', width, height, 'popout indicator', popout padding, popout padding over, 'Normal background image', 'Over background image');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');
addItem('<a href="/admin/login/admin.asp" target="_top"><img src=media/images/clear.gif width=54 height=24 border=0 alt="Home"></a>', '', '','hBar',54,0,'',0,0,'media/images/nav-home.gif','media/images/nav-home-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');
addItem('<a href="/admin/ContentManager/" target="_top"><img src=media/images/clear.gif width=120 height=24 border=0 alt="Content Manager"></a>', 'mCM', 'sm:','hBar',120,0,'',0,0,'media/images/nav-cm.gif','media/images/nav-cm-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');
addItem('<a href="/admin/AutoID/" target="_top"><img src=media/images/clear.gif width=75 height=24 border=0 alt="Inventory"></a>', '', '','hBar',75,0,'',0,0,'media/images/nav-inventory.gif','media/images/nav-inventory-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');
addItem('<a href="/admin/login/logout.asp" target="_top"><img src=media/images/clear.gif width=67 height=24 border=0 alt="Logout"></a>', '', '','hBar',67,0,'',0,0,'media/images/nav-logout.gif','media/images/nav-logout-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');
addItem('<a href="/admin/login/help.asp" target="_top"><img src=media/images/clear.gif width=47 height=24 border=0 alt="Help"></a>', '', '','hBar',47,0,'',0,0,'media/images/nav-help.gif','media/images/nav-help-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division.gif','media/images/nav-division.gif');

//  Menus begin here

startMenu('mCM', false, 0, 25, 24, productMenu);
addItem('<a href="/admin/WebCoupons/" target="_top"><img src=media/images/clear.gif width=69 height=24 border=0 alt="Coupons"></a>', '', '','hBar',69,0,'',0,0,'media/images/nav-coupons.gif','media/images/nav-coupons-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division2.gif','media/images/nav-division2.gif');
addItem('<a href="/admin/Ads/" target="_top"><img src=media/images/clear.gif width=42 height=24 border=0 alt="Ads"></a>', '', '','hBar',42,0,'',0,0,'media/images/nav-ads.gif','media/images/nav-ads-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division2.gif','media/images/nav-division2.gif');
addItem('<a href="/admin/CalendarFX/" target="_top"><img src=media/images/clear.gif width=73 height=24 border=0 alt="Calendar"></a>', '', '','hBar',73,0,'',0,0,'media/images/nav-calendar.gif','media/images/nav-calendar-h.gif');
addItem('<img src=media/images/clear.gif width=1 height=24 border=0 alt="">', '', '','',1,0,'',0,0,'media/images/nav-division2.gif','media/images/nav-division2.gif');
addItem('<a href="/admin/CalendarFX/" target="_top"><img src=media/images/clear.gif width=104 height=24 border=0 alt="Form Manager"></a>', '', '','hBar',104,0,'',0,0,'media/images/nav-form-manager.gif','media/images/nav-form-manager-h.gif');


// Submenus begin here

// No submenus today.  thank you.  come again.

}

if (!isNS4)
{
 // Write menus now in non-NS4 browsers, by calling the "Fast" mode .update(true) method.
 pMenu.update(true);
 //anotherMenu.update(true);
}
else
{
 // For Netscape 4, back up the old onload function and make a new one to update our menus.
 // This is the regular "Dynamic" mode menu update, it works in IE and NS6 too.
 var popOldOL = window.onload;
 window.onload = function()
 {
  if (popOldOL) popOldOL();
  pMenu.update();
  //anotherMenu.update();
 }
}


// Other events must be assigned, these are less complicated, just add or remove menu objects.

window.onresize = function()
{
 ns4BugCheck();
 pMenu.position();
 //anotherMenu.position();
}

window.onscroll = function()
{
 pMenu.position();
 //anotherMenu.position();
}

if (isNS4) document.captureEvents(Event.CLICK);
document.onclick = function(evt)
{
 pMenu.click();
 //anotherMenu.click();
 if (isNS4) return document.routeEvent(evt);
}


// A small function that refreshes NS4 on window resize to avoid bugs, called above.
var nsWinW = window.innerWidth, nsWinH = window.innerHeight;
function ns4BugCheck()
{
 if (isNS4 && (nsWinW!=innerWidth || nsWinH!=innerHeight)) location.reload()
}

// Activate the useful 'onscroll' event for non-Microsoft browsers.
if (!isIE || window.opera)
{
 var nsPX=pageXOffset, nsPY=pageYOffset;
 setInterval('if (nsPX!=pageXOffset || nsPY!=pageYOffset) ' +
 '{ nsPX=pageXOffset; nsPY=pageYOffset; window.onscroll() }', 50);
}


function menuClip(menuObj, menuName, dir)
{
 // The array index of the named menu (e.g. 'mFile') in the menu object (e.g. 'pMenu').
 var mD = menuObj.menu[menuName][0];
 // Add timer and counter variables to the menu data structure, we'll need them.
 if (!mD.timer) mD.timer = 0;
 if (!mD.counter) mD.counter = 0;
 with (mD)
 {
  // Stop any existing animation.
  clearTimeout(timer);
  // If the layer doesn't exist (cross-frame navigation) quit.
  if (!lyr || !lyr.ref) return;
  // Show the menu if that's what we're doing.
  if (dir==1) lyr.vis('visible');
  // Also raise showing layers above hiding ones.
  lyr.sty.zIndex = 1001 + dir;
  // Clip the visible area. Tweak this if you want to change direction/acceleration etc.
  lyr.clip(0, 0, menuW+2, (menuH+2)*Math.pow(Math.sin(Math.PI*counter/20),0.75) );
  // Increment the counter and if it hasn't reached the end (10 steps either way) set the timer.
  // Clear the clipping value in DOM browsers as early NS6 versions are quite terrible at this.
  counter += dir;
  if (counter==11) { counter = 10; if (isDOM&&!isIE) lyr.sty.clip='' }
  else if (counter<0) { counter = 0; lyr.vis('hidden') }
  else timer = setTimeout(menuObj.myName+'.'+(dir==1?'show':'hide')+'Menu("'+menuName+'")', 40);
 }
}


// Add the effect to the 'pMenu' menu object for supported browsers. Opera doesn't support clipping
// so we turn it off, neither does IE4/Mac.
if (!window.opera)
{
 pMenu.showMenu = new Function('mN','menuClip(pMenu, mN, 1)');
 pMenu.hideMenu = new Function('mN','menuClip(pMenu, mN, -1)');
}


function changeMenu() { with (pMenu)
{
 // If your modifications are quite extensive, probably hide the menu before commencing
 // them, as the script relies on the menu array in realtime to handle highlighting etc.
 //over('root', 0);
 //menu.root[0].lyr.vis('hidden');

 // Create a whole new menu...
 startMenu('mNewMenu', true, 0, 22, 130, subM);
 addItem('<b>TwinHelix Designs:</b><br>Extreme DHTML.<br>Small Code.<br>Click to Visit...',
  'window.open("http://www.twinhelix.com")', 'js:', subM, 70);

 // Alter just the 'Visit My Site' menu item to pop it out... setting nextItem tells the
 // addItem() command which item number we are up to in the sequence.
 actMenu = 'root';
 nextItem = 4;
 addItem('&nbsp; Dynamism...', 'mNewMenu', 'sm:', hBar, 80);

 // Call the update function to implement our changes.
 update();
}}


// This is just the moving command called when you click the feature list.
function moveRoot()
{
 with (pMenu.menu.root[0].lyr) x( (x()<100) ? 100 : 10);
}


// End Hide -->

//<!-- *** STOP EDITING HERE - END OF MENU SECTION *** -->
</script>
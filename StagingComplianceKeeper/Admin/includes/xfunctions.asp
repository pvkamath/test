<%
' DisplayBranchesDropDown(p_iSelectedValue,p_iCompanyID,p_iDisplayType)
' DisplayBusinessTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayCompaniesDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayCourseDropDown(p_iSelectedValue,p_sCourseType,p_bDisabled,p_iDisplayType)
' DisplayCrossCertCourseDropDown(p_sDropDownName,p_iSelectedValue,p_oRS,p_sCourseIDs,p_iDisplayType)
' DisplayCourseTypesDropDown(p_iSelectedValue)
' DisplayCrossCertificationTypesDropDown(p_iSelectedValue,p_iDisplayType, p_bCrossCertTypeDisabled)
' DisplayCurrentExpiredDropDown(p_iSelectedValue,p_sJSEvents)
' DisplayLessonSectionsDropDown(p_iSelectedValue,p_iLessonID,p_bDisabled,p_iDisplayType)
' DisplayProgramDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayReleaseTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayStatesDropDown(p_iSelectedValue,p_iDisplayType,p_iRestricted)
' DisplayStatusDropDown(p_iSelectedValue,p_sJSEvents)
' DisplaySupplementCourseDropDown(p_iSelectedValue,p_iCourseID,p_iDisplayType)
' DisplayTestTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayTimeTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayUserStatusDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayUserTypeDropDown(p_iSelectedValue,p_iDisplayType)
' DisplayVerificationMethodDropDown(p_iSelectedValue,p_iDisplayType)
' ValueInDelString(p_sValue,p_sSearchString)
'



' Name : DisplayCompanyL2sDropDown
' Purpose : displays a drop down box filled with the appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
Sub DisplayCompanyL2sDropDown(p_iSelectedValue,p_iCompanyId,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL2s
	dim iCompanyL2Id
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT CompanyL2Id, Name FROM CompaniesL2 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	bFirst = true
	aCompanyL2s = split(GetUserCompanyL2IdList, ", ")
	for each iCompanyL2Id in aCompanyL2s
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL2ID = '" & iCompanyL2Id & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name "

	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	response.write("<select name=""CompanyL2Id"" " & sDisabled)
	if (cint(p_iDisplayType) = 2) then
		response.write(" onChange=""CompanyL2Change();"">" & vbcrlf)
	else
		Response.Write(">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		'nothing		
	else
		response.write("<option value="""">No " & session("CompanyL2Name") & "</option>" & vbcrlf)
	end if
	
	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
		do while not rs.eof
			if (trim(p_iSelectedValue) = trim(rs("CompanyL2Id"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CompanyL2ID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if 
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub


' Name : DisplayCompanyL3sDropDown
' Purpose : displays a drop down box filled with the appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
Sub DisplayCompanyL3sDropDown(p_iSelectedValue,p_iCompanyId,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL3s
	dim iCompanyL3Id
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT CompanyL3Id, Name FROM CompaniesL3 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	bFirst = true
	aCompanyL3s = split(GetUserCompanyL3IdList, ", ")
	for each iCompanyL3Id in aCompanyL3s
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL3ID = '" & iCompanyL3Id & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name "

	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	response.write("<select name=""CompanyL3Id"" " & sDisabled)
	if (cint(p_iDisplayType) = 2) then
		response.write(" onChange=""CompanyL3Change();"">" & vbcrlf)
	else
		Response.Write(">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		'nothing		
	else
		response.write("<option value="""">No " & session("CompanyL3Name") & "</option>" & vbcrlf)
	end if
	
	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
		do while not rs.eof
			if (trim(p_iSelectedValue) = trim(rs("CompanyL3Id"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CompanyL3ID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub


' Name : DisplayBranchesDropDown
' Purpose : displays a drop down box filled with all existing branch names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	     : p_iCompanyID = the company ID of the Branches to retrieve
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
Sub DisplayBranchesDropDown(p_iSelectedValue,p_iCompanyID,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aBranches
	dim iBranchId
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT BranchID, Name FROM CompanyBranches " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	bFirst = true
	aBranches = split(GetUserBranchIdList, ", ")
	for each iBranchId in aBranches
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "BranchID = '" & iBranchId & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name, BranchNum "

	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	
	response.write("<select name=""BranchId"" " & sDisabled)
	if (cint(p_iDisplayType) = 2) then
		response.write(" onChange=""BranchChange();"">" & vbcrlf)
	else
		Response.Write(">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		'nothing		
	else
		response.write("<option value="""">No Branch</option>" & vbcrlf)
	end if
	
	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
		do while not rs.eof
			if (trim(p_iSelectedValue) = trim(rs("BranchID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("BranchID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub


' Name : DisplayL2sL3sBranchesDropDown
' Purpose : displays a drop down box filled with the appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
Sub DisplayL2sL3sBranchesDropDown(p_iSelectedValue, p_iOwnerTypeId, p_iCompanyId,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rsBranches
	dim rsL2s
	dim rsL3s
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL2s
	dim iCompanyL2Id
	dim aCompanyL3s
	dim iCompanyL3Id
	dim aBranches
	dim iBranchId
	dim bFirstL2
	dim bFirstL3
	dim bFirstBranch
	
	set objConn = New HandyADO


	'Get the list of CompanyL2s.
	sSQL = "SELECT CompanyL2Id, Name FROM CompaniesL2 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
	bFirstL2 = true
	aCompanyL2s = split(GetUserCompanyL2IdList(), ", ")
	for each iCompanyL2Id in aCompanyL2s
		if bFirstL2 then
			sSql = sSql & "AND ("
			bFirstL2 = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL2ID = '" & iCompanyL2Id & "' "
	next	
	if not bFirstL2 then
		sSql = sSql & ") "
		sSql = sSql & "ORDER BY Name "
		set rsL2s = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	
	'Get the list of CompanyL3s
	sSQL = "SELECT CompanyL3Id, Name FROM CompaniesL3 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
	bFirstL3 = true
	aCompanyL3s = split(GetUserCompanyL3IdList, ", ")
	for each iCompanyL3Id in aCompanyL3s
		if bFirstL3 then
			sSql = sSql & "AND ("
			bFirstL3 = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL3ID = '" & iCompanyL3Id & "' "
	next	
	if not bFirstL3 then
		sSql = sSql & ") "
		sSql = sSql & "ORDER BY Name "
		set rsL3s = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	
	'Get the list of Branches
	sSQL = "SELECT BranchID, Name FROM CompanyBranches " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
	bFirstBranch = true
	aBranches = split(GetUserBranchIdList, ", ")
	for each iBranchId in aBranches
		if bFirstBranch then
			sSql = sSql & "AND ("
			bFirstBranch = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "BranchID = '" & iBranchId & "' "
	next	
	if not bFirstBranch then
		sSql = sSql & ") "
		sSql = sSql & "ORDER BY Name, BranchNum "
		set rsBranches = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	
	
	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	response.write("<select name=""OwnerId"" " & sDisabled)
	if (cint(p_iDisplayType) = 2) then
		response.write(" onChange=""OwnerChange();"">" & vbcrlf)
	else
		Response.Write(">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		'nothing		
	else
		response.write("<option value="""">None</option>" & vbcrlf)
	end if
	
	if not bFirstL2 then
		do while not rsL2s.eof
			if (trim(p_iSelectedValue) = trim(rsL2s("CompanyL2Id"))) and p_iOwnerTypeId = 4 then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""a" & rsL2s("CompanyL2ID") & """ " & sSelected & ">" & session("CompanyL2Name") & " - " & rsL2s("Name") & "</option>" & vbcrlf)
			rsL2s.MoveNext
		loop
	end if
	
	if not bFirstL3 then
		do while not rsL3s.eof
			if (trim(p_iSelectedValue) = trim(rsL3s("CompanyL3Id"))) and p_iOwnerTypeId = 5 then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""b" & rsL3s("CompanyL3ID") & """ " & sSelected & ">" & session("CompanyL3Name") & " - " & rsL3s("Name") & "</option>" & vbcrlf)
			rsL3s.MoveNext
		loop
	end if
	
	if not bFirstBranch then
		do while not rsBranches.eof and not bFirstBranch
			if (trim(p_iSelectedValue) = trim(rsBranches("BranchId"))) and p_iOwnerTypeId = 2 then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""c" & rsBranches("BranchID") & """ " & sSelected & ">Branch - " & rsBranches("Name") & "</option>" & vbcrlf)
			rsBranches.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rsL2s = nothing
	set rsL3s = nothing
	set rsBranches = nothing
	set objConn = nothing
End Sub


' Name : DisplayL2sL3sDropDown
' Purpose : displays a drop down box filled with the appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
Sub DisplayL2sL3sDropDown(p_iSelectedValue,p_iCompanyId,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rsL2s
	dim rsL3s
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL2s
	dim iCompanyL2Id
	dim aCompanyL3s
	dim iCompanyL3Id
	dim bFirstL2
	dim bFirstL3
	
	set objConn = New HandyADO


	'Get the list of CompanyL2s.
	sSQL = "SELECT CompanyL2Id, Name FROM CompaniesL2 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
	bFirstL2 = true
	aCompanyL2s = split(GetUserCompanyL2IdList(), ", ")
	for each iCompanyL2Id in aCompanyL2s
		if bFirstL2 then
			sSql = sSql & "AND ("
			bFirstL2 = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL2ID = '" & iCompanyL2Id & "' "
	next	
	if not bFirstL2 then
		sSql = sSql & ") "
		sSql = sSql & "ORDER BY Name "
		set rsL2s = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	
	'Get the list of CompanyL3s
	sSQL = "SELECT CompanyL3Id, Name FROM CompaniesL3 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
	bFirstL3 = true
	aCompanyL3s = split(GetUserCompanyL3IdList, ", ")
	for each iCompanyL3Id in aCompanyL3s
		if bFirstL3 then
			sSql = sSql & "AND ("
			bFirstL3 = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL3ID = '" & iCompanyL3Id & "' "
	next	
	if not bFirstL3 then
		sSql = sSql & ") "
		sSql = sSql & "ORDER BY Name "
		set rsL3s = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	end if
	
	
	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	response.write("<select name=""OwnerId"" " & sDisabled)
	if (cint(p_iDisplayType) = 2) then
		response.write(" onChange=""OwnerChange();"">" & vbcrlf)
	else
		Response.Write(">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		'nothing		
	else
		response.write("<option value="""">None</option>" & vbcrlf)
	end if
	
	if not bFirstL2 then
		do while not rsL2s.eof
			if (trim(p_iSelectedValue) = trim(rsL2s("CompanyL2Id"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""a" & rsL2s("CompanyL2ID") & """ " & sSelected & ">" & session("CompanyL2Name") & " - " & rsL2s("Name") & "</option>" & vbcrlf)
			rsL2s.MoveNext
		loop
	end if
	
	if not bFirstL3 then
		do while not rsL3s.eof
			if (trim(p_iSelectedValue) = trim(rsL3s("CompanyL3Id"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""b" & rsL3s("CompanyL3ID") & """ " & sSelected & ">" & session("CompanyL3Name") & " - " & rsL3s("Name") & "</option>" & vbcrlf)
			rsL3s.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rsL2s = nothing
	set rsL3s = nothing
	set objConn = nothing
End Sub



' Name : DisplayCompanyL2sListBox
' Purpose : displays a drop down box filled with all appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	     : p_iCompanyID = the company ID of the Branches to retrieve
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens 
' Outputs : prints the drop down to the screen
Sub DisplayCompanyL2sListBox(p_sSelectedValue,p_iCompanyID,p_bDisabled,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL2s
	dim iCompanyL2Id
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT CompanyL2Id, Name FROM CompaniesL2 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	'We want to filter this down to only the assigned companyL2s
	bFirst = true
	aCompanyL2s = split(GetUserCompanyL2IdList, ", ")
	for each iCompanyL2Id in aCompanyL2s
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL2Id = '" & iCompanyL2Id & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name "

	'Response.Write(sSql)
	
	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	if p_sFormName = "" then
		p_sFormName = "CompanyL2IdList" 
	end if 
	
	p_sSelectedValue = ", " & p_sSelectedValue & ","
	
	response.write("<select name=""" & p_sFormName & """ size=""6"" multiple " & sDisabled & ">" & vbCrLf)

	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif cint(p_iDisplayType) = 2 then
		'Do nothing
	else
		response.write("<option value="""">No " & session("CompanyL2Name") & "</option>" & vbcrlf)
	end if
	
	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
		do while not rs.eof
			if instr(1, p_sSelectedValue, ", " & rs("CompanyL2Id") & ",") then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CompanyL2ID") & """" & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub


' Name : DisplayCompanyL3sListBox
' Purpose : displays a drop down box filled with all appropriate names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	     : p_iCompanyID = the company ID of the Branches to retrieve
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens 
' Outputs : prints the drop down to the screen
Sub DisplayCompanyL3sListBox(p_sSelectedValue,p_iCompanyID,p_bDisabled,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aCompanyL3s
	dim iCompanyL3Id
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT CompanyL3Id, Name FROM CompaniesL3 " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	'We want to filter this down to only the assigned companyL3s
	bFirst = true
	aCompanyL3s = split(GetUserCompanyL3IdList, ", ")
	for each iCompanyL3Id in aCompanyL3s
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "CompanyL3Id = '" & iCompanyL3Id & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name "

	'Response.Write(sSql)
	
	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	if p_sFormName = "" then
		p_sFormName = "CompanyL3IdList" 
	end if 
	
	p_sSelectedValue = ", " & p_sSelectedValue & ","
	
	response.write("<select name=""" & p_sFormName & """ size=""6"" multiple " & sDisabled & ">" & vbCrLf)

	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif cint(p_iDisplayType) = 2 then
		'Do nothing
	else
		response.write("<option value="""">No " & session("CompanyL3Name") & "</option>" & vbcrlf)
	end if
	
	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
		do while not rs.eof
			if instr(1, p_sSelectedValue, ", " & rs("CompanyL3Id") & ",") then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CompanyL3ID") & """" & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub


' Name : DisplayBranchesListBox
' Purpose : displays a drop down box filled with all existing branch names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	     : p_iCompanyID = the company ID of the Branches to retrieve
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens 
' Outputs : prints the drop down to the screen
Sub DisplayBranchesListBox(p_sSelectedValue,p_iCompanyID,p_bDisabled,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled
	dim aBranches
	dim iBranchId
	dim bFirst
	
	set objConn = New HandyADO

	sSQL = "SELECT BranchID, Name FROM CompanyBranches " & _
		   "WHERE CompanyID = '" & p_iCompanyID & "' " & _
		   "AND NOT Deleted = '1' " 
		   
	'We want to filter this down to only the assigned branches
	bFirst = true
	aBranches = split(GetUserBranchIdList, ", ")
	for each iBranchId in aBranches
		if bFirst then
			sSql = sSql & "AND ("
			bFirst = false
		else
			sSql = sSql & "OR "
		end if
		sSql = sSql & "BranchID = '" & iBranchId & "' "
	next	
	if not bFirst then
		sSql = sSql & ") "
	end if
	sSql = sSql & "ORDER BY Name, BranchNum "

	'Response.Write(sSql)
	
	'determine if the dropdown should be disabled
	if p_bDisabled then
		sDisabled = "DISABLED"
	else
		sDisabled = ""	
	end if
	
	if p_sFormName = "" then
		p_sFormName = "BranchIdList" 
	end if 
	
	p_sSelectedValue = ", " & p_sSelectedValue & ","
	
	response.write("<select name=""" & p_sFormName & """ size=""6"" multiple " & sDisabled & ">" & vbCrLf)

	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif cint(p_iDisplayType) = 2 then
		'Do nothing
	else
		response.write("<option value="""">No Branch</option>" & vbcrlf)
	end if

	if not bFirst then
		set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)	
		do while not rs.eof
			if instr(1, p_sSelectedValue, ", " & rs("BranchId") & ",") then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("BranchID") & """" & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
			rs.MoveNext
		loop
	end if
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub



' Name : DisplayBusinessTypeDropDown
' Purpose : displays a drop down box filled with all existing Business Types
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens
' Outputs : prints the drop down to the screen
Sub DisplayBusinessTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM BusinessTypes"
	
	if trim(p_iSelectedValue) <> "" then
		sSQL = sSQL & "WHERE BusinessTypeID = '" & p_iSelectedValue & "' "
	end if

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""BusinessType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select a Business Type</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("BusinessTypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("BusinessTypeID") & """ " & sSelected & ">" & rs("BusinessType") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name : DisplayCompaniesDropDown
' Purpose : displays a drop down box filled with all existing company names
' Preconditions: Assumes the existance of the CreateBranchList script via the
'	CreateBranchesJavascript sub.
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen, 3 is for Searching on the User List, 4 is for Searching on the Company Branch list.
' Outputs : prints the drop down to the screen
sub DisplayCompaniesDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT CompanyID, Name FROM Companies WHERE Active = '1' "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""Company"" ")
	
	'determine which method to use
	if (cint(p_iDisplayType) = 3) then
		response.write("onChange=""CompanyChange();"">" & vbcrlf)
	elseif (cint(p_iDisplayType) = 4) then
		Response.write(">" & vbCrLf)	
	else
		response.write("onLoad=""UpdateBranchList(this.value)"" onChange=""UpdateBranchList(this.value);"">" & vbcrlf)
	end if
	
	if (cint(p_iDisplayType) = 0) or (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Company</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("CompanyID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("CompanyID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayTpCompaniesDropDown
' Purpose : displays a drop down box filled with all existing company names
' Preconditions: Assumes the existance of the CreateBranchList script via the
'	CreateBranchesJavascript sub.
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen, 3 is for Searching on the User List, 4 is for Searching on the Company Branch list.
' Outputs : prints the drop down to the screen
sub DisplayTpCompaniesDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT CompanyID, Name FROM trainingpro2.dbo.Companies WHERE Active = '1' ORDER BY Name "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""TpCompany"">" & vbCrLf)
	
	
	if (cint(p_iDisplayType) = 0) or (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Company</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("CompanyID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("CompanyID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayPricingPlansDropDown
' Purpose : displays the pricing plans drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	       p_iSearchType - 0 for searching, 1 for assigning
' Outputs : prints the drop down to the screen
sub DisplayPricingPlansDropDown(p_iSelectedValue, p_iSearchType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM PricingPlans ORDER BY PricingPlanId "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""PricingPlanId"">" & vbcrlf)
	
	if p_iSearchType = 0 then 
		response.write("<option value="""">ALL</option>")
	end if 
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("PricingPlanId"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("PricingPlanId") & """ " & sSelected & ">Plan " & rs("PricingPlanId"))
		if rs("MaxOfficers") <> "" then 
			Response.Write(" - " & rs("MaxOfficers") & " Officer Limit</option>" & vbcrlf)
		else
			Response.Write(" - No Limit</option>" & vbcrlf)
		end if 
		
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayProvidersDropDown
' Purpose : displays a drop down box filled with all existing provider names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning for the Company screens, 2 is for Assigning on the User screen
' Outputs : prints the drop down to the screen
sub DisplayProvidersDropDown(p_iSelectedValue,p_iCompanyId, p_bDisabled, p_iDisplayType, p_bShowTp, p_sJSEvents)
	dim objConn
	dim rs
	dim sSelected
	dim sDisabled
	dim sSQL
	set objConn = New HandyADO

	'Determine if the dropdown is disabled.
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	

	sSQL = "SELECT ProviderID, Name FROM Providers WHERE OwnerCompanyId = '" & p_iCompanyId & "' AND Status = '1'"
	'Response.Write(sSql)

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""Provider"" " & p_sJSEvents & " " & sDisabled & ">" & vbCrLf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Provider</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 3) then
		Response.Write("<option value="""">Select a Provider</option>" & vbCrLf)
	end if
	
	'Write out an option for TrainingPro
	if p_bShowTp then
		Response.Write("<option value=""0""")
		if trim(p_iSelectedValue) = 0 then
			Response.Write(" SELECTED")
		end if
		Response.Write(">TrainingPro</option>" & vbCrLf)
	end if 
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ProviderId"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ProviderID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name	  : DisplayProviderCoursesDropDown
' Purpose : Display a dropdown selection of courses for a given provider
' Inputs  : p_iProviderId - the id of the provider whose courses we'll be retrieving
'		  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayProviderCoursesDropDown(p_iProviderId, p_iSelectedValue, p_bDisabled, p_iDisplayType)

	dim objConn
	dim oRs
	dim sSelected
	dim sSql
	dim bDisplayValue
	dim iCurrentCourseID, iPreviousCourseID
	dim sDisabled
	set objConn = New HandyADO

	
	'Determine if the dropdown is disabled
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	
	'If we're pulling trainingpro courses
	if p_iProviderId = "0" then
	
		sSql = "SELECT Cs.CourseId, Cs.Name " & _
			"FROM " & application("sTpDbName") & ".dbo.Courses AS Cs " & _
			"WHERE Cs.Purchasable = '1'"
		
	else
	
		sSql = "SELECT Cs.CourseId, Cs.Name, Cs.Number " & _
			"FROM Courses AS Cs " & _
			"WHERE NOT Cs.Deleted = '1' " & _
			"AND Cs.ProviderId = '" & p_iProviderId & "'"
	
	end if 

	set oRs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

	response.write("<select name=""CourseId"" " & sDisabled & ">" & vbCrLf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	elseif (cint(p_iDisplayType) = 2) then
		response.write("<option value="""">No Course</option>" & vbcrlf)
	end if

	if (oRs.BOF and oRs.EOF) then
		Response.Write("<option value="""">*No Courses Found</option>" & vbCrLf)
	end if 

	do while not oRs.eof		
		iCurrentCourseID = oRs("CourseID")
		if (trim(iCurrentCourseID) <> trim(iPreviousCourseID)) then
			if (trim(p_iSelectedValue) = trim(oRs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			if p_iProviderId = 0 then
				response.write("<option value=""" & oRs("CourseID") & """ " & sSelected & ">" & oRs("Name") & "</option>" & vbcrlf)
			else
				Response.Write("<option value=""" & oRs("CourseId") & """ " & sSelected & ">" & oRs("Number") & " - " & oRs("Name") & "</option>" & vbCrLf)
			end if
		end if
		iPreviousCourseID = iCurrentCourseID
		oRs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set oRs = nothing

end sub


' Name 	  : DisplayCourseDropDown
' Purpose : displays the course dropdown
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayCourseDropDown(p_iSelectedValue,p_oRS,p_sJSEvents,p_bDisabled,p_iDisplayType)
	dim rs
	dim sSelected
	dim sSQL
	dim bDisplayValue
	dim iCurrentCourseID, iPreviousCourseID
	dim sDisabled
	
	set rs = p_oRS
	
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	response.write("<select name=""CourseID"" " & sDisabled & " " & p_sJSEvents & ">" & vbcrlf)	
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select The Course</option>" & vbcrlf)	
	end if
	
	do while not rs.eof		
		iCurrentCourseID = rs("CourseID")
		if (trim(iCurrentCourseID) <> trim(iPreviousCourseID)) then
			if (trim(p_iSelectedValue) = trim(rs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CourseID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		end if
		iPreviousCourseID = iCurrentCourseID
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
End Sub


' Name 	  : DisplayCourseNumDropDown
' Purpose : displays the course dropdown, with course numbers instead of names
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_bDisabled - sets whether or not the dropdown is disabled
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayCourseNumDropDown(p_iSelectedValue, p_bDisabled, p_iDisplayType)

	dim oConn
	dim oRs
	dim sSelected
	dim sSql
	dim bDisplayValue
	dim iCurrentCourseId, iPreviousCourseId
	dim sDisabled
	
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	set objConn = New HandyADO
	
	sSql = "SELECT CourseId, Name, Number FROM Courses " & _
		"WHERE OwnerStateId = '" & session("StateId") & "'"
	
	set oRs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	Response.Write("<select name=""CourseID"" " & sDisabled & ">" & vbCrLf)
	
	if (cint(p_iDisplayType) = 0) then
		Response.Write("<option value="""">ALL</option>" & vbCrLf)
	else
		Response.Write("<option value="""">Please Select The Course</option>" & vbCrLf)
	end if 
	
	do while not oRs.EOF
		iCurrentCourseId = oRs("CourseID")
		if (trim(iCurrentCourseId) <> trim(iPreviousCourseId)) then
			if (trim(p_iSelectedValue) = trim(oRs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if 
			
			Response.Write("<option value=""" & oRs("CourseID") & """ " & sSelected & ">Course " & oRs("Number") & "</option>" & vbCrLf)
		end if 
		iPreviousCourseId = iCurrentCourseId
		oRs.MoveNext
	loop
	
	Response.Write("</select>" & vbCrLf)
	
	set oRs = nothing
	
end sub
	

' Name 	  : DisplayCrossCertCourseDropDown
' Purpose : displays the cross cert course dropdown
' Inputs  : p_sDropDownName - the name of the dropdown
'		  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_oRS - the recordset to populate the dropdown 
'		  : p_sCourseIDs - the courseIDs the dropdown should filter out, optional
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayCrossCertCourseDropDown(p_sDropDownName,p_iSelectedValue,p_oRS,p_sCourseIDs,p_iDisplayType)
	dim rs
	dim sSelected
	dim sSQL
	dim iProgramID
	dim bDisplayValue
	
	set rs = p_oRS
	
	response.write("<select name=""" & p_sDropDownName & """>" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Course</option>")	
	end if
	
	do while not rs.eof		
		'true, if the dropdown should screen out Courses
		if (trim(p_sCourseIDs) <> "") then
			if (ValueInDelString(rs("CourseID"), p_sCourseIDs)) then  'located in this file
				bDisplayValue = false
			else
				bDisplayValue = true			
			end if 
		else
			bDisplayValue = true
		end if
		
		'display the course if set to true
		if bDisplayValue then
			if (trim(p_iSelectedValue) = trim(rs("CourseID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("CourseID") & """ " & sSelected & ">" & rs("Name") & "</option>" & vbcrlf)
		end if
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
End Sub

' Name : DisplayCrossCertificationTypesDropDown
' Purpose : displays the course types drop down
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_iDisplayType - 0 is for Search, 1 is for Assigning
'		    p_bCrossCertTypeDisabled - whether or not the dropdown should be disabled
' Outputs : prints the drop down to the screen
Sub DisplayCrossCertificationTypesDropDown(p_iSelectedValue,p_iDisplayType, p_bCrossCertTypeDisabled)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sDisabled

	set objConn = New HandyADO

	sSQL = "SELECT * FROM CrossCertificationTypes"

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	'Determin if dropdown should be disabled
	if (p_bCrossCertTypeDisabled) then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if
	
	response.write("<select name=""CrossCertType"" " & sDisabled & ">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name : DisplayCourseTypesDropDown
' Purpose : displays the course types drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'	       p_iSearchType - 0 for searching, 1 for assigning
' Outputs : prints the drop down to the screen
sub DisplayCourseTypesDropDown(p_iSelectedValue, p_iSearchType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM CourseTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CourseType"">" & vbcrlf)
	
	if p_iSearchType = 0 then 
		response.write("<option value="""">ALL</option>")
	end if 
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("typeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayCurrentExpiredDropDown
' Purpose : displays the Current/Expired drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		 : p_iDisplayType - 0 is for displaying the All option, 1 is for not showing the All Option
' Outputs : prints the drop down to the screen
sub DisplayCurrentExpiredDropDown(p_iSelectedValue,p_sJSEvents,p_iDisplayType)
	dim sSelected
	
	response.write("<select name=""CurrentExpired"" " & p_sJSEvents & ">" & vbcrlf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>")
	end if
	
	if trim(p_iSelectedValue) = "1" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if
	response.write("<option value=""1"" " & sSelected & ">Current</option>" & vbcrlf)

	if trim(p_iSelectedValue) = "0" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if	
	response.write("<option value=""0"" " & sSelected & ">Expired</option>" & vbcrlf)
		
	response.write("</select>" & vbcrlf)
end sub

' Name : DisplayLessonSectionsDropDown
' Purpose : displays the Lesson Sections drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		   p_iLessonID - the Lesson ID of the lesson 
'		 : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayLessonSectionsDropDown(p_iSelectedValue,p_iLessonID,p_bDisabled,p_iDisplayType)
	dim oCourseObj
	dim rs
	dim sSelected
	dim sSQL
	
	set oCourseObj = New Course
	oCourseObj.ConnectionString = application("sDataSourceName")
	
	'Retrieve Course Sections
	oCourseObj.LessonID = iLessonID
	set rs = oCourseObj.GetAllLessonSections()

	if p_bDisabled then
		response.write("<select name=""LessonSectionID"" DISABLED>" & vbcrlf)
	else
		response.write("<select name=""LessonSectionID"">" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Section</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("SectionID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("SectionID") & """ " & sSelected & ">" & rs("Title") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set oCourseObj = nothing
end sub

' Name : DisplayProgramDropDown
' Purpose : displays the programs drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayProgramDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM Programs Order By ProgramID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CourseProgram"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Program</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ProgramID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ProgramID") & """ " & sSelected & ">" & rs("name") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayReleaseTypeDropDown
' Purpose : displays the certificate release type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
Sub DisplayReleaseTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM CertificateReleaseTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""CertificateReleaseType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Release Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
End Sub

' Name: GetBranchName
' Purpose: Returns the full branch name given the branch ID
' Inputs: p_iBranchId
' Returns: Branch name
Function GetBranchName(p_iBranchId)

	if not (isNumeric(p_iBranchId)) or p_iBranchId = "" then
		
		GetBranchName = ""
		exit function
		
	end if
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = new HandyADO
	
	sSql = "SELECT Name FROM CompanyBranches WHERE BranchID = '" & p_iBranchID & "'"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	GetBranchName = oRs("Name")
	
	set oRs = nothing
	set oConn = nothing

End Function

' Name: GetStateName
' Purpose: Returns the full state name given the state ID
' Inputs: p_iStateId - state id
'		  p_bAbbrev - return abreviation instead of full name
' Returns: State name
Function GetStateName(p_iStateId, p_bAbbrev)

	if not (IsNumeric(p_iStateId)) or p_iStateId = "" then
	
		GetStateName = ""
		exit function
	
	end if

	dim oConn
	dim oRs
	dim sSql
	
	set oConn = new HandyADO
	
	sSql = "SELECT State, Abbrev FROM States WHERE StateID = '" & p_iStateID & "'"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if not oRs.EOF then 
		if p_bAbbrev then
			GetStateName = oRs("Abbrev")
		else
			GetStateName = oRs("State")
		end if
	end if 
	
	set oRs = nothing
	set oConn = nothing	

End Function


' Name: GetStateIdByAbbrev
' Purpose: Returns the state ID given the state abbreviation
' Inputs: p_sAbbrev - state abbreviation
' Returns: State ID
Function GetStateIdByAbbrev(p_sAbbrev)

	if p_sAbbrev = "" then
	
		GetStateIdByAbbrev = ""
		exit function
	
	end if

	dim oConn
	dim oRs
	dim sSql
	
	set oConn = new HandyADO
	
	sSql = "SELECT StateId, State FROM States WHERE Abbrev LIKE '" & ucase(p_sAbbrev) & "'"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if not oRs.EOF then 
		GetStateIdByAbbrev = oRs("StateId")
	end if 
	
	set oRs = nothing
	set oConn = nothing	

End Function


' Name: DisplaySavedReports
' Desc: Displays a dropdown list of saved reports for the current company
sub DisplaySavedReports(p_iSelectedValue, p_sFormName)

	dim oConn
	dim oRs
	dim sSql
	dim sSelected
	
	set oConn = new HandyADO
	
	sSql = "SELECT ReportId, Name FROM SavedReports ORDER BY Name"
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if p_sFormName <> "" then
		Response.Write("<select name=""" & p_sFormName & """ width=""5"">" & vbCrLf)
	else
		Response.Write("<select name=""SavedReport"" width=""5"">" & vbCrLf)
	end if

	do while not oRs.EOF
		if (trim(p_iSelectedValue) = trim(oRs("ReportId"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if

		Response.Write("<option value=""" & oRs("ReportId") & """ " & sSelected & ">" & oRs("Name") & "</option>" & vbCrLf)
		oRs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set oRs = nothing
	set oConn = nothing
	
end sub
		


' Name : DisplayStatesDropDown
' Purpose : displays the states drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning, 2 is for state code assignment dropdown, 
'							3 for Front End Calendar. 4 for the Billing Info screen (option values is the entire name of state)
'		 : p_sFormName - Name of the dropdown box placed
' Outputs : prints the drop down to the screen
Sub  DisplayStatesDropDown(p_iSelectedValue,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	
	'Name the dropdown box
	dim sFormName
	if p_sFormName = "" then
		if (cint(p_iDisplayType) = 3) then
			sFormName = "SearchState"
		else
			sFormName = "State"
		end if 
	else
		sFormName = p_sFormName
	end if 
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM " & application("TrainingProDBName") & ".dbo.States "
	
	sSQL = sSQL & "	Order By State "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if (cint(p_iDisplayType) = 3) then
		response.write("<select name=""" & sFormName & """ onchange=""this.form.submit();"">" & vbcrlf)
	else
		response.write("<select name=""" & sFormName & """>" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	elseif (cint(p_iDisplayType) = 3) then
		response.write("<option value="""">All States</option>")		
'	else
'		response.write("<option value="""">Please Select The State</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("StateID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if

		if p_iDisplayType = 2 then	
			Response.Write("<option value=""" & rs("Abbrev") & """ " & sSelected & ">" & rs("State") & "</option>" & vbCrLf)
		elseif p_iDisplayType = 4 then	
			Response.Write("<option value=""" & rs("State") & """ " & sSelected & ">" & rs("State") & "</option>" & vbCrLf)		
		else
			response.write("<option value=""" & rs("StateID") & """ " & sSelected & ">" & rs("state") & "</option>" & vbcrlf)
		end if
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayStatesListBox
' Purpose : displays the states list box
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning, 2 is for state code assignment dropdown, 
'		 : p_sFormName - Name of the dropdown box placed
' Outputs : prints the drop down to the screen
Sub  DisplayStatesListBox(p_sSelectedValue,p_iDisplayType,p_sFormName)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	
	p_sSelectedValue = "," & trim(p_sSelectedValue) & ","
	
	'Name the dropdown box
	dim sFormName
	if p_sFormName = "" then
		sFormName = "State"
	else
		sFormName = p_sFormName
	end if 
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM " & application("TrainingProDBName") & ".dbo.States "
	
	sSQL = sSQL & "	Order By State "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""" & sFormName & """ size=""6"" multiple>" & vbcrlf)	
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	end if
	
	do while not rs.eof
		if instr(1, p_sSelectedValue, "," & rs("StateId") & ",") > 0 then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if

		if p_iDisplayType = 2 then	
			Response.Write("<option value=""" & rs("Abbrev") & """ " & sSelected & ">" & rs("State") & "</option>" & vbCrLf)
		else
			response.write("<option value=""" & rs("StateID") & """ " & sSelected & ">" & rs("state") & "</option>" & vbcrlf)
		end if
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name: DisplayOrgTypesDropDown
' Purpose: Displays the org types list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayOrgTypesDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "OrgType"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM OrganizationTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("OrganizationTypeId") & """")
		
		if oRs("OrganizationTypeId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("OrganizationType") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name: DisplayLicenseStatusDropDown
' Purpose: Displays the license statuses list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayLicenseStatusDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "LicenseStatus"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM LicenseStatusTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("LicenseStatusID") & """")
		
		if oRs("LicenseStatusId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("LicenseStatus") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name: DisplayLicenseTypeDropDown
' Desc: Displays the license type list dropdown
' Inputs: p_iSelectedValue - the value the dropdown should be preselected to
'		  p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the selected form element
'		  p_bDisabled - Should the form be disabled?
sub DisplayLicenseTypeDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName, p_bDisabled)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "LicenseType"
	else
		sFormName = p_sFormName
	end if 
	
	'Is this form disabled?
	dim sDisabled
	if p_bDisabled then
		sDisabled = " DISABLED"
	else
		sDisabled = ""
	end if 
	
	Response.Write("<select name=""" & sFormName & """" & sDisabled & ">" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if 
	end if 

	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM LicenseTypes"
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF 
	
		Response.Write("<option value=""" & oRs("LicenseTypeId") & """")

		if oRs("LicenseTypeId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("LicenseType") & "</option>" & vbCrLf)
		
		oRs.MoveNext
		
	loop
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing
	
end sub


' Name: DisplayLicensingTypeDropDown
' Purpose: Displays the licensing type list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayLicensingTypeDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "LicensingType"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM LicensingTypes" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("LicensingTypeId") & """")
		
		if oRs("LicensingTypeId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("LicensingType") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name: DisplayDocStateDropDown
' Purpose: Displays the document status list dropdown
' Inputs: p_iSelectedValue - the value of the dropdown should be preselected to
'	      p_iDisplayType - 0 displays the all option, 1 does not
'		  p_sFormName - Name of the select form element
sub DisplayDocStateDropDown(p_iSelectedValue, p_iDisplayType, p_sFormName)

	'Get the form name
	dim sFormName
	if p_sFormName = "" then
		sFormName = "DocStateId"
	else
		sFormName = p_sFormName
	end if 


	Response.Write("<select name=""" & sFormName & """ >" & vbCrLf)
	
	if isnumeric(p_iDisplayType) then
		if cint(p_iDisplayType) = 0 then
			Response.Write("<option value="""">ALL</option>")
		end if
	end if 
	
	
	dim oConn
	dim oRs
	dim sSql
	
	set oConn = Server.CreateObject("ADODB.Connection")
	oConn.ConnectionString = application("sDataSourceName")
	oConn.Open
	
	sSql = "SELECT * FROM DocumentStates" 
	set oRs = oConn.Execute(sSql)
	
	
	do while not oRs.EOF
	
		Response.Write("<option value=""" & oRs("DocStateId") & """")
		
		if oRs("DocStateId") = p_iSelectedValue then
			Response.Write(" SELECTED")
		end if 
		
		Response.Write(">" & oRs("DocState") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	
	oConn.Close
	set oConn = nothing
	set oRs = nothing

end sub


' Name : DisplayStatusDropDown
' Purpose : displays the Status drop down
' Inputs  : p_iSelectedValue - the value the drop down should be preselected to
'		  : p_sJSEvents - a string of Javascript events that should process for this dropdown
'		  : p_iDisplayType - 0 is for displaying the All option, 1 is for not showing the All Option
' Outputs : prints the drop down to the screen
sub DisplayStatusDropDown(p_iSelectedValue,p_sJSEvents,p_iDisplayType)
	dim sSelected
	
	response.write("<select name=""Status"" " & p_sJSEvents & ">" & vbcrlf)
	
	if cint(p_iDisplayType) = 0 then
		response.write("<option value="""">ALL</option>")
	end if
	
	if trim(p_iSelectedValue) = "1" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if
	response.write("<option value=""1"" " & sSelected & ">Active</option>" & vbcrlf)

	if trim(p_iSelectedValue) = "0" then
		sSelected = " SELECTED"
	else
		sSelected = ""
	end if	
	response.write("<option value=""0"" " & sSelected & ">Disabled</option>" & vbcrlf)
		
	response.write("</select>" & vbcrlf)
end sub

' Name : DisplayTestTypeDropDown
' Purpose : displays the test types drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_bDisabled - sets whether or not the dropdown is disabled
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayTestTypeDropDown(p_iSelectedValue,p_bDisabled,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM TestTypes Order By TypeID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if p_bDisabled then
		response.write("<select name=""TestType"" DISABLED>" & vbcrlf)
	else
		response.write("<select name=""TestType""")
		
		if (cint(p_iDisplayType) <> 0) then
			response.write(" onChange='EnableDisableSections(this.form)'")
		end if
		
		response.write(">" & vbcrlf)	
	end if
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Type</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayTimeTypeDropDown
' Purpose : displays the Time Type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayTimeTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM TimeTypes "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	if (cint(p_iDisplayType) = 1) then
		sEvent = "onChange=EnableDisableTimedMinutes(this.form)"
	end if
	
	response.write("<select name=""TimeType"" " & sEvent & ">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name: DisplayUsersDropdown
' Purpose: Creates a dropdown to select from a list of a company's users
' Inputs: p_iSelectedValue, p_iCompanyId, p_iBranchId,
'		  p_iDisplayType: 0 for search
' Outputs: Prints the dropdown to the screen
sub DisplayUsersDropdown(p_iSelectedValue, p_iCompanyId, p_iBranchId, _
	p_iDisplayType)
	
	dim oConn
	dim oRs
	dim sSql
	
	dim bFirstClause
	bFirstClause = true
	
	dim sSelected 'Holds text to select an option
	
	sSql = "SELECT UserID, FirstName, LastName, UserName FROM Users "
	
	'Add Company search clause
	if (p_iCompanyId <> "") and (p_iCompanyId <> 0) then 
		
		if bFirstClause then
			bFirstClause = false
			sSql = sSql & " WHERE "
		else
			sSql = sSql & " AND "
		end if
			
		sSql = sSql & "CompanyID = '" & p_iCompanyId & "'" 
		
	end if
	
	'Add Branch search clause
	if (p_iBranchId <> "") and (p_iBranchId <> 0) then
	
		if bFirstClause then
			bFirstClause = false
			sSql = sSql & " WHERE "
		else
			sSql = sSql & " AND "
		end if
		
		sSql = sSql & "BranchID = '" & p_iBranchId & "'"
	
	end if
	
	sSql = sSql & " ORDER BY FirstName"
	
	set oConn = new HandyADO
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

	Response.Write("<select name=""Users"">" & vbCrLf)
	
	if p_iDisplayType = 0 then
		Response.Write("<option value="""">All Users</option>" & vbCrLf)
	end if
	
	do while not oRs.EOF
	
		if IsNumeric(p_iSelectedValue) then
			if oRs("UserID") = CINT(p_iSelectedValue) then
				sSelected = "SELECTED"
			end if
		else
			sSelected = ""
		end if
	
		Response.Write("<option value=""" & oRs("UserID") & """ " & _
			sSelected & ">" & oRs("FirstName") & " " & oRs("LastName") & _
			": " & oRs("UserName") & "</option>" & vbCrLf)
	
		oRs.MoveNext
	
	loop
	
	Response.Write("</select>" & vbCrLf)
	
	set oConn = nothing
	set oRs = nothing
	
end sub

' Name : DisplayUserStatusDropDown
' Purpose : displays the User Status drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayUserStatusDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM UserStatusTypes WHERE Display = 1 ORDER BY StatusID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""UserStatus"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
'		response.write("<option value="""">Please Select The User Status</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("StatusID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("StatusID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : DisplayUserTypeDropDown
' Purpose : displays the user Type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayUserTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM afxSecurityGroups ORDER BY GroupID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""GroupID"" onChange=""StatusChange()"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select The User Type</option>" & vbcrlf)
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("GroupID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("GroupID") & """ " & sSelected & ">" & rs("GroupName") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name : DisplayCompanyUserTypeDropDown
' Purpose : displays the user Type drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayCompanyUserTypeDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	dim sEvent
	set objConn = New HandyADO

	sSQL = "SELECT * FROM afxSecurityGroups ORDER BY GroupID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""GroupID"" onChange=""StatusChange()"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>" & vbcrlf)
	else
		response.write("<option value="""">Please Select The User Type</option>" & vbcrlf)
	end if
	
	do while not rs.eof
	
		if not rs("GroupId") = 1 then
		
			if (trim(p_iSelectedValue) = trim(rs("GroupID"))) then
				sSelected = " SELECTED"
			else
				sSelected = ""
			end if
	
			response.write("<option value=""" & rs("GroupID") & """ " & sSelected & ">" & rs("GroupName") & "</option>" & vbcrlf)
			
		end if 
			
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name: WriteBranchesDropdownJavascript 
' Purpose: Writes a javascript that allows a dropdown box to be changed with
'	the passed parameter - this can be used with a Companies dropdown box to
'	allow a dynamic Branches dropdown box that changes according to the
'	selected company. 
' Inputs: p_sFormName
' Outputs: Writes javascript to the file
sub WriteBranchesDropdownJavascript(p_sFormName)

	dim oConn
	dim oRs
	dim sSql
	
	if p_sFormName = "" then
		exit sub
	end if

	set oConn = new HandyADO
	
	sSql = "SELECT * FROM CompanyBranches ORDER BY CompanyID ASC"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	if not (oRs.EOF and oRs.BOF) then

		Response.Write("<script langauge=""JavaScript"">" & vbCrLf)
		Response.Write("function UpdateBranchList(company) {" & vbCrLf)

		dim iCompanyId
		dim bFirstCompany
		dim iOptionNum
		bFirstCompany = true
		iOptionNum = 1
				
			do while not oRs.EOF
			
				if oRs("CompanyID") <> iCompanyId then
					iCompanyId = oRs("CompanyID")
					
					if not bFirstCompany then
						Response.Write("	}" & vbCrLf)
					end if 
					bFirstCompany = false
					
					Response.Write("	if (company == " & iCompanyId & ") {" & vbCrLf)
					Response.Write("		document." & p_sFormName & ".Branch.length = 0;" & vbCrLf)
					Response.Write("		document." & p_sFormName & ".Branch.options[0] = new Option('No Branch', '');" & vbCrLf) 
					
					iOptionNum = 1
									
				end if
				
				Response.Write("		document." & p_sFormName & ".Branch.options[" & iOptionNum & "] = new Option('" & oRs("Name") & "','" & oRs("BranchID") & "');" & vbCrLf)
			
				iOptionNum = iOptionNum + 1
				oRs.MoveNext
			
			loop
			
		Response.Write("	} else {" & vbCrLf)
		Response.Write("		document." & p_sFormName & ".Branch.length = 0;" & vbCrLf)
		Response.Write("		document." & p_sFormName & ".Branch.options[0] = new Option('No Branch', '');" & vbCrLf) 
		Response.Write("	}" & vbCrLf)
		Response.Write("}" & vbCrLf)
		Response.Write("</script>" & vbCrLf)
		
	end if

end sub


' Name: DisplayCouponTypesDropDown
' Purpose: Displays a dropdown box with %/$ coupon type options.  Exciting.
' Inputs: p_iDisplayType - 0 for search, 1 for assignment.
' Outputs: Prints the aforementioned dropdown box.
sub DisplayCouponTypesDropDown(p_sSelect, p_iDisplayType)

	Response.Write("<select name=""CouponType"">" & vbCrLf)
	
	if p_iDisplayType = 0 then
		Response.Write("<option value="""">ALL</option>" & vbCrLf)
	end if
	
	dim sSelected
	
	if p_sSelect = "$" then sSelected = "SELECTED" else sSelected = ""
	Response.Write("<option value=""$""" & sSelected & ">$</option>" & vbCrLf)
	if p_sSelect = "%" then sSelected = "SELECTED" else sSelected = ""
	Response.Write("<option value=""%""" & sSelected & ">%</option>" & vbCrLf)
	
	Response.Write("</select>" & vbCrLf)		

end sub


' Name : DisplayPLPTypesDropDown
' Purpose : displays a drop down box filled with all existing PLPsTypes names
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayPLPTypesDropDown(p_iSelectedValue,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	set objConn = New HandyADO

	sSQL = "SELECT * FROM PLPTypes "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""PLPType"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("TypeID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("TypeID") & """ " & sSelected & ">" & rs("Type") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub


' Name: DisplayCandidateStatusDropdown(p_iSelectedValue)
' Purpose: Display the select dropdown box for candidate credit status types in the database.
' Inputs: p_iSelectedValue - value to preselect
'		  p_iDisplayType - 0 - default
'						   1 - display ALL option.
'						   2 - display "Select" first option
' Outputs: prints dropdown to screen
sub DisplayCreditStatusDropdown(p_iSelectedValue, p_iDisplayType)

	dim oConn
	dim oRs
	dim sSql
	dim sSelected
	
	set oConn = new HandyADO
	
	sSql = "SELECT CreditStatus, CreditStatusID FROM CreditStatusTypes"
	
	set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)
	
	Response.Write("<select name=""CreditStatus"">" & vbCrLf)
	
	if cint(p_iDisplayType) = 1 then
		Response.Write("<option value="""">ALL</option>")
	elseif cint(p_iDisplayType) = 2 then
		Response.Write("<option value="""">Select a Credit Status</option>")
	end if
	
	do while not oRs.EOF
	
		if trim(p_iSelectedValue) = trim(oRs("CreditStatusID")) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		Response.Write("<option value=""" & oRs("CreditStatusID") & """ " & sSelected & ">" & oRs("CreditStatus") & "</option>" & vbCrLf)
		
		oRs.MoveNext
		
	loop

	Response.Write("</select>" & vbCrLf)
	
	set oRs = nothing
	set oConn = nothing

end sub

' DisplayVerificationMethodDropDown(p_iSelectedValue,p_iDisplayType)
' Name : DisplayVerificationMethodDropDown
' Purpose : displays the verification method drop down
' Inputs : p_iSelectedValue - the value the drop down should be preselected to
'		 : p_sUserType - the type of user the drop down should be filtered for, also used in the drop down name
'		 : p_iDisplayType - 0 is for Search, 1 is for Assigning
' Outputs : prints the drop down to the screen
sub DisplayVerificationMethodDropDown(p_iSelectedValue,p_sUserType,p_iDisplayType)
	dim objConn
	dim rs
	dim sSelected
	dim sSQL
	
	set objConn = New HandyADO

	sSQL = "SELECT * FROM VerificationMethods " & _
			"WHERE UserType = 'All' or UserType = '" & p_sUserType & "' " & _
			"ORDER BY ID "

	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)
	
	response.write("<select name=""" & p_sUserType & "UserVerificationID"">" & vbcrlf)
	
	if (cint(p_iDisplayType) = 0) then
		response.write("<option value="""">ALL</option>")
	else
		response.write("<option value="""">Please Select The Verification Method</option>")	
	end if
	
	do while not rs.eof
		if (trim(p_iSelectedValue) = trim(rs("ID"))) then
			sSelected = " SELECTED"
		else
			sSelected = ""
		end if
	
		response.write("<option value=""" & rs("ID") & """ " & sSelected & ">" & rs("method") & "</option>" & vbcrlf)
		rs.MoveNext
	loop
	
	response.write("</select>" & vbcrlf)
	
	set rs = nothing
	set objConn = nothing
end sub

' Name : ValueInDelString
' Purpose : determines if a value is contained in a string of comma delimited elements
' Inputs : p_sValue - the value to check for
'		 : p_sElementString - string of elements delimited by commas
' Outputs : true if the value was in the string, false if not
function ValueInDelString(p_sValue,p_sElementString)
	dim sCurrentValue
	dim aElementArray
	
	aElementArray = split(p_sElementString, ", ")
	
	for each sCurrentValue in aElementArray
		if (trim(sCurrentValue) = trim(p_sValue)) then
			ValueInDelString = true
			exit function
		end if
	next
	
	ValueInDelString = false
end function
%>

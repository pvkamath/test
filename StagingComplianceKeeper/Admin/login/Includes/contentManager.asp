<!-- #include virtual = "/admin/login/includes/contentReport.Class.asp" ------------------------>
<script language="vbscript" runat="server">
dim g_iContentID 'as integer
dim g_sText 'as string
dim g_sHeadline 'as string
dim g_sImagePath 'as string
dim g_sImageName 'as string
dim g_dPostingDate 'as String
</script>
<script language="vbscript" runat="server">
sub setGlobalContentData(p_sKeywords)
	on error resume next
	dim oContentReport 'as object
	dim rsContentList 'as object
	dim rsContentDetail 'as object
	dim dPostingDate 'as date
	dim bDataReturned 'as boolean
		
		g_iContentID = ""
		g_sHeadline = ""
		g_sText = ""
		g_sImagePath = ""
		g_sImagePath = ""
		g_sImageName = ""
		g_dPostingDate = ""
		
		
		set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
		oContentReport.ConnectionString = application("sXIGConnectionString")

		oContentReport.Keywords = p_sKeywords
		
		bDataReturned = oContentReport.getContentList("Active","")
		
		if bDataReturned then

			set rsContentList = oContentReport.ContentListRS
			rsContentList.Open
					
			if not rsContentList.EOF then

				iContentID = rsContentList.fields("NewsItemID").Value
				bDataReturned = oContentReport.getContent(iContentID)
				if bDataReturned then
				
					set rsContentDetail = oContentReport.ContentRS
					rsContentDetail.Open
				
					g_iContentID = rsContentDetail.fields("NewsItemID").Value
					g_sHeadline = rsContentDetail.fields("headline").Value
					g_sText = replace(rsContentDetail.fields("BodyText").Value,"''","""")
					g_sImagePath = rsContentDetail.fields("mediaPath").Value
					if isNull(g_sImagePath) then
						g_sImagePath = ""
					end if
					g_sImageName = rsContentDetail.fields("mediaName").Value
					if isNull(g_sImageName) then
						g_sImageName = ""
					end if		
					g_dPostingDate = rsContentDetail.fields("PostingDate").value		
					rsContentDetail.Close
				end if				
			end if
		end if	

end sub

sub printNewsSection(p_sKeywords)
on error resume next
dim oContentReport 'as object
dim rsContentList 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
dim iContentID 'as integer
dim	sHeadline 'as string
dim sURL 'as string
	
	
	set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
	oContentReport.ConnectionString = application("sXIGConnectionString")

	oContentReport.Keywords = p_sKeywords
	
	bDataReturned = oContentReport.getContentList("Active","")
	if bDataReturned then
		set rsContentList = oContentReport.ContentListRS
		rsContentList.Open
				
		do while not rsContentList.EOF 
		
			iContentID = rsContentList.fields("NewsItemID").Value
			sHeadline = rsContentList.fields("headline").Value
			select case ucase(p_sKeywords)
				case "IN THE NEWS"
					sURL = "displayNews.asp"
				case "RECENT PROJECTS"
					sURL = "displayRecentProjects.asp"
				case else
					sURL = "displayNews.asp"
			end select
			Response.Write("<a href=/" & sURL & "?ID=" & iContentID & ">" & sHeadline & "</a><br><img src=""media/images/clear.gif"" width=""10"" height=""10"" alt="""" border=""0""><br>")

			rsContentList.MoveNext
		loop
	end if	

end sub

</script>
<%
'===============================================================================================================
' Class: Content Report
' Description: Content Report Class
' Create Date: 2/15/02
' Current Version: 1.0
' Author(s): Joseph Lascola
' References to other DLL's: newsFx2
' References other classes : 
' ----------------------
' Properties:
'	- ConnectionString
'	- BodyText, HeadLine, ByLine, PostingDate
'	- WrittenDate, Source, Abstract, Status
'	- Keywords, AuthorID, ContentID, SiteID
'	- ErrorDescription
'	- Message
'	- ContentRS, ContentListRS
'	- Connection, MediaRS
'	- TruncateLength
'
' Methods:
'	- ClearContent
'	- GetContent
'	- GetContentList
'	- GetMedia
'	- SearchContent
'	- SetConnection
'===============================================================================================================
Class ContentReport

	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString
	dim g_sBodyText, g_sHeadLine, g_sByLine, g_sPostingDate
	dim g_sWrittenDate, g_sSource, g_sAbstract, g_sStatus
	dim g_sKeywords, g_iAuthorID, g_iContentID, g_iSiteID
	dim g_sErrorDescription
	dim g_oContentRS, g_oContentListRS
	dim g_oConnection
	dim g_sMessage
	dim g_oMediaRS
	
	' PUBLIC METHODS ===========================================================================================
		
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContent
	' Description:		Set the recordset for the contentid passed
	' Inputs:			p_iContentID - Contentid for content that will be set in the recordset
	' Output:			true - Successful
	'					false - unSuccessful
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContent(p_iContentId)
	'on error resume next
	dim sSQL 'as string
	dim rsReport 'as object
	dim bReturn 'as boolean
	
		bReturn = false
			
		'call clearContent() 'subroutine found below
		
		call setConnection()
		
		set rsReport = Server.CreateObject("ADODB.Recordset")

		'check if a content id was passed
		if len(p_iContentID) > 0 and isNumeric(p_iContentID) then
		
			if len(g_sErrorDescription) = 0 then
				set rsReport.ActiveConnection = g_oConnection
		
		
				sSQL = "SELECT NI.NewsItemID, NI.Headline, NI.Byline, NI.PostingDate, NI.WrittenDate, NI.Source, " & _
						"NI.Abstract , NI.Keywords, NI.Status, NI.Revision, NB.BodyID, NB.BodyText, NA.AuthorID, NA.LastName, NA.FirstName, NI.Subject,  " & _
						"	 NM.MediaID , NM.MediaType, NM.MediaPath, NM.MediaName, NM.AltTag, NM.Caption, NM.Credit, NC.CategoryID, L.URL, L.LinkID  " & _
				        "    FROM (afxNewsItems as NI  " & _
				        "    inner join afxNewsBody as NB ON (NI.NewsItemID = NB.NewsItemID))  " & _
				        "    INNER JOIN afxNewsAuthorItem as NAI ON (NI.NewsItemID = NAI.NewsItemID) " & _ 
				        "    Left Outer JOIN afxNewsAuthors as NA ON (NAI.AuthorID = NA.AuthorID)  " & _
				        "    LEFT OUTER JOIN afxNewsKeywordsX as NK ON (NI.NewsItemID = NK.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxNewsMedia as NM ON (NI.NewsItemID = NM.NewsItemID AND NM.mediaType='I')  " & _
				        "    LEFT OUTER JOIN afxNewsSitesX as NSX ON (NI.NewsItemID = NSX.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxNewsCategoriesX as NC ON (NI.NewsItemID = NC.NewsItemID) " & _ 
				        "    LEFT OUTER JOIN afxNewsLinksX as NL ON (NI.NewsItemID = NL.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxLinks as L ON (NL.LinkID = L.LinkID)  " & _
				        "    Where (NI.NewsItemID = " & p_iContentID & ")  " & _
						"ORDER BY NI.PostingDate DESC "

					'Get the Content back
					rsReport.Open sSQL

					if not rsReport.EOF then
						set g_oContentRS = rsReport

						bReturn = true					
					else
						g_sMessage = "No Data was found"
					end if
		
					rsReport.Close
			end if			
		else
			g_sErrorDescription = "A content id is needed"
			bReturn = false
		end if		
		
		set rsReport = Nothing
		set oNewsReport = Nothing
		
		getContent = bReturn
	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getContentList
	' Description:		
	' Required Property : Keyword   -   Needed to pull back the specific list. 
	'									If none then all content will be returned in a recordset stored in a property
	' Inputs:			p_iOrderBy	-	This is the integer for which column the data should be sorted.
	'					p_sStatus - is the status to pull back ("Archived","Active","Pending")					
	' Output:			
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getContentList(p_sStatus, p_iOrderBy)
	'on error resume next
	dim oNewsSearch
	dim rsSearch
	dim bReturn
	dim sKeywordSplit

		bReturn = false
		call setConnection()
		
		set rsSearch = Server.CreateObject("ADODB.Recordset")
		
		
		
		if len(g_sErrorDescription) = 0 then
		
			set rsSearch.ActiveConnection = g_oConnection
			
			if len(g_sKeywords) > 0 then
				sSQL = "SELECT NI.NewsItemID, NI.Headline, NI.Byline, NI.PostingDate, NI.WrittenDate, NI.Source, " & _
						"NI.Abstract , NI.Keywords, NI.Status, NI.Revision, NB.BodyID, NB.BodyText, NA.AuthorID, NA.LastName, NA.FirstName,  " & _
						"NM.MediaID , NM.MediaType, NM.MediaPath, NM.MediaName, NM.AltTag, NM.Caption, NM.Credit  " & _
						"FROM (afxNewsItems as NI  " & _
						"LEFT OUTER JOIN afxNewsBody as NB ON (NI.NewsItemID = NB.NewsItemID))  " & _
						"INNER JOIN afxNewsAuthorItem as NAI ON (NI.NewsItemID = NAI.NewsItemID)  " & _
						"INNER JOIN afxNewsSitesX as NSX ON (NI.NewsItemID = NSX.NewsItemID)  " & _
						"LEFT OUTER JOIN afxNewsAuthors as NA ON (NAI.AuthorID = NA.AuthorID)  " & _
						"LEFT OUTER JOIN afxNewsMedia as NM ON (NI.NewsItemID = NM.NewsItemID AND NM.mediaType='I')  " & _
						"LEFT OUTER JOIN afxNewsKeywordsX as NK ON (NI.NewsItemID = NK.NewsItemID)  " & _
						"LEFT OUTER JOIN afxKeywords as AK ON (NK.KeywordID = AK.KeywordID)  " & _
						"WHERE NI.NewsItemID IN ( SELECT AK.NewsItemID FROM afxNewsKeywordsX AK, afxKeywords K WHERE  " & _
						"K.KeywordID = AK.KeywordID And ( "
				
				'ADD ABILITY FOR AN ARRAY OF KEYWORDS
				sKeywordSplit = split(g_sKeywords,",")
				for i = 0 to  ubound(sKeywordSplit)
					'oNewsSearch.AddKeywordSearch(sKeywordSplit(i))
					sSQL = sSQL & "	K.Keyword = '" & sKeywordSplit(i) & "' "
				next
				
				sSQL = sSQL & ") AND AK.SiteID = '1') AND NI.STATUS = '" & p_sStatus & "' " 
				
				if len(p_iOrderBy) = 0 then
					sSQL = sSQL & "ORDER BY NI.PostingDate desc"
				else
					sSQL = sSQL & "ORDER BY " & p_iOrderBy 
				end if


				'call clearContent() 'subroutine found below

				'Get the Content back
				rsSearch.Open sSQL
				
				if not rsSearch.EOF then
					set g_oContentListRS = rsSearch

					bReturn = true					
				else
					g_sMessage = "No Data was found"
				end if
		
				rsSearch.Close
			else
				g_sErrorDescription = "Keyword Must be Supplied"
			end if
		end if
		
		set rsSearch = Nothing
		set oNewsSearch = Nothing
		
		getContentList = bReturn

	end function



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				clearContent
	' Description:		Clears all properties except the connetionstring
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub clearContent()
		g_sBodyText = "" 
		g_sHeadLine = "" 
		g_sByLine = "" 
		g_sPostingDate = ""
		g_sWrittenDate = "" 
		g_sSource = "" 
		g_sAbstract = "" 
		g_sStatus = ""
		g_sKeywords = "" 
		g_iAuthorID = "" 
		g_iContentID = "" 
		g_iSiteID = ""
		g_sErrorDescription = ""
		g_sMessage = ""
		
		if isObject(g_oContentRS) then
			g_oContentRS.Close
			set g_oContentRS = nothing
		end if

		if isObject(g_oContentListRS) then
			g_oContentListRS.Close
			set g_oContentListRS = nothing
		end if	
	end sub



	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				getMedia()
	' Description:		This function returns a recordset of all the media for the content id
	' Pre-conditions:	g_iContentID is a Required field
	'		Required:	g_oConnection
	' Inputs:			
	' Outputs:			
	' Returns:			Sets g_oMediaRS
	' --------------------------------------------------------------------------------------------------------------------------------
	public function getMedia()
	dim rsMedia
	dim sSQL
	dim bReturn 
	
		bReturn = false
	
		call setConnection()
	
		set rsMedia = Server.CreateObject("ADODB.Recordset")
		set rsMedia.ActiveConnection = g_oConnection
		
		if len(g_sErrorDescription) = 0 then		
		
			if len(g_iContentID) > 0 then
				sSQL = "SELECT mediaName, mediaPath, mediaType " & _
						"FROM afxNewsMedia " & _
						"WHERE newsItemID = " & g_iContentID

				'Run query to return data
				rsMedia.Open sSQL

				if not rsMedia.EOF then
					set g_oMediaRS = rsMedia

					bReturn = true					
				else
					g_sMessage = "No Data was found"
				end if
			else
				g_sErrorDescription = "No Content ID was supplied"			
			end if
		else
			g_sErrorDescription = "No DataSource was supplied"
		end if		
		
		set rsMedia = Nothing
		
		getMedia = bReturn
	end function
		
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				TruncateLength
	' Description:		Cuts the length of the passed article down to the passed length
	' Inputs:			
	'	Default Parameters : TheStory - The Text that you want to change
	'						 MaxChars - The number of characters to return
	' Output:			The truncated story.
	' --------------------------------------------------------------------------------------------------------------------------------
	Function TruncateLength(TheStory, MaxChars) 
		Dim newline, tmpStr 'As String
		Dim Pos 'As Integer

		'Line Break Character
		newline = Chr(13) & Chr(10)

		' Check for Null value
		If (IsNull(TheStory)) Then
		    TruncateLength = ""
		    Exit Function
		End If

		' If article is shorter than max chars than no processing has to be done
		If (Len(TheStory) <= MaxChars) Then
		    TruncateLength = Replace(TheStory, newline, "<BR>")
		    Exit Function
		End If

		'truncate story to max chars
		'if the max chars are in the middle of a word, then truncate to the last blank space
		TheStory = Left(TheStory, MaxChars)
		I = MaxChars
		Do While ((I >= 0) And (Right(TheStory, 1) <> " ") And (Right(TheStory, 1) <> ""))
		    I = I - 1
		    TheStory = Left(TheStory, I)
		Loop

		'If there is a newline in the last 20 Chars, truncate story to the first occurrence
		tmpStr = Right(TheStory, 20)
		Pos = InStr(tmpStr, newline)

		If Pos > 0 Then
		    I = Len(TheStory)
		    TheStory = Left(TheStory, I - (20 - Pos))
		End If

		TruncateLength = Replace(TheStory, newline, "<BR>")

	End Function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				SearchContent
	' Description:		Returns a recordset of content that matches search criteria
	' Inputs:			p_sSearchText, p_bSearchHeadline, p_bSearchBody, p_iSearchArchive
	' Output:			True - successful, False - unsuccessful
	' --------------------------------------------------------------------------------------------------------------------------------
	public function SearchContent(p_sSearchText, p_bSearchHeadline, p_bSearchBody, p_sStatus)
		'on error resume next
		dim sSQL 'as string
		dim rsReport 'as object
		dim bReturn 'as boolean
	
		bReturn = false
			
		call setConnection()
		
		set rsReport = Server.CreateObject("ADODB.Recordset")

		'check if text was passed in to search for
		if p_sSearchText <> ""  then
		
			if len(g_sErrorDescription) = 0 then
				set rsReport.ActiveConnection = g_oConnection
		
		
				sSQL = "SELECT NI.NewsItemID, NI.Headline, NI.Byline, NI.PostingDate, NI.WrittenDate, NI.Source, " & _
						"NI.Abstract , NI.Keywords, NI.Status, NI.Revision, NB.BodyID, NB.BodyText, NA.AuthorID, NA.LastName, NA.FirstName, NI.Subject,  " & _
						"	 NM.MediaID , NM.MediaType, NM.MediaPath, NM.MediaName, NM.AltTag, NM.Caption, NM.Credit, NC.CategoryID, L.URL, L.LinkID  " & _
				        "    FROM (afxNewsItems as NI  " & _
				        "    inner join afxNewsBody as NB ON (NI.NewsItemID = NB.NewsItemID))  " & _
				        "    INNER JOIN afxNewsAuthorItem as NAI ON (NI.NewsItemID = NAI.NewsItemID) " & _ 
				        "    Left Outer JOIN afxNewsAuthors as NA ON (NAI.AuthorID = NA.AuthorID)  " & _
				        "    LEFT OUTER JOIN afxNewsKeywordsX as NK ON (NI.NewsItemID = NK.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxNewsMedia as NM ON (NI.NewsItemID = NM.NewsItemID AND NM.mediaType='I')  " & _
				        "    LEFT OUTER JOIN afxNewsSitesX as NSX ON (NI.NewsItemID = NSX.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxNewsCategoriesX as NC ON (NI.NewsItemID = NC.NewsItemID) " & _ 
				        "    LEFT OUTER JOIN afxNewsLinksX as NL ON (NI.NewsItemID = NL.NewsItemID)  " & _
				        "    LEFT OUTER JOIN afxLinks as L ON (NL.LinkID = L.LinkID)  " &_
						" WHERE 1 = 1 "
				if p_bSearchHeadline then
					sSQL = sSQL & " AND Headline LIKE '% " & ScrubForSQL(p_sSearchText) & "%' " 
				end if
				if p_bSearchBody then
					sSQL = sSQL & " AND BodyText LIKE '% " & ScrubForSQL(p_sSearchText) & "%' " 
				end if
				if p_sStatus <> "" then
					sSQL = sSQL & " AND status = '" & p_sStatus & "'"
				end if
				sSQL = sSQL & " ORDER BY NI.PostingDate DESC "

				'Get the Content back
				rsReport.Open sSQL

				if not rsReport.EOF then
					set g_oContentRS = rsReport

					bReturn = true					
				else
					g_sMessage = "No Data was found"
				end if
	
				rsReport.Close
			end if			
		else
			g_sErrorDescription = "Search text is needed"
			bReturn = false
		end if		
		
		set rsReport = Nothing
		set oNewsReport = Nothing
		
		SearchContent = bReturn
	end function

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				setConnection
	' Description:		Set The connection if one does not exist
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	private sub setConnection()
		if not isObject(g_oConnection) then
			set g_oConnection = server.CreateObject("ADODB.Connection")
			if len(g_sConnectionString) = 0 then
				g_sErrorDescription = "No Connection String was supplied"
			else
				g_oConnection.ConnectionString = g_sConnectionString
			end if
			g_oConnection.Open
		end if
	end sub

	' PRIVATE METHODS ==========================================================================================
	Private Sub Class_Initialize()
		' Enter code you want to run on Object Startup here.
	End Sub

	Private Sub Class_Terminate()
		' Enter code you want to run on Object Termination here.
	End Sub

	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined
	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	Public Property Let BodyText(p_sBodyText)
		g_sBodyText = p_sBodyText
	End Property

	Public Property Get BodyText()
		BodyText = g_sBodyText
	End Property

	Public Property Let HeadLine(p_sHeadLine)
		g_sHeadLine = p_sHeadLine
	End Property

	Public Property Get HeadLine()
		HeadLine = g_sHeadLine
	End Property

	Public Property Let ByLine(p_sByLine)
		g_sByLine = p_sByLine
	End Property

	Public Property Get ByLine()
		ByLine = g_sByLine
	End Property

	Public Property Let PostingDate(p_sPostingDate)
		g_sPostingDate = p_sPostingDate
	End Property

	Public Property Get PostingDate()
		PostingDate = g_sPostingDate
	End Property

	Public Property Let WrittenDate(p_sWrittenDate)
		g_sWrittenDate = p_sWrittenDate
	End Property

	Public Property Get WrittenDate()
		WrittenDate = g_sWrittenDate
	End Property

	Public Property Let Source(p_sSource)
		g_sSource = p_sSource
	End Property

	Public Property Get Source()
		Source = g_sSource
	End Property

	Public Property Let Abstract(p_sAbstract)
		g_sAbstract = p_sAbstract
	End Property

	Public Property Get Abstract()
		Abstract = g_sAbstract
	End Property

	Public Property Let Status(p_sStatus)
		g_sStatus = p_sStatus
	End Property

	Public Property Get Status()
		Status = g_sStatus
	End Property

	Public Property Let Keywords(p_sKeywords)
		g_sKeywords = p_sKeywords
	End Property

	Public Property Get Keywords()
		Keywords = g_sKeywords
	End Property

	Public Property Let AuthorID(p_iAuthorID)
		g_iAuthorID = p_iAuthorID
	End Property

	Public Property Get AuthorID()
		AuthorID = g_iAuthorID
	End Property

	Public Property Let ContentID(p_iContentID)
		g_iContentID = p_iContentID
	End Property

	Public Property Get ContentID()
		ContentID = g_iContentID
	End Property


	Public Property Let SiteID(p_iSiteID)
		g_iSiteID = p_iSiteID
	End Property

	Public Property Get SiteID()
		SiteID = g_iSiteID
	End Property

	Public Property Let ErrorDescription(p_sErrorDescription)
		g_sErrorDescription = p_sErrorDescription
	End Property

	Public Property Get ErrorDescription()
		ErrorDescription = g_sErrorDescription
	End Property

	Public Property Let Message(p_sMessage)
		g_sMessage = p_sMessage
	End Property

	Public Property Get Message()
		Message = g_sMessage
	End Property

	Public Property Set ContentRS(p_oContentRS)
		set g_oContentRS = p_oContentRS
	End Property

	Public Property Get ContentRS()
		set ContentRS = g_oContentRS
	End Property	

	Public Property Set ContentListRS(p_oContentListRS)
		set g_oContentListRS = p_oContentListRS
	End Property

	Public Property Get ContentListRS()
		set ContentListRS = g_oContentListRS
	End Property	

	Private Property Set Connection(p_oConnection)
		set g_oConnection = p_oConnection
	End Property

	Private Property Get Connection()
		set Connection = g_oConnection
	End Property

	Public Property Set MediaRS(p_oMediaRS)
		set g_oMediaRS = p_oMediaRS
	End Property

	Public Property Get MediaRS()
		set MediaRS = g_oMediaRS
	End Property
	'===========================================================================================================
End Class
%>
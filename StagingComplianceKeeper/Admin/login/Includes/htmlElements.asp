<%
	'--------------------------------------------------------------------------------------------------
	sub BeginHead(p_sTitle, p_bUsesFormValidation, p_nRefreshTime, p_sRefreshURL)
	
		response.write("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">")
		response.write("<html>")
		
		' Begin head tag:
		response.write("<head>" & vbCrLf)
		response.write PrintTextFile(Application("sDynWebroot") & "includes/designShellHeadInformation.asp")
		response.write("<title>" & Application("sSiteName") & ": " & p_sTitle & "</title>" & vbCrLf)
		
		' Redirect if refresh url and refresh time are specified parameters
		if p_nRefreshTime <> "" and p_sRefreshURL <> "" then
			' Print Meta Refresh Info
			response.write("<META HTTP-EQUIV=""Refresh"" " &_
							"CONTENT=""" & p_nRefreshTime & ";" &_
							"URL=" & p_sRefreshURL & """>" & vbCrLf)
		end if
		
		
		response.write("<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/")
		if Session("browser") = "IE" then
			response.write Application("sDefaultIEStyle")
		else
			response.write Application("sDefaultNSStyle")
		end if
		response.write(""">" & vbCrLf)
		Response.Write("<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/Style.css"">")	
		response.write("<script language=""JavaScript"" src=""/admin/login/includes/common.js""></script>" & vbCrLf)

		if p_bUsesFormValidation then response.write("<script language=""JavaScript"" src=""" & Application("sStandardFormValidationLibrary") & """></script>")
		
		' Add in Author, Company, etc.
	
	end sub
	
	'--------------------------------------------------------------------------------------------------
	sub FinishHeadBeginBody(bShowBackground, bTrimMargins, bShowMenus)
		response.write("</head>" & vbCrLf)
		response.write("<body bgcolor=""#FFFFFF""")

		' Print Javascript References:
		response.write(" onLoad=""JavaScript:")
		response.write("InitializePage();")
		'if bShowMenus then response.write("createMenus();")
		if bShowMenus then response.write("return true;")
		response.write("""")
		if bShowMenus then response.write(" onResize=""Javascript:if (isNS4) resizeHandler()""")


		if bShowBackground then
			response.write("background=""/admin/login/media/images/admin-login-bgr-B.jpg"" style=""background-repeat: no-repeat""")
		end if
		
		if bTrimMargins then
			'response.write("  style=""margin-top:0;margin-left:0;margin-right:0"" ")
			response.write(" class=""" & Application("sDefaultBodyStylesheetClass") & """")
			response.write(" style=""background-repeat:repeat-x"" rightmargin=""0"" leftmargin=""0"" bottommargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0""")
		end if


		'Check to see if this page needs the "onSelect" tag for the text formatting
		strPageName = Request.ServerVariables("PATH_INFO")
		if (instr(1,strPageName, "AddArticleGen") > 0) or (instr(1,strPageName, "EditArticle") > 0) then
			strOnSelect = "DisableFormatting()"
		else
			strOnSelect = ""
		end if
		
		response.write(" onSelectStart=""" & strOnSelect & """")
		response.write(">")
	end sub
	
	'--------------------------------------------------------------------------------------------------
	sub EndPage()
		response.write("</body>" & vbCrLf & "</html>")
	end sub
	
	' ------------------------------------------------------------------
	sub PrintMetaTags
		dim sSQL, rs_tmp
		
		' Print Site Description:
		sSQL = "select * from site_setting where UCase(key_name) = 'DESCRIPTION'"
		set rs_tmp = objDBConn.Execute(sSQL)
		if not rs_tmp.eof then
			response.write(vbCrLf & "<meta name=""description"" content=""" & replace(rs_tmp("key_value"), """", vbBlank) & """>" & vbCrLf)
		end if
		rs_tmp.Close
		
		' Print Keywords:
		sSQL = "select * from site_keyword order by keyword"
		set rs_tmp = objDBConn.Execute(sSQL)
		
		response.write("<meta name=""keywords"" content=""")
		
		do while not rs_tmp.eof
			response.write(replace(rs_tmp("keyword"), """", vbBlank))
			rs_tmp.MoveNext
			
			if not rs_tmp.eof then response.write(", ")
		Loop
		
		response.write(""">")
		
		' Deinit Objects:
		rs_tmp.Close
		set rs_tmp = Nothing
	end sub
	

%>

<script language="javascript">
<!-- // Begin IMAGE rollovers

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {		
		nav_home = newImage("/admin/login/media/images/nav-home-h.gif");
		nav_cm = newImage("/admin/login/media/images/nav-cm-h.gif");
		nav_inventory = newImage("/admin/login/media/images/nav-inventory-h.gif");
		nav_coupons = newImage("/admin/login/media/images/nav-coupons-h-blue.gif");
		nav_ads = newImage("/admin/login/media/images/nav-ads-h-blue.gif");
		nav_calendar = newImage("/admin/login/media/images/nav-calendar-h-blue.gif");
		nav_formmanager = newImage("/admin/login/media/images/nav-form-manager-h-blue.gif");
		nav_staff = newImage("/admin/login/media/images/nav-staff-h-blue.gif");
		nav_logout = newImage("/admin/login/media/images/nav-logout.gif");
		preloadFlag = true;
	}
}
preloadImages() 
// end IMAGE rollovers
// -->
</script>


<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>

<td><a href="/admin/login/admin.asp"
	onmouseover="changeImages('home', '/media/images/nav-home-h.gif'); return true;"
	onmouseout="changeImages('home', '/media/images/nav-home.gif'); return true;">
	<img name="home" src="media/images/nav-home.gif" width="54" height="24" alt="Home" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>

<%if application("bShowContentManager") = true then%>
<td><a href="/admin/ContentManager/"
	onmouseover="changeImages('cm', '/media/images/nav-cm-h.gif'); return true;"
	onmouseout="changeImages('cm', '/media/images/nav-cm.gif'); return true;">
	<img name="cm" src="media/images/nav-cm.gif" width="120" height="24" alt="Content Manager" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>


<%if application("bShowAutoID") = true then%>
<td><a href="/admin/AutoID/"
	onmouseover="changeImages('inventory', '/media/images/nav-inventory-h.gif'); return true;"
	onmouseout="changeImages('inventory', '/media/images/nav-inventory.gif'); return true;">
	<img name="inventory" src="media/images/nav-inventory.gif" width="75" height="24" alt="Inventory" border="0"></a></td>	
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<%if application("bShowWebCoupons") = true then%>
<td><a href="/admin/WebCoupons/"
	onmouseover="changeImages('coupons', '/media/images/nav-coupons-h-blue.gif'); return true;"
	onmouseout="changeImages('coupons', '/media/images/nav-coupons-blue.gif'); return true;">
	<img name="coupons" src="media/images/nav-coupons-blue.gif" width="69" height="24" alt="Coupons" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<%if application("bShowAds") = true then%>
<td><a href="/admin/Ads/"
	onmouseover="changeImages('ads', '/media/images/nav-ads-h-blue.gif'); return true;"
	onmouseout="changeImages('ads', '/media/images/nav-ads-blue.gif'); return true;">
	<img name="ads" src="media/images/nav-ads-blue.gif" width="42" height="24" alt="Ads" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<% if application("bShowCalendar") = true then %>	
<td><a href="/admin/CalendarFX/"
	onmouseover="changeImages('calendar', '/media/images/nav-calendar-h-blue.gif'); return true;"
	onmouseout="changeImages('calendar', '/media/images/nav-calendar-blue.gif'); return true;">
	<img name="calendar" src="media/images/nav-calendar-blue.gif" width="73" height="24" alt="Calendar" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<% if application("bShowFormManager") = true then %>
<td><a href="/admin/survey/surveysadmin.asp"
	onmouseover="changeImages('form', '/media/images/nav-form-manager-h-blue.gif'); return true;"
	onmouseout="changeImages('form', '/media/images/nav-form-manager-blue.gif'); return true;">
	<img name="form" src="media/images/nav-form-manager-blue.gif" width="104" height="24" alt="Form Manager" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<% if application("bShowMeetTheStaff") = true then %>	
<td><a href="/admin/MeetTheStaff/"
	onmouseover="changeImages('staff', '/media/images/nav-staff-h-blue.gif'); return true;"
	onmouseout="changeImages('staff', '/media/images/nav-staff-blue.gif'); return true;">
	<img name="staff" src="media/images/nav-staff-blue.gif" width="106" height="24" alt="Meet the Staff" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<% end if %>

<td><a href="/admin/login/logout.asp"
	onmouseover="changeImages('logout', '/media/images/nav-logout-h.gif'); return true;"
	onmouseout="changeImages('logout', '/media/images/nav-logout.gif'); return true;">
	<img name="logout" src="media/images/nav-logout.gif" width="67" height="24" alt="Logout" border="0"></a></td>
<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
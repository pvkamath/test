
<script language="javascript">
<!-- // Begin IMAGE rollovers

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {		
		nav_home = newImage("/admin/login/media/images/nav-home-h.gif");
		nav_cm = newImage("/admin/login/media/images/nav-cm-h.gif");
		nav_inventory = newImage("/admin/login/media/images/nav-inventory-h.gif");
		nav_coupons = newImage("/admin/login/media/images/nav-coupons-h-blue.gif");
		nav_ads = newImage("/admin/login/media/images/nav-ads-h-blue.gif");
		nav_calendar = newImage("/admin/login/media/images/nav-calendar-h-blue.gif");
		nav_formmanager = newImage("/admin/login/media/images/nav-form-manager-h-blue.gif");
		nav_staff = newImage("/admin/login/media/images/nav-staff-h-blue.gif");
		nav_leadtracker = newImage("/admin/login/media/images/nav-leadtracker-h-blue.gif");
		nav_scionpricing = newImage("/admin/login/media/images/nav-pricing-h.gif");		
		nav_dealerpromo = newImage("/admin/login/media/images/nav-dealerpromo-h-blue.gif");
		nav_logout = newImage("/admin/login/media/images/nav-logout.gif");
		nav_gallery = newImage("/admin/login/media/images/nav-gallery.gif");
		nav_medialibrary = newImage("/admin/login/media/images/nav-medialibrary-h.gif");
		nav_livestats = newImage("/admin/login/media/images/nav-liveStats-h.gif");
		nav_help = newImage("/admin/login/media/images/nav-help-h.gif");
		nav_states = newImage("/admin/login/media/images/nav-states-h.gif");
		nav_reports = newImage("/admin/login/media/images/nav-reports-h.gif");
		nav_private_label_pages = newImage("/admin/login/media/images/nav-private-label-pages-h.gif");
		nav_courses = newImage("/admin/login/media/images/nav-courses-h.gif");
		nav_companies = newImage("/admin/login/media/images/nav-companies-h.gif");
		nav_gen_settings = newImage("/admin/login/media/images/nav-gen-settings-h.gif");
		nav_newsletter = newImage("/admin/login/media/images/nav-newsletter-h.gif");
		preloadFlag = true;
	}
}
preloadImages() 
// end IMAGE rollovers
// -->
</script>

<table width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr bgcolor="6099D2">
		<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>

		<td><a href="/admin/login/admin.asp" target="_top"
			onmouseover="changeImages('home', '/admin/login/media/images/nav-home-h.gif'); return true;"
			onmouseout="changeImages('home', '/admin/login/media/images/nav-home.gif'); return true;">
			<img name="home" src="/admin/login/media/images/nav-home.gif" width="54" height="24" alt="Home" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>		
<% if cint(session("Access_Level")) < 2 then %>
		
		<!--
		<td nowrap><a href="/admin/Logos/" target="_top"
			onmouseover="changeImages('assoc', '/admin/login/media/images/nav-assoc-logos-h.gif'); return true;"
			onmouseout="changeImages('assoc', '/admin/login/media/images/nav-assoc-logos.gif'); return true;">
			<img name="assoc" src="/admin/login/media/images/nav-assoc-logos.gif" width="127" height="24" alt="Association Logos" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>	
		
		<td><a href="/admin/CalendarFX/" target="_top"
			onmouseover="changeImages('calendar', '/admin/login/media/images/nav-calendar-h-blue.gif'); return true;"
			onmouseout="changeImages('calendar', '/admin/login/media/images/nav-calendar-blue.gif'); return true;">
			<img name="calendar" src="/admin/login/media/images/nav-calendar-blue.gif" width="73" height="24" alt="Calendar" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
		-->
		<td><a href="/Admin/Blogs/" target="_top"
			onmouseover="changeImages('blogs', '/admin/login/media/images/nav-blogs-h.gif'); return true;"
			onmouseout="changeImages('blogs', '/admin/login/media/images/nav-blogs.gif'); return true;">
			<img name="blogs" src="/admin/login/media/images/nav-blogs.gif" width="55" height="24" alt="Blogs" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>	
		
		<td><a href="/admin/Companies/" target="_top"
			onmouseover="changeImages('companies', '/admin/login/media/images/nav-companies-h.gif'); return true;"
			onmouseout="changeImages('companies', '/admin/login/media/images/nav-companies.gif'); return true;">
			<img name="companies" src="/admin/login/media/images/nav-companies.gif" width="86" height="24" alt="Companies" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>			

		<td><a href="/admin/ContentManager/" target="_top"
			onmouseover="changeImages('cm', '/admin/login/media/images/nav-cm-h.gif'); return true;"
			onmouseout="changeImages('cm', '/admin/login/media/images/nav-cm.gif'); return true;">
			<img name="cm" src="/admin/login/media/images/nav-cm.gif" width="120" height="24" alt="Content Manager" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
		
		<td><a href="/admin/MediaLibrary/" target="_top"
			onmouseover="changeImages('medialibrary', '/admin/login/media/images/nav-medialibrary-h.gif'); return true;"
			onmouseout="changeImages('medialibrary', '/admin/login/media/images/nav-medialibrary.gif'); return true;">
			<img name="medialibrary" src="/admin/login/media/images/nav-medialibrary.gif" width="106" height="24" alt="Media Library" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
		
		<!--
		<td><a href="/admin/Coupons/" target="_top"
			onmouseover="changeImages('coupons', '/admin/login/media/images/nav-coupons-h-blue.gif'); return true;"
			onmouseout="changeImages('coupons', '/admin/login/media/images/nav-coupons-blue.gif'); return true;">
			<img name="coupons" src="/admin/login/media/images/nav-coupons-blue.gif" width="69" height="24" alt="Coupons" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>	

		<td><a href="/admin/Courses/" target="_top"
			onmouseover="changeImages('courses', '/admin/login/media/images/nav-courses-h.gif'); return true;"
			onmouseout="changeImages('courses', '/admin/login/media/images/nav-courses.gif'); return true;">
			<img name="courses" src="/admin/login/media/images/nav-courses.gif" width="68" height="24" alt="Courses" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>		
		-->

<!--
		<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr bgcolor="#FFFFFF">
		<td><img src="/admin/login/media/images/spacer.gif" width=10 height=1 border=0 alt=""></td>
	</tr>
	<tr bgcolor="#6099D2">
		<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
-->
		<!--
		<td><a href="/admin/Settings" target="_top"
			onmouseover="changeImages('gen', '/admin/login/media/images/nav-gen-settings-h.gif'); return true;"
			onmouseout="changeImages('gen', '/admin/login/media/images/nav-gen-settings.gif'); return true;">
			<img name="gen" src="/admin/login/media/images/nav-gen-settings.gif" width="119" height="24" alt="General Settings" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>		

		<td><a href="/admin/ComingSoon.asp" target="displayarea"
			onmouseover="changeImages('help', '/admin/login/media/images/nav-help-h.gif'); return true;"
			onmouseout="changeImages('help', '/admin/login/media/images/nav-help.gif'); return true;">
			<img name="help" src="/admin/login/media/images/nav-help.gif" width="47" height="24" alt="Help" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>		
		-->
<!--
		<td><a href="http://livestats.xigroup.com/global/1203396555/stats?type=login&action=login&serverid=<%= application("sLiveStatsServerID") %>" target="_blank"
			onmouseover="changeImages('photogallery', '/admin/login/media/images/nav-liveStats-h.gif'); return true;"
			onmouseout="changeImages('photogallery', '/admin/login/media/images/nav-liveStats.gif'); return true;">
			<img name="photogallery" src="/admin/login/media/images/nav-liveStats.gif" width="78" height="24" alt="Photo Gallery" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>

		<td><a href="/admin/ComingSoon.asp" target="displayarea"  
			onmouseover="changeImages('photogallery', '/admin/login/media/images/nav-liveStats-h.gif'); return true;"
			onmouseout="changeImages('photogallery', '/admin/login/media/images/nav-liveStats.gif'); return true;">
			<img name="photogallery" src="/admin/login/media/images/nav-liveStats.gif" width="78" height="24" alt="Photo Gallery" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
	
		<td><a href="/admin/Newsletter" target="_top"
			onmouseover="changeImages('newsletter', '/admin/login/media/images/nav-newsletter-h.gif'); return true;"
			onmouseout="changeImages('newsletter', '/admin/login/media/images/nav-newsletter.gif'); return true;">
			<img name="newsletter" src="/admin/login/media/images/nav-newsletter.gif" width="85" height="24" alt="Newsletter" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>				
		
		<td nowrap><a href="/admin/PLP/" target="_top"  
			onmouseover="changeImages('privatelabelpages', '/admin/login/media/images/nav-private-label-pages-h.gif'); return true;"
			onmouseout="changeImages('privatelabelpages', '/admin/login/media/images/nav-private-label-pages.gif'); return true;">
			<img name="privatelabelpages" src="/admin/login/media/images/nav-private-label-pages.gif" width="138" height="24" alt="Private Label Pages" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>		
-->
<!--
		<td><a href="/admin/Reports/" target="_top"
			onmouseover="changeImages('reports', '/admin/login/media/images/nav-reports-h.gif'); return true;"
			onmouseout="changeImages('reports', '/admin/login/media/images/nav-reports.gif'); return true;">
			<img name="reports" src="/admin/login/media/images/nav-reports.gif" width="67" height="24" alt="Reports" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>				
-->

		<td><a href="/admin/States/" target="_top"
			onmouseover="changeImages('states', '/admin/login/media/images/nav-states-h.gif'); return true;"
			onmouseout="changeImages('states', '/admin/login/media/images/nav-states.gif'); return true;">
			<img name="states" src="/admin/login/media/images/nav-states.gif" width="59" height="24" alt="States" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>	

		<td><a href="/admin/Users/" target="_top"
			onmouseover="changeImages('users', '/admin/login/media/images/nav-users-h.gif'); return true;"
			onmouseout="changeImages('users', '/admin/login/media/images/nav-users.gif'); return true;">
			<img name="users" src="/admin/login/media/images/nav-users.gif" width="54" height="24" alt="Users" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>

		<td><a href="/admin/login/logout.asp" target="_top"
			onmouseover="changeImages('logout', '/admin/login/media/images/nav-logout-h.gif'); return true;"
			onmouseout="changeImages('logout', '/admin/login/media/images/nav-logout.gif'); return true;">
			<img name="logout" src="/admin/login/media/images/nav-logout.gif" width="67" height="24" alt="Logout" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>	
<!--
		<td><a href="/admin/login/logout.asp" target="_top"
			onmouseover="changeImages('logout', '/admin/login/media/images/nav-logout-h.gif'); return true;"
			onmouseout="changeImages('logout', '/admin/login/media/images/nav-logout.gif'); return true;">
			<img name="logout" src="/admin/login/media/images/nav-logout.gif" width="67" height="24" alt="Logout" border="0"></a></td>
		<td><img src="/admin/login/media/images/nav-division.gif" width=1 height=24 border=0 alt=""></td>
-->
<% end if %>		
		<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0" >
	<tr bgcolor="#FFFFFF">
		<td><img src="/admin/login/media/images/spacer.gif" width=10 height=1 border=0 alt=""></td>
	</tr>
	<tr bgcolor="#6099D2">
		<td width="50%"><img src="/admin/login/media/images/spacer.gif" width=1 height=24 border=0 alt=""></td>
	</tr>
</table>

<!-- #include virtual="/admin/login/includes/security.asp" -->
<!-- #include virtual = "/admin/login/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/login/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/login/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/login/includes/ContentManager.asp" -------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Administrative Tools"
	
	ipage = 2
	
If Session("access_level") = "" Then
	Response.Redirect("login.asp?referer=admin.asp&db="&Request("db"))
End If

dim strSQLConnectionString
dim objAdminConnection
dim objRS
dim strSQL
dim strDBName

strSQLConnectionString = application("sDataSourceName")
session("DBNameLogin") = session("DBName")

'********************************************************
'Get ConMan content from XIGroup DB
'********************************************************

sLoginKeyword = "Login"
sNewsKeyword = "In the News"

if application("bIsAutoSite") then
	sKeyword1 = "BPAutomotive"
	iBannerTypeID = 1
	if application("bShowContentManager") then
		sKeyword5 = "BPContentManagement"
	else
		sKeyword5 = ""
	end if
else
	iBannerTypeID = 3
	sKeyword1 = "BPContentManagement"
	sKeyword5 = ""
end if

sKeyword2 = "BPSOS"
sKeyword3 = "BPSecurity"
sKeyword4 = "BPNetwork"


'Set up media path to XIGroup.com (depending on server)
sXigUrl = GetServer()	'at the bottom

if bShowImage then
	sMediaPath = sXIGUrl & "/usermedia/images/" & sImgPath
end if	
'********************************************************
'End ConMan content
'********************************************************

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
var preloadFlag = true;
var bCharts = false;


		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<script language="JavaScript">

function OpenBannerWindow(p_sUrl, p_BannerID)
{
	window.open('redirectBanner.asp?BannerID=' + p_BannerID + '&URL=' + p_sUrl,'XIGWindow')
}

function OpenXigWindow(p_sUrl)
{
	window.open(p_sUrl,'XIGWindow')
}		

// Done -->

<%
'Determine whether or not to display a pop up banner
if application("bIsAutoSite") then
	iPopUpBannerTypeID = "2"
else
	iPopUpBannerTypeID = "4"
end if

if getPopupStatus(iPopUpBannerTypeID) then	'at bottom of page
	'pull a random banner
	iPopUpBannerID = GetBanner(iPopUpBannerTypeID)	'at bottom of page
	
	'Make sure this banner has not been previously displayed
	sCookieName = "XIBANNER" & iPopUpBannerID
	
	if trim(Request.Cookies(sCookieName)) <> "1" then
%>
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open('popup_banner.asp?BannerID=<%=iPopUpBannerID%>', '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=150,left = 100,top = 134');");
<%
	else
		'Banner Previously displayed
		
		'Since the VB Component records this view as a hit,
		'we need to delete that record
		set rs = server.CreateObject("ADODB.RecordSet")
		set objBannerConn = server.CreateObject("ADODB.Connection")
		objBannerConn.ConnectionString = application("sXIGConnectionString")
		objBannerConn.Open	
		sSQL = "SELECT Max(ID) FROM afxBannerHits WHERE BannerID = '" & iPopUpBannerID & "' AND HitType = 'I'"
		rs.Open sSQL, objBannerConn, 3, 3
		iMaxID = trim(rs(0))
		rs.close
		set rs = nothing

		sSQL = "DELETE FROM afxBannerHits WHERE ID = '" & iMaxID & "' "
		
		objBannerConn.execute(sSQL)
		set objBannerConn = nothing
	end if
end if
%>

</script>

<%
		'bShowAutoID = True
		'bShowJobsFX = False
		'bShowWebCoupons = True
		'bShowShopFX = False
		'bShowNewsFX = False
		'bShowLeadTracker = False
		'bShowContentManager = False
		'bShowDealerPromo = False
		'bShowFormManager = False
		bShowUserSecurity = False
		'bShowCalendar = False

%>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<LINK REL="stylesheet" TYPE="text/css" HREF="includes/Style_ie.css">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<td nowrap>Contact<br>
			<a href="http://www.xigroup.com" target="_blank"><b>XIGroup</b></a>  |  410-276-0010<br>
			<a href="mailto:customersupport@xigroup.com">email us</a></td>
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<td width="100%" bgcolor="339933">
			<table border="0" cellspacing="0" cellpadding="0">
				<% DisplayBanner(iBannerTypeID) 'Display banners if necessary %>
			</table>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
			<br>
			<table width="100%" border="0">
				<tr>
					<td>
						<table width="160" bgcolor="#6699CC" cellpadding="5" cellspacing="1">
							<tr>
								<td bgcolor="White" class="subtitle">XIG News</td>
							</tr>
						</table>
						<br>				
					</td>
				</tr>
				<%
				setGlobalContentData(sLoginKeyword)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sLoginKeyword & "')"">" & g_sHeadline & "</a><br>")

						if WordCount(g_sText) > 50 then
							bShowContinued = true
							sBody = TruncateStoryByWords(g_sText,50) & "..."
						else
							bShowContinued = false
							sBody = g_sText
						end if
						response.write(sBody)
											
						if bShowContinued then
							response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sLoginKeyword & "')"">(continued)</a>")
						end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>
					
				<%
				setGlobalContentData(sNewsKeyword)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sNewsKeyword & "')"">" & g_sHeadline & "</a><br>")

						if WordCount(g_sText) > 50 then
							bShowContinued = true
							sBody = TruncateStoryByWords(g_sText,50) & "..."
						else
							bShowContinued = false
							sBody = g_sText
						end if
						response.write(sBody)
											
						if bShowContinued then
							response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sNewsKeyword & "')"">(continued)</a>")
						end if											
					%>
						<br><br>
					</td>
				</tr>
				<%
					end if
				%>		
				<tr>
					<td>
						<table width="160" bgcolor="#6699CC" cellpadding="5" cellspacing="1">
							<tr>
								<td bgcolor="White" class="subtitle">Best Practices</td>
							</tr>
						</table>
						<br>					
					</td>
				</tr>
				<%
				setGlobalContentData(sKeyword1)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
					response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword1 & "')"">" & g_sHeadline & "</a><br>")

					if WordCount(g_sText) > 50 then
						bShowContinued = true
						sBody = TruncateStoryByWords(g_sText,50) & "..."
					else
						bShowContinued = false
						sBody = g_sText
					end if
					response.write(sBody)
					
					if bShowContinued then
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword1 & "')"">(continued)</a>")
					end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>
				<%
				setGlobalContentData(sKeyword2)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
					response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword2 & "')"">" & g_sHeadline & "</a><br>")

					if WordCount(g_sText) > 50 then
						bShowContinued = true
						sBody = TruncateStoryByWords(g_sText,50) & "..."
					else
						bShowContinued = false
						sBody = g_sText
					end if
					response.write(sBody)
					
					if bShowContinued then
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword2 & "')"">(continued)</a>")
					end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>
				<%
				setGlobalContentData(sKeyword3)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
					response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword3 & "')"">" & g_sHeadline & "</a><br>")

					if WordCount(g_sText) > 50 then
						bShowContinued = true
						sBody = TruncateStoryByWords(g_sText,50) & "..."
					else
						bShowContinued = false
						sBody = g_sText
					end if
					response.write(sBody)
					
					if bShowContinued then
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword3 & "')"">(continued)</a>")
					end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>			
				<%
				setGlobalContentData(sKeyword4)
				if trim(len(g_sHeadline)) > 0 then
				%>
				<tr>
					<td>
					<%
					response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword4 & "')"">" & g_sHeadline & "</a><br>")

					if WordCount(g_sText) > 50 then
						bShowContinued = true
						sBody = TruncateStoryByWords(g_sText,50) & "..."
					else
						bShowContinued = false
						sBody = g_sText
					end if
					response.write(sBody)
					
					if bShowContinued then
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword4 & "')"">(continued)</a>")
					end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>															
				<%
				if len(sKeyword5) > 0  then
					setGlobalContentData(sKeyword5)
				%>
				<tr>
					<td>
					<%
					response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword5 & "')"">" & g_sHeadline & "</a><br>")

					if WordCount(g_sText) > 50 then
						bShowContinued = true
						sBody = TruncateStoryByWords(g_sText,50) & "..."
					else
						bShowContinued = false
						sBody = g_sText
					end if
					response.write(sBody)
					
					if bShowContinued then
						response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword5 & "')"">(continued)</a>")
					end if											
					%>
					<br><br>
					</td>
				</tr>
				<%
				end if
				%>
			</table>
		</td>
		<%if bShowImage then%>
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<!-- Place Photo here -->
		<td><img src="<%=sMediaPath%>" alt="<%=sAltTag%>" border="0"></td>
		<%else%>
		<%end if%>
		<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" colspan="2"><img src="media/images/addfx-xig-2001.gif" width="72" height="40" align="right" border="0" alt=""> </td>
	</tr>
</table>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
'#####################################################################
'Name : GetBanner
'Description : returns the bannerid of a random banner based upon the type
'#####################################################################
function GetBanner(p_iBannerType)
	dim largetop, largeTopRS 'as object 
	dim LargeTopImagePath, LargeTopAltTag, LargeTopURL, sSQL 'as string
	dim LargeTopBannerID  'as integer

	'Large Top Banner
	set LargeTopRS = server.CreateObject("ADODB.Recordset")
	set LargeTop = server.CreateObject("BannerFXv2.SearchForBanner")

	LargeTop.ConnectString = application("sXIGConnectionString")
	LargeTop.siteID = 1
	sMainImgPath = application("sDYNWebRoot") & "userMedia/Images/Banners/"

	largeKeyword = replace("Banner", "'", "''")
	LargeTop.BuildKeywordArray(cstr(largeKeyword))
	LargeTop.BannerType = p_iBannerType
	set LargeTopRS = LargeTop.GetBanner
	set LargeTop = nothing

	if not LargeTopRS.eof then		
		GetBanner = LargeTopRS("BannerID")
	else
		GetBanner = 0
	end if
	
	set LargeTopRs = nothing
end function

'#####################################################################
'Name : DisplayBanner
'Description : displays a random banner on the page based upon the type
'#####################################################################
sub DisplayBanner(p_iBannerType)
	dim largetop, largeTopRS 'as object 
	dim LargeTopImagePath, LargeTopAltTag, LargeTopURL, sSQL 'as string
	dim LargeTopBannerID 'as integer
	
	'Large Top Banner
	set LargeTopRS = server.CreateObject("ADODB.Recordset")
	set LargeTop = server.CreateObject("BannerFXv2.SearchForBanner")

	LargeTop.ConnectString = application("sXIGConnectionString")
	LargeTop.siteID = 1
	sMainImgPath = application("sDYNWebRoot") & "userMedia/Images/Banners/"

	largeKeyword = replace("Banner", "'", "''")
	LargeTop.BuildKeywordArray(cstr(largeKeyword))
	LargeTop.BannerType = p_iBannerType
	set LargeTopRS = LargeTop.GetBanner
	set LargeTop = nothing

	if not LargeTopRS.eof then		
		LargeTopImagePath = LargeTopRS("ImgPath")
		if not ucase(right(LargeTopImagePath, 4)) = "HTTP" then
			LargeTopImagePath = sMainImgPath & LargeTopImagePath
		end if	
		LargeTopAltTag = LargeTopRS("AltTag")
		'LargeTopURL = server.URLEncode(LargeTopRS("ReferralURL"))
		LargeTopURL = LargeTopRS("ReferralURL")
		LargeTopBannerID = LargeTopRS("BannerID")
		
		response.write("<tr>" & vbcrlf)
		response.write("<td align=""center"">" & vbcrlf)
		response.write("<a href=""javascript:OpenBannerWindow('" & LargeTopURL & "','" & LargeTopBannerID & "')""><img src=""" & GetServer() & LargeTopImagePath & """ alt=""" & LargeTopAltTag & """ border=""0"" vspace=""0"" hspace=""0""></a>")
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)
		'response.write("<tr>" & vbcrlf)
		'response.write("<td>" & vbcrlf)
		'response.write("&nbsp;")
		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)
	end if

	LargeTopRS.Close
	set LargeTopRS = nothing
	set LargeTop = nothing
end sub

'#####################################################################
'Name : GetServer
'Description : determines what server to use
'#####################################################################
function GetServer()
	sServerName = ucase(Request.ServerVariables("SERVER_NAME"))
	select case mid(sServerName,1,4)
	' www is the production server
		case "WWW."
			GetServer = "http://xigroup.com"
		case "STAG"
			GetServer = "http://staging.xigroup.com"
		case "DEV."
			GetServer = "http://dev.xigroup"
		case else
			if (instr(UCase(sServerName), "STAGING") > 0) then
				GetServer = "http://staging.xigroup.com"
			else			
				GetServer = "http://xigroup.com"
			end if
	end select	
end function

'#####################################################################
'Name : GetPopupStatus
'Description : Checks to see if the popup is turned on or off
'#####################################################################
function getPopupStatus(p_iBannerTypeID)
dim sSQL
dim rsStatus
dim oDBConn
dim bReturn
dim rs

	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsStatus = server.CreateObject("ADODB.Recordset")
	set rs = server.CreateObject("ADODB.Recordset")	

	oDBConn.ConnectionString = application("sXIGConnectionString")
	oDBConn.Open

	set rsStatus.ActiveConnection = oDBConn

	sSQL = "SELECT * FROM Popup"

	rsStatus.Open sSQL

	if not rsStatus.EOF then
		bReturn = rsStatus.Fields("Status").Value
		
		if bReturn then
			'Check to see if there are any pop up surveys in the system
			sSQL = "SELECT COUNT(*) FROM afxBanners WHERE BannerType = '" & p_iBannerTypeID & "' "
			
			rs.Open sSQL, oDBConn, 3, 3
			if not rs.eof then
				'if true, there are banners to display
				if rs(0) > 0 then
					bReturn = true
				else
					bReturn = false
				end if
			else
				bReturn = false
			end if
		else
			bReturn = false
		end if
	else
		bReturn = false
	end if

	rsStatus.Close
	oDBConn.Close

	getPopupStatus = bReturn
	
	set rs = nothing
	set rsStatus = Nothing
	set oDBConn = Nothing
end function
%>
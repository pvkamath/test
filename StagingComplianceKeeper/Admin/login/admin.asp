<!-- #include virtual="/admin/login/includes/security.asp" -->
<!-- #include virtual = "/admin/login/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/login/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/login/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/login/includes/ContentManager.asp" -------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Administrative Tools"
	
	ipage = 2
	
If Session("access_level") = "" Then
	Response.Redirect("login.asp?referer=admin.asp&db="&Request("db"))
End If

dim strSQLConnectionString
dim objAdminConnection
dim objRS
dim strSQL
dim strDBName

strSQLConnectionString = application("sDataSourceName")
session("DBNameLogin") = session("DBName")

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
var preloadFlag = true;
var bCharts = false;


		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<LINK REL="stylesheet" TYPE="text/css" HREF="includes/Style_ie.css">
<br>
<table border=0 align="center">
	<tr>
		<th align="center" colspan="2">
			Admin Features
		</th>
	</tr>
	<tr>
		<td>
			<table cellspacing="10" cellpadding="10">
				<tr>
<% if cint(session("Access_Level")) < 2 then %>
					<td valign="top">
						<a href="/admin/Companies/">Companies</a><br>
						<a href="/admin/ContentManager/">Content Manager</a><br>
					</td>
					<td valign="top">				
						<a href="/admin/States">States</a><br>				
						<a href="/admin/Users/">Users</a>							
					</td>				
<% end if %>						
				</tr>
			</table>
		</td>	
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>


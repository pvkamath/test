<!-- #include virtual="/admin/login/includes/security.asp" -->
<!-- #include virtual = "/admin/login/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/login/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/login/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state

	dim sPgeTitle							' Template Variable
	dim objConn 'as object
	
	' Set Page Title:
	sPgeTitle = "Administrative Login"
	
	ipage = 1

	'Check to make sure that a user name was passed
		'If its not, send them back to the login page
	if not request("User_Password") = "" then 
		'Instantiate LoginFX and a Recordset
		set rs = server.CreateObject("ADODB.Recordset")
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open		
		
		sUserName = replace(request("User_Login"),"'","''")
		sPassword = replace(request("User_Password"),"'","''")
		
		sSQL = "SELECT * FROM Users WHERE UserName = '" & sUserName & "' AND Password = '" & sPassword & "' AND GroupID < 5"
		
		set rs = objConn.execute(sSQL)

		'If the recordset is at the end of the file, there was no match
		if rs.EOF then 
			set rs = nothing
			set objConn = nothing
			'If no match, then we send them back to login.asp
			Response.Redirect "/admin/login/Login.asp" 
		else 	
			'If a non Active user, display appropriate errror message
			if cint(rs("UserStatus")) = 2 then
				set rs = nothing
				set objConn = nothing			
				response.redirect("/admin/error.asp?message=Your account has been Suspended.")
			elseif cint(rs("UserStatus")) = 3 then
				set rs = nothing
				set objConn = nothing						
				response.redirect("/admin/error.asp?message=Your account has been Deleted.")			
			end if
		
			'if there was a match, set the UserID and AccessLevel to a session variable
			'to use for validating other pages
			'session.Abandon
			session("Access_Level") = rs("GroupID") 
			session("User_ID") = rs("UserID") 

			'Verify that this is an allowed access level.
			if session("Access_Level") > 1 then
				set rs = nothing
				set objConn = nothing						
				Response.Redirect("/admin/error.asp?message=This is not an administrative account.")
			end if

			'Response.Write session("User_ID")
				
			'Get fullname and store it in session var
			set oUserObj = New User
			oUserObj.ConnectionString = application("sDataSourceName")
			oUserObj.UserID = rs("UserID")
			
			set oRs2 = oUserObj.GetUser
			if trim(oRs2("MiddleName")) <> "" then
				sFullName = oRs2("FirstName") & " " & oRs2("MiddleName") & ". " & oRs2("LastName")
			else
				sFullName = oRs2("FirstName") & " " & oRs2("LastName")
			end if			
			session("User_FullName") = sFullName
			session("User_UserName") = oRs2("UserName")
			session("User_CompanyId") = oRs2("CompanyID")
			session("UserCompanyId") = oRs2("CompanyID")
			session("User_BranchId") = oRs2("BranchID")

			'if company admin, set AllowedToAddUsers var
			if cint(session("Access_Level")) = 4 then
				set oRs2 = server.CreateObject("ADODB.RecordSet")
				sSQL = "SELECT AllowedToAddUsers FROM CompanyAdmins WHERE UserID = '" & rs("UserID") & "' "
				oRs2.Open sSQL, objConn, 3, 3
				
				if not oRs2.eof then
					Session("AllowedToAddUsers") = oRs2("AllowedToAddUsers")
				end if
				oRs2.close()
			end if
			
			'record last login date
			sSQL = "UPDATE Users Set LastLoginDate = '" & now() & "' WHERE UserID = '" & session("User_ID") & "' "
			objConn.execute(sSQL)
		
			set rs = nothing
			set objConn = nothing			
			set oRs2 = nothing
			set oUserObj = nothing
				
			response.redirect "/admin/login/admin.asp"				
				
		end if
	end if	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
var preloadFlag = true;
var bCharts = false;


		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {

		}
		// Enter Javascript Validation and Functions below: ----------------------
	var bInfoWindow = 0
	 
	 function openInfoWindow(strHTMLPage, intHeight, intWidth){
	  bInfoWindow = 1
	  infoWindow = window.open(strHTMLPage, "Info", "toolbar=0,height="+intHeight+",width="+intWidth+",resizable=0,scrollbars=1");  
	  infoWindow.focus();
	 }
	 
	 function closeInfoWindow(){
	  if (bInfoWindow == 1) {
	   if (infoWindow && !infoWindow.closed){
	    infoWindow.close();
	   }
	  }
	 }		
//--></script>

<script language="JavaScript">

<!-- Hide this
if (document.images) {
   off = new MakeArray(8);
   over = new MakeArray(8);

   off[1].src = "media/images/title-contentmanager.gif";
   off[2].src = "media/images/title-coupons.gif";
   off[3].src = "media/images/title-formmanager.gif";
   off[4].src = "media/images/title-leadtracker.gif";
   off[5].src = "media/images/title-dealerpromo.gif";
   off[6].src = "media/images/title-inventory.gif";
   off[7].src = "media/images/title-shoppingcart.gif";
   off[8].src = "media/images/title-calendar.gif";
   
   
   over[1].src = "media/images/title-contentmanager-over.gif";
   over[2].src = "media/images/title-coupons-over.gif";
   over[3].src = "media/images/title-formmanager-over.gif";
   over[4].src = "media/images/title-leadtracker-over.gif";
   over[5].src = "media/images/title-dealerpromo-over.gif";
   over[6].src = "media/images/title-inventory-over.gif";
   over[7].src = "media/images/title-shoppingcart-over.gif";
   over[8].src = "media/images/title-calendar-over.gif";
      

}
function MakeArray(n) {
   this.length = n
   for (var i = 1; i<=n; i++) {
      this[i] = new Image()
   }
   return this;
}
function UDIover(name,number){
if (document.images){
      name.src = over[number].src;
   }
}
function UDIoff(name,number){
if (document.images){
      name.src = off[number].src;
   }
}
// Done -->

</script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2"><img src="media/images/clear.gif" width="10" height="10" alt="" border="0"></td>
		<!-- Space left of vertical line -->
		<td rowspan="3"><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<!-- dotted line -->
		<!-- Space right of vertical line -->
		<td rowspan="3"><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
		<td><img src="media/images/clear.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
</table>
<br>
<table align="center">
	<tr> 
		<td valign="top" align="center">
           		<!-- Login Form Start Here ------------------------------------------->
              		<table border="0" cellspacing="0" cellpadding="0">
                		<form method=post action=login.asp id=form1 name=form1>
                  		<input type=hidden name=referer value=admin.asp>
                  		<tr>
							<td colspan="2">
								<img src="media/images/clear.gif" width="10" height="5" alt="" border="0"><br>
								<b>Please login to access all of your administrative controls.</b><br>
								<img src="media/images/clear.gif" width="10" height="15" alt="" border="0">
							</td>
						</tr>
						<tr>
	               			<td nowrap><span>&nbsp;Username:&nbsp;</span></td>
	               			<td width="100%"> 
	                 			<input type="text" name="user_login" vspace="0" hspace="0">
	               			</td>
	             		</tr>
	             		<tr>
	               			<td>&nbsp;Password:&nbsp;</td>
	               			<td width="100%"> 
	                 			<input type="password" name="user_Password" vspace="0" hspace="0">
	               			</td>
	             		</tr>
	             		<tr>
	               			<td align="right">&nbsp;</td>
	               			<td width="100%">
						<img src="media/images/clear.gif" width="10" height="10" alt="" border="0"><br>
	                			<input type="image" name="Submit" value="Submit" src="media/images/submit.gif" border="0" width="60" height="15">
	               			</td>
	             		</tr>
	        	</form>
	        	</table>
	        <!-- Login Form Ends Here -->
	    	</td>		
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual="/common/connect.asp" -->
<!-- #include virtual="/includes/security.asp" -->
<!-- #include virtual="/includes/config.asp" -->
<%
'response.write(application("sAbsWebroot"))
'GetConnectionString
'CheckSecurity
%>
<!-- #include virtual="/common/Connect.asp" -->
<!-- #include virtual = "/admin/login/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/login/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/login/includes/shell.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/login/includes/ContentManager.asp" -------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "AddFX Administrative Tools"
	
If Session("access_level") = "" Then
	Response.Redirect("login.asp?referer=admin.asp&db="&Request("db"))
End If

dim strSQLConnectionString
dim objAdminConnection
dim objRS
dim strSQL
dim strDBName

getConnectionString

strSQLConnectionString = session("strSQLConnectionString")
session("DBNameLogin") = session("DBName")

'********************************************************
'Get ConMan content from XIGroup DB
'********************************************************
if application("bIsAutoSite") then
	sKeyword1 = "BPAutomotive"
	iBannerID = 1
	if application("bShowContentManager") then
		sKeyword5 = "BPContentManagement"
	end if
else
	iBannerID = 3
	sKeyword1 = "BPContentManagement"
	sKeyword5 = ""
end if

sKeyword2 = "BPSOS"
sKeyword3 = "BPSecurity"
sKeyword4 = "BPNetwork"


'Set up media path to XIGroup.com (depending on server)
sXigUrl = GetServer()	'at the bottom

if bShowImage then
	sMediaPath = sXIGUrl & "/usermedia/images/" & sImgPath
end if	
'********************************************************
'End ConMan content
'********************************************************

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
var preloadFlag = true;
var bCharts = false;


		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {

		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>


<script language="JavaScript">

<!-- Hide this
if (document.images) {
   off = new MakeArray(10);
   over = new MakeArray(10);

   off[1].src = "media/images/bttn_contentmanager.gif";
   off[2].src = "media/images/bttn_coupons.gif";
   off[3].src = "media/images/bttn_survey.gif";
   off[4].src = "media/images/bttn_leadtracker.gif";
   off[5].src = "media/images/bttn_dealerpromo.gif";
   off[6].src = "media/images/bttn_inventory.gif";
   off[7].src = "media/images/bttn_shoppingcart.gif";
   off[8].src = "media/images/bttn_calendar.gif";
   off[9].src = "media/images/bttn_jobs.gif";
   off[10].src = "media/images/bttn_newsFX.gif";   
   
   over[1].src = "media/images/bttn_contentmanager_over.gif";
   over[2].src = "media/images/bttn_coupons_over.gif";
   over[3].src = "media/images/bttn_survey_over.gif";
   over[4].src = "media/images/bttn_leadtracker_over.gif";
   over[5].src = "media/images/bttn_dealerpromo_over.gif";
   over[6].src = "media/images/bttn_inventory_over.gif";
   over[7].src = "media/images/bttn_shoppingcart_over.gif";
   over[8].src = "media/images/bttn_calendar_over.gif";
   over[9].src = "media/images/bttn_jobs_over.gif";
   over[10].src = "media/images/bttn_newsFX_over.gif";     

}
function MakeArray(n) {
   this.length = n
   for (var i = 1; i<=n; i++) {
      this[i] = new Image()
   }
   return this;
}
function UDIover(name,number){
if (document.images){
      name.src = over[number].src;
   }
}
function UDIoff(name,number){
if (document.images){
      name.src = off[number].src;
   }
}

function OpenXigWindow(p_sUrl)
{
	window.open(p_sUrl,'XIGWindow')
}

// Done -->

<%
'Determine whether or not to display a pop up banner
if application("bIsAutoSite") then
	iPopUpBannerID = "2"
else
	iPopUpBannerID = "4"
end if

if getPopupStatus(iPopUpBannerID) then	'at bottom of page
%>
day = new Date();
id = day.getTime();
eval("page" + id + " = window.open('popup_banner.asp?BannerType=<%=iPopUpBannerID%>', '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=150,left = 100,top = 134');");
<%
end if
%>

</script>

<%
		'bShowAutoID = True
		'bShowJobsFX = False
		'bShowWebCoupons = True
		'bShowShopFX = False
		'bShowNewsFX = False
		'bShowLeadTracker = False
		'bShowContentManager = False
		'bShowDealerPromo = False
		'bShowFormManager = False
		bShowUserSecurity = False
		'bShowCalendar = False

%>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
        		<table width="100%" border="0" cellspacing="0" cellpadding="0">
          			<tr> 
            				<td valign="top"> 
              					<table width="100%" border="0" rules="none" cellspacing="0" cellpadding="0">
                					<tr valign="top"> 
			            				<td>
									<table width="100%" border="0" rules="none" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" class="subtitle">Access AddFX:<br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%if bShowContentManager = true then%>
										<tr>
											<td><a href="/admin/ContentManager/" OnMouseOver="UDIover(content,1); return true;" OnMouseOut="UDIoff(content,1)"><img src="media/images/bttn_contentmanager.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Content Manager" border="0" name="content"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>	
<%if bShowNewsFX = true then%>
										<tr>
											<td><a href="/admin/NewsFX/" OnMouseOver="UDIover(newsFX,10); return true;" OnMouseOut="UDIoff(newsFX,10)"><img src="media/images/bttn_newsFX.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX NewsFX" border="0" name="newsFX"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>	

<%if bShowWebCoupons = true then%>
										<tr>
											<td><a href="/admin/WebCoupons/" OnMouseOver="UDIover(coupons,2); return true;" OnMouseOut="UDIoff(coupons,2)"><img src="media/images/bttn_coupons.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Coupons" border="0" name="coupons"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>

<%if bShowFormManager = true then%>
										<tr>
											<td><a href="/admin/Survey/" OnMouseOver="UDIover(survey,3); return true;" OnMouseOut="UDIoff(survey,3)"><img src="media/images/bttn_survey.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Survey" border="0" name="survey"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>

<%if bShowLeadTracker = true then%>
										<tr>
											<td><a href="/LeadTracker2.0/" OnMouseOver="UDIover(leadtracker,4); return true;" OnMouseOut="UDIoff(leadtracker,4)"><img src="media/images/bttn_leadtracker.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Lead Tracker" border="0" name="leadtracker"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>

<%if bShowDealerPromo = true then%>
										<tr>
											<td><a href="/admin/DealerPromo/" OnMouseOver="UDIover(dealerpromo,5); return true;" OnMouseOut="UDIoff(dealerpromo,5)"><img src="media/images/bttn_dealerpromo.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Dealer Promo" border="0" name="dealerpromo"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>
										
<%if bShowAutoID = true then%>
										<tr>
											<td><a href="/admin/AutoID/" OnMouseOver="UDIover(inventory,6); return true;" OnMouseOut="UDIoff(inventory,6)"><img src="media/images/bttn_inventory.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Inventory" border="0" name="inventory"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>

<%if bShowShopFX = true then%>										
										<tr>
											<td><a href="/admin/ShopFX/" OnMouseOver="UDIover(shoppingcart,7); return true;" OnMouseOut="UDIoff(shoppingcart,7)"><img src="media/images/bttn_shoppingcart.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Shopping Cart" border="0" name="shoppingcart"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>

<%if bShowCalendar = true then%>										
										<tr>
											<td><a href="/admin/CalendarFX/" OnMouseOver="UDIover(calendar,8); return true;" OnMouseOut="UDIoff(calendar,8)"><img src="media/images/bttn_calendar.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Calendar" border="0" name="calendar"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>
<%if bShowJobsFX = true then%>										
										<tr>
											<td><a href="/admin/JobsFX/" OnMouseOver="UDIover(jobs,9); return true;" OnMouseOut="UDIoff(jobs,9)"><img src="media/images/bttn_jobs.gif" width="160" height="28" hspace="0" vspace="0" alt="AddFX Calendar" border="0" name="jobs"></a><br>
												<img src="media/images/clear.gif" width="10" height="15" alt="" border="0"></td>
										</tr>
<%end if%>
									</table>
			            				</td>
								<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
								<td bgcolor="#000000"><img src="media/images/clear.gif" width="1" height="10" alt="" border="0"></td>
								<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
								<td width="100%">
									<br>
									<table width="100%" border="0">
									<% DisplayBanner(iBannerID) 'Display banners if necessary %>
									<%
									setGlobalContentData(sKeyword1)
									if trim(len(g_sHeadline)) > 0 then
									%>
										<tr>
											<td>
											<%
											response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword1 & "')"">" & g_sHeadline & "</a><br>")

											if WordCount(g_sText) > 50 then
												bShowContinued = true
												sBody = TruncateStoryByWords(g_sText,50) & "..."
											else
												bShowContinued = false
												sBody = g_sText
											end if
											response.write(sBody)
											
											if bShowContinued then
												response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword1 & "')"">(continued)</a>")
											end if											
											%>
											<br><br>
											</td>
										</tr>
									<%
									end if
									%>
									<%
									setGlobalContentData(sKeyword2)
									if trim(len(g_sHeadline)) > 0 then
									%>
										<tr>
											<td>
											<%
											response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword2 & "')"">" & g_sHeadline & "</a><br>")

											if WordCount(g_sText) > 50 then
												bShowContinued = true
												sBody = TruncateStoryByWords(g_sText,50) & "..."
											else
												bShowContinued = false
												sBody = g_sText
											end if
											response.write(sBody)
											
											if bShowContinued then
												response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword2 & "')"">(continued)</a>")
											end if											
											%>
											<br><br>
											</td>
										</tr>
									<%
									end if
									%>
									<%
									setGlobalContentData(sKeyword3)
									if trim(len(g_sHeadline)) > 0 then
									%>
										<tr>
											<td>
											<%
											response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword3 & "')"">" & g_sHeadline & "</a><br>")

											if WordCount(g_sText) > 50 then
												bShowContinued = true
												sBody = TruncateStoryByWords(g_sText,50) & "..."
											else
												bShowContinued = false
												sBody = g_sText
											end if
											response.write(sBody)
											
											if bShowContinued then
												response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword3 & "')"">(continued)</a>")
											end if											
											%>
											<br><br>
											</td>
										</tr>
									<%
									end if
									%>			
									<%
									setGlobalContentData(sKeyword4)
									if trim(len(g_sHeadline)) > 0 then
									%>
										<tr>
											<td>
											<%
											response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword4 & "')"">" & g_sHeadline & "</a><br>")

											if WordCount(g_sText) > 50 then
												bShowContinued = true
												sBody = TruncateStoryByWords(g_sText,50) & "..."
											else
												bShowContinued = false
												sBody = g_sText
											end if
											response.write(sBody)
											
											if bShowContinued then
												response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword4 & "')"">(continued)</a>")
											end if											
											%>
											<br><br>
											</td>
										</tr>
									<%
									end if
									%>															
									<%
									if len(sKeyword5) > 0  then
										setGlobalContentData(sKeyword5)
									%>
										<tr>
											<td>
											<%
											response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword5 & "')"">" & g_sHeadline & "</a><br>")

											if WordCount(g_sText) > 50 then
												bShowContinued = true
												sBody = TruncateStoryByWords(g_sText,50) & "..."
											else
												bShowContinued = false
												sBody = g_sText
											end if
											response.write(sBody)
											
											if bShowContinued then
												response.write("<a href=""javascript:OpenXigWindow('" & sXIGUrl & "/displayContent.asp?Keywords=" & sKeyword5 & "')"">(continued)</a>")
											end if											
											%>
											<br><br>
											</td>
										</tr>
									<%
									end if
									%>																								
									</table>
								</td>
								<td><img src="media/images/clear.gif" width="15" height="10" alt="" border="0"></td>
								<!-- Place Photo here -->
								<td><%if bShowImage then%><img src="<%=sMediaPath%>" alt="<%=sAltTag%>" border="0"><%else%>&nbsp;<%end if%></td>
								<td width="100%">&nbsp;</td>
                					</tr>
              					</table>
            				</td>
          			</tr>
        		</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" colspan="2"><img src="media/images/addfx_xig_2001.gif" width="72" height="40" align="right" border="0" alt=""> </td>
				</tr>
			</table>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
sub DisplayBanner(p_iBannerType)
	dim objBannerConn, largetop, largeTopRS 'as object 
	dim LargeTopImagePath, LargeTopAltTag, LargeTopURL, sSQL 'as string
	dim LargeTopBannerID, tempBannerID 'as integer
	
	set objBannerConn = server.CreateObject("ADODB.Connection")
	objBannerConn.ConnectionString = application("sXIGConnectionString")
	objBannerConn.Open

	'Large Top Banner
	set LargeTopRS = server.CreateObject("ADODB.Recordset")
	set LargeTop = server.CreateObject("BannerFXv2.SearchForBanner")

	LargeTop.ConnectString = application("sXIGConnectionString")
	LargeTop.siteID = 1
	sMainImgPath = application("sDYNWebRoot") & "userMedia/Images/Banners/"

	largeKeyword = replace("Banner", "'", "''")
	LargeTop.BuildKeywordArray(cstr(largeKeyword))
	LargeTop.BannerType = p_iBannerType
	set LargeTopRS = LargeTop.GetBanner
	set LargeTop = nothing

	if not LargeTopRS.eof then		
		LargeTopImagePath = LargeTopRS("ImgPath")
		if not ucase(right(LargeTopImagePath, 4)) = "HTTP" then
			LargeTopImagePath = sMainImgPath & LargeTopImagePath
		end if	
		LargeTopAltTag = LargeTopRS("AltTag")
		'LargeTopURL = server.URLEncode(LargeTopRS("ReferralURL"))
		LargeTopURL = LargeTopRS("ReferralURL")
		LargeTopBannerID = LargeTopRS("BannerID")
		tempBannerID = LargeTopBannerID
		
		response.write("<tr>" & vbcrlf)
		response.write("<td align=""center"">" & vbcrlf)
		response.write("<a href=""javascript:OpenXigWindow('" & LargeTopURL & "')""><img src=""" & GetServer() & LargeTopImagePath & """ alt=""" & LargeTopAltTag & """ border=""0""></a>")
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)
		response.write("<tr>" & vbcrlf)
		response.write("<td>" & vbcrlf)
		response.write("&nbsp;")
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)
	end if

	LargeTopRS.Close
	set LargeTopRS = nothing
	set LargeTop = nothing
		
	sSQL = "INSERT INTO afxBannerHits (BannerID, HitType) " _
		 & "VALUES ('" & tempBannerID & "', 'I') "
	objBannerConn.execute(sSQL)
			
	set objBannerConn = nothing
end sub

function GetServer()
	sServerName = ucase(Request.ServerVariables("SERVER_NAME"))
	select case mid(sServerName,1,4)
	' www is the production server
		case "WWW."
			GetServer = "http://xigroup.com"
		case "STAG"
			GetServer = "http://staging.xigroup.com"
		case "DEV."
			GetServer = "http://dev.xigroup"
		case else
			GetServer = "http://xigroup.com"
	end select	
end function

'#####################################################################
'Name : GetPopupStatus
'Description : Checks to see if the popup is turned on or off
'#####################################################################
function getPopupStatus(p_iBannerID)
dim sSQL
dim rsStatus
dim oDBConn
dim bReturn
dim rs

	set oDBConn = server.CreateObject("ADODB.Connection")
	set rsStatus = server.CreateObject("ADODB.Recordset")
	set rs = server.CreateObject("ADODB.Recordset")	

	oDBConn.ConnectionString = application("sXIGConnectionString")
	oDBConn.Open

	set rsStatus.ActiveConnection = oDBConn

	sSQL = "SELECT * FROM Popup"

	rsStatus.Open sSQL

	if not rsStatus.EOF then
		bReturn = rsStatus.Fields("Status").Value
		
		if bReturn then
			'Check to see if there are any pop up surveys in the system
			sSQL = "SELECT COUNT(*) FROM afxBanners WHERE BannerType = '" & p_iBannerID & "' "
			
			rs.Open sSQL, oDBConn, 3, 3
			if not rs.eof then
				'if true, there are banners to display
				if rs(0) > 0 then
					bReturn = true
				else
					bReturn = false
				end if
			else
				bReturn = false
			end if
		else
			bReturn = false
		end if
	else
		bReturn = false
	end if

	rsStatus.Close
	oDBConn.Close

	getPopupStatus = bReturn
	
	set rs = nothing
	set rsStatus = Nothing
	set oDBConn = Nothing
end function
%>
<%
iBannerType = request("BannerType")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<script language="Javascript">
function OpenXigWindow(p_sUrl)
{
	window.open(p_sUrl,'XIGWindow')
	window.close();
}
</script>

<body bgcolor="white" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">
<br>
<table cellpadding="0" cellspacing="0" border="0" align="center" valign="middle">
	<tr>
	<!--
		<td><a href="javascript: window.opener.location = 'https://www.bic.edu/admissions.asp'; window.close();"><img src="/media/images/popup_left.gif" width="185" height="195" alt="Apply Now for Spring Semester" border="0"></a></td>
		<td><a href="javascript: window.opener.location = 'https://www.bic.edu/admissions.asp'; window.close();"><img src="/media/images/popup_right.jpg" width="285" height="195" alt="Apply Now for Spring Semester" border="0"></a></td>
	-->
		<td align="center">
			<%
			call DisplayBanner(iBannerType)' sub routine found at the bottom of the page
			%>
		</td>
	</tr>
</table>

</body>
</html>


<%
function GetServer()
	sServerName = ucase(Request.ServerVariables("SERVER_NAME"))
	select case mid(sServerName,1,4)
	' www is the production server
		case "WWW."
			GetServer = "http://xigroup.com"
		case "STAG"
			GetServer = "http://staging.xigroup.com"
		case "DEV."
			GetServer = "http://dev.xigroup"
		case else
			GetServer = "http://xigroup.com"
	end select	
end function

sub DisplayBanner(p_iBannerType)
	dim objBannerConn, largetop, largeTopRS 'as object 
	dim LargeTopImagePath, LargeTopAltTag, LargeTopURL, sSQL 'as string
	dim LargeTopBannerID, tempBannerID 'as integer
	
	set objBannerConn = server.CreateObject("ADODB.Connection")
	objBannerConn.ConnectionString = application("sXIGConnectionString")
	objBannerConn.Open

	'Large Top Banner
	set LargeTopRS = server.CreateObject("ADODB.Recordset")
	set LargeTop = server.CreateObject("BannerFXv2.SearchForBanner")

	LargeTop.ConnectString = application("sXIGConnectionString")
	LargeTop.siteID = 1
	sMainImgPath = application("sDYNWebRoot") & "userMedia/Images/Banners/"

	largeKeyword = replace("Banner", "'", "''")
	LargeTop.BuildKeywordArray(cstr(largeKeyword))
	LargeTop.BannerType = p_iBannerType
	set LargeTopRS = LargeTop.GetBanner
	set LargeTop = nothing

	if not LargeTopRS.eof then		
		LargeTopImagePath = LargeTopRS("ImgPath")
		if not ucase(right(LargeTopImagePath, 4)) = "HTTP" then
			LargeTopImagePath = sMainImgPath & LargeTopImagePath
		end if	
		LargeTopAltTag = LargeTopRS("AltTag")
		'LargeTopURL = server.URLEncode(LargeTopRS("ReferralURL"))
		LargeTopURL = LargeTopRS("ReferralURL")
		LargeTopBannerID = LargeTopRS("BannerID")
		tempBannerID = LargeTopBannerID
		sCookieName = "XIBANNER" & LargeTopBannerID

		if trim(Request.Cookies(sCookieName)) <> "1" then
			response.write("<a href=""javascript:OpenXigWindow('" & LargeTopURL & "')""><img src=""" & GetServer() & LargeTopImagePath & """ alt=""" & LargeTopAltTag & """ border=""0""></a>")
		
			'Create Cookie
			Response.Cookies(sCookieName) = "1"
			Response.Cookies(sCookieName).expires = #01/01/2036#	
		else
			response.write("<script>window.close()</script>")
		end if
	end if

	LargeTopRS.Close
	set LargeTopRS = nothing
	set LargeTop = nothing
		
	sSQL = "INSERT INTO afxBannerHits (BannerID, HitType) " _
		 & "VALUES ('" & tempBannerID & "', 'I') "
	objBannerConn.execute(sSQL)
			
	set objBannerConn = nothing
end sub
%>

<!-- #include virtual="/common/connect.asp" -->
<!-- #include virtual="/includes/security.asp" -->
<%
GetConnectionString
'CheckSecurity
%>
<!-- #include virtual = "/admin/login/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/login/includes/htmlElements.asp" ---------------------------------------------->
<!-- #include virtual = "/admin/login/includes/shell.asp" ---------------------------------------------------><%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const MENU_SELECTED = 0						'The menu number of the menu that should be in the "ON" state

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Home"
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
var preloadFlag = true;
var bCharts = false;


		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {

		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
	TEXT GOES HERE...
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

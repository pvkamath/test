<%
iBannerID = request("BannerID")
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

<script language="Javascript">
function OpenBannerWindow(p_sUrl, p_BannerID)
{
	window.open('redirectBanner.asp?BannerID=' + p_BannerID + '&URL=' + p_sUrl,'XIGWindow')
}
</script>

<body bgcolor="white" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">
<br>
<table cellpadding="0" cellspacing="0" border="0" align="center" valign="middle">
	<tr>
	<!--
		<td><a href="javascript: window.opener.location = 'https://www.bic.edu/admissions.asp'; window.close();"><img src="/media/images/popup_left.gif" width="185" height="195" alt="Apply Now for Spring Semester" border="0"></a></td>
		<td><a href="javascript: window.opener.location = 'https://www.bic.edu/admissions.asp'; window.close();"><img src="/media/images/popup_right.jpg" width="285" height="195" alt="Apply Now for Spring Semester" border="0"></a></td>
	-->
		<td align="center">
			<%
			call DisplayBanner(iBannerID)' sub routine found at the bottom of the page
			%>
		</td>
	</tr>
</table>

</body>
</html>


<%
'#####################################################################
'Name : GetServer
'Description : determines what server to use
'#####################################################################
function GetServer()
	sServerName = ucase(Request.ServerVariables("SERVER_NAME"))
	select case mid(sServerName,1,4)
	' www is the production server
		case "WWW."
			GetServer = "http://xigroup.com"
		case "STAG"
			GetServer = "http://staging.xigroup.com"
		case "DEV."
			GetServer = "http://dev.xigroup"
		case else
			if (instr(UCase(sServerName), "STAGING") > 0) then
				GetServer = "http://staging.xigroup.com"
			else			
				GetServer = "http://xigroup.com"
			end if
	end select	
end function

'#####################################################################
'Name : DisplayBanner
'Description : displays a banner on the page based upon the id
'#####################################################################
sub DisplayBanner(p_iBannerID)
	dim Banner, rs

	set rs = server.CreateObject("ADODB.RecordSet")
	set Banner = server.CreateObject("BannerFXv2Admin.ReportIt")
	
	Banner.connectstring = application("sXIGConnectionString")
	Banner.bannID = p_iBannerID
	Banner.SiteID = 1
	
	set rs = banner.GetBannerInfo
	set banner = nothing
	ImgPath = rs("ImgPath")
	AltTag = rs("alttag")
	URL = rs("ReferralURL")

	if not ucase(right(ImgPath, 4)) = "HTTP" then
		ImgPath = application("sDYNWebRoot") & "userMedia/Images/Banners/" & ImgPath
	end if	
	rs.Close
	set rs = nothing
	
	'create cookies
	sCookieName = "XIBANNER" & p_iBannerID
	Response.Cookies(sCookieName) = 1
	Response.Cookies(sCookieName).expires = #01/01/2036#
	
	response.write("<a href=""javascript:OpenBannerWindow('" & URL & "','" & p_iBannerID & "');javascript:window.close()""><img src=""" & GetServer() & ImgPath & """ alt=""" & AltTag & """ border=""0""></a>")
end sub
%>

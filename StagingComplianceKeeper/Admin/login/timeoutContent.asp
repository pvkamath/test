<!-- #include virtual = "/admin/includes/misc.asp" ------------------------------------------------------>
<!-- #include virtual = "/admin/security/includes/Security.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/contentManager.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" -------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" --------------------------------------------------->


<script language="vbscript" runat="server">
dim iContentID 'as integer
dim oContentAdmin 'as object
dim rsContent 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
dim bPassedDataIsGood 'as boolean
dim sKeywords 'as string	
dim sText 'as string
dim sHeadline 'as string
dim sImagePath 'as string
dim sImageName 'as string
dim iUserID 'as integer
dim oSecurity 'as object
dim bHasAdminAccessLevel 'as boolean
</script>
<%
	bPassedDataIsGood = true
	bDataReturned = false
	
	sText = getFormElement("text")
	sHeadline = getFormElement("headline")
	dPostingDate= getFormElement("postingdate")
	sImagePath = getFormElement("imagePath")
	sImageName = getFormElement("imageName")
	
	iContentID = getFormElement("contentid")
	sKeywords = getFormElement("keywords")
	iUserID = session("User_ID")

	if not isNumeric(iContentID) then
		Response.Write("This page needs a Content ID passed to it<br>")	
		bPassedDataIsGood = false
	end if
	if len(sKeywords) = 0 then
		Response.Write("This page needs a Keyword<br>")	
		bPassedDataIsGood = false
	end if
	
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		//	preloadImages();
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>

<script language="Javascript" src="includes/cookies.js" type="text/javascript"></script>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><span class="bigtitle">Your Session Has Expired.</span></td>
	<tr>
<script TYPE="text/javascript">
	var expireDate = new Date();
	expireDate.setTime(expireDate.getTime() + (1 * 24 * 60 * 60 * 1000));
	
	var entryText = "<% = Replace(sText, """", "\""") %>";
	var contentId = <% = iContentID %>;
	var headline = "<% = sHeadline %>";
	var postDate = "<% = dPostingDate %>";
	
	setCookie("Text", entryText, expireDate, "/");
	setCookie("ContentID", contentId, expireDate, "/");
	setCookie("Headline", headline, expireDate, "/");
	setCookie("PostingDate", postDate, expireDate, "/");
	
	var cookie = getCookie("Text");
	if (!cookie) {
		document.write("		<td><span>Content Manager <strong>was unable to store your data</strong> for future use.  The data from your last entry is printed below so that you may save it elsewhere and use it when you return.</span></td>");
	} else {
		document.write("		<td><span>However, <strong>your data has not been lost</strong>.  Content Manager will remember your last entry if you return to it within the next 24 hours.  The data saved from your last entry is printed below for your convenience.</span></td>");
	}
</script>
	<tr>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="20" alt="" border="0"></td>
	<tr>
		<td><span class="ActiveTitle">Recently Edited Content for <%= sKeywords %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"></td>
	</tr>
	<tr>
		<!-- dotted line -->
		<td background="/admin/login/media/images/dots-horizontal.gif"><img src="/admin/login/media/images/clear.gif" width="10" height="1" alt="" border="0"></td>
	</tr>
	<tr>
		<td><img src="/admin/login/media/images/clear.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" border="0">
	<tr>
		<td rowspan="5">&nbsp;</td>
		<td align="right" class="labelText">Posting Date:</td>
		<td rowspan="5">&nbsp;</td>
		<td width="100%" class="elementText"><%= dPostingDate%></td>
	</tr>
	<tr>
		<td align="right" class="labelText">Headline:</td>
		<td class="elementText"><%= sHeadline%></td>
	</tr>
	<tr>
		<td align="right" class="labelText" nowrap>Body Text:</td>
		<td class="elementText"><%= sText%></td>
	</tr>
<%

		if ucase(trim(sStatus)) = "PENDING" and bHasAdminAccessLevel then
%>
	
	<tr>
		<td>&nbsp;</td>
		<td colspan="4"><img src="/admin/login/media/images/clear.gif" width="10" height="10" border="0" hspace="0" vspace="0"><br>
			<input type="image" src="/admin/login/media/images/bttn-approve.gif" value="Approve" id=submit1 name=submit1></td>
	</tr>
<%
		end if
%>

</table>
<%
		'hide form if user does not have admin access level
		if bHasAdminAccessLevel then
%>
</form>
<%	
		end if
	'end if
'-------------------- END PAGE CONTENT ----------------------------------------------------------
%>
<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/StateDeadline.Class.asp" -------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/State.Class.asp" ---------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const MENU_NUMBER = 1
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim iDaysFrom 'Number of days from now the search range starts
dim iDaysTo 'Number of days from now the search range ends
dim sSearchType 'What type of search is this?  Expiration/pending/etc
dim iSearchDays 'Number of days specified in form
dim iSearchFrom 
dim iBranchId

if CheckIsBranchAdmin() then
	iBranchId = session("UserBranchId")
end if

'Determine the search type, and assign the appropriate date range if necessary
sSearchType = request("searchtype")
iSearchDays = request("expiringto") 
iSearchFrom = request("expiringfrom")
if isempty(sSearchType) then
	sSearchType = "numbered"
end if
if sSearchType = "numbered" then

	if isnumeric(iSearchFrom) and iSearchFrom <> "" then
		if iSearchFrom > 365 then
			iSearchFrom = 365
		elseif iSearchFrom < 1 then
			iSearchFrom = 1
		end if 
		iDaysFrom = cint(iSearchFrom)
	else
		iDaysFrom = 0
	end if 
	
	if isnumeric(iSearchDays) and iSearchDays <> "" then
		if iSearchDays > 365 then
			iSearchDays = 365
		elseif iSearchDays < 1 then
			iSearchDays = 1
		end if		
		iDaysTo = cint(iSearchDays)
	else
		iDaysTo = 30
	end if 

end if


dim sSearchText 'Holder for search text
dim sSearchLastName
dim iSearchBranchId

sSearchText = ScrubForSQL(request("SearchText"))
sSearchLastName = ScrubForSql(request("SearchLastName"))
if iBranchId <> "" then
	iSearchBranchId = iBranchId
else
	iSearchBranchId = ScrubForSql(request("BranchId"))
end if



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>

		<td width="100%" valign="top">

			<!-- Renewal Alerts -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> Course Expiration Status Tracking</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table border="0" cellpadding="5" cellspacing="0">
							<form name="90DaySearch" method="POST" action="dllist.asp">
							<tr>
								<td class="newstitle"><input type="hidden" name="searchtype" value="numbered">Expiring in <input type="text" name="ExpiringFrom" size="5" value="<% = iDaysFrom %>"> to <input type="text" name="ExpiringTo" size="5" value="<% = iDaysTo %>"> Days
								</td>
								<td><img src="/Media/Images/spacer.gif" width="40" height="17" alt="" border="0"></td>
								<td class="newstitle" nowrap>Course Name: </td>
								<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
								<td><input type="text" name="SearchText" size="20" value="<% = sSearchText %>"></td>
								<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
								<td valign="top" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" border="0"></td>
							</tr>
							<tr>
								<% if iBranchId <> "" then %>
								<td></td>
								<% else %>
								<td class="newstitle">Branch: &nbsp;<% call DisplayBranchesDropdown(iSearchBranchId, session("UserCompanyId"), false, 0) %></td>
								<% end if %>
								<td><img src="/Media/Images/spacer.gif" width="40" height="17" alt="" border="0"></td>
								<td class="newstitle" nowrap>Officer Name: </td>
								<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
								<td><input type="text" name="SearchLastName" size="20" value="<% = sSearchLastName %>"></td>
								<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
								<td></td>
							</tr>
							</form>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table border="0" cellpadding="10" cellspacing="0" width="100%">
							<tr>
								<td>
									<table width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim sOutputLine 'HTML to print depending on what is licensed in the state

dim oState
dim iStateId
set oState = new State
oState.ConnectionString = application("sDataSourceName")
oState.VocalErrors = application("bVocalErrors")

dim oDeadline
set oDeadline = new StateDeadline
oDeadline.ConnectionString = application("sDataSourceName")
oDeadline.TpConnectionString = application("sTpDataSourceName")
oDeadline.VocalErrors = application("bVocalErrors")
oDeadline.CompanyId = session("UserCompanyId")


'Restrict to the l2/l3/branch list for specific admins/viewers
if CheckIsCompanyL2Admin or CheckIsCompanyL2Viewer then
	oDeadline.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oDeadline.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin or CheckIsCompanyL3Viewer then
	oDeadline.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin or CheckIsBranchViewer then
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
end if


dim sSql
dim oConn
dim oOfficers
dim iOfficerCount
dim oRs

set oConn = Server.CreateObject("ADODB.Connection")
oConn.ConnectionString = application("sDataSourceName")
oConn.Open

sSql = "SELECT * FROM States ORDER BY State"
set oRs = oConn.Execute(sSql)

dim bFoundRecords 'Have we returned any officers at all?

do while not oRs.EOF 

	set oOfficers = oDeadline.SearchExpirationsByTime(now() + iDaysFrom, now() + iDaysTo, oRs("StateId"), iSearchBranchId, sSearchText, sSearchLastName)
	iOfficerCount = oOfficers.RecordCount

	if iOfficerCount <> 0 then
		bFoundRecords = true
		Response.Write(oRs("State") & ": <b>" & iOfficerCount & "</b> - ")
		%><a href="dlliststate.asp?searchtype=numbered&expiringfrom=<% = iDaysFrom %>&expiringto=<% = iDaysTo %>&stateid=<% = oRs("StateId") %>">View Officer List</a><br><%
	end if

	oRs.MoveNext

loop

set oOfficers = oDeadline.SearchExpirationsByTime(now() + iDaysFrom, now() + iDaysTo, 0, iSearchBranchId, sSearchText, sSearchLastName)
iOfficerCount = oOfficers.RecordCount

if iOfficerCount <> 0 then
	bFoundRecords = true
	Response.Write("All States: <b>" & iOfficerCount & "</b> - ")
	%><a href="dlliststate.asp?searchtype=numbered&expiringfrom=<% = iDaysFrom %>&expiringto=<% = iDaysTo %>&stateid=0">View Officer List</a><br><%
end if



'Print a message if we haven't found any expiring officers.
if not bFoundRecords then

	Response.Write("No expiring course records found in this range.")

end if
%>


												</table>
											</td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="5" height="5" alt="" border="0"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage															' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next
		
		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate
	
		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")
		
			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if
	
			iMonth = cint(p_month) - 1
			iYear = cint(p_year)
		
			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if
	
		if err then
			sReturn = ""
		end if
		
		sTestDate = p_month&"/"&sReturn&"/"&p_year
		
		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if
		
		getLastDayofmonth = sReturn
	
	end function
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const MENU_NUMBER = 1
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim iDaysFrom 'Number of days from now the search range starts
dim iDaysTo 'Number of days from now the search range ends
dim sSearchType 'What type of search is this?  Expiration/pending/etc
dim iSearchDays 'Number of days specified in form
dim iSearchFrom 
dim bShowOfficers 'Do we return officer licenses in the results
dim bShowBranches 'Do we return branch licenses in the results
dim bShowCompanyL2 'Do we return companyL2 licenses in the results
dim bShowCompanyL3 'Do we return companyL3 licenses in the results
dim bShowCompany 'Do we return company licenses in the results

dim sSearchName 'Holder for search text
dim sSearchLicenseNum
dim iSearchBranchId
dim iSearchStateId

sSearchName = ScrubForSQL(request("SearchOfficerName"))
sSearchLicenseNum = ScrubForSql(request("SearchLicenseNum"))
iSearchBranchId = ScrubForSql(request("BranchId"))
iSearchStateId = ScrubForSql(request("StateId"))

'Determine which licenses to show
bShowOfficers = request("showOfficers")
bShowBranches = request("showBranches")
bShowCompanyL2 = request("showCompanyL2")
bShowCompanyL3 = request("showCompanyL3")
bShowCompany = request("showCompany")
'Show all the licenses if none are selected
if bShowOfficers = "" and bShowBranches = "" and bShowCompany = "" and bShowCompanyL2 = "" and bShowCompanyL3 = "" then
	bShowOfficers = true
	bShowBranches = true
	bShowCompany = true
	bShowCompanyL2 = true
	bShowCompanyL3 = true
end if


'Determine the search type, and assign the appropriate date range if necessary
sSearchType = request("searchtype")
iSearchDays = request("expiringto") 
iSearchFrom = request("expiringfrom")
if isempty(sSearchType) then
	sSearchType = "numbered"
end if
if sSearchType = "numbered" then

	if isnumeric(iSearchFrom) and iSearchFrom <> "" then
		if iSearchFrom > 365 then
			iSearchFrom = 365
		elseif iSearchFrom < 1 then
			iSearchFrom = 1
		end if 
		iDaysFrom = cint(iSearchFrom)
	else
		iDaysFrom = 0
	end if 
	
	if isnumeric(iSearchDays) and iSearchDays <> "" then
		if iSearchDays > 365 then
			iSearchDays = 365
		elseif iSearchDays < 1 then
			iSearchDays = 1
		end if		
		iDaysTo = cint(iSearchDays)
	else
		iDaysTo = 30
	end if 

end if


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>

		<td width="100%" valign="top">

			<!-- Renewal Alerts -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> License Status Tracking</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table border="0" cellpadding="5" cellspacing="0">
							<tr>
								<form name="90DaySearch" method="POST" action="list.asp">
								<td width="50%" valign="top" class="newstitle">Search Licenses:<br>
								<img src="/Media/Images/spacer.gif" width="1" height="3" alt="" border="0"><br>
								<input type="radio" name="searchtype" value="numbered"<% if sSearchType = "numbered" or sSearchType = "" then %>CHECKED<% end if %>>Expiring in <input type="text" name="ExpiringFrom" size="5" value="<% = iDaysFrom %>"> to <input type="text" name="ExpiringTo" size="5" value="<% = iDaysTo %>"> Days<br>
								<input type="radio" name="searchtype" value="pendingrenewal"<% if sSearchType = "pendingrenewal" then %>CHECKED<% end if %>>Pending License Renewals<br>
								<input type="radio" name="searchtype" value="pendingnew"<% if sSearchType = "pendingnew" then %>CHECKED<% end if %>>Pending New Licenses<br>
								<input type="radio" name="searchtype" value="active"<% if sSearchType = "active" then %>CHECKED<% end if %>>Active Licenses
								<p>
								<input type="checkbox" name="showOfficers" value="1"<% if bShowOfficers then %> CHECKED<% end if %>> Show Officer Licenses
								<br><input type="checkbox" name="showBranches" value="1"<% if bShowBranches then %> CHECKED<% end if %>> Show Branch Licenses
								<br><input type="checkbox" name="showCompanyL2" value="1"<% if bShowCompanyL2 then %> CHECKED<% end if %>> Show <% = session("CompanyL2Name") %> Licenses
								<br><input type="checkbox" name="showCompanyL3" value="1"<% if bShowCompanyL3 then %> CHECKED<% end if %>> Show <% = session("CompanyL3Name") %> Licenses
								<br><input type="checkbox" name="showCompany" value="1"<% if bShowCompany then %> CHECKED<% end if %>> Show <% = session("CompanyName") %> Licenses
								</td>
								<td><img src="/Media/Images/spacer.gif" width="25" height="17" alt="" border="0"></td>
								<td valign="top">
									<table cellpadding="5" cellspacing="0" border="0">
										<tr>
											<td class="newstitle" nowrap>License Number: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="SearchLicenseNum" size="20" value="<% = sSearchLicenseNum %>"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Officer Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="SearchOfficerName" size="20" value="<% = sSearchName %>"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Branch: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% call DisplayBranchesDropdown(iSearchBranchId, session("UserCompanyId"), false, 0) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% call DisplayStatesDropdown(iSearchStateId, 0, "StateId") %></td>
										</tr>
										<tr>
											<td colspan="3"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" border="0"></td>
										</tr>
									</table>
								</td>
								</form>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table border="0" cellpadding="10" cellspacing="0" width="100%">
							<tr>
								<td>
									<table width="100%" border=0 cellpadding=4 cellspacing=0>
<%
dim sOutputLine 'HTML to print depending on what is licensed in the state
dim oLicense 'License object
dim oRs 'Recordset object


'Initialize our License object to search based on the current state, chosen
'date range, licensing type, etc.
set oLicense = new License
oLicense.VocalErrors = application("bVocalErrors")
oLicense.ConnectionString = application("sDataSourceName")
'oLicense.LicenseStateId = session("StateId")
'oLicense.SearchOwnerTypesId = 7'oPreference.LicensingTypeId


'Assign the license properties based on the search type.
if sSearchType = "numbered" then
	oLicense.SearchLicenseExpDateFrom = now() + iDaysFrom
	oLicense.SearchLicenseExpDateTo = now() + iDaysTo
	oLicense.LicenseStatusId = 1
elseif sSearchType = "pendingrenewal" then
	oLicense.LicenseStatusId = 10
	oLicense.SearchLicenseExpDateFrom = now()
elseif sSearchType = "pendingnew" then
	oLicense.LicenseStatusId = 0
	oLicense.SearchLicenseExpDateFrom = now()
elseif sSearchType = "active" then
	oLicense.LicenseStatusId = 1
	oLicense.SearchLicenseExpDateFrom = now()
end if
if sSearchLicenseNum <> "" then
	oLicense.LicenseNum = sSearchLicenseNum
end if 
if sSearchName <> "" then
	oLicense.SearchLastName = sSearchName
end if
if iSearchBranchId <> "" then
	oLicense.SearchBranchId = iSearchBranchId
end if
if iSearchStateId <> "" then
	oLicense.LicenseStateId = iSearchStateId
end if

'Restrict the search based on the chosen owner types
if bShowOfficers then
	oLicense.SearchOwnerTypeIdList = oLicense.SearchOwnerTypeIdList & "1, "
end if
if bShowBranches then
	oLicense.SearchOwnerTypeIdList = oLicense.SearchOwnerTypeIdList & "2, "
end if
if bShowCompany	then
	oLicense.SearchOwnerTypeIdList = oLicense.SearchOwnerTypeIdList & "3, "
end if
if bShowCompanyL2 then
	oLicense.SearchOwnerTypeIdList = oLicense.SearchOwnerTypeIdList & "4, "
end if
if bShowCompanyL3 then
	oLicense.SearchOwnerTypeIdList = oLicense.SearchOwnerTypeIdList & "5, "
end if

'if bShowOfficers and bShowBranches and bShowCompany then
'	oLicense.SearchOwnerTypesId = 7
'elseif bShowOfficers and bShowBranches then
'	oLicense.SearchOwnerTypesId = 6
'elseif bShowOfficers and bShowCompany then
'	oLicense.SearchOwnerTypesId = 5
'elseif bShowBranches and bShowCompany then
'	oLicense.SearchOwnerTypesId = 4
'elseif bShowOfficers then
'	oLicense.SearchOwnerTypesId = 1
'elseif bShowBranches then
'	oLicense.SearchOwnerTypesId = 2
'elseif bShowCompany then
'	oLicense.SearchOwnerTypesId = 3
'end if

'Restrict to the l2/l3/branch list for specific admins/viewers
if CheckIsCompanyL2Admin or CheckIsCompanyL2Viewer then
	oLicense.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oLicense.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oLicense.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin or CheckIsCompanyL3Viewer then
	oLicense.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oLicense.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin or CheckIsBranchViewer then
	oLicense.SearchBranchIdList = GetUserBranchIdList()
end if

'Retrieve the licenses
set oRs = oLicense.SearchLicenses


'Set the page size based on the user-selected values.
oRs.PageSize = 40
oRs.CacheSize = 40
iPageCount = oRs.PageCount

'Get page number of which records are showing
dim iCurPage
dim iPageCount
iCurPage = trim(request("page_number"))
if (iCurPage = "") then
	if (trim(Session("CurPage")) <> "") then
		iCurPage = Session("CurPage")
	end if
else
	Session("CurPage") = iCurPage
end if

'Determine which search page the user has requested
if iCurPage = "" then iCurPage = 1

if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
if clng(iCurPage) <= 0 then iCurPage = 1
		



dim bCol 'Boolean to track which column we're on
dim sColor 'Cell background color value
dim iCount 'Track our position within the recordset page


dim oAssociate 'Loan officer class
dim oCompany 'Company class
dim oCompanyL2 'CompanyL2 class
dim oCompanyL3 'CompanyL3 class
dim oBranch 'Branch class

'Initialize the Associate, Branch, and Company classes in order to retrieve our 
'loan officer/company/branch information for each license.
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = false 'application("bVocalErrors")

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = false 'application("bVocalErrors")

set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = false

set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = false

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = false 'application("bVocalErrors")

if (oRs.BOF and oRs.EOF) then
%>
													<tr>
														<td colspan="2" width="100%" align="left">&nbsp;
														No
														<%
														if sSearchType = "pendingrenewal" or sSearchType = "pendingnew" then
															Response.Write(" pending ")
														end if 
														%>
														licenses
														<%
														if sSearchName <> "" then
															Response.Write("with the text """ & sSearchName & """ ")
														end if
														if sSearchType = "numbered" then
															if iDaysFrom = 0 then
																Response.Write("expiring in " & iDaysTo & " day")
																if iDaysTo > 1 then 
																	Response.Write("s")
																end if 
															else														
																Response.Write("expiring in " & iDaysFrom & " - " & iDaysTo & " days")
															end if 
														end if
														%>
														</td>
													</tr>
<%
else

	'Set the beginning record to be displayed on the page
	oRs.AbsolutePage = iCurPage

	'Initialize the row/col style values
	bCol = false
	sColor = ""

	iCount = 0
	
	'sponse.Write("X" & oRs.PageSize & "X<p>")
	do while (iCount < oRs.PageSize) and (not oRs.EOF)
	
		'esponse.Write(iCount & ": " & oRs("LicenseNum") & "<br>")

		'We won't output anything if this isn't a valid owner ID.
		sOutputLine = ""	

		'We redirect to the appropriate detail pages based on what kind of owner holds this license.
		if oRs("OwnerTypeId") = 1 then
		
			'Make sure the associate this license belongs to is still valid.
			if not oAssociate.LoadAssociateById(oRs("OwnerId")) = 0 then
				sOutputLine = "<a href=""/officers/officerdetail.asp?id=" & oRs("OwnerId") & "&ls=" & oRs("LicenseStateId") & """>(" & oRs("LicenseNum") & ")</a> " & oAssociate.FullName & " - Officer"
			end if 
			
		elseif oRs("OwnerTypeId") = 3 then
		
			'Make sure the company this license belongs to is still valid.			
			if not oCompany.LoadCompanyById(oRs("OwnerId")) = 0 then
				sOutputLine = "<a href=""/company/companydetail.asp?id=" & oRs("OwnerId") & "&ls=" & oRs("LicenseStateId") & """>(" & oRs("LicenseNum") & ")</a> " & oCompany.Name
			end if
			
		elseif oRs("OwnerTypeId") = 2 then
		
			'Make sure the company branch this license belongs to is still valid.
			if not oBranch.LoadBranchById(oRs("OwnerId")) = 0 then
				sOutputLine = "<a href=""/branches/branchdetail.asp?branchid=" & oRs("OwnerId") & "&ls=" & oRs("LicenseStateId") & """>(" & oRs("LicenseNum") & ")</a> " & oBranch.BranchName & " - Company Branch"
			end if 

		elseif oRs("OwnerTypeId") = 4 then
		
			'Make sure the company this license belongs to is still valid.			
			if not oCompanyL2.LoadCompanyById(oRs("OwnerId")) = 0 then
				sOutputLine = "<a href=""/branches/companyl2detail.asp?id=" & oRs("OwnerId") & "&ls=" & oRs("LicenseStateId") & """>(" & oRs("LicenseNum") & ")</a> " & oCompanyL2.Name
			end if

		elseif oRs("OwnerTypeId") = 5 then
		
			'Make sure the company this license belongs to is still valid.			
			if not oCompanyL3.LoadCompanyById(oRs("OwnerId")) = 0 then
				sOutputLine = "<a href=""/branches/companyl3detail.asp?id=" & oRs("OwnerId") & "&ls=" & oRs("LicenseStateId") & """>(" & oRs("LicenseNum") & ")</a> " & oCompanyL3.Name
			end if
			
		end if

	
		if sOutputLine <> "" then

			if not bCol then
	%>
														<tr>
															<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% = sOutputLine %></td>
															<td width="4%"></td>
	<%
			else
	%>
															<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% = sOutputLine %></td>
														</tr>
	<%

			end if

			'Set the col to the opposite value
			if bCol = false then
				bCol = true
			else
				bCol = false
				'Every two columns, set the row to the opposite color
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
			end if
		
		end if
	
		oRs.MoveNext
		iCount = iCount + 1

	loop
	
	'esponse.Write("X" & iCount & "X" & oRs.EOF & "X")

	'Finish off the table row if we ended on a left column.  Note that we check
	'for the positive bCol because the value is flipped at the end of the loop.
	if bCol then
%>
														<td width="48%" align="center">&nbsp;</td>
													</tr>
<%
	end if
	
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Get the current querystring and strip and page references
		dim asQueryString, sQueryItem, sQueryString
		if Request.QueryString <> "" then 
			asQueryString = split(Request.QueryString, "&")
		else
			asQueryString = split(Request.Form, "&")
		end if 
		for each sQueryItem in asQueryString
		
			if left(sQueryItem, 11) <> "page_number" then
			
				sQueryString = sQueryString & sQueryItem & "&"
			
			end if 
				
		next
		
	
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""list.asp?" & sQueryString & "page_number=" & iCurPage - 1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""list.asp?" & sQueryString & "page_number=" & iCurPage + 1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		%>
											</td>
										</tr>
		<%


end if

set oRs = nothing
%>
												</table>
											</td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="5" height="5" alt="" border="0"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage															' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next
		
		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate
	
		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")
		
			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if
	
			iMonth = cint(p_month) - 1
			iYear = cint(p_year)
		
			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if
	
		if err then
			sReturn = ""
		end if
		
		sTestDate = p_month&"/"&sReturn&"/"&p_year
		
		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if
		
		getLastDayofmonth = sReturn
	
	end function
%>
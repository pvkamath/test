<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- include virtual="/includes/StateCandidate.Class.asp" -------------------->
<!-- include virtual="/includes/StatePreferences.Class.asp" ------------------>

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that we have already set up a state ID.  
'if session("StateId") = "" then
'	
'	Response.Write("State Reference Required.")
'	Response.End
'
'end if 
'session("UserCompanyId") = -1


'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


dim oBranch
set oBranch = new Branch
oBranch.VocalErrors = application("bVocalErrors")
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.CompanyId = session("UserCompanyId")


'Verify that the user has logged in.
'CheckIsLoggedIn()

'dim oPreference
'set oPreference = new StatePreference
'oPreference.ConnectionString = application("sDataSourceName")
'oPreference.LoadPreferencesByStateId(session("StateId"))


	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

		<td>
			<table width=760 bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
						<!-- Listed Branches -->
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="100%">
									<table bgcolor="FFFFFF" border=0 cellpadding=10 cellspacing=0>
										<tr bgcolor="#8A99B6">
											<td><img src="/Media/Images/photo-companies.jpg" width="40" height="40" alt="" border="0"></td>
											<td width="100%"><span class="sectiontitle">Company Branches</span><br>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td class="newstitle" nowrap>Search Branches</td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<form name="CompanySearch" method="POST" action="branchsearch.asp">
														<td><input type="text" name="BranchSearchText" size="20" value="<% = ScrubForSql(request("BranchSearchText")) %>"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
														</form>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" bgcolor="FFFFFF" border=0 cellpadding=4 cellspacing=0>
<%
'Retrieve form data
dim sBranchSearchText
dim iSearchBranchId
sBranchSearchText = ScrubForSql(trim(request("BranchSearchText")))
iSearchBranchId = ScrubForSql(trim(request("BranchId")))

'Declare the recordset object
dim oRs
set oRs = Server.CreateObject("ADODB.Recordset")
'set oRs = oCompany.Branches.GetBranches(oCompany.CompanyId)
set oRs = oBranch.SearchBranches


'Define the variables we'll use for tracking row/column styles below
dim sColor
dim bCol

'dim oRs
'dim oConn
'dim sSql 

'if not sCompanySearchText = "" then

'set oRs = Server.CreateObject("ADODB.Recordset")

'set oConn = Server.CreateObject("ADODB.Connection")
'oConn.ConnectionString = application("sDataSourceName")
'oConn.Open

'sSql = "SELECT CompanyId, Name FROM Companies " & _
'       "WHERE (OwnerStateId = '" & session("StateID") & "' " & _
'       "OR OwnerStateId = '0') " & _
'       "AND Name LIKE '%" & sCompanySearchText & "%'"
'set oRs = oConn.Execute(sSql)

if (oRs.EOF and oRs.BOF) then

%>
													<tr>
														<td colspan="2" width="100%" align="center">No Branches Found</td>
													</tr>
<%

else

	'Initialize the row/col values
	bCol = false
	sColor = ""
	
	dim iResult
	
	do while not oRs.EOF
	
	
		iResult = oBranch.LoadBranchById(oRs("BranchId"))
		
	
		if sBranchSearchText <> "" and instr(1,ucase(oBranch.Name),ucase(sBranchSearchText)) = 0 then 
		
		else
	
			if not bCol then
%>
													<tr>
														<td width="48%" align="left"<% = sColor %>>
														<% if iSearchBranchId = cstr(oBranch.BranchId) then %>
														&nbsp;<b><% = oBranch.Name %></b> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail"></a></td>
														<% else %>
														&nbsp;<a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><% = oBranch.Name %></a> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail"></a></td>
														<% end if %>
														<td width="4%"></td>
<%
			else
%>
														<td width="48%" align="left"<% = sColor %>>
														<% if iSearchBranchId = cstr(oBranch.BranchId) then %>
														&nbsp;<b><% = oBranch.Name %></b> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail"></a></td>
														<% else %>
														&nbsp;<a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><% = oBranch.Name %></a> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail"></a></td>
														<% end if %>
													</tr>
<%
			end if
		
			'Set the col to the opposite value
			if bCol = false then
				bCol = true
			else
				bCol = false
				'Every two columns, set the row to the opposite color
				if sColor = "" then
					sColor = " bgcolor=""#e1e1e1"""
				else
					sColor = ""
				end if
			end if
			
		end if 
	
		oRs.MoveNext
	
	loop

	'Finish off the table row if we ended on a left column.  Note that we check
	'for the positive bCol because the value is flipped at the end of the loop.
	if bCol then
%>
														<td width="48%">&nbsp;</td>
													</tr>
<%
	
end if 

'oConn.Close
'set oConn = nothing

end if

%>
													</table>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

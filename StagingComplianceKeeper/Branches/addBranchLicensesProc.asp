<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" -->
<%
dim oRsCKlicense

for iCount = 0 to ubound(sCSVData, 2)	
	
		'RecordType 7 means this is a branch update
		
		'Pull the company values from the array
        sAddress = ScrubForSql(sCsvData(3, iCount))
        sCity = ScrubForSql(sCsvData(4, iCount))
        iStateID = GetStateIdByAbbrev(ScrubForSql(sCsvData(5, iCount)))
        sZipCode = ScrubForSql(sCsvData(7, iCount))
		sLicenseNum = ScrubForSql(sCsvData(10, iCount))
		sLicenseType = ScrubForSql(sCsvData(11, iCount))
        iLicenseStateId = GetStateIdByAbbrev(MID(sLicenseType,1,2))            
        iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(13, iCount)))
        dLicenseStatusDate = ScrubForSql(sCsvData(14, iCount))
		iExpirationYear = ScrubForSql(sCsvData(15, iCount))
	    iLicenseOwnerTypeId = 2

        if IsNull(sLicenseNum) or IsEmpty(sLicenseNum) or sLicenseNum="" then
            sLicenseNum="0"
        end if
		'Check if this is a blank array entry
		if not isnumeric(ScrubForSql(sCsvData(0, iCount))) then			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Missing or invalid branch information.<br>")
					
		else
	
			'Attempt to load the branch object
			set oBranch = new Branch
			oBranch.ConnectionString = application("sDataSourceName")
			oBranch.VocalErrors = false 'application("bVocalErrors")

            if sZipcode <> "" then
                sZipcode = Right("00000" & sZipcode, 5)
            end if

			if oBranch.LoadBranchByAddress(sAddress, sCity, iStateID, sZipcode) = 0 then
				bErrors = true
				Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Unable to load branch information for the record.<br>")
			end if
				
			if not bErrors then
                oLicense.ReleaseLicense
			    oLicense.OwnerTypeId = iLicenseOwnerTypeID
			    oLicense.OwnerCompanyId = session("UserCompanyId")
			    oLicense.OwnerId = oBranch.BranchID
                oLicense.LicenseStateID = iLicenseStateID
                oLicense.LicenseType = sLicenseType

                dim tLicenseID
                set oRsCKlicense = oLicense.SearchLicensesV2
				if safecstr(oLicense.LicenseExpDate) <> safecstr(dLicenseExpDate) or _
                    safecstr(oLicense.LicenseStatusDate) <> safecstr(dLicenseStatusDate) or _
					safecstr(oLicense.LicenseAppDeadline) <> safecstr(dLicenseAppDeadline) or _
					safecstr(oLicense.LicenseStatusId) <> safecstr(iLicenseStatusId) then
					oLicense.Triggered30 = 0
					oLicense.Triggered60 = 0
					oLicense.Triggered90 = 0
				end if
                if not (oRsCKlicense.EOF and oRsCKlicense.BOF) then
                    'Update the company properties
                    bUpdate = true
                    tLicenseID = oRsCKlicense("LicenseID")
                else
                    bUpdate = false
                    tLicenseID = 0
				end if
				oLicense.LicenseType = sLicenseType
				oLicense.LegalName = oBranch.LegalName
				oLicense.LicenseNum = sLicenseNum
                oLicense.LicenseStatusID = iLicenseStatusId
                oLicense.LicenseStatusDate = dLicenseStatusDate
                IF iLicenseStatusId= "2" THEN
                    if IsNumeric(iExpirationYear) then
                        oLicense.LicenseExpDate = "12/31/" & iExpirationYear
                    else
                        oLicense.LicenseExpDate = iExpirationYear
                    end if
                ELSE
                    oLicense.LicenseExpDate = dLicenseExpDate                  
                END IF 

				if oLicense.SaveLicense(bUpdate,tLicenseID)  <> 0 then
					Response.Write("<li>Updated " & oLicense.LicenseNum & "<br>")	 			
				else
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License Record): Could not save the license information.<br>")
				end if	
			end if				
		end if
	
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false

next 

'Clean up

if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The import was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
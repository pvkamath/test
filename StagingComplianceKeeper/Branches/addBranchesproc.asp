<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" -->
<%

for iCount = 0 to ubound(sCSVData, 2)	
		'RecordType 7 means this is a branch update
		
		'Pull the company values from the array
		sName = ScrubForSql(sCsvData(2, iCount))
		sBranchNumber = ScrubForSql(sCsvData(1, iCount))
		sLegalName = ScrubForSql(sCsvData(3, iCount))
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(4, iCount)))
		iCompanyL3Id = oCompanyL3.LookupCompanyIdByName(ScrubForSql(sCsvData(4, iCount)))
		sAddress = ScrubForSql(sCsvData(5, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(6, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(7, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(8, iCount)))
		sZipcode = ScrubForSql(sCsvData(9, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(10, iCount))
		sPhone = ScrubForSql(sCsvData(11, iCount))
		sPhoneExt = ScrubForSql(sCsvData(12, iCount))
		sFax = ScrubForSql(sCsvData(13, iCount))
		sMailingAddress = ScrubForSql(sCsvData(14, iCount))
		sMailingAddress = replace(sMailingAddress, """", "")
		sMailingAddress2 = ScrubForSql(sCsvData(15, iCount))
		sMailingAddress2 = replace(sMailingAddress2, """", "")
		sMailingCity = ScrubForSql(sCsvData(16, iCount))
		iMailingStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(17, iCount)))
		sMailingZipcode = ScrubForSql(sCsvData(18, iCount))
		sMailingZipcodeExt = ScrubForSql(sCsvData(19, iCount))
		sEntityLicNum = ScrubForSql(sCsvData(20, iCount))
		sFhaBranchId = ScrubForSql(sCsvData(21, iCount))
		sVaBranchId = ScrubForSql(sCsvData(22, iCount))

	
		'Check if this is a blank array entry
		if sName = "" then			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Missing or invalid branch information.<br>")
					
		else
	
			'Attempt to load the branch object
			set oBranch = new Branch
			oBranch.ConnectionString = application("sDataSourceName")
			oBranch.VocalErrors = false 'application("bVocalErrors")
			
			if oBranch.LoadBranchByName(sName) = 0 then
				bUpdate = false
			else
				if not CheckIsThisBranchAdmin(oBranch.BranchId) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Unable to load the branch record.<br>")
				end if
				bUpdate = true
			end if
				

			if not bErrors then
				'Update the company properties
				oBranch.CompanyId = session("UserCompanyId")
				oBranch.BranchName = sName
				oBranch.BranchNum = sBranchNumber
				oBranch.LegalName = sLegalName
				if iCompanyL3Id <> 0 then
					oBranch.CompanyL2Id = ""
					oBranch.CompanyL3Id = iCompanyL3Id
				elseif iCompanyL2Id <> 0 then
					oBranch.CompanyL2Id = iCompanyL2Id
					oBranch.CompanyL3Id = ""
				else
					oBranch.CompanyL2Id = ""
					oBranch.CompanyL3Id = ""
				end if	
				oBranch.Address = sAddress
				oBranch.Address2 = sAddress2 
				oBranch.City = sCity 
				oBranch.StateId = iStateId 
				oBranch.Zipcode = Right("00000" & sZipcode, 5)
				oBranch.ZipcodeExt = sZipcodeExt
				oBranch.Phone = sPhone 
				oBranch.PhoneExt = sPhoneExt 
				oBranch.Fax = sFax 
				oBranch.MailingAddress = sMailingAddress 
				oBranch.MailingAddress2 = sMailingAddress2
				oBranch.MailingCity = sMailingCity 
				oBranch.MailingStateId = iMailingStateId 
                if sMailingZipcode <> "" then
				    oBranch.MailingZipcode = Right("00000" & sMailingZipcode, 5)
                end if
				oBranch.MailingZipcodeExt = sMailingZipcodeExt 
				oBranch.EntityLicNum = sEntityLicNum
				oBranch.FhaBranchId = sFhaBranchId
				oBranch.VaBranchId = sVaBranchId
				
				if oBranch.SaveBranch <> 0 then
					Response.Write("<li>Updated " & oBranch.BranchName & "<br>")	 			
				else
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Could not save the branch information.<br>")
				end if	
			end if				
		end if
	
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false

next 

'Clean up
set oAssociate = nothing

if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The import was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
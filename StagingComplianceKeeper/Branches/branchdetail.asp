<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/Manager.Class.asp" ----------------------------->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" ------------------------>

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim iCompanyId
iCompanyId = session("UserCompanyId")


'Make sure we were passed a valid BranchId
dim oBranch
set oBranch = new Branch
oBranch.VocalErrors = application("bVocalErrors")
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")

dim iBranchId
iBranchId = ScrubForSql(request("BranchId"))

if oBranch.LoadBranchById(iBranchID) = 0 then

	AlertError("This page requires a valid Branch ID.")
	Response.End
	
end if
	

'Make sure that the user has access to this Branch
dim bCheckIsThisBranchAdmin
dim bCheckIsThisBranchViewer
if not CheckIsThisBranchAdmin(oBranch.BranchId) then
	if not CheckIsThisBranchViewer(oBranch.BranchId) then
		AlertError("This page requires a valid Branch ID.")
		Response.End
	else
		bCheckIsThisBranchViewer = true
	end if
else
	bCheckIsThisBranchAdmin = true
end if


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmBranchDelete(p_iBranchId,p_sBranchName)
{
	if (confirm("Are you sure you wish to delete the following Branch:\n  " + p_sBranchName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/branches/deletebranch.asp?id=' + p_iBranchId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletenote.asp?id=' + p_iNoteId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}
function ConfirmManagerDelete(p_iManagerId)
{
	if (confirm("Are you sure you wish to delete this Manager?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/branches/modmanagerproc.asp?delete=1&managerid=' + p_iManagerId;
}
function OpenMoveLicWindow(p_iLicenseId,p_sTextbox) {
	window.open("/officers/MoveLicWin.asp?lid=" + p_iLicenseId + "&textbox=" + p_sTextbox , "ImageWindow", "width=500,height=300,scrollbars=1")
}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Branch Detail -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Profile: <% = oBranch.BranchName %><% if oBranch.BranchNum <> "" then %> (#<% = oBranch.BranchNum %>)<% end if %></td>
				</tr>				

						<% if CheckIsAdmin() then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modbranch.asp?id=<% = oBranch.BranchId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Branch Information" alt="Edit Branch Information"> &nbsp;Edit Branch</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmBranchDelete(<% = oBranch.BranchId %>,'<% = oBranch.Name %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This Branch" alt="Delete This Branch"> Delete Branch</a></td>
										<td nowrap align="left" valign="center"><a href="mergebranch.asp?id=<% = oBranch.BranchId %>&type=2" class="nav"><img src="<% = application("sDynMediaPath") %>bttnMove.gif" align="top" border="0" title="Merge Branch" alt="Merge Branch"> &nbsp;Merge Branch</a></td>
										<form name="BranchSearch" method="POST" action="branchdetail.asp">
										<td><% call DisplayBranchesDropdown(oBranch.BranchId, iCompanyId, false, 3) %></td>
										<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0"></td>
										</form>
									</tr>
								</table>
							</td>
						</tr>
						<% elseif CheckIsThisBranchAdmin(oBranch.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modbranch.asp?id=<% = oBranch.BranchId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Branch Information" alt="Edit Branch Information"> &nbsp;Edit Branch</a></td>
										<td align="left" valign="center"></td>
									</tr>
								</table>
							</td>
						</tr>						
						<% end if %>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Legal Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.LegalName %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.Phone %> <% if oBranch.PhoneExt <> "" then %> Ext. <% = oBranch.PhoneExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.Fax %></td>
										</tr>
										<!--
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><%' = oBranch.Email %></td>
										</tr>
										-->
										<tr>
											<td class="newstitle" nowrap>Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.Address %></td>
										</tr>
										<% if not oBranch.Address2 = "" then %>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.Address2 %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.City %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.LookupState(oBranch.StateId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.Zipcode %><% if oBranch.ZipcodeExt <> "" then %> - <% oBranch.ZipcodeExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>NMLS Number: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.NMLSNumber %></td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<% if session("CustField1Name") <> "" and oBranch.CustField1 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField1Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.CustField1 %></td>
										</tr>
										<% end if %>
										<% if session("CustField2Name") <> "" and oBranch.CustField2 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField2Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.CustField2 %></td>
										</tr>
										<% end if %>
										<% if session("CustField3Name") <> "" and oBranch.CustField3 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField3Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.CustField3 %></td>
										</tr>
										<% end if %>
										<% if session("CustField4Name") <> "" and oBranch.CustField4 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField4Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.CustField4 %></td>
										</tr>
										<% end if %>
										<% if session("CustField5Name") <> "" and oBranch.CustField5 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField5Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.CustField5 %></td>
										</tr>
										<% end if %>
									</table>
											</td>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0" valign="top">
										<tr>
											<td valign="top" class="newstitle" nowrap>Branch is Inactive: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% if oBranch.Inactive then response.write("Yes") else response.write("No") end if %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Inactive Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><%= oBranch.InactiveDate %></td>
										</tr>									
										<tr>
											<td valign="top" class="newstitle" nowrap>Mailing Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.MailingAddress %><br>
											<% if not oBranch.MailingAddress2 = "" then %><% = oBranch.MailingAddress2 %><br><% end if %>
											<% = oBranch.MailingCity %><br>
											<% = oBranch.LookupState(oBranch.MailingStateId) %><br>
											<% = oBranch.MailingZipcode %>
											</td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Entity License Number: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.EntityLicNum %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>FHA Branch ID: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.FhaBranchId %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>VA Branch ID: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oBranch.VaBranchId %></td>
										</tr>			
										<%
										dim oManager
										set oManager = new Manager
										oManager.ConnectionString = application("sDataSourceName")
										oManager.VocalErrors = application("bVocalErrors")
										oManager.CompanyId = session("UserCompanyId")
										oManager.OwnerId = oBranch.BranchId
										oManager.OwnerTypeId = 1
										set oRs = oManager.SearchManagers
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Branch Managers: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oManager.LoadManagerById(oRs("ManagerId")) <> 0 then
												
													%>
													<% if bCheckIsThisBranchAdmin then %>
													<a href="javascript:ConfirmManagerDelete(<% = oManager.ManagerId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Manager" alt="Delete This Manager" align="top"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/branches/modmanager.asp?id=<% = oManager.ManagerId %>"><% = oManager.FullName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsThisBranchAdmin then %>
											<a href="/branches/modmanager.asp?otid=1&oid=<% = oBranch.BranchId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Manager" alt="Add New Manager"> Add New Manager</a>
											<% end if %>
											</td>
										</tr>
																			
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										dim oAssociateDoc
										set oAssociateDoc = new AssociateDoc
										oAssociateDoc.ConnectionString = application("sDataSourceName")
										oAssociateDoc.VocalErrors = application("bVocalErrors")
										oAssociateDoc.CompanyId = session("UserCompanyId")
										oAssociateDoc.OwnerId = oBranch.BranchId
										oAssociateDoc.OwnerTypeId = 2
										set oRs = oAssociateDoc.SearchDocs
										
										'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<% if bCheckIsThisBranchAdmin then %>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document" align="top"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsThisBranchAdmin then %>
											<a href="/officers/moddocument.asp?otid=2&oid=<% = oBranch.BranchId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											<% end if %>
											</td>
										</tr>
									</table>
										</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
						
			
						<!-- Branch Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Branch Licenses</td>
				</tr>

						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modlicense.asp?bid=<% = oBranch.BranchId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
							<tr>
								<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>
<%
dim oLicense
dim oLicRs
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oBranch.BranchId
oLicense.OwnerTypeId = 2


set oRs = oLicense.SearchLicensesByState()

if not (oRs.EOF and oRs.BOF) then
	%>
	<table width="100%" cellpadding="5" cellspacing="0" border="0">
		<tr>
			<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then %>
			<td></td>
			<td></td>
			<td></td>
			<% end if %>
			<td><b>License Number</b></td>
			<td><b>License Type</b></td>
			<td><b>License Status</b></td>
			<td><b>Expiration Date</b></td>
            <td><b>Filing Schedule</b></td>
		</tr>
	<%
	do while not oRs.EOF
	    %>
            <tr>
                <td colspan="100%">
		<%
	
		Response.Write("<b>(" & oRs("LicenseCount") & ") " & oLicense.LookupState(oRs("LicenseStateId")))
		if oRs("LicenseCount") > 1 then 
			Response.Write(" Licenses ")
		else
			Response.Write(" License ")
		end if
		Response.Write("</b><br>")
		
        %>
                </td>
            </tr>
        <%
		'Get the full list if we've selected this state.
				
		oLicense.LicenseStateId = oRs("LicenseStateId")
		set oLicRs = oLicense.SearchLicenses()
			
			do while not oLicRs.EOF
                dim oLicenseInstance
                set oLicenseInstance = new License
                oLicenseInstance.ConnectionString = application("sDataSourceName")
                oLicenseInstance.VocalErrors = application("bVocalErrors")

				if oLicenseInstance.LoadLicenseById(oLicRs("LicenseId")) <> 0 then

					if sColor = "" then
						sColor = " bgcolor=""#ffffff"""
					else
						sColor = ""
					end if


					Response.Write("<tr>" & vbCrLf)
					if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then
						Response.Write("<td " & sColor & "><a href=""#"" onClick=""javascript:OpenMoveLicWindow('" & oLicenseInstance.LicenseId & "','');""><img src=""" & application("sDynMediaPath") & "bttnMove.gif"" border=""0"" title=""Move This License"" alt=""Move This License""></a></td>" & vbCrLf)
						Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicenseInstance.LicenseId & "&ls=" & oRs("LicenseStateId") & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
						Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicenseInstance.LicenseId & ",'" & oLicenseInstance.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
					end if 
					Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/licensedetail.asp?lid=" & oLicenseInstance.Licenseid & """>" & oLicenseInstance.LicenseNum & "</a></td>" & vbCrLf)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LicenseType & "</td>" & vbCrLf)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LookupLicenseStatus(oLicenseInstance.LicenseStatusId) & "</td>" & vbCrLf)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LicenseExpDate)
					if oLicenseInstance.LicenseExpDate < date() + 120 and oLicenseInstance.LicenseExpDate > date() then
						Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicenseInstance.LicenseExpDate - date(), 0) & " Days</font>)</b>")
					end if
					Response.Write("</td>" & vbCrLf)
                    Response.Write("<td " & sColor & ">")
                    Response.Write("<div align=""center"">")
                    Response.Write("<a href=""" & application("URL") & "/Calendar/EventGroupList.asp?LicenseID=" & oLicenseInstance.LicenseID & """>")
                    Response.Write("<img src=""" & application("URL") & "/admin/calendarfx/Media/Images/calicon.gif"" border=0>")
                    Response.Write("</a></div></td>")
					Response.Write("</tr>" & vbCrLf)
							
				end if 

				oLicRs.MoveNext

			loop
			
		oRs.MoveNext
	
	loop
	%>
	</table>
	<%

else
	
	Response.Write("No Licenses found.")
	
end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						

			<!-- Branch Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Branch Loan Officers</td>
				</tr>
						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modofficer.asp?branchid=<% = oBranch.BranchId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Officer" alt="Add New Officer"> New Officer</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
						
							<tr>
								<td class="bckRight">
									<table width="100%" border=0 cellpadding=15 cellspacing=0>
										<tr>
											<td>
<%
dim oAssociate
dim oRs

set oAssociate = new Associate
oAssociate.VocalErrors = false
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.BranchId = oBranch.BranchId
oAssociate.UserStatus = 1

set oRs = oAssociate.SearchAssociates

if not oRs.EOF then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><b>Loan Officer Name</b></td>
	</tr>
	<tr>
<%

	sColor = ""
	do while not oRs.EOF

		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

		'if the officer is inactive, grey out name
		if oRs("Inactive") then
			sClass = "class=""greyedLink"""
		else
			sClass = ""
		end if
		
		Response.Write("<tr>")
		Response.Write("<td " & sColor & "><a href=""../Officers/officerdetail.asp?id=" & oRs("UserID") & """ " & sClass & ">" & oRs("LastName") & ", " & oRs("FirstName") & "</a><br>" & vbCrLf)
		Response.Write("</tr>")

		oRs.MoveNext

	loop
%>
</table>
<%

else

	Response.Write("No Loan Officers found for this Company.<br>" & vbCrLf)

end if

set oRs = nothing
set oAssociate = nothing
%>
</table>
											</td>
										</tr>
									</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>



						<!-- Branch Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Branch Notes</td>
				</tr>
						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modnote.asp?bid=<% = oBranch.BranchId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oBranch.BranchId
oNote.OwnerTypeId = 2

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
sColor = ""
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId) then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="400"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if (CheckIsAdmin() or CheckIsThisBranchAdmin(oBranch.BranchId)) and (oNote.UserId = session("User_Id")) then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			else
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & month(oNote.NoteDate) & "/" & day(oNote.NoteDate) & "/" & year(oNote.NoteDate) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

							
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

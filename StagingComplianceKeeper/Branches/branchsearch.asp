<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


dim oBranch
set oBranch = new Branch
oBranch.VocalErrors = application("bVocalErrors")
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.CompanyId = session("UserCompanyId")



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"



'Retrieve form data
dim sBranchSearchText
dim iSearchBranchId
dim sOwnerID
dim iSearchCompanyL2Id, iSearchCompanyL3Id
dim iSelectedValue 
dim iOwnerTypeID

sBranchSearchText = ScrubForSql(trim(request("BranchSearchText")))
sBranchSearchLetter = ScrubForSql(request("letter"))
iSearchBranchId = ScrubForSql(trim(request("BranchId")))
sOwnerId = ScrubForSql(request("OwnerId"))
sAction = ScrubForSQL(request("Action"))

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchBranchesText") 
	Session.Contents.Remove("SearchBranchOwnerId") 
	Session.Contents.Remove("SearchBranchesLetter") 
	Session.Contents.Remove("SearchBranchesCurPage") 
else
	if (sBranchSearchText = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchBranchesText")) <> "") then
			sBranchSearchText = session("SearchBranchesText")
		end if
	else
		session("SearchBranchesText") = sBranchSearchText
	end if
	oBranch.SearchInName = sBranchSearchText
	
	if (trim(sOwnerId) = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchBranchOwnerId")) <> "") then
			sOwnerId = session("SearchBranchOwnerId")
			if left(session("SearchBranchOwnerId"), 1) = "a" then
				iSearchCompanyL2Id = mid(session("SearchBranchOwnerId"), 2)
				oBranch.SearchCompanyL2Id = iSearchCompanyL2Id
			elseif left(session("SearchBranchOwnerId"), 1) = "b" then
				iSearchCompanyL3Id = mid(session("SearchBranchOwnerId"), 2)
				oBranch.SearchCompanyL3Id = iSearchCompanyL3Id
			end if
		end if
	else
		session("SearchBranchOwnerId") = sOwnerId
		
		if left(sOwnerId, 1) = "a" then
			iSearchCompanyL2Id = mid(sOwnerId, 2)
			oBranch.SearchCompanyL2Id = iSearchCompanyL2Id
		elseif left(sOwnerId, 1) = "b" then
			iSearchCompanyL3Id = mid(sOwnerId, 2)
			oBranch.SearchCompanyL3Id = iSearchCompanyL3Id
		end if
	end if
	
	if (sBranchSearchLetter = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchBranchesLetter")) <> "") then
			sBranchSearchLetter = session("SearchBranchesLetter")
		end if
	else
		session("SearchBranchesLetter") = sBranchSearchLetter
	end if
	oBranch.SearchNameStart = sBranchSearchLetter
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("SearchBranchesCurPage")) <> "") then
			iCurPage = Session("SearchBranchesCurPage")
		end if
	else
		Session("SearchBranchesCurPage") = iCurPage
	end if

end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Listed Branches -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Branches</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder" align="center">		
						<form name="CompanySearch" method="POST" action="branchsearch.asp">
						<input type="hidden" name="action" value="SEARCH">					
						<table border="0" cellpadding="5" cellspacing="0" align="center">
							<tr>
								<td class="newstitle" nowrap>Search Branches: </td>
								<td><img src="/Media/Images/spacer.gif" width="5" alt="" border="0"></td>
								<td><input type="text" name="BranchSearchText" size="20" value="<% = session("SearchBranchesText") %>"></td>
								<td><img src="/Media/Images/spacer.gif" width="5" alt="" border="0"></td>
								<td class="newstitle" nowrap>Association: </td>								
								<td>
								<%
								if iSearchCompanyL2Id <> "" then
									iSelectedValue = iSearchCompanyL2Id
									iOwnerTypeID = 4
								elseif iSearchCompanyL3Id <> "" then
									iSelectedValue = iSearchCompanyL3Id
									iOwnerTypeID = 5
								else
									iSelectedValue = 0
									iOwnerTypeID = 0
								end if

								call DisplayL2sL3sDropdown(iSelectedValue, iOwnerTypeID, session("UserCompanyId"), false, 0)
								%>
								</td>								
								<td valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
							</tr>
						</table>
						</form>						
					</td>
				</tr>
				<tr>
					<td class="bckRight">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td align="center">
															<a href="branchsearch.asp?action=search&BranchSearchText=<%=sBranchSearchText%>&BranchId=<%=iSearchBranchId%>&OwnerId=<%=sOwnerId%>"><% if ucase(session("SearchBranchesLetter")) = "" then %><u><b>All</b></u><% else %>All<% end if%></a> |
															<a href="branchsearch.asp?letter=A"><% if ucase(session("SearchBranchesLetter")) = "A" then %><u><b>A</b></u><% else %>A<% end if%></a> | 
															<a href="branchsearch.asp?letter=B"><% if ucase(session("SearchBranchesLetter")) = "B" then %><u><b>B</b></u><% else %>B<% end if%></a> |
															<a href="branchsearch.asp?letter=C"><% if ucase(session("SearchBranchesLetter")) = "C" then %><u><b>C</b></u><% else %>C<% end if%></a> |
															<a href="branchsearch.asp?letter=D"><% if ucase(session("SearchBranchesLetter")) = "D" then %><u><b>D</b></u><% else %>D<% end if%></a> |
															<a href="branchsearch.asp?letter=E"><% if ucase(session("SearchBranchesLetter")) = "E" then %><u><b>E</b></u><% else %>E<% end if%></a> |
															<a href="branchsearch.asp?letter=F"><% if ucase(session("SearchBranchesLetter")) = "F" then %><u><b>F</b></u><% else %>F<% end if%></a> |
															<a href="branchsearch.asp?letter=G"><% if ucase(session("SearchBranchesLetter")) = "G" then %><u><b>G</b></u><% else %>G<% end if%></a> |
															<a href="branchsearch.asp?letter=H"><% if ucase(session("SearchBranchesLetter")) = "H" then %><u><b>H</b></u><% else %>H<% end if%></a> |
															<a href="branchsearch.asp?letter=I"><% if ucase(session("SearchBranchesLetter")) = "I" then %><u><b>I</b></u><% else %>I<% end if%></a> |
															<a href="branchsearch.asp?letter=J"><% if ucase(session("SearchBranchesLetter")) = "J" then %><u><b>J</b></u><% else %>J<% end if%></a> |
															<a href="branchsearch.asp?letter=K"><% if ucase(session("SearchBranchesLetter")) = "K" then %><u><b>K</b></u><% else %>K<% end if%></a> |
															<a href="branchsearch.asp?letter=L"><% if ucase(session("SearchBranchesLetter")) = "L" then %><u><b>L</b></u><% else %>L<% end if%></a> |
															<a href="branchsearch.asp?letter=M"><% if ucase(session("SearchBranchesLetter")) = "M" then %><u><b>M</b></u><% else %>M<% end if%></a> |
															<a href="branchsearch.asp?letter=N"><% if ucase(session("SearchBranchesLetter")) = "N" then %><u><b>N</b></u><% else %>N<% end if%></a> |
															<a href="branchsearch.asp?letter=O"><% if ucase(session("SearchBranchesLetter")) = "O" then %><u><b>O</b></u><% else %>O<% end if%></a> |
															<a href="branchsearch.asp?letter=P"><% if ucase(session("SearchBranchesLetter")) = "P" then %><u><b>P</b></u><% else %>P<% end if%></a> |
															<a href="branchsearch.asp?letter=Q"><% if ucase(session("SearchBranchesLetter")) = "Q" then %><u><b>Q</b></u><% else %>Q<% end if%></a> |
															<a href="branchsearch.asp?letter=R"><% if ucase(session("SearchBranchesLetter")) = "R" then %><u><b>R</b></u><% else %>R<% end if%></a> |
															<a href="branchsearch.asp?letter=S"><% if ucase(session("SearchBranchesLetter")) = "S" then %><u><b>S</b></u><% else %>S<% end if%></a> |
															<a href="branchsearch.asp?letter=T"><% if ucase(session("SearchBranchesLetter")) = "T" then %><u><b>T</b></u><% else %>T<% end if%></a> |
															<a href="branchsearch.asp?letter=U"><% if ucase(session("SearchBranchesLetter")) = "U" then %><u><b>U</b></u><% else %>U<% end if%></a> |
															<a href="branchsearch.asp?letter=V"><% if ucase(session("SearchBranchesLetter")) = "V" then %><u><b>V</b></u><% else %>V<% end if%></a> |
															<a href="branchsearch.asp?letter=W"><% if ucase(session("SearchBranchesLetter")) = "W" then %><u><b>W</b></u><% else %>W<% end if%></a> |
															<a href="branchsearch.asp?letter=X"><% if ucase(session("SearchBranchesLetter")) = "X" then %><u><b>X</b></u><% else %>X<% end if%></a> |
															<a href="branchsearch.asp?letter=Y"><% if ucase(session("SearchBranchesLetter")) = "Y" then %><u><b>Y</b></u><% else %>Y<% end if%></a> |
															<a href="branchsearch.asp?letter=Z"><% if ucase(session("SearchBranchesLetter")) = "Z" then %><u><b>Z</b></u><% else %>Z<% end if%></a>
														</td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="4" cellspacing="0">
<%

'Declare the recordset object
dim oRs
set oRs = Server.CreateObject("ADODB.Recordset")
'set oRs = oCompany.Branches.GetBranches(oCompany.CompanyId)
set oRs = oBranch.SearchBranches

	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20


'Define the variables we'll use for tracking row/column styles below
dim sColor
dim bCol

'dim oRs
'dim oConn
'dim sSql 

'if not sCompanySearchText = "" then

'set oRs = Server.CreateObject("ADODB.Recordset")

'set oConn = Server.CreateObject("ADODB.Connection")
'oConn.ConnectionString = application("sDataSourceName")
'oConn.Open

'sSql = "SELECT CompanyId, Name FROM Companies " & _
'       "WHERE (OwnerStateId = '" & session("StateID") & "' " & _
'       "OR OwnerStateId = '0') " & _
'       "AND Name LIKE '%" & sCompanySearchText & "%'"
'set oRs = oConn.Execute(sSql)

if (oRs.EOF and oRs.BOF) then

%>
													<tr>
														<td colspan="2" width="100%" align="center">No Branches Found</td>
													</tr>
<%

else


		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		
		oRs.AbsolutePage = iCurPage
		
		'Find the requested page if a letter was specified.
'		sLetter = ucase(left(request("letter"), 1))
'		if sLetter <> "" then
'			oRs.MoveFirst
'			do while iLetterPage = ""
'				do while (not oRs.EOF) and (iLetterPage = "")
'					if ucase(left(oRs("Name"), 1)) = ucase(left(sLetter, 1)) then
'						iLetterPage = oRs.AbsolutePage
'					end if
'					oRs.MoveNext
'				loop
'				if iLetterPage <> "" then
'					oRs.AbsolutePage = iLetterPage
'					iCurPage = iLetterPage
'				else
'					'Move back a letter and try again
'					if sLetter = "A" then
'						iLetterPage = "1"
'						oRs.MoveFirst
'					else
'						sLetter = chr(asc(sLetter) - 1)
'						oRs.MoveFirst
'					end if
'				end if
'			loop
'		else
'			'Set the beginning record to be displayed on the page
'			oRs.AbsolutePage = iCurPage
'		end if


	'Initialize the row/col values
	bCol = false
	sColor = ""
	
	dim iResult
	
	iCount = 0
	do while (iCount < oRs.PageSize) and (not oRs.EOF)
	
		if CheckIsAdmin or CheckIsThisBranchAdmin(oRs("BranchId")) or CheckIsThisBranchViewer(oRs("BranchId")) then
	
	
		iResult = oBranch.LoadBranchById(oRs("BranchId"))
		
	
'		if sBranchSearchText <> "" and instr(1,ucase(oBranch.Name),ucase(sBranchSearchText)) = 0 then 
		
'		else
	
			if not bCol then
%>
													<tr>
														<td width="48%" align="left"<% = sColor %>>
														<% if iSearchBranchId = cstr(oBranch.BranchId) then %>
														&nbsp;<b><% if oBranch.Inactive then response.write("<font class=""greyedLink"">" & oBranch.Name & "</font>") end if %></b> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><!--<img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail">--></a></td>
														<% else %>
														&nbsp;<a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>" <% if oBranch.Inactive then %>class="greyedLink"<% end if %>><% = oBranch.Name %><% if oBranch.BranchNum <> "" then %> (#<% = oBranch.BranchNum %>)<% end if %></a> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><!--<img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail">--></a></td>
														<% end if %>
														<td width="4%"></td>
<%
			else
%>
														<td width="48%" align="left"<% = sColor %>>
														<% if iSearchBranchId = cstr(oBranch.BranchId) then %>
														&nbsp;<b><% if oBranch.Inactive then response.write("<font class=""greyedLink"">" & oBranch.Name & "</font>") end if %></b> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><!--<img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail">--></a></td>
														<% else %>
														&nbsp;<a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>" <% if oBranch.Inactive then %>class="greyedLink"<% end if %>><% = oBranch.Name %><% if oBranch.BranchNum <> "" then %> (#<% = oBranch.BranchNum %>)<% end if %></a> <a href="branchdetail.asp?branchid=<% = oBranch.BranchId %>"><!--<img src="/media/images/bttnsearch.gif" height="12" width="12" border="0" alt="Branch Detail">--></a></td>
														<% end if %>
													</tr>
<%
			end if
		
			'Set the col to the opposite value
			if bCol = false then
				bCol = true
			else
				bCol = false
				'Every two columns, set the row to the opposite color
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
			end if
			
'		end if 
		end if 
	
		iCount = iCount + 1
		oRs.MoveNext
	
	loop

	'Finish off the table row if we ended on a left column.  Note that we check
	'for the positive bCol because the value is flipped at the end of the loop.
	if bCol then
%>
														<td width="48%">&nbsp;</td>
													</tr>
<%
	end if 


		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""branchsearch.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""branchsearch.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%
'oConn.Close
'set oConn = nothing

end if

%>
													</table>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

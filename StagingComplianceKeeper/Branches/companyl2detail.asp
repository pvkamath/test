<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/Manager.Class.asp" --->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsThisCompanyL2Admin 'Does the user have admin access?
dim bCheckIsThisCompanyL2Viewer 'Does the user have viewer access?


'Make sure we were passed a valid CompanyL2Id
dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")

dim iCompanyL2Id
iCompanyL2Id = ScrubForSql(request("id"))

if oCompanyL2.LoadCompanyById(iCompanyL2Id) = 0 then

	AlertError("This page requires a valid " & session("CompanyL2Name") & " ID.")
	Response.End

end if


'Make sure that the user has access to this CompanyL2.	
if not CheckIsThisCompanyL2Admin(iCompanyL2Id) then
	if not CheckIsThisCompanyL2Viewer(iCompanyL2Id) then
		AlertError("This page requires a valid " & session("CompanyL2Name") & " ID.")
		Response.End
	else
		bCheckIsThisCompanyL2Viewer = true
	end if
else
	bCheckIsThisCompanyL2Admin = true
end if
	

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmCompanyL2Delete(p_iCompanyL2Id, p_sCompanyL2Name)
{
	if (confirm("Are you sure you wish to delete the following <% = session("CompanyL2Name") %>:\n " + p_sCompanyL2Name + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/branches/deletecompanyl2.asp?id=' + p_iCompanyL2Id;		
}
function ConfirmOfficerDelete(p_iCandidateId,p_sCandidateName)
{
	if (confirm("Are you sure you wish to delete the following Officer:\n  " + p_sCandidateName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficer.asp?id=' + p_iCandidateId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletenote.asp?id=' + p_iNoteId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}
function ConfirmManagerDelete(p_iManagerId)
{
	if (confirm("Are you sure you wish to delete this Manager?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/branches/modmanagerproc.asp?delete=1&managerid=' + p_iManagerId;
}
function OpenMoveLicWindow(p_iLicenseId,p_sTextbox) {
	window.open("/officers/MoveLicWin.asp?lid=" + p_iLicenseId + "&textbox=" + p_sTextbox , "ImageWindow", "width=500,height=300,scrollbars=1")
}
</script>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">

			<!-- Company Details -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="<% = session("CompanyL2Name") %>" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Profile</td>
				</tr>
					
						<% if bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td align="left" valign="center"><a href="modcompanyl2.asp?id=<% = oCompanyL2.CompanyL2Id %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyL2Name") %> Information" alt="Edit <% = session("CompanyL2Name") %> Information"> &nbsp;Edit <% = session("CompanyL2Name") %> Information</a></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmCompanyL2Delete(<% = oCompanyL2.CompanyL2Id %>,'<% = oCompanyL2.Name %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This <% = session("CompanyL2Name") %>" alt="Delete This <% = session("CompanyL2Name") %>"> Delete <% = session("CompanyL2Name") %></a></td>
										<td nowrap align="left" valign="center"><a href="mergebranch.asp?id=<% = oCompanyL2.CompanyL2Id %>&type=4" class="nav"><img src="<% = application("sDynMediaPath") %>bttnMove.gif" align="top" border="0" title="Merge Division" alt="Merge Division"> &nbsp;Merge Division</a></td>
										<td align="left" valign="center"></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
				<tr>
					<td class="bckRight">
					
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Legal Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.LegalName %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Trade Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Name %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State of Incorp: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.LookupState(oCompanyL2.IncorpStateId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Date of Incorp: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.IncorpDate %></td>
										</tr>
										<% if 1 = 2 then %>
										<tr>
											<td class="newstitle" nowrap>Fiscal Year End: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.FiscalYearEndMonth %> <% = oCompanyL2.FiscalYearEndDay %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Phone %> <% if oCompanyL2.PhoneExt <> "" then %> Ext. <% = oCompanyL2.PhoneExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Phone2 %> <% if oCompanyL2.PhoneExt2 <> "" then %> Ext. <% = oCompanyL2.PhoneExt2 %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Phone3 %> <% if oCompanyL2.PhoneExt3 <> "" then %> Ext. <% = oCompanyL2.PhoneExt3 %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Fax %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Address %></td>
										</tr>
										<% if not oCompanyL2.Address2 = "" then %>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Address2 %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.City %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.LookupState(oCompanyL2.StateId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Zipcode %><% if oCompanyL2.ZipcodeExt <> "" then %> - <% oCompanyL2.ZipcodeExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Email %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Website: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><a href="http://<% = oCompanyL2.Website %>"><% = oCompanyL2.Website %></td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<% if session("CustField1Name") <> "" and oCompanyL2.CustField1 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField1Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.CustField1 %></td>
										</tr>
										<% end if %>
										<% if session("CustField2Name") <> "" and oCompanyL2.CustField2 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField2Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.CustField2 %></td>
										</tr>
										<% end if %>
										<% if session("CustField3Name") <> "" and oCompanyL2.CustField3 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField3Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.CustField3 %></td>
										</tr>
										<% end if %>
										<% if session("CustField4Name") <> "" and oCompanyL2.CustField4 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField4Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.CustField4 %></td>
										</tr>
										<% end if %>
										<% if session("CustField5Name") <> "" and oCompanyL2.CustField5 <> "" then %>
										<tr>
											<td class="newstitle" nowrap><% = session("CustField5Name") %>: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.CustField5 %></td>
										</tr>
										<% end if %>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
									</table>
											</td>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0" valign="top">
										<tr>
											<td valign="top" class="newstitle" nowrap><% = session("CompanyL2Name") %>  is Inactive: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% if oCompanyL2.Inactive then response.write("Yes") else response.write("No") end if %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Inactive Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><%= oCompanyL2.InactiveDate %></td>
										</tr>									
										<tr>
											<td class="newstitle" nowrap>Organization Type: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.LookupOrgType(oCompanyL2.OrgTypeId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>EIN: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.Ein %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Main Contact: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.MainContactName %><br>
											Phone: <% = oCompanyL2.MainContactPhone %><% if oCompanyL2.MainContactPhoneExt <> "" then %> Ext. <% = oCompanyL2.MainContactPhoneExt %><% end if %><br>
											Fax: <% = oCompanyL2.MainContactFax %><br>
											<% = oCompanyL2.MainContactEmail %><br>
											</td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Mailing Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompanyL2.MailingAddress %><br>
											<% if not oCompanyL2.MailingAddress2 = "" then %><% = oCompanyL2.MailingAddress2 %><br><% end if %>
											<% = oCompanyL2.MailingCity %><br>
											<% = oCompanyL2.LookupState(oCompanyL2.MailingStateId) %><br>
											<% = oCompanyL2.MailingZipcode %>
											</td>
										</tr>
										<%
										dim oManager
										set oManager = new Manager
										oManager.ConnectionString = application("sDataSourceName")
										oManager.VocalErrors = application("bVocalErrors")
										oManager.CompanyId = session("UserCompanyId")
										oManager.OwnerId = oCompanyL2.CompanyL2Id
										oManager.OwnerTypeId = 2
										set oRs = oManager.SearchManagers
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Branch Managers: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oManager.LoadManagerById(oRs("ManagerId")) <> 0 then
												
													%>
													<% if bCheckIsThisCompanyL2Admin then %>
													<a href="javascript:ConfirmManagerDelete(<% = oManager.ManagerId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Manager" alt="Delete This Manager" align="top"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/branches/modmanager.asp?id=<% = oManager.ManagerId %>"><% = oManager.FullName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsThisCompanyL2Admin then %>
											<a href="/branches/modmanager.asp?otid=2&oid=<% = oCompanyL2.CompanyL2Id %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Manager" alt="Add New Manager"> Add New Manager</a>
											<% end if %>
											</td>
										</tr>

										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										dim oAssociateDoc
										set oAssociateDoc = new AssociateDoc
										oAssociateDoc.ConnectionString = application("sDataSourceName")
										oAssociateDoc.VocalErrors = application("bVocalErrors")
										oAssociateDoc.CompanyId = session("UserCompanyId")
										oAssociateDoc.OwnerId = oCompanyL2.CompanyL2Id
										oAssociateDoc.OwnerTypeId = 4
										set oRs = oAssociateDoc.SearchDocs
										
										'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<% if bCheckIsThisCompanyL2Admin then %>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document" align="top"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsThisCompanyL2Admin then %>
											<a href="/officers/moddocument.asp?otid=4&oid=<% = oCompanyL2.CompanyL2Id %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											<% end if %>
											</td>
										</tr>
									</table>
										</td>
										</tr>
									</table>
					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
						
			<!-- Company Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = session("CompanyL2Name") %> Licenses</td>
				</tr>

						<% if bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modlicense.asp?c2id=<% = oCompanyL2.CompanyL2Id %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
				<tr>
					<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>
<%
dim oLicense
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oCompanyL2.CompanyL2Id
oLicense.OwnerTypeId = 4


set oRs = oLicense.SearchLicensesByState()

if not (oRs.EOF and oRs.BOF) then
	do while not oRs.EOF
	
		Response.Write("<b>(" & oRs("LicenseCount") & ") <a href=""companyl2detail.asp?id=" & oCompanyL2.CompanyL2Id & "&ls=" & oRs("LicenseStateId") & """>" & oLicense.LookupState(oRs("LicenseStateId")))
		if oRs("LicenseCount") > 1 then 
			Response.Write(" Licenses ")
		else
			Response.Write(" License ")
		end if
		Response.Write("</a></b><br>")
		
		'Get the full list if we've selected this state.
		if request("ls") = cstr(oRs("LicenseStateId")) then
				
			oLicense.LicenseStateId = oRs("LicenseStateId")
			set oLicRs = oLicense.SearchLicenses()
			
			%>
			<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
					<% if bCheckIsThisCompanyL2Admin then %>
					<td></td>
					<td></td>
					<td></td>
					<% end if %>
					<td><b>License Number</b></td>
					<td><b>License Type</b></td>
					<td><b>License Status</b></td>
					<td><b>Expiration Date</b></td>
                    <td><b>Filing Schedule</b></td>
				</tr>
			<%
				do while not oLicRs.EOF

					if oLicense.LoadLicenseById(oLicRs("LicenseId")) <> 0 then

						if sColor = "" then
							sColor = " bgcolor=""#ffffff"""
						else
							sColor = ""
						end if


						Response.Write("<tr>" & vbCrLf)
						if (bCheckIsThisCompanyL2Admin) and oLicense.OwnerId = oCompanyL2.CompanyL2Id then
							Response.Write("<td " & sColor & "><a href=""#"" onClick=""javascript:OpenMoveLicWindow('" & oLicense.LicenseId & "','');""><img src=""" & application("sDynMediaPath") & "bttnMove.gif"" border=""0"" title=""Move This License"" alt=""Move This License""></a></td>" & vbCrLf)
							Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & "&ls=" & oRs("LicenseStateId") & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
							Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
						elseif bCheckIsThisCompanyL2Admin then
							Response.Write("<td " & sColor & "></td>" & vbCrLf)
							Response.Write("<td " & sColor & "></td>" & vbCrLf)
						end if 
						Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/licensedetail.asp?lid=" & oLicense.Licenseid & """>" & oLicense.LicenseNum & "</a></td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseType & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseExpDate)
						if oLicense.LicenseExpDate < date() + 120 and oLicense.LicenseExpDate > date() then
							Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicense.LicenseExpDate - date(), 0) & " Days</font>)</b>")
						end if
						Response.Write("</td>" & vbCrLf)
                        Response.Write("<td " & sColor & ">")
                        Response.Write("<div align=""center"">")
                        Response.Write("<a href=""" & application("URL") & "/Calendar/EventGroupList.asp?LicenseID=" & oLicense.LicenseID & """>")
                        Response.Write("<img src=""" & application("URL") & "/admin/calendarfx/Media/Images/calicon.gif"" border=0>")
                        Response.Write("</a></div></td>")
						Response.Write("</tr>" & vbCrLf)
							
					end if 

					oLicRs.MoveNext

				loop

			%>
			</table>
			<%
			
		end if

		oRs.MoveNext
	
	loop

else
	
	Response.Write("No Licenses found.")
	
end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						




			<!-- Third Tier Companies -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Assigned <% = session("CompanyL3Name") %> List</td>
				</tr>
						<% if bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/branches/modcompanyl3.asp" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New <% = session("CompanyL3Name") %>" alt="Add New <% = session("CompanyL3Name") %>"> New <% = session("CompanyL3Name") %></a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
						
							<tr>
								<td class="bckRight">
									<table width="100%" border=0 cellpadding=15 cellspacing=0>
										<tr>
											<td>
<%
dim oCompanyL3
dim oRs

set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")
oCompanyL3.CompanyL2Id = oCompanyL2.CompanyL2Id

set oRs = oCompanyL3.SearchCompanies

if not oRs.EOF then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><b><% = session("CompanyL3Name") %> Name</b></td>
	</tr>
	<tr>
<%

	sColor = ""
	do while not oRs.EOF

		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

		Response.Write("<tr>")
		Response.Write("<td " & sColor & "><a href=""companyl3detail.asp?id=" & oRs("CompanyL3Id") & """>" & oRs("Name") & "</a><br>" & vbCrLf)
		Response.Write("</tr>")

		oRs.MoveNext

	loop
%>
</table>
<%

else

	Response.Write("No " & session("CompanyL3Name") & " associations found for this " & session("CompanyL2Name") & ".<br>" & vbCrLf)

end if

set oRs = nothing
set oCompanyL3 = nothing
%>
</table>
											</td>
										</tr>
									</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>







			<!-- Branches -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Assigned Branch List</td>
				</tr>
						<% if bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/branches/modbranch.asp" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Branch" alt="Add New Branch"> New Branch</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
						
							<tr>
								<td class="bckRight">
									<table width="100%" border=0 cellpadding=15 cellspacing=0>
										<tr>
											<td>
<%
dim oBranch

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")
oBranch.CompanyL2Id = oCompanyL2.CompanyL2Id

set oRs = oBranch.SearchBranches

if not oRs.EOF then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><b>Branch Name</b></td>
	</tr>
	<tr>
<%

	sColor = ""
	do while not oRs.EOF

		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

		Response.Write("<tr>")
		Response.Write("<td " & sColor & "><a href=""branchdetail.asp?branchid=" & oRs("BranchId") & """>" & oRs("Name") & "</a><br>" & vbCrLf)
		Response.Write("</tr>")

		oRs.MoveNext

	loop
%>
</table>
<%

else

	Response.Write("No Branch associations found for this " & session("CompanyL2Name") & ".<br>" & vbCrLf)

end if

set oRs = nothing
set oBranch = nothing
%>
</table>
											</td>
										</tr>
									</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>





						
						
						
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = session("CompanyL2Name") %> Loan Officers</td>
				</tr>
						<% if bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modofficer.asp?companyl2id=<% = oCompanyL2.CompanyL2Id %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Officer" alt="Add New Officer"> New Officer</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
						
							<tr>
								<td class="bckRight">
									<table width="100%" border=0 cellpadding=15 cellspacing=0>
										<tr>
											<td>
<%
dim oAssociate

set oAssociate = new Associate
oAssociate.VocalErrors = false
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.CompanyL2Id = oCompanyL2.CompanyL2Id
oAssociate.UserStatus = 1

set oRs = oAssociate.SearchAssociates

if not oRs.EOF then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td><b>Loan Officer Name</b></td>
	</tr>
	<tr>
<%

	sColor = ""
	do while not oRs.EOF

		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

		'if the officer is inactive, grey out name
		if oRs("Inactive") then
			sClass = "class=""greyedLink"""
		else
			sClass = ""
		end if
		
		Response.Write("<tr>")
		Response.Write("<td " & sColor & "><a href=""../Officers/officerdetail.asp?id=" & oRs("UserID") & """ " & sClass & ">" & oRs("LastName") & ", " & oRs("FirstName") & "</a><br>" & vbCrLf)
		Response.Write("</tr>")

		oRs.MoveNext

	loop
%>
</table>
<%

else

	Response.Write("No Loan Officers found for this " & session("CompanyL2Name") & ".<br>" & vbCrLf)

end if

set oRs = nothing
set oAssociate = nothing
%>
</table>
											</td>
										</tr>
									</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>

						
						
						
			<!-- Company Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = session("CompanyL2Name") %> Notes</td>
				</tr>

						<% if CheckIsAdmin() or bCheckIsThisCompanyL2Admin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modnote.asp?c2id=<% = oCompanyL2.CompanyL2Id %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oCompanyL2.CompanyL2Id
oNote.OwnerTypeId = 4

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() or bCheckIsThisCompanyL2Admin then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="400"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if (CheckIsAdmin() or bCheckIsThisCompanyL2Admin) and (oNote.UserId = session("User_Id")) then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			elseif CheckIsAdmin() or bCheckIsThisCompanyL2Admin then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & month(oNote.NoteDate) & "/" & day(oNote.NoteDate) & "/" & year(oNote.NoteDate) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
%>
						</table>
					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

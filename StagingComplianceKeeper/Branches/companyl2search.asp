<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" --------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")
oCompanyL2.CompanyId = oCompany.CompanyId



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Listed Branches -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyName") %>&nbsp;<% = session("CompanyL2Name") %>s</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">		
						<table border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="newstitle" nowrap>Search <% = session("CompanyL2Name") %>: </td>
								<td><img src="/Media/Images/spacer.gif" width="5" alt="" border="0"></td>
								<form name="CompanyL2Search" method="POST" action="companyL2search.asp">
								<td><input type="text" name="CompanyL2SearchText" size="20" value="<% = ScrubForSql(request("CompanyL2SearchText")) %>"></td>
								<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0"></td>
								</form>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td align="center">
															<a href="companyl2search.asp?action=search">All</a> |
															<a href="companyl2search.asp?letter=A">A</a> | 
															<a href="companyl2search.asp?letter=B">B</a> |
															<a href="companyl2search.asp?letter=C">C</a> |
															<a href="companyl2search.asp?letter=D">D</a> |
															<a href="companyl2search.asp?letter=E">E</a> |
															<a href="companyl2search.asp?letter=F">F</a> |
															<a href="companyl2search.asp?letter=G">G</a> |
															<a href="companyl2search.asp?letter=H">H</a> |
															<a href="companyl2search.asp?letter=I">I</a> |
															<a href="companyl2search.asp?letter=J">J</a> |
															<a href="companyl2search.asp?letter=K">K</a> |
															<a href="companyl2search.asp?letter=L">L</a> |
															<a href="companyl2search.asp?letter=M">M</a> |
															<a href="companyl2search.asp?letter=N">N</a> |
															<a href="companyl2search.asp?letter=O">O</a> |
															<a href="companyl2search.asp?letter=P">P</a> |
															<a href="companyl2search.asp?letter=Q">Q</a> |
															<a href="companyl2search.asp?letter=R">R</a> |
															<a href="companyl2search.asp?letter=S">S</a> |
															<a href="companyl2search.asp?letter=T">T</a> |
															<a href="companyl2search.asp?letter=U">U</a> |
															<a href="companyl2search.asp?letter=V">V</a> |
															<a href="companyl2search.asp?letter=W">W</a> |
															<a href="companyl2search.asp?letter=X">X</a> |
															<a href="companyl2search.asp?letter=Y">Y</a> |
															<a href="companyl2search.asp?letter=Z">Z</a>
														</td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border=0 cellpadding=4 cellspacing=0>

<%
'Retrieve form data
dim sSearchText
dim iSearchCompanyL2Id
sSearchText = ScrubForSql(trim(request("CompanyL2SearchText")))
iSearchCompanyL2Id = ScrubForSql(trim(request("CompanyL2Id")))

sAction = ScrubForSQL(request("Action"))


if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchCompanyL2sText") 
	Session.Contents.Remove("SearchCompanyL2sCurPage") 
	Session.Contents.Remove("SearchCompanyL2sLetter")
else

	oCompanyL2.Name = ScrubForSql(request("CompanyL2SearchText"))
	if (oCompanyL2.Name = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCompanyL2sText")) <> "") then
			oCompanyL2.Name = session("SearchCompanyL2sText")
		end if
	else
		session("SearchCompanyL2sText") = oCompanyL2.Name
	end if
	
	oCompanyL2.SearchNameStart = ScrubForSql(request("letter"))
	if (oCompanyL2.SearchNameStart = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCompanyL2sLetter")) <> "") then	
			oCompanyL2.SearchNameStart = session("SearchCompanyL2sLetter")
		end if
	else
		session("SearchCompanyL2sLetter") = oCompanyL2.SearchNameStart
	end if
	
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("SearchCompanyL2sCurPage")) <> "") then
			iCurPage = Session("SearchCompanyL2sCurPage")
		end if
	else
		Session("SearchCompanyL2sCurPage") = iCurPage
	end if

end if


'Declare the recordset object
dim oRs
set oRs = Server.CreateObject("ADODB.Recordset")
set oRs = oCompanyL2.SearchCompanies

if iCurPage = "" then iCurPage = 1
if iMaxRecs = "" then iMaxRecs = 20


'Define the variables we'll use for tracking row/column styles below
dim sColor
dim bCol

if (oRs.EOF and oRs.BOF) then

%>
													<tr>
														<td colspan="2" width="100%" align="center">No <% = session("CompanyL2Name") %>s Found</td>
													</tr>
<%

else

		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		oRs.AbsolutePage = iCurPage
		
		'Find the requested page if a letter was specified.
'		sLetter = ucase(left(request("letter"), 1))
'		if sLetter <> "" then
'			oRs.MoveFirst
'			do while iLetterPage = ""
'				do while (not oRs.EOF) and (iLetterPage = "")
'					if ucase(left(oRs("Name"), 1)) = ucase(left(sLetter, 1)) then
'						iLetterPage = oRs.AbsolutePage
'					end if
'					oRs.MoveNext
'				loop
'				if iLetterPage <> "" then
'					oRs.AbsolutePage = iLetterPage
'					iCurPage = iLetterPage
'				else
'					'Move back a letter and try again
'					if sLetter = "A" then
'						iLetterPage = "1"
'						oRs.MoveFirst
'					else
'						sLetter = chr(asc(sLetter) - 1)
'						oRs.MoveFirst
'					end if
'				end if
'			loop
'		else
'			'Set the beginning record to be displayed on the page
'			oRs.AbsolutePage = iCurPage
'		end if


	'Initialize the row/col values
	bCol = false
	sColor = ""
	
	dim iResult
	
	iCount = 0
	do while not oRs.EOF
	
	
	
		if CheckIsAdmin or CheckIsThisCompanyL2Admin(oRs("CompanyL2Id")) or CheckIsThisCompanyL2Viewer(oRs("CompanyL2Id")) then
	
	
		iResult = oCompanyL2.LoadCompanyById(oRs("CompanyL2Id"))
		
	
		if sSearchText <> "" and instr(1,ucase(oCompanyL2.Name),ucase(sSearchText)) = 0 then 
		
		else
	
			if not bCol then
%>
													<tr>
														<td width="48%" align="left"<% = sColor %>>
														&nbsp;<a href="companyl2detail.asp?id=<% = oCompanyL2.CompanyL2Id %>" <% if oCompanyL2.Inactive then %>class="greyedLink"<% end if %>><% = oCompanyL2.Name %></a></td>
														<td width="4%"></td>
<%
			else
%>
														<td width="48%" align="left"<% = sColor %>>
														&nbsp;<a href="companyl2detail.asp?id=<% = oCompanyL2.CompanyL2Id %>" <% if oCompanyL2.Inactive then %>class="greyedLink"<% end if %>><% = oCompanyL2.Name %></a></td>
													</tr>
<%
			end if
		
			'Set the col to the opposite value
			if bCol = false then
				bCol = true
			else
				bCol = false
				'Every two columns, set the row to the opposite color
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
			end if
			
		end if 
		end if 
	
		iCount = iCount + 1
		oRs.MoveNext
	
	loop

	'Finish off the table row if we ended on a left column.  Note that we check
	'for the positive bCol because the value is flipped at the end of the loop.
	if bCol then
%>
														<td width="48%">&nbsp;</td>
													</tr>
<%
	end if 
	
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""companyl2search.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""companyl2search.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%

end if

%>
													</table>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

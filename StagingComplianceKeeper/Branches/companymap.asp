<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" --------------------------->
<!-- #include virtual="/includes/CompanyL3.Class.asp" --------------------------->
<!-- #include virtual="/includes/Branch.Class.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")
oCompanyL2.CompanyId = oCompany.CompanyId
oCOmpanyL2.Inactive = false

dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")
oCompanyL3.CompanyId = oCompany.CompanyId
oCompanyL3.Inactive = false

dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")
oBranch.CompanyId = oCompany.CompanyId
oBranch.Inactive = false




'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Corporate Organization Map </td>
				</tr>
				<tr>
					<td class="bckRight">
						<table border="0" cellpadding="10" cellspacing="0" width="100%">
							<tr>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">						
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
									</table>
									<table width="100%" border=0 cellpadding=4 cellspacing=0>
										<tr>
											<td>
												<b><a href="/company/companydetail.asp"><% = oCompany.Name %></a></b><p>
												

<%

'Declare the recordset object
dim oL2Rs
dim oL3Rs
dim oBranchRs
set oL2Rs = Server.CreateObject("ADODB.Recordset")
set oL2Rs = oCompanyL2.SearchCompanies

do while not oL2Rs.EOF
	
	%>
	<% = Spacer(12) %><a href="/branches/companyl2detail.asp?id=<% = oL2Rs("CompanyL2Id") %>"><% = oL2Rs("Name") %></a><p>
	<%
	
	oCompanyL3.CompanyL2Id = oL2Rs("CompanyL2Id")
	set oL3Rs = oCompanyL3.SearchCompanies
	do while not oL3Rs.EOF
		
		%>
		<% = Spacer(24) %><a href="/branches/companyl3detail.asp?id=<% = oL3Rs("CompanyL3Id") %>"><% = oL3Rs("Name") %></a><p>
		<%		

		oBranch.SearchCompanyL2Id = 0
		oBranch.SearchCompanyL3Id = ""
		oBranch.CompanyL3Id = oL3Rs("CompanyL3Id")
		set oBranchRs = oBranch.SearchBranches
		do while not oBranchRs.EOF
			%>
			<% = Spacer(32) %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>"><% = oBranchRs("Name") %></a><p>
			<%		
			oBranchRs.MoveNext
		loop
		
		oL3Rs.MoveNext
	loop
	
	oBranch.CompanyL2Id = oL2Rs("CompanyL2Id")
	oBranch.SearchCompanyL2Id = ""
	oBranch.SearchCompanyL3Id = 0
	set oBranchRs = oBranch.SearchBranches
	do while not oBranchRs.EOF
		%>
		<% = Spacer(24) %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>"><% = oBranchRs("Name") %></a><p>
		<%		
		oBranchRs.MoveNext
	loop
	
	oL2Rs.MoveNext
loop



oCompanyL3.CompanyL2Id = 0
set oL3Rs = oCompanyL3.SearchCompanies
do while not oL3Rs.EOF

	%>
	<% = Spacer(12) %><a href="/branches/companyl3detail.asp?id=<% = oL3Rs("CompanyL3Id") %>"><% = oL3Rs("Name") %></a><p>
	<%

	oBranch.SearchCompanyL2Id = 0
	oBranch.SearchCompanyL3Id = ""
	oBranch.CompanyL3Id = oL3Rs("CompanyL3Id")
	set oBranchRs = oBranch.SearchBranches
	do while not oBranchRs.EOF
		%>
		<% = Spacer(24) %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>"><% = oBranchRs("Name") %></a><p>
		<%		
		oBranchRs.MoveNext
	loop
	
	oL3Rs.MoveNext
loop


oBranch.CompanyL2Id = ""
oBranch.CompanyL3Id = ""
oBranch.SearchCompanyL2Id = 0
oBranch.SearchCompanyL3Id = 0
set oBranchRs = oBranch.SearchBranches
do while not oBranchRs.EOF
	%>
	<% = Spacer(12) %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>"><% = oBranchRs("Name") %></a><p>
	<%		
	oBranchRs.MoveNext
loop


%>
														<td width="48%">&nbsp;</td>
													</tr>
<%


%>
													</table>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
	
	
	
function Spacer(p_iNumSpaces)
	
	for i = 0 to p_iNumSpaces
		Spacer = Spacer & "&nbsp;"
	next

end function
%>

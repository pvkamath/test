<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Note.Class.asp" --------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

dim iBranchId 
iBranchId = ScrubForSql(request("Id"))

dim iNewBranchId
dim sTypeName
dim oBranch

dim iType
iType = ScrubForSql(request("type"))

dim sMode
if iBranchId <> "" then 

    if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	    'The load was unsuccessful.  End.
	    HandleError("Failed to load the passed Company ID.")
	    Response.end
		
    end if 

    select case iType
        case 2
            set oBranch = new Branch
            sTypeName = "Branch"
            oBranch.ConnectionString = application("sDataSourceName")
            oBranch.VocalErrors = application("bVocalErrors")
            oBranch.TpConnectionString = application("sTpDataSourceName")
	        if oBranch.LoadBranchById(iBranchId) = 0 then

		        'The load was unsuccessful.  End.
		        HandleError("Failed to load the passed Branch ID.")
		        Response.End
	
	        end if
            iNewBranchId = ScrubForSql(request("BranchId"))
        case 4
            set oBranch = new CompanyL2
            oBranch.ConnectionString = application("sDataSourceName")
            oBranch.VocalErrors = application("bVocalErrors")
            sTypeName = "Division"
	        if oBranch.LoadCompanyById(iBranchId) = 0 then

		        'The load was unsuccessful.  End.
		        HandleError("Failed to load the passed Division ID.")
		        Response.End
	
	        end if
            iNewBranchId = ScrubForSql(request("CompanyL2Id"))
        case 5
            set oBranch = new CompanyL3
            oBranch.ConnectionString = application("sDataSourceName")
            oBranch.VocalErrors = application("bVocalErrors")
            sTypeName = "Region"
	        if oBranch.LoadCompanyById(iBranchId) = 0 then

		        'The load was unsuccessful.  End.
		        HandleError("Failed to load the passed Region ID.")
		        Response.End
	
	        end if
            iNewBranchId = ScrubForSql(request("CompanyL3Id"))
    end select

    if oBranch.CompanyId <> oCompany.CompanyId then
	
		'This is not their officer!
		HandleError("You do not have access to this Branch.")
		Response.End
		
	end if 

	sMode = "Delete"

else

	HandleError("This page requires a valid Branch ID.")

end if

dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerID = iBranchId
oNote.OwnerTypeID = iType

dim oNotesRs
set oNotesRs = oNote.SearchNotes()

do while not oNotesRs.eof
    set oNote = new Note
    oNote.ConnectionString = application("sDataSourceName")
    oNote.VocalErrors = application("bVocalErrors")
    if oNote.LoadNoteById(oNotesRs("NoteId")) <> 0 then
        oNote.OwnerId = iNewBranchId
        oNote.SaveNote
    end if    
    oNotesRs.MoveNext
loop

dim oLicense
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerID = iBranchId
oLicense.OwnerTypeID = iType

dim oLicensesRs
set oLicensesRs = oLicense.SearchLicenses()

do while not oLicensesRs.eof
    set oLicense = new License
    oLicense.ConnectionString = application("sDataSourceName")
    oLicense.VocalErrors = application("bVocalErrors")
    if oLicense.LoadLicenseById(oLicensesRs("LicenseId")) <> 0 then
        oLicense.OwnerId = iNewBranchId
        oLicense.SaveLicense "",""
    end if    
    oLicensesRs.MoveNext
loop

dim oAssociate
set oAssociate = new Associate

oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")
select case iType
    case 2
        oAssociate.BranchId = iBranchId
    case 4
        oAssociate.CompanyL2Id = iBranchId
    case 5
        oAssociate.CompanyL3Id = iBranchId
end select

dim oAssociatesRs
set oAssociatesRs = oAssociate.SearchAssociates

do while not oAssociatesRs.eof
    set oAssociate = new Associate
    oAssociate.ConnectionString = application("sDataSourceName")
    oAssociate.VocalErrors = application("bVocalErrors")
    if oAssociate.LoadAssociateById(oAssociatesRs("UserId")) <> 0 then
        select case iType
            case 2
                oAssociate.BranchId = iNewBranchId
            case 4
                oAssociate.CompanyL2Id = iNewBranchId
            case 5
                oAssociate.CompanyL3Id = iNewBranchId
        end select
        oAssociate.SaveAssociate
    end if    
    oAssociatesRs.MoveNext
loop

if iType = 4 then
    dim oCompanyL3
    set oCompanyL3 = new CompanyL3

    oCompanyL3.ConnectionString = application("sDataSourceName")
    oCompanyL3.VocalErrors = application("bVocalErrors")
    oCompanyL3.CompanyL2Id = iBranchId

    dim oCompanyL3Rs
    set oCompanyL3Rs = oCompanyL3.SearchCompanies

    do while not oCompanyL3Rs.eof
        set oCompanyL3 = new CompanyL3
        oCompanyL3.ConnectionString = application("sDataSourceName")
        oCompanyL3.VocalErrors = application("bVocalErrors")
        if oCompanyL3.LoadCompanyById(oCompanyL3Rs("CompanyL3Id")) <> 0 then
            oCompanyL3.CompanyL2Id = iNewBranchId
            oCompanyL3.SaveCompany
        end if    
        oCompanyL3Rs.MoveNext
    loop

    dim oChildBranch
    set oChildBranch = new Branch

    oChildBranch.ConnectionString = application("sDataSourceName")
    oChildBranch.VocalErrors = application("bVocalErrors")
    oChildBranch.CompanyL2Id = iBranchId

    dim oChildBranchRs
    set oChildBranchRs = oChildBranch.SearchBranches

    do while not oChildBranchRs.eof
        set oChildBranch = new Branch
        oChildBranch.ConnectionString = application("sDataSourceName")
        oChildBranch.VocalErrors = application("bVocalErrors")
        if oChildBranch.LoadBranchById(oChildBranchRs("BranchId")) <> 0 then
            oChildBranch.CompanyL2Id = iNewBranchId
            oChildBranch.SaveBranch
        end if    
        oChildBranchRs.MoveNext
    loop

elseif iType = 5 then
    set oChildBranch = new Branch

    oChildBranch.ConnectionString = application("sDataSourceName")
    oChildBranch.VocalErrors = application("bVocalErrors")
    oChildBranch.CompanyL3Id = iBranchId

    set oChildBranchRs = oChildBranch.SearchBranches

    do while not oChildBranchRs.eof
        set oChildBranch = new Branch
        oChildBranch.ConnectionString = application("sDataSourceName")
        oChildBranch.VocalErrors = application("bVocalErrors")
        if oChildBranch.LoadBranchById(oChildBranchRs("BranchId")) <> 0 then
            oChildBranch.CompanyL3Id = iNewBranchId
            oChildBranch.SaveBranch
        end if    
        oChildBranchRs.MoveNext
    loop

end if

    

select case iType
    case 2
        oBranch.DeleteBranch
    case 4
        oBranch.DeleteCompany
    case 5
        oBranch.DeleteCompany
end select

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Branch Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <%=sTypeName %> Profile</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Merge a <%=sTypeName %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<b>The <% = sTypeName %> was successfully merged.</b>
<p>
<% 
select case iType
    case 2
%>
    <a href="BranchSearch.asp">Return to Branch Listing</a>
<%
    case 4
%>
    <a href="companyl2search.asp">Return to Division Listing</a>
<%
    case 5
%>
    <a href="companyl3search.asp">Return to Region Listing</a>
<%
end select
%>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oBranch = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
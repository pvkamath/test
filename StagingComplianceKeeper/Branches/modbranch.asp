<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Branch Admin"


'Get the passed branch ID
dim iBranchId
iBranchId = ScrubForSql(request("id"))


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Verify access to this specific branch for branch admins/viewers
if not CheckIsAdmin and iBranchId <> "" then

	if not CheckIsThisBranchAdmin(cint(iBranchId)) then
	
		AlertError("This page requires a valid Branch ID. ")
		Response.End
		
	end if
	
end if 



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 

if iBranchId <> "" then 

	if oBranch.LoadBranchById(iBranchId) = 0 then

		'The load was unsuccessful.  End.
		Response.Write("Failed to load the passed Branch ID.")
		Response.End
	
	end if
	
	if oBranch.CompanyId <> oCompany.CompanyId then
	
		'This is not their branch!
		Response.Write("You do not have access to this Branch.")
		Response.End
		
	end if 

	sMode = "Edit"

else

	sMode = "Add"

end if

%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	//Hide or Display the Inactive Date depending upon if the inactive flag is set
	function DisplayHideInactiveDate(FORM)
	{
		if (FORM.Inactive.checked)
		{
			document.getElementById("InactiveDateRow").className = "RowVisible";	
		}
		else
		{
			document.getElementById("InactiveDateRow").className = "RowHidden";				
		}
	}
	
	function Validate(FORM)
	{
		if (
			!checkString(FORM.BranchName, "Name")
		   )
			return false;

		//If Inactive flag set, make sure a valid date is entered
		if (FORM.Inactive.checked)
		{
			if (!checkIsDate(FORM.InactiveDate,"Please enter a valid Inactive Date."))
				return false;			
		}					
			
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Company Branch</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="BranchForm" action="modbranchproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the CompanyID on to the next step so we know we're working with an existing
'Company.
if iBranchId <> "" then
%>
<input type="hidden" value="<% = iBranchId %>" name="BranchId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formheading">Branch Number: </td>
		<td align="left" valign="top">
			<input type="text" name="BranchNum" value="<% = oBranch.BranchNum %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Branch Name: </td>
		<td align="left" valign="top">
			<input type="text" name="BranchName" value="<% = oBranch.BranchName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Legal Name: </td>
		<td align="left" valign="top">
			<input type="text" name="LegalName" value="<% = oBranch.LegalName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Branch is Inactive: </td>
		<td align="left" valign="top">
			<input type="checkbox" name="Inactive" value="1"<% if oBranch.Inactive then %> CHECKED<% end if %> onClick="javascript:DisplayHideInactiveDate(this.form)">
		</td>
	</tr>
	<tr id="InactiveDateRow" class="<% if oBranch.Inactive then response.write("RowVisible") else response.write("RowHidden") end if%>">
		<td class="formheading">Inactive Date: </td>
		<td align="left" valign="top">
			<input type="text" name="InactiveDate" value="<% = oBranch.InactiveDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('BranchForm.InactiveDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('BranchForm.InactiveDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>		
	<tr>
		<td class="formheading">Association: </td>
		<td align="left" valign="top">
			<% 
			if oBranch.CompanyL2Id <> "" then 
				call DisplayL2sL3sDropdown(oBranch.CompanyL2Id, 4, session("UserCompanyId"), false, 1) 
			elseif oBranch.CompanyL3Id <> "" then
				call DisplayL2sL3sDropdown(oBranch.CompanyL3Id, 5, session("UserCompanyId"), false, 1) 
			else
				call DisplayL2sL3sDropdown(0, 0, session("UserCompanyId"), false, 1) 
			end if
			%>
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone" value="<% = oBranch.Phone %>" size="20" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone Extension: </td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt" value="<% = oBranch.PhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Fax: </td>
		<td align="left" valign="top">
			<input type="text" name="Fax" value="<% = oBranch.Fax %>" size="20" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Address: </td>
		<td align="left" valign="top">
			<input type="text" name="Address" value="<% = oBranch.Address %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Address Line 2: </td>
		<td align="left" valign="top">
			<input type="text" name="Address2" value="<% = oBranch.Address2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">City: </td>
		<td align="left" valign="top">
			<input type="text" name="City" value="<% = oBranch.City %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>		
		<td class="formheading">State: </td>
		<td>
			<% call DisplayStatesDropDown(oBranch.StateId,1,"StateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Zipcode: </td>
		<td align="left" valign="top">
			<input type="text" name="Zipcode" value="<% = oBranch.Zipcode %>" size="12" maxlength="15">
		</td>
	</tr>	
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td class="formheading">Mailing Address: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress" value="<% = oBranch.MailingAddress %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing Address Line 2: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress2" value="<% = oBranch.MailingAddress2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing City: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingCity" value="<% = oBranch.MailingCity %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing State: </td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oBranch.MailingStateId,1,"MailingStateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing Zipcode: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingZipcode" value="<% = oBranch.MailingZipcode %>" size="12" maxlength="15">
		</td>
	</tr>
	<tr>
		<td class="formheading">Entity License Number: </td>
		<td align="left" valign="top">
			<input type="text" name="EntityLicNum" value="<% = oBranch.EntityLicNum %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">FHA Branch ID: </td>
		<td align="left" valign="top">
			<input type="text" name="FhaBranchId" value="<% = oBranch.FhaBranchId %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">VA Branch ID: </td>
		<td align="left" valign="top">
			<input type="text" name="VaBranchId" value="<% = oBranch.VaBranchId %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">NMLS Number: </td>
		<td align="left" valign="top">
			<input type="text" name="NMLSNumber" value="<% = oBranch.NMLSNumber %>" size="25" maxlength="25">
		</td>
	</tr>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<% if session("CustField1Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField1Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField1" value="<% = oBranch.CustField1 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField2Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField2Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField2" value="<% = oBranch.CustField2 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField3Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField3Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField3" value="<% = oBranch.CustField3 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField4Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField4Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField4" value="<% = oBranch.CustField4 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField5Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField5Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField5" value="<% = oBranch.CustField5 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
set oBranch = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
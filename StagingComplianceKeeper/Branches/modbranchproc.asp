<%
'option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Broker Admin"

'Get the passed branch ID
dim iBranchId
iBranchId = ScrubForSql(request("BranchId"))


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Verify access to this specific branch for branch admins/viewers
if not CheckIsAdmin and iBranchId <> "" then

	if not CheckIsThisBranchAdmin(cint(iBranchId)) then
	
		AlertError("This page requires a valid Branch ID. ")
		Response.End
		
	end if
	
end if 



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"
	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")


dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 

if iBranchId <> "" then 

	if oBranch.LoadBranchById(iBranchId) = 0 then

		'The load was unsuccessful.  End.
		Response.Write("Failed to load the passed Branch ID.")
		Response.End
	
	end if
	
	if oBranch.CompanyId <> oCompany.CompanyId then
	
		'This is not their branch!
		Response.Write("You do not have access to this Branch.")
		Response.End
		
	end if 

	sMode = "Edit"

else

	sMode = "Add"

end if

dim sOwnerId
dim iCompanyL2Id
dim iCompanyL3Id
sOwnerId = ScrubForSql(request("OwnerId"))

if left(sOwnerId, 1) = "a" then
	iCompanyL2Id = mid(sOwnerId, 2)
elseif left(sOwnerId, 1) = "b" then
	iCompanyL3Id = mid(sOwnerID, 2)
end if


'Assign the object properties based on the passed values.
oBranch.CompanyId = session("UserCompanyId")
if iCompanyL2Id <> "" then
	oBranch.CompanyL2Id = iCompanyL2Id
elseif iCompanyL3Id <> "" then
	oBranch.CompanyL3Id = iCompanyL3Id
else
	oBranch.CompanyL2Id = ""
	oBranch.CompanyL3Id = ""
end if
if ScrubForSql(request("Inactive")) = "1" then
	oBranch.Inactive = true
	oBranch.InactiveDate = ScrubForSql(request("InactiveDate"))
else
	oBranch.Inactive = false
	oBranch.InactiveDate = ""
end if
oBranch.Name = ScrubForSql(request("BranchName"))
oBranch.LegalName = ScrubForSql(request("LegalName"))
oBranch.BranchNum = ScrubForSql(request("BranchNum"))
oBranch.BranchManager = ScrubForSql(request("BranchManager"))
oBranch.Phone = ScrubForSql(request("Phone"))
oBranch.PhoneExt = ScrubForSql(request("PhoneExt"))
oBranch.Fax = ScrubForSql(request("Fax"))
'if request("Active") = "1" then 
oBranch.Active = true
'else
'	oBranch.Active = false
'end if 
oBranch.Address = ScrubForSQL(request("Address"))
oBranch.Address2 = ScrubForSQL(request("Address2"))
oBranch.City = ScrubForSQL(request("City"))
oBranch.StateId = ScrubForSQL(request("StateId"))
oBranch.Zipcode = ScrubForSQL(request("Zipcode"))
oBranch.ZipcodeExt = ScrubForSql(request("ZipcodeExt"))
oBranch.MailingAddress = ScrubForSQL(request("MailingAddress"))
oBranch.MailingAddress2 = ScrubForSQL(request("MailingAddress2"))
oBranch.MailingCity = ScrubForSQL(request("MailingCity"))
oBranch.MailingStateId = ScrubForSQL(request("MailingStateId"))
oBranch.MailingZipcode = ScrubForSQL(request("MailingZipcode"))
oBranch.EntityLicNum = ScrubForSql(request("EntityLicNum"))
oBranch.FhaBranchId = ScrubForSql(request("FhaBranchId"))
oBranch.VaBranchId = ScrubForSql(request("VaBranchId"))
oBranch.NMLSNumber = ScrubForSql(request("NMLSNumber"))
if session("CustField1Name") <> "" then
	oBranch.CustField1 = ScrubForSql(request("CustField1"))
end if
if session("CustField2Name") <> "" then
	oBranch.CustField2 = ScrubForSql(request("CustField2"))
end if
if session("CustField3Name") <> "" then
	oBranch.CustField3 = ScrubForSql(request("CustField3"))
end if
if session("CustField4Name") <> "" then
	oBranch.CustField4 = ScrubForSql(request("CustField4"))
end if
if session("CustField5Name") <> "" then	
	oBranch.CustField5 = ScrubForSql(request("CustField5"))
end if

'We need a connectionstring too.
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")


'If this is a new Branch we need to save it before it will have a unique ID.
dim iNewBranchId
if oBranch.BranchId = "" then

	iNewBranchId = oBranch.SaveBranch

	if iNewBranchId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Branch.  Stop.
		Response.Write("Error: Failed to save new Branch.")
		Response.End

	end if
	
else

	iNewBranchId = oBranch.SaveBranch

	if iNewBranchId = 0 then

		'The save failed.  Die.
		Response.Write("Error: Failed to save existing Branch.")
		Response.End

	elseif CINT(iNewBranchId) <> CINT(iBranchId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Branch object.
		Response.Write("Error: Unexpected results while saving existing Branch.")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Branch Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a Company Branch</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Branch "<% = oBranch.Name %>" was successfully created.</b>
<%
else
%>
<b>The Branch "<% = oBranch.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="branchsearch.asp">Return to Branch Listing</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%
set oCompany = nothing
set oBranch = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
'option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Get the passed companyl2 ID
dim iCompanyL2Id
iCompanyL2Id = ScrubForSql(request("id"))


'Verify access to this specific company for companyl2 admins/viewers
if not CheckIsAdmin and iCompanyL2Id <> "" then

	if not CheckIsThisCompanyL2Admin(cint(iCompanyL2Id)) then
	
		AlertError("This page requires a valid CompanyL2 ID. ")
		Response.End
		
	end if
	
end if 

	
'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim oCompanyL2
dim oCompany

set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")

if iCompanyL2Id <> "" then

	if oCompanyL2.LoadCompanyById(iCompanyL2Id) = 0 then
		
		'The load was unsuccessful.  Stop.
		AlertError("Failed to load the passed " & session("CompanyL2Name") & ".")
		Response.End
		
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if




'Assign the properties based on our passed parameters
oCompanyL2.CompanyId = session("UserCompanyId")
oCompanyL2.Name = ScrubForSQL(request("Name"))
oCompanyL2.LegalName = ScrubForSql(request("LegalName"))
if ScrubForSql(request("Inactive")) = "1" then
	oCompanyL2.Inactive = true
	oCompanyL2.InactiveDate = ScrubForSql(request("InactiveDate"))
else
	oCompanyL2.Inactive = false
	oCompanyL2.InactiveDate = ""
end if
oCompanyL2.IncorpStateId = ScrubForSQL(request("IncorpStateId"))
oCompanyL2.IncorpDate = ScrubForSQL(request("IncorpDate"))
oCompanyL2.OrgTypeId = ScrubForSQL(request("OrgTypeId"))
oCompanyL2.Ein = ScrubForSql(request("Ein"))
oCompanyL2.Phone = ScrubForSQL(request("Phone"))
oCompanyL2.PhoneExt = ScrubForSQL(request("PhoneExt"))
oCompanyL2.Phone2 = ScrubForSQL(request("Phone2"))
oCompanyL2.PhoneExt2 = ScrubForSQL(request("PhoneExt2"))
oCompanyL2.Phone3 = ScrubForSQL(request("Phone3"))
oCompanyL2.PhoneExt3 = ScrubForSQL(request("PhoneExt3"))
oCompanyL2.Fax = ScrubForSQL(request("Fax"))
oCompanyL2.Website = ScrubForSQL(request("Website"))
oCompanyL2.Email = ScrubForSql(request("Email"))
oCompanyL2.Address = ScrubForSQL(request("Address"))
oCompanyL2.Address2 = ScrubForSQL(request("Address2"))
oCompanyL2.City = ScrubForSQL(request("City"))
oCompanyL2.StateId = ScrubForSQL(request("StateId"))
oCompanyL2.Zipcode = ScrubForSQL(request("Zipcode"))
oCompanyL2.ZipcodeExt = ScrubForSql(request("ZipcodeExt"))
oCompanyL2.MailingAddress = ScrubForSQL(request("MailingAddress"))
oCompanyL2.MailingAddress2 = ScrubForSQL(request("MailingAddress2"))
oCompanyL2.MailingCity = ScrubForSQL(request("MailingCity"))
oCompanyL2.MailingStateId = ScrubForSQL(request("MailingStateId"))
oCompanyL2.MailingZipcode = ScrubForSQL(request("MailingZipcode"))
oCompanyL2.MainContactName = ScrubForSQL(request("MainContactName"))
oCompanyL2.MainContactPhone = ScrubForSQL(request("MainContactPhone"))
oCompanyL2.MainContactPhoneExt = ScrubForSql(request("MainContactPhoneExt"))
oCompanyL2.MainContactEmail = ScrubForSQL(request("MainContactEmail"))
oCompanyL2.MainContactFax = ScrubForSQL(request("MainContactFax"))
if session("CustField1Name") <> "" then
	oCompanyL2.CustField1 = ScrubForSql(request("CustField1"))
end if
if session("CustField2Name") <> "" then
	oCompanyL2.CustField2 = ScrubForSql(request("CustField2"))
end if
if session("CustField3Name") <> "" then
	oCompanyL2.CustField3 = ScrubForSql(request("CustField3"))
end if
if session("CustField4Name") <> "" then
	oCompanyL2.CustField4 = ScrubForSql(request("CustField4"))
end if
if session("CustField5Name") <> "" then	
	oCompanyL2.CustField5 = ScrubForSql(request("CustField5"))
end if

'If this is a new Company we need to save it before it will have a unique ID.
dim iNewCompanyId
if oCompanyL2.CompanyL2Id = "" then

	iNewCompanyId = oCompanyL2.SaveCompany

	if iNewCompanyId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Company.  Stop.
		AlertError("Failed to save new " & session("CompanyL2Name") & ".")
		Response.End

	end if
	
else

	iNewCompanyId = oCompanyL2.SaveCompany

	if iNewCompanyId = 0 then

		'The save failed.  Stop.
		AlertError("Failed to save existing " & session("CompanyL2Name") & ".")
		Response.End

	elseif CINT(iNewCompanyId) <> CINT(iCompanyL2Id) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		AlertError("Unexpected results while saving existing " & session("CompanyL2Name") & ".")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="<% = session("CompanyL2Name") %>" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a <% = session("CompanyL2Name") %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new <% = session("CompanyL2Name") %> "<% = oCompanyL2.Name %>" was successfully created.</b>
<%
else
%>
<b>The <% = session("CompanyL2Name") %> "<% = oCompanyL2.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="companyl2detail.asp?id=<% = oCompanyL2.CompanyL2Id %>">Return to <% = session("CompanyL2Name") %> Profile</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
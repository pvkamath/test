<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Get the passed companyl3 ID
dim iCompanyL3Id
iCompanyL3Id = ScrubForSql(request("id"))


'Verify access to this specific company for companyl2 admins/viewers
if not CheckIsAdmin and iCompanyL3Id <> "" then

	if not CheckIsThisCompanyL3Admin(cint(iCompanyL3Id)) then
	
		AlertError("This page requires a valid CompanyL3 ID. ")
		Response.End
		
	end if
	
end if 

	
'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim oCompanyL3
dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")

if iCompanyL3Id <> "" then

	if oCompanyL3.LoadCompanyById(iCompanyL3Id) = 0 then
		
		'The load was unsuccessful.  Stop.
		AlertError("Failed to load the passed " & session("CompanyL3Name") & ".")
		Response.End
		
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if

%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	//Hide or Display the Inactive Date depending upon if the inactive flag is set
	function DisplayHideInactiveDate(FORM)
	{
		if (FORM.Inactive.checked)
		{
			document.getElementById("InactiveDateRow").className = "RowVisible";	
		}
		else
		{
			document.getElementById("InactiveDateRow").className = "RowHidden";				
		}
	}
	
	function Validate(FORM)
	{
		if (
			!checkString(FORM.Name, "Name")
		   )
			return false;

		//If Inactive flag set, make sure a valid date is entered
		if (FORM.Inactive.checked)
		{
			if (!checkIsDate(FORM.InactiveDate,"Please enter a valid Inactive Date."))
				return false;			
		}			
			
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="<% = session("CompanyL3Name") %>" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table  cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%"  border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a <% = session("CompanyL3Name") %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CompanyForm" action="modcompanyl3proc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the CompanyID on to the next step so we know we're working with an existing
'Company.
if iCompanyL3Id <> "" then
%>
<input type="hidden" value="<% = iCompanyL3Id %>" name="id">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Trade Name: </td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oCompanyL3.Name %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Legal Name: </td>
		<td align="left" valign="top">
			<input type="text" name="LegalName" value="<% = oCompanyL3.LegalName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading"><% = session("CompanyL3Name") %> is Inactive: </td>
		<td align="left" valign="top">
			<input type="checkbox" name="Inactive" value="1"<% if oCompanyL3.Inactive then %> CHECKED<% end if %> onClick="javascript:DisplayHideInactiveDate(this.form)">
		</td>
	</tr>
	<tr id="InactiveDateRow" class="<% if oCompanyL3.Inactive then response.write("RowVisible") else response.write("RowHidden") end if%>">
		<td class="formheading">Inactive Date: </td>
		<td align="left" valign="top">
			<input type="text" name="InactiveDate" value="<% = oCompanyL3.InactiveDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('CompanyForm.InactiveDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('CompanyForm.InactiveDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>	
	<tr>
		<td class="formheading">Association: </td>
		<td align="left" valign="top">
			<% call DisplayCompanyL2sDropdown(oCompanyL3.CompanyL2Id, session("UserCompanyId"), false, 1) %>
		</td>
	</tr>
	<tr>
		<td class="formheading">State of Incorporation: </td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oCompanyL3.IncorpStateId,1,"IncorpStateId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Date of Incorporation: </td>
		<td align="left" valign="top">
			<input type="text" name="IncorpDate" value="<% = oCompanyL3.IncorpDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('CompanyForm.IncorpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('CompanyForm.IncorpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td class="formheading">Organization Type: </td>
		<td align="left" valign="top">
			<% call DisplayOrgTypesDropDown(oCompanyL3.OrgTypeId, 1, "OrgTypeId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">EIN: </td>
		<td align="left" valign="top">
			<input type="text" name="Ein" value="<% = oCompanyL3.Ein %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #1: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone" value="<% = oCompanyL3.Phone %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #1 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt" value="<% = oCompanyL3.PhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #2: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone2" value="<% = oCompanyL3.Phone2 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #2 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt2" value="<% = oCompanyL3.PhoneExt2 %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #3: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone3" value="<% = oCompanyL3.Phone3 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #3 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt3" value="<% = oCompanyL3.PhoneExt3 %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Fax: </td>
		<td align="left" valign="top">
			<input type="text" name="Fax" value="<% = oCompanyL3.Fax %>" size="12" maxlength="15">
		</td>
	</tr>
	<tr>
		<td class="formheading">Website: </td>
		<td align="left" valign="top">
			<input type="text" name="Website" value="<% = oCompanyL3.Website %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Email: </td>
		<td align="left" valign="top">
			<input type="text" name="Email" value="<% = oCompanyL3.Email %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Address: </td>
		<td align="left" valign="top">
			<input type="text" name="Address" value="<% = oCompanyL3.Address %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Address Line 2: </td>
		<td align="left" valign="top">
			<input type="text" name="Address2" value="<% = oCompanyL3.Address2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">City: </td>
		<td align="left" valign="top">
			<input type="text" name="City" value="<% = oCompanyL3.City %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>		
		<td class="formheading">State: </td>
		<td>
			<% call DisplayStatesDropDown(oCompanyL3.StateId,1,"StateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Zipcode: </td>
		<td align="left" valign="top">
			<input type="text" name="Zipcode" value="<% = oCompanyL3.Zipcode %>" size="12" maxlength="15">
		</td>
	</tr>	
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td class="formheading">Mailing Address: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress" value="<% = oCompanyL3.MailingAddress %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing Address Line 2: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress2" value="<% = oCompanyL3.MailingAddress2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing City: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingCity" value="<% = oCompanyL3.MailingCity %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing State: </td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oCompanyL3.MailingStateId,1,"MailingStateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Mailing Zipcode: </td>
		<td align="left" valign="top">
			<input type="text" name="MailingZipcode" value="<% = oCompanyL3.MailingZipcode %>" size="12" maxlength="15">
		</td>
	</tr>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td class="formheading">Main Contact Name: </td>
		<td align="left" valign="top">
			<input type="text" name="MainContactName" value="<% = oCompanyL3.MainContactName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Main Contact Phone: </td>
		<td align="left" valign="top">
			<input type="text" name="MainContactPhone" value="<% = oCompanyL3.MainContactPhone %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Main Contact Phone Extension: </td>
		<td align="left" valign="top">
			<input type="text" name="MainContactPhoneExt" value="<% = oCompanyL3.MainContactPhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Main Contact Email: </td>
		<td align="left" valign="top">
			<input type="text" name="MainContactEmail" value="<% = oCompanyL3.MainContactEmail %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Main Contact Fax: </td>
		<td align="left" valign="top">
			<input type="text" name="MainContactFax" value="<% = oCompanyL3.MainContactFax %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<% if session("CustField1Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField1Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField1" value="<% = oCompanyL3.CustField1 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField2Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField2Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField2" value="<% = oCompanyL3.CustField2 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField3Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField3Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField3" value="<% = oCompanyL3.CustField3 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField4Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField4Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField4" value="<% = oCompanyL3.CustField4 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<% if session("CustField5Name") <> "" then %>
	<tr>
		<td class="formheading"><% = session("CustField5Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="CustField5" value="<% = oCompanyL3.CustField5 %>" size="50" maxlength="50">
		</td>
	</tr>
	<% end if %>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompanyL3 = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
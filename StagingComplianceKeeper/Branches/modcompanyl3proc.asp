<%
'option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Get the passed companyl3 ID
dim iCompanyL3Id
iCompanyL3Id = ScrubForSql(request("id"))


'Verify access to this specific company for companyl2 admins/viewers
if not CheckIsAdmin and iCompanyL3Id <> "" then

	if not CheckIsThisCompanyL3Admin(cint(iCompanyL3Id)) then
	
		AlertError("This page requires a valid CompanyL3 ID. ")
		Response.End
		
	end if
	
end if 

	
'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim oCompanyL3
dim oCompany

set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")

if iCompanyL3Id <> "" then

	if oCompanyL3.LoadCompanyById(iCompanyL3Id) = 0 then
		
		'The load was unsuccessful.  Stop.
		AlertError("Failed to load the passed " & session("CompanyL3Name") & ".")
		Response.End
		
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if




'Assign the PLP properties based on our passed parameters
oCompanyL3.CompanyId = session("UserCompanyId")
oCompanyL3.CompanyL2Id = ScrubForSql(request("CompanyL2Id"))
oCompanyL3.Name = ScrubForSQL(request("Name"))
oCompanyL3.LegalName = ScrubForSql(request("LegalName"))
if ScrubForSql(request("Inactive")) = "1" then
	oCompanyL3.Inactive = true
	oCompanyL3.InactiveDate = ScrubForSql(request("InactiveDate"))
else
	oCompanyL3.Inactive = false
	oCompanyL3.InactiveDate = ""
end if
oCompanyL3.IncorpStateId = ScrubForSQL(request("IncorpStateId"))
oCompanyL3.IncorpDate = ScrubForSQL(request("IncorpDate"))
oCompanyL3.OrgTypeId = ScrubForSQL(request("OrgTypeId"))
oCompanyL3.Ein = ScrubForSql(request("Ein"))
oCompanyL3.Phone = ScrubForSQL(request("Phone"))
oCompanyL3.PhoneExt = ScrubForSQL(request("PhoneExt"))
oCompanyL3.Phone2 = ScrubForSQL(request("Phone2"))
oCompanyL3.PhoneExt2 = ScrubForSQL(request("PhoneExt2"))
oCompanyL3.Phone3 = ScrubForSQL(request("Phone3"))
oCompanyL3.PhoneExt3 = ScrubForSQL(request("PhoneExt3"))
oCompanyL3.Fax = ScrubForSQL(request("Fax"))
oCompanyL3.Website = ScrubForSQL(request("Website"))
oCompanyL3.Email = ScrubForSql(request("Email"))
oCompanyL3.Address = ScrubForSQL(request("Address"))
oCompanyL3.Address2 = ScrubForSQL(request("Address2"))
oCompanyL3.City = ScrubForSQL(request("City"))
oCompanyL3.StateId = ScrubForSQL(request("StateId"))
oCompanyL3.Zipcode = ScrubForSQL(request("Zipcode"))
oCompanyL3.ZipcodeExt = ScrubForSql(request("ZipcodeExt"))
oCompanyL3.MailingAddress = ScrubForSQL(request("MailingAddress"))
oCompanyL3.MailingAddress2 = ScrubForSQL(request("MailingAddress2"))
oCompanyL3.MailingCity = ScrubForSQL(request("MailingCity"))
oCompanyL3.MailingStateId = ScrubForSQL(request("MailingStateId"))
oCompanyL3.MailingZipcode = ScrubForSQL(request("MailingZipcode"))
oCompanyL3.MainContactName = ScrubForSQL(request("MainContactName"))
oCompanyL3.MainContactPhone = ScrubForSQL(request("MainContactPhone"))
oCompanyL3.MainContactPhoneExt = ScrubForSql(request("MainContactPhoneExt"))
oCompanyL3.MainContactEmail = ScrubForSQL(request("MainContactEmail"))
oCompanyL3.MainContactFax = ScrubForSQL(request("MainContactFax"))
if session("CustField1Name") <> "" then
	oCompanyL3.CustField1 = ScrubForSql(request("CustField1"))
end if
if session("CustField2Name") <> "" then
	oCompanyL3.CustField2 = ScrubForSql(request("CustField2"))
end if
if session("CustField3Name") <> "" then
	oCompanyL3.CustField3 = ScrubForSql(request("CustField3"))
end if
if session("CustField4Name") <> "" then
	oCompanyL3.CustField4 = ScrubForSql(request("CustField4"))
end if
if session("CustField5Name") <> "" then	
	oCompanyL3.CustField5 = ScrubForSql(request("CustField5"))
end if

'If this is a new Company we need to save it before it will have a unique ID.
dim iNewCompanyId
if oCompanyL3.CompanyL3Id = "" then

	iNewCompanyId = oCompanyL3.SaveCompany

	if iNewCompanyId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Company.  Stop.
		AlertError("Failed to save new " & session("CompanyL3Name") & ".")
		Response.End

	end if
	
else

	iNewCompanyId = oCompanyL3.SaveCompany

	if iNewCompanyId = 0 then

		'The save failed.  Stop.
		AlertError("Failed to save existing " & session("CompanyL3Name") & ".")
		Response.End

	elseif CINT(iNewCompanyId) <> CINT(iCompanyL3Id) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		AlertError("Unexpected results while saving existing " & session("CompanyL3Name") & ".")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="<% = session("CompanyL3Name") %>" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a <% = session("CompanyL3Name") %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new <% = session("CompanyL3Name") %> "<% = oCompanyL3.Name %>" was successfully created.</b>
<%
else
%>
<b>The <% = session("CompanyL3Name") %> "<% = oCompanyL3.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="companyl3detail.asp?id=<% = oCompanyL3.CompanyL3Id %>">Return to <% = session("CompanyL3Name") %> Profile</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oCompanyL3 = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
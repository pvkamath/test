<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Manager.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Manager variables
dim iManagerId
dim iCompanyId
dim iOwnerId
dim iOwnerTypeId
dim sManagerName

dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iManagerId = ScrubForSql(request("id"))
iOwnerTypeId = ScrubForSql(request("otid"))
iOwnerId = ScrubForSql(request("oid"))


dim oManager 'Manager Object
dim oCompany 'Company object
dim oCompanyL2 'CompanyL2 object
dim oCompanyL3 'CompanyL3 object
dim oBranch 'Officer object


'Initialize the Manager object
set oManager = new Manager
oManager.ConnectionString = application("sDataSourceName")
oManager.VocalErrors = application("bVocalErrors")


'If we were passed a Manager ID, try to load it now.  Otherwise, we'll go
'through each of the owner type possibilities and see if we were given an owner
'to whom we'll assign this manager.
if iManagerId <> "" then

	if oManager.LoadManagerById(iManagerId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed Manager ID.")
		Response.End
	
	'If this is a companyL2 Manager, verify that it belongs to the user's company.
	elseif oManager.OwnerTypeId = 2 then 
	
		if not CheckIsThisCompanyL2Admin(oManager.OwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 Manager, verify that it belongs to the user's company.
	elseif oManager.OwnerTypeId = 3 then
	
		if not CheckIsThisCompanyL3Admin(oManager.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if				
	
	'If this is a branch Manager, verify that its branch belongs to the user's company.
	elseif oManager.OwnerTypeId = 1 then
	
		if not CheckIsThisBranchAdmin(oManager.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	end if 
	
	iOwnerId = oManager.OwnerId
	iOwnerTypeId = oManager.OwnerTypeId
	sMode = "Edit"

	
elseif iOwnerTypeId = "2" then
	
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL2Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "3" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL3Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "1" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisBranchAdmin(iOwnerId) then
		AlertError("Failed to load the passed Branch ID.")
		Response.End
	end if
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "BRANCHES"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.FullName, "Manager Name"))
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<% if iOwnerTypeId = 1 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Managers: <% = oManager.LookupBranchNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 2 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Managers</td>
					<% elseif iOwnerTypeId = 3 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Managers</td>
					<% end if %>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Manager</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="ManagerForm" action="modmanagerproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iOwnerId %>" name="OwnerId">
<input type="hidden" value="<% = iOwnerTypeId %>" name="OwnerTypeId">
<input type="hidden" value="<% = iManagerId %>" name="ManagerId">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Manager Name: </td>
		<td>
			<input type="text" name="FullName" value="<% = oManager.FullName %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
set oManager = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
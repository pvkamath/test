<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Manager.Class.asp" ----------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for manager type/id
dim iManagerId 'ID of the manager in question
dim iOwnerId 'ID of whatever owner type owns the manager
dim iOwnerTypeId 'What kind of manager?
dim iDelete
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iOwnerId = ScrubForSql(request("OwnerId"))
iOwnerTypeId = ScrubForSql(request("OwnerTypeId"))
iManagerId = ScrubForSql(request("ManagerId"))
iDelete = ScrubForSql(request("Delete"))

dim oManager 'Manager Object

'Initialize the Manager object
set oManager = new Manager
oManager.ConnectionString = application("sDataSourceName")
oManager.VocalErrors = application("bVocalErrors")

'If we were passed a Manager ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iManagerId <> "" then

	if oManager.LoadManagerById(iManagerId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed Manager ID.")
		Response.End

	'If this is a companyL2 Manager, verify that it belongs to the user's company.
	elseif oManager.OwnerTypeId = 2 then 
	
		if not CheckIsThisCompanyL2Admin(oManager.OwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 Manager, verify that it belongs to the user's company.
	elseif oManager.OwnerTypeId = 3 then
	
		if not CheckIsThisCompanyL3Admin(oManager.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if
	
	'If this is a branch Manager, verify that it's branch belongs to the user's company.
	elseif oManager.OwnerTypeId = 1 then
	
		if not CheckIsThisBranchAdmin(oManager.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End	
		end if 
	
	end if 
	
	sMode = "Edit"
	
elseif iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	oManager.OwnerId = iOwnerId
	oManager.OwnerTypeId = iOwnerTypeId
	
	'If this is a Branch Manager...
	if iOwnerTypeId = 1 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	'If this is a companyL2 Manager
	elseif iOwnerTypeId = 2 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 Manager
	elseif iOwnerTypeId = 3 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
		
	else
	
		AlertError("Invalid Owner Type specified.")
		Response.End
	
	end if 

	sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'We have a valid Manager.  Go ahead and set the properties based on our passed 
'form data.
if iDelete = "1" then
	oManager.Deleted = true
else
	if sMode = "New" then
		oManager.OwnerId = iOwnerId
		oManager.OwnerTypeId = iOwnerTypeId
	end if
	oManager.CompanyId = session("UserCompanyId")
	oManager.FullName = ScrubForSql(request("FullName"))
end if 


'Save the Manager
if oManager.SaveManager = 0 then

	AlertError("Failed to save the Manager.  Please try again.")
	Response.End
	
end if 



'Redirect back to the appropriate page.
if oManager.OwnerTypeId = 1 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oManager.OwnerId)

elseif oManager.OwnerTypeId = 2 then

	Response.Redirect("/branches/companyl2detail.asp?id=" & oManager.OwnerId)
	
elseif oManager.OwnerTypeId = 3 then

	Response.Redirect("/branches/companyl3detail.asp?id=" & oManager.OwnerId)
	
end if 



%>

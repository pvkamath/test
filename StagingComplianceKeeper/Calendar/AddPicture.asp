<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	dim iPage
	' Set Page Title:
	sPgeTitle = "Edit a Picture"




'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ChangeSampleImage()
		{
			document.ImgSample.src = document.frm.WhatFile.value		
		}	
		
		function ConfirmDelete() {
		if ( window.confirm("Are you sure you want to permanantly delete this file?"))
			return true;
		else
			return false;
		}
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<%
					'Get event name
					Dim iEventID, strSQLConnectionString, sSQL, rs, objConn, sImageEdit
                    dim sEventGroup, iLicenseID

					sImageEdit = " Add "
					
					strSQLConnectionString = application("sDataSourceName")
					
					Set objConn = Server.CreateObject("ADODB.Connection")
					objConn.Open strSQLConnectionString
					
					Set rs = Server.CreateObject("ADODB.Recordset")

					iEventID = request("eventid")
					sEventGroup = request("EventGroup")
                    iLicenseID = request("LicenseID")

					sSQL = "Select Event,  ImgPath From afxEvents Where EventID = '" & iEventID & "'"
					
					rs.Open sSQL, objConn, 3, 3
					
					If rs("ImgPath") <> "" Then sImageEdit = " Change " 
%>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<!-- title line	 -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	 <tr>
        	<td valign="top" class="activeTitle"><%=sImageEdit%> a File<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
<!-- content here -->

				
				<td height="100%" valign="top"> 
					<table cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td colspan="2">Would you like to <%=sImageEdit%> a File for "<%=rs("Event")%>" ?</td>							
						</tr>
						<tr>
							<td colspan="2"><%If rs("ImgPath") <> "" Then%><a href="<%=Application("sDynWebroot")%>usermedia/images/calendarFX/<%=rs("ImgPath")%>" target="_blank">Click here to view the file</a><%End If%>&nbsp;&nbsp;<img src="/admin/login/media/images/spacer.gif" name=ImgSample  border="0">							
							<%if sImageEdit <> " Add " Then %>								
									<br>
							<FORM action="DeletePictureProc.asp" method="Post" name="frm1" id="frm1">
								<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
                                <input type="hidden" name="EventGroup" id="Hidden1" value="" />
                                <input type="hidden" name="LicenseID" id="Hidden2" value="" />
								<input type="submit" name="delete" id="delete" value="Delete File"  onClick="Javascript:return ConfirmDelete();">
							</FORM>								
							<%End If%>
							</td>
						</tr>
							<FORM action="AddPictureProc.asp" method="Post" name="frm" id="frm" enctype="multipart/Form-Data">
							<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
                            <input type="hidden" name="EventGroup" id="EventGroup" value="<%=sEventGroup%>" />
                            <input type="hidden" name="LicenseID" id="LicenseID" value="<%=iLicenseID%>" />
						<tr>
							<td colspan="2"><input type="File" id="WhatFile" name="WhatFile" size="30" onPropertyChange="ChangeSampleImage()"></td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;<br>
							<input type="image" src="/admin/calendarfx/media/images/btn_submit.gif" border="0" name="submit" value="Submit">
							&nbsp;&nbsp;
                            <%if sEventGroup = "1" then %>
                            <a href="EventGroupList.asp?ParentID=<%=iEventID%><%if iLicenseID <> "" then Response.Write("&LicenseID=" & iLicenseID)%>"><img src="/admin/calendarfx/media/images/btn_skip.gif" border="0" name="skip" value="skip"></a>
                            <%else %>
							<a href="calendar.asp"><img src="/admin/calendarfx/media/images/btn_skip.gif" border="0" name="skip" value="skip"></a>
                            <%end if %>
							</td>
						</tr>
					</table>
					</FORM>					
				</td>
<!-- end content -->				
			</tr>    
   		</table>
   		
   		
   		
   		
   		
									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

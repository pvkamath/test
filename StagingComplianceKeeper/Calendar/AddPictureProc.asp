<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Edit a Picture"
	
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"	
	
	

	Dim objFileUp, sMode, iEventID
	Dim sEventGroup, iLicenseID


	set objFileUp = server.CreateObject("softartisans.fileup")
	
	'Set the default path to store uploaded files.
	'This should be set immediately after creating an instance. 

	objFileUP.Path = Application("sAbsUserMediaPath") & "calendarFX/"
    sEventGroup = objFileUp.Form("EventGroup")
    iLicenseID = objFileUp.Form("LicenseID")
	iEventID = objFileUp.Form("eventid")
	'response.write iEventID

	Dim objConn, sSQL, strSQLConnectionString

	strSQLConnectionString = application("sDataSourceName")
	
	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString


If objFileUp.IsEmpty Then

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>

<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
		function backtoAddpicture(){
			window.location = "Addpicture.asp?eventid=<%=iEventID%>"
		}
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
	
	
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>	
	
	
	
	
	
	
	<table width="300" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>Invalid filename. Please go back and try again.</td>
		</tr>
		<tr>
			<td><BR><input type="button" name="back" id="back" value="Back" onClick="backtoAddpicture()">
				<p>&nbsp;</p>
			</td>
		</tr>
	</table>
<%
Else
	'Upload file
	
	'iEventID = objFileUp.Form("eventid")
			
	'Get the name and path of the file to be uploaded
	FilePath = objFileUp.UserFileName
	
	'Breakdown uploaded file path and get the original filename
	FileArray = split(FilePath, "\")
	FilePathSpace = ubound(FileArray)
	FileName = FileArray(FilePathSpace)
	
	'Breakdown original file name and get the file extension
	FileNameArray = Split(FileName, ".")
	FileNameSpace = UBound(FileNameArray)
	FileNameExt = FileNameArray(FileNameSpace)
	
	'Change file name
	NewFileName = "evt" & iEventID & "." & FileNameExt
	
	'Upload the image file
	objFileUp.SaveAs NewFileName
	
	'Update image path in DB
	sSQL = "Update afxEvents SET ImgPath = '" & NewFileName & "' WHERE EventID ='" & iEventID & "'"
	
	objConn.Execute(sSQL)
	
	Set objConn = Nothing
	set objFileUp = nothing
    
    dim redirectURL
    if sEventGroup = "1" then
        redirectURL = "EventGroupList.asp"
        if iLicenseID <> "" then
            redirectURL = redirectURL & "?LicenseID=" & iLicenseID
        end if
    else
        redirectURL = "calendar.asp"
    end if
	Response.redirect redirectURL
End If
%>




									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
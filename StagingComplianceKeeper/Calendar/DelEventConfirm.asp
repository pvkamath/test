<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	'Verify that the user has logged in.
	CheckIsLoggedIn()
	afxsecCheckDirectoryAccess()
	afxsecCheckPageAccess()

	'Configure the administration submenu options
	bAdminSubmenu = true
	sAdminSubmenuType = "EVENTS"

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Confirm Event Deletion"

	Dim sSQL, rs, objConn, strSQLConnectionString
	Dim iEventID, id, aEventID
    dim iEventGroup, iLicenseID, iParentID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("delevent")
    iEventGroup = Request("EventGroup")
    iLicenseID = Request("LicenseID")
    iParentID = Request("ParentID")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
	
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
	
	
<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top" class="activeTitle">Confirm Delete<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content here -->
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td valign="top">
		<%	If isnull(iEventID) or iEventID = "" Then	%>	
			<table width="95%" border="0">
				<tr>
					<td>You have not selected an event to be deleted.<BR>Please press the back button and try again.</td>
				</tr>
				<tr>
					<td><BR><a href="javascript:history.back()">
						<img src="/admin/calendarfx/media/images/btn_back.gif" border="0" name="clear" value="clear"></a></td>
				</tr>
			</table>
		<%	Else	%>
			<table width="100%" border="0" cellpadding="4">
				<tr>
					<td colspan="2"><B>The following event(s) will be deleted if you press Delete:</B></td>
				</tr>
				<form method=post action="DelEventProc.asp" id=form1 name=form1>
		<%
				aEventID = split(iEventID, ",")

				For each id in aEventID

					sSQL = "Select Event From afxEvents Where EventID = '" & id & "'"
					rs.Open sSQL, objConn, 3, 1
		%>
				<tr>
					<td width="15%"><b>Event Name:</b></td>
					<td width="85%"><%=rs("Event")%></td>
				</tr>
		<%
					rs.Close
				Next
		%>
				<tr>
					<td colspan="2"><BR><input type="image" value="Delete" border=0 alt="delete" id="delete" name="delete" src="/admin/calendarfx/media/images/btn_delete.gif">&nbsp;&nbsp;
					<a href="javascript:history.back()"><img src="/admin/calendarfx/media/images/btn_cancel.gif" width="91" height="27" alt="Cancel" border="0"></a>
					</td>
				</tr>
				<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
				<input type="hidden" name="EventGroup" id="EventGroup" value="<%=iEventGroup%>">
				<input type="hidden" name="LicenseID" id="LicenseID" value="<%=iLicenseID%>">
				<input type="hidden" name="ParentID" id="ParentID" value="<%=iParentID%>">
			</form>	
			</table>
		<%
			End If

			Set rs = Nothing
			Set objConn = Nothing
		%>
		</td>
<!-- end content -->				
	</tr>
</table>




									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>



<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

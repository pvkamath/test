<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	'Verify that the user has logged in.
	CheckIsLoggedIn()
	afxsecCheckDirectoryAccess()
	afxsecCheckPageAccess()

	'Configure the administration submenu options
	bAdminSubmenu = true
	sAdminSubmenuType = "EVENTS"

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Delete Events"

	Dim sSQL, oRs, objConn, strSQLConnectionString
	Dim iEventID, id, aEventID
    Dim iEventGroup, iLicenseID, iParentID

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set oRs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("eventid")
    iEventGroup = Request("EventGroup")
    iLicenseID = Request("LicenseID")
    iParentID = Request("ParentID")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>

<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top" class="activeTitle">Delete  Event<br>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content here -->
<table width="90%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="top">
<%
aEventID = split(iEventID, ",")

For each id in aEventID
	
	sSql = "SELECT * FROM afxEvents WHERE EventId = '" & id & "' " & _
		"AND CompanyId = '" & session("UserCompanyId") & "' "
	set oRs = objConn.Execute(sSql)
	
	if not oRs.EOF then

		'Delete event
		sSQL = "Delete From afxEvents Where EventID = '" & id & "'; " & _
			   "Delete From afxEventsEventTypeX Where EventID = '" & id & "'; " & _
			   "Delete From afxEventDisplayDateX Where EventID = '" & id & "'; " & _
			   "Delete From afxWeeklyRecurrenceEvents Where EventID = '" & id & "'; " & _
			   "Delete From afxMonthlyRecurrenceEvents Where EventID = '" & id & "'; "
		objConn.Execute (sSQL)	
	
	end if	

Next

Set objConn = Nothing
%>
			<table width="500" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td><span>Event(s) Deleted Successfully</span></td>
                    <%if iEventGroup = "1" then%>
                </tr>
                <tr>
                    <td><p><a href="EventGroupList.asp?LicenseID=<%=iLicenseID%>&ParentID=<%=iParentID%>">Return to Event Group List</a></p></td>
                    <%end if%>
				</tr>
			</table>
		</td>
<!-- end content -->
	</tr>
        
</table>


									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>



<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Delete a Picture"
	
	
	'Verify that the user has logged in.
	CheckIsLoggedIn()
	afxsecCheckDirectoryAccess()
	afxsecCheckPageAccess()


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<%
'Get event name
Dim iEventID, strSQLConnectionString, sSQL, rs, objConn, sImageEdit
sImageEdit = request("delete")
					
strSQLConnectionString = application("sDataSourceName")
					
Set objConn = Server.CreateObject("ADODB.Connection")
objConn.Open strSQLConnectionString
					
Set rs = Server.CreateObject("ADODB.Recordset")
					
iEventID = request("eventid")
sReferrer = request("referrer")
iCourseID = request("CourseID")

sSQL = "Update afxEvents SET ImgPath = '' WHERE EventID ='" & iEventID & "' " & _
	"AND CompanyId = '" & session("UserCompanyId") & "'"
					
rs.Open sSQL, objConn, 3, 3
					
Response.redirect "list.asp"

%>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>


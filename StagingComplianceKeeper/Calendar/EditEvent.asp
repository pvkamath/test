<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/license.Class.asp" ------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	
	
	

	const MIN_ACCESS_LEVEL = 10		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant

	'determine if the user's using IE and on what platform
	sUserAgent = UCase(Request.ServerVariables("HTTP_USER_AGENT"))
	if (instr(sUserAgent,"MSIE")) then
		bIsIE = true
	end if

	if (instr(sUserAgent,"MAC")) then
		bIsMac = true
	end if 

	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Editing an Event"





'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"

	
	Dim objConn, rs, rs2, rsEvent, rsCat, rsSubCat, sSQL, iEventID
	Dim sEvent, iSectionID, dStartDate, dEndDate, sLocation, iEventTypeID
	Dim sAddress, sCity, sState, sZip, sPhone, sFax, sHost, sURL, sContact
	Dim sEmail, sInfo, sCost, iActive, sAction, strSQLConnectionString, iComplete
	dim bUseTracker
	dim oCourseObj
	dim iCourseID
	dim iBranchId, iCompanyL2Id, iCompanyL3Id
	dim iOwnerID, iOwnerTypeID
	dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList
    dim iLicenseId
    dim bEventGroup
    
	'User Access Lists
	sCompanyL2IdList = GetUserCompanyL2IdList()
	sCompanyL3IdList = GetUserCompanyL3IdList()
	sBranchIdList = GetUserBranchIdList()

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	'set oCourseObj = New Course
	'oCourseObj.ConnectionString = application("sDataSourceName")
	'Get Courses for Course Dropdown
	'oCourseObj.AddCourseType(3) 'Live Course
	'set courseRS = oCourseObj.GetCourses()
	
	Set rs = Server.CreateObject("ADODB.Recordset")
	Set rsEvent = server.CreateObject("ADODB.recordset")
	Set rsCat = Server.CreateObject("ADODB.recordset")
	Set rsSubCat = Server.CreateObject("ADODB.recordset")

	iEventID = Request("eventid")		
	sEditMode = Request("editmode")
	sEventType = request("EventType")
    iLicenseId = request("licenseid")
    bEventGroup = request("EventGroup")

	If sEditMode <> "Add" And sEditMode <> "Copy" Then 
		sEditMode = "Edit"
		sTitle = "Edit Event"
    ElseIf sEditMode = "Copy" Then
        sTitle = "Copy Event"
	Else
		sTitle = "Add A New Event"
	End If
	
    If bEventGroup = 1 Then
        sTitle = sTitle & " Group"
    End If

	If 	sEditMode = "Edit" Or sEditMode = "Copy" Then 
	'sSQL = "Select * From afxEvents Where EventID = '" & iEventID & "'"
	'sSQL = "SELECT *, EET.EventTypeID, ET.EventType " & _
			'"FROM afxEvents E, afxEventType ET, afxEventsEventTypeX EET " & _
			'"WHERE E.EventID = EET.EventID AND EET.EventTypeID = ET.EventTypeID " & _
			'"AND E.EventID = '" & iEventID & "'"
		If sEventType <> "" Then 
			sSQL = "SELECT *, EET.EventTypeID, ET.EventType " & _
				"FROM afxEvents E, afxEventType ET, afxEventsEventTypeX EET " & _
				"WHERE E.EventID = EET.EventID AND EET.EventTypeID = ET.EventTypeID " & _
				"AND E.EventID = '" & iEventID & "' " & _
				"AND E.CompanyId = '" & session("UserCompanyId") & "' "
		Else
			sSQL = "Select * From afxEvents Where EventID = '" & iEventID & "' " & _
				"AND CompanyId = '" & session("UserCompanyId") & "' "
		End If
	
		'Restrict the list of events to only the entities to which the user has access.
		'Division Clause
		sSQL = sSQL & "AND (CompanyL2Id is null"
		if trim(sCompanyL2IdList) <> "" then
			sSQL = sSQL & " OR CompanyL2Id in (" & sCompanyL2IdList & ")"
		end if
		sSQL = sSQL & ") "

		'Region Clause
		sSQL = sSQL & "AND (CompanyL3Id is null"
		if trim(sCompanyL3IdList) <> "" then
			sSQL = sSQL & " OR CompanyL3Id in (" & sCompanyL3IdList & ")"
		end if
		sSQL = sSQL & ") "

		'Branch Clause
		sSQL = sSQL & "AND (BranchId is null"
		if trim(sBranchIdList) <> "" then
			sSQL = sSQL & " OR BranchID in (" & sBranchIdList & ")"
		end if
		sSQL = sSQL & ") "
		
		rsEvent.Open sSQL, objConn, 3, 1
	
	if rsEvent.BOF and rsEvent.EOF then
		HandleError("This Event record could not be found.")
	end if 

	sEvent = rsEvent("Event")
	
	iBranchId = rsEvent("BranchId")
	iCompanyL2Id = rsEvent("CompanyL2Id")
	iCompanyL3Id = rsEvent("CompanyL3Id")

	'Set OwnerID and OwnerTypeID
	if not isnull(iBranchID) and iBranchId <> "" then
		iOwnerID = iBranchID
		iOwnerTypeId = 2
	elseif not isnull(iCompanyL2Id) and iCompanyL2Id <> "" then
		iOwnerID = iCompanyL2Id
		iOwnerTypeId = 4
	elseif not isnull(iCompanyL3Id) and iCompanyL3Id <> "" then
		iOwnerID = iCompanyL3Id
		iOwnerTypeId = 5
	else
		iOwnerID = ""
		iOwnerTypeId = 3
	end if
	
	dStartDate = rsEvent("startdate")
	dEndDate = rsEvent("enddate")
	iCourseID = rsEvent("CourseID")
	sCity = rsEvent("City")
	iStateID = rsEvent("StateID")
	sLocation = replace(rsEvent("location"), """", "&quot;")
	'sPhone = rsEvent("phone")
	sURL = rsEvent("WebAddress")
	'sContact = rsEvent("contact")
	'sEmail = rsEvent("email")
	sInfo = rsEvent("Directions")
	iActive = rsEvent("active")
    iComplete = rsEvent("Complete")
	iSectionID = rsEvent("SectionID")	
	bUseTracker = cbool(rsEvent("UseTracker"))
	If sEventType <> "" Then 
		iEventTypeID = rsEvent("EventTypeID")
	End If
	

	sEvent = replace(sEvent, """", "&quot;")
	'sContact = replace(sContact, """", "&quot;")
	sInfo = replace(sInfo, """", "&quot;")
	sCost = replace(sCost, """", "&quot;")

	'Check for Weekly Recurrences
	sSQL = "SELECT * FROM afxWeeklyRecurrenceEvents WHERE EventID = '" & iEventID & "' "
	rs.Open sSQL, objConn, 3, 3
	
	if not rs.eof then
		sRecurrenceType = "WEEKLY"
		iWeekCycle = rs("WeekCycle")
		sWeekDays = rs("WeekDays")
		iEventSpan = rs("EventSpan")
		sRecurrenceLink = "recurrence.asp?RecurrenceType=WEEKLY&WeekCycle=" & iWeekCycle & "&WeekDays=" & sWeekDays	 & "&EventSpan=" & iEventSpan
	else
		rs.Close
		'Check for Monthly Recurrences
		sSQL = "SELECT * FROM afxMonthlyRecurrenceEvents WHERE EventID = '" & iEventID & "' "
		rs.Open sSQL, objConn, 3, 3
		
		if not rs.eof then
			sRecurrenceType = "MONTHLY"
			iMonthCycle = rs("MonthCycle")
			iMonthOccurrence = rs("Occurrence")
			sMonthWeekDayOfOccurrence = rs("WeekDayOfOccurrence")
			iEventSpan = rs("EventSpan")
			sRecurrenceLink = "recurrence.asp?RecurrenceType=MONTHLY&MonthCycle=" & iMonthCycle & "&MonthOccurrence=" & iMonthOccurrence & "&MonthWeekDayOfOccurrence=" & sMonthWeekDayOfOccurrence	 & "&EventSpan=" & iEventSpan
		else
			sRecurrenceLink = "recurrence.asp"
		end if
	end if
	rs.close
	else
		sRecurrenceLink = "recurrence.asp"
	End If 

    if not isnull(iLicenseId) and iLicenseId <> "" then
        dim oLicenseObj
        set oLicenseObj = new License

        oLicenseObj.ConnectionString = application("sDataSourceName")
        oLicenseObj.LoadLicenseById(iLicenseId)

        if oLicenseObj.OwnerCompanyId = session("UserCompanyId") then 'Additional verification so that a user can only load information for a company they belong to
            iOwnerID = oLicenseObj.OwnerId
            iOwnerTypeId = oLicenseObj.OwnerTypeId
        end if

        if sEditMode = "Add" then
            'Default State value
            iStateID = oLicenseObj.LicenseStateID
        end if
    end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<SCRIPT LANGUAGE="JavaScript1.1" SRC="/admin/calendarfx/includes/TextFormat.js"></SCRIPT>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<script language="javascript" src="/admin/calendarfx/includes/DatePicker.js"></script>
<script language="javascript" src="/admin/calendarfx/includes/FormCheck2.js"></script>
<SCRIPT LANGUAGE="JavaScript">
function SetEventName(FORM)
{
	//Set the Event Name to the Course Name if the event name is not filled
	if (isWhitespace(FORM.event.value))
	{
		if (FORM.CourseID[FORM.CourseID.selectedIndex].text != 'Please Select The Course')
		{
			FORM.event.value = FORM.CourseID[FORM.CourseID.selectedIndex].text;
		}
	}
}

function EnableDisableCourseDropDown(FORM)
{
	//if Live Class, enable Course drop down, if not, disable
	if (FORM.EventTypeID.selectedIndex == 1)
		FORM.CourseID.disabled = false;
	else
		FORM.CourseID.disabled = true;
}

// Begin IMAGE rollovers
function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}


//for insert <br> into textfield at cursor place
function storeCaret (textEl) {
			if (textEl.createTextRange) 
				textEl.caretPos = document.selection.createRange().duplicate();
}
	
function insertAtCaret () {
			var textE1 = document.frm.Info;
			var text = "<br>";
			if (document.frm.Info.createTextRange && document.frm.Info.caretPos) {
				var caretPos = document.frm.Info.caretPos;
				caretPos.text =" " + caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? text + ' ' : text;
			}
			else
			document.frm.Info.value = text;
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		// name of variable does not matter
		blank = newImage("/admin/calendarfx/media/images/Blank.gif");
		preloadFlag = true;
	}
}

preloadImages();

// end IMAGE rollovers

function ValidateForm(frm) {
	//if (!checkString(frm.EventTypeID,"Event Type"))
	//	return false;
		
	if (isEmpty(frm.event.value)) {
		alert("Please enter a name.");
		frm.event.focus();
		return false;
	}
	
	if (!checkIsDate(frm.StartDate, "The start date you entered is not a valid date.", false)) {
		return false;
	}
	
	if (frm.EndDate.value != '')
	{
		if (!checkIsDate(frm.EndDate, "The end date you entered is not a valid date.", false)) {				
			return false;
		}
		
		if (!checkStartAndEndDate(frm.StartDate, frm.EndDate, "The end date is earlier than the start date."))	{						
			frm.EndDate.focus();
			return false;
		}	
	}

	
	//else if (isEmpty(frm.Location.value)) {
		//alert("Please select a location.");
		//frm.Location.focus();
		//return false;
	//}
	return true;
}


function ChangeDate(p_oBox,p_sDate){
		
var bFlg;
		
	bFlg = true
	
	if (p_sDate == 's') {
			
		if (document.frm.shour.disabled) {
			bFlg = false
		}
				
		document.frm.shour.disabled = bFlg;
		document.frm.sminute.disabled = bFlg;
		document.frm.sampm.disabled = bFlg;
		
	} else {
	
		if (document.frm.ehour.disabled) {
			bFlg = false
		}
			
		document.frm.ehour.disabled = bFlg;
		document.frm.eminute.disabled = bFlg;
		document.frm.eampm.disabled = bFlg;
	
	}		
}

function setEndDate(){
	if (frm.EndDate.value.length == 0) {
		frm.EndDate.value = frm.StartDate.value
	}
}

function OpenRecurrenceWindow(FORM)
{
	window.open(document.frm.RecurrenceLink.value,"RecurrenceWindow","Width=400,Height=310,Scrollbars=1,maximizable=no,resizable=no")
}

// End -->
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
			

<!-- title line	 -->      
<table border="0" cellspacing="0" cellpadding="0">
       <tr>
        	<td valign="top"><div class="activeTitle"><%=sTitle%></div>
			 <p>Click <b>Submit</b> when you have entered the needed information.<br>
			 Click <b>Cancel</b> if you decide not to make the add/edits.</p>
			 
			 <p>You may apply different styles to your text in the General Information box<br>
			 by selecting text that you have typed and clicking on one of the buttons above.<br>
			 You may make text <b>bold</b>,<i> italicize</i> text, <u>underline</u> text, add a line break,<br>
			 make text a web link, and make text an email link.</p>
			<img src="/admin/login/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
<!-- content here -->			
		<td height="300" valign="top">
			<FORM action="EventProc.asp" method="post" name="frm" id="frm" onSubmit="return ValidateForm(this)">
			<input type="hidden" name="editmode" id="editmode" value="<%=sEditMode%>">
			<input type="hidden" name="eventid" id="eventid" value="<%=iEventID%>">
			<input type="hidden" name="mode" id="mode" value="<%=sMode%>">
			<input type="hidden" name="RecurrenceType" value="<%= sRecurrenceType %>">
			<input type="hidden" name="WeekCycle" id="WeekCycle" value="<%= iWeekCycle %>">
			<input type="hidden" name="WeekDays" id="WeekDays" value="<%= sWeekDays %>">	
			<input type="hidden" name="MonthCycle" id="MonthCycle" value="<%= iMonthCycle %>">
			<input type="hidden" name="MonthOccurrence" id="MonthOccurrence" value="<%= iMonthOccurrence %>">				
			<input type="hidden" name="MonthWeekDayOfOccurrence" id="MonthWeekDayOfOccurrence" value="<%= sMonthWeekDayOfOccurrence %>">							
			<input type="hidden" name="EventSpan" id="EventSpan" value="<%=iEventSpan%>">
			<input type="hidden" name="RecurrenceLink" id="RecurrenceLink" value="<%= sRecurrenceLink %>">				
            <input type="hidden" name="LicenseID" id="LicenseID" value="<%= iLicenseID %>" />
            <input type="hidden" name="EventGroup" id="EventGroup" value="<%= bEventGroup %>" />
			<table width="565" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td class="formreqheading"><span class="formreqstar">*</span> Name: </td>
					<td><input type="text" name="event" id="event" value="<%=sEvent%>" size="50" maxlength="50"></td>
				</tr>
		
		<!-- If Categories section needs to be restored place it here -->
				<tr>
					<td>&nbsp;</td>
					<td><b>Leave the End Date blank for On-Going Events</b></td>
				</tr>
				<tr>
					<td class="formreqheading"><span class="formreqstar">*</span> Start Date (mm/dd/yyyy): </td>
					<td nowrap><%
						Dim dateArray, timeArray, sDate, sTime, sAMPM, sHour, sMinute
				
						if request("refdate") <> "" then
							dStartDate = ScrubForSql(Request("refdate"))
						end if 
				
						If dStartDate <> "" Then
							dateArray = Split(dStartDate)
							sDate = trim(dateArray(0))
							
							if Left(sDate, 2) = Left(sDate, 1) & "/" Then
								sDate = "0" & sDate
							End if
							
							'Must set a check for when the time is equal to 12AM,
							'because the Database does not store the time value for 12AM
							if (UBound(dateArray) > 0) then
								sTime = dateArray(1)
								sAMPM = dateArray(2)
							else
								'sTime = "12:00"
								'sAMPM = "AM"
								sTime = ""
								sAMPM = ""
							end if
						End If
				
						If sTime <> "" Then
							timeArray = Split(sTime, ":")
							sHour = timeArray(0)
							sMinute = timeArray(1)
						End If
						
						%>
						<input name="StartDate" id="StartDate" value="<%=sDate%>" size="15">
						<a href="javascript:show_calendar('frm.StartDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;">
						<img src="/admin/calendarfx/Media/Images/calicon.gif" border=0></a></td>
				</tr>
				<tr>
					<td class="formheading">End Date (mm/dd/yyyy): </td>
					<td nowrap><%
						Dim eDate, eTime, eAMPM, eHour, eMinute, eDateArray
						
						if request("refdate") <> "" then
							dEndDate = ScrubForSql(Request("refdate"))
						end if 
						
						if dEndDate = "1/1/2020" then
							edate = ""
						elseIf dEndDate <> "" Then
							eDateArray = Split(dEndDate)
							eDate = trim(eDateArray(0))

							if Left(eDate, 2) = Left(eDate, 1) & "/" Then
								eDate = "0" & eDate
							End if
							
							'response.write (eDate)
							'Must set a check for when the time is equal to 12AM,
							'because the Database does not store the time value for 12AM
							if (UBound(eDateArray) > 0) then
								eTime = eDateArray(1)
								eAMPM = eDateArray(2)
							else
								'etime = "12:00"
								'eAMPM = "AM"
								eTime = ""
								eAMPM = ""
							end if
						End If

						If eTime <> "" Then
							timeArray = Split(eTime, ":")
							eHour = timeArray(0)
							eMinute = timeArray(1)
						End If
						
						%>
						<input name="EndDate" id="EndDate" value="<%=eDate%>" size="15">
						<a href="javascript:show_calendar('frm.EndDate');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;">
						<img src="/admin/calendarfx/Media/Images/calicon.gif" border=0></a></td>
					</td>
				</tr>
				<tr>
					<td class="formheading">Start Time: </td>
					<td nowrap>
						<table cellpadding="0" cellspacing="0" border="0">
							<td><SELECT <%if len(sTime) = 0 then %>DISABLED <%end if%> name="shour">
									<!--<Option selected value="">Select Hour</Option>-->
									<% For i = 1 To 12 %>
										<Option value="<%=i%>" <% If CStr(i) = sHour Then response.write(" selected") %>><%=i%></Option>
									<% Next %>
								</SELECT></td>
							<td><SELECT <%if len(sTime) = 0 then %>DISABLED <%end if%> name="sminute">
									<!--<Option selected value="">Minute</Option>-->
									<%
									i = 0
									Do While i <= 45
										If i = 0 Then i = "00" 
									%>
										<Option value="<%=i%>" <% If CInt(i) = CInt(sMinute) Then response.write(" selected")%>><%=i%></Option>	
									<%
										i = i + 15
									Loop
									%>
								</SELECT></td>
							<td><SELECT <%if len(sTime) = 0 then %>DISABLED <%end if%> name="sampm">
									<%If sAMPM = "AM" Then%>
										<Option value="am" selected>AM</Option>
										<Option value="pm">PM</Option>
									<%Else%>
										<Option value="am">AM</Option>
										<Option value="pm" selected>PM</Option>
									<%End If%>
								</SELECT></td>
							<td>&nbsp;</td>
							<td>Ignore Start Time&nbsp;<input type="checkbox" name="startDateFlg"  onclick="ChangeDate(this,'s')" <%if len(sTime) = 0 then %> checked <%end if%>></td>
						</table>
					</td>
				</tr>
				<tr>
					<td class="formheading">End Time: </td>
					<td nowrap>
						<table cellpadding="0" cellspacing="0" border="0">
							<td><SELECT <%if len(eTime) = 0 then %>DISABLED <%end if%> name="ehour">
									<% For i = 1 To 12 %>
										<Option value="<%=i%>" <% If CStr(i) = eHour Then response.write(" selected") %>><%=i%></Option>
									<% Next %>
									
									<%
									'For i = 1 To 12
										'If CStr(i) = eHour Then
									%>
										<!--	<Option value="<%=i%>" selected><%=i%></Option>
									<%	'Else	%>
											<Option value="<%=i%>"><%=i%></Option> -->
									<%
										'End If
									'Next
									%>
								</SELECT></td>
							<td><SELECT <%if len(eTime) = 0 then %>DISABLED <%end if%> name="eminute">
									<%
									i = 0
									Do While i <= 45
										If CInt(i) = CInt(eMinute) Then
											If i = 0 Then i = "00"
									%>
											<Option value="<%=i%>" selected><%=i%></Option>
									<%	Else	
											If i = 0 Then i = "00"
									%>
											<Option value="<%=i%>"><%=i%></Option>
									<%
										End If
										i = i + 15
									Loop
									%>
								</SELECT></td>
							<td><SELECT <%if len(eTime) = 0 then %>DISABLED <%end if%> name="eampm">
									<%If eAMPM = "AM" Then%>
										<Option value="am" selected>AM</Option>
										<Option value="pm">PM</Option>
									<%Else%>
										<Option value="am">AM</Option>
										<Option value="pm" selected>PM</Option>
									<%End If%>
								</SELECT></td>
							<td>&nbsp;</td>
							<td>Ignore End Time&nbsp;<input type="checkbox" name="endDateFlg"  onclick="ChangeDate(this,'e')" <%if len(eTime) = 0 then %> checked <%end if%>></td>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="1">&nbsp</td>
					<td colspan="5"><a href="javascript:OpenRecurrenceWindow()">Event Recurrence Options</a></td>					
				</tr>				
				<tr>
					<td class="formheading">City: </td>
					<td><input type="text" name="City" id="City" value="<%=sCity%>" size="50" maxlength="100"></td>
				</tr>
				<tr>
					<td class="formheading">URL: </td>
					<td><input type="text" name="URL" id="URL" value="<%=sURL%>" size="50" maxlength="100"></td>
				</tr>
				<tr>
					<td class="formheading">State: </td>
					<td><% call DisplayStatesDropDown(iStateID,1,"State") %></td>
				</tr>								
				<tr>
					<td class="formheading">Association: </td>
                    <td><% call DisplayL2sL3sBranchesDropdown(iOwnerID, iOwnerTypeID, session("UserCompanyId"), cbool(iLicenseID > 0), 1) %></td>
				</tr>				
				<%if bIsIE and not bIsMac then%>
				<tr>
					<td>&nbsp;</td>
					<td>
						<img src="/admin/calendarfx/media/images/miscimages/bold.gif" width=23 height=24 onClick="FormatText('B')" alt="Bold" name="Bold" ONMOUSEOVER="changeImages('Bold', '/admin/calendarfx/media/images/miscimages/bold_over.gif'); return true; " onMouseOut="changeImages('Bold', '/admin/calendarfx/media/images/miscimages/bold.gif'); return true; ">
						<img src="/admin/calendarfx/media/images/miscimages/italic.gif" width=23 height=24 onClick="FormatText('I')" alt="Italics" name="Italic" ONMOUSEOVER="changeImages('Italic', '/admin/calendarfx/media/images/miscimages/italic_over.gif'); return true; " onMouseOut="changeImages('Italic', '/admin/calendarfx/media/images/miscimages/italic.gif'); return true; ">
						<img src="/admin/calendarfx/media/images/miscimages/underline.gif" width=23 height=24 onClick="FormatText('U')" alt="Underline" name="Underline" ONMOUSEOVER="changeImages('Underline', '/admin/calendarfx/media/images/miscimages/underline_over.gif'); return true; " onMouseOut="changeImages('Underline', '/admin/calendarfx/media/images/miscimages/underline.gif'); return true; ">
						<img src="/admin/calendarfx/media/images/miscimages/br.gif" width=23 height=24 alt="BR" name="BR" ONMOUSEOVER="changeImages('BR', '/admin/calendarfx/media/images/miscimages/br_over.gif'); return true; " onMouseOut="changeImages('BR', '/admin/calendarfx/media/images/miscimages/br.gif'); return true; "onClick="insertAtCaret(); return true; ">
						<img src="/admin/calendarfx/media/images/miscimages/weblink.gif" width=89 height=24 onClick="MakeLink('W')" alt="Make link" name="WebLink" ONMOUSEOVER="changeImages('WebLink', '/admin/calendarfx/media/images/miscimages/weblink_over.gif'); return true; " onMouseOut="changeImages('WebLink', '/admin/calendarfx/media/images/miscimages/weblink.gif'); return true; ">
						<img src="/admin/calendarfx/media/images/miscimages/email.gif" width=67 height=24 onClick="MakeLink('M')" alt="Make email link" name="MailLink" ONMOUSEOVER="changeImages('MailLink', '/admin/calendarfx/media/images/miscimages/email_over.gif'); return true; " onMouseOut="changeImages('MailLink', '/admin/calendarfx/media/images/miscimages/email.gif'); return true; ">
					</td>
				</tr>
				<%end if%>				
				<tr>
					<td class="formheading">General Information: </td>
					<td><textarea rows="15" cols="40" id="Info" name="Info" maxlength=5000 onSelectStart="EnableFormatting()" ONSELECT="storeCaret(this);" ONCLICK="storeCaret(this);" ONKEYUP="storeCaret(this);"><%=sInfo%></textarea></td>
				</tr>
				<tr>
					<td class="formheading">Active: </td>
					<td><%If iActive = "1" Then%>
						<input type="Checkbox" name="Active" id="Active" checked></td>
						<%Else%>
						<input type="Checkbox" name="Active" id="Active"></td>
						<%End If%>
				</tr>
				<tr>
					<td class="formheading">Completed: </td>
					<td><%If iComplete Then%>
						<input type="Checkbox" name="Completed" id="Completed" checked></td>
						<%Else%>
						<input type="Checkbox" name="Completed" id="Completed"></td>
						<%End If%>
				</tr>
				<tr>
					<td class="formheading">Send Alert Emails: </td>
					<td><input type="checkbox" name="UseTracker" id="UseTracker" value="1"<% if bUseTracker then %>CHECKED<% end if %>></td>
				</tr>					
				<tr>
					<td >&nbsp;</td>
					<td colspan="2">&nbsp;<br>
						<input type="image" src="/admin/calendarfx/media/images/btn_submit.gif" border="0" name="submit" value="Submit">
						&nbsp;&nbsp;
						<a href="#" onClick="history.back()"><img src="/admin/calendarfx/media/images/btn_cancel.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</FORM>
		</td>
	</tr>
</table>				



									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
	'rsEvent.Close
	Set rsEvent = Nothing
	set oCourseObj = Nothing
%>
	

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

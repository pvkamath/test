<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Calendar Admin"
	
	
	
	

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"	
	

	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>

<%
strSQLConnectionString = application("sDataSourceName")
Dim objConn, rs, sSQL, iSectionID, iMonthID, dDate, iYear, sLocation, sImgPath
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList
dim oLicenseObj
dim iLicenseID, sBranchName, sLicenseName, sLicenseNumber
dim iParentID
dim sSubmitType
dim sCompleted

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()

sNewSearch = request("NewSearch")
iSectionID = Request("SectionID")
sMonthYear = Request("MonthYear")
dDate = Request("dt")
iYear = Request("yr")
sSearchCity = Request("SearchCity")
iSearchStateID = Request("State")
iNumEvent = Request("numEvent")
sLocation = Request("location")
sEventType = Request("EventType")
sMode = Request("mode") 'list coming active events or history events
iLicenseID = Request("LicenseID")
iParentID = Request("ParentID")
sSubmitType = Request("submit")

if not isnull(iLicenseID) and iLicenseID <> "" then
    set oLicenseObj = new License
    oLicenseObj.ConnectionString = application("sDataSourceName")
    oLicenseObj.LoadLicenseById(clng(iLicenseID))

    sLicenseName = oLicenseObj.LicenseType
    sLicenseNumber = oLicenseObj.LicenseNum

    set oLicenseObj = nothing
end if

if iSectionID = "" then
	iSectionID = 0
else
	iSectionID = cint(iSectionID)
end if

if dDate = "" then
	dDate = 0
else
	dDate = cint(dDate)
end if
		
dim sLicenseID
if isnumeric(iLicenseID) then
    sLicenseID = iLicenseID
else
    sLicenseID = "NULL"
end if

select case sSubmitType
    case "add"
        ssql = "INSERT INTO afxEvents (CompanyID, Active, UseTracker, ParentID, EventGroup, LicenseID, Complete) " & _
               "VALUES (" & session("UserCompanyId") & ",1,0," & iParentID & ",1," & sLicenseID & ",0) "

	    Set objConn = Server.CreateObject("ADODB.Connection")
	    objConn.Open strSQLConnectionString
		objConn.Execute(sSQL)

        set objConn = nothing
    case "submit"
        dim iCount
        iCount = Request("EventCount")

        dim index
	    Set objConn = Server.CreateObject("ADODB.Connection")
	    objConn.Open strSQLConnectionString
        for index = 0 to iCount - 1
            iEventID = Request("txtEventID" & index)
            sEventName = Request("txtEventName" & index)
            dEventDate = Request("txtEventDate" & index)
            sDirections = Request("txtDirections" & index)
            sCompleted = Request("chkCompleted" & index)
            
            if sCompleted = "on" then
                bCompleted = 1
            else
                bCompleted = 0
            end if

            sSQL = "UPDATE afxEvents SET " & _
                   "Event = '" & Replace(sEventName, "'", "''") & "', " & _
                   "StartDate = '" & Replace(dEventDate, "'", "''") & "', " & _
                   "EndDate = '" & Replace(dEventDate, "'", "''") & "', " & _
                   "Directions = '" & Replace(sDirections, "'", "''") & "', " & _
                   "Complete = '" & Replace(bCompleted, "'", "''") & "', " & _
                   "LicenseID = " & Replace(sLicenseID, "'", "''") & " " & _
                   "WHERE EventID = '" & iEventID & "'"

		    objConn.Execute(sSQL)


        next
        set objConn = nothing
end select

%>
<script language="javascript" src="/admin/calendarfx/includes/DatePicker.js"></script>
<script language="javascript" src="/admin/CalendarFX/includes/FormCheck2.js"></script>
<script language="JavaScript">
function OpenLiveView() {
	CalendarWindow = window.open('/calendar.asp','CalendarWindow');
	CalendarWindow.location.reload();
}

function ValidateForm(frm2) {
	if (!delarray[i].checked) {
		alert("You have not chosen an event to delete.");
		return false;
		}
	else
		if (window.confirm("Are you sure you want to permanantly delete the selected events?"))
			return true;
		else
			return false;
}

var bDirty;
bDirty = false;

function SetDirty() {
    bDirty = true;
}

function CheckClean() {
    if (bDirty) {
        var value = window.confirm("Data on the page has been modified. If you continue, your changes will NOT be saved. Continue?")
        return value;
    }
    else {
        return true;
    }
}

</script> 

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<link rel="stylesheet" href="includes/style.css">
<!-- title line -->
<table cellspacing="0" cellpadding="0" border="0">
       	<tr>
        	<td valign="top"><span class="activeTitle">Events List </span>
            <% if iLicenseID <> "" then %>
                For <% =sBranchName %> <% =sLicenseName %> - (<% =sLicenseNumber %>)
            <% end if %>
			<p><a href="<%= application("URL")%>/Calendar/EditEvent.asp?editmode=Add&EventGroup=1<% if iLicenseID <> "" then response.write("&LicenseID=" & iLicenseID) %>"><img src="../Admin/CalendarFx/media/images/btn_addeventgroup.gif" /></a></p>
			<img src="/admin/calendarfx/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content  -->
	<%' if session("access_level") >= 10 then %>	
<table cellpadding="4" cellspacing="0" border="0">
		<%				
		Set objConn = Server.CreateObject("ADODB.Connection")
		objConn.Open strSQLConnectionString
				
		%>			
		<tr>
			<td colspan="2">
				<table cellpadding="4" cellspacing="0" border="0">
					<FORM action="EventGroupList.asp" method="Post" name="frm2" id="frm2">
                    <input type="hidden" name="ParentID" value="<%=iParentID%>" />
                    <input type="hidden" name="LicenseID" value="<%=iLicenseID%>" />
					<%
					Dim rsEvent, iEventID, sEvent, intPage, intPageCount, sParentName
					
					CurrMonth = Month(Date)
					CurrYear = Year(Date)
					CurrDate = Day(Date)
					
					Set rsEvent = Server.CreateObject("ADODB.Recordset")
					
					'sSQL = "SELECT DISTINCT EventID, Event, StartDate, EndDate, Location, ImgPath FROM afxEvents WHERE 1=1 "
					
					sSQL = "SELECT DISTINCT E.EventID, E.Event, E.StartDate, E.EndDate, E.City, E.ImgPath, E.Location, E.Active, EET.EventID, ET.EventType, S.State, E.Complete, E.UseTracker " & _
							"FROM afxEvents as E " & _
							"LEFT OUTER JOIN afxEventsEventTypeX as EET on (E.EventID = EET.EventID) " & _
							"LEFT OUTER JOIN afxEventType as ET on (EET. EventTypeID = ET.EventTypeID) " & _
							"LEFT OUTER JOIN States as S on (E.StateID = S.StateID) " 
							
					sSql = sSql & "WHERE E.CompanyId = '" & session("UserCompanyId") & "' "

                    if trim(iLicenseID) <> "" then
                        sSQL = sSQL & " AND LicenseID = '" & iLicenseID & "'"
                    'else
                    '    sSQL = sSQL & " AND LicenseID IS NULL "
                    end if
					
                    sSQL = sSQL & " AND EventGroup = 1 AND ParentID IS NULL "

					sSQL = sSQL & " ORDER BY E.StartDate ASC"
					rsEvent.Open sSQL, objConn, 3, 1

					If Not rsEvent.EOF Then
						%>
						<tr>
							<td class="wk_title" height="20" width="40">Edit</td>
							<td class="wk_title" height="20" colspan="4">Event Group Title</td>
							<td class="wk_title" height="20">Start Date</td>
							<td class="wk_title" height="20">End Date</td>
							<td class="wk_title" height="20">Alert Emails</td>
							<td class="wk_title" height="20">Completed</td>
							<td class="wk_title" height="20" width="60">Delete</td>
						</tr>
						<%

						Do While Not rsEvent.EOF
							sDateArray = Split(rsEvent("StartDate"), " ")							
							sEventDate = sDateArray(0)
							sEventDay = Day(rsEvent("StartDate"))
							DateComp = DateValue(rsEvent("StartDate"))				

							
								iEventID = rsEvent("EventID")
								sEvent = rsEvent("Event")
                                if cstr(iEventID) = iParentID then
                                    sParentName = sEvent
                                end if
                                sStartDateArray = Split(rsEvent("StartDate"), " ")
								sEndDateArray = Split(rsEvent("EndDate"), " ")							
                                sEventStartDate = sStartDateArray(0)
								sEventEndDate = sEndDateArray(0)
                                bUseTracker = rsEvent("UseTracker")
                                bCompleted = rsEvent("Complete")
								bActive = rsEvent("active")															
								sCity = rsEvent("City")
								sState = rsEvent("State")
								
								if (trim(sCity) <> "" or trim(sState) <> "") then
									if (trim(sCity) <> "" and trim(sState) <> "") then
										sLocation = rsEvent("City") & ", " & rsEvent("state") 
									elseif (trim(sCity) <> "") then
										sLocation = rsEvent("City")
									else
										sLocation = rsEvent("state")
									end if
								else
									sLocation = ""
								end if

								sEventType = rsEvent("EventType")
								sDateArray = Split(rsEvent("StartDate"), " ")
								sImgPath = rsEvent("ImgPath")
								
								'Must set a check for when the time is equal to 12AM,
								'because the Database does not store the time value for 12AM
								if (UBound(sDateArray) > 0) then
									sEventTime = sDateArray(1)
									sTime = Split(sEventTime, ":")
									sTimeStart = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
								else
									'sTimeStart = "12:00"
									sTimeStart = ""
								end if
								
								'Pull the 1st DateID for the event for the Event preview
								set oDateRS = server.createobject("ADODB.RecordSet")
								
								sSQL = "SELECT TOP 1 DateID FROM afxEventDisplayDateX WHERE EventID = " & rsEvent("EventID") & " Order By DateId"
								oDateRS.cursorlocation = 3
								oDateRS.CursorType = 3
							    oDateRS.Open sSQL, objConn
							    oDateRS.ActiveConnection = nothing
								
								if not oDateRS.EOF then
									iDateID = oDateRS("DateID")
								end if
								
								set oDateRS = nothing
    						    %>
						        <tr>
							        <td class="desc"><a href="EditEvent.asp?eventType=<%=sEventType%>&mode=<%=sMode%>&eventid=<%=iEventID%>&EventGroup=1<%if iLicenseID <> "" then Response.Write("&LicenseID=" & iLicenseID) %>">Edit</a></td>
							        <td class="desc" colspan="4"><a href="EventGroupList.asp?ParentID=<%=iEventID%><%if iLicenseID <> "" then Response.Write("&LicenseID=" & iLicenseID) %>"><%=sEvent%></a></td>							
							        <td class="desc"><%=sEventStartDate%></td>							
							        <td class="desc">
							        <% 
								        if sEventEndDate = "1/1/2020" then
									        response.write("On-Going")
								        else
									        response.write(sEventEndDate)
								        end if
							        %>
							        </td>
							        <% if bUseTracker = 1 Then sUseTracker = true
								        if bUseTracker = 0 Then sUseTracker = false	%>
							        <td class="desc"><%=sUseTracker %></td>
							        <% if bCompleted = 1 or bCompleted = true Then sCompleted = true
								        if bCompleted = 0 or bCompleted = false Then sCompleted = false	%>
							        <td class="desc"><%=sCompleted%></td>							
							        <td class="desc"><a href="DelEventConfirm.asp?delevent=<%=iEventID%>&EventGroup=1&LicenseID=<%=iLicenseID%>">Delete</a></td>
							        <td class="desc"><%=sActive%></td>
						        </tr>
					            <%
						
								i = i + 1
								rsEvent.MoveNext
							
								If rsEvent.EOF Then Exit Do
														
						Loop
						
						rsEvent.Close
						%>
						    <tr>
							    <td colspan="6"><img src="/admin/calendarfx/media/images/spacer.gif" width="20" height="10"></td>
						    </tr>		
                        </table>
				        <table cellpadding="4" cellspacing="0" border="0">
                        <%
                        if iParentID <> "" then
                        %>
                            <tr>
                                <td colspan="100%"><h3><%=sParentName%></h3></td>
                            </tr>
                        <%
                            sSQL = "SELECT E.EventID AS eID, E.Event, E.StartDate, E.EndDate, E.City, E.ImgPath, E.Location, E.Active, EET.EventID, ET.EventType, S.State, E.Directions, E.Complete " & _
							    "FROM afxEvents as E " & _
							    "LEFT OUTER JOIN afxEventsEventTypeX as EET on (E.EventID = EET.EventID) " & _
							    "LEFT OUTER JOIN afxEventType as ET on (EET. EventTypeID = ET.EventTypeID) " & _
							    "LEFT OUTER JOIN States as S on (E.StateID = S.StateID) " & _
                                "WHERE E.CompanyId = '" & session("UserCompanyId") & "' " & _
                                "AND E.ParentId = '" & iParentID & "' AND E.ParentID IS NOT NULL" & _
                                " ORDER BY CASE WHEN E.StartDate = '1900-01-01' OR E.StartDate IS NULL THEN '2100-12-31' ELSE E.StartDate END ASC"
					        
                            rsEvent.Open sSQL, objConn, 3, 1
                            
                            If Not rsEvent.EOF Then
						
						        'Get page number of which records are showing
						        intPage = Request("page_number")
						
						        If intPage = "" Then intPage = 1
						
						        If iNumEvent = "" Then iNumEvent = 15
						
						        'Set the number of records displayed on a page
						        rsEvent.PageSize = iNumEvent
						        rsEvent.Cachesize = rsEvent.PageSize
						        intPageCount = rsEvent.PageCount
						
						        'determine which search page the user has requested.
		     			        If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount
						
		      			        If CInt(intPage) <= 0 Then intPage = 1
						
						        'Set the beginning record to be display on the page
		        		        rsEvent.AbsolutePage = intPage
						
						        i = 0
						        Do While i < rsEvent.pagesize
							        'sDateArray = Split(rsEvent("StartDate"), " ")							
							        'sEventDate = sDateArray(0)
							        'sEventDay = Day(rsEvent("StartDate"))
							        'DateComp = DateValue(rsEvent("StartDate"))				

						        %>
						        <tr>
							        <td class="wk_title" height="20">Title</td>
							        <td class="wk_title" height="20">Date</td>
							        <td class="wk_title" height="20">Notes</td>
							        <td class="wk_title" height="20">File</td>
							        <td class="wk_title" height="20">Completed</td>
                                    <td class="wk_title" height="20">Delete</td>
						        </tr>
						        <%
							
							        Do Until (i >= rsEvent.pagesize)
								
								        iEventID = rsEvent("eID")
								        sEvent = rsEvent("Event")
                                        if not isnull(rsEvent("EndDate")) then
                                            sEndDateArray = Split(rsEvent("EndDate"), " ")							
								            sEventEndDate = sEndDateArray(0)
                                        else
                                            sEventEndDate = ""
                                        end if
								        bActive = rsEvent("active")															
								        sCity = rsEvent("City")
								        sState = rsEvent("State")
                                        sDirections = rsEvent("Directions")
                                        bCompleted = rsEvent("Complete")

								        if (trim(sCity) <> "" or trim(sState) <> "") then
									        if (trim(sCity) <> "" and trim(sState) <> "") then
										        sLocation = rsEvent("City") & ", " & rsEvent("state") 
									        elseif (trim(sCity) <> "") then
										        sLocation = rsEvent("City")
									        else
										        sLocation = rsEvent("state")
									        end if
								        else
									        sLocation = ""
								        end if

								        sEventType = rsEvent("EventType")
                                        if not isnull(rsEvent("StartDate")) then
								            sDateArray = Split(rsEvent("StartDate"), " ")
                                        end if
								        sImgPath = rsEvent("ImgPath")
								
								        'Must set a check for when the time is equal to 12AM,
								        'because the Database does not store the time value for 12AM
								        if (UBound(sDateArray) > 0) then
									        sEventTime = sDateArray(1)
									        sTime = Split(sEventTime, ":")
									        sTimeStart = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
								        else
									        'sTimeStart = "12:00"
									        sTimeStart = ""
								        end if
								
								        'Pull the 1st DateID for the event for the Event preview
								        set oDateRS = server.createobject("ADODB.RecordSet")
								
								        sSQL = "SELECT TOP 1 DateID FROM afxEventDisplayDateX WHERE EventID = " & rsEvent("eID") & " Order By DateId"
								        oDateRS.cursorlocation = 3
								        oDateRS.CursorType = 3
							            oDateRS.Open sSQL, objConn
							            oDateRS.ActiveConnection = nothing
								
								        if not oDateRS.EOF then
									        iDateID = oDateRS("DateID")
								        end if
								
								        set oDateRS = nothing
						        %>
						        <tr>
                                    <td class="desc"><input type="hidden" name="txtEventID<%=i%>" value="<%=iEventID%>" /><input type="text" name="txtEventName<%=i%>" value="<%=sEvent%>" onchange="SetDirty()" /></td>
							        <td class="desc"><input name="txtEventDate<%=i%>" id="txtEventDate" value="<%=sEventEndDate%>" size="15" onchange="SetDirty()">
						                <a href="javascript:show_calendar('frm2.txtEventDate<%=i%>');" onmouseover="window.status='Date Picker';return true;" onmouseout="window.status='';return true;" onclick="SetDirty()">
						                <img src="/admin/calendarfx/Media/Images/calicon.gif" border=0></a></td>							
							        <td class="desc"><textarea name="txtDirections<%=i%>" onChange="SetDirty()" ><%=sDirections%></textarea></td>
							        <% 
								        dim sPath
								        If sImgPath <> "" Then 
									        sPath = "<a href='AddPicture.asp?eventid=" & rsEvent("eID") & "&EventGroup=1&LicenseID=" & iLicenseID & "' onclick='return CheckClean();'>Change</a>"
								        Else 
									        sPath = "<a href='AddPicture.asp?eventid=" & rsEvent("eID") & "&EventGroup=1&LicenseID=" & iLicenseID & "' onclick='return CheckClean();'>Add</a>"
								        End If %>
							        <td class="desc"><%=sPath%></td>
							        <td class="desc"><input type="checkbox" name="chkCompleted<%=i%>" <%if bCompleted = "True" then response.write("checked=""true""") %> onclick="SetDirty()" /></td>							
							        <td class="desc"><a href="DelEventConfirm.asp?delevent=<%=iEventID%>&EventGroup=1&LicenseID=<%=iLicenseID%>&ParentID=<%=iParentID%>" onclick="return CheckClean();">Delete</a></td>
						        </tr>
					        <%
						
								        i = i + 1
								        rsEvent.MoveNext
							
								        If rsEvent.EOF Then Exit Do
														
							        Loop
		
							        If rsEvent.eof Then Exit Do
						        Loop
						
						        rsEvent.Close
						        Set rsEvent = Nothing
						
						        Set rs = Nothing
					        %>
                            <tr>
                                <td>
                                    <input type="hidden" name="EventCount" value="<%=i%>" />
                                </td>
                            </tr>
					        <tr>
						        <td colspan="6">
							        <%
								        response.write("Page " & intPage & " of " & intPageCount & "<br>")
				
								        If CInt(intPage) > 1 Then
								        response.write("<a href=""list.asp?EventType=" & sEventType & "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & iMonthID & "&date=" & dDate & "&year=" & iYear & "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) - 1 & """>Previous Page</a>")
								        End If
				
								        If CInt(intPage) < CInt(intPageCount) Then
									        If CInt(intPage) = 1 Then 
										        response.write("<a href=""list.asp?EventType=" & sEventType & "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & iMonthID & "&date=" & dDate & "&year=" & iYear & "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) + 1 & """>Next Page</a>")
									        Else
										        response.write("&nbsp;|&nbsp;")
										        response.write("<a href=""list.asp?EventType=" & sEventType &  "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & monthid & "&date=" & dt & "&year=" & yr &  "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) + 1 & """>Next Page</a>")
									        End If
								        End If
							        %>
						        </td>
					        </tr>
                            <%else%>
                            <tr>
							    <td class="week" height="20" colspan="6">
								    No Events Found.						
							    </td>
						    </tr>
                            <%end if%>
			  		        <tr>
						        <td colspan="6">&nbsp;<br>
                                    <input type="submit" name="submit" value="add" style="margin:0; border:0; padding:0; text-indent:-1000px;background-position:center center; background-repeat:no-repeat;background-image: url(/admin/calendarfx/media/images/btn_add.gif); width: 88px; height: 26px;" onclick="return CheckClean();" />
                                    &nbsp;&nbsp;
							        <input type="submit" name="submit" value="submit" style="margin:0; border:0; padding:0; text-indent:-1000px;background-position:center center; background-repeat:no-repeat;background-image: url(/admin/calendarfx/media/images/btn_submit.gif); width: 88px; height: 26px;">
							        &nbsp;&nbsp;
						        </td>
					        </tr>
                        <%end if%>

                    <%Else%>
					    <tr>
							<td class="week" height="20" colspan="6">
								No Events Found.						
							</td>
						</tr>
					<%End If%>
					</FORM>
				</table>			
			<%' end if %>
		</td>
	</tr>
</table>







									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>







<%
	' -----------------------------------------------------------------------------------------------
	function DaysInMonth(p_nMonth, p_nYear)
		dim nRetVal
		
		' Convert Month to Integer:
		if p_nMonth <> "" then 
			p_nMonth = cint(p_nMonth)
		else
			nRetVal = 0
		end if
		
		' Conver Year to Integer:
		if p_nYear <> "" then p_nYear = cint(p_nYear)

		' Go Ahead if Month isn't blank:
		if p_nMonth <> "" then
			' Months with 30 days:
			if p_nMonth = 4 or p_nMonth = 6 or p_nMonth = 9 or p_nMonth = 11 then
				nRetVal = 30
			
			' February and Leap Year:
			elseif p_nMonth = 2 then
				if p_nYear = "" then
					nRetVal = 28
				else
					' Check for Leap Year:
					if p_nYear MOD 4 = 0 then
						nRetVal = 29
					else
						nRetVal = 28
					end if
				end if

			' All other months:
			else
				nRetVal = 31
			end if
		end if

		' Return the number of days:
		DaysInMonth = nRetVal
	end function

	' -----------------------------------------------------------------------------------------------
' BuildDate
' Will build an AND clause onto the given fieldname:
function BuildDate(p_nMonth, p_nDay, p_nYear, p_sFieldName)

	dim nMonth, nDay, nYear
	dim sRetVal
	sRetVal = ""
	
	if p_nMonth <> "" then 
		p_nMonth = cint(p_nMonth)
		if p_nMonth = 0 then p_nMonth = ""
	end if

	if p_nDay <> "" then 
		p_nDay = cint(p_nDay)
		if p_nDay = 0 then p_nDay = ""
	end if

	if p_nYear <> "" then
		p_nYear = cint(p_nYear)
		if p_nYear = 0 then p_nYear = ""
	end if

	if p_nMonth = "" then	

		' If day is blank, then do nothing

		' If the year is blank:
		if p_nYear <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '1/1/" & p_nYear & " 00:00:00'" _
					& " AND " & p_sFieldName & " <= '12/31/" & p_nYear & " 23:59:59' "
		end if
	
	' If the Day is blank:
	elseif p_nDay = "" then
		
		' If the Year is selected and the Month is not, then get everything in the current year
			' Already took care of this.

		' If the Month is selected and the Year is not, then get everything in the current month in the current year
		if p_nMonth <> "" and p_nYear = "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/1/" & Year(Now) & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & DaysInMonth(p_nMonth, Year(Now)) & "/" & Year(Now) & " 23:59:59'"

		' If both the Year and the Month Are selected, then get everything in the selected month in the selected year
		elseif p_nYear <> "" and p_nMonth <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/1/" & p_nYear & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & DaysInMonth(p_nMonth, p_nYear) & "/" & p_nYear & " 23:59:59'"
		end if
		

	' If the Year is blank:
	elseif p_nYear = "" then
		
		' Month is selected and Day is not, then get everything in the current Month in the current year
			' Already took care of this
			
		' Day is selected but month is not, then do nothing

		' If both the Month and the day are selected, then get everything in the selected month and selected day of the current year
		if p_nMonth <> "" and p_nDay <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/" & p_nDay & "/" & Year(Now) & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & p_nDay & "/" & Year(Now) & " 23:59:59'"
		end if
	
	' If All Fields are Not blank, get the specific date:
	else
		sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/" & p_nDay & "/" & p_nYear & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & p_nDay & "/" & p_nYear & " 23:59:59'"
	end if

	BuildDate = sRetVal

end function

%>
	

<%	
set objConn = nothing
set rs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

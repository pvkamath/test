<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/calendar/includes/RecurDateMethods.asp" ----->
<!-- #include virtual = "/includes/License.Class.asp" ----->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	const MIN_ACCESS_LEVEL = 5		' Loyola Specific Template Constant
	'checkSecurity							' From Security.asp

	' Template Constants -- Modify as needed per each page:

	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Please wait . . ."
	
	
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()




	
	
	

	Dim iEventID, sEvent, iSectionID, dStartDate, dEndDate, sHour, sMinute, sAMPM
	Dim eHour, eMinute, eAMPM, sLocation, sOrganization, sPhone, iEventTypeID
	Dim sURL, sContact, sEmail, sInfo, iActive, sAction, word, iComplete, iLicenseID
	Dim objConn, rs, sSQL, strSQLConnectionString
	dim tmpDateEnd, sMode
	Dim iCourseID, sCity, iStateID
	dim bUseTracker
	dim iBranchId, iCompanyL2Id, iCompanyL3Id
    dim bEventGroup
	
	sEvent = replace(request("event"), "'", "''")
    iLicenseID = request("LicenseID")

	'Set Owner ID
    if isnull(iLicenseID) or iLicenseID = "" then
    	sOwnerId = ScrubForSql(request("OwnerId"))
	    if left(sOwnerId, 1) = "a" then
		    iCompanyL2Id = mid(sOwnerId, 2)
	    elseif left(sOwnerId, 1) = "b" then
		    iCompanyL3Id = mid(sOwnerID, 2)
	    elseif left(sOwnerId, 1) = "c" then
		    iBranchId = mid(sOwnerId, 2)
	    end if	
    else
        dim oLicenseObj
        set oLicenseObj = new License

        oLicenseObj.ConnectionString = application("sDataSourceName")
        oLicenseObj.LoadLicenseById(iLicenseId)

        if oLicenseObj.OwnerCompanyId = session("UserCompanyId") then 'Additional verification so that a user can only load information for a company they belong to
            select case oLicenseObj.OwnerTypeId
                case 2
                    iBranchId = oLicenseObj.OwnerId
                case 4 
                    iCompanyL2Id = oLicenseObj.OwnerId
                case 5
                    iCompanyL3Id = oLicenseObj.OwnerId
            end select
        end if
    end if

	iSectionID = request("SectionID")
	dStartDate = request("startdate")
	dEndDate = trim(request("enddate"))
	'if end date is blank, then it's an on-going event
	if dEndDate = "" then
		dEndDate = "1/1/2500"
	end if
	sHour = request("shour")
	sMinute = request("sminute")
	sAMPM = request("sampm")
	eHour = request("ehour")
	eMinute = request("eminute")
	eAMPM = request("eampm")
	iEventTypeID = request("EventTypeID")
	iCourseID = replace(request("CourseID"), "'", "''")
	sCity = replace(request("City"), "'", "''")
	iStateID = replace(request("State"), "'", "''")
	sLocation = replace(request("location"), "'", "''")
	sOrganization = replace(request("organization"), "'", "''")
	sMode = request("mode")
	'sPhone = request("phone")
	sURL = replace(request("url"), "'", "''")
	'sContact = replace(request("contact"), "'", "''")
	'sEmail = request("email")
	sInfo = replace(request("info"), "'", "''")
	iActive = request("active")
    iComplete = request("Completed")
	if request("UseTracker") = "1" then
		bUseTracker = true
	end if
	sAction = request("editmode")
	sRecurrenceType = replace(request("RecurrenceType"), "'", "''")
	iWeekCycle = replace(request("WeekCycle"), "'", "''")
	sWeekDays = replace(request("WeekDays"), "'", "''")
	iMonthCycle = replace(request("MonthCycle"), "'", "''")
	iMonthOccurrence = replace(request("MonthOccurrence"), "'", "''")
	sMonthWeekDayOfOccurrence = replace(request("MonthWeekDayOfOccurrence"), "'", "''")
	iEventSpan = replace(request("EventSpan"), "'", "''")
	dStartDate = trim(dStartDate) 
    bEventGroup = request("EventGroup")

	if len(sHour) <> 0 then	
		tmpDateEnd =  sHour & ":" & sMinute & " " & UCase(sAMPM)
		
		if ucase(trim(tmpDateEnd)) = ucase("12:00 am") then
			tmpDateEnd = "00:00:01"
		end if
		
		dStartDate = dStartDate & " " & tmpDateEnd
	end if
	
	dEndDate = trim(dEndDate) 
	
	if len(eHour) <> 0 then
		tmpDateEnd = eHour & ":" & eMinute & " " & UCase(eAMPM)

		if ucase(trim(tmpDateEnd)) = ucase("12:00 am") then
			tmpDateEnd = "00:00:01"
		end if

		dEndDate = dEndDate & " "  & tmpDateEnd
	end if

	
	If iActive = "on" Then
		iActive = "1"
	Else
		iActive = "0"
	End If

   	If iComplete = "on" Then
		iComplete = "1"
	Else
		iComplete = "0"
	End If

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	Select Case sAction
		Case "Add", "Copy"
			'Add new event
			sSQL = "INSERT INTO afxEvents (CompanyId, StartDate, EndDate, Event, City, Location, " & _
					"Phone, WebAddress, Contact, Email, Directions, Organization, Active, Complete, UseTracker, SectionID " 
			
			'if Course ID is set	
			if trim(iCourseID) <> "" then
				sSQL = sSQL & ",CourseID"
			end if
			
			'if stateID is set
			if trim(iStateID) <> "" then
				sSQL = sSQL & ",StateID"
			end if					
			
			'if Divison is set
			if trim(iCompanyL2Id) <> "" then
				sSQL = sSQL & ",CompanyL2Id"
			end if
			
			'if Region is set
			if trim(iCompanyL3Id) <> "" then
				sSQL = sSQL & ",CompanyL3Id"
			end if
			
			'if Branch is set
			if trim(iBranchId) <> "" then
				sSQL = sSQL & ",BranchID"
			end if
			
            if trim(iLicenseId) <> "" then
                sSQL = sSQL & ",LicenseID"
            end if

            sSQL = sSQL & ",EventGroup"
            
			sSQL = sSQL & ") VALUES ('" & session("UserCompanyId") & "', '" & dStartDate & "', '" & dEndDate & "', '" & _
					sEvent & "', '" & sCity & "','" & sLocation & "', '" & _
					sPhone & "', '" & sURL & "', '" & sContact & "', '" & sEmail & "', '" & sInfo & "', '" & sOrganization & "', '" & _
					iActive & "', '" & iComplete & "', '" & abs(cint(bUseTracker)) & "', '" & iSectionID & "'"
			
			'if Course ID is set	
			if trim(iCourseID) <> "" then
				sSQL = sSQL & ",'" & iCourseID & "'"
			end if
			
			'if stateID is set
			if trim(iStateID) <> "" then
				sSQL = sSQL & ",'" & iStateID & "'"
			end if			
			
			'if Divison is set
			if trim(iCompanyL2Id) <> "" then
				sSQL = sSQL & "," & iCompanyL2Id
			end if
			
			'if Region is set
			if trim(iCompanyL3Id) <> "" then
				sSQL = sSQL & "," & iCompanyL3Id
			end if
			
			'if Branch is set
			if trim(iBranchId) <> "" then
				sSQL = sSQL & "," & iBranchId
			end if

            if trim(iLicenseId) <> "" then
                sSQL = sSQL & "," & iLicenseID
            end if
			
            if bEventGroup = 1 then
                sSQL = sSQL & "," & 1
            else
                sSQL = sSQL & "," & 0
            end if

			sSQL = sSQL & ")"

			objConn.Execute(sSQL)
			
			'Get the max event id which is the new event id
			sSQL = "Select max(EventID) From afxEvents"
			
			rs.Open sSQL, objConn, 3, 1	
			
			if not rs.EOF then
				iEventID = rs(0)

				sSQL = "INSERT INTO afxEventsEventTypeX (EventID, EventTypeID) " & _
						"VALUES ('" & iEventID & "', '" & iEventTypeID & "')"
				objConn.Execute(sSQL)
			end if
			rs.Close
		Case "Edit"
			
			'if session("access_level") >= 10 then
				iEventID = Request("eventid")
				
				sSQL = "UPDATE afxEvents " & _
					   "SET StartDate = '" & dStartDate & "', " & _
					   "	EndDate = '" & dEndDate & "', " & _
					   "	Event = '" & sEvent & "', "
					   
				if trim(iCourseID) <> "" then
					sSQL = sSQL & "	CourseID = '" & iCourseID & "', "
				end if
				
				if trim(iStateID) <> "" then
					sSQL = sSQL & "	StateID = '" & iStateID & "', " 
				end if 
				stop
				sSQL = sSQL & "	City = '" & sCity & "', " & _
	  					      "	Location = '" & sLocation & "', " & _
					   		  "	Phone = '" & sPhone & "', " & _
					   		  "	WebAddress = '" & sURL & "', " & _
					   		  "	Contact = '" & sContact & "', " & _
					   		  "	Organization = '" & sOrganization & "', " & _
					   		  "	Email = '" & sEmail & "', " & _
					   		  "	Directions = '" & sInfo & "', " & _
					   		  "	Active = '" & iActive & "', " & _
					   		  " UseTracker = '" & abs(cint(bUseTracker)) & "', " & _
					   		  "	SectionID = '" & iSectionID & "'"
							  
				'if Divison is set
				if trim(iCompanyL2Id) <> "" then
					sSQL = sSQL & ", CompanyL2Id = " & iCompanyL2Id
				else
					sSQL = sSQL & ", CompanyL2Id = null"
				end if
				
				'if Region is set
				if trim(iCompanyL3Id) <> "" then
					sSQL = sSQL & ", CompanyL3Id = " & iCompanyL3Id
				else
					sSQL = sSQL & ", CompanyL3Id = null"
				end if 
				
				'if Branch is set
				if trim(iBranchId) <> "" then
					sSQL = sSQL & ", BranchID = " & iBranchId 
				else
					sSQL = sSQL & ", BranchID = null"
				end if 
							  
		   		 sSQL = sSQL & " Where EventID = '" & iEventID & "'"
				response.write(sSQL)
				objConn.Execute(sSQL)
				
				sSQL = "UPDATE afxEventsEventTypeX SET EventTypeID = '" & iEventTypeID & "' " & _
						"WHERE EventID = '" & iEventID & "'"
				
				objConn.Execute(sSQL)
	End Select

	'Delete display date Xreference table entries
	sSQL = "DELETE FROM afxEventDisplayDateX WHERE EventID = '" & iEventID & "'; "
	objConn.Execute(sSQL)	

	'Add display date to Xreference table for regular events
	if trim(sRecurrenceType) = "" then
		sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
			   "VALUES ('" & iEventID & "', '" & dStartDate & "','" & dEndDate & "')"
			   
		objConn.Execute(sSQL)	
	end if
	
	'Process the Recurrence options
	'delete old options, if any
	sSQL = "DELETE FROM afxWeeklyRecurrenceEvents WHERE EventID = '" & iEventID & "'; " & _
		   "DELETE FROM afxMonthlyRecurrenceEvents WHERE EventID = '" & iEventID & "' "
	objConn.Execute(sSQL)
	
	if Ucase(sRecurrenceType) = "WEEKLY" AND iWeekCycle <> "" AND sWeekDays <> "" then
		'Retrieve all the dates this event will display
		sDateString = ComputeWeeklyRecurDates(dStartDate, dEndDate, iWeekCycle, sWeekDays)
		aRecurDates = split(sDateString, ", ")
		
		for each sRecurDate in aRecurDates
			'Since reccurrence events display on multiple days, the start and end date for a
			'given occurrence should be the same.  Add any time data to the recurrence date also.
			sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
				   "VALUES ('" & iEventID & "', '" & sRecurDate & " " & formatdatetime(dStartDate,3) & "','" & dateadd("d", iEventSpan-1, sRecurDate) & " " & formatdatetime(dEndDate,3) & "')"
	   
			objConn.Execute(sSQL)	
		next

		sSQL = "INSERT INTO afxWeeklyRecurrenceEvents(EventID, WeekCycle, Weekdays, EventSpan) " & _
			   "VALUES ('" & iEventID & "', '" & iWeekCycle & "', '" & sWeekDays & "','" & iEventSpan & "')"
			   
		objConn.Execute(sSQL)			 
	end if
	
	if Ucase(sRecurrenceType) = "MONTHLY" AND iMonthCycle <> "" AND iMonthOccurrence <> "" and sMonthWeekDayOfOccurrence <> "" then
		'Retrieve all the dates this event will display
		sDateString = ComputeMonthlyRecurDates(dStartDate, dEndDate, iMonthOccurrence, iMonthCycle, sMonthWeekDayOfOccurrence)
		aRecurDates = split(sDateString, ", ")
		
		for each sRecurDate in aRecurDates
			'Since reccurrence events display on multiple days, the start and end date for a
			'given occurrence should be the same.  Add any time data to the recurrence date also.
			sSQL = "INSERT INTO afxEventDisplayDateX(EventID, DisplayStartDate, DisplayEndDate) " & _
				   "VALUES ('" & iEventID & "', '" & sRecurDate & " " & formatdatetime(dStartDate,3) & "','" & dateadd("d", iEventSpan-1, sRecurDate) & " " & formatdatetime(dEndDate,3) & "')"
	   
			objConn.Execute(sSQL)	
		next
			
		sSQL = "INSERT INTO afxMonthlyRecurrenceEvents(EventID, Occurrence, MonthCycle, WeekDayOfOccurrence, EventSpan) " & _
			   "VALUES ('" & iEventID & "', '" & iMonthOccurrence & "', '" & iMonthCycle & "', '" & sMonthWeekDayOfOccurrence & "','" & iEventSpan & "')"
			   
		objConn.Execute(sSQL)			 
	end if
	
    dim redirectURL 
    redirectURL = "AddPicture.asp?eventid=" & iEventID
    if bEventGroup = "1" then
        redirectURL = redirectURL & "&EventGroup=1"
    end if
    if iLicenseID <> "" then
        redirectURL = redirectURL & "&LicenseID=" & iLicenseID
    end if
	response.redirect redirectURL
	Set rs = Nothing

	objConn.Close
	Set objConn = Nothing

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

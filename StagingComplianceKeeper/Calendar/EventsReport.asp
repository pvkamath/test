<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
' Template Constants -- Modify as needed per each page:

const USES_FORM_VALIDATION = False			' Template Constant
const SHOW_PAGE_BACKGROUND = True			' Template Constant
const TRIM_PAGE_MARGINS = True			' Template Constant
const SHOW_MENUS = True					' Template Constant
const SIDE_MENU_SELECTED = ""				' Template Constant
const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
dim sPgeTitle							' Template Variable
dim iPage


' Set Page Title:
sPgeTitle = "Events Report"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">

	$(function() {
		$(".datepicker").datepicker({dateFormat: 'mm/dd/yy'});
	});
		
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
	function InitializePage() {
	}
//-->
</script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Events Report</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<!-- content here -->										
											<td align="center" valign="top" height="200">
												<form name="frm1" action="/Calendar/EventsReportProc.asp" method="POST">
												<table cellpadding="4" cellspacing="0" border="0">
													<tr>
														<td valign="top" class="newstitle" nowrap>Start Date (From): </td>
														<td nowrap>
															<input type="text" id="StartDateFrom" name="StartDateFrom" value="" size="10" maxlength="10" class="datepicker" readonly>
														</td>	
														<td valign="top" class="newstitle" nowrap>End Date (From): </td>
														<td nowrap>
															<input type="text" id="EndDateFrom" name="EndDateFrom" value="" size="10" maxlength="10" class="datepicker" readonly>
														</td>												
													</tr>										
													<tr>
														<td valign="top" class="newstitle" nowrap>Start Date (To): </td>
														<td nowrap>
															<input type="text" id="StartDateTo" name="StartDateTo" value="" size="10" maxlength="10" class="datepicker" readonly>
														</td>	
														<td valign="top" class="newstitle" nowrap>End Date (To): </td>
														<td nowrap>
															<input type="text" id="EndDateTo" name="EndDateTo" value="" size="10" maxlength="10" class="datepicker" readonly>
														</td>	
													</tr>		
                                                    <tr>
                                                        <td valign="top" class="newstitle" nowrap>State: </td>
                                                        <td nowrap>
                                                            <%DisplayStatesListBox 0,0,"",false %>
                                                        </td>
                                                        <td valign="top" class="newstitle" nowrap>Structure: </td>
                                                        <td nowrap>
                                                            <%DisplayL2sL3sBranchesListBox 0,session("UserCompanyId"), false, 0, "OwnerID" %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="newstitle" nowrap>Title: </td>
                                                        <td nowrap><input type="text" id="EventName" name="EventName" /></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="newstitle" nowrap>Include Child Events: </td>
                                                        <td nowrap><input type="checkbox" id="IncludeChildEvents" name="IncludeChildEvents" /></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
													<tr>
														<td colspan="4" align="center">
															<br>
															<input type="image" src="/media/images/blue_bttn_search.gif" width="79" height="23" alt="Execute Search" border="0">
															<img src="/media/images/btn_clear.gif" width="79" height="23" alt="Clear Search" border="0" onclick="javascript:document.frm1.reset();">															
														</td>
													</tr>																												
												</table>
												</form>											
											</td>
											<!-- end content -->				
										</tr>    
							   		</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

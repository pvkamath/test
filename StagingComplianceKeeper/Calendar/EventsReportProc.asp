<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<%
dim sStartDateFrom, sStartDateTo, sEndDateFrom, sEndDateTo 'as string
dim sOwnerIDList, sStateIDList, sEventName, bShowChildren
dim sSQL 'as string
dim sReportDetails 'as string
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList
dim sUserAllowedCompanyL2IdList, sUserAllowedCompanyL3IdList, sUserAllowedBranchIdList

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

'User Access Lists
sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList()
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList()
sUserAllowedBranchIdList = GetUserBranchIdList()

'Validate input
if isdate(request("StartDateFrom")) then
	sStartDateFrom = request("StartDateFrom")	
end if

if isdate(request("StartDateTo")) then
	sStartDateTo = request("StartDateTo")	
end if

if isdate(request("EndDateFrom")) then
	sEndDateFrom = request("EndDateFrom")	
end if

if isdate(request("EndDateTo")) then
	sEndDateTo = request("EndDateTo")	
end if

if replace(request("State"), ", ", ",") <> "" then
    sStateIDList = replace(request("State"), ", ", ",")
end if

if replace(request("OwnerID"), ", ", ",") <> "" then
    sOwnerIDList = replace(request("OwnerID"), ", ", ",")
end if

if request("EventName") <> "" then
    sTitle = request("EventName")
end if

if request("IncludeChildEvents") = "on" then
    bShowChildren = true
else
    bShowChildren = false
end if

sSQLConnectionString = application("sDataSourceName")

set objConn = server.createobject("ADODB.Connection")
objConn.Open application("sDataSourceName")
set rs = server.createobject("ADODB.RecordSet")

'Get Events that match the search criteria
sSQL = "SELECT E.EventID, E.Event, E.StartDate, CASE WHEN E.EndDate = '1/1/2500' THEN NULL ELSE E.EndDate END AS EndDateRevised, E.City, ST.State, E.Directions, CL2.Name AS DivisionName, CL3.Name AS RegionName, CB.Name AS BranchName FROM afxEvents AS E " & _
			  "LEFT OUTER JOIN States AS ST ON (ST.StateID = E.StateID) " & _
			  "LEFT OUTER JOIN CompaniesL2 AS CL2 ON (CL2.CompanyL2ID = E.CompanyL2ID) " & _
			  "LEFT OUTER JOIN CompaniesL3 AS CL3 ON (CL3.CompanyL3ID = E.CompanyL3ID) " & _
			  "LEFT OUTER JOIN CompanyBranches AS CB ON (CB.BranchID = E.BranchID) " & _			  
		      "WHERE Event <> '' "

sSQL = sSQL & "AND E.CompanyId = '" & session("UserCompanyId") & "' "

'Restrict the list of events to only the entities to which the user has access.
'Division Clause
sSQL = sSQL & "AND (E.CompanyL2Id is null"
if trim(sUserAllowedCompanyL2IdList) <> "" then
		sSQL = sSQL & " OR E.CompanyL2Id in (" & sUserAllowedCompanyL2IdList & ")"
end if
sSQL = sSQL & ") "

'Region Clause
sSQL = sSQL & "AND (E.CompanyL3Id is null"
if trim(sUserAllowedCompanyL3IdList) <> "" then
	sSQL = sSQL & " OR E.CompanyL3Id in (" & sUserAllowedCompanyL3IdList & ")"
end if
sSQL = sSQL & ") "

'Branch Clause
sSQL = sSQL & "AND (E.BranchId is null"
if trim(sUserAllowedBranchIdList) <> "" then
	sSQL = sSQL & " OR E.BranchID in (" & sUserAllowedBranchIdList & ")"
end if
sSQL = sSQL & ") "		  
			  
if sStartDateFrom <> "" then
	sSQL = sSQL & "AND E.StartDate >= '" & sStartDateFrom & " 00:00' "
end if

if sStartDateTo <> "" then
	sSQL = sSQL & "AND E.StartDate <= '" & sStartDateTo & " 23:59' "
end if

if sEndDateFrom <> "" then
	sSQL = sSQL & "AND E.EndDate >= '" & sEndDateFrom & " 00:00' "
end if

if sEndDateTo <> "" then
	sSQL = sSQL & "AND E.EndDate <= '" & sEndDateTo & " 23:59' "
end if

if sStateIDList <> "" then
    aStateIdList = split(sStateIDList, ",")
    bFirstState = true
    for each iStateId in aStateIdList
			
	    if bFirstClause then
		    sSql = sSql & " WHERE ("
		    bFirstClause = false
	    elseif not bFirstState then
		    sSql = sSql & " OR "
	    else
		    sSql = sSql & " AND ("
	    end if
				
	    sSql = sSql & "E.StateID = '" & iStateId & "' "
				
	    bFirstState = false
			
    next
    sSql = sSql & ") "
end if

if sOwnerIDList <> "" then
	'We need to take the grouped-together list we got and break it into separate
	'lists for L2s, L3s, and Branches.  Where we get the list from depends on
	'where we are getting the search criteria.
	sAssocIdList = sOwnerIDList
	aiAssocIdList = split(sAssocIdList, ", ")
	for each sAssocId in aiAssocIdList
		if left(sAssocId, 1) = "a" then
			if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
				sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
				AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
			end if
		elseif left(sAssocId, 1) = "b" then
			if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
				sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
				AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
			end if
		elseif left(sAssocId, 1) = "c" then
			if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
				sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
			end if
		end if
	next
	
	'Remove the trailing commas
	if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
	if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
	if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)
	
    if sCompanyL2IdList <> "" then
        sSQL = sSQL & "AND E.CompanyL2Id IN (" & sCompanyL2IdList & ") "
    end if
    if sCompanyL3IdList <> "" then
        sSQL = sSQL & "AND E.CompanyL3Id IN (" & sCompanyL3IdList & ") "
    end if
    if sBranchIdList <> "" then
        sSQL = sSQL & "AND E.BranchId IN (" & sBranchIdList & ") "
    end if
end if

if sTitle <> "" then
    sSQL = sSQL & "AND E.Event LIKE '%" & replace(sTitle, "'", "''") & "%' "
end if


sSQL = sSQL & " AND ParentID IS NULL "
sSQL = sSQL & "ORDER BY E.StartDate, E.Event"

rs.Open sSQL, objConn, 3, 3

'Create Report Details String

'Headings
sReportDetails = """Name"","

if bShowChildren then
    sReportDetails = sReportDetails & """Parent Event"","
end if

sReportDetails = sReportDetails & """Start Date"",""End Date"",""City"",""State"",""Association"",""General Information""" & vbcrlf
if not rs.eof then
	do while not rs.eof
        dim EventName
        EventName = rs("Event")
        if isnull(EventName) then
            EventName = ""
        end if
        
        dim City
        City = rs("City")
        if isnull(City) then
            City = ""
        end if
		
        sReportDetails = sReportDetails & """" & replace(EventName, """", """""") & ""","

        if bShowChildren then
            sReportDetails = sReportDetails & """" & replace(EventName, """", """""") & ""","
        end if
							   sReportDetails = sReportDetails & """" & rs("StartDate") & """," & _
																 """" & rs("EndDateRevised") & """," & _
																 """" & replace(City, """", """""") & """," & _
																 """" & rs("State") & ""","

		'Determine the Association, if any
		if not isnull(rs("DivisionName")) and trim(rs("DivisionName")) <> "" then
			sReportDetails = sReportDetails & """" & replace(rs("DivisionName"), """", """""") & ""","		
		elseif not isnull(rs("RegionName")) and trim(rs("RegionName")) <> "" then
			sReportDetails = sReportDetails & """" & replace(rs("RegionName"), """", """""") & ""","		
		elseif not isnull(rs("BranchName")) and trim(rs("BranchName")) <> "" then
			sReportDetails = sReportDetails & """" & replace(rs("BranchName"), """", """""") & ""","		
		else
			sReportDetails = sReportDetails & """N/A"","
		end if
															 
		sReportDetails = sReportDetails & """" & replace(rs("Directions"), """", """""") & """" & vbcrlf
		
        if bShowChildren then
            set rs2 = server.createobject("ADODB.RecordSet")

            sSQL = "SELECT E.Event, E.StartDate, CASE WHEN E.EndDate = '1/1/2500' THEN NULL ELSE E.EndDate END AS EndDateRevised, E.City, ST.State, E.Directions, CL2.Name AS DivisionName, CL3.Name AS RegionName, CB.Name AS BranchName FROM afxEvents AS E " & _
			              "LEFT OUTER JOIN States AS ST ON (ST.StateID = E.StateID) " & _
			              "LEFT OUTER JOIN CompaniesL2 AS CL2 ON (CL2.CompanyL2ID = E.CompanyL2ID) " & _
			              "LEFT OUTER JOIN CompaniesL3 AS CL3 ON (CL3.CompanyL3ID = E.CompanyL3ID) " & _
			              "LEFT OUTER JOIN CompanyBranches AS CB ON (CB.BranchID = E.BranchID) " & _			  
		                  "WHERE Event <> '' "

            sSQL = sSQL & "AND E.CompanyId = '" & session("UserCompanyId") & "' "
            sSQL = sSQL & "AND E.ParentId = '" & rs("EventID") & "' "

            sSQL = sSQL & "ORDER BY E.StartDate, E.Event "

            rs2.Open sSQL, objConn, 3, 3

            do while not rs2.eof
                ChildEventName = rs2("Event")
                if isnull(ChildEventName) then
                    ChildEventName = ""
                end if
        
                City = rs2("City")
                if isnull(City) then
                    City = ""
                end if
		
                sReportDetails = sReportDetails & """" & replace(ChildEventName, """", """""") & """," 
                if bShowChildren then
                    sReportDetails = sReportDetails & """" & replace(EventName, """", """""") & ""","
                end if
							           sReportDetails = sReportDetails & """" & rs("StartDate") & """," & _
																         """" & rs2("EndDateRevised") & """," & _
																         """" & replace(City, """", """""") & """," & _
																         """" & rs2("State") & ""","

		        'Determine the Association, if any
		        if not isnull(rs2("DivisionName")) and trim(rs2("DivisionName")) <> "" then
			        sReportDetails = sReportDetails & """" & replace(rs2("DivisionName"), """", """""") & ""","		
		        elseif not isnull(rs2("RegionName")) and trim(rs2("RegionName")) <> "" then
			        sReportDetails = sReportDetails & """" & replace(rs2("RegionName"), """", """""") & ""","		
		        elseif not isnull(rs2("BranchName")) and trim(rs2("BranchName")) <> "" then
			        sReportDetails = sReportDetails & """" & replace(rs2("BranchName"), """", """""") & ""","		
		        else
			        sReportDetails = sReportDetails & """N/A"","
		        end if
															 
		        sReportDetails = sReportDetails & """" & replace(rs2("Directions"), """", """""") & """" & vbcrlf

                rs2.MoveNext
            Loop
        end if
        
		rs.MoveNext
	loop
else
	sReportDetails = sReportDetails & "There are currently no results."
end if

set rs = nothing
set objConn = nothing

response.ContentType = "application/download"
response.AddHeader "Content-Disposition", "attachment; filename=" & "EventsReport.csv"	
response.write(sReportDetails)
%>
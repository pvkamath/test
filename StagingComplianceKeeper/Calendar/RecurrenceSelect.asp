<%
sRecurrenceType = trim(request("RecurrenceType"))
%>

<html>
<head>
<title>Recurrence Selection</title>
<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/style.css"">

<script language="JavaScript">
function ChangeType(obj)
{
	var sType = obj[obj.selectedIndex].value
	window.parent.ChangeFrame(sType.toUpperCase());
}
</script>
</head>

<body>

<table width="100%" align="center" cellpadding="5" bgcolor="" background="/admin/login/media/images/hdr-bgr.gif">
	<tr>
		<td align="center">
			<b>Please Select the Recurrence Type</b>
		</td>
	</tr>
	<tr>
		<td align="center">
			<form name="frm1">
				<select name="Type" onChange="javascript:ChangeType(this)">
					<option value="">Select Recurrence Type</option>
					<option value="weekly" <% if ucase(sRecurrenceType) = "WEEKLY" then  response.write("selected") end if %>>Weekly</option>
					<option value="monthly" <% if ucase(sRecurrenceType) = "MONTHLY" then response.write("selected") end if %>>Monthly</option>
				</select>
			</form>
		</td>
	</tr>
</table>

</body>
</html>

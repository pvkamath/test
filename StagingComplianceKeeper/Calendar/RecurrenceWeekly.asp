<%
iWeekCycle = trim(request("WeekCycle"))
sWeekDays = trim(request("WeekDays"))
iEventSpan = request("EventSpan")

'if recurrence set, enable remove button
if iWeekCycle <> "" then
	sRemoveBtn = ""
	
	aWeekDay = split(sWeekDays, ", ")

	for each sDay in aWeekDay
		select case UCase(sDay)
			case "SUNDAY"
				bSunday = true
			case "MONDAY"
				bMonday = true
			case "TUESDAY"
				bTuesday = true
			case "WEDNESDAY"
				bWednesday = true
			case "THURSDAY"
				bThursday = true
			case "FRIDAY"
				bFriday = true
			case "SATURDAY"
				bSaturday = true
		end select
	next
else
	sRemoveBtn = "disabled"
end if
%>

<html>

<script language="JavaScript" src="/admin/includes/FormCheck2.js"></script>
<script language="JavaScript">
function ValidateForm(FORM)
{
	//Check Recurrence Field
	if (!isDigit(FORM.WeekCycle.value) || (FORM.WeekCycle.value < 1)) 
	{
		alert('The recurrence field must be a number greater than 0.');
		FORM.WeekCycle.focus();
		return false;
	}
	
	//Check Week Day values
	bFound = false;
	i = 0;
	while ((i < FORM.SelectedDays.length) &&(!bFound))
	{
		if (FORM.SelectedDays[i].checked == true)
			bFound = true;
					
		i++;
	}
	
	if (!bFound)
	{
		alert('You must select atleast one weekday.');
		return false;
	}

	//Check Event Span
	if (!isDigit(FORM.EventSpan.value) || (FORM.EventSpan.value < 1)) 
	{
		alert('The event span must be a number greater than 0.');
		FORM.EventSpan.focus();
		return false;
	}	
	
	return true;
}

function SubmitValues(FORM)
{
	if (ValidateForm(FORM))
	{
		window.parent.opener.document.frm.RecurrenceType.value = "WEEKLY"
		var sWeekDays = "";
		for (i = 0; i < FORM.SelectedDays.length; i++)
		{
			if (FORM.SelectedDays[i].checked)
			{
				if (sWeekDays == "")
					sWeekDays = FORM.SelectedDays[i].value;
				else
					sWeekDays = sWeekDays + ", " + FORM.SelectedDays[i].value;
			}
		}
		window.parent.opener.document.frm.WeekCycle.value = FORM.WeekCycle.value;
		window.parent.opener.document.frm.WeekDays.value = sWeekDays;		
		window.parent.opener.document.frm.EventSpan.value = FORM.EventSpan.value;
		window.parent.opener.document.frm.RecurrenceLink.value = "recurrence.asp?RecurrenceType=WEEKLY&WeekCycle=" + FORM.WeekCycle.value + "&WeekDays=" + sWeekDays + "&EventSpan=" + FORM.EventSpan.value
		window.parent.opener.document.frm.MonthCycle.value = '';
		window.parent.opener.document.frm.MonthOccurrence.value = '';		
		window.parent.opener.document.frm.MonthWeekDayOfOccurrence.value = '';			
		window.parent.close();
	}
}

function RemoveValues(FORM)
{
	window.parent.opener.document.frm.RecurrenceType.value = '';
	window.parent.opener.document.frm.WeekCycle.value = '';
	window.parent.opener.document.frm.WeekDays.value = '';		
	window.parent.opener.document.frm.MonthCycle.value = '';
	window.parent.opener.document.frm.MonthOccurrence.value = '';		
	window.parent.opener.document.frm.MonthWeekDayOfOccurrence.value = '';					
	window.parent.opener.document.frm.EventSpan.value = '';	
	window.parent.opener.document.frm.RecurrenceLink.value = 'recurrence.asp';
	window.parent.close();
}
</script>
<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""/admin/login/includes/style.css"">
<body>

<form name="frm1">
<table align="center">
	<tr>
		<th align="center">Weekly Recurrence</th>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Recur every &nbsp;<input name="WeekCycle" type="text" value="<%= iWeekCycle %>" size="3" maxlength="3">&nbsp; week(s) on:</td>
	</tr>
	<tr>
		<td>
			<table>
				<tr>
					<td><input type="checkbox" name="SelectedDays" value="Sunday" <% if bSunday then response.write("checked") end if %>>&nbsp;Sunday</td>
					<td><input type="checkbox" name="SelectedDays" value="Monday" <% if bMonday then response.write("checked") end if %>>&nbsp;Monday</td>
					<td><input type="checkbox" name="SelectedDays" value="Tuesday" <% if bTuesday then response.write("checked") end if %>>&nbsp;Tuesday</td>
					<td><input type="checkbox" name="SelectedDays" value="Wednesday" <% if bWednesday then response.write("checked") end if %>>&nbsp;Wednesday</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="SelectedDays" value="Thursday" <% if bThursday then response.write("checked") end if %>>&nbsp;Thursday</td>
					<td><input type="checkbox" name="SelectedDays" value="Friday" <% if bFriday then response.write("checked") end if %>>&nbsp;Friday</td>
					<td><input type="checkbox" name="SelectedDays" value="Saturday" <% if bSaturday then response.write("checked") end if %>>&nbsp;Saturday</td>
				</tr>			
			</table>
		</td>
	</tr>
	<tr>
		<td>
			The event spans&nbsp;<input type="text" size="3" maxlength="3" name="EventSpan" value="<%=iEventSpan%>">&nbsp;days
		</td>
	</tr>	
	<tr>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td align="center"><input type="button" name="okBtn" value="OK" onClick="SubmitValues(this.form)">&nbsp;<input type="button" name="cancelBtn" value="Cancel" onClick="javascript:window.parent.close()">&nbsp;<input type="button" name="removeBtn" value="Remove" <%= sRemoveBtn %> onClick="RemoveValues(this.form)"></td>
	</tr>
</table>
</form>

</body>
</html>

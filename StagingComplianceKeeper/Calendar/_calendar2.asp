<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/CalendarDisplay.Class.asp" ------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/StatePreferences.Class.asp" ----------------->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that we have already set up a state ID.  
'if session("StateId") = "" then
'	
'	Response.Write("State Reference Required.")
'	Response.End
'
'end if 

'Verify that the user has logged in.
CheckIsLoggedIn()

dim oPreference
set oPreference = new StatePreference
oPreference.ConnectionString = application("sDataSourceName")
oPreference.LoadPreferencesByStateId(session("StateId"))


'Are we changing the administration bar options?
'if trim(request("AdminOpt")) = "1" and not session("bAdminSidebar") then
'	session("bAdminSidebar") = true
'elseif trim(request("AdminOpt")) = "0" and session("bAdminSidebar") then
'	session("bAdminSidebar") = false
'end if 

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"


'Calendar Object initialization
dim oCalendarDisplay
set oCalendarDisplay = new CalendarDisplay
dim sEventType
dim iSearchStateId

sEventType = ScrubForSql(request("SearchEventType"))
iSearchStateId = ScrubForSql(request("SearchState"))

oCalendarDisplay.TemporaryRecordsetFile = application("sAbsWebroot") & "includes\TempRecordsetDefinition.rst"
oCalendarDisplay.StartOfBusinessDay = "7:00 AM"
oCalendarDisplay.EndOfBusinessDay = "9:00 PM"
oCalendarDisplay.HighlightReferenceDate = false
oCalendarDisplay.DateLink = application("sDynWebroot") & "calendar/calendar.asp?SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & "&View=day&refdate="
oCalendarDisplay.TimeLink = ""
oCalendarDisplay.ShowEmptyDaysInWeekView = true

'Get search criteria
dim dStartDate, dEndDate, dRefDate, sView
dim dRsStartDate, dRsEndDate, dTmpDate
dim bDisplayEventListing
dim sMode

'Show the monthly view if there is not specified view
sView = request("view")
if sView = "" then
	sView = "month"
end if

'Display the event listing if this is a monthly view
if sView = "month" then
	bDisplayEventListing = true
else
	bDisplayEventListing = false
end if

'If we do not have a specified reference date, use the current one
dRefDate = request("refDate")
if dRefDate = "" then
	dRefDate = Date
end if 

sMode = request("mode")

'Depending on our chosen view mode, set the start and end date accordingly
select case sView
	case "month"
		
		'Set startdate to the beginning of the month, then subract one month from that date
		dStartDate = month(dRefDate) & "/1/" & year(dRefDate)
		dStartDate = oCalendarDisplay.SubtractOneMonth(dStartDate)
		
		'Set end date to the last day of the month, then add one month to that date
		dEndDate = getLastDayOfMonth(Month(dRefDate), year(dRefDate))
		dEndDate = oCalendarDisplay.AddOneMonth(dEndDate)
		
	case else
	
		'Assign these the the refdate, which if unspecified is the current date
		dStartDate = dRefDate
		dEndDate = dRefDate
	
end select
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-events.gif" width="17" alt="Events" align="absmiddle" vspace="10"> Events Calendar</td>
				</tr>
<%
'===========================================
'New Caledar code

'Get events
sSql = "SELECT " & _
	   "DISTINCT E.EventId, " & _
	   "E.Event, " & _
	   "E.StartDate, " & _
	   "E.EndDate, " & _
	   "E.Active, " & _
	   "E.ImgPath, " & _
	   "E.Location, " & _
	   "E.Directions, " & _
	   "EET.EventId, " & _
	   "ET.EventType, " & _
	   "E.RedirectUrl " & _
	   "FROM afxEvents AS E " & _
	   "LEFT OUTER JOIN afxEventsEventTypeX EET ON (E.EventId = EET.EventId) " & _
	   "LEFT OUTER JOIN afxEventType ET ON (EET.EventTypeId = ET.EventTypeId) " & _
	   "LEFT OUTER JOIN States AS S ON (E.StateId = S.StateId) " & _
	   "WHERE E.EndDate >= '" & dStartDate & "' AND E.StartDate <= '" & dEndDate & " 23:59:59'" & _
	   "AND E.Active = 1 "

'Filter by search state if provided
if trim(iSearchStateId) <> "" then
	sSql = sSql & " AND S.StateId = '" & iSearchStateId & "'"
end if

'Filter according to specified mode
if sMode = "past" then
	sSql = sSql & " AND E.EndDate < '" & Date & "' " & _
				  "ORDER BY E.StartDate DESC "
else
	sSql = sSql & " AND E.EndDate >= '" & Date & "' " & _
				  "ORDER BY E.StartDate ASC "
end if


dim oConn
set oConn = new HandyAdo
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)


'Add the retrieved events to the calendar.
dim sEvent
dim sRedirectUrl

if not (oRs.BOF and oRs.EOF) then
	do while not oRs.EOF
		dRsStartDate = oRs("StartDate")
		dRsEndDate = oRs("EndDate")
		sRedirectUrl = oRs("RedirectUrl")
		if isdate(dRsStartDate) and isdate(dRsEndDate) then
			dTmpDate = dRsStartDate
			
			'Loop through the dates from start date to end date
			do while DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dRsEndDate, 2)) >= 0
				sEvent = "<a class=""newsTitle"" href=""calendardetail.asp?id=" & oRs("EventId") & "&SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & """>" & oRs("Event") & "</a>"
				oCalendarDisplay.AddEvent cdate(dTmpDate), sEvent, oRs("Directions")
				dTmpDate = DateAdd("D", 1, dTmpDate)
			loop
		end if 
		oRs.MoveNext
	loop
	oRs.MoveFirst
end if 
'oRs.Close

'===========================================

dim sCalendarRow

if sView = "month" then
%>
				<tr>
					<td>
						<table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" width="100%"><!-- Three calendars table -->
							<tr>
								<td align="center" colspan="2">
									<center>
									<table cellpadding="0" cellspacing="5" border="0">
										<tr>
											<td valign="top">
<%

	dim sPrevDate, sNextDate
	dim bDisplayPrevMonth, bDisplayNextMonth

	sPrevDate = oCalendarDisplay.SubtractOneMonth(dRefDate)
	sNextDate = oCalendarDisplay.AddOneMonth(dRefDate)
	if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
		bDisplayPrevMonth = false
	else
		bDisplayPrevMonth = true
	end if
	if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
		bDispalyNextMonth = false
	else
		bDisplayPrevMonth = true
	end if

	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
	oCalendarDisplay.HighlightReferenceDate = false

%>
		</td>
		<td valign="top">
<%
	oCalendarDisplay.PrintCalendar(oCalendarDisplay.SubtractOneMonth(dRefDate))
%>
		</td>
		<td valign="top">
<%		
	oCalendarDisplay.PrintCalendar(dRefDate)
%>
		</td>
		<td valign="top">
<%
	oCalendarDisplay.PrintCalendar(oCalendarDisplay.AddOneMonth(dRefDate))
%>
											</td>
										</tr>
									</table>
									</center>
									<p>
<%
if oRs.EOF and oRs.BOF then
%>
No Events Listed
<%
end if 
%>
						
									<ul class="bullet">
<%

do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""F6F4EE"""
	else
		sCalendarRow = ""
	end if

	'Response.Write("<tr>" & vbCrLf)
	'Response.Write("	<td valign=""top""" & sCalendarRow & "><a href=""event.asp?id=" & oRs("EventID") & """ class=""newstitle"">" & oRs("StartDate") & " - " & oRs("Event") & "</a></td>" & vbCrLf)
	'Response.Write("</tr>")		
	Response.Write("<li type=""circle""><b>" & FormatDateTime(oRs("StartDate"), 2))
	'if (oRs("EndDate") <> "") and (oRs("EndDate") <> oRs("StartDate")) then 
	'	Response.Write(" -<br> " & oRs("EndDate"))
	'end if
	Response.Write("</b> - <a href=""calendardetail.asp?id=" & oRs("EventId") & """>" & oRs("Event") & "</a></li>" & vbCrLf)

	oRs.MoveNext

loop


elseif sView = "day" then 

	oCalendarDisplay.Mode = "day"
	oCalendarDisplay.ShowAllTimeSlotsInDayView = false
	'oCalendarDisplay.DayViewTimeInterval = 60
	'oCalendarDisplay.StartOfBusinessDay = session("sStartOfBusinessDay")
	'oCalendarDisplay.EndOfBusinessDay = session("sEndOfBusinessDay")	
	oCalendarDisplay.PrintCalendar(dRefDate)

end if

%>
									</ul>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>


<%

set oCalendarDisplay = nothing


set oConn = nothing
set oRs = nothing
%>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next
		
		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate
	
		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")
		
			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if
	
			iMonth = cint(p_month) - 1
			iYear = cint(p_year)
		
			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if
	
		if err then
			sReturn = ""
		end if
		
		sTestDate = p_month&"/"&sReturn&"/"&p_year
		
		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if
		
		getLastDayofmonth = sReturn
	
	end function

%>


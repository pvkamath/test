<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/CalendarDisplay.Class.asp" ------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"


'Calendar Object initialization
dim oCalendarDisplay
set oCalendarDisplay = new CalendarDisplay
dim sEventType
dim iSearchStateId
dim sCurrMonth
dim sCurrYear
dim sCurrDate
dim bSetToFirst
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()

sEventType = ScrubForSql(request("SearchEventType"))
iSearchStateId = ScrubForSql(request("SearchState"))

oCalendarDisplay.TemporaryRecordsetFile = application("sAbsWebroot") & "includes\TempRecordsetDefinition.rst"
oCalendarDisplay.StartOfBusinessDay = "7:00 AM"
oCalendarDisplay.EndOfBusinessDay = "9:00 PM"
oCalendarDisplay.HighlightReferenceDate = false
oCalendarDisplay.DateLink = application("sDynWebroot") & "calendar/calendar.asp?SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & "&View=day&refdate="
if CheckIsAdmin() then
	oCalendarDisplay.EmptyDateLink = application("sDynWebroot") & "calendar/editevent.asp?editmode=Add&refdate="
else
	oCalendarDisplay.EmptyDateLink = "" 
end if


oCalendarDisplay.TimeLink = ""
oCalendarDisplay.ShowEmptyDaysInWeekView = true

'Get search criteria
dim dCalendarStartDateRange, dCalendarEndDateRange, dRefDate, sView
dim dEventStartDate, dEventEndDate, dTmpDate
dim bDisplayEventListing

'Show the monthly view if there is not specified view
sView = request("view")
if sView = "" then
	sView = "month"
end if

'Display the event listing if this is a monthly view
if sView = "month" then
	bDisplayEventListing = true
else
	bDisplayEventListing = false
end if

if GetFormElementAndScrub("Month") <> "" then
	dRefDate = GetFormElementAndScrub("Month")
	bSetToFirst = true
else
	dRefDate = GetFormElementAndScrub("refDate")

	if dRefDate = "" then
		dRefDate = Date
		bSetToFirst = true
	else
		bSetToFirst = false
	end if
end if

'Depending on our chosen view mode, set the start and end date accordingly
	select case sView
		case "month"
		
			'Set ref date to 1st of the 3 months
			if bSetToFirst then
				dCalendarStartDateRange = Month(dRefDate) & "/1/" & Year(dRefDate)					'make the rs start at the beginning of da month
				
				'make sure start date is not greater than 9 months from the current date
'				if cint (datediff("m",date(),dCalendarStartDateRange)) > 9 then
'					'Set to 9 months from current date
'					dCalendarStartDateRange = dateadd("m",9,Month(date()) & "/1/" & Year(date()))
'				end if
				
				'End Date is 2 months ahead
				dCalendarEndDateRange = dateadd("m",2,getLastDayOfMonth(Month(dRefDate), Year(dRefDate)))				
			else 'set ref date to the middle month
				dCalendarStartDateRange = Month(dRefDate) & "/1/" & Year(dRefDate)					'make the rs start at the beginning of da month			
				dCalendarStartDateRange = oCalendarDisplay.SubtractOneMonth(dCalendarStartDateRange)
			
				dCalendarEndDateRange = getLastDayOfMonth(Month(dRefDate), Year(dRefDate))
				dCalendarEndDateRange = oCalendarDisplay.AddOneMonth(dCalendarEndDateRange)
			end if
		case "week"
'			dEventStartDate = DateAdd("d", -(DatePart("W", dRefDate) - 1), dRefDate)	'first day of week
		case else
			dCalendarStartDateRange = dRefDate
			dCalendarEndDateRange = dRefDate
'			dEventStartDate = dRefDate
	end select
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
	
<table width=760 border=0 cellpadding=0 cellspacing=0><!-- Main Page Table -->
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
						
<%
'===========================================
'New Caledar code
sCurrMonth = Month(Date)
sCurrYear = Year(Date)
sCurrDate = Day(Date)

'Get events that are in the displayed calendar range, active, and have not ended (enddate greater than current date)
sSql = "SELECT E.EventID, E.Event, E.Directions, E.RedirectUrl, EDDX.DateID, EDDX.DisplayStartdate, EDDX.DisplayEnddate, E.EventGroup FROM afxEvents AS E " & _
	   "INNER JOIN afxEventDisplayDateX AS EDDX ON (EDDX.EventID = E.EventID) " & _
	   "LEFT OUTER JOIN afxEventsEventTypeX EET ON (E.EventId = EET.EventId) " & _
	   "LEFT OUTER JOIN afxEventType ET ON (EET.EventTypeId = ET.EventTypeId) " & _
	   "LEFT OUTER JOIN States AS S ON (E.StateId = S.StateId) " & _
	   "WHERE EDDX.DisplayEndDate >= '" & date & "' AND EDDX.DisplayEnddate >= '" & dCalendarStartDateRange & "' AND EDDX.DisplayStartdate <= '" & dCalendarEndDateRange & " 23:59:59' AND E.Active = 1 AND E.CompanyId = '" & session("UserCompanyId") & "' "

'Restrict the list of events to only the entities to which the user has access.
'Division Clause
sSql = sSql & "AND (CompanyL2Id is null"
if trim(sCompanyL2IdList) <> "" then
	sSql = sSql & " OR CompanyL2Id in (" & sCompanyL2IdList & ")"
end if
sSql = sSql & ") "

'Region Clause
sSql = sSql & "AND (CompanyL3Id is null"
if trim(sCompanyL3IdList) <> "" then
	sSql = sSql & " OR CompanyL3Id in (" & sCompanyL3IdList & ")"
end if
sSql = sSql & ") "

'Branch Clause
sSql = sSql & "AND (BranchId is null"
if trim(sBranchIdList) <> "" then
	sSql = sSql & " OR BranchID in (" & sBranchIdList & ")"
end if
sSql = sSql & ") "
   
'Filter by search state if provided
if trim(iSearchStateId) <> "" then
	sSql = sSql & " AND S.StateId = '" & iSearchStateId & "'"
end if

sSql = sSql & "ORDER BY EDDX.DisplayStartdate ASC "

dim oConn
set oConn = new HandyAdo
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

'Add the retrieved events to the calendar.
dim sEvent
dim sRedirectUrl

if not (oRs.BOF and oRs.EOF) then
	do while not oRs.EOF
		dEventStartDate = oRs("DisplayStartdate")
		dEventEndDate = oRs("DisplayEnddate")
		sRedirectUrl = oRs("RedirectUrl")
		
		if isdate(dEventStartDate) and isdate(dEventEndDate) then
			'set the interval date to the greater of the Event Start date or Calendar Range Start Date
			if DateDiff("D", FormatDateTime(dEventStartDate, 2), FormatDateTime(dCalendarStartDateRange, 2)) <= 0 then
				dTmpDate = dEventStartDate
			else
				dTmpDate = dCalendarStartDateRange 
			end if 
			
			'Loop through the dates from start date to end date within the displayed Calendar ramge
			do while DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dEventEndDate, 2)) >= 0 and DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dCalendarEndDateRange, 2)) >= 0 
				sEvent = "<a class=""newsTitle"" href=""calendardetail.asp?id=" & oRs("DateID") & "&SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & """>" & oRs("Event") & "</a>"
				oCalendarDisplay.AddEvent cdate(dTmpDate), sEvent, oRs("Directions")
				dTmpDate = DateAdd("D", 1, dTmpDate)
			loop
		end if 
		oRs.MoveNext
	loop
	oRs.MoveFirst
end if 
'oRs.Close

'===========================================

dim sCalendarRow

%>
				<tr>
					<td>
						<table bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" width="100%"><!-- Three calendars table -->
							<tr>
								<td align="center" colspan="2">
<%
if sView = "month" then
%>
									<center>
									<table cellpadding="0" cellspacing="5" border="0">
										<tr>
											<td valign="top">
<%

	dim sPrevDate, sNextDate
	dim bDisplayPrevMonth, bDisplayNextMonth

	sPrevDate = oCalendarDisplay.SubtractOneMonth(dRefDate)
	sNextDate = oCalendarDisplay.AddOneMonth(dRefDate)
	if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
		bDisplayPrevMonth = false
	else
		bDisplayPrevMonth = true
	end if
	if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
		bDispalyNextMonth = false
	else
		bDisplayPrevMonth = true
	end if

	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
%>
											</td>
											<td valign="top">
<%
    oCalendarDisplay.HighlightReferenceDate = false
	oCalendarDisplay.PrintCalendar(dCalendarStartDateRange)
%>
											</td>
											<td valign="top">
<%		
	oCalendarDisplay.HighlightReferenceDate = False
	oCalendarDisplay.PrintCalendar(dateadd("m",1,dCalendarStartDateRange))
%>
											</td>
											<td valign="top">
<%
	oCalendarDisplay.HighlightReferenceDate = False
	oCalendarDisplay.PrintCalendar(dateadd("m",2,dCalendarStartDateRange))
%>
											</td>
										</tr>
									</table>
									</center>
									<p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<p>
			
			
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td>&nbsp;</td>
					<td valign="bottom"><span class="bluetitle">Upcoming Events</span>
					<td align="right" valign="bottom"></td>
				</tr>
				<tr>
					<td colspan="3"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td></td>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">		

			
			
<%
if oRs.EOF and oRs.BOF then
%>
							<tr>
								<td colspan="2">No Events Listed</td>
							</tr>
<%
end if 

do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""F6F4EE"""
	else
		sCalendarRow = ""
	end if

%>
							<tr>
								<td align="left" width="135">
									<b>
										<% = FormatDateTime(oRs("DisplayStartdate"), 2) %>
<%
										if (oRs("DisplayEnddate") <> "") and (oRs("DisplayEnddate") <> oRs("DisplayStartdate")) then 
											if FormatDateTime(oRs("DisplayEnddate"), 2) = "1/1/2500" then
												response.write " to On-Going"
											else
												Response.Write(" - " & FormatDateTime(oRs("DisplayEnddate"), 2))
											end if
										end if
%>
									</b>
								</td>	
								<td align="left">&nbsp;&nbsp;<a href="/calendar/calendardetail.asp?id=<% = oRs("DateID") %>"><% = oRs("Event") %></a>
                                
                                <%if oRs("EventGroup") = true then %>
                                    (<a href="EventGroupList.asp?ParentID=<%=oRs("EventID")%>">View Child Events</a>)
                                <%end if %>
                                </td>
							</tr>
<%

	oRs.MoveNext

loop

elseif sView = "day" then 

	oCalendarDisplay.Mode = "day"
	oCalendarDisplay.ShowAllTimeSlotsInDayView = false
	'oCalendarDisplay.DayViewTimeInterval = 60
	'oCalendarDisplay.StartOfBusinessDay = session("sStartOfBusinessDay")
	'oCalendarDisplay.EndOfBusinessDay = session("sEndOfBusinessDay")	
	oCalendarDisplay.PrintCalendar(dRefDate)

end if

%>
						</table>
					</td>
				</tr>
			</table>
			<p><br><p>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>
<%

set oCalendarDisplay = nothing


set oConn = nothing
set oRs = nothing
%>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next
		
		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate
	
		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")
		
			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if
	
			iMonth = cint(p_month) - 1
			iYear = cint(p_year)
		
			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if
	
		if err then
			sReturn = ""
		end if
		
		sTestDate = p_month&"/"&sReturn&"/"&p_year
		
		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if
		
		getLastDayofmonth = sReturn
	
	end function

%>
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Events"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Make sure we were passed an event ID so we have something useful to do with this page.
dim iDateID
iDateID = ScrubForSql(request("id"))

dim oConn
dim sSql
dim oRs
dim sCalendarRow
dim sEventStartDate, sEventStartTime
dim sEventEndDate, sEventEndTime
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()

set oConn = new HandyADO

sSql = "SELECT Es.*, EDDX.DisplayStartdate, EDDX.DisplayEnddate " & _
	   "FROM afxEvents AS Es " & _
	   "INNER JOIN afxEventDisplayDateX AS EDDX ON (EDDX.EventID = Es.EventID) " & _
	   "WHERE EDDX.DateID = '" & iDateID & "' " & _
	   "AND Active = '1' " & _
	   "AND CompanyId = '" & session("UserCompanyId") & "' "
	   
'Restrict the list of events to only the entities to which the user has access.
'Division Clause
sSql = sSql & "AND (Es.CompanyL2Id is null"
if trim(sCompanyL2IdList) <> "" then
	sSql = sSql & " OR Es.CompanyL2Id in (" & sCompanyL2IdList & ")"
end if
sSql = sSql & ") "

'Region Clause
sSql = sSql & "AND (Es.CompanyL3Id is null"
if trim(sCompanyL3IdList) <> "" then
	sSql = sSql & " OR Es.CompanyL3Id in (" & sCompanyL3IdList & ")"
end if
sSql = sSql & ") "

'Branch Clause
sSql = sSql & "AND (Es.BranchId is null"
if trim(sBranchIdList) <> "" then
	sSql = sSql & " OR Es.BranchID in (" & sBranchIdList & ")"
end if
sSql = sSql & ") "

set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

if oRs.BOF and oRs.EOF then
	
	Response.Write("Error: A valid Event ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if 


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"	

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Event Detail -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = oRs("Event") %></td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

									<table cellpadding="0" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top">
									<table border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Event: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oRs("Event") %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Starts: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
<%
										   if oRs("DisplayStartdate") <> "" then
												sEventStartDate = oRs("DisplayStartdate") 'pull date from the afxEventDisplayDateX table (used for recurrence events and standard events)
												sEventStartTime = FormatDateTime(oRs("Startdate"), 3) 'Must pull from StartDate in the afxEvents table for the time
		
												if trim(sEventStartTime) = "12:00:00 AM" then
													sEventStartTime = ""
												end if 
				
												response.write(formatdatetime(sEventStartDate, 2) & " " & sEventStartTime)
											end if
%>
											</td>  
										</tr>
										<tr>
											<td class="newstitle" nowrap>Ends: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
<%
										   if oRs("DisplayEnddate") <> "" then
												sEventEndDate = oRs("DisplayEnddate") 'pull date from the afxEventDisplayDateX table (used for recurrence events and standard events)
												
												if FormatDateTime(oRs("DisplayEnddate"), 2) = "1/1/2500" then
													response.write("On-Going")
												else
													sEventEndTime = FormatDateTime(oRs("Enddate"), 3) 'Must pull from EndDate in the afxEvents table for the time
		
													if trim(sEventEndTime) = "12:00:00 AM" then
														sEventEndTime = ""
													end if 
				
													response.write(formatdatetime(sEventEndDate, 2) & " " & sEventEndTime)
												end if
											end if
%>													
											</td>
										</tr>										
										<tr>
											<td class="newstitle" nowrap>City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oRs("City") %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = GetStateName(oRs("StateId"), false) %></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>Description: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<% = oRs("Directions") %>
											
											<% if oRs("ImgPath") <> "" then %>
											<p>
											<a href="<%=Application("sDynWebroot")%>usermedia/images/calendarFX/<%=oRs("ImgPath")%>" target="_blank">Click here to view additional information</a>
											<% end if %>											
											</td>											
										</tr>
									</table>
											<p align="center"><a href="javascript:history.back()"><img src="<%= application("sDynMediaPath") %>bttnBack.gif" border=0></a>
										</td>
										</tr>
									</table>
								
<%

set oRs = nothing
set oConn = nothing
set oPreference = nothing

%>	
		
					</td>
				</tr>
			</table>						
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Calendar Admin"
	
	
	
	

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "EVENTS"	
	

	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>

<%
strSQLConnectionString = application("sDataSourceName")
Dim objConn, rs, sSQL, iSectionID, iMonthID, dDate, iYear, sLocation, sImgPath
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()


sNewSearch = request("NewSearch")
iSectionID = Request("SectionID")
sMonthYear = Request("MonthYear")
dDate = Request("dt")
iYear = Request("yr")
sSearchCity = Request("SearchCity")
iSearchStateID = Request("State")
iNumEvent = Request("numEvent")
sLocation = Request("location")
sEventType = Request("EventType")
sMode = Request("mode") 'list coming active events or history events

if iSectionID = "" then
	iSectionID = 0
else
	iSectionID = cint(iSectionID)
end if

if dDate = "" then
	dDate = 0
else
	dDate = cint(dDate)
end if

'set up session vars
if (trim(sMode) = "") then
	if (trim(Session("Mode")) <> "")  then
		sMode = Session("Mode")
	end if
else
	Session("Mode") = sMode
end if

if (trim(sEventType) = "") then
	if (trim(Session("EventType")) <> "") and (trim(sNewSearch) = "") then
		sEventType = Session("EventType")
	else 
		Session.Contents.Remove("EventType") 
	end if
else
	Session("EventType") = sEventType
end if
			
if (trim(sMonthYear) = "") then
	if (trim(Session("MonthYear")) <> "") and (trim(sNewSearch) = "") then
		sMonthYear = Session("MonthYear")
	else 
		Session.Contents.Remove("MonthYear") 
	end if
else
	Session("MonthYear") = sMonthYear
end if

if (trim(sSearchCity) = "") then
	if (trim(Session("SearchCity")) <> "") and (trim(sNewSearch) = "") then
		sSearchCity = Session("SearchCity")
	else 
		Session.Contents.Remove("SearchCity") 
	end if
else
	Session("SearchCity") = sSearchCity
end if

if (trim(iSearchStateID) = "") then
	if (trim(Session("SearchStateID")) <> "") and (trim(sNewSearch) = "") then
		iSearchStateID = Session("SearchStateID")
	else 
		Session.Contents.Remove("SearchStateID") 
	end if
else
	Session("SearchStateID") = iSearchStateID
end if

if (trim(iNumEvent) = "") then
	if (trim(Session("NumEvent")) <> "") and (trim(sNewSearch) = "") then
		iNumEvent = Session("NumEvent")
	else 
		Session.Contents.Remove("NumEvent") 
	end if
else
	Session("NumEvent") = iNumEvent
end if				
%>

<script language="javascript" src="/admin/CalendarFX/includes/FormCheck2.js"></script>
<script language="JavaScript">
function OpenLiveView() {
	CalendarWindow = window.open('/calendar.asp','CalendarWindow');
	CalendarWindow.location.reload();
}

function ValidateForm(frm2) {
	if (!delarray[i].checked) {
		alert("You have not chosen an event to delete.");
		return false;
		}
	else
		if (window.confirm("Are you sure you want to permanantly delete the selected events?"))
			return true;
		else
			return false;
}

function ClearCheckbox(frm2) {
	for (i = 0; i < frm2.elements.length; i++) {
         frm2.elements[i].checked = false;
	}
   return true;
}

</script> 

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%"><!-- Calendar box -->
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<link rel="stylesheet" href="includes/style.css">
<!-- title line -->
<table cellspacing="0" cellpadding="0" border="0">
       	<tr>
        	<td valign="top"><span class="activeTitle">Events List <%'PrintNameOfSection(1)%></span>
			<p>Below is a list of all events in chronological order.  To edit an event click on <b>Edit</b> (red text).<br>
			To view all details regarding an event click on the <b>event name</b>.  To delete an event,<br>
			or events, select the corresponding boxes in the delete column to the event(s) you wish to delete<br>
			and then scroll to the bottom of the page and click on <b>Delete</b>.  You will be given the opportunity<br>
			to review the events you have chosen to delete.  If you decide you would not like to delete the <br>
			chosen events click on <b>Cancel</b>.  Click on <b>Delete</b> if you wish to delete the chosen events.<br>
			Click on link under <b>File</b> to add or change File. <br>
			Click on link under <b>Active</b> to active or disable this event.</p>
			<p><b>Please do not use punctuation in the naming of events when adding an event.</b><br>
			For example, do not use apostrophes or "&" in place of the word "and".</p>
			<img src="/admin/calendarfx/media/images/spacer.gif" width="20" height="15"></td>
	</tr>
</table>
<!-- content  -->
	<%' if session("access_level") >= 10 then %>	
<table cellpadding="4" cellspacing="0" border="0">
		<tr>
			<FORM action="list.asp" method="post" name="frm" id="frm">
			<input type="hidden" name="NewSearch" value="YES">				
			<td valign="bottom">
				<input type="Hidden" name="mode" value="<%=sMode%>">
				<table>
					<tr>
						<td>
							View By Type:&nbsp;
						</td>
						<td>
				<%				
				Set objConn = Server.CreateObject("ADODB.Connection")
				objConn.Open strSQLConnectionString
				
				Set rs = Server.CreateObject("ADODB.Recordset")
				
				sSQL = "Select Distinct EventType From afxEventType Order by EventType"
		
				rs.Open sSQL, objConn, 3, 1
		
				If Not rs.EOF Then
				%>
					<Select name="EventType">
						<Option value="">All Event Types</Option>
					<%
					Do While Not rs.EOF
					If rs("EventType") <> "" Then 
					%>
						
						<Option value="<%=rs("EventType")%>"<%
							If rs("EventType") = sEventType Then response.write(" selected")
							%>><%=rs("EventType")%></Option>
					<%
					End  If
						rs.MoveNext
					Loop
					%>
					</Select>
				<%
				Else
					response.write("<B>No event type is found</B>")
				End If
				rs.Close
				%>			
						</td>
					</tr>
					<tr>
						<td>
							View By Month:&nbsp;
						</td>
						<td>
					<select name="MonthYear" id="MonthYear">
						<option value="">All Months</option>
						<%										
						For i = 0 To 35
							'If on the past events page, decrement value instead of increment.
							if lcase(sMode) = "history" then 					
								sNewDate = DateAdd("M", i*-1, Date)
							else
								sNewDate = DateAdd("M", i, Date)
							end if
							
							sMonth = Month(sNewDate)
							sYear = Year(sNewDate)
							
							response.write "<option value=""" & sMonth & " " & sYear & """"
							If sMonth & " " & sYear = sMonthYear Then response.write " selected"
							response.write ">" & MonthName(sMonth) & " " & sYear & "</option>" & vbcrlf
						next
						%>
					</select>
						</td>
					</tr>
					<tr>
						<td>
							View By City:&nbsp;						
						</td>
						<td>
							<input type="text" name="SearchCity" value="<%=sSearchCity%>">
						</td>
					</tr>
					<tr>
						<td>
							View By State:&nbsp;					
						</td>
						<td>
							<% call DisplayStatesDropDown(iSearchStateID,0,"State") %>	
						</td>
					</tr>
					<tr>
						<td>
							Events per page:&nbsp;
						</td>
						<td>
					<Select name="numEvent" id="numEvent">
						<%
						ii = 15
						Do While ii <= 30
						%>
							<Option value="<%=ii%>" <%If CInt(iNumEvent) = CInt(ii) Then response.write(" selected")%>><%=ii%></Option>
						<%
							ii = ii + 5
						Loop
						%>
					</Select>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Search">
			</td>
		</tr>		
		</FORM>		
		<tr>
			<td colspan="2">
				<table cellpadding="4" cellspacing="0" border="0">
					<FORM action="DelEventConfirm.asp" method="Post" name="frm2" id="frm2">
					<%
					Dim rsEvent, iEventID, sEvent, intPage, intPageCount
					
					CurrMonth = Month(Date)
					CurrYear = Year(Date)
					CurrDate = Day(Date)
					
					Set rsEvent = Server.CreateObject("ADODB.Recordset")
					
					'sSQL = "SELECT DISTINCT EventID, Event, StartDate, EndDate, Location, ImgPath FROM afxEvents WHERE 1=1 "
					
					sSQL = "SELECT DISTINCT E.EventID, E.Event, E.StartDate, E.EndDate, E.City, E.ImgPath, E.Location, E.Active, ET.EventType, S.State " & _
							"FROM afxEvents as E " & _
							"LEFT OUTER JOIN afxEventsEventTypeX as EET on (E.EventID = EET.EventID) " & _
							"LEFT OUTER JOIN afxEventType as ET on (EET. EventTypeID = ET.EventTypeID) " & _
							"LEFT OUTER JOIN States as S on (E.StateID = S.StateID) " 
							
					If sMode = "coming" Then
						sActivitySQL = " WHERE ( (datepart(mm, E.EndDate) = " & CurrMonth & "  AND datepart(yyyy, E.EndDate) = " & CurrYear & "  AND datepart(dd, E.EndDate) >= " & CurrDate & "  ) OR (datepart(mm, E.EndDate) > " & CurrMonth & "   AND datepart(yyyy, E.EndDate) = " & CurrYear & "  ) OR datepart(yyyy, E.EndDate) > " & CurrYear & "  )"
					Else 
						sActivitySQL = " WHERE ( (datepart(mm, E.EndDate) = " & CurrMonth & "  AND datepart(yyyy, E.EndDate) = " & CurrYear & "  AND datepart(dd, E.EndDate) < " & CurrDate & "  ) OR (datepart(mm, E.EndDate) < " & CurrMonth & "   AND datepart(yyyy, E.EndDate) = " & CurrYear & "  ) OR datepart(yyyy, E.EndDate) < " & CurrYear & "  )"
					End If
					
					If sMonthYear = "" Then						
						sSQL = sSQL & sActivitySQL '& " AND E.active = 1"
					Else
						sSQL = sSQL & sActivitySQL & " AND datepart(mm, E.StartDate) = '" & Month(sMonthYear) & "'" & _
								" AND datepart(yyyy, E.StartDate) = '" & Year(sMonthYear) & "' "'AND E.active = 1"
					End If

					If sEventType <> "" Then											
						sSQL = sSQL & " AND ET.EventType = '" & sEventType & "'"
					End If
					
					'if true, filter by city
					if trim(sSearchCity) <> "" then
						sSQL = sSQL & " AND E.City like '%" & sSearchCity & "%'"
					end if
					
					'if true, filter by state
					If trim(iSearchStateID) <> "" Then											
						sSQL = sSQL & " AND S.StateID = '" & iSearchStateID & "'"
					End If
					
					sSql = sSql & "AND E.CompanyId = '" & session("UserCompanyId") & "' "

					'Restrict the list of events to only the entities to which the user has access.
					'Division Clause
					sSQL = sSQL & "AND (CompanyL2Id is null"
					if trim(sCompanyL2IdList) <> "" then
						sSQL = sSQL & " OR CompanyL2Id in (" & sCompanyL2IdList & ")"
					end if
					sSQL = sSQL & ") "

					'Region Clause
					sSQL = sSQL & "AND (CompanyL3Id is null"
					if trim(sCompanyL3IdList) <> "" then
						sSQL = sSQL & " OR CompanyL3Id in (" & sCompanyL3IdList & ")"
					end if
					sSQL = sSQL & ") "

					'Branch Clause
					sSQL = sSQL & "AND (BranchId is null"
					if trim(sBranchIdList) <> "" then
						sSQL = sSQL & " OR BranchID in (" & sBranchIdList & ")"
					end if
					sSQL = sSQL & ") "
					
					sSQL = sSQL & " ORDER BY E.StartDate ASC"
					rsEvent.Open sSQL, objConn, 3, 1

					If Not rsEvent.EOF Then
						
						'Get page number of which records are showing
						intPage = Request("page_number")
						
						If intPage = "" Then intPage = 1
						
						If iNumEvent = "" Then iNumEvent = 15
						
						'Set the number of records displayed on a page
						rsEvent.PageSize = iNumEvent
						rsEvent.Cachesize = rsEvent.PageSize
						intPageCount = rsEvent.PageCount
						
						'determine which search page the user has requested.
		     			If CInt(intPage) > CInt(intPageCount) Then intPage = intPageCount
						
		      			If CInt(intPage) <= 0 Then intPage = 1
						
						'Set the beginning record to be display on the page
		        		rsEvent.AbsolutePage = intPage
						
						i = 0
						Do While i < rsEvent.pagesize
							sDateArray = Split(rsEvent("StartDate"), " ")							
							sEventDate = sDateArray(0)
							sEventDay = Day(rsEvent("StartDate"))
							DateComp = DateValue(rsEvent("StartDate"))				

						%>
						<tr>
							<td class="week" height="20" colspan="100%">
								<%If (iMonthID <> "") Then
									sEventDate = MonthSelected & "/" & sEventDay & "/" & iYear
								End if%>								
								<%=FormatDateTime(sEventDate, 1)%>								
							</td>
						</tr>
						<tr>
							<td class="wk_title" height="20" width="40">Edit</td>
                            <td class="wk_title" height="20" width="40">Copy</td>
							<td class="wk_title" height="20" width="60">Delete</td>
							<td class="wk_title" height="20" width="75">Time</td>
							<td class="wk_title" height="20">View Event</td>
							<td class="wk_title" height="20">End Date</td>
							<td class="wk_title" height="20">Event Type</td>
							<td class="wk_title" height="20">Location</td>
							<td class="wk_title" height="20">File</td>
							<td class="wk_title" height="20">Active</td>
						</tr>
						<%
							
							Do Until (DateComp <> DateValue(rsEvent("StartDate")) or (i >= rsEvent.pagesize))
								
								iEventID = rsEvent("EventID")
								sEvent = rsEvent("Event")
								sEndDateArray = Split(rsEvent("EndDate"), " ")							
								sEventEndDate = sEndDateArray(0)
								bActive = rsEvent("active")															
								sCity = rsEvent("City")
								sState = rsEvent("State")
								
								if (trim(sCity) <> "" or trim(sState) <> "") then
									if (trim(sCity) <> "" and trim(sState) <> "") then
										sLocation = rsEvent("City") & ", " & rsEvent("state") 
									elseif (trim(sCity) <> "") then
										sLocation = rsEvent("City")
									else
										sLocation = rsEvent("state")
									end if
								else
									sLocation = ""
								end if

								sEventType = rsEvent("EventType")
								sDateArray = Split(rsEvent("StartDate"), " ")
								sImgPath = rsEvent("ImgPath")
								
								'Must set a check for when the time is equal to 12AM,
								'because the Database does not store the time value for 12AM
								if (UBound(sDateArray) > 0) then
									sEventTime = sDateArray(1)
									sTime = Split(sEventTime, ":")
									sTimeStart = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
								else
									'sTimeStart = "12:00"
									sTimeStart = ""
								end if
								
								'Pull the 1st DateID for the event for the Event preview
								set oDateRS = server.createobject("ADODB.RecordSet")
								
								sSQL = "SELECT TOP 1 DateID FROM afxEventDisplayDateX WHERE EventID = " & rsEvent("EventID") & " Order By DateId"
								oDateRS.cursorlocation = 3
								oDateRS.CursorType = 3
							    oDateRS.Open sSQL, objConn
							    oDateRS.ActiveConnection = nothing
								
								if not oDateRS.EOF then
									iDateID = oDateRS("DateID")
								end if
								
								set oDateRS = nothing
						%>
						<tr>
							<td class="desc"><a href="EditEvent.asp?eventType=<%=sEventType%>&mode=<%=sMode%>&eventid=<%=iEventID%>">Edit</a></td>
                            <td class="desc"><a href="EditEvent.asp?eventType=<%=SEventType%>&eventid=<%=iEventID%>&editmode=Copy">Copy</a></td>
							<td class="desc"><input type="checkbox" name="delevent" value="<%=rsEvent("EventID")%>"</td>
							<td class="desc"><%=sTimeStart%></td>
							<td class="desc"><a href="calendardetail.asp?id=<%=iDateID%>"><%=sEvent%></a></td>							
							<td class="desc">
							<% 
								if sEventEndDate = "1/1/2020" then
									response.write("On-Going")
								else
									response.write(sEventEndDate)
								end if
							%>
							</td>
							<td class="desc"><%=sEventType%></td>
							<td class="desc"><%=sLocation%></td>							
							<% 
								dim sPath
								If sImgPath <> "" Then 
									sPath = "<a href='AddPicture.asp?eventid=" & rsEvent("EventID") & "'>Change</a>"
								Else 
									sPath = "<a href='AddPicture.asp?eventid=" & rsEvent("EventID") & "'>Add</a>"
								End If %>
							<td class="desc"><%=sPath%></td>
							<% if bActive = 1 Then sActive = true
								if bActive = 0 Then sActive = false	%>
							<td class="desc"><%=sActive%></td>
						</tr>
						<tr>
							<td colspan="6"><img src="/admin/calendarfx/media/images/spacer.gif" width="20" height="10"></td>
						</tr>		
					<%
						
								i = i + 1
								rsEvent.MoveNext
							
								If rsEvent.EOF Then Exit Do
														
							Loop
		
							If rsEvent.eof Then Exit Do
						Loop
						
						rsEvent.Close
						Set rsEvent = Nothing
						
						Set rs = Nothing
					%>
					<tr>
						<td colspan="6">
							<%
								response.write("Page " & intPage & " of " & intPageCount & "<br>")
				
								If CInt(intPage) > 1 Then
								response.write("<a href=""list.asp?EventType=" & sEventType & "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & iMonthID & "&date=" & dDate & "&year=" & iYear & "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) - 1 & """>Previous Page</a>")
								End If
				
								If CInt(intPage) < CInt(intPageCount) Then
									If CInt(intPage) = 1 Then 
										response.write("<a href=""list.asp?EventType=" & sEventType & "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & iMonthID & "&date=" & dDate & "&year=" & iYear & "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) + 1 & """>Next Page</a>")
									Else
										response.write("&nbsp;|&nbsp;")
										response.write("<a href=""list.asp?EventType=" & sEventType &  "&MonthYear=" & sMonthYear & "&SectionID=" & iSectionID & "&monthid=" & monthid & "&date=" & dt & "&year=" & yr &  "&SearchCity=" & sSearchCity & "&State=" & iSearchStateID & "&numevent=" & iNumEvent & "&page_number=" & cint(intPage) + 1 & """>Next Page</a>")
									End If
								End If
							%>
						</td>
					</tr>
					<%Else%>
					<tr>
							<td class="week" height="20" colspan="6">
								No Event Found.						
							</td>
						</tr>
					<%End If%>
			  		<tr>
						<td colspan="6">&nbsp;<br>
							<input type="image" src="/admin/calendarfx/media/images/btn_delete.gif" border="0" name="delete" value="Submit">
							&nbsp;&nbsp;
							<a href="#" onClick="javascript:ClearCheckbox(document.frm2)">
							<img src="/admin/calendarfx/media/images/btn_clear.gif" border="0" name="clear" value="clear"></a>
						</td>
					</tr>
					</FORM>
				</table>			
			<%' end if %>
		</td>
	</tr>
</table>







									</ul>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>







<%
	' -----------------------------------------------------------------------------------------------
	function DaysInMonth(p_nMonth, p_nYear)
		dim nRetVal
		
		' Convert Month to Integer:
		if p_nMonth <> "" then 
			p_nMonth = cint(p_nMonth)
		else
			nRetVal = 0
		end if
		
		' Conver Year to Integer:
		if p_nYear <> "" then p_nYear = cint(p_nYear)

		' Go Ahead if Month isn't blank:
		if p_nMonth <> "" then
			' Months with 30 days:
			if p_nMonth = 4 or p_nMonth = 6 or p_nMonth = 9 or p_nMonth = 11 then
				nRetVal = 30
			
			' February and Leap Year:
			elseif p_nMonth = 2 then
				if p_nYear = "" then
					nRetVal = 28
				else
					' Check for Leap Year:
					if p_nYear MOD 4 = 0 then
						nRetVal = 29
					else
						nRetVal = 28
					end if
				end if

			' All other months:
			else
				nRetVal = 31
			end if
		end if

		' Return the number of days:
		DaysInMonth = nRetVal
	end function

	' -----------------------------------------------------------------------------------------------
' BuildDate
' Will build an AND clause onto the given fieldname:
function BuildDate(p_nMonth, p_nDay, p_nYear, p_sFieldName)

	dim nMonth, nDay, nYear
	dim sRetVal
	sRetVal = ""
	
	if p_nMonth <> "" then 
		p_nMonth = cint(p_nMonth)
		if p_nMonth = 0 then p_nMonth = ""
	end if

	if p_nDay <> "" then 
		p_nDay = cint(p_nDay)
		if p_nDay = 0 then p_nDay = ""
	end if

	if p_nYear <> "" then
		p_nYear = cint(p_nYear)
		if p_nYear = 0 then p_nYear = ""
	end if

	if p_nMonth = "" then	

		' If day is blank, then do nothing

		' If the year is blank:
		if p_nYear <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '1/1/" & p_nYear & " 00:00:00'" _
					& " AND " & p_sFieldName & " <= '12/31/" & p_nYear & " 23:59:59' "
		end if
	
	' If the Day is blank:
	elseif p_nDay = "" then
		
		' If the Year is selected and the Month is not, then get everything in the current year
			' Already took care of this.

		' If the Month is selected and the Year is not, then get everything in the current month in the current year
		if p_nMonth <> "" and p_nYear = "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/1/" & Year(Now) & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & DaysInMonth(p_nMonth, Year(Now)) & "/" & Year(Now) & " 23:59:59'"

		' If both the Year and the Month Are selected, then get everything in the selected month in the selected year
		elseif p_nYear <> "" and p_nMonth <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/1/" & p_nYear & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & DaysInMonth(p_nMonth, p_nYear) & "/" & p_nYear & " 23:59:59'"
		end if
		

	' If the Year is blank:
	elseif p_nYear = "" then
		
		' Month is selected and Day is not, then get everything in the current Month in the current year
			' Already took care of this
			
		' Day is selected but month is not, then do nothing

		' If both the Month and the day are selected, then get everything in the selected month and selected day of the current year
		if p_nMonth <> "" and p_nDay <> "" then
			sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/" & p_nDay & "/" & Year(Now) & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & p_nDay & "/" & Year(Now) & " 23:59:59'"
		end if
	
	' If All Fields are Not blank, get the specific date:
	else
		sRetVal = " AND " & p_sFieldName & " >= '" & p_nMonth & "/" & p_nDay & "/" & p_nYear & " 00:00:00' " _
					& " AND " & p_sFieldName & " <= '" & p_nMonth & "/" & p_nDay & "/" & p_nYear & " 23:59:59'"
	end if

	BuildDate = sRetVal

end function

%>
	

<%	
set objConn = nothing
set rs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

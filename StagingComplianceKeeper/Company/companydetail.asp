<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Make sure we were passed a candidate ID so we have something useful to do with this page.
dim oCompany
dim iCompanyId
iCompanyId = session("UserCompanyId")


set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(iCompanyId) = 0 then

	Response.Write("Error: A valid Company ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<% if CheckIsAdmin() then %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmOfficerDelete(p_iCandidateId,p_sCandidateName)
{
	if (confirm("Are you sure you wish to delete the following Officer:\n  " + p_sCandidateName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficer.asp?id=' + p_iCandidateId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletenote.asp?id=' + p_iNoteId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}

function OpenMoveLicWindow(p_iLicenseId,p_sTextbox) {
	window.open("/officers/MoveLicWin.asp?lid=" + p_iLicenseId + "&textbox=" + p_sTextbox , "ImageWindow", "width=500,height=300,scrollbars=1")
}	

</script>
<% end if %>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">

			<!-- Company Details -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Profile</td>
				</tr>
					
						<% if CheckIsAdmin() then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td align="left" valign="center"><a href="modcompany.asp" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyName") %> Information" alt="Edit <% = session("CompanyName") %> Information"> &nbsp;Edit <% = session("CompanyName") %> Information</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
				<tr>
					<td class="bckRight">
					
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Legal Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.LegalName %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Trade Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Name %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State of Incorp: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.LookupState(oCompany.IncorpStateId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Date of Incorp: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.IncorpDate %></td>
										</tr>
										<% if 1 = 2 then %>
										<tr>
											<td class="newstitle" nowrap>Fiscal Year End: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.FiscalYearEndMonth %> <% = oCompany.FiscalYearEndDay %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Phone %> <% if oCompany.PhoneExt <> "" then %> Ext. <% = oCompany.PhoneExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Phone2 %> <% if oCompany.PhoneExt2 <> "" then %> Ext. <% = oCompany.PhoneExt2 %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Phone3 %> <% if oCompany.PhoneExt3 <> "" then %> Ext. <% = oCompany.PhoneExt3 %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Fax %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Address %></td>
										</tr>
										<% if not oCompany.Address2 = "" then %>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Address2 %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.City %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.LookupState(oCompany.StateId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Zipcode %><% if oCompany.ZipcodeExt <> "" then %> - <% oCompany.ZipcodeExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Email %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Website: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><a href="http://<% = oCompany.Website %>"><% = oCompany.Website %></td>
										</tr>
										
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
									</table>
											</td>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0" valign="top">
										<tr>
											<td class="newstitle" nowrap>Organization Type: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.LookupOrgType(oCompany.OrgTypeId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>EIN: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.Ein %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Main Contact: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.MainContactName %><br>
											Phone: <% = oCompany.MainContactPhone %><% if oCompany.MainContactPhoneExt <> "" then %> Ext. <% = oCompany.MainContactPhoneExt %><% end if %><br>
											Fax: <% = oCompany.MainContactFax %><br>
											<% = oCompany.MainContactEmail %><br>
											</td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Mailing Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oCompany.MailingAddress %><br>
											<% if not oCompany.MailingAddress2 = "" then %><% = oCompany.MailingAddress2 %><br><% end if %>
											<% = oCompany.MailingCity %><br>
											<% = oCompany.LookupState(oCompany.MailingStateId) %><br>
											<% = oCompany.MailingZipcode %>
											</td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										if CheckIsAdmin() then
										
											dim oAssociateDoc
											set oAssociateDoc = new AssociateDoc
											oAssociateDoc.ConnectionString = application("sDataSourceName")
											oAssociateDoc.VocalErrors = application("bVocalErrors")
											oAssociateDoc.CompanyId = session("UserCompanyId")
											oAssociateDoc.OwnerId = oCompany.CompanyId
											oAssociateDoc.OwnerTypeId = 3
											set oRs = oAssociateDoc.SearchDocs
										
											'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<% if CheckIsAdmin() then %>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if CheckIsAdmin() then %>
											<a href="/officers/moddocument.asp?otid=3&oid=<% = oCompany.CompanyId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											<% end if %>
											</td>
										</tr>
										<% end if %>
									</table>
										</td>
										</tr>
									</table>
					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
<% if CheckIsAdmin() then %>
						
			<!-- Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = session("CompanyName") %> Licenses</td>
				</tr>

						<% if CheckIsAdmin() then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modlicense.asp?cid=<% = oCompany.CompanyId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
				<tr>
					<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>
<%
dim oLicense
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oCompany.CompanyId
oLicense.OwnerTypeId = 3


set oRs = oLicense.SearchLicensesByState()

if not (oRs.EOF and oRs.BOF) then
	do while not oRs.EOF
	
		Response.Write("<b>(" & oRs("LicenseCount") & ") <a href=""companydetail.asp?ls=" & oRs("LicenseStateId") & """>" & oLicense.LookupState(oRs("LicenseStateId")))
		if oRs("LicenseCount") > 1 then 
			Response.Write(" Licenses ")
		else
			Response.Write(" License ")
		end if
		Response.Write("</a></b><br>")
		
		'Get the full list if we've selected this state.
		if request("ls") = cstr(oRs("LicenseStateId")) then
				
			oLicense.LicenseStateId = oRs("LicenseStateId")
			set oLicRs = oLicense.SearchLicenses()
			
			%>
			<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
					<% if CheckIsAdmin() then %>
					<td></td>
					<td></td>
					<td></td>
					<% end if %>
					<td><b>License Number</b></td>
					<td><b>License Type</b></td>
					<td><b>License Status</b></td>
					<td><b>Expiration Date</b></td>
                    <td><b>Filing Schedule</b></td>
				</tr>
			<%
				do while not oLicRs.EOF

					if oLicense.LoadLicenseById(oLicRs("LicenseId")) <> 0 then

						if sColor = "" then
							sColor = " bgcolor=""#ffffff"""
						else
							sColor = ""
						end if


						Response.Write("<tr>" & vbCrLf)
						if CheckIsAdmin() and oLicense.OwnerId = oCompany.CompanyId then
							Response.Write("<td " & sColor & "><a href=""#"" onClick=""javascript:OpenMoveLicWindow('" & oLicense.LicenseId & "','');""><img src=""" & application("sDynMediaPath") & "bttnMove.gif"" border=""0"" title=""Move This License"" alt=""Move This License""></a></td>" & vbCrLf)
							Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & "&ls=" & oRs("LicenseStateId") & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
							Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
						end if 
						Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/licensedetail.asp?lid=" & oLicense.Licenseid & """>" & oLicense.LicenseNum & "</a></td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseType & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseExpDate)
						if oLicense.LicenseExpDate < date() + 120 and oLicense.LicenseExpDate > date() then
							Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicense.LicenseExpDate - date(), 0) & " Days</font>)</b>")
						end if
						Response.Write("</td>" & vbCrLf)
                        Response.Write("<td " & sColor & ">")
                        Response.Write("<div align=""center"">")
                        Response.Write("<a href=""" & application("URL") & "/Calendar/EventGroupList.asp?LicenseID=" & oLicense.LicenseID & """>")
                        Response.Write("<img src=""" & application("URL") & "/admin/calendarfx/Media/Images/calicon.gif"" border=0>")
                        Response.Write("</a></div></td>")
						Response.Write("</tr>" & vbCrLf)
							
					end if 

					oLicRs.MoveNext

				loop

			%>
			</table>
			<%
			
		end if

		oRs.MoveNext
	
	loop

else
	
	Response.Write("No Licenses found.")
	
end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						
			<!-- Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = session("CompanyName") %> Notes</td>
				</tr>

						<% if CheckIsAdmin() then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modnote.asp?cid=<% = oCompany.CompanyId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oCompany.CompanyId
oNote.OwnerTypeId = 3

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="400"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if CheckIsAdmin() and (oNote.UserId = session("User_Id")) then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			elseif CheckIsAdmin() then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & month(oNote.NoteDate) & "/" & day(oNote.NoteDate) & "/" & year(oNote.NoteDate) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
%>


						</table>
					</td>
				</tr>
			</table>
<% end if %>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

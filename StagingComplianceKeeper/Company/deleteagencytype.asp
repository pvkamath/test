﻿<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/AgencyType.Class.asp" ------------------------------------------------------>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Agency Type Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
      // Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
      function InitializePage() {
      }
      // Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Courses" align="absmiddle" vspace="10"> Agency Types</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Delete Agency Type</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>

<%

dim iAgencyTypeId
iAgencyTypeId = ScrubForSQL(request("AgencyTypeID"))

dim oAgencyType
set oAgencyType = new AgencyType
oAgencyType.ConnectionString = application("sDataSourceName")
oAgencyType.TpConnectionString = application("sTpDataSourceName")
oAgencyType.VocalErrors = application("bVocalErrors")

'Check for a valid ID.
if iAgencyTypeId <> "" then

	'We need to have an ID loaded before we can delete it.
	if oAgencyType.LoadAgencyTypeById(iAgencyTypeId) = 0 then
	
		'The load was unsuccessful.  Stop.
		HandleError("<b>Failed to load the passed AgencyType ID: " & iAgencyTypeId & "</b>")
		Response.End
		
	elseif (oAgencyType.CompanyId <> application("UserCompanyId")) then
	
		'Make sure the user has access to this company.
		HandleError("You do not have access to the passed Course ID: " & iAgencyTypeId)
		response.end
		
	end if

	oAgencyType.Deleted = 1
	if oAgencyType.SaveAgencyType = 0 then
		
		'The delete failed.  
		HandleError("<b>Failed to delete the Agency Type.</b>")
		Response.End
		
	end if

end if

%>

<b>Successfully deleted the Agency Type "<% = oAgencyType.Name %>".</b>

<%
set oAgencyType = nothing
%>

<p>
<a href="listagencytypes.asp">Return to Agency Types Listing</a>
<br>

					</td>
				</tr>
			</table>
			</td></tr></table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			<!-- space-->
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>

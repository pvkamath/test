<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual = "/includes/EmailLog.Class.asp" ----------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim iCompanyId
iCompanyId = session("UserCompanyId")

'Make sure we were passed a valid EmailId
dim oLog
set oLog = new EmailLog
oLog.ConnectionString = application("sDataSourceName")
oLog.VocalErrors = application("bVocalErrors")


dim iEmailId
iEmailId = ScrubForSql(request("id"))

if oLog.LoadEmailLogById(iEmailId) = 0 then

	AlertError("This page requires a valid Email ID.")
	Response.End
	
elseif oLog.CompanyId <> iCompanyId then
	
	AlertError("This page requires a valid Email ID.")
	Response.End
	
end if 


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Email Detail -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Email Log" align="absmiddle" vspace="10"> Email Log</td>
				</tr>				

						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="100%">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Sender: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oLog.LookupUserId(oLog.SenderId) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Recipients: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oLog.RecipientList %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Date Sent: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oLog.SendDate %></td>
										</tr>										
										<tr>
											<td class="newstitle" nowrap>Subject: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oLog.Subject %></td>
										</tr>
										<tr>
											<td class="newstitle" valign="top" nowrap>Body: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><pre><% = oLog.BodyText %></pre></td>
										</tr>
										<% if oLog.Attachment <> "" then %>
										<tr>
											<td class="newstitle" nowrap>Attachment: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oLog.Attachment %></td>
										</tr>
										<% end if %>
									</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>

							
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

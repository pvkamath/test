<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/EmailLog.Class.asp" ---------------------------------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oLog

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))


set oLog = new EmailLog
oLog.ConnectionString = application("sDataSourceName")
oLog.VocalErrors = application("bVocalErrors")
oLog.CompanyId = session("UserCompanyId")

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchLogDateFrom")
	Session.Contents.Remove("SearchLogDateTo")
	Session.Contents.Remove("SearchLogRecipientList")
	Session.Contents.Remove("SearchLogSubject")
	Session.Contents.Remove("CurPage")
else


	oLog.SearchDateTo = ScrubForSql(request("SearchDateTo"))
	if (oLog.SearchDateTo = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchLogDateTo")) <> "") then
			oLog.SearchDateTo = session("SearchLogDateTo")
		end if
	else
		session("SearchLogDateTo") = oLog.SearchDateTo
	end if

	oLog.SearchDateFrom = ScrubForSql(request("SearchDateFrom"))
	if (oLog.SearchDateFrom = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchLogDateFrom")) <> "") then
			oLog.SearchDateFrom = session("SearchLogDateFrom")
		end if
	else
		session("SearchLogDateFrom") = oLog.SearchDateFrom
	end if
	
	oLog.RecipientList = ScrubForSql(request("Recipient"))
	if (oLog.RecipientList = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchLogRecipientList")) <> "") then
			oLog.RecipientList = session("SearchLogRecipientList")
		end if
	else
		session("SearchLogRecipientList") = oLog.RecipientList
	end if
	
	oLog.Subject = ScrubForSql(request("Subject"))
	if (oLog.Subject = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchLogSubject")) <> "") then
			oLog.Subject = session("SearchLogSubject")
		end if
	else
		session("SearchLogSubject") = oLog.Subject
	end if


	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if

end if

	
	set oRs = oLog.SearchEmailLogs()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20
	
%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Email Log" align="absmiddle" vspace="10"> Email Log</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<form name="frm1" action="EmailList.asp" method="POST">
								<input type="hidden" name="action" value="SEARCH">
								<input type="hidden" name="page_number" value="1">
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Recipient Name: </td>
											<td><input type="text" name="Recipient" size="20" maxlength="100" value="<% = oLog.RecipientList %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Subject: </td>
											<td><input type="text" name="Subject" size="20" value="<% = oLog.Subject %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="3"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Date Sent (From): </td>
											<td>
												<input type="text" name="SearchDateFrom" value="<% = oLog.SearchDateFrom %>" size="10" maxlength="10">
												<a href="javascript:show_calendar('frm1.SearchDateFrom');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frm1.SearchDateFrom');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Date Sent (To): </td>
											<td>			
												<input type="text" name="SearchDateTo" value="<% = oLog.SearchDateTo %>" size="10" maxlength="10">
												<a href="javascript:show_calendar('frm1.SearchDateTo');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('frm1.SearchDateTo');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>

				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value

	if not (oRs.BOF and oRs.EOF) then
	
		set oLog =  nothing
		set oLog = new EmailLog
		oLog.ConnectionString = application("sDataSourceName")
		oLog.VocalErrors = application("bVocalErrors")
	
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		'Set the beginning record to be displayed on the page
		oRs.AbsolutePage = iCurPage
		
		'Initialize the row/col style values
		sColor = ""
		
	
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
			if oLog.LoadEmailLogById(oRs("EmailId")) <> 0 then
		
					%>
										<tr>
											<td align="left"<% = sColor %>>&nbsp;&nbsp;<% = oLog.LookupUserId(oLog.SenderId) %></td>
											<td <% = sColor %>><a href="emaildetail.asp?id=<% = oRs("EmailId") %>" title="View Email Information"><% = oLog.Subject %></a></td>
											<td <% = sColor %>><% = oLog.SendDate %></td>
										</tr>
					<%

				'Set the col to the opposite value
					if sColor = "" then
						sColor = " bgcolor=""#ffffff"""
					else
						sColor = ""
					end if


			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

				
		'Finish off the table row if we ended on a left column.  Note that we check
		'for the positive bCol because the value is flipped at the end of the loop.
		if bCol then
			%>	
											<td width="48%" align="center"<% = sColor %>>&nbsp;</td>
										</tr>
			<%
		end if 	
		
		
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""emaillist.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""emaillist.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)	
		'response.write("</table>" & vbcrlf)
	
		'display Page number
		'response.write("<table width=""100%"">" & vbcrlf)	
		'response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		'response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)		
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%
	else
		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;There are currently no email records that match your search criteria.</a></td>
										</tr>
		<%
		'response.write("<tr><td colspan=""4"">There are currently no Loan Officers that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>
									</table>
								</td>
							</tr>
						</table>




								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oLog = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
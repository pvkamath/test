<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Search.Class.asp" ------------------------->
<!-- #include virtual = "/includes/User.Class.asp" --------------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if iCompanyId <> "" then

	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.end
			
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if

'get the company SA's
'set saRS = oCompany.GetCompanySAs(iCompanyId)

%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			(!checkString(FORM.Name, "Name") && !checkString(FORM.BranchNum, "Branch Number"))
			|| !checkString(FORM.Address, "Address")
			|| !checkString(FORM.City, "City")
			|| !checkString(FORM.State, "State")
			|| !checkZIPCode(FORM.Zipcode, 0)
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> Company Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">				

						<table  cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%"  border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Company</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CompanyForm" action="modcompanyproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the CompanyID on to the next step so we know we're working with an existing
'Company.
if iCompanyId <> "" then
%>
<input type="hidden" value="<% = iCompanyId %>" name="CompanyId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>Trade Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oCompany.Name %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Legal Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="LegalName" value="<% = oCompany.LegalName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Active: </b>
		</td>
		<td align="left" valign="top">	
			<% if oCompany.Active then %>
			<input type="checkbox" name="Active" value="1" CHECKED> 
			<% else %>
			<input type="checkbox" name="Active" value="1">
			<% end if %>
		</td>
	</tr>
	<!--
	<tr>
		<td align="left" valign="top">
			<b>Business Type: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayBusinessTypeDropDown(iBusinessType,1) %>
		</td>
	</tr>	
	-->
	<tr>
		<td align="left" valign="top">
			<b>State of Incorporation: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oCompany.IncorpStateId,1,"IncorpStateId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Date of Incorporation: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="IncorpDate" value="<% = oCompany.IncorpDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('CompanyForm.IncorpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('CompanyForm.IncorpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Organization Type: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayOrgTypesDropDown(oCompany.OrgTypeId, 1, "OrgTypeId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>EIN: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Ein" value="<% = oCompany.Ein %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #1: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Phone" value="<% = oCompany.Phone %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #1 Ext: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt" value="<% = oCompany.PhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #2: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Phone2" value="<% = oCompany.Phone2 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #2 Ext: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt2" value="<% = oCompany.PhoneExt2 %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #3: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Phone3" value="<% = oCompany.Phone3 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone #3 Ext: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt3" value="<% = oCompany.PhoneExt3 %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Fax: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Fax" value="<% = oCompany.Fax %>" size="12" maxlength="15">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Website: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Website" value="<% = oCompany.Website %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Email: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Email" value="<% = oCompany.Email %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Address: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Address" value="<% = oCompany.Address %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Address Line 2: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Address2" value="<% = oCompany.Address2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>City: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="City" value="<% = oCompany.City %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>		
		<td><b>State:</b></td>
		<td>
			<% call DisplayStatesDropDown(oCompany.StateId,1,"StateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Zipcode: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Zipcode" value="<% = oCompany.Zipcode %>" size="12" maxlength="15">
		</td>
	</tr>	
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Mailing Address: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress" value="<% = oCompany.MailingAddress %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Mailing Address Line 2: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MailingAddress2" value="<% = oCompany.MailingAddress2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Mailing City: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MailingCity" value="<% = oCompany.MailingCity %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Mailing State: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oCompany.MailingStateId,1,"MailingStateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Mailing Zipcode: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MailingZipcode" value="<% = oCompany.MailingZipcode %>" size="12" maxlength="15">
		</td>
	</tr>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Main Contact Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MainContactName" value="<% = oCompany.MainContactName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Main Contact Phone: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MainContactPhone" value="<% = oCompany.MainContactPhone %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Main Contact Phone Extension: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MainContactPhoneExt" value="<% = oCompany.MainContactPhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Main Contact Email: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MainContactEmail" value="<% = oCompany.MainContactEmail %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Main Contact Fax: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="MainContactFax" value="<% = oCompany.MainContactFax %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
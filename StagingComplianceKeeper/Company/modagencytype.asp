<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/AgencyType.Class.asp" ------------------------------------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Agency Type Admin"



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim iAgencyTypeId
iAgencyTypeId = ScrubForSQL(request("AgencyTypeID"))

dim oAgencyType
dim saRS
dim sMode

set oAgencyType = new AgencyType
oAgencyType.ConnectionString = application("sDataSourceName")
oAgencyType.TpConnectionString = application("sTpDataSourceName")
oAgencyType.VocalErrors = application("bVocalErrors")

if iAgencyTypeId <> "" then

	if oAgencyType.LoadAgencyTypeById(iAgencyTypeId) = 0 then
	
		'The load was unsuccessful.  Die.
		HandleError("Failed to load the passed Agency Type ID: " & iAgencyTypeId)
    elseif (oAgencyType.CompanyId <> application("UserCompanyId")) then
	
		'Make sure the user has access to this company.
		HandleError("You do not have access to the passed Agency Type ID: " & iAgencyTypeId)
	end if
	sMode = "Edit"
else
	sMode = "Add"
end if

%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>

<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.Name, "Name")
		   )
			return false;

		return true;
	}
</script>


<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Courses" align="absmiddle" vspace="10"> AgencyTypes</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> Agency Type</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="AgencyTypeForm" action="modagencytypeproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the CourseID on to the next step so we know we're working with an existing
'Course.
if iAgencyTypeId <> "" then
%>
<input type="hidden" value="<% = iAgencyTypeId %>" name="AgencyTypeId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oAgencyType.Name %>" size="50" maxlength="100">
		</td>
	</tr>		
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>


								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


<%
set saRS = nothing
set oAgencyType = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>
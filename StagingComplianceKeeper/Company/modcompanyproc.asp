<%
'option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = session("CompanyName") &  " Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
bNew = true
if iCompanyId <> "" then

	bNew = false
	
	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("Failed to load the passed " & session("CompanyName") & " ID: " & iCompanyId)
		Response.End
			
	end if

end if


'Assign the PLP properties based on our passed parameters
'oCompany.OwnerStateId = session("AdminStateId")
oCompany.Name = ScrubForSQL(request("Name"))
oCompany.LegalName = ScrubForSql(request("LegalName"))
'if request("Active") = "1" then 
'	oCompany.Active = true
'else
'	oCompany.Active = false
'end if 
oCompany.IncorpStateId = ScrubForSQL(request("IncorpStateId"))
oCompany.IncorpDate = ScrubForSQL(request("IncorpDate"))
'oCompany.FiscalYearEnd = ScrubForSQL(request("FiscalYearEnd"))
oCompany.OrgTypeId = ScrubForSQL(request("OrgTypeId"))
oCompany.Ein = ScrubForSql(request("Ein"))
oCompany.Phone = ScrubForSQL(request("Phone"))
oCompany.PhoneExt = ScrubForSQL(request("PhoneExt"))
oCompany.Phone2 = ScrubForSQL(request("Phone2"))
oCompany.PhoneExt2 = ScrubForSQL(request("PhoneExt2"))
oCompany.Phone3 = ScrubForSQL(request("Phone3"))
oCompany.PhoneExt3 = ScrubForSQL(request("PhoneExt3"))
oCompany.Fax = ScrubForSQL(request("Fax"))
oCompany.Website = ScrubForSQL(request("Website"))
oCompany.Email = ScrubForSql(request("Email"))
oCompany.Address = ScrubForSQL(request("Address"))
oCompany.Address2 = ScrubForSQL(request("Address2"))
oCompany.City = ScrubForSQL(request("City"))
oCompany.StateId = ScrubForSQL(request("StateId"))
oCompany.Zipcode = ScrubForSQL(request("Zipcode"))
oCompany.ZipcodeExt = ScrubForSql(request("ZipcodeExt"))
oCompany.MailingAddress = ScrubForSQL(request("MailingAddress"))
oCompany.MailingAddress2 = ScrubForSQL(request("MailingAddress2"))
oCompany.MailingCity = ScrubForSQL(request("MailingCity"))
oCompany.MailingStateId = ScrubForSQL(request("MailingStateId"))
oCompany.MailingZipcode = ScrubForSQL(request("MailingZipcode"))
oCompany.MainContactName = ScrubForSQL(request("MainContactName"))
oCompany.MainContactPhone = ScrubForSQL(request("MainContactPhone"))
oCompany.MainContactPhoneExt = ScrubForSql(request("MainContactPhoneExt"))
oCompany.MainContactEmail = ScrubForSQL(request("MainContactEmail"))
oCompany.MainContactFax = ScrubForSQL(request("MainContactFax"))


'We need a connectionstring too.
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")


'If this is a new Company we need to save it before it will have a unique ID.
dim iNewCompanyId
if oCompany.CompanyId = "" then

	iNewCompanyId = oCompany.SaveCompany

	if iNewCompanyId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Company.  Stop.
		Response.Write("Error: Failed to save new " & session("CompanyName") & ".")
		Response.End

	end if
	
else

	iNewCompanyId = oCompany.SaveCompany

	if iNewCompanyId = 0 then

		'The save failed.  Die.
		Response.Write("Error: Failed to save existing " & session("CompanyName") & ".")
		Response.End

	elseif CINT(iNewCompanyId) <> CINT(iCompanyId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		Response.Write("Error: Unexpected results while saving existing " & session("CompanyName") & ".")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% if bNew then %>Add <% else %>Edit <% end if %> <% = session("CompanyName") %> Profile</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new <% = session("CompanyName") %> "<% = oCompany.Name %>" was successfully created.</b>
<%
else
%>
<b>The <% = session("CompanyName") %> "<% = oCompany.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="Companydetail.asp">Return to <% = session("CompanyName") %> Profile</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
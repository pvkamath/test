﻿<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/EmployeeType.Class.asp" ------------------------------------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Employee Type Admin"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
      // Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
      function InitializePage() {
      }
      // Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim iEmployeeTypeId
iEmployeeTypeId = ScrubForSQL(request("EmployeeTypeID"))

dim oEmployeeType
set oEmployeeType = new EmployeeType
oEmployeeType.VocalErrors = application("bVocalErrors")
oEmployeeType.ConnectionString = application("sDataSourceName")
oEmployeeType.TpConnectionString = application("sTpDataSourceName")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
bNew = true
if iEmployeeTypeId <> "" then

	bNew = false
	
	if oEmployeeType.LoadEmployeeTypeById(iEmployeeTypeId) = 0 then
	
		'The load was unsuccessful.  Die.
		HandleError("Failed to load the passed Employee Type ID: " & iEmployeeTypeId)
	elseif (oEmployeeType.CompanyId <> application("UserCompanyId")) then
	
		'Make sure the user has access to this company.
		HandleError("You do not have access to the passed Employee Type ID: " & iEmployeeTypeId)
	end if

end if


'Assign the Course properties based on our passed parameters
'oEmployeeType.EmployeeTypeId = session("TypeID")
oEmployeeType.CompanyId = session("UserCompanyId")
oEmployeeType.Name = ScrubForSql(request("Name"))
'If this is a new Company we need to save it before it will have a unique ID.
dim iNewEmployeeTypeId
if oEmployeeType.EmployeeTypeId = "" then

	iNewEmployeeTypeId = oEmployeeType.SaveEmployeeType

	if iNewEmployeeTypeId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Course.  Stop.
		HandleError("Error: Failed to save new Employee Type.")

	end if
	
else

	iNewEmployeeTypeId = oEmployeeType.SaveEmployeeType

	if iNewEmployeeTypeId = 0 then

		'The save failed.  Die.
		HandleError("Error: Failed to save existing Employee Type.")

	elseif CINT(iNewEmployeeTypeId) <> CINT(iEmployeeTypeId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our object.
		HandleError("Error: Unexpected results while saving existing Employee Type.")
		
	end if

end if

%>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Courses" align="absmiddle" vspace="10"> Employee Types</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% if bNew then %>Add<% else %>Edit<% end if %> Employee Type</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Employee Type "<% = oEmployeeType.Name %>" was successfully created.</b>
<%
else
%>
<b>The Employee Type "<% = oEmployeeType.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="listEmployeetypes.asp">Return to Employee Type Listing</a>
<br>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oEmployeeType = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>


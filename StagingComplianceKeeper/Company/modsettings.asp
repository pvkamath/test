<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Settings.Class.asp" ----------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim i
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if iCompanyId <> "" then

	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.end
		
	end if
	
	sMode = "Edit"

end if


'Load the settings for this company if we have any.
dim oSettings
set oSettings = new Settings
oSettings.ConnectionString = application("sDataSourceName")
oSettings.VocalErrors = false 'application("bVocalErrors")

oSettings.LoadSettingsByCompanyId(iCompanyId)
%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	//Enables or Disables the appropriate Send Daily 30 Day email box depending upon if the Send30DayEmail/Send30DayCourseEmail box is set.
	function EnableDisableEmailDailyBox(p_o30DayBox, p_oDailyBox)
	{
		if (p_o30DayBox.checked)
		{
			p_oDailyBox.disabled = false;
		}
		else
		{
			p_oDailyBox.disabled = true;
			p_oDailyBox.checked = false;
		}
	}
	
	function Validate(FORM)
	{
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">				

						<table  cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%"  border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> System Settings</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CompanyForm" action="modsettingsproc.asp" method="POST" onSubmit="return Validate(this)">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a license expiring in 30 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Use Renewal Application Deadline instead of License Expiration Date: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="UseRenewalAppDeadlineLicExpEmails" value="1" <% if oSettings.UseRenewalAppDeadlineLicExpEmails then %>CHECKED<% end if%>>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
	<tr>
		<td align="left" valign="top">
			<b>Send 30 Day Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send30DayEmail" value="1"<% if oSettings.Send30DayEmail then %> CHECKED<% end if %> onClick="EnableDisableEmailDailyBox(this, document.CompanyForm.Send30DayEmailDaily)">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 30 Day emails every day until expiration date passes: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send30DayEmailDaily" value="1"<% if oSettings.Send30DayEmailDaily then %> CHECKED<% end if %> <% if not oSettings.Send30DayEmail then %> DISABLED<% end if %>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>30 Day Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="Email30DayText" cols="50" rows="7"><% = oSettings.Email30DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send to Loan Officers: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="SendLO30DayEmail" value="1" <% if oSettings.SendLO30DayEmail then %>CHECKED<% end if%>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin30Day" value="1"<% if oSettings.CcCoAdmin30Day then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin30Day" value="1"<% if oSettings.CcL2Admin30Day then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin30Day" value="1"<% if oSettings.CcL3Admin30Day then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>					
					<td><input type="checkbox" name="CcBrAdmin30Day" value="1"<% if oSettings.CcBrAdmin30Day then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a license expiring in 60 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 60 Day Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send60DayEmail" value="1" <% if oSettings.Send60DayEmail then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>60 Day Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="Email60DayText" cols="50" rows="7"><% = oSettings.Email60DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send to Loan Officers: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="SendLO60DayEmail" value="1" <% if oSettings.SendLO60DayEmail then %>CHECKED<% end if%>>
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin60Day" value="1"<% if oSettings.CcCoAdmin60Day then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin60Day" value="1"<% if oSettings.CcL2Admin60Day then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin60Day" value="1"<% if oSettings.CcL3Admin60Day then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcBrAdmin60Day" value="1"<% if oSettings.CcBrAdmin60Day then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a license expiring in 90 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 90 Day Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send90DayEmail" value="1"<% if oSettings.Send90DayEmail then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>90 Day Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="Email90DayText" cols="50" rows="7"><% = oSettings.Email90DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send to Loan Officers: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="SendLO90DayEmail" value="1" <% if oSettings.SendLO90DayEmail then %>CHECKED<% end if%>>
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin90Day" value="1"<% if oSettings.CcCoAdmin90Day then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin90Day" value="1"<% if oSettings.CcL2Admin90Day then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin90Day" value="1"<% if oSettings.CcL3Admin90Day then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcBrAdmin90Day" value="1"<% if oSettings.CcBrAdmin90Day then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr></td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a course expiring in 30 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 30 Day Course Update Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send30DayCourseEmail" value="1"<% if oSettings.Send30DayCourseEmail then %> CHECKED<% end if %> onClick="EnableDisableEmailDailyBox(this, document.CompanyForm.Send30DayCourseEmailDaily)">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 30 Day emails every day until expiration date passes: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send30DayCourseEmailDaily" value="1"<% if oSettings.Send30DayCourseEmailDaily then %> CHECKED<% end if %> <% if not oSettings.Send30DayCourseEmail then %> DISABLED<% end if %>>
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>30 Day Course Update Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="CourseEmail30DayText" cols="50" rows="7"><% = oSettings.CourseEmail30DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin30DayCourse" value="1"<% if oSettings.CcCoAdmin30DayCourse then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin30DayCourse" value="1"<% if oSettings.CcL2Admin30DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin30DayCourse" value="1"<% if oSettings.CcL3Admin30DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcBrAdmin30DayCourse" value="1"<% if oSettings.CcBrAdmin30DayCourse then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a course expiring in 60 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 60 Day Course Update Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send60DayCourseEmail" value="1"<% if oSettings.Send60DayCourseEmail then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>60 Day Course Update Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="CourseEmail60DayText" cols="50" rows="7"><% = oSettings.CourseEmail60DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin60DayCourse" value="1"<% if oSettings.CcCoAdmin60DayCourse then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin60DayCourse" value="1"<% if oSettings.CcL2Admin60DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin60DayCourse" value="1"<% if oSettings.CcL3Admin60DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcBrAdmin60DayCourse" value="1"<% if oSettings.CcBrAdmin60DayCourse then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers with a course expiring in 90 days.  You also may CC the officer's administrators by checking the boxes below.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Send 90 Day Course Update Emails: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="Send90DayCourseEmail" value="1"<% if oSettings.Send90DayCourseEmail then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>90 Day Course Update Email Body Text: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="CourseEmail90DayText" cols="50" rows="7"><% = oSettings.CourseEmail90DayText %></textarea>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Administrators: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="CcCoAdmin90DayCourse" value="1"<% if oSettings.CcCoAdmin90DayCourse then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcL2Admin90DayCourse" value="1"<% if oSettings.CcL2Admin90DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="CcL3Admin90DayCourse" value="1"<% if oSettings.CcL3Admin90DayCourse then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="CcBrAdmin90DayCourse" value="1"<% if oSettings.CcBrAdmin90DayCourse then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr></td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers and administrators regarding events which have been marked with the Send Alert Emails setting.  Updates will be sent automatically 30 days prior to the event.  The check boxes below control which officers and user types receive the updates.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Event Update Emails: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="SendCoAdminEventEmail" value="1"<% if oSettings.SendCoAdminEventEmail then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="SendL2AdminEventEmail" value="1"<% if oSettings.SendL2AdminEventEmail then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="SendL3AdminEventEmail" value="1"<% if oSettings.SendL3AdminEventEmail then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="SendBrAdminEventEmail" value="1"<% if oSettings.SendBrAdminEventEmail then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
				<tr>
					<td colspan="3"><input type="checkbox" name="SendLoEventEmail" value="1"<% if oSettings.SendLoEventEmail then %> CHECKED<% end if %>> Loan Officers</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr></td>
	</tr>
	<tr>
		<td align="left" colspan="2">
		<i>This setting allows alert emails to be automatically sent to loan officers and administrators regarding officers with Fingerprint Deadlines.  Updates will be sent automatically 90, 60 and 30 days prior to the Fingerprint Deadline Date.  The check boxes below control which officers and user types receive the updates.</i>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC Fingerprint Deadline Emails: </b>
		</td>
		<td align="left" valign="top">
			<table cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td><input type="checkbox" name="SendCoAdminFingerprintDeadlineEmail" value="1"<% if oSettings.SendCoAdminFingerprintDeadlineEmail then %> CHECKED<% end if %>> Company Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="SendL2AdminFingerprintDeadlineEmail" value="1"<% if oSettings.SendL2AdminFingerprintDeadlineEmail then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Administrators</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="SendL3AdminFingerprintDeadlineEmail" value="1"<% if oSettings.SendL3AdminFingerprintDeadlineEmail then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Administrators</td>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="SendBrAdminFingerprintDeadlineEmail" value="1"<% if oSettings.SendBrAdminFingerprintDeadlineEmail then %> CHECKED<% end if %>> Branch Administrators</td>
				</tr>
				<tr>
					<td colspan="3"><input type="checkbox" name="SendLoFingerprintDeadlineEmail" value="1"<% if oSettings.SendLoFingerprintDeadlineEmail then %> CHECKED<% end if %>> Loan Officers</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr></td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Company Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CompanyName" value="<% = oSettings.CompanyName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Second-Tier Company Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CompanyL2Name" value="<% = oSettings.CompanyL2Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Third-Tier Company Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CompanyL3Name" value="<% = oSettings.CompanyL3Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Allow Editing of Officer Notes: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="AllowOfficerNoteEditing" value="1"<% if oSettings.AllowOfficerNoteEditing then %> CHECKED<% end if %>>
		</td>
	</tr>	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>Custom Field #1 Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CustField1Name" value="<% = oSettings.CustField1Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Custom Field #2 Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CustField2Name" value="<% = oSettings.CustField2Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Custom Field #3 Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CustField3Name" value="<% = oSettings.CustField3Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Custom Field #4 Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CustField4Name" value="<% = oSettings.CustField4Name %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Custom Field #5 Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CustField5Name" value="<% = oSettings.CustField5Name %>" size="50" maxlength="50">
		</td>
	</tr>
	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<%
	for i = 1 to 15
	%>
	<tr>
		<td align="left" valign="top">
			<b>Officer Custom Field #<%= i %> Label: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="OfficerCustField<%=i%>Name" value="<% = oSettings.GetOfficerCustFieldName(i) %>" size="50" maxlength="50">
		</td>
	</tr>	
	<%
	next
	%>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif" id=image1 name=image1>&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
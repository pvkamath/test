<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Settings.Class.asp" ----------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Company Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim i
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if iCompanyId <> "" then

	if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.write("Failed to load the passed CompanyID: " & iCompanyId)
		Response.end
		
	end if
	
	sMode = "Edit"

end if


'Load the settings for this company if we have any.
dim oSettings
set oSettings = new Settings
oSettings.ConnectionString = application("sDataSourceName")
oSettings.VocalErrors = false 'application("bVocalErrors")

if oSettings.LoadSettingsByCompanyId(iCompanyId) = 0 then

	oSettings.CompanyId = iCompanyId
	
end if


'Set the properties based on our passed values.
if request("UseRenewalAppDeadlineLicExpEmails") = "1" then
	oSettings.UseRenewalAppDeadlineLicExpEmails = true
else
	oSettings.UseRenewalAppDeadlineLicExpEmails = false
end if
if request("Send30DayEmail") = "1" then
	oSettings.Send30DayEmail = true
else
	oSettings.Send30DayEmail = false
end if 
if request("Send30DayEmailDaily") = "1" then
	oSettings.Send30DayEmailDaily = true
else
	oSettings.Send30DayEmailDaily = false
end if 
if request("Send60DayEmail") = "1" then
	oSettings.Send60DayEmail = true
else
	oSettings.Send60DayEmail = false
end if
if request("Send90DayEmail") = "1" then
	oSettings.Send90DayEmail = true
else
	oSettings.Send90DayEmail = false
end if 
if request("SendCourseEmail") = "1" then
	oSettings.SendCourseEmail = true
else
	oSettings.SendCourseEmail = false
end if 
if request("Send30DayCourseEmail") = "1" then
	oSettings.Send30DayCourseEmail = true
else
	oSettings.Send30DayCourseEmail = false
end if 
if request("Send30DayCourseEmailDaily") = "1" then
	oSettings.Send30DayCourseEmailDaily = true
else
	oSettings.Send30DayCourseEmailDaily = false
end if 
if request("Send60DayCourseEmail") = "1" then
	oSettings.Send60DayCourseEmail = true
else
	oSettings.Send60DayCourseEmail = false
end if
if request("Send90DayCourseEmail") = "1" then
	oSettings.Send90DayCourseEmail = true
else
	oSettings.Send90DayCourseEmail = false
end if 
if request("SendLO30DayEmail") = "1" then
	oSettings.SendLO30DayEmail = true
else
	oSettings.SendLO30DayEmail = false
end if
if request("CcBrAdmin30Day") = "1" then
	oSettings.CcBrAdmin30Day = true
else
	oSettings.CcBrAdmin30Day = false
end if
if request("CcCoAdmin30Day") = "1" then
	oSettings.CcCoAdmin30Day = true
else
	oSettings.CcCoAdmin30Day = false
end if
if request("CcL2Admin30Day") = "1" then
	oSettings.CcL2Admin30Day = true
else
	oSettings.CcL2Admin30Day = false
end if
if request("CcL3Admin30Day") = "1" then
	oSettings.CcL3Admin30Day = true
else
	oSettings.CcL3Admin30Day = false
end if
if request("SendLO60DayEmail") = "1" then
	oSettings.SendLO60DayEmail = true
else
	oSettings.SendLO60DayEmail = false
end if
if request("CcBrAdmin60Day") = "1" then
	oSettings.CcBrAdmin60Day = true
else
	oSettings.CcBrAdmin60Day = false
end if
if request("CcCoAdmin60Day") = "1" then
	oSettings.CcCoAdmin60Day = true
else
	oSettings.CcCoAdmin60Day = false
end if
if request("CcL2Admin60Day") = "1" then
	oSettings.CcL2Admin60Day = true
else
	oSettings.CcL2Admin60Day = false
end if
if request("CcL3Admin60Day") = "1" then
	oSettings.CcL3Admin60Day = true
else
	oSettings.CcL3Admin60Day = false
end if
if request("SendLO90DayEmail") = "1" then
	oSettings.SendLO90DayEmail = true
else
	oSettings.SendLO90DayEmail = false
end if
if request("CcBrAdmin90Day") = "1" then
	oSettings.CcBrAdmin90Day = true
else
	oSettings.CcBrAdmin90Day = false
end if
if request("CcCoAdmin90Day") = "1" then
	oSettings.CcCoAdmin90Day = true
else
	oSettings.CcCoAdmin90Day = false
end if
if request("CcL2Admin90Day") = "1" then
	oSettings.CcL2Admin90Day = true
else
	oSettings.CcL2Admin90Day = false
end if
if request("CcL3Admin90Day") = "1" then
	oSettings.CcL3Admin90Day = true
else
	oSettings.CcL3Admin90Day = false
end if

if request("CcBrAdmin30DayCourse") = "1" then
	oSettings.CcBrAdmin30DayCourse = true
else
	oSettings.CcBrAdmin30DayCourse = false
end if
if request("CcCoAdmin30DayCourse") = "1" then
	oSettings.CcCoAdmin30DayCourse = true
else
	oSettings.CcCoAdmin30DayCourse = false
end if
if request("CcL2Admin30DayCourse") = "1" then
	oSettings.CcL2Admin30DayCourse = true
else
	oSettings.CcL2Admin30DayCourse = false
end if
if request("CcL3Admin30DayCourse") = "1" then
	oSettings.CcL3Admin30DayCourse = true
else
	oSettings.CcL3Admin30DayCourse = false
end if

if request("CcBrAdmin60DayCourse") = "1" then
	oSettings.CcBrAdmin60DayCourse = true
else
	oSettings.CcBrAdmin60DayCourse = false
end if
if request("CcCoAdmin60DayCourse") = "1" then
	oSettings.CcCoAdmin60DayCourse = true
else
	oSettings.CcCoAdmin60DayCourse = false
end if
if request("CcL2Admin60DayCourse") = "1" then
	oSettings.CcL2Admin60DayCourse = true
else
	oSettings.CcL2Admin60DayCourse = false
end if
if request("CcL3Admin60DayCourse") = "1" then
	oSettings.CcL3Admin60DayCourse = true
else
	oSettings.CcL3Admin60DayCourse = false
end if

if request("CcBrAdmin90DayCourse") = "1" then
	oSettings.CcBrAdmin90DayCourse = true
else
	oSettings.CcBrAdmin90DayCourse = false
end if
if request("CcCoAdmin90DayCourse") = "1" then
	oSettings.CcCoAdmin90DayCourse = true
else
	oSettings.CcCoAdmin90DayCourse = false
end if
if request("CcL2Admin90DayCourse") = "1" then
	oSettings.CcL2Admin90DayCourse = true
else
	oSettings.CcL2Admin90DayCourse = false
end if
if request("CcL3Admin90DayCourse") = "1" then
	oSettings.CcL3Admin90DayCourse = true
else
	oSettings.CcL3Admin90DayCourse = false
end if




if request("SendCoAdminCourseEmail") = "1" then
	oSettings.SendCoAdminCourseEmail = true
else
	oSettings.SendCoAdminCourseEmail = false
end if 
if request("SendBrAdminCourseEmail") = "1" then
	oSettings.SendBrAdminCourseEmail = true
else
	oSettings.SendBrAdminCourseEmail = false
end if 
if request("SendL2AdminCourseEmail") = "1" then
	oSettings.SendL2AdminCourseEmail = true
else
	oSettings.SendL2AdminCourseEmail = false
end if 
if request("SendL3AdminCourseEmail") = "1" then
	oSettings.SendL3AdminCourseEmail = true
else
	oSettings.SendL3AdminCourseEmail = false
end if 
if request("SendLoCourseEmail") = "1" then
	oSettings.SendLoCourseEmail = true
else
	oSettings.SendLoCourseEmail = false
end if 


if request("SendCoAdminEventEmail") = "1" then
	oSettings.SendCoAdminEventEmail = true
else
	oSettings.SendCoAdminEventEmail = false
end if 
if request("SendBrAdminEventEmail") = "1" then
	oSettings.SendBrAdminEventEmail = true
else
	oSettings.SendBrAdminEventEmail = false
end if 
if request("SendL2AdminEventEmail") = "1" then
	oSettings.SendL2AdminEventEmail = true
else
	oSettings.SendL2AdminEventEmail = false
end if 
if request("SendL3AdminEventEmail") = "1" then
	oSettings.SendL3AdminEventEmail = true
else
	oSettings.SendL3AdminEventEmail = false
end if 
if request("SendLoEventEmail") = "1" then
	oSettings.SendLoEventEmail = true
else
	oSettings.SendLoEventEmail = false
end if 

if request("SendCoAdminFingerprintDeadlineEmail") = "1" then
	oSettings.SendCoAdminFingerprintDeadlineEmail = true
else
	oSettings.SendCoAdminFingerprintDeadlineEmail = false
end if 
if request("SendBrAdminFingerprintDeadlineEmail") = "1" then
	oSettings.SendBrAdminFingerprintDeadlineEmail = true
else
	oSettings.SendBrAdminFingerprintDeadlineEmail = false
end if 
if request("SendL2AdminFingerprintDeadlineEmail") = "1" then
	oSettings.SendL2AdminFingerprintDeadlineEmail = true
else
	oSettings.SendL2AdminFingerprintDeadlineEmail = false
end if 
if request("SendL3AdminFingerprintDeadlineEmail") = "1" then
	oSettings.SendL3AdminFingerprintDeadlineEmail = true
else
	oSettings.SendL3AdminFingerprintDeadlineEmail = false
end if 
if request("SendLoFingerprintDeadlineEmail") = "1" then
	oSettings.SendLoFingerprintDeadlineEmail = true
else
	oSettings.SendLoFingerprintDeadlineEmail = false
end if 

oSettings.Email30DayText = ScrubForSql(request("Email30DayText"))
oSettings.Email60DayText = ScrubForSql(request("Email60DayText"))
oSettings.Email90DayText = ScrubForSql(request("Email90DayText"))
oSettings.CourseEmailText = ScrubForSql(request("CourseEmailText"))
oSettings.CourseEmail30DayText = ScrubForSql(request("CourseEmail30DayText"))
oSettings.CourseEmail60DayText = ScrubForSql(request("CourseEmail60DayText"))
oSettings.CourseEmail90DayText = ScrubForSql(request("CourseEmail90DayText"))
oSettings.CompanyName = ScrubForSql(request("CompanyName"))
oSettings.CompanyL2Name = ScrubForSql(request("CompanyL2Name"))
oSettings.CompanyL3Name = ScrubForSql(request("CompanyL3Name"))

if request("AllowOfficerNoteEditing") = "1" then
	oSettings.AllowOfficerNoteEditing = true
else
	oSettings.AllowOfficerNoteEditing = false
end if 

oSettings.CustField1Name = ScrubForSql(request("CustField1Name"))
oSettings.CustField2Name = ScrubForSql(request("CustField2Name"))
oSettings.CustField3Name = ScrubForSql(request("CustField3Name"))
oSettings.CustField4Name = ScrubForSql(request("CustField4Name"))
oSettings.CustField5Name = ScrubForSql(request("CustField5Name"))

for i = 1 to 15
	call oSettings.SetOfficerCustFieldName(i,ScrubForSql(request("OfficerCustField" & i & "Name")))
next


'Reset the session vars for the company names based on the new values.
if oSettings.CompanyName <> "" then
	session("CompanyName") = oSettings.CompanyName
else
	session("CompanyName") = "Company"
end if
if oSettings.CompanyL2Name <> "" then
	session("CompanyL2Name") = oSettings.CompanyL2Name
else
	session("CompanyL2Name") = "Division"
end if
if oSettings.CompanyL3Name <> "" then
	session("CompanyL3Name") = oSettings.CompanyL3Name
else
	session("CompanyL3Name") = "Region"
end if

'Reset the session var for the Allow Editing of Officer Notes var with the new value
session("AllowOfficerNoteEditing") = oSettings.AllowOfficerNoteEditing

'Reset the session vars for the custom field labels based on the new values.
if oSettings.CustField1Name <> "" then
	session("CustField1Name") = oSettings.CustField1Name
else
	Session.Contents.Remove("CustField1Name")
end if
if oSettings.CustField2Name <> "" then
	session("CustField2Name") = oSettings.CustField2Name
else
	Session.Contents.Remove("CustField2Name")
end if
if oSettings.CustField3Name <> "" then
	session("CustField3Name") = oSettings.CustField3Name
else
	Session.Contents.Remove("CustField3Name") 
end if
if oSettings.CustField4Name <> "" then
	session("CustField4Name") = oSettings.CustField4Name
else
	Session.Contents.Remove("CustField4Name") 
end if
if oSettings.CustField5Name <> "" then
	session("CustField5Name") = oSettings.CustField5Name
else
	Session.Contents.Remove("CustField5Name") 
end if

'Reset the session vars for the custom officer field labels based on the new values.
if oSettings.OfficerCustField1Name <> "" then
	session("OfficerCustField1Name") = oSettings.OfficerCustField1Name
else
	Session.Contents.Remove("OfficerCustField1Name") 
end if
if oSettings.OfficerCustField2Name <> "" then
	session("OfficerCustField2Name") = oSettings.OfficerCustField2Name
else
	Session.Contents.Remove("OfficerCustField2Name") 
end if
if oSettings.OfficerCustField3Name <> "" then
	session("OfficerCustField3Name") = oSettings.OfficerCustField3Name
else
	Session.Contents.Remove("OfficerCustField3Name") 
end if
if oSettings.OfficerCustField4Name <> "" then
	session("OfficerCustField4Name") = oSettings.OfficerCustField4Name
else
	Session.Contents.Remove("OfficerCustField4Name") 
end if
if oSettings.OfficerCustField5Name <> "" then
	session("OfficerCustField5Name") = oSettings.OfficerCustField5Name
else
	Session.Contents.Remove("OfficerCustField5Name")
end if
if oSettings.OfficerCustField6Name <> "" then
	session("OfficerCustField6Name") = oSettings.OfficerCustField6Name
else
	Session.Contents.Remove("OfficerCustField6Name") 
end if
if oSettings.OfficerCustField7Name <> "" then
	session("OfficerCustField7Name") = oSettings.OfficerCustField7Name
else
	Session.Contents.Remove("OfficerCustField7Name") 
end if
if oSettings.OfficerCustField8Name <> "" then
	session("OfficerCustField8Name") = oSettings.OfficerCustField8Name
else
	Session.Contents.Remove("OfficerCustField8Name") 
end if
if oSettings.OfficerCustField9Name <> "" then
	session("OfficerCustField9Name") = oSettings.OfficerCustField9Name
else
	Session.Contents.Remove("OfficerCustField9Name") 
end if
if oSettings.OfficerCustField10Name <> "" then
	session("OfficerCustField10Name") = oSettings.OfficerCustField10Name
else
	Session.Contents.Remove("OfficerCustField10Name") 
end if
if oSettings.OfficerCustField11Name <> "" then
	session("OfficerCustField11Name") = oSettings.OfficerCustField11Name
else
	Session.Contents.Remove("OfficerCustField11Name") 
end if
if oSettings.OfficerCustField12Name <> "" then
	session("OfficerCustField12Name") = oSettings.OfficerCustField12Name
else
	Session.Contents.Remove("OfficerCustField12Name") 
end if
if oSettings.OfficerCustField13Name <> "" then
	session("OfficerCustField13Name") = oSettings.OfficerCustField13Name
else
	Session.Contents.Remove("OfficerCustField13Name") 
end if
if oSettings.OfficerCustField14Name <> "" then
	session("OfficerCustField14Name") = oSettings.OfficerCustField14Name
else
	Session.Contents.Remove("OfficerCustField14Name") 
end if
if oSettings.OfficerCustField15Name <> "" then
	session("OfficerCustField15Name") = oSettings.OfficerCustField15Name
else
	Session.Contents.Remove("OfficerCustField15Name")
end if


'Save the settings object
if oSettings.SaveSettings = 0 then

	HandleError("The settings were not properly saved.  Please try again.")
	Response.End

end if 
%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Profile</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">				

						<table  cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%"  border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> System Settings</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

The system settings were updated successfully.  

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
'on error resume next
%>
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->

<%


' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
	



dim iUserID
dim sMode
dim oUserObj
dim rs
dim sLogin
dim sEmail, sOldLogin, sPassword, sConfirmPassword
dim sFirstName, sMid, sLastName
dim iGroupID, iProviderID, iBranchID, iCompanyId
dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo
dim sCity, sAddress, sZipcode, sPhone, sFax
dim iStateID, iUserStatus, iCrossCertEligible, iNewsletter, iAllowedToAddUsers, iCConAlertEmails
dim sMarketWareConfirmNo
dim sNotes
dim sMessage
dim bSuccess

set oUserObj = New User
oUserObj.ConnectionString = application("sDataSourceName")

sMode = trim(request("Mode"))
iUserID = ScrubForSQL(request("UserID"))
sEmail = ScrubForSQL(request("Email"))
sLogin = ScrubForSql(request("Login"))
sOldLogin = ScrubForSQL(request("OldLogin"))
sPassword = ScrubForSQL(request("Password"))
sConfirmPassword = ScrubForSQL(request("ConfirmPassword"))
sFirstName = ScrubForSQL(request("FirstName"))
sMid = ScrubForSQL(request("MiddleName"))
sLastName = ScrubForSQL(request("LastName"))
iGroupID = ScrubForSQL(request("GroupID"))
if iGroupId = 1 then
	iGroupId = 10
end if
iCompanyId = session("UserCompanyId")
'iBranchId = ScrubForSql(request("BranchId"))

sCompanyL2IdList = ScrubForSql(request("CompanyL2Id"))
if (iGroupId = 3 or iGroupId = 4) and sCompanyL2IdList = "" then
	AlertError("This operation requires a valid " & session("CompanyL2Name") & " selection.")
end if

sCompanyL3IdList = ScrubForSql(request("CompanyL3Id"))
if (iGroupId = 5 or iGroupId = 6) and sCompanyL3IdList = "" then
	AlertError("This operation requires a valid " & session("CompanyL3Name") & " selection.")
end if

sBranchIdList = ScrubForSql(request("BranchId"))
if (iGroupId = 10 or iGroupId = 11) and sBranchIdList = "" then
	HandleError("This operation requires a valid Branch selection.")
end if



iProviderId = ScrubForSql(request("Provider"))
if iGroupId = 20 and iProviderId = "" then
	HandleError("This operation requires a valid Provider selection.")
end if 

sCity = ScrubForSQL(request("City"))
iStateID = ScrubForSQL(request("State"))
sAddress = ScrubForSQL(request("Address"))
sZipcode = ScrubForSQL(request("Zipcode"))
sPhone = ScrubForSQL(request("Phone"))
sFax = ScrubForSQL(request("Fax"))
iUserStatus = ScrubForSQL(request("UserStatus"))
sNotes = ScrubForSQL(request("Notes"))

if Ucase(trim(request("CConAlertEmails"))) = "ON" then
	iCConAlertEmails = 1
else
	iCConAlertEmails = 0
end if

if UCase(sMode) = "ADD" then
	'Make Sure this Login is not in the system
	if (oUserObj.LoginExists(sLogin)) then
		HandleError("The Login - " & sLogin & " - is being used by another user.  Please choose another login.")
	end if
	
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MiddleName = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.CompanyL2IdList = sCompanyL2IdList
	oUserObj.CompanyL3IdList = sCompanyL3IdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.CConAlertEmails = iCConAlertEmails
	oUserObj.Notes = sNotes
	
	iUserID = oUserObj.AddUser()
	
	if cint(iUserID) > 0 then
		sMessage = sFirstName & " " & sLastName & " has been Added successfully."
	else
		response.redirect("/admin/error.asp?message=The User was not Added successfully.")		
	end if
elseif UCase(sMode) = "EDIT" then
	'if login has changed, Make Sure this Login is not in the system
	if (trim(sLogin) <> trim(sOldLogin)) and (oUserObj.LoginExists(sLogin)) then
		HandleError("The Login - " & sLogin & " - is being used by another user.  Please choose another login.")
	end if
	
	oUserObj.UserID = iUserID
	oUserObj.Login = sLogin
	oUserObj.Email = sEmail
	oUserObj.Password = sPassword
	oUserObj.FirstName = sFirstName
	oUserObj.MiddleName = sMid
	oUserObj.LastName = sLastName
	oUserObj.GroupID = iGroupID
	oUserObj.CompanyId = iCompanyId
	'oUserObj.BranchId = iBranchId
	oUserObj.CompanyL2IdList = sCompanyL2IdList
	oUserObj.CompanyL3IdList = sCompanyL3IdList
	oUserObj.BranchIdList = sBranchIdList
	oUserObj.ProviderId = iProviderId
	oUserObj.City = sCity
	oUserObj.StateID = iStateID
	oUserObj.Address = sAddress
	oUserObj.Zipcode = sZipcode
	oUserObj.Phone = sPhone
	oUserObj.Fax = sFax
	oUserObj.UserStatus = iUserStatus
	oUserObj.CConAlertEmails = iCConAlertEmails
	oUserObj.Notes = sNotes
	
	bSuccess = oUserObj.EditUser()
	
	if bSuccess then
		sMessage = sFirstName & " " & sLastName & " has been Edited successfully."
	else
		AlertError("The User was not Edited successfully.")		
	end if
elseif UCase(sMode) = "DELETE" then
	oUserObj.UserID = iUserID
	oUserObj.UserStatus = 3 'deleted status
	bSuccess = oUserObj.ChangeStatus()
	
	if bSuccess then
		response.redirect("UserList.asp?&page_number=" & Session("UserCurPage"))
	else
		AlertError("The User was not Deleted successfully.")		
	end if
end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Brokers" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Administrators</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">				

						<table  cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMessage %></span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<table cellpadding="2" cellspacing="2">
	<tr>
		<td>
			<a href="UserList.asp?&page_number=<%=Session("UserCurPage")%>">Return to User Listing</a>
		</td>
	</tr>
</table>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>



<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oUserObj = nothing
%>
<%
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>


<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Search.Class.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/User.Class.asp" -------------------------------------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim rs
dim sSQL
dim sClass
dim oSearchObj, oUserObj
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sAction
dim sSearchEmail, sSearchLastName, iSearchCompanyId, iSearchProviderID, iSearchBranchID, iSearchUserStatus
dim bBranchesDropDownDisabled
dim sLink

set oSearchObj = New Search
oSearchObj.ConnectionString = application("sDataSourceName")

sAction = trim(request("Action"))

if Ucase(sAction) = "NEW" then
	Session.Contents.Remove("UserSearchEmail") 
	Session.Contents.Remove("UserSearchLastName") 
	Session.Contents.Remove("UserSearchCompanyID") 
	Session.Contents.Remove("UserSearchUserStatus") 
else
	'Get Search values
	sSearchEmail = ScrubForSQL(request("SearchEmail"))
	if (sSearchEmail = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchEmail")) <> "") then
			sSearchEmail = Session("UserSearchEmail")
		end if
	else
		Session("UserSearchEmail") = sSearchEmail
	end if

	sSearchLastName = ScrubForSQL(request("SearchLastName"))
	if (sSearchLastName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchLastName")) <> "") then
			sSearchLastName = Session("UserSearchLastName")
		end if
	else
		Session("UserSearchLastName") = sSearchLastName
	end if
	
	iSearchBranchID = ScrubForSQL(request("BranchId"))
	if (iSearchBranchID = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchBranchID")) <> "") then
			iSearchBranchID = Session("UserSearchBranchID")
		end if
	else
		Session("UserSearchBranchID") = iSearchBranchID
	end if

	iSearchUserStatus = ScrubForSQL(request("UserStatus"))
	if (iSearchUserStatus = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(Session("UserSearchUserStatus")) <> "") then
			iSearchUserStatus = Session("UserSearchUserStatus")
		end if
	else
		Session("UserSearchUserStatus") = iSearchUserStatus
	end if
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("UserCurPage")) <> "") then
			iCurPage = Session("UserCurPage")
		end if
	else
		Session("UserCurPage") = iCurPage
	end if
	
	'Add values to search if not empty
	if sSearchEmail <> "" then
		oSearchObj.Email = sSearchEmail
	end if
	
	if sSearchLastName <> "" then
		oSearchObj.LastName = sSearchLastName
	end if
	
	if iSearchProviderID <> "" then
		oSearchObj.ProviderID = iSearchProviderID
	end if
	
	oSearchObj.CompanyId = session("UserCompanyId")
	
	if iSearchBranchID <> "" then
		oSearchObj.BranchId = iSearchBranchId
	end if 
	
	if iSearchUserStatus <> "" then
		oSearchObj.UserStatus = iSearchUserStatus
	end if
end if	

set rs = oSearchObj.SearchUsers()

set oSearchObj = nothing

If iCurPage = "" Then iCurPage = 1
If iMaxRecs = "" Then iMaxRecs = 20

	
%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Administrators" align="absmiddle" vspace="10"> <% = session("CompanyName") %> Administrators</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<form name="frm1" action="userlist.asp" method="POST">
								<input type="hidden" name="action" value="SEARCH">
								<input type="hidden" name="page_number" value="1">
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><input type="text" name="SearchEmail" size="20" maxlength="100" value="<% = sSearchEmail %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Last Name: </td>
											<td><input type="text" name="SearchLastName" size="20" value="<% = sSearchLastname %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="3"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Branch: </td>
											<td><% call DisplayBranchesDropdown(iSearchBranchId, session("UserCompanyId"), false, 0) %></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>User Status: </td>
											<td><% call DisplayUserStatusDropDown(iSearchUserStatus,0) %></td>
											<td></td>
										</tr>
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>

				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border="0" cellpadding="4" cellspacing="0">
<%

	dim sColor
	
	if not rs.eof then
		set oUserObj = New User
		oUserObj.ConnectionString = application("sDataSourceName")	
		
		'Set the number of records displayed on a page
		rs.PageSize = iMaxRecs
		rs.Cachesize = iMaxRecs
		iPageCount = rs.PageCount
	
		'determine which search page the user has requested.
		If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
		If clng(iCurPage) <= 0 Then iCurPage = 1
			
		'Set the beginning record to be display on the page
		rs.AbsolutePage = iCurPage		
		
		'Initialize the row/col style values
		sColor = ""
		
		iCount = 0	
		do while (iCount < rs.PageSize) AND (not rs.eof)
			if sClass = "row2" then
				sClass = "row1"
			else
				sClass = "row2"
			end if		
			
			%>
						<tr>
							<td <% = sColor %>><% = rs("UserName") %></td>
							<td align="left"<% = sColor %>>&nbsp;&nbsp;<a href="moduser.asp?userid=<% = rs("UserId") %>"><% = rs("Firstname") & " " & rs("LastName") %></a></td>
							<td <% = sColor %>><% = rs("Email") %></a></td>
						</tr>
			<%
			
			'Set the color to the opposite value
			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if
			
	
			rs.MoveNext
			iCount = iCount + 1		
		loop

		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""officerlist.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""officerlist.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")
		
		
		
		response.write("<tr>" & vbcrlf)
		response.write("<td colspan=""4"" align=""center"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""UserList.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""UserList.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)

	else

		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;There are currently no administrators that match your search criteria.</a></td>
										</tr>
		<%

	end if
%>
									</table>
								</td>
							</tr>
						</table>




								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
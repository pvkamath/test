<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Provider.Class.asp" ------------------------------------------------------>

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Provider Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oProvider

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oProvider = new Provider

oProvider.ConnectionString = application("sDataSourceName")
oProvider.VocalErrors = application("bVocalErrors") ' false
oProvider.Status = 1

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchProviderName") 
	Session.Contents.Remove("SearchProviderStateId") 
	Session.Contents.Remove("CurPage") 
else
	oProvider.Name = ScrubForSQL(request("ProviderName"))
	if (oProvider.Name = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchProviderName")) <> "") then
			oProvider.Name = session("SearchProviderName")
		end if
	else
		session("SearchProviderName") = oProvider.Name
	end if
	
	oProvider.StateId = ScrubForSQL(request("StateId"))
	if (oProvider.StateId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchProviderStateId")) <> "") then
			oProvider.StateId = session("SearchProviderStateId")
		end if
	else	
		session("SearchProviderStateId") = oProvider.StateId
	end if
	

	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if
	
end if
	
	oProvider.OwnerCompanyId = session("UserCompanyId")
	set oRs = oProvider.SearchProviders()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20
	
%>

<script language="JavaScript">
function ConfirmDelete(p_iProviderId,p_sProviderName)
{
	if (confirm("Are you sure you wish to delete the following Provider:\n  " + p_sProviderName + "\n\nAll information will be deleted."))
		window.location.href = 'deleteprovider.asp?ProviderId=' + p_iProviderId;
}
</script>


<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Course Providers" align="absmiddle" vspace="10"> Course Providers</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						

<form name="frm1" action="ProviderList.asp" method="POST">
<input type="hidden" name="action" value="SEARCH">
<input type="hidden" name="page_number" value="1">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td><b>Provider Name:</b></td>
		<td><input  type="text" name="ProviderName" value="<%= oProvider.Name %>" maxlength="100"></td>				
	</tr>
	
	<tr>
		<td><b>Provider State:</b></td>
		<td><% call DisplayStatesDropDown(oProvider.StateId,0,"StateId") 'located in functions.asp %></td>
	</tr>
	
	<tr>
		<td></td>
		<td>
			<input type="image" src="/admin/media/images/blue_bttn_search.gif">
		</td>
	</tr>
</table>
</form>

					</td>
				</tr>
				<tr>
					<td class="bckRight">

			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>



<p>
<table cellpadding="2" cellspacing="2" width="100%">
	<tr class="reportHeaderColor1">
		<td class="reportHeaderText">&nbsp; Provider Name &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Provider City &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Provider State &nbsp;</td>
		<td class="reportHeaderText">&nbsp; Delete &nbsp;</td>		
	</tr>
<%
	if not (oRs.BOF and oRs.EOF) then
	
		set oProvider = nothing
		set oProvider = new Provider
		oProvider.ConnectionString = application("sDataSourceName")
		oProvider.VocalErrors = application("bVocalErrors")
		
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		'Set the beginning record to be displayed on the page
		oRs.AbsolutePage = iCurPage
		
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
			if oProvider.LoadProviderById(oRs("ProviderID")) <> 0 then
		
				if sClass = "newrow2" then
					sClass = "newrow1"
				else
					sClass = "newrow2"
				end if

				Response.Write("<tr class=""" & sClass & """>" & vbCrLf)

				Response.Write("<td><a href=""modprovider.asp?providerid=" & oProvider.ProviderId & """>" & oProvider.Name & "</a></td>" & vbCrLf)

				Response.Write("<td>" & oProvider.City & "</td>" & vbCrLf)
				
				Response.Write("<td>" & oProvider.LookupState(oProvider.StateId) & "</td>" & vbCrLf)

				Response.Write("<td><a href=""javascript:ConfirmDelete(" & oProvider.ProviderId & ",'" & oProvider.Name & "')"">Delete</a></td>" & vbCrLf)

				Response.Write("</tr>" & vbCrLf)

			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
		
		loop		
		
		if sClass = "newrow2" then
			sClass = "newrow1"
		else
			sClass = "newrow2"
		end if		
		
		response.write("<tr class=""" & sClass & """>" & vbcrlf)
		response.write("<td colspan=""4"">" & vbcrlf)

		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""ProviderList.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""ProviderList.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if

		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)	
		response.write("</table>" & vbcrlf)
	
		'display Page number
		response.write("<table width=""100%"">" & vbcrlf)	
		response.write("<tr>" & vbcrlf)
		response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)	
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		response.write("</td>" & vbcrlf)
		response.write("</tr>" & vbcrlf)		
	else
		response.write("<tr><td colspan=""4"">There are currently no Providers that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>
</table>



						</ul>
					</td>
				</tr>
			</table>
			</td></tr></table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			<!-- space-->
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oProvider = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Provider.Class.asp" ------------------------------------------------------>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Provider Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Providers" align="absmiddle" vspace="10"> Course Providers</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Delete a Provider</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>

<%

dim iProviderId
iProviderId = ScrubForSQL(request("ProviderId"))

dim oProvider
set oProvider = new Provider
oProvider.ConnectionString = application("sDataSourceName")
oProvider.VocalErrors = application("bVocalErrors")


'Check for a valid ID.
if iProviderId <> "" then

	'We need to have an ID loaded before we can delete it.
	if oProvider.LoadProviderById(iProviderId) = 0 then
	
		'The load was unsuccessful.  Stop.
		HandleError("<b>Failed to load the passed Provider ID: " & iProviderId & "</b>")
		Response.End
		
	elseif (oProvider.OwnerCompanyId <> session("UserCompanyId")) then
	
		'Make sure the user has access to this company.
		HandleError("You do not have access to the passed Provider ID: " & iProviderId)
		response.end
		
	end if

	oProvider.Status = 3
	if oProvider.SaveProvider = 0 then
		
		'The delete failed.  
		HandleError("<b>Failed to delete the Provider.</b>")
		Response.End
		
	end if

end if

%>

<b>Successfully deleted the Provider "<% = oProvider.Name %>".</b>

<%
set oProvider = nothing
%>

<p>
<a href="ProviderList.asp">Return to Provider Listing</a>
<br>

					</td>
				</tr>
			</table>
			</td></tr></table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			<!-- space-->
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>
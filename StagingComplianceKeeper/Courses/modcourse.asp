<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------------------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Course Admin"



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim iCourseId
iCourseId = ScrubForSQL(request("CourseId"))

dim oCourse
dim saRS
dim sMode

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

if iCourseId <> "" then

	if oCourse.LoadCourseById(iCourseId) = 0 then
	
		'The load was unsuccessful.  Die.
		HandleError("Failed to load the passed Course ID: " & iCourseId)
		
	elseif (oCourse.CompanyId <> session("UserCompanyId")) then
	
		'Make sure the user has access to this course.
		HandleError("You do not have access to the passed Course ID: " & iCourseId)
		
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if
%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.Name, "Name")
			|| !checkString(FORM.Number, "Number")
			|| !checkString(FORM.StateId, "State")
			|| !checkString(FORM.Provider, "Provider")
			|| !checkString(FORM.Credits, "Credit Hours")
		   )
			return false;

		return true;
	}
</script>


<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Courses" align="absmiddle" vspace="10"> Courses</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Course</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CourseForm" action="modcourseproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the CourseID on to the next step so we know we're working with an existing
'Course.
if iCourseId <> "" then
%>
<input type="hidden" value="<% = iCourseId %>" name="CourseId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oCourse.Name %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Number: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Number" value="<% = oCourse.Number %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* State: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oCourse.StateId,1,"StateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Course Type: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayCourseTypesDropDown(oCourse.CourseTypeId, 1) %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Accreditation Type: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayAccreditationTypesDropDown(oCourse.AccreditationTypeId, 1) %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Provider: </b>
		</td>
		<td align="left" valign="top">
			<% call DisplayProvidersDropDown(oCourse.ProviderId, session("UserCompanyId"), 0, 3, 0, "") %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Credit Hours: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Credits" value="<% = oCourse.ContEdHours %>" size="10" maxlength="5">
		</td>
	</tr>
	<%
	if false then
	'We are now determining this based on the renewal date assigned for each
	'course-officer association.
	%>
	<tr>
		<td align="left" valign="top">
			<b>Renewal Date: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="ExpDate" value="<% = oCourse.ExpDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('CourseForm.ExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('CourseForm.ExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<% end if %>
	<tr>
		<td align="left" valign="top">
			<b>State Approved: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="StateApproved" value="1"<% if oCourse.StateApproved then %> CHECKED<% end if %>>
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>Non Approved: </b>
		</td>
		<td align="left" valign="top">
			<input type="checkbox" name="NonApproved" value="1"<% if oCourse.NonApproved then %> CHECKED<% end if %>>
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>Instructor Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="InstructorName" value="<% = oCourse.InstructorName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Instructor Education Level: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="InstructorEducationLevel" value="<% = oCourse.InstructorEducationLevel  %>" size="50" maxlength="100">
		</td>
	</tr>		
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>


								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


<%
set saRS = nothing
set oCourse = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------------------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Course Admin"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim iCourseId
iCourseId = ScrubForSQL(request("CourseId"))

dim oCourse
set oCourse = new Course
oCourse.VocalErrors = application("bVocalErrors")
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")


'Check for a valid CompanyID.  If we get one, we're working with an existing 
'Company so we'll load it from the database.
dim bNew 'Marker to determine if this is a creation or an update
bNew = true
if iCourseId <> "" then

	bNew = false
	
	if oCourse.LoadCourseById(iCourseId) = 0 then
	
		'The load was unsuccessful.  Die.
		HandleError("Failed to load the passed Course ID: " & iCourseId)
		
	elseif (oCourse.CompanyId <> session("UserCompanyId")) then
	
		'Make sure the user has access to this company.
		HandleError("You do not have access to the passed Course ID: " & iCourseId)
		
	end if

end if


'Assign the Course properties based on our passed parameters
oCourse.CompanyId = session("UserCompanyId")
oCourse.StateId = ScrubForSql(request("StateId"))
oCourse.Name = ScrubForSQL(request("Name"))
oCourse.Number = ScrubForSQL(request("Number"))
oCourse.CourseTypeId = ScrubForSQL(request("CourseType"))
oCourse.AccreditationTypeID = ScrubForSQL(request("AccreditationType"))
oCourse.ProviderId = ScrubForSQL(request("Provider"))
oCourse.ContEdHours = ScrubForSQL(request("Credits"))
oCourse.ExpDate = ScrubForSQL(request("ExpDate"))
if request("StateApproved") = "1" then
	oCourse.StateApproved = true
else
	oCourse.StateApproved = false
end if
if request("NonApproved") = "1" then
	oCourse.NonApproved = true
else
	oCourse.NonApproved = false
end if
oCourse.InstructorName = ScrubForSQL(request("InstructorName"))
oCourse.InstructorEducationLevel = ScrubForSQL(request("InstructorEducationLevel"))

'If this is a new Company we need to save it before it will have a unique ID.
dim iNewCourseId
if oCourse.CourseId = "" then

	iNewCourseId = oCourse.SaveCourse

	if iNewCourseId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Course.  Stop.
		HandleError("Error: Failed to save new Course.")

	end if
	
else

	iNewCourseId = oCourse.SaveCourse

	if iNewCourseId = 0 then

		'The save failed.  Die.
		HandleError("Error: Failed to save existing Course.")

	elseif CINT(iNewCourseId) <> CINT(iCourseId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our object.
		HandleError("Error: Unexpected results while saving existing Course.")
		
	end if

end if

%>


<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Courses" align="absmiddle" vspace="10"> Courses</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% if bNew then %>Add<% else %>Edit<% end if %> a Course</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if bNew then
%>
<b>The new Course "<% = oCourse.Name %>" was successfully created.</b>
<%
else
%>
<b>The Course "<% = oCourse.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="CourseList.asp">Return to Course Listing</a>
<br>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oCourse = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Provider.Class.asp" ------------------------------------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Provider Admin"



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "COMPANY"



' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



dim iProviderId
iProviderId = ScrubForSQL(request("ProviderId"))

dim oProvider
dim saRS
dim sMode

set oProvider = new Provider
oProvider.ConnectionString = application("sDataSourceName")
oProvider.VocalErrors = application("bVocalErrors")

if iProviderId <> "" then

	if oProvider.LoadProviderById(iProviderId) = 0 then
	
		'The load was unsuccessful.
		HandleError("Failed to load the passed Provider ID: " & iProviderId)
		
	elseif (oProvider.OwnerCompanyId <> session("UserCompanyId")) then
	
		'Make sure the user has access to this Provider.
		HandleError("You do not have access to the passed Provider ID: " & iProviderId)
		
	end if
	
	sMode = "Edit"
else
	sMode = "Add"
end if

%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.Name, "Name")
			|| !checkString(FORM.Address, "Address")
			|| !checkString(FORM.City, "City")
			|| !checkZIPCode(FORM.Zipcode, 0)
		   )
			return false;

		return true;
	}
</script>


<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="Providers" align="absmiddle" vspace="10"> Course Providers</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
					
			<table cellpadding="10" cellspacing="0" border="0" width="100%">
				<tr>
					<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Provider</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="ProviderForm" action="modProviderproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the ProviderID on to the next step so we know we're working with an existing
'Provider.
if iProviderId <> "" then
%>
<input type="hidden" value="<% = iProviderId %>" name="ProviderId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oProvider.Name %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Address: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Address" value="<% = oProvider.Address %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Address Line 2: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Address2" value="<% = oProvider.Address2 %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* City: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="City" value="<% = oProvider.City %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>		
		<td><b>* State:</b></td>
		<td>
			<% call DisplayStatesDropDown(oProvider.StateId,1,"StateId") 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>* Zipcode: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Zipcode" value="<% = oProvider.Zipcode %>" size="12" maxlength="15">
		</td>
	</tr>	
	<tr>
		<td align="left" valign="top">
			<b>Phone: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Phone" value="<% = oProvider.Phone %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Phone Ext: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt" value="<% = oProvider.PhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Fax: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Fax" value="<% = oProvider.Fax %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Website: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Website" value="<% = oProvider.Website %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Contact Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="ContactName" value="<% = oProvider.ContactName %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Contact Title: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="ContactTitle" value="<% = oProvider.ContactTitle %>" size="50" maxlength="50">
		</td>
	</tr>	

	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>


								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->


<%
set saRS = nothing
set oProvider = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage		
%>
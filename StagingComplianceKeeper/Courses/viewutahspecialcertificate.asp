<% 'option explicit %>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 

<%

dim sPgeTitle								' Template Variable
	
' Set Page Title:
sPgeTitle = "Compliance Management Solutions"
	

'Verify that the user has logged in
CheckIsLoggedIn()	
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
'Declare and initialize the company object; make sure we were passed a company
'ID, and load the company object
dim oCompany
dim iCompanyId
iCompanyId = session("UserCompanyId")

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(iCompanyId) = 0 then

	Response.Write("Error: A valid Company ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if


'Declare and initialize the associate object; make sure we were passed a valid
'AssociateId
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
iAssociateId = ScrubForSql(request("id"))

if oAssociate.LoadAssociateById(iAssociateID) = 0 then

	AlertError("This page requires a valid Officer ID.")
	Response.End

end if 


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


dim iCourseID 'as integer
'dim oCourseObj, oUserObj 'as object
dim rs 'as object
dim sBackgroundImage, sStampImage, sSignatureImage 'as string
dim sText, sTextBlock1, sTextBlock2 'as string
dim sSignerName, sSignerPosition 'as string
dim iCertificateID 'as integer
dim oPDF 'as object
dim oDoc 'as object
dim oPage 'as object
dim oDest 'as object
dim oTimesFont, oTimesFontBold 'as object
dim oCanvas 'as object
dim oImage 'as object
dim oParam 'as object
dim oTable 'as object
dim sTempText 'as string
dim sUserText 'as string
dim bCertificateReleased 'as boolean
dim iUserCourseID 'as integer
dim iUserID 'as integer
dim iTpUserId 'as integer
dim sFullName 'as string
dim sSsn 'as string
dim sCompanyName 'as string
dim sCompanyNameOnCert 'as string
dim sCompletionDate 'as string
dim sCertificateExpirationDate 'as string
dim bSuccess 'as boolean
dim iXCord, iYCord 'as integer
dim iStateID 'as integer
dim sLicenseNo 'as string
dim sCourseName 'as string
dim bShowExpiration 'as boolean



'Declare and initialize the course object; make sure we can load this as a
'valid course
iUserCourseId = ScrubForSql(request("UserCourseId"))
iUserId = iAssociateId
iTpUserId = oAssociate.LookupTpUserId(oAssociate.Email)
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")



'Assign the user info
sFullName = oAssociate.FullName
sSsn = enDeCrypt(trim(oAssociate.Ssn),application("RC4Pass"))

'format social if necessary
if cint(len(sSSN)) = 9 then
	sSSN = left(sSSN,3) & "-" & mid(sSSN,4,2) & "-" & mid(sSSN,6)
end if	

sCompanyName = oCompany.Name
sCompanyNameOnCert = ""
sUserText = sName
if trim(sCompanyNameOnCert) <> "" then
	sUserText = sUserText & " of " & sCompanyNameOnCert
elseif trim(sCompanyName) <> "" then
	sUserText = sUserText & " of " & sCompanyName
end if
sLicenseNo = ""


'Get the course info
set oRs = oAssociate.GetCourse(iUserCourseId)

iCourseId = oRs("CourseId")
sCompletionDate = oRs("CompletionDate")
sCertificateExpirationDate = oRs("CertificateExpirationDate")
bCertificateReleased = oRs("CertificateReleased")


'Make sure the certificate has been released
if not bCertificateReleased then
	HandleError("The Certificate has not yet been released.")
end if


'Load the course object
if oCourse.LoadCourseById(iCourseId) = 0 then
	HandleError("A valid Course ID is required to load this page.")
end if


'Get the State ID of the course
sCourseName = oCourse.Name
iStateId = oCourse.StateId


'Get the certificate info
'set oRs = oCourse.GetCertificate
'if not oRs.State = 0 then
'	if not oRs.EOF then
'		iCertificateId = oRs("CertificateId")
'		sTextBlock1 = trim(oRs("TextBlock1"))
'		sTextBlock2 = trim(oRs("TextBlock2"))
'		sSignerName = trim(oRs("SignerName"))
'		sSignerPosition = trim(oRs("SignerPosition"))
'		sBackgroundImage = trim(oRs("BackgroundImage"))
'		sStampImage = trim(oRs("StampImage"))
'		sSignatureImage = trim(oRs("SignatureImage"))
'		bShowExpiration = oRs("ShowExpiration")
'	else
'		HandleError("The Certificate could not be retrieved.")
'	end if
'else
'	HandleError("The Certificate could not be retrieved.")
'end if


set oRs = nothing
set oCourse = nothing
set oAssociate = nothing
set oCompany = nothing




arrX = Array(45, 375, 150, 155, 150, 225, 230, 225, 119, 260, 260)
arrY = Array(650, 650, 538, 505, 472, 538, 505, 472, 453, 536, 503)
arrText = Array(sFullName, sSSN, 16, 4, 20, 16, 4, 20, "x", formatdatetime(sCompletionDate,2), formatdatetime(sCompletionDate,2))
arrFont = Array(1,1,1,1,1,1,1,1,1,2, 2)

Set oPDF = Server.CreateObject("Persits.Pdf")
oPDF.RegKey = "IWez17RlvL6PIQc213KacCXF1eD2xzmp+C8FGLDD04ptU96y5Mi8KtfWC9XKP4Nl6qpjPqlOSwG3"

' Open blank PDF form from file
Set oDoc = oPDF.OpenDocument(application("sAbsTpWebroot") & "media\documents\Utah20Pre_Cert.pdf" )

Set oFont = oDoc.Fonts("Helvetica-Bold")

' Obtain the only page's canvas
Set oCanvas = oDoc.Pages(1).Canvas
Set oParam = oPDF.CreateParam

' Go over all items in arrays
For i = 0 to UBound(arrX)
	oParam("x") = arrX(i)
	oParam("y") = arrY(i)

	' Draw text on canvas
	if cint(arrFont(i)) = 1 then
		oParam("size") = 10
	else
		oParam("size") = 7
	end if
	
	oCanvas.DrawText arrText(i), oParam, oFont	
Next 

'School Stamp
Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & "UtahSchoolStamp.jpg" )
oParam("x") = 325
oParam("y") = 435
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oParam("expand") = true
oCanvas.DrawImage oImage, oParam

'Signature Image
Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & "signature_blue.jpg" )
oParam("x") = 315
oParam("y") = 557
oParam("ScaleX") = .40
oParam("ScaleY") = .40
oParam("expand") = true
oCanvas.DrawImage oImage, oParam

' Send directly to browser, do not save a temporary copy on disk
oDoc.SaveHttp "attachment;filename=certificate.pdf"

set oDoc = nothing
set oPDF = nothing
set oFont = nothing
set oImage = nothing
%>

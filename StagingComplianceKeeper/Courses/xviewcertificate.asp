<% 'option explicit %>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<%

' Template Constants -- Modify as needed per each page:
const USES_FORM_VALIDATION = False			' Template Constant
const SHOW_PAGE_BACKGROUND = True			' Template Constant
const TRIM_PAGE_MARGINS = True				' Template Constant
const SHOW_MENUS = False					' Template Constant
const MENU_NUMBER = 0
const SIDE_MENU_SELECTED = ""				' Template Constant
dim sPgeTitle								' Template Variable
	
' Set Page Title:
sPgeTitle = "Compliance Management Solutions"
	

'Verify that the user has logged in
CheckIsLoggedIn()	
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
'Declare and initialize the company object; make sure we were passed a company
'ID, and load the company object
dim oCompany
dim iCompanyId
iCompanyId = session("UserCompanyId")

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(iCompanyId) = 0 then

	Response.Write("Error: A valid Company ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if


'Declare and initialize the associate object; make sure we were passed a valid
'AssociateId
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
iAssociateId = ScrubForSql(request("id"))

if oAssociate.LoadAssociateById(iAssociateID) = 0 then

	AlertError("This page requires a valid Officer ID.")
	Response.End

end if 


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>

<%

dim iCourseID 'as integer
'dim oCourseObj, oUserObj 'as object
dim rs 'as object
dim sBackgroundImage, sStampImage, sSignatureImage 'as string
dim sText, sTextBlock1, sTextBlock2 'as string
dim sSignerName, sSignerPosition 'as string
dim iCertificateID 'as integer
dim oPDF 'as object
dim oDoc 'as object
dim oPage 'as object
dim oDest 'as object
dim oTimesFont, oTimesFontBold 'as object
dim oCanvas 'as object
dim oImage 'as object
dim oParam 'as object
dim oTable 'as object
dim sTempText 'as string
dim sUserText 'as string
dim bCertificateReleased 'as boolean
dim iUserCourseID 'as integer
dim iUserID 'as integer
dim sName 'as string
dim sCompanyName 'as string
dim sCompanyNameOnCert 'as string
dim sCompletionDate 'as string
dim sCertificateExpirationDate 'as string
dim bSuccess 'as boolean
dim iXCord, iYCord 'as integer
dim iStateID 'as integer
dim sLicenseNo 'as string
dim sCourseName 'as string
dim bShowExpiration 'as boolean



'Declare and initialize the course object; make sure we can load this as a
'valid course
iUserCourseId = ScrubForSql(request("UserCourseId"))
iUserId = iAssociateId
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")



'Assign the user info
sName = oAssociate.FullName
sCompanyName = oCompany.Name
sCompanyNameOnCert = ""
sUserText = sName
if trim(sCompanyNameOnCert) <> "" then
	sUserText = sUserText & " of " & sCompanyNameOnCert
elseif trim(sCompanyName) <> "" then
	sUserText = sUserText & " of " & sCompanyName
end if
sLicenseNo = ""


'Get the course info
set oRs = oAssociate.GetCourse(iUserCourseId)

iCourseId = oRs("CourseId")
sCompletionDate = oRs("CompletionDate")
sCertificateExpirationDate = oRs("CertificateExpirationDate")
bCertificateReleased = oRs("CertificateReleased")


'Special redirect for Utah certificates
if clng(iCourseId) = 283 then
	HandleError("Please contact TrainingPro at 877-878-3600 for Utah State required certificate.")
end if


'Make sure the certificate has been released
if not bCertificateReleased then
	HandleError("The Certificate has not yet been released.")
end if


'Load the course object
if oCourse.LoadCourseById(iCourseId) = 0 then
	HandleError("A valid Course ID is required to load this page.")
end if


'Get the State ID of the course
sCourseName = oCourse.Name
iStateId = oCourse.StateId


'Get the certificate info
set oRs = oCourse.GetCertificate

if not oRs.State = 0 then

	if not oRs.EOF then
	
		iCertificateId = oRs("CertificateId")
		sTextBlock1 = trim(oRs("TextBlock1"))
		sTextBlock2 = trim(oRs("TextBlock2"))
		sSignerName = trim(oRs("SignerName"))
		sSignerPosition = trim(oRs("SignerPosition"))
		sBackgroundImage = trim(oRs("BackgroundImage"))
		sStampImage = trim(oRs("StampImage"))
		sSignatureImage = trim(oRs("SignatureImage"))
		bShowExpiration = oRs("ShowExpiration")
	
	else
	
		HandleError("The Certificate could not be retrieved.")
		
	end if
	
else
	
	HandleError("The Certificate could not be retrieved.")
	
end if


set oRs = nothing
set oCourse = nothing
set oAssociate = nothing
set oCompany = nothing


'Create PDF
Set oPDF = Server.CreateObject("Persits.PDFManager")
Set oDoc = oPDF.CreateDocument

'Make the Doc Print Only
oDoc.Encrypt "", "", 40, pdfFull And (Not pdfModify) And (Not pdfCopy) And (Not pdfExtract) And (Not pdfAssemble) And (not pdfAnnotations) And (not pdfForm)

Set oPage = oDoc.Pages.Add(792,612)

Set oDest = oDoc.CreateDest(oPage, "Fit=True")
oDoc.OpenAction = oDest

Set oTimesFont = oDoc.Fonts("Times-Roman")
Set oTimesFontBold = oDoc.Fonts("Times-Bold")
set oCanvas = oPage.Canvas

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_logo.gif") )
Set oParam = oPdf.CreateParam

oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 525
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_header_1.gif") )
oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 485
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam


if sBackgroundImage <> "" then
	Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & sBackgroundImage )
	oParam("x") = (oPage.Width - oImage.Width) / 2
	oParam("y") = (477 - 275) + ((275 - oImage.Height) / 2)
	oParam("ScaleX") = oImage.ResolutionX / 72
	oParam("ScaleY") = oImage.ResolutionY / 72
	oPage.Background.DrawImage oImage, oParam
end if

set oTable = oDoc.CreateTable("rows=3; cols=2; cellspacing=1; cellpadding=2; height=465; width=680; border=1;bordercolor=white;")
oTable.Font = oTimesFont

With oTable.Rows(1)
	.Height = "275"
	.Cells(1).ColSpan = 2
	.Cells(1).AddText sTextBlock1 & " <b>" & sUserText & "</b> " & FormatTextForHTML(sTextBlock2), "alignment=left;color=black;size=14;html=true;"
end with

With oTable.Rows(2)
	.Height = "120"
	.Cells(1).width = 400
	.Cells(2).width = 280		

if sStampImage <> "" then
	Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & sStampimage )
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(1).Canvas.DrawImage oImage, oParam
else
	.Cells(1).AddText "", "alignment=left;color=black;html=true;"
end if

if sSignatureImage <> "" then
	'Response.Write(Application("sAbsCertificateImageFolder") & sSignatureImage)
	Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & sSignatureImage )
else
	Set oImage = oDoc.OpenImage( Application("sAbsCertificateImageFolder") & Application("DefaultSignatureImage") )
end if
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(2).Canvas.DrawImage oImage, oParam
end with

With oTable.Rows(3)
	.Height = "70"
	.Cells(1).width = 400	
	.Cells(2).width = 280		

	sText = "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br>"
	
	'Utah 14hr course
	if (cint(iStateID) = 45) and (cint(instr(1,sCourseName,"14 hr",1) > 0) or cint(instr(1,sCourseName,"14 hour",1) > 0))  and trim(sLicenseNo) <> ""  then
		sText = sText & "<b>License Number: " & sLicenseNo & "</b><br>"
	end if
	
	sText = sText & "<b>" & "Authentication Number: 1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid  & "</b>"
	
	'Display Expiration text
	if bShowExpiration then
		if cint(iStateID) = 38 then 'Special code for Oregon
			sText = sText & "<br><b>This certification is valid for the two (2) year period following<br>your last License Renewal</b>"
		else
			sText = sText & "<br><b>This certification is valid for Renewal Period Ending: " & formatdatetime(sCertificateExpirationDate,2) & "</b>"
		end if
	end if

	.Cells(1).AddText sText, "alignment=right;color=black;size=12;html=true;"
	.Cells(2).AddText "<font color=""#295385"" style=""font-size:16pt;"" face=""Verdana, Arial, helvetica""><b>" & sSignerName & "</b></font><br><b>" & sSignerPosition & "</b>", "alignment=right;color=black;size=12;html=true;"
end with

oCanvas.DrawTable oTable, "x=54;y=480"

'check if expired
'if (datediff("d",date(),sCertificateExpirationDate) < 0) then
'	for iYCord = 570 to 0 step -120
'		for iXCord = 50 to 750 step 200
'			oPage.Background.DrawText "EXPIRED", "x=" & iXCord & "; y=" & iYCord & "; size=20; angle=0; color=red;", oTimesFont
'		next
'	next
'end if

'oCanvas.DrawText sTextBlock1 & " <b>First Name Last Name of Company</b> " & FormatTextForHTML(sTextBlock2), "x=54; y=450; html=true; width=684; size=14; alignment=left; rendering=0", oTimesFont

'oCanvas.DrawText "Issue Date: " & formatdatetime(date(),2), "x=54; y=350; size=12; alignment=left; rendering=0", oTimesFont

' Send directly to browser, do not save a temporary copy on disk
oDoc.SaveHttp "attachment;filename=certificate.pdf"

set oTable = nothing
set oPDF = nothing
set oDoc = nothing
set oPage = nothing
set oDest = nothing
set oTimesFont = nothing
Set oTimesFontBold = nothing
set oImage = nothing
%>

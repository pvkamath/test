<%
server.scripttimeout = 100000

set objConn1 = server.CreateObject("ADODB.Connection")
objConn1.ConnectionString = application("sDataSourceName")
objConn1.Open

sSQL = "SELECT * FROM FEED ORDER BY LName"

set rs = objconn1.execute(sSQL)
iCount = 0
do while not rs.eof
	bFound = false
	iLOID = trim(rs("LOID"))
	sFirstName = replace(trim(rs("FName")),"'","''")
	sLastName = replace(trim(rs("LName")),"'","''")
	sSSN = trim(rs("ssn"))
	
	if (cint(len(sSSN)) = 8) then
		sSSN = "0" & sSSN
	end if 
	
	'check to see if the user can be matched by SSN
	if (cint(len(sSSN)) = 9) then
		'put SSN in proper format
		sNewSSN = left(sSSN,3) & "-" & mid(sSSN,4,2) & "-" & right(sSSN,4)
		
		sSQL = "SELECT UserID,FirstName,LastName FROM TP_Users WHERE SSN = '" & sNewSSN & "' "

		set rs2 = objconn1.execute(sSQL)
		if not rs2.eof then
			bFound = true
		end if
	end if
	
	if bFound then
		response.write("FEED - " & sFirstName & " " & sLastName & " " & sSSN & "<br>")
		response.write("DB(SSN) - " & rs2("FirstName") & " " & rs2("LastName") & " " & rs2("UserID") & "<HR>")
		sSQL = "UPDATE TP_Users " & _
			   "SET LoanOfficerID = '" & iLOID & "' " & _
			   "WHERE UserID = '" & trim(rs2("UserID")) & "' "
		objConn1.execute(sSQL)		
		if err.number = 0 then
			iCount = iCount + 1
		else
			response.write(err.description)
			response.end
		end if			
	else
		'try to match by first and lastname
		sSQL = "SELECT UserID,FirstName,LastName FROM TP_Users WHERE Firstname = '" & sFirstName & "' and lastname = '" & sLastName & "' "

		set rs2 = objconn1.execute(sSQL)
		if not rs2.eof then
			bFound = true
		end if
		
		if bFound then
			response.write("FEED - " & sFirstName & " " & sLastName & " " & sSSN & "<br>")
			response.write("DB(First,Last) - " & rs2("FirstName") & " " & rs2("LastName") & " " & rs2("UserID") & "<HR>")
			sSQL = "UPDATE TP_Users " & _
				   "SET LoanOfficerID = '" & iLOID & "' " & _
				   "WHERE UserID = '" & trim(rs2("UserID")) & "' "
			objConn1.execute(sSQL)			
			if err.number = 0 then
				iCount = iCount + 1
			else
				response.write(err.description)
				response.end
			end if
		end if
	end if
	
	
	rs.MoveNext
loop
response.write(iCount)
set rs2 = nothing
set rs = nothing
set objConn1 = nothing
set objConn2 = nothing
%>
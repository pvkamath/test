<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	dim sSQL
	dim objConn
	dim rs
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Error"

	if session("User_ID") = "" then
		iPage = 1
	elseif session("Access_Level") = 4 then
		iPage = 3
	else
		iPage = 0
	end if
	
	set objConn = New HandyADO
	
	sEmail = ScrubForSQL(request("Email"))

	sSQL = "SELECT Password FROM Users WHERE Email = '" & sEmail & "' "
	set rs = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

	if not rs.eof then
		dim objJMail
		dim sPassword
		
		sPassword = rs("Password")
		
		'Create an instance of the Jmail object and set it's initial values
		Set objJMail = server.CreateObject("JMail.Message")
		objJMail.ContentType = "text/html"
		objJmail.Body = "<u>TrainingPro Login Information</u><br><br>" & _
						"<b>Username:</b> " & sEmail & "<br>" & _
						"<b>Password:</b> " & sPassword & "<br><br>" & _
						"<a href=""" & application("URL") & """>Click here to go to the site</a>"
						
		objJmail.AddRecipient sEmail
		objJmail.From = application("CustServiceEmail")
		objJmail.FromName = "TrainingPro"		
		objJMail.Subject = "TrainingPro - Login Information"
		
		'mail it
		objJmail.Send("mail.xigroup.com")
		objJmail.Close	
	
		'if true, no errors occurred while sending
		if objJMail.ErrorCode = 0 then
			sMessage = "Your password was sent successfully to your email address."
		else
			sMessage = "The Confirmation email was not sent successfully.<br><br><a class=""newstitle"" href=""javascript:history.back()"">Click here to try again</a>"	
		end if		
		
		set objJMail = nothing
	else
		sMessage = "Your email address could not be found in the system.<br><br><a class=""newstitle"" href=""javascript:history.back()"">Click here to try again</a>"
	end if
	
	set rs = nothing
	set objConn = nothing
	
	'set current date
	sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td>
												<%= sMessage %>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/Search.Class.asp" ------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Library"
	
	iPage = 1
	
	sKeywords = getFormElement("keywords")
	
	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/Library.asp?Keywords=" & sKeywords)
	end if	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
dim oSearchObj 'as object
dim rs 'as object

set oSearchObj = New Search
oSearchObj.ConnectionString = application("sDataSourceName")

'Get Courses
oSearchObj.AddCourseType(1) 'Online
oSearchObj.AddCourseType(2) 'HomeStudy
oSearchObj.CurrentCourses = true
oSearchObj.Purchasable = 1

set rs = oSearchObj.SearchCourses()

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td>
												<span class="date"><%=sCurrentDate%></span>
											</td>
										</tr>
										<tr>
											<td><span class="pagetitle">Library</span>
										</tr>
										<tr>
											<td>											
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
<%
do while not rs.eof
	if sBgrColor = "" then
		sBgrColor = "#EAE3D3"
	else
		sBgrColor = ""
	end if	
%>
													<tr bgcolor="<%=sBgrColor%>">
														<td><a class="newsTitle" href="LibraryDescription.asp?CourseID=<%=rs("CourseID")%>"><%= rs("Name") %></a></td>
													</tr>
<%	
	rs.MoveNext
loop
%>													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

set oSearchObj = nothing
set rs = nothing
%>

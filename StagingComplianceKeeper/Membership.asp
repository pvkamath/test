<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/includes/User.Class.asp" --------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable

	' Set Page Title:
	sPgeTitle = "TrainingPro - Membership"
	sKeywords = "Membership"  ' getFormElement("keywords")

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/Membership.asp")
	end if	

	setGlobalContentData(sKeywords)
	
	if session("User_ID") = "" then
		Response.Redirect("default.asp")
	else
		iPageId = 0
	end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
dim oUser 'as object

set oUserObj = new User

oUserObj.ConnectionString = application("sDataSourceName")

'determine if the user has courses
bHasCourses = oUserObj.HasCourses(Session("User_ID"))

set oUserObj = nothing

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle"><% = g_sHeadline%></span>
												
												
												<p><% = g_sText%></td>
										</tr>
										<tr>
											<td>
											<% if bHasCourses then %>
												<form name="frm1" method="POST" action="MembershipProc.asp">
												<input type="checkbox" name="subscribe" CHECKED> <B>Yes, I would like to become a Member!</B>												
												<p>
												<input type="image" src="<% = application("sDynMediaPath") %>bttnContinue.gif">
												</form>				
											<% else %>
												<b>You can Sign Up for the program once you have purchased a course.</b>
											<% end if %>																			
											</td>
										</tr>
										
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

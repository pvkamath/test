<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual = "/includes/User.Class.asp" --------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro"
	
	iPage = 1
	
	if session("User_ID") = "" then
		Response.Redirect("default.asp")
	else
		iPage = 0
	end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle">Membership Programs</span>
<%

dim oRs
dim oUser
set oUser = new User

dim iUserID
dim bMembership

dim sMessage
dim bSuccess

bSuccess = true

oUser.ConnectionString = application("sDataSourceName")

if (Ucase(trim(request("subscribe"))) = "ON") then
	bMembership = true
	sMemberNumber = "TPM" & session("User_ID")
else
	bMembership = false
	sMemberNumber = ""	
end if

iUserID = session("User_ID")

bSuccess = oUser.ChangeMembership(iUserID, bMembership, sMemberNumber)

if not bSuccess then
	sMessage = "There was a problem updating your account.  Please try again at another time."
else
	if bMembership then
		sMessage = "You have successfully been registered into the TraingPro Membership Program."
	else
		sMessage = "You have successfully been unregistered from the TraingPro Membership Program."
	end if
end if 
	
'send email to admin
call EmailConfirmation(iUserID,bMembership)
%>

<p>
<b><% = sMessage %></b>
<p>

<% if bError then %>
<a href="javascript:history.back()"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" alt="Go Back" border="0"></a>
<% else %>
<a href="userPage.asp"><img src="<% = application("sDynMediaPath") %>bttnContinue.gif" alt="Proceed to Your User Page" border="0"></a>
<% end if %>

<!-- Page Extension -->
<br>
<img src="<% = application("sDynMediaPath") %>clear.gif" width="1" height="150" alt="" border="0">

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
				
					</td>					


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
sub EmailConfirmation(p_iUserID,p_bMembership)
	dim strMailServerAddress	'The SMTP server that Jmail will use
	dim strSuccessURL		'The page to redirect the user to after successfully sending mail.
						'Will be set when the email is built
	' Jmail values
	dim objJmail
	dim sBody
	dim sFullname
	dim oCourseObj
	dim sRecipient
	dim rs
	
	strMailServerAddress		= "mail.xigroup.com:25"
	
	'Create an instance of the Jmail object and set it's initial values
	Set objJMail = server.CreateObject("JMail.Message")
	objJMail.ContentType = "text/html"
		
	'Cycle through each field in the form, pulling out required values, and appending 
	'additional field names and values to the append variable
		
	dim intTotalFields 	'Total number of form fields
	dim i 				'counter variable
	dim strFieldName	'Form Field Name

	dim oUser
	set oUser = New User
	oUser.ConnectionString = application("sDataSourceName")
	oUser.UserID = p_iUserID
		
	set rs = oUser.GetUser()

	if trim(rs("MidInitial")) <> "" then
		sFullName = rs("FirstName") & " " & rs("MidInitial") & ". " & rs("LastName")
	else
		sFullName = rs("FirstName") & " " & rs("LastName")
	end if			
	
	sMemberNumber = rs("MemberNumber")
	
	set oUser = nothing
	set rs = nothing
		
	'Construct Body
	if p_bMembership then
		sBody = "<u>The following user has been registered into the TraingPro Membership Program.:</u><br><br>" 	
	
		sBody = sBody & "<b>Name:</b> " & sFullName & "<br>" & _
					    "<b>Member Number:</b> " & sMemberNumber		
	else
		sBody = "<u>The following user has been unregistered from the TraingPro Membership Program.:</u><br><br>" 	
		
		sBody = sBody & "Name: " & sFullName		
	end if
	
	objJmail.Body = sBody

	'set the Jmail values with the information collected
	objJmail.AddRecipient application("MembershipEmail")
	objJmail.From = "membership@trainingpro.com"
	objJmail.FromName = "TrainingPro"
	objJmail.Subject = "Memberships"
	
	'mail it
	objJmail.Send(strMailServerAddress)
	objJmail.Close	
	
	'if true, no errors occurred while sending
	if objJMail.ErrorCode <> 0 then
		response.redirect("/error.asp?message=The Confirmation email was not sent successfully.")
	end if
	
	set objJmail = nothing
end sub
%>

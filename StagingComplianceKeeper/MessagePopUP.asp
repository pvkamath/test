<%
dim sStylesheet 'as string

'set stylesheet
if Session("browser") = "IE" then
	sStylesheet = Application("sDefaultIEStyle")
else
	sStylesheet = Application("sDefaultNSStyle")
end if
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>TrainingPro - Message</title>
	<link rel="stylesheet" type="text/css" href="<%= Application("sDynWebroot")%>includes/<%= sStylesheet %>">	
</head>

<body bgcolor="#EAE3D3">

<table align="center" cellpadding="4">
	<tr>
		<td>
			<b>Attention TrainingPro Students!!!</b><br><br>

 			TrainingPro is proud to announce the introduction of our newly redesigned website!  If you have registered for a training course prior to November 22, 2004 or are in the process of completing a training course that was ordered prior to November 22, 2004, please click the link below or visit the following URL: <a href="http://aes.trainingpro.com" class="newsTitle" target=_blank>http://aes.trainingpro.com</a>.  Once you have completed your old training courses, these records will be imported to the new site.<br><br>
			
			Our New Site will continue to provide the same high level of service with new enhancements and features that will boost your educational experience!  If you have any questions please e-mail <A class=newsTitle href="mailto:support@trainingpro.com">support@trainingpro.com</A>.
		</td>
	</tr>
	<tr>
		<td align="center">
			<br>
			<a href="javascript:window.close()" class="newsTitle">Close Window</a>
		</td>
	</tr>
</table>

</body>
</html>

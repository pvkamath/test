<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/User.Class.asp" ------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Notes"
	
	iPage = 0
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function Validate(FORM)
		{
			var sText;
			
			if (!checkString(FORM.NoteTitle,"Title"))
				return false;
			
			//make sure the field is not empty
			FORM.Text.value = idContent.document.body.innerHTML;
			
			if (FORM.Text.value == "") 
			{
				alert("Note is a required Field!");
				//frm.Text.focus();
				return false;
			}
			
			//Make sure the Text value is not over 100,000 chars, if gets far over this limit it will cause a stack overflow
			sText = FORM.Text.value;
	
			if (sText.length > 5000)
			{
				alert("The Note has " + sText.length + " chars.\nThe maximum length is 5,000 chars.");
				return false;
			}

			return true;		
		}
//--></script>
<%
CheckIsLoggedIn()

dim iUserID 'as integer
dim sCurrentDate 'as string
dim rs 'as object
dim oUserObj 'as object
dim iNoteID 'as integer
dim sMode 'as string
dim sText 'as string
dim sNoteTitle 'as string

iUserID = session("User_ID")
iNoteID = getFormElement("NoteID")

set oUserObj = new User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID

'set mode
if (trim(iNoteID) <> "") then
	sMode = "EDIT"
	sTitle = "Edit Note"
	set rs = oUserObj.GetNote(iNoteID)

	if not rs.eof then
		sNoteTitle = rs("Title")
		sText = rs("Text")
	else
		set rs = nothing
		set oUserObj = nothing
		response.redirect("/error.asp?message=The Note could not be retrieved.")
	end if
else
	sMode = "ADD"
	sTitle = "Add Note"
end if

set rs = nothing
set oUserObj = nothing

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>	
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle"><%= sTitle %></span>
										</tr>
										<tr>
											<td>
												<form name="frm1" action="/ModNoteProc.asp" method="POST" onSubmit="return Validate(this)">
												<input type="hidden" name="NoteID" value="<%=iNoteID%>">												
												<input type="hidden" name="mode" value="<%=sMode%>">
												<table cellpadding="4">
													<tr>
														<td colspan="2">
															<span class="formstar">*</span> Required Fields
														</td>
													</tr>
													<tr>
														<td><b>Title:</b><span class="formstar">*</span></td>
														<td><input type="text" name="NoteTitle" value="<% = sNoteTitle %>" size="30" maxlength="100"></td>				
													</tr>													
													<tr>
														<td><b>Text:</b><span class="formstar">*</span></td>
														<td><input type="hidden" name="Text"><!-- #include virtual="/admin/TextFX1.0/NoteFormat.asp"--></td>				
													</tr>
													<tr>
														<td colspan="2" align="center">
															<input type="image" src="<% = application("sDynMediaPath") %>bttnSubmit.gif" border=0>&nbsp;&nbsp;
															<img src="<% = application("sDynMediaPath") %>bttnCancel.gif" border=0 onClick="history.back()">
														</td>
													</tr>													
												</table>
												</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/User.Class.asp" ------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro"
	iPage = 0
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
CheckIsLoggedIn()

dim iUserID 'as integer
dim sCurrentDate 'as string
dim rs 'as object
dim oUserObj 'as object
dim iNoteID 'as integer
dim sMode 'as string
dim sNoteTitle 'as string
dim sText 'as string
dim sTitle 'as string
dim sAction 'as string

iUserID = session("User_ID")
iNoteID = getFormElement("NoteID")
sNoteTitle = getFormElement("NoteTitle")
sText = getFormElement("text")
sMode = getFormElement("Mode")

set oUserObj = new User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID

if Ucase(sMode) = "ADD" then
	sTitle = "Add Note"
	sAction = "Added"
	bSuccess = oUserObj.AddNote(sNoteTitle,sText)
elseif Ucase(sMode) = "EDIT" then
	sTitle = "Edit Note"
	sAction = "Edited"
	bSuccess = oUserObj.EditNote(iNoteID,sNoteTitle,sText)	
elseif Ucase(sMode) = "DELETE" then
	sTitle = "Delete Note"
	sAction = "Deleted"
	bSuccess = oUserObj.DeleteNote(iNoteID)	
end if

if not bSuccess then
	set oUserObj = nothing
	response.redirect("/error.asp?message=The Note was not " & sAction & " successfully.")
end if

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>	
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle"><%= sTitle %></span>
										</tr>
										<tr>
											<td>
												The Note was successfully <%= sAction %>.<br><br>
												<a class="newsTitle" href="Notes.asp">Return to Notes Listing</a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
	
set rs = nothing
set oUserObj = nothing
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/User.Class.asp" ------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Notes"
	
	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/Notes.asp")
	end if		
	
	iPage = 0
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ConfirmDelete(p_iNoteID)
		{
			if (confirm("Are you sure you want to delete this Note?"))
				window.location.href = '/ModNoteProc.asp?NoteID=' + p_iNoteID + '&mode=DELETE';
		}

//--></script>
<%
dim iUserID 'as integer
dim sCurrentDate 'as string
dim rs 'as object
dim oUserObj 'as object
dim sSort 'as object

iUserID = session("User_ID")

'Get page number of which records are showing
sSort = trim(request("Sort"))
if (sSort = "") then
	if (trim(Session("Sort")) <> "") then
		sSort = Session("Sort")
	end if
else
	Session("Sort") = sSort
end if

if UCase(trim(sSort)) = "LASTUPDATED" then
	sSort = "UpdatedDate Desc"
elseif UCase(trim(sSort)) = "TITLE" then
	sSort = "Title"
else
	sSort = "UpdatedDate Desc"
end if

'Get page number of which records are showing
iCurPage = trim(request("page_number"))
if (iCurPage = "") then
	if (trim(Session("CurPage")) <> "") then
		iCurPage = Session("CurPage")
	end if
else
	Session("CurPage") = iCurPage
end if

if UCase(trim(sSort)) = "LASTUPDATED" then
	sSort = "UpdatedDate Desc"
elseif UCase(trim(sSort)) = "TITLE" then
	sSort = "Title"
else
	sSort = "UpdatedDate Desc"
end if

set oUserObj = new User
oUserObj.ConnectionString = application("sDataSourceName")
oUserObj.UserID = iUserID

'get the notes
set rs = oUserObj.GetNotes(sSort)

If iCurPage = "" Then iCurPage = 1
If iMaxRecs = "" Then iMaxRecs = 20

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>	
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle">Notes</span>
										</tr>								
										<tr>
											<td><a class="newsTitle" href="ModNote.asp">Add New Note</a></td>
										</tr>
										<tr>
											<td>
												<form name="frm1" method="POST" action="/Notes.asp">
												<input type="hidden" name="page_number" value="1">
												<b>Sort By:</b>&nbsp;
												<select name="sort" onChange="document.frm1.submit()">
													<option value="UpdatedDate" <% if Ucase(sSort) = "UPDATEDDATE" then response.write("SELECTED") end if %>>Last Updated</option>
													<option value="Title" <% if Ucase(sSort) = "TITLE" then response.write("SELECTED") end if %>>Note</option>
												</select>
												</form>
											</td>
										</tr>												
										<tr>
											<td>											
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr bgcolor="#D8CBAA">
														<td valign="top"><b>Note</b></td>
														<td valign="top" align="center"><b>Last Updated</b></td>
														<td align="center"><b>Delete</b></td>
													</tr>
<%
if not rs.eof then
		'Set the number of records displayed on a page
		rs.PageSize = iMaxRecs
		rs.Cachesize = iMaxRecs
		iPageCount = rs.PageCount
	
		'determine which search page the user has requested.
		If clng(iCurPage) > clng(iPageCount) Then iCurPage = iPageCount
			
		If clng(iCurPage) <= 0 Then iCurPage = 1
			
		'Set the beginning record to be display on the page
		rs.AbsolutePage = iCurPage		
		
		iCount = 0	
		
		do while (iCount < rs.PageSize) AND (not rs.eof)
		if sBgrColor = "" then	
			sBgrColor = "#EAE3D3"
		else
			sBgrColor = ""
		end if				
%>
													<tr bgcolor="<%= sBgrColor %>">
														<td><a class="newsTitle" href="/ModNote.asp?NoteID=<%=rs("ID")%>"><%= rs("Title") %></a></td>
														<td align="center"><%= rs("UpdatedDate") %></td>														
														<td align="center"><a class="newsTitle" href="javascript:ConfirmDelete(<%= rs("ID") %>)">Delete</a></td>
													</tr>											
<%
		rs.MoveNext
		iCount = iCount + 1
	loop

	if sBgrColor = "" then	
		sBgrColor = "#EAE3D3"
	else
		sBgrColor = ""
	end if				
		
	response.write("<tr bgcolor=""" & sBgrColor& """>" & vbcrlf)
	response.write("<td colspan=""3"">" & vbcrlf)

	'Display the proper Next and/or Previous page links to view any records not contained on the present page
	if iCurPage > 1 then
		response.write("<a class=""newsTitle"" href=""/Notes.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
	end if
	if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
		response.write("&nbsp;|&nbsp;")
	end if 
	if trim(iCurPage) <> trim(iPageCount) then
		response.write("<a class=""newsTitle"" href=""/Notes.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
	end if
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)	
	response.write("</table>" & vbcrlf)
	
	'display Page number
	response.write("<table width=""100%"">" & vbcrlf)	
	response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
	response.write("<td align=""center"" colspan=""3""><br>" & vbcrlf)	
	response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
	response.write("</td>" & vbcrlf)
	response.write("</tr>" & vbcrlf)	
else
%>
													<tr>
														<td colspan="2">You currently do not have any Notes.</td>
													</tr>
<%
end if
%>													
												</table>												
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
	
set rs = nothing
set oUserObj = nothing
%>

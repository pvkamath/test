<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iCandidateId
dim iCompanyId 
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
dim iAssociateBranchId
dim iLicenseId
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerId 'Holds the id of whatever type of owner the license has
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iCandidateId = ScrubForSQL(request("oid"))
iCompanyId = ScrubForSql(request("cid"))
iCompanyL2Id = ScrubForSql(request("c2id"))
iCompanyL3Id = ScrubForSql(request("c3id"))
iBranchId = ScrubForSql(request("bid"))
iLicenseId = ScrubForSql(request("lid"))


dim oLicense 'License Object
dim oCompany 'Company object
dim oCompanyL2 'CompanyL2 object
dim oCompanyL3 'CompanyL3 object
dim oAssociate 'Officer object


'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'through each of the owner type possibilities and see if we were given an owner
'to whom we'll assign this license.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed License ID.")
		Response.End
	
	'If this is a company license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 3 and oLicense.OwnerId <> session("UserCompanyId") then
		
		AlertError("Unable to load the passed License ID.")
		Response.End
		
	'If this is a companyL2 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 4 then
	
		set oCompanyL2 = new CompanyL2
		oCompanyL2.ConnectionString = application("sDataSourceName")
		oCompanyL2.VocalErrors = application("bVocalErrors")
		
		if not oCompanyL2.LoadCompanyById(oLicense.OwnerId) = 0 then
		
			if oCompanyL2.CompanyL2Id <> session("UserCompanyId") then
				
				AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
				Response.End
			
			end if				
		
		else
		
			AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
			
		end if
		
		set oCompanyL2 = nothing

	'If this is a companyL3 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 5 then
	
		set oCompanyL3 = new CompanyL3
		oCompanyL3.ConnectionString = application("sDataSourceName")
		oCompanyL3.VocalErrors = application("bVocalErrors")
		
		if not oCompanyL3.LoadCompanyById(oLicense.OwnerId) = 0 then
		
			if oCompanyL3.CompanyL2Id <> session("UserCompanyId") then
				
				AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
				Response.End
			
			end if				
		
		else
		
			AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
			
		end if
		
		set oCompanyL3 = nothing
	
	'If this is a branch license, verify that its branch belongs to the user's company.
	elseif oLicense.OwnerTypeId = 2 then
	
		'Initialize the company object
		set oCompany = new Company
		oCompany.ConnectionString = application("sDataSourceName")
		oCompany.TpConnectionString = application("sTpDataSourceName")
		oCompany.VocalErrors = application("bVocalErrors")
	
		'Verify that the ID passed is valid and that the user has access.	
		if not oCompany.LoadCompanyByBranchId(oLicense.OwnerId) = 0 then
			
			if oCompany.CompanyId <> session("UserCompanyId") then
				
				AlertError("Unable to load the passed Branch ID.")
				Response.End
				
			end if 
		
		else
			
			AlertError("Failed to load the passed Branch ID.")
			Response.End
		
		end if
		
		set oCompany = nothing
		
	'If this is an officer license, verify that the officer belongs to the user's company.
	elseif oLicense.OwnerTypeId = 1 then
	
		'Initialize the officer object
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = application("bVocalErrors")

		'Verify that the ID passed is valid and that the user has access.	
		if not oAssociate.LoadAssociateById(oLicense.OwnerId) = 0 then
		
			if oAssociate.CompanyId <> session("UserCompanyId") then

				AlertError("Unable to load the passed Officer ID.")
				Response.End

			end if
		
		else
		
			AlertError("Failed to load the passed Officer ID.")
			Response.End
		
		end if 
		
		iAssociateBranchId = oAssociate.BranchId

		set oAssociate = nothing
		
	end if 
	
	iOwnerId = oLicense.OwnerId
	iOwnerTypeId = oLicense.OwnerTypeId
	sMode = "Edit"

elseif iCandidateId <> "" then 

	iOwnerId = iCandidateId	
	iOwnerTypeId = 1
	sMode = "Add"

elseif iCompanyId <> "" then
	
	iOwnerId = iCompanyId
	iOwnerTypeId = 3
	sMode = "Add"
	
elseif iCompanyL2Id <> "" then
	
	iOwnerId = iCompanyL2Id
	iOwnerTypeId = 4
	sMode = "Add"
	
elseif iCompanyL3Id <> "" then

	iOwnerId = iCompanyL3Id
	iOwnerTypeId = 5
	sMode = "Add"
	
elseif iBranchId <> "" then

	iOwnerId = iBranchId
	iOwnerTypeId = 2
	sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'*****
'Instead of going through each of the access types, we should be able to take the type of license (branch, CompanyL3, etc) and
'do a CheckIsTHIS_____Admin(id) and do a check.  If It's new, then... We can go to the sections above and do a check based on the
'specified values for each of the license types.  We need to add "CheckIsThisAssociateAdmin" function to the misc.asp file.
'*****






'If a branch admin is loading this license, make sure they have access.
if CheckIsBranchAdmin() then

	if oLicense.OwnerTypeId = 3 or oLicense.OwnerTypeId = 4 or oLicense.OwnerTypeId = 5 then
	
		'Can't open company licenses
		AlertError("Unable to load this license ID.")
		Response.End

	elseif oLicense.OwnerTypeId = 2 then
	
		'Make sure they administer the same branch.
		if not CheckIsThisBranchAdmin(cint(oLicense.OwnerId)) then
		
			AlertError("Unable to load this license ID.")
			Response.End
		
		end if
		
	elseif oLicense.OwnerTypeId = 1 then
	
		'Make sure they administer the branch that this user belongs to.	
		if not CheckIsThisBranchAdmin(cint(iAssociateBranchId)) then

			AlertError("Unable to load this license ID.")
			Response.End
	
		end if
		
	end if
	
	
elseif CheckIsCompanyL2Admin() then

	if oLicense.OwnerTypeId = 3 then
	
		'Can't open Company licenses
		AlertError("Unable to load this license ID.")
		Response.End
		
	elseif oLicense.OwnerTypeId = 4 then
	
		'Make sure they administer this CompanyL2
		if not CheckIsThisCompanyL2Admin(cint(oLicense.OwnerId)) then
			
			AlertError("Unable to load this license ID.")
			Response.End
			
		end if
		
	elseif oLicense.OwnerTypeId = 5 then
	
		'Make sure they administer this CompanyL3 via its parent CompanyL2
		if not CheckIsThisCompanyL3Admin(cint(oLicense.OwnerId)) then
		
			AlertError("Unable to load this license ID.")
			Response.End
		
		end if
		
	elseif oLicense.OwnerTypeId = 2 then
		
		'Make sure they administer this Branch via its parent CompanyL2 
		'or CompanyL3
		if not CheckIsThisBranchAdmin(cint(oLicense.OwnerId)) then
		
			AlertError("Unable to load this license ID.")
			Response.End
			
		end if
		
	elseif oLicense.OwnerTypeId = 1 then
	
		'Make sure they administer this Associate via its parent CompanyL2,
		'CompanyL3, or Branch
		if not CheckIsThisAssociateAdmin(cint(oLicense.OwnerId)) then
		
			AlertError("Unable to load this license ID.")
			Response.End
			
		end if
		
	end if
	
end if 



'Configure the administration submenu options
if iOwnerTypeId = 1 then
	bAdminSubmenu = true
	sAdminSubmenuType = "OFFICERS"
elseif iOwnerTypeId = 2 then
	bAdminSubmenu = true
	sAdminSubmenuType = "BRANCHES"
elseif iOwnerTypeId = 3 then
	bAdminSubmenu = true
	sAdminSubmenuType = "COMPANY"
end if


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.LicenseNum, "License Number")
			|| !checkIsDate(FORM.LicenseExpDate, "License Expiration Date must be in the proper format.", 0)
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Licensing -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<% if iOwnerTypeId = 1 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Officer Licensing</td>
					<% elseif iOwnerTypeId = 2 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Licensing</td>
					<% else %>
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Company" align="absmiddle" vspace="10"> Corporate Licensing</td>
					<% end if %>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a License</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="LicenseForm" action="modlicenseproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iOwnerId %>" name="OwnerId">
<input type="hidden" value="<% = iOwnerTypeId %>" name="OwnerTypeId">
<input type="hidden" value="<% = iLicenseId %>" name="LicenseId">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td><b>License Number: </b></td>
		<td>
			<input type="text" name="LicenseNum" value="<% = oLicense.LicenseNum %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td><b>License State: </b></td>
		<td>
			<% call DisplayStatesDropDown(oLicense.LicenseStateId, 1, "LicenseStateId") 'functions.asp %>
		</td>
	</tr>
	<tr>
		<td><b>License Type: </b></td>
		<td>
			<input type="text" name="LicenseType" value="<% = oLicense.LicenseType %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td><b>License Status: </b></td>
		<td>
			<% call DisplayLicenseStatusDropDown(oLicense.LicenseStatusId, 1, "LicenseStatus") %>
		</td>
	</tr>	
	<tr>
		<td><b>License Expiration: </b></td>
		<td>
			<input type="text" name="LicenseExpDate" value="<% = oLicense.LicenseExpDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.LicenseExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.LicenseExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td><b>Renewal Application Deadline: </b></td>
		<td>
			<input type="text" name="LicenseAppDeadline" value="<% = oLicense.LicenseAppDeadline %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.LicenseAppDeadline');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.LicenseAppDeadline');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td><b>Renewal Submission Date: </b></td>
		<td>
			<input type="text" name="SubmissionDate" value="<% = oLicense.SubmissionDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.SubmissionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.SubmissionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td><b>Renewal Received Date: </b></td>
		<td>
			<input type="text" name="ReceivedDate" value="<% = oLicense.ReceivedDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.ReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.ReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
'set oPreference = nothing
set oLicense = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
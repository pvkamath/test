<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iLicenseId 'ID of the licenese in question
dim iOwnerId 'ID of whatever owner type owns the license
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerBranchId 'Is there a specific branch to which this license belongs?
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iOwnerId = ScrubForSql(request("OwnerId"))
iOwnerTypeId = ScrubForSql(request("OwnerTypeId"))
iLicenseId = ScrubForSql(request("LicenseId"))

dim oLicense 'License Object

'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'Declare the Associate object and the Company object.  We'll use these to
'verify permissions for new licenses.
dim oAssociate
dim oCompany

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		Response.Write("Failed to load the passed License ID.<br>" & vbCrLf)
		Response.End

	'If this is a company license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 3 and oLicense.OwnerId <> session("UserCompanyId") then
		
		Response.Write("Unable to load the passed License ID.<br>" & vbCrLf)
		Response.End
	
	'If this is a branch license, verify that it's branch belongs to the user's company.
	elseif oLicense.OwnerTypeId = 2 then
	
		'Initialize the company object
		set oCompany = new Company
		oCompany.ConnectionString = application("sDataSourceName")
		oCompany.TpConnectionString = application("sTpDataSourceName")
		oCompany.VocalErrors = application("bVocalErrors")
	
		'Verify that the ID passed is valid and that the user has access.	
		if not oCompany.LoadCompanyByBranchId(oLicense.OwnerId) = 0 then
			
			if oCompany.CompanyId <> session("UserCompanyId") then
				
				Response.Write("Unable to load the passed Branch ID.<br>" & vbCrLf)
				Response.End
				
			end if 
		
		else
			
			Response.Write("Failed to load the passed Branch ID.<br>" & vbCrLf)
			Response.End
		
		end if
		
		iOwnerBranchId = oLicense.OwnerId
		
		set oCompany = nothing
		
	'If this is an officer license, verify that the officer belongs to the user's company.
	elseif oLicense.OwnerTypeId = 1 then
	
		'Initialize the officer object
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = application("bVocalErrors")

		'Verify that the ID passed is valid and that the user has access.	
		if not oAssociate.LoadAssociateById(oLicense.OwnerId) = 0 then
		
			if oAssociate.CompanyId <> session("UserCompanyId") then

				Response.Write("Unable to load the passed Officer ID.<br>" & vbCrLf)
				Response.End

			end if
		
		else
		
			Response.Write("Failed to load the passed Officer ID.<br>" & vbCrLf)
			Response.End
		
		end if 
		
		iOwnerBranchId = oAssociate.BranchId

		set oAssociate = nothing
	
	end if 
	
	sMode = "Edit"
	
elseif iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	oLicense.OwnerId = iOwnerId
	oLicense.OwnerTypeId = iOwnerTypeId
	
	'If this is an Officer license...
	if iOwnerTypeId = 1 then

		'Initialize the officer object
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = application("bVocalErrors")

		'Verify that the ID passed is valid and that the user has access.	
		if not oAssociate.LoadAssociateById(iOwnerId) = 0 then
		
			if oAssociate.CompanyId <> session("UserCompanyId") then

				Response.Write("Unable to load the passed Officer ID.<br>" & vbCrLf)
				Response.End

			end if
		
		else
		
			Response.Write("Failed to load the passed Officer ID.<br>" & vbCrLf)
			Response.End
		
		end if 
		
		iOwnerBranchId = oAssociate.BranchId
		
	'If this is a Branch license...
	elseif iOwnerTypeId = 2 then
	
		'Initialize the company object
		set oCompany = new Company
		oCompany.ConnectionString = application("sDataSourceName")
		oCompany.TpConnectionString = application("sTpDataSourceName")
		oCompany.VocalErrors = application("bVocalErrors")
	
		'Verify that the ID passed is valid and that the user has access.	
		if not oCompany.LoadCompanyByBranchId(iOwnerId) = 0 then
			
			if oCompany.CompanyId <> session("UserCompanyId") then
				
				Response.Write("Unable to load the passed Branch ID.<br>" & vbCrLf)
				Response.End
				
			end if 
		
		else
			
			Response.Write("Failed to load the passed Branch ID.<br>" & vbCrLf)
			Response.End
			
		end if 
	
		iOwnerBranchId = iOwnerId
	
	'If this is a Company license...
	elseif iOwnerTypeId = 3 then
	
		'Initialize the company object
		set oCompany = new Company
		oCompany.ConnectionString = application("sDataSourceName")
		oCompany.TpConnectionString = application("sDataSourceName")
		oCompany.VocalErrors = application("bVocalErrors")

		'Verify that the ID passed is valid and that the user has access.
		if not oCompany.LoadCompanyById(iOwnerId) = 0 then
			
			if oCompany.CompanyId <> session("UserCompanyId") then
				
				Response.Write("Unable to load the passed Company ID.<br>" & vbCrLf)
				Response.End
			
			end if 
		
		else
			
			Response.Write("Failed to load the passed Company ID.<br>" & vbCrLf)
			Response.End
			
		end if 
	
	else
	
		Response.Write("Invalid Owner Type specified.<br>" & vbCrLf)
		Response.End
	
	end if 

	sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	Response.Write("Failed to load a passed Owner ID.<br>" & vbCrLf)
	Response.End

end if 



'If a branch admin is loading this license, make sure they have access.
if CheckIsBranchAdmin() then

	if oLicense.OwnerTypeId = 3 then
	
		'Can't open company licenses
		AlertError("This page requires a proper license ID.")
		Response.End
		
	elseif oLicense.OwnerTypeId = 2 then
	
		'Make sure they administer the same branch.
		if not CheckIsThisBranchAdmin(cint(oLicense.OwnerId)) then
		
			AlertError("This page requires a proper license ID.")
			Response.End
		
		end if
		
	elseif oLicense.OwnerTypeId = 1 then
	
		'Make sure they administer the branch that this user belongs to.	
		if not CheckIsThisBranchAdmin(cint(iOwnerBranchId)) then

			AlertError("This page requires a proper license ID.")
			Response.End
	
		end if 
		
	end if

end if 



'We have a valid license.  Go ahead and set the properties based on our passed 
'form data.
if sMode = "New" then
	oLicense.OwnerId = iOwnerId
	oLicense.OwnerTypeId = iOwnerTypeId
	'oLicense.OwnerCompanyId = session("UserCompanyId")
end if
oLicense.OwnerCompanyId = session("UserCompanyId")
oLicense.OwnerBranchId = iOwnerBranchId
oLicense.LicenseNum = ScrubForSql(request("LicenseNum"))
oLicense.LicenseExpDate = ScrubForSql(request("LicenseExpDate"))
oLicense.LicenseAppDeadline = ScrubForSql(request("LicenseAppDeadline"))
oLicense.LicenseStatusId = ScrubForSql(request("LicenseStatus"))
oLicense.LicenseType = ScrubForSql(request("LicenseType"))
oLicense.LicenseStateId = ScrubForSql(request("LicenseStateId"))
oLicense.ReceivedDate = ScrubForSql(request("ReceivedDate"))
oLicense.SubmissionDate = ScrubForSql(request("SubmissionDate"))



'Save the license
if oLicense.SaveLicense = 0 then

	Response.Write("Failed to save the license.  Please try again.<br>" & vbCrLf)
	Response.End
	
end if 


'Clean up
set oAssociate = nothing
set oCompany = nothing


'Redirect back to the appropriate page.
if oLicense.OwnerTypeId = 1 then

	Response.Redirect("/officers/officerdetail.asp?id=" & oLicense.OwnerId)

elseif oLicense.OwnerTypeId = 2 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oLicense.OwnerId)
	
elseif oLicense.OwnerTypeId = 3 then

	Response.Redirect("/company/companydetail.asp?id=" & oLicense.OwnerId)
	
end if 



%>

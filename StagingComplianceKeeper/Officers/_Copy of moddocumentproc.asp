<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/AssociateDoc.Class.asp" ------------------->
<!-- #include virtual = "/includes/License.Class.asp" --->

<!-- #include virtual = "/admin/includes/gfxSpex.asp" ------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Documents"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

'Create the Fileup object that will allow us to upload files
dim oFileUp
set oFileUp = server.CreateObject("softartisans.fileup")
oFileUp.UseMemory = true



dim iCompanyId
dim iOwnerId
dim iOwnerTypeId
dim iDocId
dim sMode
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))
iOwnerId = ScrubForSql(oFileUp.Form("oid"))
iOwnerTypeId = ScrubForSql(oFileUp.Form("otid"))
iDocId = ScrubForSql(oFileUp.Form("DocId"))



dim oAssociateDoc
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")

if iDocId <> "" then
	if oAssociateDoc.LoadDocById(iDocId) = 0 then
	
		AlertError("Failed to load the Document information.")
		Response.End
		
	'If this is a company document, check that it belongs to the user's company
	elseif oAssociateDoc.OwnerTypeId = 3 and (oAssociateDoc.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then

		AlertError("Unable to load the passed Document ID.")
		Response.End
	
	'If this is a companyL2 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 4 then
		
		if not CheckIsThisCompanyL2Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyl3 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if
	
	'If this is a branch doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if
		
	'If this is an officer doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 1 then
		
		if not CheckIsThisAssociateAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	'If this is a state doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 6 then
	
		if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
			AlertError("Unable to load the passed State ID.")
			Response.End
		end if
		
	'If this is a license doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 7 then
	
		dim oLicense
		set oLicense = new License
		oLicense.ConnectionString = application("sDataSourceName")
		oLicense.VocalErrors = application("bVocalErrors")
		if oLicense.LoadLicenseById(oAssociateDoc.OwnerId) <> 0 then
			if oLicense.OwnerTypeId = 1 then
				if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 2 then
				if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 3 then
				if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 4 then
				if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 5 then
				if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			end if
		else
			AlertError("Unable to load the passed License ID.")
			Response.End
		end if
	
	end if 
	
	iOwnerId = oAssociateDoc.OwnerId
	iOwnerTypeId = oAssociateDoc.OwnerTypeId
	sMode = "Edit"
	
elseif iOwnerTypeId = "1" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisAssociateAdmin(iOwnerId) then
		AlertError("Failed to load the passed Officer ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = 3 then
	
	sMode = "Add"
	
	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed Company ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = 4 then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL2Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = 5 then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL3Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = 2 then
	
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisBranchAdmin(iOwnerId) then
		AlertError("Failed to load the passed Branch ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = 6 then

	sMode = "Add"
	
	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed State ID.")
		Response.End
	end if
	
'If this is a license doc, check that the user has access.
elseif iOwnerTypeId = 7 then
	
	sMode = "Add"
	
	set oLicense = new License
	oLicense.ConnectionString = application("sDataSourceName")
	oLicense.VocalErrors = application("bVocalErrors")
	if oLicense.LoadLicenseById(iOwnerId) <> 0 then
		if oLicense.OwnerTypeId = 1 then
			if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 2 then
			if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 3 then
			if not CheckIsAdmin() then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 4 then
			if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 5 then
			if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		end if
	else
		AlertError("Unable to load the passed License ID.")
		Response.End
	end if

else

	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End
		
end if 



'Determine if we got a new document file
dim sFileName
dim bChangeFileName
dim iStrPos 
if oFileUp.Form("Document").UserFileName <> "" then
	
	bChangeFileName = true
	iStrPos = instrrev(oFileUp.Form("Document").UserFileName, "\")
	if iStrPos = 0 then
		iStrPos = 1
	end if
	sFileName = ScrubForSql(mid(oFileUp.Form("Document").UserFileName, iStrPos))
	
end if

dim sUploadPath, sDynPath
dim sExtension

sUploadPath = application("sAbsMediaUploadPath") & "\OfficerDocs\"
sDynPath = application("sDynMediaUploadPath") & "OfficerDocs/"
sExtension = mid(oFileUp.Form("Document").UserFileName, instrrev(oFileUp.Form("Document").UserFileName, ".") + 1)
	


'Get the passed form properties and assign them to the document object
oAssociateDoc.DocName = ScrubForSql(oFileUp.Form("DocName"))
oAssociateDoc.CompanyId = iCompanyId
oAssociateDoc.OwnerId = iOwnerId
oAssociateDoc.OwnerTypeId = iOwnerTypeId
oAssociateDoc.FileName = sFileName
oAssociateDoc.FileExtension = sExtension

if oAssociateDoc.SaveDoc = 0 then

	AlertError("Failed to save the document information")
	Response.End

end if 


'We've saved the document, so we can set the path values and resave.
'if bChangeFileName then
'
'	oAssociateDoc.FileName = sDynPath & oAssociateDoc.DocId & oAssociate.UserId & oAssociate.CompanyId & "." & sExtension
'	
'end if
if oAssociateDoc.SaveDoc = 0 then

	AlertError("Failed to save the document information")
	Response.End

end if 


'Save the actual file

		'dim oConn
		'dim oRs
		'dim sSql
		dim oStream
		set oStream = server.CreateObject("ADODB.Stream")
				
		'set oConn = server.CreateObject("ADODB.Connection")
		'set oRs = server.CreateObject("ADODB.Recordset")
				
		'oConn.ConnectionString = application("sDataSourceName")'g_sConnectionString
		'oConn.Open
		
		'See if there is an existing file in the database already
		'sSql = "SELECT DocId, FileContents FROM AssociateDocsFiles " & _
		'	"WHERE DocId = '" & oAssociateDoc.DocId & "'"
		'oRs.Open sSql, oConn, adOpenKeyset, adLockOptimistic
		
		oStream.Type = 1 'adTypeBinary
		oStream.Open
		Response.Write("x" & oStream.Size & "x")
		oStream.Write oFileUp.Form("Document").UploadContents(2)
		Response.Write("<br>x" & oStream.Size & "x")
		'Response.End
		
		'oRs.AddNew
		'oRs("DocId") = oAssociateDoc.DocId
		'oRs("FileContents") = oStream.Read
		'oFileUp.Form("Document").SaveAsBlob(oRs("FileContents"))

oAssociateDoc.SaveDocFile(oStream)
'Response.End
'if bChangeFileName then
'	oFileUp.Form("Document").SaveAs sUploadPath & oAssociateDoc.DocId & oAssociate.UserId & oAssociate.CompanyId & "." & sExtension
'	if err <> 0 then
'		AlertError("Save Failed:" & err.Description)
'		Response.End
'	end if
'end if


'Response.Redirect("officerdetail.asp?id=" & oAssociate.UserId)
'Response.End

%>
<%
oStream.Close
set oStream = nothing

'Redirect back to the appropriate page.
if oAssociateDoc.OwnerTypeId = 1 then
	Response.Redirect("/officers/officerdetail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 2 then
	Response.Redirect("/branches/branchdetail.asp?branchid=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 4 then
	Response.Redirect("/branches/companyl2detail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 5 then
	Response.Redirect("/branches/companyl3detail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 3 then
	Response.Redirect("/company/companydetail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 6 then
	Response.Redirect("/states/stateguide.asp?state=" & oAssociateDoc.LookupState(oAssociateDoc.OwnerId, 1))
elseif oAssociateDoc.OwnerTypeId = 7 then
	Response.Redirect("/officers/modlicense.asp?lid=" & oAssociateDoc.OwnerId)
end if 


set oAssociateDoc = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
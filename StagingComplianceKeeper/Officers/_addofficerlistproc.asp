<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"

server.ScriptTimeout = 600


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim bIsBranchAdmin
bIsBranchAdmin = CheckIsBranchAdmin


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Course information variables
dim sFirstName
dim sMidInitial
dim sLastName
dim sSsn
dim dDateOfBirth
dim iDriversLicenseStateId
dim sDriversLicenseNo
dim iBranchId
dim iCompanyL2Id
dim iCompanyL3Id
dim dHireDate
dim dTerminationDate
dim sEmployeeId
dim sTitle
dim sDepartment
dim sEmail
dim sPhone
dim sPhoneExt
dim sPhone2
dim	sPhone2Ext
dim sPhone3
dim sPhone3Ext
dim sFax
dim sAddress
dim sAddress2
dim sCity
dim iStateId
dim sZipcode
dim sZipcodeExt
dim sCellPhone
dim sHomePhone
dim sHomeEmail

dim sLicenseNum
dim iLicenseStateId
dim iLicenseTypeId
dim sLicenseType
dim iLicenseStatusId
dim dLicenseExpDate

dim iProviderId
dim sCourseName
dim iCourseHours
dim dCompletionDate
dim dExpDate

	
'We need to find and parse the binary information uploaded
dim iBinFormData
dim sFormData
dim iCount
	
iBinFormData = Request.BinaryRead(Request.TotalBytes)
	
for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next

	
dim sFormArray
sFormArray = split(sFormData, "-----------------------------")
	
dim sName
dim sValue
dim iLocStart
dim iLocEnd
	
dim sForm()
	
dim sCSVLines
dim sCSVSingleLine
dim sCSVData
dim iLineCount
dim iCountCols

for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next
	
dim oRegExp
dim sRegExpMatches
dim oMatch
set oRegExp = new RegExp
oRegExp.Pattern = ",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))"

'Response.Write("<p>" & oRegExp.Pattern & "<p>")

oRegExp.IgnoreCase = true
oRegExp.Global = true

'Loop through the array of form values to parse out the data.
for iCount = 0 to ubound(sFormArray)
	
	'Response.Write(sFormArray(iCount) & "<p><br>")
	if instr(1, sFormArray(iCount), "name=") > 0 then
		'Response.Write(mid(sFormArray(iCount), instr(1, sFormArray(iCount), "name=")) & "<br>")
		iLocStart = instr(1, sFormArray(iCount), "name=""") + 6
		iLocEnd = instr(iLocStart, sFormArray(iCount), """")
		sName = mid(sFormArray(iCount), iLocStart, iLocEnd - iLocStart)
			
		'Find the CSV file within the data
		if sName = "AddUserCSV" then 
			
			'+28 accounts for mime type text and two CR/LF pairs.
			sValue = mid(sFormArray(iCount), instr(1, sFormArray(iCount), "Content-Type: ") + 14)
			'response.write("X" & sValue & "X, " & instr(1, sValue, vbCrLf) & "<br>")
			sValue = mid(sValue, instr(1, sValue, vbCrLf) + 4)
			sValue = left(sValue, len(sValue) - 2)
			'Response.Write("'" & sName & "' : '" & sValue & "'<p>")
				
			sCSVLines = split(sValue, vbCrLf)
				
			redim sCSVData(26, ubound(sCSVLines))
				
			for iLineCount = 0 to ubound(sCSVLines)

				sCSVSingleLine = split(sCSVLines(iLineCount), ",")
	
				'Response.Write(sCsvLines(iLineCount) & "<br>")
				'response.write(uBound(sCSVSingleLine) & "<br>")				

				if ubound(sCSVSingleLine) >= 3 then
				
'					set sRegExpMatches = oRegExp.Execute(trim(sCsvSingleLine(iCountCols)))
'						
'					'Print the # of matches we found
'					Response.Write sRegExpMatches.Count & " matches found...<P>"
'						
'					'Step through our matches 
'					For Each oMatch in sRegExpMatches 
'						Response.Write oMatch.Value & " | "
'					Next 
'						
'					'Clean up 
'					Set oMatch = Nothing 
'				
					for iCountCols = 0 to ubound(sCsvSingleLine)
'					
'						'if not instr(1, sCsvSingleLine(iCountCols), """") >= 1 then
'						'
'						'	if not instr(instr(1, sCsvSingleLine(iCountCols), """"), sCsvSingleLine(iCountCols), """") >= 1 then
'						''	
'						'		do while not bFoundQoute
'						'		
'						'			if 
'						'		
'						'		loop
'				
'						Response.Write("L" & iCountCols & ": " & trim(sCSVSingleLine(iCountCols)) & "<br>")
						sCSVData(iCountCols, iLineCount) = trim(sCSVSingleLine(iCountCols))
						'Response.Write("L" & iCountCols & ": " & sCsvData(iCountCols, iLineCount) & "<br>")
'					
					next
					
				'Response.Write(sCSVData(0, iLineCount) & " " & sCSVData(1, iLineCount) & ": " & sCSVData(2, iLineCount) & "<p>")
				
				else
				
					'Response.Write("Making Blank Line...<br>")
					
					'We'll get here if we get some line in our CSV that's not in
					'the correct format.  Rather than bother with removing just
					'that line from the array, we'll just make a blank entry that
					'we can ignore later.
					sCsvData(0, iLineCount) = ""
					sCsvData(1, iLineCount) = ""
					sCsvData(2, iLineCount) = ""
					sCsvData(3, iLineCount) = ""
					sCsvData(4, iLineCount) = ""
					sCsvData(5, iLineCount) = ""
					sCsvData(6, iLineCount) = ""
					sCsvData(7, iLineCount) = ""
					sCsvData(8, iLineCount) = ""
					sCsvData(9, iLineCount) = ""
					sCsvData(10, iLineCount) = ""
					sCsvData(11, iLineCount) = ""
					sCsvData(12, iLineCount) = ""
					sCsvData(13, iLineCount) = ""
					sCsvData(14, iLineCount) = ""
					sCsvData(15, iLineCount) = ""
					sCsvData(16, iLineCount) = ""
					sCsvData(17, iLineCount) = ""
					sCsvData(18, iLineCount) = ""
					sCsvData(19, iLineCount) = ""
					sCsvData(20, iLineCount) = ""
					sCsvData(21, iLineCount) = ""
					sCsvData(22, iLineCount) = ""
					sCsvData(23, iLineCount) = ""
					sCsvData(24, iLineCount) = ""
					sCsvData(25, iLineCount) = ""
					sCsvData(26, iLineCount) = ""
				
				end if
				
			next 
			
		else

			sValue = trim(mid(sFormArray(iCount), iLocEnd + 1))
			sValue = mid(left(sValue, len(sValue) - 2), 5)
			'Response.Write("'" & sName & "' : '" & sValue & "' : " & len(sValue) & "<p>")
			redim preserve sForm(2, iCount + 1)
			sForm(0, iCount) = sName
			sForm(1, iCount) = sValue
			
		end if
						
	end if
				
next


dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 


%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add a Loan Officer Listing</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
<%

'Initialize the associate object.
dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.CompanyId = session("UserCompanyId")

'Initialize the license object.
dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'Initialize the course object
dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

'Initialize the CompanyL2 object
dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")
oCompanyL2.CompanyId = session("UserCompanyId")

'Initialize the CompanyL3 object
dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")
oCompanyL3.CompanyId = session("UserCompanyId")

'Initialize the Branch object
dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")
oBranch.CompanyId = session("UserCompanyId")


dim oRs 'Recordset object used for search results.


'Declare some variable to track where we are in the loop
dim bErrors 
dim bErrorsInList
dim bBlankSkip

dim bUpdate 'Determines whether this is a new record or an update

response.write("<ul>")

'Variable to hold the type of records we're parsing now, 1 for officers, 
'2 for licenses, 3 for courses.
dim iRecordType 
iRecordType = 1

dim bSkipLine

'Loop through each of the records in our array.
for iCount = 0 to ubound(sCSVData, 2)

	'If we come across a marker to designate a change in the record type,
	'change the value accordingly.
	if sCsvData(0, iCount) = "*LICENSE" then
		iRecordType = 2
	elseif sCsvData(0, iCount) = "*COURSE" then
		iRecordType = 3
	else
		iRecordType = 1
	end if 	
	
	if iRecordType = 1 then	
	
		'RecordType 1 means this is an officer record.

		'Pull the officer attribute values out of the array.
		sFirstName = ScrubForSql(sCsvData(0, iCount))
		sMidInitial = ScrubForSql(sCsvData(1, iCount))
		sLastName = ScrubForSql(sCsvData(2, iCount))
		sSsn = ScrubForSql(sCsvData(3, iCount))
		dDateOfBirth = ScrubForSql(sCsvData(4, iCount))
		iDriversLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(5, iCount)))
		sDriversLicenseNo = ScrubForSql(sCsvData(6, iCount))
		'if bIsBranchAdmin then
		'	iBranchId = session("UserBranchId")
		'else
		iBranchId = oBranch.LookupBranchIdByBranchNum(ScrubForSql(sCsvData(7, iCount)))
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(7, iCount)))
		iCompanyL3Id = oCompanyL3.LookupCompanyIdByName(ScrubForSql(sCsvData(7, iCount)))
'		if bIsBranchAdmin then
'			if not CheckIsThisBranchAdmin(iBranchId) then
'				bErrors = true
'			end if
'		end if
		'end if 
		dHireDate = ScrubForSql(sCsvData(8, iCount))
		dTerminationDate = ScrubForSql(sCsvData(9, iCount))
		sEmployeeId = ScrubForSql(sCsvData(10, iCount))
		sTitle = ScrubForSql(sCsvData(11, iCount))
		sDepartment = ScrubForSql(sCsvData(12, iCount))
		sEmail = ScrubForSql(sCsvData(13, iCount))
		sPhone = ScrubForSql(sCsvData(14, iCount))
		sPhoneExt = ScrubForSql(sCsvData(15, iCount))
		sPhone2 = ScrubForSql(sCsvData(16, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(17, iCount))
		sPhone3 = ScrubForSql(sCsvData(18, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(19, iCount))
		sFax = ScrubForSql(sCsvData(20, iCount))
		sAddress = ScrubForSql(sCsvData(21, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(22, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(23, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(24, iCount)))
		sZipcode = ScrubForSql(sCsvData(25, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(26, iCount))
	
		'Check if this is a blank array entry
		if sSsn = "" and sFirstName = "" and sLastName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			'Response.Write("BlankSkip<br>")
			
		else	

			'Check to see if we have an existing officer with this information
			'already in the system.  If so, we'll want to update rather than
			'create a new officer listing.  We'll search for other existing
			'users with the same SSN and CompanyId.
			set oAssociate = nothing
			set oAssociate = new Associate
			oAssociate.ConnectionString = application("sDataSourceName")
			oAssociate.TpConnectionString = application("sTpDataSourceName")
			oAssociate.VocalErrors = application("bVocalErrors")
			oAssociate.CompanyId = session("UserCompanyId")
			oAssociate.Ssn = sSsn
			set oRs = oAssociate.SearchAssociates
			if not (oRs.EOF and oRs.BOF) then
			
				if oAssociate.LoadAssociateById(oRs("UserId")) = 0 then
				
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Failed to load the existing officer information.<br>")
					
				else

					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					if bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					
						bErrors = true
						Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): This officer currently belongs to another branch.<br>")
					
					else
					
						bUpdate = true
					
					end if 
			
				end if 
			
			end if 
			
			
			'Assign the properties to this officer object
			oAssociate.FirstName = sFirstName
			oAssociate.MidInitial = sMidInitial
			oAssociate.LastName = sLastName
			oAssociate.Ssn = sSsn
			oAssociate.DateOfBirth = dDateOfBirth
			oAssociate.DriversLicenseStateId = iDriversLicenseStateId
			oAssociate.DriversLicenseNo = sDriversLicenseNo
			if iBranchId <> 0 then
				oAssociate.BranchId = iBranchId
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			elseif iCompanyL3Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = iCompanyL3Id
			elseif iCompanyL2Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = iCompanyL2Id
				oAssociate.CompanyL3Id = ""
			else
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			end if				
			oAssociate.HireDate = dHireDate
			oAssociate.TerminationDate = dTerminationDate
			oAssociate.EmployeeId = sEmployeeId
			oAssociate.Title = sTitle
			oAssociate.Department = sDepartment
			oAssociate.Email = sEmail
			oAssociate.Phone = sPhone
			oAssociate.PhoneExt = sPhoneExt
			oAssociate.Phone2 = sPhone2
			oAssociate.Phone2Ext = sPhone2Ext
			oAssociate.Phone3 = sPhone3
			oAssociate.Phone3Ext = sPhone3Ext
			oAssociate.Fax = sFax
			oAssociate.Address = sAddress
			oAssociate.Address2 = sAddress2
			oAssociate.City = sCity
			oAssociate.StateId = iStateId
			oAssociate.Zipcode = sZipcode
			oAssociate.ZipcodeExt = sZipcodeExt
	
			oAssociate.UserStatus = 1
			oAssociate.Inactive = 0
			oAssociate.OutOfStateOrig = 0

				
			'Attempt to create a new record for this officer.
			if not bErrors and not bBlankSkip then
	
				if oAssociate.SaveAssociate <> 0 then
					
					if bUpdate then
					
						Response.Write("<li>Updated " & oAssociate.FullName & "<br>")
				
					else
				
						Response.Write("<li>Added " & oAssociate.FullName & "<br>")
						
					end if 
					 			
				else
				
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Could not save the officer information.<br>")
				
				end if		
	
			end if 
			
		end if 
	
	elseif iRecordType = 2 then	
	
		'RecordType 2 means this is a license record

		'Pull the license values from the array
		sLastName = ScrubForSql(sCsvData(1, iCount))
		sSsn = ScrubForSql(sCsvData(2, iCount))
		sLicenseNum = ScrubForSql(sCsvData(3, iCount))
		iLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(4, iCount)))
		'iLicenseTypeId = oLicense.LookupLicenseTypeIdByName(ScrubForSql(sCsvData(5, iCount)))
		sLicenseType = ScrubForSql(sCsvData(5, iCount))
		iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(6, iCount)))
		dLicenseExpDate = ScrubForSql(sCsvData(7, iCount))
		
		'Response.Write(sLastName & "," & sSsn & "," & sLicenseNum & "," & iLicenseStateId & "," & iLicenseTypeId & "," & iLicenseStatusId & ",x" & sCsvData(6, iCount) & "x<br>")
		
	
		'Check if this is a blank array entry
		if sSsn = "" and sLastName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif sLicenseNum = "" or iLicenseStateId = "" or iLicenseStatusId = "" or dLicenseExpDate = "" then
		
			bErrors = true
			Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Missing or invalid license information.<br>")
			
		else
	
	
			'Verify that the license is meant for a valid officer record to which
			'the user has access.
			if not oAssociate.LoadAssociateByLastNameSsn(sSsn, sLastName, session("UserCompanyId")) = 0 then
		
				if oAssociate.CompanyId <> session("UserCompanyId") then

					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this officer record.<br>")
					
				'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
				elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): This officer currently belongs to another branch.<br>")

				end if
		
			else

				bErrors = true
				Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to locate this officer record.<br>")
		
			end if 
	
	
			'Assign the properties to this officer object
			if not bErrors and not bBlankSkip then
			
				'Check to see if we have an existing license with this
				'information already in the system.  If so, we'll want to 
				'update rather than create a new listing.  We'll search for 
				'other existing licenses with the same Owner/State/Number.
				oLicense.ReleaseLicense
				oLicense.LicenseNum = sLicenseNum
				oLicense.OwnerTypeId = 1
				oLicense.OwnerCompanyId = session("UserCompanyId")
				oLicense.OwnerId = oAssociate.UserId
				oLicense.LicenseStateId = iLicenseStateId
				set oRs = oLicense.SearchLicenses
				if not (oRs.EOF and oRs.BOF) then
				
					if oLicense.LoadLicenseById(oRs("LicenseId")) = 0 then
				
						bErrors = true
						Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to laod the existing license information.<br>")
			
					else
					
						bUpdate = true
				
					end if 
				
				else
				
				end if 				
				
				oLicense.LicenseStatusId = iLicenseStatusId
				'oLicense.LicenseTypeId = iLicenseTypeId
				oLicense.LicenseType = sLicenseType
				oLicense.LicenseExpDate = dLicenseExpDate
				
				if oLicense.SaveLicense <> 0 then
					
					if bUpdate then
					
						Response.Write("<li>Updated License #" & oLicense.LicenseNum & " for " & oAssociate.FullName & "<br>")
					
					else
					
						Response.Write("<li>Added License #" & oLicense.LicenseNum & " for " & oAssociate.FullName & "<br>")
					
					end if 
					
				else
					
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to save the license information.<br>")
					
				end if
				
			end if 
			
		end if 

	elseif iRecordType = 3 then	
	
		'RecordType 3 means this is a course record
		
		'Pull the course values from the array
		sLastName = ScrubForSql(sCsvData(1, iCount))
		sSsn = ScrubForSql(sCsvData(2, iCount))
		iProviderId = oCourse.LookupProviderId(ScrubForSql(sCsvData(3, iCount)), session("UserCompanyId"))
		sCourseName = ScrubForSql(sCsvData(4, iCount))
		iCourseHours = ScrubForSql(sCsvData(5, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(6, iCount)))
		dCompletionDate = ScrubForSql(sCsvData(7, iCount))
		dExpDate = ScrubForSql(sCsvData(8, iCount))

	
		'Check if this is a blank array entry
		if sSsn = "" and sLastName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif iProviderId = "" then
	
			'Verify that we got at least a SSN and Last Name, which we'll need to
			'look up the officer.
			
			bErrors = true
			Response.Write("<li>Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid provider information.<br>")
			
		elseif sCourseName = "" or iCourseHours = "" or iStateId = "" or dCompletionDate = "" or dExpDate = "" then
		
			bErrors = true
			Response.Write("<li>Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid course information.<br>")
			
		else
	
			'Attempt to load a course based on the passed course information.
			oCourse.ReleaseCourse
			oCourse.Name = sCourseName
			oCourse.ProviderId = iProviderId
			oCourse.CompanyId = session("UserCompanyId")
			oCourse.ContEdHours = iCourseHours
			oCourse.StateId = iStateId
			set oRs = oCourse.SearchCourses
			if not (oRs.EOF and oRs.BOF) then
				
				if oCourse.LoadCourseById(oRs("CourseId")) = 0 then
				
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to load this course record.<br>")
				
				end if
			
			else
								
				bErrors = true
				Response.Write("<li>Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to locate this course record.<br>")
		
			end if 
	
	
			'Verify that the course is meant for a valid officer record to which
			'the user has access.
			if not bErrors and not bBlankSkip then 
			
				if not oAssociate.LoadAssociateByLastNameSsn(sSsn, sLastName, session("UserCompanyId")) = 0 then
		
					if oAssociate.CompanyId <> session("UserCompanyId") then

						bErrors = true
						Response.Write("<li>Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to load this officer record.<br>")
						
					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
						
						bErrors = true
						Response.Write("<li>Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): This officer currently belongs to another branch.<br>")

					end if
		
				else

					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to locate this officer record.<br>")
		
				end if 
				
			end if 
		
		
			'Assign the properties based on our passed parameters
			if not bErrors and not bBlankSkip then
			
				if oAssociate.AddCourse(oCourse.CourseId, dExpDate, dCompletionDate) = 0 then
	
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to save the course information.<br>")
					
				else
		
					Response.Write("<li>Added Course """ & oCourse.Name & """ for " & oAssociate.FullName & "<br>") 
		
				end if 
				
			end if 
		
		end if

	
	end if
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false
	
next 

'Clean up
set oAssociate = nothing

if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The loan officer list was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/Course.Class.asp" --------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1


'Make sure we were passed a candidate ID so we have something useful to do with this page.
dim oCompany
dim iCompanyId
iCompanyId = session("UserCompanyId")


set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(iCompanyId) = 0 then

	Response.Write("Error: A valid Company ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if


'Declare and initialize the branch object
dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")



'Make sure we were passed a valid AssociateId
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
iAssociateId = ScrubForSql(request("id"))

if oAssociate.LoadAssociateById(iAssociateID) = 0 then

	AlertError("This page requires a valid Officer ID.")
	Response.End
	
elseif oAssociate.CompanyId <> session("UserCompanyId") then
	
	AlertError("This page requires a valid Officer ID.")
	Reponse.End
	
elseif CheckIsBranchAdmin() then

	if not CheckIsThisBranchAdmin(oAssociate.BranchId) then
	
		AlertError("This page requires a valid Officer ID.")
		Response.End
		
	end if
	
end if 


'Initialize associate docs object
dim oAssociateDoc 
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmOfficerDelete(p_iCandidateId,p_sCandidateName)
{
	if (confirm("Are you sure you wish to delete the following Officer:\n  " + p_sCandidateName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficer.asp?id=' + p_iCandidateId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletenote.asp?id=' + p_iNoteId;
}
function ConfirmCourseAssocDelete(p_iAssocId, p_iOfficerId)
{
	if (confirm("Are you sure you wish to delete this Course record?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficercourse.asp?aid=' + p_iAssocId + '&id=' + p_iOfficerId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}

</script>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Officer Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <% = oAssociate.FirstName & " " & oAssociate.LastName %></td>
				</tr>		

						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modofficer.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"> &nbsp;Edit Officer</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmOfficerDelete(<% = oAssociate.UserId %>,'<% = oAssociate.FirstName & " " & oAssociate.LastName %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This Officer" alt="Delete This Officer"> Delete Officer</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="emailofficer.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnMail.gif" align="top" border="0" title="Email Notices" alt="Email Notices"> &nbsp;Email Notices</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>SSN: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Ssn %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Date Of Birth: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.DateOfBirth %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Drivers License State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = GetStateName(oAssociate.DriversLicenseStateId, 0) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Drivers License Number: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.DriversLicenseNo %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Hire Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HireDate %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Termination Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.TerminationDate %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Employee ID: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.EmployeeId %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Title: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Title %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Department: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Department %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Email %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone %> <% if oAssociate.PhoneExt <> "" then %> Ext. <% = oAssociate.PhoneExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone 2: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone2 %> <% if oAssociate.Phone2Ext <> "" then %> Ext. <% = oAssociate.Phone2Ext %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone 3: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone3 %> <% if oAssociate.Phone3Ext <> "" then %> Ext. <% = oAssociate.Phone3Ext %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Fax %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Cell Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.CellPhone %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Home Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HomePhone %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Home Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HomeEmail %></td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
									</table>
											</td>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0" valign="top">
										<tr>
											<td valign="top" class="newstitle" nowrap>Branch: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											if oAssociate.BranchId <> "" and oAssociate.BranchId <> 0 then 
											
												oBranch.LoadBranchById(oAssociate.BranchId)
												Response.Write(oBranch.Name)
												
											else
												
												Response.Write("None")
											
											end if 
											%>
											</td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Work Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Address %><br>
											<% if not oAssociate.Address2 = "" then %><% = oAssociate.Address2 %><br><% end if %>
											<% = oAssociate.City %><br>
											<% = GetStateName(oAssociate.StateId, 0) %><br>
											<% = oAssociate.Zipcode %><% if oAssociate.ZipcodeExt <> "" then %> - <% oAssociate.ZipcodeExt %><% end if %>
											</td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										oAssociateDoc.OfficerId = oAssociate.UserId
										set oRs = oAssociateDoc.SearchDocs
										
										'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document"></a>&nbsp;&nbsp;<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>"><% = oAssociateDoc.DocName %></a><br>													
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<a href="moddocument.asp?id=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											</td>
										</tr>											
									</table>
										</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
						
			
			<!-- Officer Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Licenses</td>
				</tr>

						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modlicense.asp?oid=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
							<tr>
								<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>
<%
dim oLicense
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oAssociate.UserId
oLicense.OwnerTypeId = 1


set oRs = oLicense.SearchLicenses()

if not (oRs.EOF and oRs.BOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>State</b></td>
		<td><b>License Number</b></td>
		<td><b>License Type</b></td>
		<td><b>License Status</b></td>
		<td><b>Expiration Date</b></td>
	</tr>
<%
	do while not oRs.EOF

		if oLicense.LoadLicenseById(oRs("LicenseId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then
				Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
				Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oLicense.LookupState(oLicense.LicenseStateId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LicenseNum & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LicenseType & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LicenseExpDate)
			if oLicense.LicenseExpDate < now() + 120 and oLicense.LicenseExpDate > now() then
				Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicense.LicenseExpDate - now(), 0) & " Days</font>)</b>")
			end if
			Response.Write("</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Licenses found.")
	
end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						
			<!-- Officer Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Notes</td>
				</tr>
						<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modnote.asp?oid=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oAssociate.UserId
oNote.OwnerTypeId = 1

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
sColor = ""
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="400"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if (CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId)) and (oNote.UserId = session("User_Id") or session("Access_Level") = 1) then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			elseif CheckIsAdmin() then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & month(oNote.NoteDate) & "/" & day(oNote.NoteDate) & "/" & year(oNote.NoteDate) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>





			<!-- Officer Course History -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Education History</td>
				</tr>
				<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId) then %>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td><a href="<% = application("URL") %>/officers/modofficercourse.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Course" alt="Add New Course"> New Course</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<% end if %>
				<tr>
					<td class="bckRight">
						<table width="100%" border="0" cellpadding="15" cellspacing="0">
							<tr>
								<td>
<%
dim oRs
dim oCourseInfoRs
dim oCourse

set oCourse = new Course
oCourse.VocalErrors = application("bVocalErrors")
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")

set oRs = oAssociate.GetCourses

'dim sColor
sColor = ""

if oRs.State <> 0 then

	if not oRs.EOF then
		bHasContent = true
	%>
									<table width="100%" cellpadding="5" cellspacing="0" border="0">
										<tr>
											<% if CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchID) then %>
											<td width="25"></td>
											<td></td>
											<% end if %>
											<td><b>Course</b></td>
											<td><b>Provider</b></td>
											<td><b>Date</b></td>
											<td><b>CE Hours</b></td>
											<td><b>Exp Date</b></td>
											<td><b>Certificate</b></td>
										</tr>
	<%
	else
	%>
	No Courses found.
	<%
	end if 

	do while not oRs.EOF


		if oCourse.LoadCourseById(oRs("CourseId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if

			Response.Write("<tr>" & vbCrLf)
			if (CheckIsAdmin() or CheckIsThisBranchAdmin(oAssociate.BranchId)) and (oCourse.CourseId < 0) then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modofficercourse.asp?id=" & oAssociate.UserId & "&aid=" & oRs("Id") & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Education Record"" alt=""Edit This Education Record ""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmCourseAssocDelete(" & oRs("Id") & ", " & oAssociate.UserId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Education Record"" alt=""Delete This Education Record""></a></td>" & vbCrLf)
			elseif CheckIsAdmin() then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oCourse.Name & "</td>" & vbCrLf)
			if oCourse.ProviderId <> "" then 
				Response.Write("<td " & sColor & ">" & oCourse.LookupProvider(oCourse.ProviderId) & "</td>" & vbCrLf)
			else
				Response.Write("<td " & sColor & ">TrainingPro</td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oRs("CompletionDate") & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oCourse.ContEdHours & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oRs("CourseExpirationDate") & "</td>" & vbCrLf)
			if oCourse.CourseId > 0 then
				set oCourseInfoRs = oAssociate.GetTpCourse(oRs("Id"))
				if not oCourseInfoRs.EOF then
					if oCourseInfoRs("CertificateReleased") then
						Response.Write("<td " & sColor & "><a href=""/courses/viewcertificate.asp?id=" & oAssociate.UserId & "&usercourseid=" & oRs("Id") & """>View</a></td>")
					else
						Response.Write("<td " & sColor & "></td>")					
					end if
				else
					Response.Write("<td " & sColor & "></td>")
				end if 
			else
				Response.Write("<td " & sColor & "></td>")
			end if 
			Response.Write("</tr>" & vbCrLf)
		
		end if 

		oRs.MoveNext

	loop
	
	if bHasContent = true then
		%></table><%
	end if 

else

end if
%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>



							
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim bIsBranchAdmin
bIsBranchAdmin = CheckIsBranchAdmin()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
function ProviderChange()
{
	document.CourseForm.CourseId.value = '';	
									
	RefreshScreen();
}

function updateCourseValue()
{
	document.AddFileForm.CourseId.value = document.CourseForm.CourseId.value;
}

function RefreshScreen()
{
	document.CourseForm.ScreenRefresh.value = 1;
	document.CourseForm.action = "addcourseroster.asp";
	
	document.CourseForm.submit();	
}
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 


dim iCourseId
iCourseId = ScrubForSql(request("CourseId"))


dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

if iCourseId <> "" then
	iCourseId = oCourse.LoadCourseById(iCourseId)
	if iCourseId = 0 then

		Response.Write("Failed to load the Course information.")
		Response.End
		
	elseif iCourseId > 0 then
		
		Response.Write("This course record may not be edited.")
		Response.End
		
	end if
end if


%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.AddUserCSV, "File Name") ||
			!checkIsDate(FORM.CompletionDate, "Completion Date is a required field.") ||
			!checkIsDate(FORM.ExpirationDate, "Expiration Date is a required field.")
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add an Officer Education Listing</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CourseForm" action="" method="POST">
<input type="hidden" value="0" name="ScreenRefresh">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Provider: </td>
		<td align="left" valign="top">
			<% 
			if request("ScreenRefresh") <> "1" then 
				DisplayProvidersDropdown oCourse.ProviderId, iCompanyId, 0, 3, 0, "onChange=ProviderChange();"
			else
				DisplayProvidersDropdown ScrubForSql(request("Provider")), iCompanyId, 0, 1, 0, "onChange=ProviderChange();"
			end if 
			%>
		</td>
	</tr>
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Course: </td>
		<td align="left" valign="top">
			<%
			if oCourse.ProviderId = "" and ScrubForSql(request("Provider")) = "" then
				DisplayProviderCoursesDropdown oCourse.ProviderId, oCourse.CourseId, 1, 1
			elseif request("ScreenRefresh") <> "1" then
				DisplayProviderCoursesDropdown oCourse.ProviderId, oCourse.CourseId, 0, 1
			else
				DisplayProviderCoursesDropdown ScrubForSql(request("Provider")), oCourse.CourseId, 0, 1
			end if 
			%>
		</td>
	</tr>
</form>
<form name="AddFileForm" action="addcourserosterproc.asp" method="POST" enctype="multipart/form-data" onSubmit="updateCourseValue();return Validate(this);">
<input type="hidden" name="CourseId" value="99">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> CSV File: </td>
		<td align="left" valign="top">
			<input type="file" size="30" name="AddUserCSV" value=""><p>
		</td>
	</tr>			
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Completion Date: </td>
		<td align="left" valign="top">
			<input type="text" name="CompletionDate" value="" size="10" maxlength="10">
			<a href="javascript:show_calendar('AddFileForm.CompletionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('AddFileForm.CompletionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Expiration Date: </td>
		<td align="left" valign="top">
			<input type="text" name="ExpirationDate" value="" size="10" maxlength="10">
			<a href="javascript:show_calendar('AddFileForm.ExpirationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('AddFileForm.ExpirationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%
'set saRS = nothing
'set oCourseRs = nothing
set oCourse = nothing
'set oCompany = nothing
'set oAssociate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" -->
<%
	for iCount = 0 to ubound(sCSVData, 2)	
	
		'RecordType 3 means this is a course record
		
		'Pull the course values from the array
		sEmail = ScrubForSql(sCsvData(1, iCount))
		iProviderId = oCourse.LookupProviderId(ScrubForSql(sCsvData(2, iCount)), session("UserCompanyId"))
		sCourseName = ScrubForSql(sCsvData(3, iCount))
		rCourseHours = ScrubForSql(sCsvData(4, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(5, iCount)))
		dCompletionDate = ScrubForSql(sCsvData(6, iCount))
		dExpDate = ScrubForSql(sCsvData(7, iCount))

	
		'Check if this is a blank array entry
		if sEmail = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif iProviderId = "" then
	
			'Verify that we got at least a SSN and Last Name, which we'll need to
			'look up the officer.
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid provider information.<br>")
			
		elseif sCourseName = "" or rCourseHours = "" or iStateId = "" then
		
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid course information.<br>")
			
		else
	
			'Attempt to load a course based on the passed course information.
			oCourse.ReleaseCourse
			oCourse.Name = sCourseName
			oCourse.ProviderId = iProviderId
			oCourse.CompanyId = session("UserCompanyId")
			oCourse.ContEdHours = rCourseHours
			oCourse.StateId = iStateId
			set oRs = oCourse.SearchCourses
			if not (oRs.EOF and oRs.BOF) then
				
				if oCourse.LoadCourseById(oRs("CourseId")) = 0 then
				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to load this course record.<br>")
				
				end if
			
			else
								
				bErrors = true
				Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to locate this course record.<br>")
		
			end if 
			set oRs = nothing
	
			'Verify that the course is meant for a valid officer record to which
			'the user has access.
			if not bErrors and not bBlankSkip then 
			
				if not oAssociate.LoadAssociateByEmail(sEmail, session("UserCompanyId")) = 0 then
		
					if oAssociate.CompanyId <> session("UserCompanyId") then

						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to load this officer record.<br>")
						
					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
						
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): This officer currently belongs to another branch.<br>")

					end if
		
				else

					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to locate this officer record.<br>")
		
				end if 
				
			end if 
		
		
			'Assign the properties based on our passed parameters
			if not bErrors and not bBlankSkip then
				'Determine whether to Add the course to the associate's profile, or update the existing course in the associate's profile.
				
				'Check to see if the course exists in the associate's profile
				iAssocId = oAssociate.CourseExistsInProfile(oCourse.CourseId)
				
				if clng(iAssocId) = 0 then  'Course does not exist in profile so Add
					iSuccess = oAssociate.AddCourse(oCourse.CourseId, dExpDate, dCompletionDate)
					sAction = "Added"
				else 'Course exists in profile so update
					iSuccess = oAssociate.UpdateCourse(iAssocId, oCourse.CourseId, dExpDate, dCompletionDate)
					sAction = "Updated"
				end if
			
				if iSuccess = 0 then
	
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to save the course information.<br>")
					
				else
		
					Response.Write("<li>" & sAction & " Course """ & oCourse.Name & """ for " & oAssociate.FullName & "<br>") 	
				end if 
			end if 	
		end if

	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false

next 

'Clean up
set oAssociate = nothing

if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The import was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
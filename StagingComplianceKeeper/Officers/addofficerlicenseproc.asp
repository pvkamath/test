<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" -->
<%

'Used for temp table
dim sSQL2 
dim oLicenseTempActiveRS
dim olicenseTempLastRS

'From temp table
dim oTempHasActive
dim oTempActiveRecord  

'From database records in ComplianceKeeper    
dim oRsCKlicense
dim oRsCKActive 'Recordset object used for search results.

dim oCKhasActive
dim oCKactiveRecord
dim NoUpdate

for iCount = 0 to ubound(sCSVData, 2)	
		NoUpdate = false

        'RecordType 2 means this is a license record
        'Pull the license values from the array
        sNMLSNumber=ScrubForSql(sCsvData(0, iCount))
        sLastName=ScrubForSql(sCsvData(1, iCount))
        sFirstName=ScrubForSql(sCsvData(2, iCount))
        sMiddleName=ScrubForSql(sCsvData(3, iCount))
        sLicenseNum = ScrubForSql(sCsvData(5, iCount))
       	sLicenseType = ScrubForSql(sCsvData(6, iCount))       
        iLicenseStateId = GetStateIdByAbbrev(MID(slicenseType,1,2))            
        iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(8, iCount)))
        dLicenseStatusDate = ScrubForSql(sCsvData(9, iCount))
        iExpirationYear = ScrubForSQL(sCsvData(11, iCount))

        if IsNull(sLicenseNum) or IsEmpty(sLicenseNum) or sLicenseNum="" then
            sLicenseNum="0"
        end if
        'Response.Write(sLastName & "," & sSsn & "," & sLicenseNum & "," & iLicenseStateId & "," & iLicenseTypeId & "," & iLicenseStatusId & ",x" & sCsvData(6, iCount) & "x<br>")
			
		'Check if this is a blank array entry
        	
        if sNMLSNumber="" then
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true			
		elseif iLicenseStateId = "" or iLicenseStatusId = "" then
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & ")"  & " (StateID #" & iLicenseStateId & ") "  & " (StatusID #" & iLicenseStatusId & ") : Missing or invalid license information.<br>")			
		else
			'Verify that the license is meant for a valid officer record to which the user has access.
            'if not oAssociate.LoadAssociateByEmail(sEmail, session("UserCompanyId")) = 0 then    
			if not oAssociate.LoadAssociateByNMLS(sNMLSNumber, session("UserCompanyId")) = 0 then
				if oAssociate.CompanyId <> session("UserCompanyId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this officer record.<br>")
				'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
				elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): This officer currently belongs to another branch.<br>")
				end if
				iLicenseOwnerTypeId = 1
				iLicenseOwnerId = oAssociate.UserId
				sLicenseOwnerName = oAssociate.FullName
			else
                ' Need to add new officers here
                'iUserID=""
                set oAssociate = new Associate
                oAssociate.ConnectionString = application("sDataSourceName")
                oAssociate.TpConnectionString = application("sTpDataSourceName")
                oAssociate.VocalErrors = false
                
                oAssociate.CompanyId = session("UserCompanyId")
                oAssociate.FirstName = sFirstName
			    oAssociate.LastName = sLastName
                oAssociate.MiddleName = sMiddleName
                oAssociate.NMLSNumber =sNMLSNumber
                oAssociate.Email=sNMLSNumber
			    oAssociate.UserStatus = "1"   
                oAssociate.AssociateTypeID = 1            

				if oAssociate.SaveAssociate <> 0 then					
					Response.Write("<li>Added " & oAssociate.FullName & "<br>")											    
				else				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Could not save the officer information.<br>")				
				end if		
			    
                if not oAssociate.LoadAssociateByNMLS(sNMLSNumber, session("UserCompanyId")) = 0 then                    
                    iLicenseOwnerId = oAssociate.UserId
                    iLicenseOwnerTypeId = 1
                    sLicenseOwnerName = oAssociate.FullName
                else
                    bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this officer record.<br>")
                end if 
                '**************
				'bErrors = true
				'Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to locate this officer record.<br>")
		
			end if 
	
	        'stop
			'Assign the properties to this officer object
			if not bErrors and not bBlankSkip then			
				'Check to see if we have an existing license with this information already in the system.  If so, we'll want to 
				'update rather than create a new listing.  We'll search for other existing licenses with the same Owner/State/Number.
				oLicense.ReleaseLicense			
				oLicense.OwnerTypeId = iLicenseOwnerTypeId
				oLicense.OwnerCompanyId = session("UserCompanyId")
				oLicense.OwnerId = iLicenseOwnerId
				oLicense.LicenseStateId = iLicenseStateId

                'Since we are no longer checking number for match, we now need to check records manually for match                         
                oLicense.LicenseType = sLicenseType
              'stop
                set oRsCKlicense = oLicense.SearchLicensesV2
             	if not (oRsCKlicense.EOF and oRsCKlicense.BOF) then	
                    'Response.Write(sLicenseNum & "," & oRsCKlicense("LicenseType") & "," & sLicenseType & "," & iLicenseStatusId & ",x" & ScrubForSql(sCsvData(2, iCount)) & "x<br>")
                    dim tLicenseID 
                    tLicenseID=0
				    if oLicense.LoadLicenseById(oRsCKlicense("LicenseId")) = 0 then
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load the existing license information.<br>")		
					else	                                                   
                        '***** BEGIN CHECK TEMP TABLE to see if LO has an active license  *****
                        sSQL2 = "SELECT  LicenseId, NMLSNumber, LicenseStateID, LicenseName, lt.LicenseStatusPermanent FROM #tmpLicense "
                        sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        sSQL2 =  SSQL2 & "WHERE #tmpLicense.NMLSNumber= '" & sNMLSNumber & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseName= '" & sLicenseType & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "' "
                        sSQL2 =  SSQL2 & "AND lt.LicenseStatusPermanent= 0" 

                        set oLicenseTempActiveRS = CreateObject("ADODB.Recordset")
                        oLicenseTempActiveRS.Open  sSQL2, LicenseCnn,3,3
                        
                        oTempHasActive =false 
                        if oLicenseTempActiveRS.recordcount > 0 then	
                            'There is an active record
                            oTempHasActive=true
                        end if
                        oLicenseTempActiveRS.Close
                
                        '***** BEGIN CHECK TEMP TABLE for match and to 
                        'sSQL2 = "SELECT TOP 1 NMLSNumber, LicenseStateId, LicenseName, #tmpLicense.LicenseStatusID, LicenseStatusDate, lt.LicenseStatusPermanent FROM #tmpLicense "
                        'sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        'sSQL2 =  SSQL2 & "WHERE #tmpLicense.NMLSNumber= '" & sNMLSNumber & "' "
                        'sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseName= '" & sLicenseType & "' "
                        'sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "' "
                        'sSQL2 =  SSQL2 & "Order by LicenseStatusDate DESC" 

                        'set olicenseTempLastRS = CreateObject("ADODB.Recordset")
                        'olicenseTempLastRS.Open  sSQL2, LicenseCnn,3,3                

                        'if olicenseTempLastRS.recordcount = 0  then	
                        '    bErrors = true
						'    Response.Write("<li class=""error""> ERROR 2012:09<br>")		
                        'end if    
                      


                        '*****Is there an active record in ComplianceKeeper                    
                        oCKhasActive=false
                        NoUpdate=false
                        
                        set oRsCKActive = oLicense.SearchActiveLicense
                        if not (oRsCKActive.EOF and oRsCKActive.BOF) then	
                            oCKhasActive=true
                        end if 

                       dim firstTime
                       firstTime=true
                     

                       'LOOP through all licenses in the CK database to determine how to handle the records on we are evaulating fron the import
                       'We will be Adding or Updating to the database
                       'This is the inner loop inside of the import file
                       'If we are adding a record we will naturally exit the loop
                       
                       do while NOT oRsCKlicense.eof and bUpdate = false
                            oTempActiveRecord = false
                            oCKactiveRecord = false
                          ' stop
                            if oLicense.LookupLicenseStatusPermanent(iLicenseStatusId)=false then 
                                oTempActiveRecord=true
                            end if

                            if (oRsCKlicense("LicenseStatusPermanent"))=false then 
                                oCKactiveRecord=true
                            end if

                        
                            if oTempHasActive=true then
                                if oCKhasActive=true then
                                    if oTempActiveRecord=true and oCKactiveRecord=true then                                                                    
                                         bUpdate = true                                
                                         exit do         
                                    'elseif oTempActiveRecord=true and (cdate(dLicenseStatusDate)>(oRsCKlicense("LicenseStatusDate")) then               



                                    elseif oTempActiveRecord=false and ((oRsCKlicense("LicenseStatusDate"))=cdate(dLicenseStatusDate)) then 
                                        'found record, exit loop and do not update peramnent record
                                        NoUpdate=true
                                        exit do
                                    else
                                        ' Looped through and did not find match or record to oupdate.
                                        'NEW Record
                                    end if                                                                                       
                                elseif oCKhasActive=false then 
                                    if  oTempActiveRecord=true then
                                        exit do
                                    elseif (oRsCKlicense("LicenseStatusDate")=cdate(dLicenseStatusDate)) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'NEW Record
                                    end if                              
                                end if                            
                                                    
                            elseif oTempHasActive=false then                                                    
                                if oCKhasActive=true then
                                    if (oRsCKlicense("LicenseStatusDate"))=cdate(dLicenseStatusDate) then 
                                        'found record, exit loop and do not update peramnent record
                                        NoUpdate=true
                                        exit do

                                    elseif oCKactiveRecord=true and (cdate(dLicenseStatusDate) > (oRsCKlicense("LicenseStatusDate"))) then
                                        'We are on a peramnent with a date newer then active records in database.  New to Update 
                                        bUpdate = true                                
                                        exit do
                                     
                                    else
                                        ' Looped through and did not find match or record to oupdate.Must be old permanent record to add
                                        
                                    end if
                                elseif (oRsCKlicense("LicenseStatusDate"))=cdate(dLicenseStatusDate) then 
                                        NoUpdate=true
                                        exit do
                                else
                                    ' Looped through and did not find match or record to oupdate.Must be old permanent record to add
                                end if 
                            end if
                            if bUpdate=true or NoUpdate=true then
                                firstTime=false
                            end if
                            oRsCKlicense.MoveNext
                        loop
					end if 
                   
                    if bUpdate = true then
				        tLicenseID=oRsCKlicense("LicenseId")
                    end if
				else
				      'NEW Record
				end if
				
				if safecstr(oLicense.LicenseExpDate) <> safecstr(dLicenseExpDate) or _
                    safecstr(oLicense.LicenseStatusDate) <> safecstr(dLicenseStatusDate) or _
					safecstr(oLicense.LicenseAppDeadline) <> safecstr(dLicenseAppDeadline) or _
					safecstr(oLicense.LicenseStatusId) <> safecstr(iLicenseStatusId) then
					oLicense.Triggered30 = 0
					oLicense.Triggered60 = 0
					oLicense.Triggered90 = 0
				end if
				
                'Need to make sure license number is with oLicense
                oLicense.LicenseNum = sLicenseNum
				oLicense.LegalName = sLegalName
				oLicense.LicenseStatusId = iLicenseStatusId
               
				'oLicense.LicenseTypeId = iLicenseTypeId
				oLicense.LicenseType = sLicenseType		
                IF iLicenseStatusId= "2" THEN
                    if IsNumeric(iExpirationYear) then
                        oLicense.LicenseExpDate = "12/31/" & iExpirationYear
                    else
                        oLicense.LicenseExpDate = iExpirationYear
                    end if
                ELSE
                    oLicense.LicenseExpDate = dLicenseExpDate                  
                END IF 
                oLicense.LicenseStatusDate = dLicenseStatusDate
				oLicense.LicenseAppDeadline = dLicenseAppDeadline
				oLicense.Notes = sLicenseNotes
				             
                'Noupdate mead we are on a perminate license that shound not be updated                
                if NoUpdate=false then
				    if oLicense.SaveLicense(bUpdate,tLicenseID) <> 0 then
					
					    if bUpdate then					
						    Response.Write("<li>Updated License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")					
					    else					
						    Response.Write("<li>Added License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")					
					    end if 					
                        'Response.Write(sLastName & "," & sLicenseNum & "," & iLicenseStateId & "," & sLicenseType & "," & iLicenseStatusId & ",_" &  dLicenseExpDate & "_<br>")				
                    else					
					    bErrors = true
					    Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to save the license information.<br>")
					
				    end if
				end if 
			end if 
			
		end if 
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false
next 

'Clean up
set oAssociate = nothing

  
sCreateTable ="drop table #tmpLicense"
set licenseCmd = CreateObject("ADODB.Command")
licenseCmd.ActiveConnection = licenseCnn
licenseCmd.CommandText = sCreateTable
licenseCmd.Execute
set licenseCnn = nothing



if not bErrorsInList then
	Response.Write("</ul><p>Thank you.  The import was processed successfully.")
end if 
%>
</td>
</tr>
</table>
        </td></tr></table>
        </td></tr></table>
	    <img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
	    <!-- space-->
	    <table width="100%" border=0 cellpadding=0 cellspacing=0>
		    <tr><td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td></tr>
	    </table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		</tr>
		<tr><td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td></tr>
</table>
<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
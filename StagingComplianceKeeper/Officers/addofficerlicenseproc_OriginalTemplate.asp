<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" -->
<%

	
		'RecordType 2 means this is a license record

		'Pull the license values from the array
		sEmail = ScrubForSql(sCsvData(1, iCount))
		sLicenseNum = ScrubForSql(sCsvData(2, iCount))
		iLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		'iLicenseTypeId = oLicense.LookupLicenseTypeIdByName(ScrubForSql(sCsvData(5, iCount)))
		sLicenseType = ScrubForSql(sCsvData(4, iCount))
        if iLicenseStateId = "" then 
            iLicenseStateId = GetStateIdByAbbrev(MID(slicenseType,1,2))            
        end if 

		iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(5, iCount)))
		dLicenseExpDate = ScrubForSql(sCsvData(6, iCount))
		dLicenseAppDeadline = ScrubForSql(sCsvData(7, iCount))
		sLegalName = ScrubForSql(sCsvData(8, iCount))
		sLicenseNotes = ScrubForSql(sCsvData(9, iCount))
		
        if IsNull(sLicenseNum) or IsEmpty(sLicenseNum) or sLicenseNum="" then
            sLicenseNum="0"
        end if
        
		'Response.Write(sLastName & "," & sSsn & "," & sLicenseNum & "," & iLicenseStateId & "," & iLicenseTypeId & "," & iLicenseStatusId & ",x" & sCsvData(6, iCount) & "x<br>")
			
		'Check if this is a blank array entry
		'if sSsn = "" and sLastName = "" then
		if trim(sEmail) = "" then		
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif sLicenseNum = "" or iLicenseStateId = "" or iLicenseStatusId = "" then
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & ")"  & " (StateID #" & iLicenseStateId & ") "  & " (StatusID #" & iLicenseStatusId & ") : Missing or invalid license information.<br>")
			
		else
	
	
			'Verify that the license is meant for a valid officer record to which
			'the user has access.
			if not oAssociate.LoadAssociateByEmail(sEmail, session("UserCompanyId")) = 0 then
				if oAssociate.CompanyId <> session("UserCompanyId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this officer record.<br>")
				'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
				elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): This officer currently belongs to another branch.<br>")
				end if
				iLicenseOwnerTypeId = 1
				iLicenseOwnerId = oAssociate.UserId
				sLicenseOwnerName = oAssociate.FullName
		
			elseif not oCompany.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsAdmin() or (oCompany.CompanyId <> session("UserCompanyId")) then		
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyName") & " record.<br>")
				end if 
				iLicenseOwnerTypeId = 3
				iLicenseOwnerId = oCompany.CompanyId
				sLicenseOwnerName = oCompany.Name
							
			elseif not oCompanyL2.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsThisCompanyL2Admin(oCompanyL2.CompanyL2Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyL2Name") & " record.<br>")
				end if
				iLicenseOwnerTypeId = 4
				iLicenseOwnerId = oCompanyL2.CompanyL2Id
				sLicenseOwnerName = oCompanyL2.Name
				
			elseif not oCompanyL3.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsThisCompanyL3Admin(oCompanyL3.CompanyL3Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyL3Name") & " record.<br>")
				end if
				iLicenseOwnerTypeId = 5
				iLicenseOwnerId = oCompanyL3.CompanyL3Id
				sLicenseOwnerName = oCompanyL3.Name
			
			elseif not oBranch.LoadBranchByName(sEmail) = 0 then
				if not CheckIsThisBranchAdmin(oBranch.BranchId) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this Branch record.<br>")
				end if
				iLicenseOwnerTypeId = 2
				iLicenseOwnerId = oBranch.BranchId
				sLicenseOwnerName = oBranch.Name
				
			else

				bErrors = true
				Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to locate this officer record.<br>")
		
			end if 
	
	
			'Assign the properties to this officer object
			if not bErrors and not bBlankSkip then			
				'Check to see if we have an existing license with this information already in the system.  If so, we'll want to 
				'update rather than create a new listing.  We'll search for other existing licenses with the same Owner/State/Number.
				oLicense.ReleaseLicense			
				oLicense.OwnerTypeId = iLicenseOwnerTypeId
				oLicense.OwnerCompanyId = session("UserCompanyId")
				oLicense.OwnerId = iLicenseOwnerId
				oLicense.LicenseStateId = iLicenseStateId

                'Since we are no longer checking number for match, we now need to check records manually for match                         
                oLicense.LicenseType = sLicenseType

                set oRs = oLicense.SearchLicensesV2
             	if not (oRs.EOF and oRs.BOF) then	
                   'Response.Write(sLicenseNum & "," & oRs("LicenseType") & "," & sLicenseType & "," & iLicenseStatusId & ",x" & ScrubForSql(sCsvData(2, iCount)) & "x<br>")
                   dim tLicenseID 
                   tLicenseID=0
					if oLicense.LoadLicenseById(oRs("LicenseId")) = 0 then
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load the existing license information.<br>")		
					else	                                                   
                        'CHECK TEMP TABLE for an active record
                        hasActive =false

                        sSQL2 = "SELECT  LicenseId, Email, LicenseStateId, LicenseType, lt.LicenseStatusPermanent FROM #tmpLicense "
                        sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        sSQL2 =  SSQL2 & "WHERE #tmpLicense.Email= '" & sEmail & "'"
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseType= '" & sLicenseType & "'"
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "'"
                        sSQL2 =  SSQL2 & "AND lt.LicenseStatusPermanent= 0" 

                        set licenseRs = CreateObject("ADODB.Recordset")
                        licenseRs.Open  sSQL2, LicenseCnn
                      
                        if not (licenseRs.EOF and licenseRs.BOF) then	
                            'There is an active record
                            hasActive=true
                        end if
                        licenseRs.Close

                      
                        sSQL2 = "SELECT  TOP 1 Email, LicenseStateId, LicenseType, #tmpLicense.LicenseStatusID, LicenseExpDate, lt.LicenseStatusPermanent FROM #tmpLicense "
                        sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        sSQL2 =  SSQL2 & "WHERE #tmpLicense.Email= '" & sEmail & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseType= '" & sLicenseType & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "' "
                        sSQL2 =  SSQL2 & "Order by LicenseExpDate DESC" 

                        set licenseRs = CreateObject("ADODB.Recordset")
                        licenseRs.Open  sSQL2, LicenseCnn
                      
                        if licenseRs.recordcount <> -1 then	
                            bErrors = true
						    Response.Write("<li class=""error""> ERROR 2012:09<br>")		
                         end if    
                                                            
                        'is there an active record in ComplianceKeeper    
                        dim hasActiveCK
                       

                        hasActiveCK=false
                        NoUpdate=false
                        
                        set oRsActive = oLicense.SearchActiveLicense
                        if not (oRsActive.EOF and oRsActive.BOF) then	
                            hasActiveCK=true
                        end if 

                        'stop
                       dim firstTime
                       firstTime=true

                       do while NOT oRs.eof and bUpdate = false
                            onActive = false
                            foundActive = false
                           
                            if oLicense.LookupLicenseStatusPermanent(iLicenseStatusId)=false then 
                                onActive=true
                            end if

                            if (oRs("LicenseStatusPermanent"))=false then 
                                foundActive=true
                            end if

                          
                            if hasActive=true then
                                if hasActiveCK=true then
                                    if onActive=true and foundActive=true then                                
                                         bUpdate = true                                
                                         exit do
                                    elseif onActive=false and ((oRs("LicenseExpDate"))=cdate(dLicenseExpDate)) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'NEW Record
                                    end if                                                                                       
                                elseif hasActiveCK=false then 
                                    if  onActive=true then
                                        exit do
                                    elseif (oRs("LicenseExpDate")=cdate(dLicenseExpDate)) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'NEW Record
                                    end if                              
                                end if 
                           
                                                    
                            elseif hasActive=false then   
                                                 
                                if hasActiveCK=true then
                                    if (licenseRs("LicenseExpDate")=cdate(dLicenseExpDate)) and foundActive=true then
                                        bUpdate = true                                
                                        exit do
                                     elseif (oRs("LicenseExpDate"))=cdate(dLicenseExpDate) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'stop
                                    end if
                                elseif (oRs("LicenseExpDate"))=cdate(dLicenseExpDate) then 
                                        NoUpdate=true
                                        exit do
                                else
                                    'NEW Record
                                end if 
                            end if
                            if bUpdate=true or NoUpdate=true then
                                firstTime=false
                            end if

                            oRs.MoveNext

                        loop



					end if 
                   
                    if bUpdate = true then
				        tLicenseID=oRs("LicenseId")
                    end if
				else
				      'NEW Record
				end if
				
				if safecstr(oLicense.LicenseExpDate) <> safecstr(dLicenseExpDate) or _
					safecstr(oLicense.LicenseAppDeadline) <> safecstr(dLicenseAppDeadline) or _
					safecstr(oLicense.LicenseStatusId) <> safecstr(iLicenseStatusId) then
					oLicense.Triggered30 = 0
					oLicense.Triggered60 = 0
					oLicense.Triggered90 = 0
				end if
				
                'Need to make sure license number is with oLicense
                oLicense.LicenseNum = sLicenseNum

				oLicense.LegalName = sLegalName
				oLicense.LicenseStatusId = iLicenseStatusId
				'oLicense.LicenseTypeId = iLicenseTypeId
				oLicense.LicenseType = sLicenseType
				oLicense.LicenseExpDate = dLicenseExpDate
				oLicense.LicenseAppDeadline = dLicenseAppDeadline
				oLicense.Notes = sLicenseNotes
				                
                'Noupdate mead we are on a perminate license that shound not be updated                
                if NoUpdate=false then
				    if oLicense.SaveLicense(bUpdate,tLicenseID) <> 0 then
					
					    if bUpdate then					
						    Response.Write("<li>Updated License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")					
					    else					
						    Response.Write("<li>Added License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")					
					    end if 					
                        'Response.Write(sLastName & "," & sLicenseNum & "," & iLicenseStateId & "," & sLicenseType & "," & iLicenseStatusId & ",_" &  dLicenseExpDate & "_<br>")				
                    else					
					    bErrors = true
					    Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to save the license information.<br>")
					
				    end if
				end if 
			end if 
			
		end if 
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false
next 

'Clean up
set oAssociate = nothing
licenseRs.Close
licenseCnn.Close

if not bErrorsInList then
	Response.Write("</ul><p>Thank you.  The import was processed successfully.")
end if 
%>
</td>
</tr>
</table>
        </td></tr></table>
        </td></tr></table>
	    <img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
	    <!-- space-->
	    <table width="100%" border=0 cellpadding=0 cellspacing=0>
		    <tr><td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td></tr>
	    </table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		</tr>
		<tr><td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td></tr>
</table>
<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
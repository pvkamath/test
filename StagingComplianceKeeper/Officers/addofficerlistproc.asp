<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"

server.ScriptTimeout = 6000


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim bIsBranchAdmin
bIsBranchAdmin = CheckIsBranchAdmin


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Course information variables
dim sFirstName
dim sMiddleName
dim sLastName
dim sSsn
dim dDateOfBirth
dim sNMLSNumber
dim iDriversLicenseStateId
dim sDriversLicenseNo
dim iBranchId
dim iCompanyL2Id
dim iCompanyL3Id
dim dHireDate
dim dTerminationDate
dim sManager
dim sEmployeeId
dim sTitle
dim sDepartment
dim sEmail
dim sPhone
dim sPhoneExt
dim sPhone2
dim	sPhone2Ext
dim sPhone3
dim sPhone3Ext
dim sFax
dim sAddress
dim sAddress2
dim sCity
dim iStateId
dim sZipcode
dim sZipcodeExt
dim sCellPhone
dim sHomePhone
dim sHomeEmail
dim sBranchNumber
dim sLegalName
dim iIncorpStateId
dim dIncorpDate
dim iOrgTypeId
dim sEin
dim sEntityLicNum
'dim sFhaLicenseNum
dim sFhaBranchId
dim sVaBranchId

dim aOfficerCustField(14)
dim iCurField 

dim	sMailingAddress
dim	sMailingAddress2
dim	sMailingCity
dim	iMailingStateId
dim	sMailingZipcode
dim	sMailingZipcodeExt
dim	sContactName
dim	sContactPhone
dim	sContactPhoneExt
dim	sContactEmail
dim	sContactFax



dim sLicenseNum
dim iLicenseStateId
dim iLicenseTypeId
dim sLicenseType
dim iLicenseStatusId
dim dLicenseExpDate
dim dLicenseAppDeadline
dim iLicenseOwnerTypeId
dim iLicenseOwnerId
dim sLicenseOwnerName
dim sLicenseNotes

dim iProviderId
dim sCourseName
dim rCourseHours
dim dCompletionDate
dim dExpDate
dim i
dim iAssocId
dim iSuccess
dim sAction

'We need to find and parse the binary information uploaded
dim iBinFormData
dim sFormData
dim iCount


'Used to seach the temp table
dim sSQL2 
dim hasActive
dim onActive  
dim foundActive
dim NoUpdate

iBinFormData = Request.BinaryRead(Request.TotalBytes)
	
for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next

	
dim sFormArray
sFormArray = split(sFormData, "-----------------------------")
	
dim sName
dim sValue
dim iLocStart
dim iLocEnd
	
dim sForm()
	
dim sCSVLines
dim sCSVSingleLine
dim sCSVData
dim iLineCount
dim iCountCols

for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next
	
dim oRegExp
dim sRegExpMatches
dim oMatch
set oRegExp = new RegExp
oRegExp.Pattern = ",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))"

'Response.Write("<p>" & oRegExp.Pattern & "<p>")

oRegExp.IgnoreCase = true
oRegExp.Global = true


'dim iCountCols
dim iCharCount
dim sCurrentField
dim sCurrentChar
dim bInQuotes

iCountCols = 0
iCharCount = 0

'Loop through the array of form values to parse out the data.
for iCount = 0 to ubound(sFormArray)
	
	'Response.Write(sFormArray(iCount) & "<p><br>")
	if instr(1, sFormArray(iCount), "name=") > 0 then
		'Response.Write(mid(sFormArray(iCount), instr(1, sFormArray(iCount), "name=")) & "<br>")
		iLocStart = instr(1, sFormArray(iCount), "name=""") + 6
		iLocEnd = instr(iLocStart, sFormArray(iCount), """")
		sName = mid(sFormArray(iCount), iLocStart, iLocEnd - iLocStart)
			
		'Find the CSV file within the data
		if sName = "AddUserCSV" then 
			
			'+28 accounts for mime type text and two CR/LF pairs.
			sValue = mid(sFormArray(iCount), instr(1, sFormArray(iCount), "Content-Type: ") + 14)
			
            'response.write("X" & sValue & "X, " & instr(1, sValue, vbCrLf) & "<br>")

			sValue = mid(sValue, instr(1, sValue, vbCrLf) + 4)
			sValue = left(sValue, len(sValue) - 2)
			
            'Response.Write("'" & sName & "' : '" & sValue & "'<p>")
				
			sCSVLines = split(sValue, vbCrLf)
				
			redim sCSVData(43, ubound(sCSVLines))
				
			for iLineCount = 0 to ubound(sCSVLines)

				if len(sCsvLines(iLineCount)) > 5 and mid(sCsvLines(iLineCount), 1, 3) <> ",,," and mid(sCsvLines(iLineCount), 1, 10) <> "First Name" and mid(sCsvLines(iLineCount), 1, 24) <> "License Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Course Identifier Field" and mid(sCsvLines(iLineCount), 1, 24) <> "Company Identifier Field" and mid(sCsvLines(iLineCount), 1, 25) <> "Division Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Region Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Branch Identifier Field" then
			
					'Parse through the line and split at commas or quotation marks.
					for iCharCount = 1 to len(sCSVLines(iLineCount))
				
						sCurrentChar = mid(sCsvLines(iLineCount), iCharCount, 1)
						if sCurrentChar = """" then
							if bInQuotes then
								bInQuotes = false
							else
								bInQuotes = true
							end if
						elseif (sCurrentChar = "," and not bInQuotes) or sCurrentChar = chr(10) then
							'We're done with a field.  Wrap it up.
							
							'Response.Write(sCurrentField & "(" & iCountCols & ")" & "|")
							
							sCSVData(iCountCols, iLineCount) = sCurrentField
							
							iCountCols = iCountCols + 1	
							sCurrentField = ""	
						else
							sCurrentField = sCurrentField & mid(sCsvLines(iLineCount), iCharCount, 1)
						end if					
					
					next
					
					'Wrap up the leftovers...
					'Response.Write(sCurrentField & "X|X")
					sCsvData(iCountCols, iLineCount) = sCurrentField
					iCountCols = 0
					sCurrentField = ""
					
					'Response.Write("<p>")

''				sCSVSingleLine = split(sCSVLines(iLineCount), ",")
	
				'Response.Write(sCsvLines(iLineCount) & "<br>")
				'response.write(uBound(sCSVSingleLine) & "<br>")				

''				if ubound(sCSVSingleLine) >= 3 then
				
'					set sRegExpMatches = oRegExp.Execute(trim(sCsvSingleLine(iCountCols)))
'						
'					'Print the # of matches we found
'					Response.Write sRegExpMatches.Count & " matches found...<P>"
'						
'					'Step through our matches 
'					For Each oMatch in sRegExpMatches 
'						Response.Write oMatch.Value & " | "
'					Next 
'						
'					'Clean up 
'					Set oMatch = Nothing 
'				
''					for iCountCols = 0 to ubound(sCsvSingleLine)
'					
'						'if not instr(1, sCsvSingleLine(iCountCols), """") >= 1 then
'						'
'						'	if not instr(instr(1, sCsvSingleLine(iCountCols), """"), sCsvSingleLine(iCountCols), """") >= 1 then
'						''	
'						'		do while not bFoundQoute
'						'		
'						'			if 
'						'		
'						'		loop
'				
'						Response.Write("L" & iCountCols & ": " & trim(sCSVSingleLine(iCountCols)) & "<br>")
''						sCSVData(iCountCols, iLineCount) = trim(sCSVSingleLine(iCountCols))
						'Response.Write("L" & iCountCols & ": " & sCsvData(iCountCols, iLineCount) & "<br>")
'					
''					next
					
				'Response.Write(sCSVData(0, iLineCount) & " " & sCSVData(1, iLineCount) & ": " & sCSVData(2, iLineCount) & "<p>")
				
				else
				
					'Response.Write("Making Blank Line...<br>")
					
					'We'll get here if we get some line in our CSV that's not in
					'the correct format.  Rather than bother with removing just
					'that line from the array, we'll just make a blank entry that
					'we can ignore later.
					sCsvData(0, iLineCount) = ""
					sCsvData(1, iLineCount) = ""
					sCsvData(2, iLineCount) = ""
					sCsvData(3, iLineCount) = ""
					sCsvData(4, iLineCount) = ""
					sCsvData(5, iLineCount) = ""
					sCsvData(6, iLineCount) = ""
					sCsvData(7, iLineCount) = ""
					sCsvData(8, iLineCount) = ""
					sCsvData(9, iLineCount) = ""
					sCsvData(10, iLineCount) = ""
					sCsvData(11, iLineCount) = ""
					sCsvData(12, iLineCount) = ""
					sCsvData(13, iLineCount) = ""
					sCsvData(14, iLineCount) = ""
					sCsvData(15, iLineCount) = ""
					sCsvData(16, iLineCount) = ""
					sCsvData(17, iLineCount) = ""
					sCsvData(18, iLineCount) = ""
					sCsvData(19, iLineCount) = ""
					sCsvData(20, iLineCount) = ""
					sCsvData(21, iLineCount) = ""
					sCsvData(22, iLineCount) = ""
					sCsvData(23, iLineCount) = ""
					sCsvData(24, iLineCount) = ""
					sCsvData(25, iLineCount) = ""
					sCsvData(26, iLineCount) = ""
					sCsvData(27, iLineCount) = ""
					sCsvData(28, iLineCount) = ""
					sCsvData(29, iLineCount) = ""
					sCsvData(30, iLineCount) = ""
					sCsvData(31, iLineCount) = ""
					sCsvData(32, iLineCount) = ""
					sCsvData(33, iLineCount) = ""
					sCsvData(34, iLineCount) = ""
					sCsvData(35, iLineCount) = ""
					sCsvData(36, iLineCount) = ""
					sCsvData(37, iLineCount) = ""
					sCsvData(38, iLineCount) = ""
					sCsvData(39, iLineCount) = ""
					sCsvData(40, iLineCount) = ""
					sCsvData(41, iLineCount) = ""
					sCsvData(42, iLineCount) = ""
					sCsvData(43, iLineCount) = ""
				end if
				
				'Response.Write("<P>")
				
			next 
			'Response.end
			
		else

			sValue = trim(mid(sFormArray(iCount), iLocEnd + 1))
			sValue = mid(left(sValue, len(sValue) - 2), 5)
			
            'Response.Write("'" & sName & "' : '" & sValue & "' : " & len(sValue) & "<p>")
			
            redim preserve sForm(2, iCount + 1)
			sForm(0, iCount) = sName
			sForm(1, iCount) = sValue
			
		end if
						
	end if
				
next


dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = false 'application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 


%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add a Loan Officer Listing</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
<%

'Initialize the associate object.
dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = false 'application("bVocalErrors")
oAssociate.CompanyId = session("UserCompanyId")

'Initialize the license object.
dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")


'Initialize the oLicenseActive object.
'dim oLicenseActive
'set oLicenseActive = new LicenseActive
'oLicenseActive.ConnectionString = application("sDataSourceName")
'oLicenseActive.VocalErrors = application("bVocalErrors")


'Initialize the course object
dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

'Initialize the CompanyL2 object
dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = false 'application("bVocalErrors")
oCompanyL2.CompanyId = session("UserCompanyId")

'Initialize the CompanyL3 object
dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = false 'application("bVocalErrors")
oCompanyL3.CompanyId = session("UserCompanyId")

'Initialize the Branch object
dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = false 'application("bVocalErrors")
oBranch.CompanyId = session("UserCompanyId")


dim oRs 'Recordset object used for search results.
dim oRsActive 'Recordset object used for search results.


'Declare some variable to track where we are in the loop
dim bErrors 
dim bErrorsInList
dim bBlankSkip

dim bUpdate 'Determines whether this is a new record or an update

response.write("<ul>")

'Variable to hold the type of records we're parsing now, 1 for officers, 
'2 for licenses, 3 for courses.
dim iRecordType 
iRecordType = 1

dim bSkipLine

' *********** NOTE: Build queryable temp table
' From the time we create the table, until we are done reading it, 
' we must reuse the same database session to ensure the temp table is local to the user,
' and that it gets destroyed when the connection is closed (the destruction is automatic,
' though we will clean up ourselves as well as it is good form)    
dim sCreateTable ' as string

sCreateTable = "CREATE TABLE [#tmpLicense](	[LicenseId] [int] IDENTITY(1,1) NOT NULL, " & _
"   [Email] [varchar](100) NULL, " & _
"   [LicenseNum] [varchar](100) NULL, " & _
"	[LicenseStateID] [int] NULL, " & _
"	[LicenseType] [varchar] (100) NULL, " & _
"	[LicenseStatusID] [int] NULL, " & _
"	[LicenseExpDate] [datetime] NULL, " & _
"	 CONSTRAINT [PK_Licenses] PRIMARY KEY CLUSTERED  " & _
"	( " & _
"		[LicenseId] ASC " & _
"	) " & _
") " 


dim licenseCnn
dim licenseRs
dim licenseCmd

set licenseCnn = CreateObject("ADODB.Connection")
licenseCnn.Open  =  application("sDataSourceName")

set licenseCmd = CreateObject("ADODB.Command")
licenseCmd.ActiveConnection = licenseCnn
licenseCmd.CommandText = sCreateTable
licenseCmd.Execute
' Temp officer license table created

'Now loop throught the array to populate the officer license temp table
dim sSQL 
for iCount = 0 to ubound(sCSVData, 2)
    if ucase(sCsvData(0, iCount)) = "*LICENSE" then  
        sEmail = ScrubForSql(sCsvData(1, iCount))
		sLicenseNum = ScrubForSql(sCsvData(2, iCount))
		iLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		sLicenseType = ScrubForSql(sCsvData(4, iCount))
        if iLicenseStateId = "" then 
            iLicenseStateId = GetStateIdByAbbrev(MID(slicenseType,1,2))            
        end if 
		iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(5, iCount)))
		dLicenseExpDate = ScrubForSql(sCsvData(6, iCount))
			
        sSQL ="INSERT INTO #tmpLicense (Email,LicenseNum,LicenseStateID, LicenseType, LicenseStatusID, LicenseExpDate) " _
        & "VALUES ('" & sEmail & "','" & sLicenseNum & "','" & iLicenseStateId & "','" & sLicenseType & "','" & iLicenseStatusId & "','" & dLicenseExpDate & "')"  

        set licenseCmd = CreateObject("ADODB.Command")
        licenseCmd.ActiveConnection = licenseCnn
        licenseCmd.CommandText = sSQL
        licenseCmd.Execute		
    end if
next

'set licenseRs = CreateObject("ADODB.Recordset")
'licenseRs.Open  "SELECT * FROM #tmpLicense", LicenseCnn

'do while NOT licenseRs.eof
'    response.write("<BR>The number of records is: " & licenseRs("LicenseId") & licenseRs("Email") & licenseRs("LicenseStateId") & licenseRs("LicenseType"))
'    licenseRs.MoveNext
'loop

'licenseRs.Close
'licenseCnn.Close

' 
' *********** NOTE: Officer License Temporary Table is now populated

' Now run original loop to process records.
' We will use the officer license temporary table later

'Loop through each of the records in our array.
for iCount = 0 to ubound(sCSVData, 2)

	'If we come across a marker to designate a change in the record type,
	'change the value accordingly.
	if ucase(sCsvData(0, iCount)) = "*LICENSE" then
		iRecordType = 2
	elseif ucase(sCsvData(0, iCount)) = "*COURSE" then
		iRecordType = 3
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyName")) then
		iRecordType = 4
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyL2Name")) then
		iRecordType = 5
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyL3Name")) then
		iRecordType = 6
	elseif ucase(sCsvData(0, iCount)) = "*BRANCH" then
		iRecordType = 7
	else
		iRecordType = 1
	end if 	
	
	if iRecordType = 1 then	
	
		'RecordType 1 means this is an officer record.

		'Pull the officer attribute values out of the array.
		sFirstName = ScrubForSql(sCsvData(0, iCount))
		sMiddleName = ScrubForSql(sCsvData(1, iCount))
		sLastName = ScrubForSql(sCsvData(2, iCount))
		sSsn = ScrubForSql(sCsvData(3, iCount))
		dDateOfBirth = ScrubForSql(sCsvData(4, iCount))
		sNMLSNumber = ScrubForSql(sCsvData(5, iCount))
		iDriversLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(6, iCount)))
		sDriversLicenseNo = ScrubForSql(sCsvData(7, iCount))
		'if bIsBranchAdmin then
		'	iBranchId = session("UserBranchId")
		'else
		iBranchId = oBranch.LookupBranchIdByBranchName(ScrubForSql(sCsvData(8, iCount)))
		if iBranchId = 0 and trim(sCsvData(8, iCount)) <> "" then 'If the Branch was not retrieved by name, try to retrieve it by number if the field is filled.
			iBranchId = oBranch.LookupBranchIdByBranchNum(ScrubForSql(sCsvData(8, iCount)))
		end if
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(8, iCount)))
		iCompanyL3Id = oCompanyL3.LookupCompanyIdByName(ScrubForSql(sCsvData(8, iCount)))
'		if bIsBranchAdmin then
'			if not CheckIsThisBranchAdmin(iBranchId) then
'				bErrors = true
'			end if
'		end if
		'end if 
		dHireDate = ScrubForSql(sCsvData(9, iCount))
		dTerminationDate = ScrubForSql(sCsvData(10, iCount))
		sManager = ScrubForSql(sCsvData(11, iCount))
		sEmployeeId = ScrubForSql(sCsvData(12, iCount))
		sTitle = ScrubForSql(sCsvData(13, iCount))
		sDepartment = ScrubForSql(sCsvData(14, iCount))
		sEmail = ScrubForSql(sCsvData(15, iCount))
		sPhone = ScrubForSql(sCsvData(16, iCount))
		sPhoneExt = ScrubForSql(sCsvData(17, iCount))
		sPhone2 = ScrubForSql(sCsvData(18, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(19, iCount))
		sPhone3 = ScrubForSql(sCsvData(20, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(21, iCount))
		sFax = ScrubForSql(sCsvData(22, iCount))
		sAddress = ScrubForSql(sCsvData(23, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(24, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(25, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(26, iCount)))
		sZipcode = ScrubForSql(sCsvData(27, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(28, iCount))
		
		for i = 0 to 14
			iCurField = 29 + i
			aOfficerCustField(i) = ScrubForSql(sCsvData(iCurField, iCount))
		next
	
		'Check if this is a blank array entry
		if sSsn = "" and sFirstName = "" and sLastName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			'Response.Write("BlankSkip<br>")
			
		else	

			'Check to see if we have an existing officer with this information
			'already in the system.  If so, we'll want to update rather than
			'create a new officer listing.  We'll search for other existing
			'users with the same SSN and CompanyId.
			set oAssociate = nothing
			set oAssociate = new Associate
			oAssociate.ConnectionString = application("sDataSourceName")
			oAssociate.TpConnectionString = application("sTpDataSourceName")
			oAssociate.VocalErrors = false 'application("bVocalErrors")
			oAssociate.CompanyId = session("UserCompanyId")
			oAssociate.FirstName = sFirstName
			oAssociate.LastName = sLastName
			oAssociate.Email = sEmail
			oAssociate.UserStatus = "1"
			set oRs = oAssociate.SearchAssociates
			if not (oRs.EOF and oRs.BOF) then
			
				if oAssociate.LoadAssociateById(oRs("UserId")) = 0 then
				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Failed to load the existing officer information.<br>")
					
				else

					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					if bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): This officer currently belongs to another branch.<br>")
					
					else
					
						bUpdate = true
					
					end if 
			
				end if 
			
			end if 
			
			
			'Assign the properties to this officer object
			oAssociate.FirstName = sFirstName
			oAssociate.MiddleName = sMiddleName
			oAssociate.LastName = sLastName
			oAssociate.Ssn = enDeCrypt(sSsn,application("RC4Pass"))
			oAssociate.DateOfBirth = dDateOfBirth
			oAssociate.NMLSNumber = sNMLSNumber
			oAssociate.DriversLicenseStateId = iDriversLicenseStateId
			oAssociate.DriversLicenseNo = sDriversLicenseNo
			if iBranchId <> 0 then
				oAssociate.BranchId = iBranchId
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			elseif iCompanyL3Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = iCompanyL3Id
			elseif iCompanyL2Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = iCompanyL2Id
				oAssociate.CompanyL3Id = ""
			else
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			end if				
			oAssociate.HireDate = dHireDate
			oAssociate.TerminationDate = dTerminationDate
		
			'If Termination Date is filled, set InActive Flag to true
			'If not, set Inactive flag to false
			if trim(oAssociate.TerminationDate) <> "" then
				oAssociate.Inactive = true
			else
				oAssociate.Inactive = false
			end if
			
			oAssociate.Manager = sManager
			oAssociate.EmployeeId = sEmployeeId
			oAssociate.Title = sTitle
			oAssociate.Department = sDepartment
			oAssociate.Email = sEmail
			oAssociate.Phone = sPhone
			oAssociate.PhoneExt = sPhoneExt
			oAssociate.Phone2 = sPhone2
			oAssociate.Phone2Ext = sPhone2Ext
			oAssociate.Phone3 = sPhone3
			oAssociate.Phone3Ext = sPhone3Ext
			oAssociate.Fax = sFax
			oAssociate.Address1.AddressLine1 = sAddress
			oAssociate.Address1.AddressLine2 = sAddress2
			oAssociate.Address1.City = sCity
			oAssociate.Address1.StateId = iStateId
			oAssociate.Address1.Zipcode = sZipcode
			oAssociate.Address1.ZipcodeExt = sZipcodeExt
	
			oAssociate.UserStatus = 1
			oAssociate.OutOfStateOrig = 0

			'Set Custom Fields 
			for i = 0 to 14
				if trim(aOfficerCustField(i)) <> "" then
					call oAssociate.SetOfficerCustField(i+1,ScrubForSql(aOfficerCustField(i)))
				end if
			next
				
			'Attempt to create a new record for this officer.
			if not bErrors and not bBlankSkip then
	
				if oAssociate.SaveAssociate <> 0 then
					
					if bUpdate then
					
						Response.Write("<li>Updated " & oAssociate.FullName & "<br>")
				
					else
				
						Response.Write("<li>Added " & oAssociate.FullName & "<br>")
						
					end if 
					 			
				else
				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Could not save the officer information.<br>")
				
				end if		
	
			end if 
			
		end if 
	
	elseif iRecordType = 2 then	
	
		'RecordType 2 means this is a license record

		'Pull the license values from the array
		sEmail = ScrubForSql(sCsvData(1, iCount))
		sLicenseNum = ScrubForSql(sCsvData(2, iCount))
		iLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		'iLicenseTypeId = oLicense.LookupLicenseTypeIdByName(ScrubForSql(sCsvData(5, iCount)))
		sLicenseType = ScrubForSql(sCsvData(4, iCount))
        if iLicenseStateId = "" then 
            iLicenseStateId = GetStateIdByAbbrev(MID(slicenseType,1,2))            
        end if 

		iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(5, iCount)))
		dLicenseExpDate = ScrubForSql(sCsvData(6, iCount))
		dLicenseAppDeadline = ScrubForSql(sCsvData(7, iCount))
		sLegalName = ScrubForSql(sCsvData(8, iCount))
		sLicenseNotes = ScrubForSql(sCsvData(9, iCount))
		
        if IsNull(sLicenseNum) or IsEmpty(sLicenseNum) or sLicenseNum="" then
            sLicenseNum="0"
        end if
        
		'Response.Write(sLastName & "," & sSsn & "," & sLicenseNum & "," & iLicenseStateId & "," & iLicenseTypeId & "," & iLicenseStatusId & ",x" & sCsvData(6, iCount) & "x<br>")
			
		'Check if this is a blank array entry
		'if sSsn = "" and sLastName = "" then
		if trim(sEmail) = "" then		
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif sLicenseNum = "" or iLicenseStateId = "" or iLicenseStatusId = "" then
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & ")"  & " (StateID #" & iLicenseStateId & ") "  & " (StatusID #" & iLicenseStatusId & ") : Missing or invalid license information.<br>")
			
		else
	
	
			'Verify that the license is meant for a valid officer record to which
			'the user has access.
			if not oAssociate.LoadAssociateByEmail(sEmail, session("UserCompanyId")) = 0 then
				if oAssociate.CompanyId <> session("UserCompanyId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this officer record.<br>")
				'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
				elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): This officer currently belongs to another branch.<br>")
				end if
				iLicenseOwnerTypeId = 1
				iLicenseOwnerId = oAssociate.UserId
				sLicenseOwnerName = oAssociate.FullName
		
			elseif not oCompany.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsAdmin() or (oCompany.CompanyId <> session("UserCompanyId")) then		
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyName") & " record.<br>")
				end if 
				iLicenseOwnerTypeId = 3
				iLicenseOwnerId = oCompany.CompanyId
				sLicenseOwnerName = oCompany.Name
							
			elseif not oCompanyL2.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsThisCompanyL2Admin(oCompanyL2.CompanyL2Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyL2Name") & " record.<br>")
				end if
				iLicenseOwnerTypeId = 4
				iLicenseOwnerId = oCompanyL2.CompanyL2Id
				sLicenseOwnerName = oCompanyL2.Name
				
			elseif not oCompanyL3.LoadCompanyByName(sEmail) = 0 then
				if not CheckIsThisCompanyL3Admin(oCompanyL3.CompanyL3Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this " & session("CompanyL3Name") & " record.<br>")
				end if
				iLicenseOwnerTypeId = 5
				iLicenseOwnerId = oCompanyL3.CompanyL3Id
				sLicenseOwnerName = oCompanyL3.Name
			
			elseif not oBranch.LoadBranchByName(sEmail) = 0 then
				if not CheckIsThisBranchAdmin(oBranch.BranchId) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load this Branch record.<br>")
				end if
				iLicenseOwnerTypeId = 2
				iLicenseOwnerId = oBranch.BranchId
				sLicenseOwnerName = oBranch.Name
				
			else

				bErrors = true
				Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to locate this officer record.<br>")
		
			end if 
	
	
			'Assign the properties to this officer object
			if not bErrors and not bBlankSkip then			
				'Check to see if we have an existing license with this information already in the system.  If so, we'll want to 
				'update rather than create a new listing.  We'll search for other existing licenses with the same Owner/State/Number.
				oLicense.ReleaseLicense			
				oLicense.OwnerTypeId = iLicenseOwnerTypeId
				oLicense.OwnerCompanyId = session("UserCompanyId")
				oLicense.OwnerId = iLicenseOwnerId
				oLicense.LicenseStateId = iLicenseStateId

                'Since we are no longer checking number for match, we now need to check records manually for match                         
                oLicense.LicenseType = sLicenseType

                set oRs = oLicense.SearchLicensesV2
             	if not (oRs.EOF and oRs.BOF) then	
                   'Response.Write(sLicenseNum & "," & oRs("LicenseType") & "," & sLicenseType & "," & iLicenseStatusId & ",x" & ScrubForSql(sCsvData(2, iCount)) & "x<br>")
                   dim tLicenseID 
                   tLicenseID=0
					if oLicense.LoadLicenseById(oRs("LicenseId")) = 0 then
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to load the existing license information.<br>")		
					else	                                                   
                        'CHECK TEMP TABLE for an active record
                        hasActive =false

                        sSQL2 = "SELECT  LicenseId, Email, LicenseStateId, LicenseType, lt.LicenseStatusPermanent FROM #tmpLicense "
                        sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        sSQL2 =  SSQL2 & "WHERE #tmpLicense.Email= '" & sEmail & "'"
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseType= '" & sLicenseType & "'"
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "'"
                        sSQL2 =  SSQL2 & "AND lt.LicenseStatusPermanent= 0" 

                        set licenseRs = CreateObject("ADODB.Recordset")
                        licenseRs.Open  sSQL2, LicenseCnn
                      
                        if not (licenseRs.EOF and licenseRs.BOF) then	
                            'There is an active record
                            hasActive=true
                        end if
                        licenseRs.Close

                      
                        sSQL2 = "SELECT  TOP 1 Email, LicenseStateId, LicenseType, #tmpLicense.LicenseStatusID, LicenseExpDate, lt.LicenseStatusPermanent FROM #tmpLicense "
                        sSQL2 =  SSQL2 & "INNER JOIN LicenseStatusTypes lt on #tmpLicense.LicenseStatusID=lt.LicenseStatusID "
                        sSQL2 =  SSQL2 & "WHERE #tmpLicense.Email= '" & sEmail & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseType= '" & sLicenseType & "' "
                        sSQL2 =  SSQL2 & "AND #tmpLicense.LicenseStateId= '" & iLicenseStateId & "' "
                        sSQL2 =  SSQL2 & "Order by LicenseExpDate DESC" 

                        set licenseRs = CreateObject("ADODB.Recordset")
                        licenseRs.Open  sSQL2, LicenseCnn
                      
                        if licenseRs.recordcount <> -1 then	
                            bErrors = true
						    Response.Write("<li class=""error""> ERROR 2012:09<br>")		
                         end if    
                                                            
                        'is there an active record in ComplianceKeeper    
                        dim hasActiveCK
                       

                        hasActiveCK=false
                        NoUpdate=false
                        
                        set oRsActive = oLicense.SearchActiveLicense
                        if not (oRsActive.EOF and oRsActive.BOF) then	
                            hasActiveCK=true
                        end if 

                        'stop
                       dim firstTime
                       firstTime=true

                       do while NOT oRs.eof and bUpdate = false
                            onActive = false
                            foundActive = false
                           
                            if oLicense.LookupLicenseStatusPermanent(iLicenseStatusId)=false then 
                                onActive=true
                            end if

                            if (oRs("LicenseStatusPermanent"))=false then 
                                foundActive=true
                            end if

                          
                            if hasActive=true then
                                if hasActiveCK=true then
                                    if onActive=true and foundActive=true then                                
                                         bUpdate = true                                
                                         exit do
                                    elseif onActive=false and ((oRs("LicenseExpDate"))=cdate(dLicenseExpDate)) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'NEW Record
                                    end if                                                                                       
                                elseif hasActiveCK=false then 
                                    if  onActive=true then
                                        exit do
                                    elseif (oRs("LicenseExpDate")=cdate(dLicenseExpDate)) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'NEW Record
                                    end if                              
                                end if 
                           
                                                    
                            elseif hasActive=false then   
                                                 
                                if hasActiveCK=true then
                                    if (licenseRs("LicenseExpDate")=cdate(dLicenseExpDate)) and foundActive=true then
                                        bUpdate = true                                
                                        exit do
                                     elseif (oRs("LicenseExpDate"))=cdate(dLicenseExpDate) then 
                                        NoUpdate=true
                                        exit do
                                    else
                                        'stop
                                    end if
                                elseif (oRs("LicenseExpDate"))=cdate(dLicenseExpDate) then 
                                        NoUpdate=true
                                        exit do
                                else
                                    'NEW Record
                                end if 
                            end if
                            if bUpdate=true or NoUpdate=true then
                                firstTime=false
                            end if

                            oRs.MoveNext

                        loop



					end if 
                   
                    if bUpdate = true then
				        tLicenseID=oRs("LicenseId")
                    end if
				else
				      'NEW Record
				end if
				
				if safecstr(oLicense.LicenseExpDate) <> safecstr(dLicenseExpDate) or _
					safecstr(oLicense.LicenseAppDeadline) <> safecstr(dLicenseAppDeadline) or _
					safecstr(oLicense.LicenseStatusId) <> safecstr(iLicenseStatusId) then
					oLicense.Triggered30 = 0
					oLicense.Triggered60 = 0
					oLicense.Triggered90 = 0
				end if
				
                'Need to make sure license number is with oLicense
                oLicense.LicenseNum = sLicenseNum

				oLicense.LegalName = sLegalName
				oLicense.LicenseStatusId = iLicenseStatusId
				'oLicense.LicenseTypeId = iLicenseTypeId
				oLicense.LicenseType = sLicenseType
				oLicense.LicenseExpDate = dLicenseExpDate
				oLicense.LicenseAppDeadline = dLicenseAppDeadline
				oLicense.Notes = sLicenseNotes
				
                
                'Noupdate mead we are on a perminate license that shound not be updated 
                
                
                if NoUpdate=false then
				    if oLicense.SaveLicense(bUpdate,tLicenseID) <> 0 then
					
					    if bUpdate then
					
						    Response.Write("<li>Updated License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")
					
					    else
					
						    Response.Write("<li>Added License #" & oLicense.LicenseNum & " for " & sLicenseOwnerName & "<br>")
					
					    end if 
					
                        'Response.Write(sLastName & "," & sLicenseNum & "," & iLicenseStateId & "," & sLicenseType & "," & iLicenseStatusId & ",_" &  dLicenseExpDate & "_<br>")
				
                    else
					
					    bErrors = true
					    Response.Write("<li class=""error"">Entry " & iCount + 1 & " (License #" & sLicenseNum & "): Failed to save the license information.<br>")
					
				    end if
				end if 
			end if 
			
		end if 

	elseif iRecordType = 3 then	
	
		'RecordType 3 means this is a course record
		
		'Pull the course values from the array
		sEmail = ScrubForSql(sCsvData(1, iCount))
		iProviderId = oCourse.LookupProviderId(ScrubForSql(sCsvData(2, iCount)), session("UserCompanyId"))
		sCourseName = ScrubForSql(sCsvData(3, iCount))
		rCourseHours = ScrubForSql(sCsvData(4, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(5, iCount)))
		dCompletionDate = ScrubForSql(sCsvData(6, iCount))
		dExpDate = ScrubForSql(sCsvData(7, iCount))

	
		'Check if this is a blank array entry
		if sEmail = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
			
		elseif iProviderId = "" then
	
			'Verify that we got at least a SSN and Last Name, which we'll need to
			'look up the officer.
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid provider information.<br>")
			
		elseif sCourseName = "" or rCourseHours = "" or iStateId = "" then
		
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Missing or invalid course information.<br>")
			
		else
	
			'Attempt to load a course based on the passed course information.
			oCourse.ReleaseCourse
			oCourse.Name = sCourseName
			oCourse.ProviderId = iProviderId
			oCourse.CompanyId = session("UserCompanyId")
			oCourse.ContEdHours = rCourseHours
			oCourse.StateId = iStateId
			set oRs = oCourse.SearchCourses
			if not (oRs.EOF and oRs.BOF) then
				
				if oCourse.LoadCourseById(oRs("CourseId")) = 0 then
				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to load this course record.<br>")
				
				end if
			
			else
								
				bErrors = true
				Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & sCourseName & """): Failed to locate this course record.<br>")
		
			end if 
			set oRs = nothing
	
			'Verify that the course is meant for a valid officer record to which
			'the user has access.
			if not bErrors and not bBlankSkip then 
			
				if not oAssociate.LoadAssociateByEmail(sEmail, session("UserCompanyId")) = 0 then
		
					if oAssociate.CompanyId <> session("UserCompanyId") then

						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to load this officer record.<br>")
						
					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					elseif bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then
						
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): This officer currently belongs to another branch.<br>")

					end if
		
				else

					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to locate this officer record.<br>")
		
				end if 
				
			end if 
		
		
			'Assign the properties based on our passed parameters
			if not bErrors and not bBlankSkip then
				'Determine whether to Add the course to the associate's profile, or update the existing course in the associate's profile.
				
				'Check to see if the course exists in the associate's profile
				iAssocId = oAssociate.CourseExistsInProfile(oCourse.CourseId)
				
				if clng(iAssocId) = 0 then  'Course does not exist in profile so Add
					iSuccess = oAssociate.AddCourse(oCourse.CourseId, dExpDate, dCompletionDate)
					sAction = "Added"
				else 'Course exists in profile so update
					iSuccess = oAssociate.UpdateCourse(iAssocId, oCourse.CourseId, dExpDate, dCompletionDate)
					sAction = "Updated"
				end if
			
				if iSuccess = 0 then
	
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Course """ & oCourse.Name & """): Failed to save the course information.<br>")
					
				else
		
					Response.Write("<li>" & sAction & " Course """ & oCourse.Name & """ for " & oAssociate.FullName & "<br>") 
		
				end if 
				
			end if 
		
		end if


	elseif iRecordType = 4 then	
	
		'RecordType 4 means this is a company update
		
		'Pull the company values from the array
		sName = ScrubForSql(sCsvData(1, iCount))
		sLegalName = ScrubForSql(sCsvData(2, iCount))
		iIncorpStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		dIncorpDate = ScrubForSql(sCsvData(4, iCount))
		iOrgTypeId = oCompany.LookupOrgTypeByName(ScrubForSql(sCsvData(5, iCount)))
		sEin = ScrubForSql(sCsvData(6, iCount))
		sPhone = ScrubForSql(sCsvData(7, iCount))
		sPhoneExt = ScrubForSql(sCsvData(8, iCount))
		sPhone2 = ScrubForSql(sCsvData(9, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(10, iCount))
		sPhone3 = ScrubForSql(sCsvData(11, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(12, iCount))
		sFax = ScrubForSql(sCsvData(13, iCount))
		sAddress = ScrubForSql(sCsvData(14, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(15, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(16, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(17, iCount)))
		sZipcode = ScrubForSql(sCsvData(18, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(19, iCount))
		sMailingAddress = ScrubForSql(sCsvData(20, iCount))
		sMailingAddress = replace(sMailingAddress, """", "")
		sMailingAddress2 = ScrubForSql(sCsvData(21, iCount))
		sMailingAddress2 = replace(sMailingAddress2, """", "")
		sMailingCity = ScrubForSql(sCsvData(22, iCount))
		iMailingStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(23, iCount)))
		sMailingZipcode = ScrubForSql(sCsvData(24, iCount))
		sMailingZipcodeExt = ScrubForSql(sCsvData(25, iCount))
		sContactName = ScrubForSql(sCsvData(26, iCount))
		sContactPhone = ScrubForSql(sCsvData(27, iCount))
		sContactPhoneExt = ScrubForSql(sCsvData(28, iCount))
		sContactEmail = ScrubForSql(sCsvData(29, iCount))
		sContactFax = ScrubForSql(sCsvData(30, iCount))

	
		'Check if this is a blank array entry
		if sName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyName") & " Record): Missing or invalid " & session("CompanyName") & " information.<br>")
					
		else
	
			'Attempt to load the company object
			set oCompany = new Company
			oCompany.ConnectionString = application("sDataSourceName")
			oCompany.VocalErrors = application("bVocalErrors")
			
			if not oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
				if not CheckIsAdmin() then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyName") & " Record): Unable to load the " & session("CompanyName") & " record.<br>")
				else
					
					'Update the company properties
					oCompany.Name = sName
					oCompany.LegalName = sLegalName
					oCompany.IncorpStateId = iIncorpStateId
					oCompany.IncorpDate = dIncorpDate
					oCompany.OrgTypeId = iOrgTypeId
					oCompany.Ein = sEin
					oCompany.Phone = sPhone
					oCompany.PhoneExt = sPhoneExt
					oCompany.Phone2 = sPhone2
					oCompany.PhoneExt2 = sPhone2Ext
					oCompany.Phone3 = sPhone3
					oCompany.PhoneExt3 = sPhone3Ext
					oCompany.Fax = sFax
					oCompany.Address = sAddress
					oCompany.Address2 = sAddress2
					oCompany.City = sCity
					oCompany.StateId = iStateId
					oCompany.Zipcode = sZipcode
					oCompany.ZipcodeExt = sZipcodeExt
					oCompany.MailingAddress = sMailingAddress
					oCompany.MailingAddress2 = sMailingAddress2
					oCompany.MailingCity = sMailingCity
					oCompany.MailingStateId = iMailingStateId
					oCompany.MailingZipcode = sMailingZipcode
					oCompany.MailingZipcodeExt = sMailingZipcodeExt
					oCompany.MainContactName = sContactName
					oCompany.MainContactPhone = sContactPhone
					oCompany.MainContactPhoneExt = sContactPhoneExt
					oCompany.MainContactEmail = sContactEmail
					oCompany.MainContactFax = sContactFax
				
					if oCompany.SaveCompany <> 0 then
						Response.Write("<li>Updated " & oCompany.Name & "<br>")	 			
					else
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyName") & " Record): Could not save the company information.<br>")
					end if	
				
				end if
			
			end if
				
		end if


	elseif iRecordType = 5 then	
	
		'RecordType 5 means this is a company L2 update
		
		'Pull the company values from the array
		sName = ScrubForSql(sCsvData(1, iCount))
		sLegalName = ScrubForSql(sCsvData(2, iCount))
		iIncorpStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		dIncorpDate = ScrubForSql(sCsvData(4, iCount))
		iOrgTypeId = oCompany.LookupOrgTypeByName(ScrubForSql(sCsvData(5, iCount)))
		sEin = ScrubForSql(sCsvData(6, iCount))
		sPhone = ScrubForSql(sCsvData(7, iCount))
		sPhoneExt = ScrubForSql(sCsvData(8, iCount))
		sPhone2 = ScrubForSql(sCsvData(9, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(10, iCount))
		sPhone3 = ScrubForSql(sCsvData(11, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(12, iCount))
		sFax = ScrubForSql(sCsvData(13, iCount))
		sAddress = ScrubForSql(sCsvData(14, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(15, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(16, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(17, iCount)))
		sZipcode = ScrubForSql(sCsvData(18, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(19, iCount))
		sMailingAddress = ScrubForSql(sCsvData(20, iCount))
		sMailingAddress = replace(sMailingAddress, """", "")
		sMailingAddress2 = ScrubForSql(sCsvData(21, iCount))
		sMailingAddress2 = replace(sMailingAddress2, """", "")
		sMailingCity = ScrubForSql(sCsvData(22, iCount))
		iMailingStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(23, iCount)))
		sMailingZipcode = ScrubForSql(sCsvData(24, iCount))
		sMailingZipcodeExt = ScrubForSql(sCsvData(25, iCount))
		sContactName = ScrubForSql(sCsvData(26, iCount))
		sContactPhone = ScrubForSql(sCsvData(27, iCount))
		sContactPhoneExt = ScrubForSql(sCsvData(28, iCount))
		sContactEmail = ScrubForSql(sCsvData(29, iCount))
		sContactFax = ScrubForSql(sCsvData(30, iCount))

	
		'Check if this is a blank array entry
		if sName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL2Name") & " Record): Missing or invalid " & session("CompanyL2Name") & " information.<br>")
					
		else
	
			'Attempt to load the company object
			set oCompanyL2 = new CompanyL2
			oCompanyL2.ConnectionString = application("sDataSourceName")
			oCompanyL2.VocalErrors = false 'application("bVocalErrors")
			
			if oCompanyL2.LoadCompanyByName(sName) = 0 then
				bUpdate = false
			else
				if not CheckIsThisCompanyL2Admin(oCompanyL2.CompanyL2Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL2Name") & " Record): Unable to load the " & session("CompanyL2Name") & " record.<br>")
				end if
				bUpdate = true
			end if
				

			if not bErrors then	
				'Update the company properties
				oCompanyL2.CompanyId = session("UserCompanyId")
				oCompanyL2.Name = sName
				oCompanyL2.LegalName = sLegalName
				oCompanyL2.IncorpStateId = iIncorpStateId
				oCompanyL2.IncorpDate = dIncorpDate
				oCompanyL2.OrgTypeId = iOrgTypeId
				oCompanyL2.Ein = sEin
				oCompanyL2.Phone = sPhone
				oCompanyL2.PhoneExt = sPhoneExt
				oCompanyL2.Phone2 = sPhone2
				oCompanyL2.PhoneExt2 = sPhone2Ext
				oCompanyL2.Phone3 = sPhone3
				oCompanyL2.PhoneExt3 = sPhone3Ext
				oCompanyL2.Fax = sFax
				oCompanyL2.Address = sAddress
				oCompanyL2.Address2 = sAddress2
				oCompanyL2.City = sCity
				oCompanyL2.StateId = iStateId
				oCompanyL2.Zipcode = sZipcode
				oCompanyL2.ZipcodeExt = sZipcodeExt
				oCompanyL2.MailingAddress = sMailingAddress
				oCompanyL2.MailingAddress2 = sMailingAddress2
				oCompanyL2.MailingCity = sMailingCity
				oCompanyL2.MailingStateId = iMailingStateId
				oCompanyL2.MailingZipcode = sMailingZipcode
				oCompanyL2.MailingZipcodeExt = sMailingZipcodeExt
				oCompanyL2.MainContactName = sContactName
				oCompanyL2.MainContactPhone = sContactPhone
				oCompanyL2.MainContactPhoneExt = sContactPhoneExt
				oCompanyL2.MainContactEmail = sContactEmail
				oCompanyL2.MainContactFax = sContactFax
					
				if oCompanyL2.SaveCompany <> 0 then
					Response.Write("<li>Updated " & oCompanyL2.Name & "<br>")	 			
				else
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL2Name") & " Record): Could not save the " & session("CompanyL2Name") & " information.<br>")
				end if	
						
			end if
				
		end if


	elseif iRecordType = 6 then	
	
		'RecordType 6 means this is a company L3 update
		
		'Pull the company values from the array
		sName = ScrubForSql(sCsvData(1, iCount))
		sLegalName = ScrubForSql(sCsvData(2, iCount))
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(3, iCount)))
		iIncorpStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(4, iCount)))
		dIncorpDate = ScrubForSql(sCsvData(5, iCount))
		iOrgTypeId = oCompany.LookupOrgTypeByName(ScrubForSql(sCsvData(6, iCount)))
		sEin = ScrubForSql(sCsvData(7, iCount))
		sPhone = ScrubForSql(sCsvData(8, iCount))
		sPhoneExt = ScrubForSql(sCsvData(9, iCount))
		sPhone2 = ScrubForSql(sCsvData(10, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(11, iCount))
		sPhone3 = ScrubForSql(sCsvData(12, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(13, iCount))
		sFax = ScrubForSql(sCsvData(14, iCount))
		sAddress = ScrubForSql(sCsvData(15, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(16, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(17, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(18, iCount)))
		sZipcode = ScrubForSql(sCsvData(19, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(20, iCount))
		sMailingAddress = ScrubForSql(sCsvData(21, iCount))
		sMailingAddress = replace(sMailingAddress, """", "")
		sMailingAddress2 = ScrubForSql(sCsvData(22, iCount))
		sMailingAddress2 = replace(sMailingAddress2, """", "")
		sMailingCity = ScrubForSql(sCsvData(23, iCount))
		iMailingStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(24, iCount)))
		sMailingZipcode = ScrubForSql(sCsvData(25, iCount))
		sMailingZipcodeExt = ScrubForSql(sCsvData(26, iCount))
		sContactName = ScrubForSql(sCsvData(27, iCount))
		sContactPhone = ScrubForSql(sCsvData(28, iCount))
		sContactPhoneExt = ScrubForSql(sCsvData(29, iCount))
		sContactEmail = ScrubForSql(sCsvData(30, iCount))
		sContactFax = ScrubForSql(sCsvData(31, iCount))

	
		'Check if this is a blank array entry
		if sName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL3Name") & " Record): Missing or invalid " & session("CompanyL3Name") & " information.<br>")
					
		else
	
			'Attempt to load the company object
			set oCompanyL3 = new CompanyL3
			oCompanyL3.ConnectionString = application("sDataSourceName")
			oCompanyL3.VocalErrors = false 'application("bVocalErrors")
			
			if oCompanyL3.LoadCompanyByName(sName) = 0 then
				bUpdate = false
			else
				if not CheckIsThisCompanyL3Admin(oCompanyL3.CompanyL3Id) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL3Name") & " Record): Unable to load the " & session("CompanyL3Name") & " record.<br>")
				end if
				bUpdate = true
			end if
				
				
			if not bErrors then
				'Update the company properties
				oCompanyL3.CompanyId = session("UserCompanyId")
				oCompanyL3.Name = sName
				oCompanyL3.LegalName = sLegalName
				if iCompanyL2Id <> 0 then
					oCompanyL3.CompanyL2Id = iCompanyL2Id
				else
					oCompanyL3.CompanyL2Id = ""
				end if					
				oCompanyL3.IncorpStateId = iIncorpStateId
				oCompanyL3.IncorpDate = dIncorpDate
				oCompanyL3.OrgTypeId = iOrgTypeId
				oCompanyL3.Ein = sEin
				oCompanyL3.Phone = sPhone
				oCompanyL3.PhoneExt = sPhoneExt
				oCompanyL3.Phone2 = sPhone2
				oCompanyL3.PhoneExt2 = sPhone2Ext
				oCompanyL3.Phone3 = sPhone3
				oCompanyL3.PhoneExt3 = sPhone3Ext
				oCompanyL3.Fax = sFax
				oCompanyL3.Address = sAddress
				oCompanyL3.Address2 = sAddress2
				oCompanyL3.City = sCity
				oCompanyL3.StateId = iStateId
				oCompanyL3.Zipcode = sZipcode
				oCompanyL3.ZipcodeExt = sZipcodeExt
				oCompanyL3.MailingAddress = sMailingAddress
				oCompanyL3.MailingAddress2 = sMailingAddress2
				oCompanyL3.MailingCity = sMailingCity
				oCompanyL3.MailingStateId = iMailingStateId
				oCompanyL3.MailingZipcode = sMailingZipcode
				oCompanyL3.MailingZipcodeExt = sMailingZipcodeExt
				oCompanyL3.MainContactName = sContactName
				oCompanyL3.MainContactPhone = sContactPhone
				oCompanyL3.MainContactPhoneExt = sContactPhoneExt
				oCompanyL3.MainContactEmail = sContactEmail
				oCompanyL3.MainContactFax = sContactFax
					
				if oCompanyL3.SaveCompany <> 0 then
					Response.Write("<li>Updated " & oCompanyL3.Name & "<br>")	 			
				else
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & session("CompanyL3Name") & " Record): Could not save the " & session("CompanyL3Name") & " information.<br>")
				end if	
			end if
				
		end if
		
		
	elseif iRecordType = 7 then	
	
		'RecordType 7 means this is a branch update
		
		'Pull the company values from the array
		sName = ScrubForSql(sCsvData(2, iCount))
		sBranchNumber = ScrubForSql(sCsvData(1, iCount))
		sLegalName = ScrubForSql(sCsvData(3, iCount))
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(4, iCount)))
		iCompanyL3Id = oCompanyL3.LookupCompanyIdByName(ScrubForSql(sCsvData(4, iCount)))
		sAddress = ScrubForSql(sCsvData(5, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(6, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(7, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(8, iCount)))
		sZipcode = ScrubForSql(sCsvData(9, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(10, iCount))
		sPhone = ScrubForSql(sCsvData(11, iCount))
		sPhoneExt = ScrubForSql(sCsvData(12, iCount))
		sFax = ScrubForSql(sCsvData(13, iCount))
		sMailingAddress = ScrubForSql(sCsvData(14, iCount))
		sMailingAddress = replace(sMailingAddress, """", "")
		sMailingAddress2 = ScrubForSql(sCsvData(15, iCount))
		sMailingAddress2 = replace(sMailingAddress2, """", "")
		sMailingCity = ScrubForSql(sCsvData(16, iCount))
		iMailingStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(17, iCount)))
		sMailingZipcode = ScrubForSql(sCsvData(18, iCount))
		sMailingZipcodeExt = ScrubForSql(sCsvData(19, iCount))
		sEntityLicNum = ScrubForSql(sCsvData(20, iCount))
		sFhaBranchId = ScrubForSql(sCsvData(21, iCount))
		sVaBranchId = ScrubForSql(sCsvData(22, iCount))

	
		'Check if this is a blank array entry
		if sName = "" then
			
			'Set a boolean so we know to skip the rest of the operations for this
			'time through the loop.
			bBlankSkip = true
		
		elseif sAddress = "" or sCity = "" or iStateId = "" or sZipcode = "" then
			
			bErrors = true
			Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Missing or invalid branch information.<br>")
					
		else
	
			'Attempt to load the branch object
			set oBranch = new Branch
			oBranch.ConnectionString = application("sDataSourceName")
			oBranch.VocalErrors = false 'application("bVocalErrors")
			
			if oBranch.LoadBranchByName(sName) = 0 then
				bUpdate = false
			else
				if not CheckIsThisBranchAdmin(oBranch.BranchId) then
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Unable to load the branch record.<br>")
				end if
				bUpdate = true
			end if
				

			if not bErrors then
				'Update the company properties
				oBranch.CompanyId = session("UserCompanyId")
				oBranch.BranchName = sName
				oBranch.BranchNum = sBranchNumber
				oBranch.LegalName = sLegalName
				if iCompanyL3Id <> 0 then
					oBranch.CompanyL2Id = ""
					oBranch.CompanyL3Id = iCompanyL3Id
				elseif iCompanyL2Id <> 0 then
					oBranch.CompanyL2Id = iCompanyL2Id
					oBranch.CompanyL3Id = ""
				else
					oBranch.CompanyL2Id = ""
					oBranch.CompanyL3Id = ""
				end if	
				oBranch.Address = sAddress
				oBranch.Address2 = sAddress2 
				oBranch.City = sCity 
				oBranch.StateId = iStateId 
				oBranch.Zipcode = sZipcode
				oBranch.Zipcode = sZipcodeExt
				oBranch.Phone = sPhone 
				oBranch.PhoneExt = sPhoneExt 
				oBranch.Fax = sFax 
				oBranch.MailingAddress = sMailingAddress 
				oBranch.MailingAddress2 = sMailingAddress2
				oBranch.MailingCity = sMailingCity 
				oBranch.MailingStateId = iMailingStateId 
				oBranch.MailingZipcode = sMailingZipcode
				oBranch.MailingZipcodeExt = sMailingZipcodeExt 
				oBranch.EntityLicNum = sEntityLicNum
				oBranch.FhaBranchId = sFhaBranchId
				oBranch.VaBranchId = sVaBranchId
				
				if oBranch.SaveBranch <> 0 then
					Response.Write("<li>Updated " & oBranch.BranchName & "<br>")	 			
				else
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (Branch Record): Could not save the branch information.<br>")
				end if	
			end if
				
		end if



	end if
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false

next 

'Clean up
set oAssociate = nothing



if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The loan officer list was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<!-- #include virtual = "/includes/ImportProc.asp" --> 
<%





for iCount = 0 to ubound(sCSVData, 2)	
		'RecordType 1 means this is an officer record.

		'Pull the officer attribute values out of the array.
		sFirstName = ScrubForSql(sCsvData(0, iCount))
		sMiddleName = ScrubForSql(sCsvData(1, iCount))
		sLastName = ScrubForSql(sCsvData(2, iCount))
		sSsn = ScrubForSql(sCsvData(3, iCount))
		dDateOfBirth = ScrubForSql(sCsvData(4, iCount))
		sNMLSNumber = ScrubForSql(sCsvData(5, iCount))
		iDriversLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(6, iCount)))
		sDriversLicenseNo = ScrubForSql(sCsvData(7, iCount))
		'if bIsBranchAdmin then
		'	iBranchId = session("UserBranchId")
		'else
		iBranchId = oBranch.LookupBranchIdByBranchName(ScrubForSql(sCsvData(8, iCount)))
		if iBranchId = 0 and trim(sCsvData(8, iCount)) <> "" then 'If the Branch was not retrieved by name, try to retrieve it by number if the field is filled.
			iBranchId = oBranch.LookupBranchIdByBranchNum(ScrubForSql(sCsvData(8, iCount)))
		end if
		iCompanyL2Id = oCompanyL2.LookupCompanyIdByName(ScrubForSql(sCsvData(8, iCount)))
		iCompanyL3Id = oCompanyL3.LookupCompanyIdByName(ScrubForSql(sCsvData(8, iCount)))
'		if bIsBranchAdmin then
'			if not CheckIsThisBranchAdmin(iBranchId) then
'				bErrors = true
'			end if
'		end if
		'end if 
		dHireDate = ScrubForSql(sCsvData(9, iCount))
		dTerminationDate = ScrubForSql(sCsvData(10, iCount))
		sManager = ScrubForSql(sCsvData(11, iCount))
		sEmployeeId = ScrubForSql(sCsvData(12, iCount))
		sTitle = ScrubForSql(sCsvData(13, iCount))
		sDepartment = ScrubForSql(sCsvData(14, iCount))
		sEmail = ScrubForSql(sCsvData(15, iCount))
		sPhone = ScrubForSql(sCsvData(16, iCount))
		sPhoneExt = ScrubForSql(sCsvData(17, iCount))
		sPhone2 = ScrubForSql(sCsvData(18, iCount))
		sPhone2Ext = ScrubForSql(sCsvData(19, iCount))
		sPhone3 = ScrubForSql(sCsvData(20, iCount))
		sPhone3Ext = ScrubForSql(sCsvData(21, iCount))
		sFax = ScrubForSql(sCsvData(22, iCount))
		sAddress = ScrubForSql(sCsvData(23, iCount))
		sAddress = replace(sAddress, """", "")
		sAddress2 = ScrubForSql(sCsvData(24, iCount))
		sAddress2 = replace(sAddress2, """", "")
		sCity = ScrubForSql(sCsvData(25, iCount))
		iStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(26, iCount)))
		sZipcode = ScrubForSql(sCsvData(27, iCount))
		sZipcodeExt = ScrubForSql(sCsvData(28, iCount))
		
		for i = 0 to 14
			iCurField = 29 + i
			aOfficerCustField(i) = ScrubForSql(sCsvData(iCurField, iCount))
		next
	
		'Check if this is a blank array entry
        if sNMLSNumber = "" or sFirstName = "" or sLastName = "" then
			'Set a boolean so we know to skip the rest of the operations for this time through the loop.
			bBlankSkip = true
			'Response.Write("BlankSkip<br>")			
		else	

			'Check to see if we have an existing officer with this information already in the system.  If so, we'll want to update rather than
			'create a new officer listing.  We'll search for other existing users with the same SSN and CompanyId.
			set oAssociate = nothing
			set oAssociate = new Associate
			oAssociate.ConnectionString = application("sDataSourceName")
			oAssociate.TpConnectionString = application("sTpDataSourceName")
			oAssociate.VocalErrors = false 'application("bVocalErrors")
			
            oAssociate.CompanyId = session("UserCompanyId")
			oAssociate.FirstName = sFirstName
			oAssociate.LastName = sLastName
			'oAssociate.Email = sEmail
            oAssociate.NMLSNumber = sNMLSNumber
			oAssociate.UserStatus = "1"
			
            set oRs = oAssociate.SearchAssociates
			
            if not (oRs.EOF and oRs.BOF) then		
				if oAssociate.LoadAssociateById(oRs("UserId")) = 0 then				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Failed to load the existing officer information.<br>")					
				else
					'If the user is a branch admin, make sure that the loaded officer belongs to their branch.
					if bIsBranchAdmin and oAssociate.BranchId <> session("UserBranchId") then					
						bErrors = true
						Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): This officer currently belongs to another branch.<br>")					
					else					
						bUpdate = true					
					end if 			
				end if 			
			end if 
			
			
			'Assign the properties to this officer object
            oAssociate.Verified = true
			oAssociate.FirstName = sFirstName
			oAssociate.MiddleName = sMiddleName
			oAssociate.LastName = sLastName
			oAssociate.Ssn = enDeCrypt(sSsn,application("RC4Pass"))
			oAssociate.DateOfBirth = dDateOfBirth
			oAssociate.NMLSNumber = sNMLSNumber
			oAssociate.DriversLicenseStateId = iDriversLicenseStateId
			oAssociate.DriversLicenseNo = sDriversLicenseNo
			if iBranchId <> 0 then
				oAssociate.BranchId = iBranchId
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			elseif iCompanyL3Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = iCompanyL3Id
			elseif iCompanyL2Id <> 0 then
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = iCompanyL2Id
				oAssociate.CompanyL3Id = ""
			else
				oAssociate.BranchId = ""
				oAssociate.CompanyL2Id = ""
				oAssociate.CompanyL3Id = ""
			end if				
			oAssociate.HireDate = dHireDate
			oAssociate.TerminationDate = dTerminationDate
		
			'If Termination Date is filled, set InActive Flag to true
			'If not, set Inactive flag to false
			if trim(oAssociate.TerminationDate) <> "" then
				oAssociate.Inactive = true
			else
				oAssociate.Inactive = false
			end if
			
			oAssociate.Manager = sManager
			oAssociate.EmployeeId = sEmployeeId
			oAssociate.Title = sTitle
			oAssociate.Department = sDepartment
			oAssociate.Email = sEmail
			oAssociate.Phone = sPhone
			oAssociate.PhoneExt = sPhoneExt
			oAssociate.Phone2 = sPhone2
			oAssociate.Phone2Ext = sPhone2Ext
			oAssociate.Phone3 = sPhone3
			oAssociate.Phone3Ext = sPhone3Ext
			oAssociate.Fax = sFax
			oAssociate.Address1.AddressLine1 = sAddress
			oAssociate.Address1.AddressLine2 = sAddress2
			oAssociate.Address1.City = sCity
			oAssociate.Address1.StateId = iStateId
			oAssociate.Address1.Zipcode = sZipcode
			oAssociate.Address1.ZipcodeExt = sZipcodeExt
	
			oAssociate.UserStatus = 1
			oAssociate.OutOfStateOrig = 0

			'Set Custom Fields 
			for i = 0 to 14
				if trim(aOfficerCustField(i)) <> "" then
					call oAssociate.SetOfficerCustField(i+1,ScrubForSql(aOfficerCustField(i)))
				end if
			next
				
			'Attempt to create a new record for this officer.
			if not bErrors and not bBlankSkip then	
				if oAssociate.SaveAssociate <> 0 then					
					if bUpdate then					
						Response.Write("<li>Updated " & oAssociate.FullName & "<br>")				
					else			
						Response.Write("<li>Added " & oAssociate.FullName & "<br>")						
					end if 					 			
				else				
					bErrors = true
					Response.Write("<li class=""error"">Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & ", NMLS # " & sNMLSNumber & "): Could not save the officer information.<br>")				
				end if		
			end if 			
		end if 
	
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	bUpdate = false

next 

'Clean up
set oAssociate = nothing


if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The import was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
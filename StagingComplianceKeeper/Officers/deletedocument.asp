<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/AssociateDoc.Class.asp" ------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/admin/includes/gfxSpex.asp" ------------------------>

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>
<%


dim iCompanyId
dim iOwnerId
dim iOwnerTypeId
dim iDocId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))
iDocId = ScrubForSql(request("DocId"))


dim oAssociateDoc
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")

if iDocId <> "" then
	if oAssociateDoc.LoadDocById(iDocId) = 0 then
	
		AlertError("Failed to load the Document information.")
		Response.End
		
	'If this is a company document, check that it belongs to the user's company
	elseif oAssociateDoc.OwnerTypeId = 3 and (oAssociateDoc.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then

		AlertError("Unable to load the passed Document ID.")
		Response.End
	
	'If this is a companyL2 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 4 then
		
		if not CheckIsThisCompanyL2Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyl3 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if
	
	'If this is a branch doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if
		
	'If this is an officer doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 1 then
		
		if not CheckIsThisAssociateAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	'If this is a state doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 6 then
	
		if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
			AlertError("Unable to load the passed State ID.")
			Response.End
		end if

	'If this is a license doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 7 then
	
		dim oLicense
		set oLicense = new License
		oLicense.ConnectionString = application("sDataSourceName")
		oLicense.VocalErrors = application("bVocalErrors")
		if oLicense.LoadLicenseById(oAssociateDoc.OwnerId) <> 0 then
			if oLicense.OwnerTypeId = 1 then
				if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 2 then
				if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 3 then
				if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 4 then
				if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 5 then
				if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			end if
		else
			AlertError("Unable to load the passed License ID.")
			Response.End
		end if

	end if 
	
	iOwnerId = oAssociateDoc.OwnerId
	iOwnerTypeId = oAssociateDoc.OwnerTypeId
	
else

	'We needed an ID to make this useful.
	AlertError("Failed to load a passed Document ID.")
	Response.End
		
end if 




'Mark the document as deleted and save.
oAssociateDoc.Deleted = 1
if oAssociateDoc.SaveDoc = 0 then

	AlertError("Failed to save the document information")
	Response.End

end if 



'Redirect back to the appropriate page.
if oAssociateDoc.OwnerTypeId = 1 then
	Response.Redirect("/officers/officerdetail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 2 then
	Response.Redirect("/branches/branchdetail.asp?branchid=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 4 then
	Response.Redirect("/branches/companyl2detail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 5 then
	Response.Redirect("/branches/companyl3detail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 3 then
	Response.Redirect("/company/companydetail.asp?id=" & oAssociateDoc.OwnerId)
elseif oAssociateDoc.OwnerTypeId = 6 then
	Response.Redirect("/states/stateguide.asp?state=" & oAssociateDoc.LookupState(oAssociateDoc.OwnerId, 1))
elseif oAssociateDoc.OwnerTypeId = 7 then
	Response.Redirect("/officers/modlicense.asp?lid=" & oAssociateDoc.OwnerId)
end if 



set oAssociateDoc = nothing



'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
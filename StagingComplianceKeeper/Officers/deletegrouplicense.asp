<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Group.Class.asp" ------------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
dim iGroupsLicensesId 'ID of the group employee in question
dim iGroupId 'ID of the group
dim oRs 'Recordset object

'Get the passed Ids
iGroupsLicensesId = ScrubForSql(request("Id"))
iGroupId = ScrubForSql(request("GroupId"))

dim oGroup 'Group Object

'Initialize the group object
set oGroup = new Group
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")

dim success

success = oGroup.DeleteLicense(iGroupsLicensesId)

'Clean up
set oGroup = nothing

if success = "0" then 
	
	AlertError("Failed to delete Group License.")
	Response.End

else
	
    if iGroupID <> "" then
        Response.Redirect("/officers/groupdetail.asp?id=" & iGroupId)
    end if    

end if 

%>

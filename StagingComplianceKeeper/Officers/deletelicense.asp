<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iLicenseId 'ID of the licenese in question
dim iOwnerId 'ID of whatever owner type owns the license
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerBranchId 'Is there a specific branch to which this license belongs?
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iLicenseId = ScrubForSql(request("Id"))

dim oLicense 'License Object

'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'Declare the Associate object and the Company object.  We'll use these to
'verify permissions for new licenses.
dim oAssociate
dim oCompany

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed License ID.")
		Response.End
	
	end if 
	
	sMode = "Edit"

else
	
	'We needed a license ID to make this useful.
	AlertError("Failed to load a passed License ID.")
	Response.End

end if 

iOwnerId = oLicense.OwnerId
iOwnerTypeId = oLicense.OwnerTypeId

if iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	'If this is an Officer license...
	if iOwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(iOwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
	
	'If this is a Branch license...
	elseif iOwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			Response.Write("Unable to load the passed Branch ID.<br>" & vbCrLf)
			Response.End
		end if 
		
	'If this is a Company license...
	elseif iOwnerTypeId = 3 then
	
		if (iOwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
			AlertError("Unable to load the passed Company ID.")
			Response.End
		end if 
		
	'If this is a companyL2 license
	elseif iOwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license
	elseif iOwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
	
	else
	
		Response.Write("Invalid Owner Type specified.<br>" & vbCrLf)
		Response.End
	
	end if 
	
else
	
	'We needed one of the above to make this useful.
	Response.Write("Failed to load a Owner ID.<br>" & vbCrLf)
	Response.End

end if 




'We have a valid license.  Go ahead and set the properties based on our passed 
'form data.
if oLicense.DeleteLicense = 0 then

	AlertError("Failed to delete the License.")
	Response.End

end if 




'Clean up
set oAssociate = nothing
set oCompany = nothing


'Redirect back to the appropriate page.
if oLicense.OwnerTypeId = 1 then

	Response.Redirect("/officers/officerdetail.asp?id=" & oLicense.OwnerId)

elseif oLicense.OwnerTypeId = 2 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oLicense.OwnerId)
	
elseif oLicense.OwnerTypeId = 4 then

	Response.Redirect("/branches/companyl2detail.asp?id=" & oLicense.OwnerId)
	
elseif oLicense.OwnerTypeId = 5 then

	Response.Redirect("/branches/companyl3detail.asp?id=" & oLicense.OwnerId)
	
elseif oLicense.OwnerTypeId = 3 then

	Response.Redirect("/company/companydetail.asp?id=" & oLicense.OwnerId)
	
end if 



%>

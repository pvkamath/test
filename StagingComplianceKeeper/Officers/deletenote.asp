<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Note.Class.asp" --------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for note type/id
dim iNoteId 'ID of the note in question
dim iOwnerId 'ID of whatever owner type owns the note
dim iOwnerTypeId 'What kind of note?  Officer, Company, etc.
dim iAssociateBranchId 'What is the owning associate's branch ID?
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iNoteId = ScrubForSql(request("Id"))

dim oNote 'Note Object

'Initialize the note object
set oNote = new Note
oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")

'Declare the Associate object and the Company object.  We'll use these to
'verify permissions for new licenses.
dim oAssociate
dim oCompany
dim oBranch

'If we were passed a Note ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iNoteId <> "" then

	if oNote.LoadNoteById(iNoteId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed Note ID.")
		Response.End
	
	end if 
	
else

	'We needed a Note ID to make this useful.
	AlertError("Failed to load a passed Note ID.")
	Response.End

end if 


iOwnerId = oNote.OwnerId
iOwnerTypeId = oNote.OwnerTypeId

if iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	'If this is an Officer note...
	if iOwnerTypeId = 1 then

		if not CheckIsThisAssociateAdmin(iOwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
	
	'If this is a Branch note...
	elseif iOwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			Response.Write("Unable to load the passed Branch ID.<br>" & vbCrLf)
			Response.End
		end if 
	
	'If this is a Company note...
	elseif iOwnerTypeId = 3 then
	
		if (iOwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
			AlertError("Unable to load the passed Company ID.")
			Response.End
		end if 
		
	'If this is a companyL2 note
	elseif iOwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 note
	elseif iOwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
	
	else
	
		AlertError("Invalid Owner Type specified.")
		Response.End
	
	end if 
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 




'We have a valid note.  Go ahead and set the properties based on our passed 
'form data.
if oNote.DeleteNote = 0 then

	AlertError("Failed to delete the Note.")
	Response.End

end if



'Clean up
set oAssociate = nothing
set oCompany = nothing
set oBranch = nothing

'Redirect back to the appropriate page.
if oNote.OwnerTypeId = 1 then

	Response.Redirect("/officers/officerdetail.asp?id=" & oNote.OwnerId)

elseif oNote.OwnerTypeId = 2 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 3 then

	Response.Redirect("/company/companydetail.asp?id=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 4 then

	Response.Redirect("/branches/companyl2detail.asp?id=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 5 then

	Response.Redirect("/branches/companyl3detail.asp?id=" & oNote.OwnerId)
	
end if 

%>
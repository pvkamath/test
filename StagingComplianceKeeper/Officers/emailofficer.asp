<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

dim iOfficerId
iOfficerId = ScrubForSql(request("id"))

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	AlertError("Failed to load the passed Company ID.")
	Response.end
		
end if 

if iOfficerId <> "" then 

	if oAssociate.LoadAssociateById(iOfficerId) = 0 then

		'The load was unsuccessful.  End.
		AlertError("Failed to load the passed Officer ID.")
		Response.End
	
	end if
	
	if oAssociate.CompanyId <> oCompany.CompanyId then
	
		'This is not their officer!
		AlertError("You do not have access to this Officer.")
		Response.End
		
	elseif CheckIsBranchAdmin() then
		
		'Make sure that this associate actually belongs to this branchadmin.
		if not CheckIsThisBranchAdmin(oAssociate.BranchId) then
		
			AlertError("You do not have access to this Officer.")
			Response.End
		
		end if
		
	end if 

	sMode = "Edit"

else

	AlertError("This page requires a specified Officer ID.")

end if
%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.Subject, "Subject")
			|| !checkString(FORM.EmailText, "Body")
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- Email -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Send Email</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Email an Officer</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="EmailOfficerForm" action="emailofficerprev.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the OfficerID on to the next step so we know we're working with an existing
'loan officer.
if iOfficerId <> "" then
%>
<input type="hidden" value="<% = iOfficerId %>" name="OfficerId">
<%
end if
%>
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>Loan Officer: </b>
		</td>
		<td align="left" valign="top">
			<% = oAssociate.FullName %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Email Address: </b>
		</td>
		<td align="left" valign="top">
			<% = oAssociate.Email %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>CC: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="CCs" value="" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Subject: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Subject" value="" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Body: </b>
		</td>
		<td align="left" valign="top">
			<textarea name="EmailText" cols="50" rows="7"></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
set oAssociate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
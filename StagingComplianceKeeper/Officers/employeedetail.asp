<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/Course.Class.asp" --------------------------->
<!-- #include virtual="/includes/Test.Class.asp" ----------------------------->
<!-- #include virtual="/includes/Group.Class.asp" ---------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim oRs
	' Set Page Title:
	sPgeTitle = "Employee Profile"
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the hierarchy objects
dim oCompany
dim oBranch
dim oCompanyL2
dim oCompanyL3


'Declare and initialize the Associate object
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
dim bCheckIsThisAssociateAdmin
dim bCheckisThisAssociateViewer
iAssociateId = ScrubForSql(request("id"))


'Check that we were passed a valid Associate ID to load.
if oAssociate.LoadAssociateById(iAssociateID) = 0 then
	AlertError("This page requires a valid Employee ID.")
	Response.End
end if 

'Check that we have viewer or admin access to this officer
if CheckIsThisAssociateAdmin(iAssociateId) then
	bCheckIsThisAssociateAdmin = true	
elseif CheckIsThisAssociateViewer(iAssociateId) then
	bCheckIsThisAssociateViewer = true
else
	AlertError("Unable to load the passed Employee ID.")
	Reponse.End
end if 


'Initialize associate docs object
dim oAssociateDoc 
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = 'deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmOfficerDelete(p_iCandidateId,p_sCandidateName)
{
	if (confirm("Are you sure you wish to delete the following Employee:\n  " + p_sCandidateName + "\n\nAll information will be deleted."))
		window.location.href = 'deleteofficer.asp?id=' + p_iCandidateId;
}
function ConfirmGroupDelete(p_iGroupEmployeeId, p_iAssociateId) {
    if (confirm("Are you sure you wish to delete this Group?  All information in the record will be deleted."))
        window.location.href = 'deletegroupemployee.asp?id=' + p_iGroupEmployeeId + '&AssociateID=' + p_iAssociateId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = 'deletenote.asp?id=' + p_iNoteId;
}
function ConfirmCourseAssocDelete(p_iAssocId, p_iOfficerId)
{
	if (confirm("Are you sure you wish to delete this Course record?  All information in the record will be deleted."))
		window.location.href = 'deleteofficercourse.asp?aid=' + p_iAssocId + '&id=' + p_iOfficerId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = 'deletedocument.asp?docid=' + p_iDocId;
}

function ConfirmTestDelete(p_sTestName, p_iTestID)
{
	if (confirm("Are you sure you wish to delete" + p_sTestName + "?  All information in the record will be deleted."))
		window.location.href = 'modtestproc.asp?userid=<% = oAssociate.UserId %>&TestID=' + p_iTestID + '&mode=DELETE';
}

function OpenMoveLicWindow(p_iLicenseId,p_sTextbox) {
	window.open("/officers/MoveLicWin.asp?lid=" + p_iLicenseId + "&textbox=" + p_sTextbox , "ImageWindow", "width=500,height=300,scrollbars=1")
}

function HideField(controlName) {
    var ctrl;
    ctrl = document.getElementById(controlName);
    if (ctrl != null) {
        ctrl.style.color = "#cccccc";
        ctrl.style.backgroundColor = "#cccccc";
    }
}

function ShowField(controlName) {
    var ctrl;
    ctrl = document.getElementById(controlName);
    if (ctrl != null) {
        ctrl.style.color = "";
        ctrl.style.backgroundColor = "";
    }
}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Officer Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <% = oAssociate.FirstName & " " & oAssociate.LastName %></td>
				</tr>		

						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modemployee.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Employee Information" alt="Edit Employee Information"> &nbsp;Edit Employee</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmOfficerDelete(<% = oAssociate.UserId %>,'<% = replace(oAssociate.FirstName,"'","\'") & " " & replace(oAssociate.LastName,"'","\'") %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This Officer" alt="Delete This Officer"> Delete Officer</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="emailofficer.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnMail.gif" align="top" border="0" title="Email Notices" alt="Email Notices"> &nbsp;Email Notices</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
												<table border="0" cellpadding="5" cellspacing="0">
													<tr>
														<td class="newstitle" nowrap>Email: </td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td><% = oAssociate.Email %></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
										 		</table>
											</td>
											<td valign="top" width="50%">
												<table border="0" cellpadding="5" cellspacing="0" valign="top">
													<tr>
														<td valign="top" class="newstitle" nowrap>Type: </td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td><% = oAssociate.GetAssociateTypeName() %></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>

<% 
dim oGroup
set oGroup = new Group

oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")

%>						
			<!-- Group Employees -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Employee Groups</td>
				</tr>

						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modgroupemployee.asp?employeeid=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Assign Employee to Group" alt="Assign Employee to Group"> Add Group</a></td>
									</tr>
								</table>
							</td>
						</tr>
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top">
<%
dim oGroupEmployeeRs
dim sColor
set oGroupEmployeeRs = oAssociate.GetGroups()

if not (oGroupEmployeeRs.BOF and oGroupEmployeeRs.EOF) then

%>									
	                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
		                                            <tr>
			                                            <td></td>
			                                            <td><b>Group Name</b></td>
		                                            </tr>
<%
    do while not oGroupEmployeeRs.EOF
		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

        Response.Write("<tr>")
		Response.Write("<td width=""20px""" & sColor & "><a href=""javascript:ConfirmGroupDelete(" & oGroupEmployeeRs("Id") & "," & iAssociateID & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Group"" alt=""Delete This Group""></a></td>" & vbCrLf)
        Response.Write("<td " & sColor & ">" & oGroupEmployeeRs("GroupName") & "</td>")
        Response.Write("</tr>")

        oGroupEmployeeRs.MoveNext
    loop
%>
                                                </table>
<%
else
	
	Response.Write("No Groups found.")
	
end if 
set oGroupEmployeeRs = nothing
%>						
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>						
			
			<!-- Officer Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Employee Licenses</td>
				</tr>

						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modlicense.asp?oid=<% = oAssociate.UserId %>&AssociateTypeID=<%=oAssociate.AssociateTypeID%>&AssociateTerm=Employee" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
							<tr>
								<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>

                                            


<%
dim oLicense
dim oLicRs
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oAssociate.UserId
oLicense.OwnerTypeId = 1


set oRs = oLicense.SearchLicensesByState()

if not (oRs.EOF and oRs.BOF) then
	%>
	<table width="100%" cellpadding="5" cellspacing="0" border="0">
		<tr>
			<% if bCheckIsThisAssociateAdmin then %>
			<td></td>
			<td></td>
			<td></td>
			<% end if %>
			<td><b>Number</b></td>
			<td><b>Type</b></td>
			<td><b>Status</b></td>
            <td><b>Status Date</b></td>
			<td><b>Expiration Date</b></td>
		</tr>
    <%
	do while not oRs.EOF
	    %>
            <tr>
                <td colspan="100%">
		<%

		Response.Write("<b>(" & oRs("LicenseCount") & ") " & oLicense.LookupState(oRs("LicenseStateId")))
		if oRs("LicenseCount") > 1 then 
			Response.Write(" Licenses ")
		else
			Response.Write(" License ")
		end if
		Response.Write("</b><br>")
		
        %>
                </td>
            </tr>
        <%
		'Get the full list if we've selected this state.

		oLicense.LicenseStateId = oRs("LicenseStateId")
		set oLicRs = oLicense.SearchLicenses()
			
			do while not oLicRs.EOF
                dim oLicenseInstance
                set oLicenseInstance = new License
                oLicenseInstance.ConnectionString = application("sDataSourceName")
                oLicenseInstance.VocalErrors = application("bVocalErrors")

				if oLicenseInstance.LoadLicenseById(oLicRs("LicenseId")) <> 0 then

					if sColor = "" then
						sColor = " bgcolor=""#ffffff"""
					else
						sColor = ""
					end if


					Response.Write("<tr>" & vbCrLf)
					if bCheckIsThisAssociateAdmin then
						Response.Write("<td " & sColor & "><a href=""#"" onClick=""javascript:OpenMoveLicWindow('" & oLicenseInstance.LicenseId & "','');""><img src=""" & application("sDynMediaPath") & "bttnMove.gif"" border=""0"" title=""Move This License"" alt=""Move This License""></a></td>" & vbCrLf)
						Response.Write("<td " & sColor & "><a href=modlicense.asp?lid=" & oLicenseInstance.LicenseId & "&AssociateTypeID=" & oAssociate.AssociateTypeID & "&AssociateTerm=Employee><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
						Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicenseInstance.LicenseId & ",'" & oLicenseInstance.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
					end if 
					Response.Write("<td " & sColor & "><a href=licensedetail.asp?lid=" & oLicenseInstance.Licenseid & ">" & oLicenseInstance.LicenseNum & "</a></td>" & vbCrLf)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LicenseType & "</td>" & vbCrLf)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LookupLicenseStatus(oLicenseInstance.LicenseStatusId) & "</td>" & vbCrLf)
                    Response.Write("<td " & sColor & ">" & oLicenseInstance.LicenseStatusDate)
					Response.Write("<td " & sColor & ">" & oLicenseInstance.LicenseExpDate)
					if oLicenseInstance.LicenseExpDate < date() + 120 and oLicenseInstance.LicenseExpDate > date() then
						Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicenseInstance.LicenseExpDate - date(), 0) & " Days</font>)</b>")
					end if
					Response.Write("</td>" & vbCrLf)
					Response.Write("</tr>" & vbCrLf)
							
				end if 

				oLicRs.MoveNext

			loop

            set oLicRs = nothing

		oRs.MoveNext
	
	loop

	%>
	</table>
	<%

else
	
	Response.Write("No Licenses found.")
	
end if 
set oLicRs = nothing
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						
			<!-- Officer Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Employee Notes</td>
				</tr>
						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modnote.asp?oid=<% = oAssociate.UserId %>&AssociateTypeID=<%=oAssociate.AssociateTypeID %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oAssociate.UserId
oNote.OwnerTypeId = 1

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
sColor = ""
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if bCheckIsThisAssociateAdmin and session("AllowOfficerNoteEditing") then %>
		<td></td>
		<td></td>
		<% end if %>
		<td width="40"><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="330"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if bCheckIsThisAssociateAdmin and oNote.UserId = session("User_Id") and session("AllowOfficerNoteEditing") then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & "&AssociateTypeID=" & oAssociate.AssociateTypeID & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			elseif bCheckIsThisAssociateAdmin and session("AllowOfficerNoteEditing") then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteDate & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
set oNote = nothing
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>





			<!-- Officer Course History -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Employee Education History</td>
				</tr>
				<% if bCheckIsThisAssociateAdmin then %>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td><a href="modofficercourse.asp?id=<% = oAssociate.UserId %>&AssociateTypeID=<%=oAssociate.AssociateTypeID%>&AssociateTerm=Employee" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Course" alt="Add New Course"> New Course</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<% end if %>
				<tr>
					<td class="bckRight">
						<table width="100%" border="0" cellpadding="15" cellspacing="0">
							<tr>
								<td>
<%
dim oCourseInfoRs
dim oCourse
dim dCourseExpDate

set oCourse = new Course
oCourse.VocalErrors = application("bVocalErrors")
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")

set oRs = oAssociate.GetCourses

'dim sColor
sColor = ""
if oRs.State <> 0 then

	if not oRs.EOF then
		bHasContent = true
	%>
									<table width="100%" cellpadding="5" cellspacing="0" border="0">
										<tr>
											<% if bCheckIsThisAssociateAdmin then %>
											<td width="25"></td>
											<td></td>
											<% end if %>
											<td><b>Course</b></td>
											<td><b>Provider</b></td>
											<!-- <td><b>Date</b></td> -->
											<td><b>Renewal Date</b></td>
											<td><b>Progress</b></td>
											<td><b>Certificate</b></td>
										</tr>
	<%
	else
	%>
	No Courses found.
	<%
	end if 

	do while not oRs.EOF

		'if oCourse.LoadCourseById(oRs("CourseId")) <> 0 then

			if oRs("UserSpecRenewalDate") <> "" then
				dCourseExpDate = oRs("UserSpecRenewalDate")
			else
				dCourseExpDate = oRs("CertificateExpirationDate")
			end if

			'if oRs("CertificateExpirationDate") <> "" and (oCourse.ProviderId <> "" or oCourse.ProviderId <> "0") then
			'	dCourseExpDate = oRs("CertificateExpirationDate")
			'else
			'	dCourseExpDate = oRs("UserSpecRenewalDate")
			'end if

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if

			Response.Write("<tr>" & vbCrLf)
			if bCheckIsThisAssociateAdmin and oRs("Id") < 0 then
				Response.Write("<td valign=""top"" " & sColor & "><a href=modofficercourse.asp?id=" & oAssociate.UserId & "&aid=" & oRs("Id") & "&AssociateTypeID=" & oAssociate.AssociateTypeID & "&AssociateTerm=Employee><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Education Record"" alt=""Edit This Education Record ""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmCourseAssocDelete(" & oRs("Id") & ", " & oAssociate.UserId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Education Record"" alt=""Delete This Education Record""></a></td>" & vbCrLf)
			elseif bCheckIsThisAssociateAdmin then
				Response.Write("<td valign=""top"" " & sColor & "><a href=modofficertpcourse.asp?id=" & oAssociate.UserId & "&aid=" & oRs("Id") & "><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Education Record"" alt=""Edit This Education Record ""></a></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oRs("Name") & "</td>" & vbCrLf)
			if oRs("ProviderId") <> "0" then 
				Response.Write("<td " & sColor & " nowrap>" & oCourse.LookupProvider(oRs("ProviderId")) & "</td>" & vbCrLf)
			else
				Response.Write("<td " & sColor & " nowrap>TrainingPro</td>" & vbCrLf)
			end if 
			'Response.Write("<td " & sColor & ">" & oRs("CompletionDate") & "</td>" & vbCrLf)
			'Response.Write("<td " & sColor & ">" & oCourse.ContEdHours & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & " nowrap>" & dCourseExpDate)
			if dCourseExpDate < date() + 120 and dCourseExpDate > date() then
				Response.Write("   <b>(<font color=""#cc0000"">" & round(dCourseExpDate - date(), 0) & " Days</font>)</b>")
			elseif dCourseExpDate = date() then
				Response.Write("   <b>(<font color=""#cc0000"">Today</font>)</b>")
			end if			
			Response.Write("</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">")
			if oRs("Id") > 0 and oRs("Completed") then
				Response.Write("Completed " & formatdatetime(oRs("CompletionDate"), 2))
			elseif (not oRs("Completed")) and (oRs("Id") > 0) then
				'Response.Write(oAssociate.LookupTpUserId(oAssociate.Email) & "," & oCourse.CourseId & " - ")
				'Response.Write(oAssociate.GetTpCourseProgress(oRs("CourseId"), oAssociate.LookupTpUserId(oAssociate.Email)) & "%")
                dim oUserCourseID
                oUserCourseID = oAssociate.GetTpUserCourseID(oRs("CourseID"), oAssociate.LookupTpUserId(oAssociate.Email))
                if oUserCourseID > 0 then
                    Response.Write(oAssociate.GetCourseCompletionPercentage(oUserCourseID) & "%")
                else
                    Response.Write("0%")
                end if
			elseif (not oRs("Completed")) then
				Response.Write("In Progress")
			else
				Response.Write("Completed " & formatdatetime(oRs("CompletionDate"), 2))
			end if
			Response.Write("</td>" & vbCrLf)
			if oRs("Id") > 0 and oRs("CertificateReleased") then
				'set oCourseInfoRs = oAssociate.GetTpCourse(oRs("Id"))
				'if not oCourseInfoRs.EOF then
'					if oCourseInfoRs("CertificateReleased") then
				Response.Write("<td " & sColor & " align=""center""><a href=""/courses/viewcertificate.asp?id=" & oAssociate.UserId & "&usercourseid=" & oRs("Id") & """>View</a></td>")
			else
				Response.Write("<td " & sColor & "></td>")
			end if
'				else
'					Response.Write("<td " & sColor & "></td>")
'				end if 
'			else
'				Response.Write("<td " & sColor & "></td>")
'			end if 
			Response.Write("</tr>" & vbCrLf)
		
		'end if 

		oRs.MoveNext

	loop
	
	if bHasContent = true then
		%></table><%
	end if 
else
		Response.Write("No Courses found.")
end if
set oCourse = new Course
%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>


			<!-- Officer Tests -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Employee Tests</td>
				</tr>
						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modtest.asp?oid=<% = oAssociate.UserId %>&AssociateTypeID=<%=oAssociate.AssociateTypeID%>&AssociateTerm=Employee" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Test" alt="Add New Test"> New Test</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oTest
dim sStatus

set oTest = new Test

oTest.ConnectionString = application("sDataSourceName")
oTest.UserID = oAssociate.UserId

set oRs = oTest.GetTests()

'if not (oRs.EOF and oRs.BOF) then
sColor = ""

if not oRS.eof then
%>
												<table width="100%" cellpadding="5" cellspacing="0" border="0">
													<tr>
														<% if bCheckIsThisAssociateAdmin then %>
														<td></td>
														<td></td>
														<% end if %>
														<td width=""><b>Name</b></td>
														<td></td>
														<td width=""><b>State</b></td>
														<td></td>														
														<td width=""><b>Test Date</b></td>
														<td></td>
														<td width=""><b>Status</b></td>
													</tr>
<%
	do while not oRS.eof
		if sColor = "" then
			sColor = "#ffffff"
		else
			sColor = ""
		end if

		Response.Write("<tr bgcolor=""" & sColor & """>" & vbCrLf)		
		
		if bCheckIsThisAssociateAdmin  then
			Response.Write("<td valign=""top""><a href=modtest.asp?oid=" & oAssociate.UserId & "&TestID=" & oRS("TestID") & "&AssociateTypeID=" & oAssociate.AssociateTypeID & "&AssociateTerm=Employee><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Test"" alt=""Edit This Test""></a></td>" & vbCrLf)
			Response.Write("<td valign=""top""><a href=""javascript:ConfirmTestDelete('" & replace(oRS("Name"), "'", "\'") & "', " & oRS("TestID") & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Test"" alt=""Delete This Test""></a></td>" & vbCrLf)
		else
			Response.Write("<td></td>" & vbCrLf)
			Response.Write("<td></td>" & vbCrLf)
		end if 
			
		Response.Write("<td valign=""top"">" & oRS("Name") & "</td>" & vbcrlf)
		Response.Write("<td valign=""top""></td>" & vbcrlf)
		Response.Write("<td valign=""top"">" & GetStateName(oRS("StateID"), false) & "</td>" & vbcrlf)
		Response.Write("<td valign=""top""></td>" & vbcrlf)
		Response.Write("<td valign=""top"">" & oRS("TestDate") & "</td>" & vbcrlf)
		Response.Write("<td valign=""top""></td>" & vbcrlf)
		Response.Write("<td valign=""top"">" & oRS("Status") & "</td>" & vbcrlf)
			
		Response.Write("</tr>" & vbcrlf)
		oRS.MoveNext
	loop
%>													
												</table>
<%
else
	response.write("No Tests found.")
end if 
set oTest = nothing												
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>		
		
		
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<%	
set oAssociate = nothing
set oRS = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
PrintShellFooter SHOW_MENUS										' From shell.asp
EndPage														' From htmlElements.asp
%>

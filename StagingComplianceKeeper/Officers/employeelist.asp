<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual="/includes/Associate.Class.asp" ------------------------------------------------>
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Employee Admin"



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oAssociate

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

dim iVerified
dim iLicensed

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")

dim iShowDeleted
iShowDeleted = ScrubForSql(request("ShowDeleted"))
if (iShowDeleted = "3") then
	iShowDeleted = 3
else
	iShowDeleted = 1
end if 
	

'if request("ShowDeletedOfficers") = "1" then
'	oAssociate.UserStatus = 3
'else
	oAssociate.UserStatus = 1
'end if 

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchCandidateLastName") 
	Session.Contents.Remove("SearchCandidateSsn") 
	Session.Contents.Remove("SearchCandidateLicenseNum") 
	Session.Contents.Remove("SearchCandidateCompanyId") 
	Session.Contents.Remove("SearchCandidateStateId") 
    Session.Contents.Remove("SearchCandidateNMLSNumber")
	Session.Contents.Remove("SearchCandidateLetter") 
    Session.Contents.Remove("SearchAssociateType")
	Session.Contents.Remove("CurPage") 
else

	oAssociate.LastName = ScrubForSql(request("LastName"))
	if (oAssociate.LastName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCandidateLastName")) <> "") then
			oAssociate.LastName = session("SearchCandidateLastName")
		end if
	else
		session("SearchCandidateLastName") = oAssociate.LastName
	end if

	oAssociate.Ssn = enDeCrypt(ScrubForSql(request("Ssn")),application("RC4Pass"))
	if (oAssociate.Ssn = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCandidateSsn")) <> "") then
			oAssociate.Ssn = session("SearchCandidateSsn")
		end if
	else
		session("SearchCandidateSsn") = oAssociate.Ssn
	end if

	oAssociate.SearchStateId = ScrubForSql(request("StateId"))
	if (oAssociate.SearchStateId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCandidateStateId")) <> "") then
			oAssociate.SearchStateId = session("SearchCandidateStateId")
		end if
	else
		session("SearchCandidateStateId") = oAssociate.SearchStateId
	end if

	oAssociate.UserStatus = iShowDeleted
	if (oAssociate.UserStatus = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchDeletedOfficers")) <> "") then
			oAssociate.UserStatus = session("SearchDeletedOfficers")
		end if
	else
		session("SearchDeletedOfficers") = oAssociate.UserStatus
	end if
	
	oAssociate.SearchLicenseNumber = ScrubForSql(request("LicenseNum"))
	if (oAssociate.SearchLicenseNumber = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCandidateLicenseNum")) <> "") then
			oAssociate.SearchLicenseNumber = session("SearchCandidateLicenseNum")
		end if
	else
		session("SearchCandidateLicenseNum") = oAssociate.SearchLicenseNumber
	end if
	
	oAssociate.SearchLastNameStart = ScrubForSql(request("letter"))
	if (oAssociate.SearchLastNameStart = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchCandidateLastNameStart")) <> "") then
			oAssociate.SearchLastNameStart = session("SearchCandidateLastNameStart")
		end if
	else
		session("SearchCandidateLastNameStart") = oAssociate.SearchLastNameStart
	end if
	
    oAssociate.NMLSNumber = ScrubForSql(request("NMLSNumber"))
    if (oAssociate.NMLSNumber = "") and (ucase(sAction) <> "SEARCH") then
        if (trim(session("SearchCandidateNMLSNumber")) <> "") then
            oAssociate.NMLSNumber = session("SearchCandidateNMLSNumber")
        end if
    else
        session("SearchCandidateNMLSNumber") = oAssociate.NMLSNumber
    end if
    
    oAssociate.AssociateTypeID = ScrubForSql(request("AssociateTypeID"))
    if (oAssociate.AssociateTypeID = "") and (ucase(sAction) <> "SEARCH") then
        if (trim(session("SearchAssociateTypeID")) <> "") then
            oAssociate.AssociateTypeID = session("SearchAssociateTypeID")
        end if
    else
        session("SearchAssociateTypeID") = oAssociate.AssociateTypeID
    end if

	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") and (ucase(sAction) <> "SEARCH") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if

end if


'Restrict the list if the user does not have company-wide access.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
end if


iVerified = request("ShowVerified")
session("SearchVerified") = iVerified
if iVerified = 1 then
	oAssociate.Verified = true
elseif iVerified = 2 then
	oAssociate.Verified = false
else
	oAssociate.Verified = ""
end if



iLicensed = request("ShowLicensed")
session("SearchLicensed") = iLicensed
if iLicensed = 1 then
	oAssociate.Licensed = true
elseif iLicensed = 2 then
	oAssociate.Licensed = false
else
	oAssociate.Licensed = ""
end if

oAssociate.SearchExcludeLoanOfficer = true

	set oRs = oAssociate.SearchAssociates()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20
	
%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Employees" align="absmiddle" vspace="10"> Employees</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<form name="frm1" action="EmployeeList.asp" method="POST">
								<input type="hidden" name="action" value="SEARCH">
								<input type="hidden" name="page_number" value="1">
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Last Name: </td>
											<td><input type="text" name="LastName" size="20" maxlength="100" value="<% = oAssociate.LastName %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>License Number: </td>
											<td><input type="text" name="LicenseNum" size="20" value="<% = oAssociate.SearchLicenseNumber %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
                                        <tr>
                                        <td class="newstitle" nowrap>Verified:</td>
                                            <td valign="top">
											<select name="ShowVerified">
                                                <option value="0"<% if session("SearchVerified") = 0 then %> SELECTED<% end if %>>All</option>
                                                <option value="1"<% if session("SearchVerified") = 1 then %> SELECTED<% end if %>>Verified</option>
                                                <option value="2"<% if session("SearchVerified") = 2 then %> SELECTED<% end if %>>Not Verified</option>											                                               
											</select>
											</td>                                      
                                        </tr>
										<tr>
											<td class="newstitle" nowrap>NMLS Number: </td>
											<td><input type="text" name="NMLSNumber" size="20" maxlength="100" value="<% = oAssociate.NMLSNumber %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="3"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Social Security Number: </td>
											<td><input type="text" name="SSN" size="20" value="<% = enDeCrypt(oAssociate.Ssn,application("RC4Pass")) %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td colspan="2" width="100%" valign="middle"><% call DisplayStatesDropDown(oAssociate.SearchStateId,0,"StateId") 'functions.asp %></td>
                                            </tr>
										<tr>
											<td class="newstitle" nowrap>Licensed:</td>
                                            <td valign="top">
											<select name="ShowLicensed">
                                                <option value="0"<% if session("SearchLicensed") = 0 then %> SELECTED<% end if %>>All</option>
                                                <option value="1"<% if session("SearchLicensed") = 1 then %> SELECTED<% end if %>>Licensed</option>
                                                <option value="2"<% if session("SearchLicensed") = 2 then %> SELECTED<% end if %>>Not Licensed</option>											                                                
											</select>
											</td>
										</tr>
                                        <tr>
                                            <td class="newstitle" nowrap>Employee Type: </td>
                                            <td colspan="2" width="100%" valign="middle"><% call DisplayAssociateTypeDropDown(oAssociate.AssociateTypeID,0,oAssociate.CompanyId,"AssociateTypeID",false, false) %>
                                            </td>
                                            
                                        </tr>
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>

				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td align="center">
															<a href="employeelist.asp?action=search">All</a> |
															<a href="employeelist.asp?letter=A">A</a> | 
															<a href="employeelist.asp?letter=B">B</a> |
															<a href="employeelist.asp?letter=C">C</a> |
															<a href="employeelist.asp?letter=D">D</a> |
															<a href="employeelist.asp?letter=E">E</a> |
															<a href="employeelist.asp?letter=F">F</a> |
															<a href="employeelist.asp?letter=G">G</a> |
															<a href="employeelist.asp?letter=H">H</a> |
															<a href="employeelist.asp?letter=I">I</a> |
															<a href="employeelist.asp?letter=J">J</a> |
															<a href="employeelist.asp?letter=K">K</a> |
															<a href="employeelist.asp?letter=L">L</a> |
															<a href="employeelist.asp?letter=M">M</a> |
															<a href="employeelist.asp?letter=N">N</a> |
															<a href="employeelist.asp?letter=O">O</a> |
															<a href="employeelist.asp?letter=P">P</a> |
															<a href="employeelist.asp?letter=Q">Q</a> |
															<a href="employeelist.asp?letter=R">R</a> |
															<a href="employeelist.asp?letter=S">S</a> |
															<a href="employeelist.asp?letter=T">T</a> |
															<a href="employeelist.asp?letter=U">U</a> |
															<a href="employeelist.asp?letter=V">V</a> |
															<a href="employeelist.asp?letter=W">W</a> |
															<a href="employeelist.asp?letter=X">X</a> |
															<a href="employeelist.asp?letter=Y">Y</a> |
															<a href="employeelist.asp?letter=Z">Z</a>
														</td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
													</tr>
												</table>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
									<CENTER>

									</center>
									<p>
<%

dim sLetter
dim iLetterPage
dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value

	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
	
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		
		oRs.AbsolutePage = iCurPage
		
		'Find the requested page if a letter was specified.
'		sLetter = ucase(left(request("letter"), 1))
'		if sLetter <> "" then
'			oRs.MoveFirst
'			do while iLetterPage = ""
'				do while (not oRs.EOF) and (iLetterPage = "")
'					if ucase(left(oRs("LastName"), 1)) = ucase(left(sLetter, 1)) then
'						iLetterPage = oRs.AbsolutePage
'					end if
'					oRs.MoveNext
'				loop
'				if iLetterPage <> "" then
'					oRs.AbsolutePage = iLetterPage
'					iCurPage = iLetterPage
'				else
'					'Move back a letter and try again
'					if sLetter = "A" then
'						iLetterPage = "1"
'						oRs.MoveFirst
'					else
'						sLetter = chr(asc(sLetter) - 1)
'						oRs.MoveFirst
'					end if
'				end if
'			loop
'		else
'			'Set the beginning record to be displayed on the page
'			oRs.AbsolutePage = iCurPage
'		end if
		
	
		'Initialize the row/col style values
		bCol = false
		sColor = ""
		
	
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
				if not bCol then
					%>
										<tr>
											<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="modemployee.asp?id=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"></a>&nbsp;&nbsp;<% end if %><a href="employeedetail.asp?id=<% = oRs("UserId") %>" title="View Officer Information" <% if oAssociate.Inactive then %>class="greyedLink"<% end if %>><% = oAssociate.LastName %>, <% = oAssociate.FirstName %></a></td>
											<td width="4%"></td>
					<%
				else
					%>
											<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="modemployee.asp?id=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"></a>&nbsp;&nbsp;<% end if %><a href="employeedetail.asp?id=<% = oRs("UserId") %>" title="View Officer Information" <% if oAssociate.Inactive then %>class="greyedLink"<% end if %>><% = oAssociate.LastName %>, <% = oAssociate.FirstName %></a></td>
										</tr>
					<%
				end if


				'Set the col to the opposite value
				if bCol = false then
					bCol = true
				else
					bCol = false
					'Every two columns, set the row to the opposite color
					if sColor = "" then
						sColor = " bgcolor=""#ffffff"""
					else
						sColor = ""
					end if
				end if

			else
			
				Response.Write("NOPE")
				
			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

				
		'Finish off the table row if we ended on a left column.  Note that we check
		'for the positive bCol because the value is flipped at the end of the loop.
		if bCol then
			%>	
											<td width="48%" align="center"<% = sColor %>>&nbsp;</td>
										</tr>
			<%
		end if 	
		
		
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""employeelist.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""employeelist.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)	
		'response.write("</table>" & vbcrlf)
	
		'display Page number
		'response.write("<table width=""100%"">" & vbcrlf)	
		'response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		'response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)		
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%
	else
		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;There are currently no Employees that match your search criteria.</a></td>
										</tr>
		<%
		'response.write("<tr><td colspan=""4"">There are currently no Loan Officers that matched your search criteria.</td></tr>" & vbcrlf)
	end if
%>
									</table>
								</td>
							</tr>
						</table>




								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oAssociate = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual="/includes/Group.Class.asp" ---------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim oRs
	' Set Page Title:
	sPgeTitle = "Officer Profile"
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

'Declare and initialize the Associate object
dim oGroup
set oGroup = new Group
oGroup.VocalErrors = application("bVocalErrors")
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")

dim iGroupId
iGroupId = ScrubForSql(request("id"))


'Check that we were passed a valid Associate ID to load.
if oGroup.LoadGroupById(iGroupID) = 0 then
	AlertError("This page requires a valid Group ID.")
	Response.End
end if 

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmGroupDelete(p_iGroupID,p_sGroupName)
{
	if (confirm("Are you sure you wish to delete the following Group:\n  " + p_sGroupName + "\n\nAll information will be deleted."))
		window.location.href = 'deletegroup.asp?id=' + p_iGroupId;
}

function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = 'deletenote.asp?id=' + p_iNoteId;
}

function ConfirmEmployeeDelete(p_iGroupEmployeeId, p_iGroupId)
{
	if (confirm("Are you sure you wish to delete this Employee?  All information in the record will be deleted."))
	    window.location.href = 'deletegroupemployee.asp?id=' + p_iGroupEmployeeId + '&GroupID=' + p_iGroupId;
}

function ConfirmLicenseDelete(p_iGroupLicenseId, p_iGroupId) {
    if (confirm("Are you sure you wish to delete this License?  All information in the record will be deleted."))
        window.location.href = 'deletegrouplicense.asp?id=' + p_iGroupLicenseId + '&GroupID=' + p_iGroupId;
}

</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Officer Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <% = oGroup.GroupName %></td>
				</tr>		

						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modgroup.asp?id=<% = oGroup.GroupId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Group Information" alt="Edit Group Information"> &nbsp;Edit Group</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmGroupDelete(<% = oGroup.GroupId %>,'<% = oGroup.GroupName %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This Group" alt="Delete This Group"> Delete Group</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"></td>
									</tr>
								</table>
							</td>
						</tr>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
												<table border="0" cellpadding="5" cellspacing="0">
													<tr>
														<td class="newstitle" nowrap>Group Name: </td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td><% = oGroup.GroupName %></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
										 		</table>
											</td>
											<td valign="top" width="50%">
												<table border="0" cellpadding="5" cellspacing="0" valign="top">
													<tr>
														<td class="newstitle" nowrap>Group Leader: </td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td><% = oGroup.AssociateName %></td>
													</tr>													
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
			<!-- Group Employees -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Group Employees</td>
				</tr>

						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modgroupemployee.asp?groupid=<% = oGroup.GroupId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add Employee to Group" alt="Add Employee to Group"> Add Employee</a></td>
									</tr>
								</table>
							</td>
						</tr>
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top">
<%
dim oGroupEmployeeRs
dim sColor
set oGroupEmployeeRs = oGroup.GetEmployees

if not (oGroupEmployeeRs.BOF and oGroupEmployeeRs.EOF) then

%>									
	                                            <table width="100%" cellpadding="5" cellspacing="0" border="0">
		                                            <tr>
			                                            <td></td>
			                                            <td><b>Employee Name</b></td>
		                                            </tr>
<%
    do while not oGroupEmployeeRs.EOF
		if sColor = "" then
			sColor = " bgcolor=""#ffffff"""
		else
			sColor = ""
		end if

        Response.Write("<tr>")
		Response.Write("<td width=""20px""" & sColor & "><a href=""javascript:ConfirmEmployeeDelete(" & oGroupEmployeeRs("Id") & "," & iGroupID & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
        Response.Write("<td " & sColor & ">" & oGroupEmployeeRs("LastName") & ", " & oGroupEmployeeRs("FirstName") & "</td>")
        Response.Write("</tr>")

        oGroupEmployeeRs.MoveNext
    loop
%>
                                                </table>
<%
else
	
	Response.Write("No Employees found.")
	
end if 
set oGroupEmployeeRs = nothing
%>						
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			
			<!-- Group Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Group Licenses</td>
				</tr>

						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modgrouplicense.asp?GroupID=<% = iGroupID %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> Add License</a></td>
									</tr>
								</table>
							</td>
						</tr>
								
							<tr>
								<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>

                                            


<%
dim oGroupLicenseRs
set oGroupLicenseRs = oGroup.GetLicenses

sColor = ""

dim oLicense

if not (oGroupLicenseRs.EOF and oGroupLicenseRs.BOF) then
	%>
	<table width="100%" cellpadding="5" cellspacing="0" border="0">
		<tr>
			<td></td>
			<td><b>Number</b></td>
			<td><b>Type</b></td>
			<td><b>Status</b></td>
            <td><b>Status Date</b></td>
			<td><b>Expiration Date</b></td>
		</tr>
    <%
	do while not oGroupLicenseRs.EOF
	    %>
            <tr>
                <td colspan="100%">
		<%
        set oLicense = new License

        oLicense.ConnectionString = application("sDataSourceName")
        oLicense.VocalErrors = application("bVocalErrors")

        if oLicense.LoadLicenseByID(oGroupLicenseRs("LicenseID")) <> "" then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oGroupLicenseRs("Id") & ",'" & iGroupId & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
			Response.Write("<td " & sColor & "><a href=licensedetail.asp?lid=" & oLicense.Licenseid & ">" & oLicense.LicenseNum & "</a></td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LicenseType & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">" & oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) & "</td>" & vbCrLf)
            Response.Write("<td " & sColor & ">" & oLicense.LicenseStatusDate)
			Response.Write("<td " & sColor & ">" & oLicense.LicenseExpDate)
			if oLicense.LicenseExpDate < date() + 120 and oLicense.LicenseExpDate > date() then
				Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicense.LicenseExpDate - date(), 0) & " Days</font>)</b>")
			end if
			Response.Write("</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
							
        end if
		
        oGroupLicenseRs.MoveNext
	
	loop

	%>
	</table>
	<%

else
	
	Response.Write("No Licenses found.")
	
end if 
set oGroupLicenseRs = nothing
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						
			<!-- Group Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Group Notes</td>
				</tr>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="modnote.asp?gid=<% = oGroup.GroupId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oGroup.GroupId
oNote.OwnerTypeId = 6

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
sColor = ""
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td></td>
		<td></td>
		<td width="40"><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="330"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
			Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteDate & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
set oNote = nothing
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>



<%	
set oGroup = nothing
set oRS = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
PrintShellFooter SHOW_MENUS										' From shell.asp
EndPage														' From htmlElements.asp
%>

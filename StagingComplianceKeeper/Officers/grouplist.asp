<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual="/includes/Group.Class.asp" ------------------------------------------------>
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Group Admin"



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oGroup

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

dim iVerified
dim iLicensed

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oGroup = new Group
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")

oGroup.CompanyId = session("UserCompanyId")

oGroup.Deleted = 0

if ucase(sAction) = "NEW" then
	'clear values
	Session.Contents.Remove("SearchGroupName") 
	Session.Contents.Remove("CurPage") 
else

	oGroup.GroupName = ScrubForSql(request("GroupName"))
	if (oGroup.GroupName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchGroupName")) <> "") then
			oGroup.GroupName = session("SearchGroupName")
		end if
	else
		session("SearchGroupName") = oGroup.GroupName
	end if

	'oAssociate.UserStatus = iShowDeleted
	'if (oAssociate.UserStatus = "") and (ucase(sAction) <> "SEARCH") then
	'	if (trim(session("SearchDeletedOfficers")) <> "") then
	'		oAssociate.UserStatus = session("SearchDeletedOfficers")
	'	end if
	'else
	'	session("SearchDeletedOfficers") = oAssociate.UserStatus
	'end if
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") and (ucase(sAction) <> "SEARCH") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if

end if


'Restrict the list if the user does not have company-wide access.
'if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
'	oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
'	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
'	oAssociate.SearchBranchIdList = GetUserBranchIdList()
'elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
'	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
'	oAssociate.SearchBranchIdList = GetUserBranchIdList()
'elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
'	oAssociate.SearchBranchIdList = GetUserBranchIdList()
'end if


	set oRs = oGroup.SearchGroups()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20
	
%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Groups -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Groups</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<form name="frm1" action="GroupList.asp" method="POST">
								<input type="hidden" name="action" value="SEARCH">
								<input type="hidden" name="page_number" value="1">
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Group Name: </td>
											<td><input type="text" name="GroupName" size="20" maxlength="50" value="<% = oGroup.GroupName %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="3"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
										</tr>
									</table>
								</td>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0">
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>

				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
												<table width="100%" border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td align="center">
															<a href="grouplist.asp?action=search">All</a> |
															<a href="grouplist.asp?letter=A">A</a> | 
															<a href="grouplist.asp?letter=B">B</a> |
															<a href="grouplist.asp?letter=C">C</a> |
															<a href="grouplist.asp?letter=D">D</a> |
															<a href="grouplist.asp?letter=E">E</a> |
															<a href="grouplist.asp?letter=F">F</a> |
															<a href="grouplist.asp?letter=G">G</a> |
															<a href="grouplist.asp?letter=H">H</a> |
															<a href="grouplist.asp?letter=I">I</a> |
															<a href="grouplist.asp?letter=J">J</a> |
															<a href="grouplist.asp?letter=K">K</a> |
															<a href="grouplist.asp?letter=L">L</a> |
															<a href="grouplist.asp?letter=M">M</a> |
															<a href="grouplist.asp?letter=N">N</a> |
															<a href="grouplist.asp?letter=O">O</a> |
															<a href="grouplist.asp?letter=P">P</a> |
															<a href="grouplist.asp?letter=Q">Q</a> |
															<a href="grouplist.asp?letter=R">R</a> |
															<a href="grouplist.asp?letter=S">S</a> |
															<a href="grouplist.asp?letter=T">T</a> |
															<a href="grouplist.asp?letter=U">U</a> |
															<a href="grouplist.asp?letter=V">V</a> |
															<a href="grouplist.asp?letter=W">W</a> |
															<a href="grouplist.asp?letter=X">X</a> |
															<a href="grouplist.asp?letter=Y">Y</a> |
															<a href="grouplist.asp?letter=Z">Z</a>
														</td>
													</tr>
												</table>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
													</tr>
												</table>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
									<CENTER>

									</center>
									<p>
<%

dim sLetter
dim iLetterPage
dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value

	if not (oRs.BOF and oRs.EOF) then
	
		set oGroup = nothing
		set oGroup = new Group
		oGroup.ConnectionString = application("sDataSourceName")
		oGroup.TpConnectionString = application("sTpDataSourceName")
		oGroup.VocalErrors = true
	
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		
		oRs.AbsolutePage = iCurPage
		
		'Initialize the row/col style values
		bCol = false
		sColor = ""
		
	
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
			if oGroup.LoadGroupById(oRs("GroupId")) <> 0 then
		
				if not bCol then
					%>
										<tr>
											<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="modgroup.asp?id=<% = oGroup.GroupId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Group Information" alt="Edit Group Information"></a>&nbsp;&nbsp;<% end if %><a href="groupdetail.asp?id=<% = oRs("GroupId") %>" title="View Group Information"><% = oGroup.GroupName %></a></td>
											<td width="4%"></td>
					<%
				else
					%>
											<td width="48%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="modgroup.asp?id=<% = oGroup.GroupId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Group Information" alt="Edit Group Information"></a>&nbsp;&nbsp;<% end if %><a href="groupdetail.asp?id=<% = oRs("GroupId") %>" title="View Group Information"><% = oGroup.GroupName %></a></td>
										</tr>
					<%
				end if


				'Set the col to the opposite value
				if bCol = false then
					bCol = true
				else
					bCol = false
					'Every two columns, set the row to the opposite color
					if sColor = "" then
						sColor = " bgcolor=""#ffffff"""
					else
						sColor = ""
					end if
				end if

			else
			
				Response.Write("NOPE")
				
			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

				
		'Finish off the table row if we ended on a left column.  Note that we check
		'for the positive bCol because the value is flipped at the end of the loop.
		if bCol then
			%>	
											<td width="48%" align="center"<% = sColor %>>&nbsp;</td>
										</tr>
			<%
		end if 	
		
		
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""grouplist.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""grouplist.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		%>
											</td>
										</tr>
		<%
	else
		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;There are currently no Groups that match your search criteria.</a></td>
										</tr>
		<%
	end if
%>
									</table>
								</td>
							</tr>
						</table>




								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oGroup = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" -->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iCandidateId
dim iCompanyId 
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
dim iAssociateBranchId
dim iLicenseId
dim sLegalName
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerId 'Holds the id of whatever type of owner the license has
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iCandidateId = ScrubForSQL(request("oid"))
iCompanyId = ScrubForSql(request("cid"))
iCompanyL2Id = ScrubForSql(request("c2id"))
iCompanyL3Id = ScrubForSql(request("c3id"))
iBranchId = ScrubForSql(request("bid"))
iLicenseId = ScrubForSql(request("lid"))


dim oLicense 'License Object
dim oCompany 'Company object
dim oCompanyL2 'CompanyL2 object
dim oCompanyL3 'CompanyL3 object
dim oBranch 'Branch object
dim oAssociate 'Officer object


'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'through each of the owner type possibilities and see if we were given an owner
'to whom we'll assign this license.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed License ID.")
		Response.End
	
	'If this is a company license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 3 and (oLicense.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
		AlertError("Unable to load the passed License ID.")
		Response.End
		
	'If this is a companyL2 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) and not CheckIsThisCompanyL2Viewer(oLicense.OwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) and not CheckIsThisCompanyL3Viewer(oLicense.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if				
	
	'If this is a branch license, verify that its branch belongs to the user's company.
	elseif oLicense.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oLicense.OwnerId) and not CheckIsThisBranchViewer(oLicense.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	'If this is an officer license, verify that the officer belongs to the user's company.
	elseif oLicense.OwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(oLicense.OwnerId) and not CheckIsThisAssociateViewer(oLicense.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	end if 
	
	iOwnerId = oLicense.OwnerId
	iOwnerTypeId = oLicense.OwnerTypeId
	sMode = "View"

else
	
	'We needed a license id to make this useful.
	AlertError("Failed to load a passed License ID.")
	Response.End

end if 



'Configure the administration submenu options
if iOwnerTypeId = 1 then
	bAdminSubmenu = true
	sAdminSubmenuType = "OFFICERS"
elseif iOwnerTypeId = 2 or iOwnerTypeId = 4 or iOwnerTypeId = 5 then
	bAdminSubmenu = true
	sAdminSubmenuType = "BRANCHES"
elseif iOwnerTypeId = 3 then
	bAdminSubmenu = true
	sAdminSubmenuType = "COMPANY"
end if


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Licensing -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<% if iOwnerTypeId = 1 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Officer Licensing: <% = oLicense.LookupAssociateNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 2 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Licensing: <% = oLicense.LookupBranchNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 4 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Licensing</td>
					<% elseif iOwnerTypeId = 5 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Licensing</td>
					<% else %>
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Company" align="absmiddle" vspace="10"> Corporate Licensing: <% = oLicense.LookupCompanyNameById(iOwnerId) %></td>
					<% end if %>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">License Detail</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="LicenseForm" action="modlicenseproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iOwnerId %>" name="OwnerId">
<input type="hidden" value="<% = iOwnerTypeId %>" name="OwnerTypeId">
<input type="hidden" value="<% = iLicenseId %>" name="LicenseId">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="newsTitle">License Number: </td>
		<td><% = oLicense.LicenseNum %></td>
	</tr>
	<tr>
		<td class="newsTitle">Holder's Legal Name: </td>
		<td><% if sLegalName <> "" then Response.Write(sLegalName) else Response.Write(oLicense.LegalName) end if %></td>
	</tr>
	<tr>
		<td class="newsTitle">License State: </td>
		<td><% = GetStateName(oLicense.LicenseStateId, 0) %></td>
	</tr>
	<tr>
		<td class="newsTitle">License Type: </td>
		<td><% = oLicense.LicenseType %></td>
	</tr>
	<tr>
		<td class="newsTitle">License Status: </td>
		<td><% = oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) %></td>
	</tr>	
    <tr>
		<td class="newsTitle">License Status Date: </td>
		<td><% = oLicense.LicenseStatusDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle">License Expiration: </td>
		<td><% = oLicense.LicenseExpDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle">Renewal Application Deadline: </td>
		<td><% = oLicense.LicenseAppDeadline %></td>
	</tr>
	<tr>
		<td class="newsTitle">Renewal Submission Date: </td>
		<td><% = oLicense.SubmissionDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle">Renewal Received Date: </td>
		<td><% = oLicense.ReceivedDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle">Issue Date: </td>
		<td><% = oLicense.IssueDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle">Submission Date: </td>
		<td><% = oLicense.SubDate %></td>		
	</tr>
	<tr>
		<td class="newsTitle" valign="top">Notes: </td>
		<td><% = oLicense.Notes %></td>
	</tr>	
	<tr>
		<td class="newsTitle" valign="top">File Attachment: </td>
		<td>
		<%
		dim oAssociateDoc
		dim oDocRs
		set oAssociateDoc = new AssociateDoc
		oAssociateDoc.ConnectionString = application("sDataSourceName")
		oAssociateDoc.VocalErrors = application("bVocalErrors")
		oAssociateDoc.CompanyId = session("UserCompanyId")
		oAssociateDoc.OwnerId = oLicense.LicenseId
		oAssociateDoc.OwnerTypeId = 7
		set oDocRs = oAssociateDoc.SearchDocs
										
		if not oDocRs.EOF then
			do while not oDocRs.EOF 
				if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
					%>
					<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
					<%
				end if
				oDocRs.MoveNext
			loop
			%><br><%
		end if
		%>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
'set oPreference = nothing
set oLicense = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
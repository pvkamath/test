<%
'option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/StateCandidate.Class.asp" ----------------->
<!-- #include virtual = "/includes/StatePreferences.Class.asp" --------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"



'Verify that we have already set up a state ID.  
if session("StateId") = "" then
	
	Response.Write("State Reference Required.")
	Response.End

end if 

'Verify that the user has logged in.
CheckIsLoggedIn()

'Verify directory security
dim bHasDirectoryAccess
dim sPagePath
dim iLocation

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(session("User_ID"), sPagePath) 'security.asp


dim oPreference
set oPreference = new StatePreference
oPreference.ConnectionString = application("sDataSourceName")
oPreference.LoadPreferencesByStateId(session("StateId"))


'Are we changing the administration bar options?
'if trim(request("AdminOpt")) = "1" and not session("bAdminSidebar") then
'	session("bAdminSidebar") = true
'elseif trim(request("AdminOpt")) = "0" and session("bAdminSidebar") then
'	session("bAdminSidebar") = false
'end if 
	

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


dim iRosterId
iRosterId = ScrubForSQL(request("RosterId"))

dim iCandidateId
iCandidateId = ScrubForSQL(request("CandidateId"))

dim oStateCandidate
dim oRs
dim sMode 'Text string describing the current action (add/edit)

set oStateCandidate = new StateCandidate
oStateCandidate.ConnectionString = application("sDataSourceName")

if iCandidateId <> "" then

	if oStateCandidate.LoadById(iCandidateId) = 0 then
	
		'The load was unsuccessful.  Die.
		Response.Write("Failed to load the passed Candidate ID: " & iCandidateId)
		Response.End
		
	end if
	
	sMode = "Edit"
	
else
	
	sMode = "Add"
	'Response.Write("Failed to load the passed Candidate.<br>" & vbCrLf)
	'Response.End
	
end if

'Verify that the user has access to this roster.
if session("Access_Level") > 2 then
	
	Response.Write("Failed to load the passed Candidate.<br>" & vbCrLf)
	Response.End
	
end if 

%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		return true;
	}
</script>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
		
			<!-- Loan Officer Form -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Loan Officers" align="absmiddle" vspace="10"> Edit Loan Officer Record</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
				
<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Loan Officer</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="CandidateForm" action="modcandidateproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iRosterId %>" name="RosterId">
<input type="hidden" value="<% = iCandidateId %>" name="CandidateId">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td><b>First Name: </b></td>
		<td>
			<input type="text" name="FirstName" value="<% = oStateCandidate.FirstName %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td><b>Last Name: </b></td>
		<td>
			<input type="text" name="LastName" value="<% = oStateCandidate.LastName %>" size="30" maxlength="50">
		</td>
	</tr>	
	<tr>
		<td><b>SSN: </b></td>
		<td>
			<input type="text" size="12" maxlength="11" name="SSN" value="<% = oStateCandidate.SSN %>">		
		</td>
	</tr>

	<tr>
		<td><b>Address: </b></td>
		<td>
			<input type="text" size="30" maxlength="100" name="Address" value="<% = oStateCandidate.Address %>">		
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="text" size="30" maxlength="100" name="Address2" value="<% = oStateCandidate.Address2 %>">		
		</td>
	</tr>
	<tr>
		<td><b>City: </b></td>
		<td>
			<input type="text" size="30" maxlength="50" name="AddressCity" value="<% = oStateCandidate.AddressCity %>">		
		</td>
	</tr>
	<tr>
		<td><b>State: </b></td>
		<td>
			<% call DisplayStatesDropDown(oStateCandidate.AddressStateId, 1, "AddressStateId") %>
		</td>
	</tr>
	<tr>
		<td><b>Zipcode: </b></td>
		<td>
			<input type="text" size="6" maxlength="5" name="AddressZipcode" value="<% = oStateCandidate.AddressZipcode %>">-<input type="text" size="5" maxlength="4" name="AddressZipcode2" value="<% = oStateCandidate.AddressZipcode2 %>">	
		</td>
	</tr>
	<tr>
		<td><b>Phone: </b></td>
		<td>
			<input type="text" size="30" maxlength="50" name="Phone" value="<% = oStateCandidate.Phone %>">		
		</td>
	</tr>
	<tr>
		<td><b>Fax: </b></td>
		<td>
			<input type="text" size="30" maxlength="50" name="Fax" value="<% = oStateCandidate.Fax %>">		
		</td>
	</tr>
	<tr>
		<td><b>Email: </b></td>
		<td>
			<input type="text" size="30" maxlength="50" name="Email" value="<% = oStateCandidate.Email %>">		
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
		</td>
		<td valign="top">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td><b>Received Resume: </b></td>
		<td>
			<input type="checkbox" name="ReceivedResume" value="1"<% if oStateCandidate.ReceivedResume then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td><b>Received Bond: </b></td>
		<td>
			<input type="checkbox" name="ReceivedBond" value="1"<% if oStateCandidate.ReceivedBond then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td><b>Received Financial Statement: </b></td>
		<td>
			<input type="checkbox" name="ReceivedFinancials" value="1"<% if oStateCandidate.ReceivedFinancials then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td><b>Received Credit Report: </b></td>
		<td>
			<input type="checkbox" name="ReceivedCreditReport" value="1"<% if oStateCandidate.ReceivedCreditReport then %> CHECKED<% end if %>>
		</td>
	</tr>
</table>
		</td>
	</tr>
</table>
</form>

					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			<!-- space-->
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>


<%
set oRs = nothing
set oPreference = nothing
set oStateCandidate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
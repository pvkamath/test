<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/StateCandidate.Class.asp" ----------------->
<!-- #include virtual = "/includes/StatePreferences.Class.asp" --------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"



'Verify that we have already set up a state ID.  
if session("StateId") = "" then
	
	Response.Write("State Reference Required.")
	Response.End

end if 

'Verify that the user has logged in.
CheckIsLoggedIn()

'Verify directory security
dim bHasDirectoryAccess
dim sPagePath
dim iLocation

sPagePath = Request.ServerVariables("SCRIPT_NAME")
iLocation = inStrRev(sPagePath,"/")
sPagePath = left(sPagePath,iLocation -1)

bHasDirectoryAccess = checkDirectoryAccess(session("User_ID"), sPagePath) 'security.asp


dim oPreference
set oPreference = new StatePreference
oPreference.ConnectionString = application("sDataSourceName")
oPreference.LoadPreferencesByStateId(session("StateId"))


'Are we changing the administration bar options?
'if trim(request("AdminOpt")) = "1" and not session("bAdminSidebar") then
'	session("bAdminSidebar") = true
'elseif trim(request("AdminOpt")) = "0" and session("bAdminSidebar") then
'	session("bAdminSidebar") = false
'end if 
	

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


dim iRosterId
iRosterId = ScrubForSQL(request("RosterId"))

dim oStateCandidate
dim iCandidateId

iCandidateId = ScrubForSQL(request("CandidateId"))

set oStateCandidate = new StateCandidate
oStateCandidate.ConnectionString = application("sDataSourceName")
oStateCandidate.VocalErrors = application("bVocalErrors")


if iCandidateId <> "" then

	if oStateCandidate.LoadById(iCandidateId) = 0 then
	
		'The load was unsuccessful.  Stop.
		Response.Write("Failed to load the passed Candidate ID: " & iCandidateId & "<br>")
		Response.End
		
	else
	
		'Verify that the user has access to this roster
		if session("Access_Level") > 2 then
		
			Response.Write("Failed to load the passed Candidate.<br>" & vbCrLf)
			Response.End
			
		end if
		
	end if
	
elseif session("Access_Level") > 2 then

	'We need more access.
	Response.Write("Failed to load the Candidate.<br>")
	Response.End	
			
end if


'Get the rest of the form data.
oStateCandidate.FirstName = ScrubForSQL(request("FirstName"))
oStateCandidate.LastName = ScrubForSQL(request("LastName"))
oStateCandidate.SSN = ScrubForSQL(request("SSN"))
oStateCandidate.Address = ScrubForSQL(request("Address"))
oStateCandidate.Address2 = ScrubForSql(request("Address2"))
oStateCandidate.AddressCity = ScrubForSql(request("AddressCity"))
oStateCandidate.AddressStateId = ScrubForSql(request("AddressStateId"))
oStateCandidate.AddressZipcode = ScrubForSql(request("AddressZipcode"))
oStateCandidate.AddressZipcode2 = ScrubForSql(request("AddressZipcode2"))
oStateCandidate.Phone = ScrubForSql(request("Phone"))
oStateCandidate.Fax = ScrubForSql(request("Fax"))
oStateCandidate.Email = ScrubForSql(request("Email"))
oStateCandidate.ReceivedResume = ScrubForSql(request("ReceivedResume"))
oStateCandidate.ReceivedBond = ScrubForSql(request("ReceivedBond"))
oStateCandidate.ReceivedFinancials = ScrubForSql(request("ReceivedFinancials"))
oStateCandidate.ReceivedCreditReport = ScrubForSql(request("ReceivedCreditReport"))


'Save the Candidate
if oStateCandidate.SaveCandidate = 0 then

	Response.Write("Error saving Candidate.<br>" & vbCrLf)
	Response.End
	
end if 


'Redirect to the add-user page.
Response.Redirect("officerlist.asp")

set oPreference = nothing
set oStateCandidate = nothing
%>
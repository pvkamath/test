<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/AssociateDoc.Class.asp" ------------------->
<!-- #include virtual = "/includes/License.Class.asp" --->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Documents"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
dim iOwnerId
dim iOwnerTypeId
dim iDocId
dim sMode
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))
iOwnerId = ScrubForSql(request("oid"))
iOwnerTypeId = ScrubForSql(request("otid"))
iDocId = ScrubForSql(request("DocId"))



dim oAssociateDoc
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")

if iDocId <> "" then
	if oAssociateDoc.LoadDocById(iDocId) = 0 then
	
		AlertError("Failed to load the Document information.")
		Response.End
		
	'If this is a company document, check that it belongs to the user's company
	elseif oAssociateDoc.OwnerTypeId = 3 and (oAssociateDoc.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then

		AlertError("Unable to load the passed Document ID.")
		Response.End
	
	'If this is a companyL2 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 4 then
		
		if not CheckIsThisCompanyL2Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyl3 doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if
	
	'If this is a branch doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if
		
	'If this is an officer doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 1 then
		
		if not CheckIsThisAssociateAdmin(oAssociateDoc.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	'If this is a state doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 6 then
	
		if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
			AlertError("Unable to load the passed State ID.")
			Response.End
		end if
		
	'If this is a license doc, check that the user has access.
	elseif oAssociateDoc.OwnerTypeId = 7 then
	
		dim oLicense
		set oLicense = new License
		oLicense.ConnectionString = application("sDataSourceName")
		oLicense.VocalErrors = application("bVocalErrors")
		if oLicense.LoadLicenseById(oAssociateDoc.OwnerId) <> 0 then
			if oLicense.OwnerTypeId = 1 then
				if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 2 then
				if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 3 then
				if (not oAssociateDoc.CompanyId = session("UserCompanyId")) or (not CheckIsAdmin()) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 4 then
				if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			elseif oLicense.OwnerTypeId = 5 then
				if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
					AlertError("Unable to load the passed License ID.")
					Response.End					
				end if
			end if
		else
			AlertError("Unable to load the passed License ID.")
			Response.End
		end if
	
	end if 
	
	iOwnerId = oAssociateDoc.OwnerId
	iOwnerTypeId = oAssociateDoc.OwnerTypeId
	sMode = "Edit"
	
elseif iOwnerTypeId = "1" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisAssociateAdmin(iOwnerId) then
		AlertError("Failed to load the passed Officer ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "3" then
	
	sMode = "Add"
	
	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed Company ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "4" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL2Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "5" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL3Admin(iOwnerId) then
		AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "2" then
	
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisBranchAdmin(iOwnerId) then
		AlertError("Failed to load the passed Branch ID.")
		Response.End
	end if
	
elseif iOwnerTypeId = "6" then

	sMode = "Add"
	
	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed State ID.")
		Response.End
	end if
	
'If this is a license doc, check that the user has access.
elseif iOwnerTypeId = 7 then
	
	sMode = "Add"
	
	set oLicense = new License
	oLicense.ConnectionString = application("sDataSourceName")
	oLicense.VocalErrors = application("bVocalErrors")
	if oLicense.LoadLicenseById(iOwnerId) <> 0 then
		if oLicense.OwnerTypeId = 1 then
			if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 2 then
			if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 3 then
			if not CheckIsAdmin() then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 4 then
			if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		elseif oLicense.OwnerTypeId = 5 then
			if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
				AlertError("Unable to load the passed License ID.")
				Response.End					
			end if
		end if
	else
		AlertError("Unable to load the passed License ID.")
		Response.End
	end if

else

	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End
		
end if 



'Configure the administration submenu options
if iOwnerTypeId = 1 then
	bAdminSubmenu = true
	sAdminSubmenuType = "OFFICERS"
elseif iOwnerTypeId = 2 or iOwnerTypeId = 4 or iOwnerTypeId = 5 then
	bAdminSubmenu = true
	sAdminSubmenuType = "BRANCHES"
elseif iOwnerTypeId = 3 then
	bAdminSubmenu = true
	sAdminSubmenuType = "COMPANY"
end if


%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (
			!checkString(FORM.DocName, "Document Name")
			|| !checkString(FORM.Document, "File Name")
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<% if iOwnerTypeId = 1 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Officer Documents: <% = oAssociateDoc.LookupAssociateNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 2 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Documents: <% = oAssociateDoc.LookupBranchNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 4 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Documents</td>
					<% elseif iOwnerTypeId = 5 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Documents</td>
					<% elseif iOwnerTypeId = 6 then %> 
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="States" align="absmiddle" vspace="10"> State Documents: <% = oAssociateDoc.LookupState(iOwnerId, 0) %></td>
					<% else %>
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Company" align="absmiddle" vspace="10"> Corporate Documents: <% = oAssociateDoc.LookupCompanyNameById(iOwnerId) %></td>
					<% end if %>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> an Officer Document</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="AssociateDocForm" action="moddocumentproc.asp" method="POST" enctype="multipart/form-data" onSubmit="return Validate(this);">
<input type="hidden" name="oid" value="<% = iOwnerId %>">
<input type="hidden" name="otid" value="<% = iOwnerTypeId %>">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> Document Name: </td>
		<td align="left" valign="top">
			<input type="text" name="DocName" value="<% = oAssociateDoc.DocName %>">
		</td>
	</tr>
	<tr>
		<td valign="top" class="formreqheading"><span class="formreqstar">*</span> Document File: </td>
		<td align="left" valign="top">
			<input type="file" size="30" name="Document" value=""><p>
		</td>
	</tr>			
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%
set oAssociateDoc = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Employee Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim i
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
dim iEmployeeId
iEmployeeId = ScrubForSql(request("OfficerId"))

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	AlertError("Failed to load the passed Company ID.")
	Response.end
		
end if 

dim sMode
if iEmployeeId <> "" then 
	if oAssociate.LoadAssociateById(iEmployeeId) = 0 then

		'The load was unsuccessful.  End.
		AlertError("Failed to load the passed Employee ID.")
		Response.End
	
	end if
	

	if not CheckIsThisAssociateAdmin(oAssociate.UserId) then
	
		AlertError("Unable to load the passed Employee ID.")
		Response.End
	
	end if
	
	sMode = "Edit"

else

	sMode = "Add"

end if

'Assign the properties based on our passed parameters
oAssociate.CompanyId = ScrubForSql(session("UserCompanyId"))
oAssociate.BranchId = ""
oAssociate.CompanyL2Id = ""
oAssociate.CompanyL3Id = ""
oAssociate.UserStatus = 1
oAssociate.FirstName = ScrubForSql(request("FirstName"))
oAssociate.MiddleName = ScrubForSql(request("MiddleName"))
oAssociate.LastName = ScrubForSql(request("LastName"))
oAssociate.AssociateTypeID = ScrubForSql(request("AssociateTypeID"))
oAssociate.Email = ScrubForSql(request("Email"))
oAssociate.NMLSNumber = ScrubForSql(request("NMLSNumber"))
oAssociate.TerminationDate = ScrubForSql(request("TerminationDate"))
oAssociate.EmployeeID = ScrubForSql(request("EmployeeID"))


if ScrubForSql(request("Inactive")) = "1" then
	oAssociate.Inactive = true
else
	oAssociate.Inactive = false
end if

if ScrubForSql(request("Verified")) = "1" then
	oAssociate.Verified = true
else
	oAssociate.Verified = false
end if

'If this is a new associate we need to save it before it will have a unique ID.
dim iNewEmployeeId
if oAssociate.UserId = "" then

	iNewEmployeeId = oAssociate.SaveAssociate

	if iNewEmployeeId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Employee.  Stop.
		if not oAssociate.VerifyUniqueEmail(oAssociate.Email) then
			AlertError("The email address provided for this employee is already in use in " & _
				"the system.  Saving the employee requires a unique email " & _
				"address.")
			Response.End
		else
			AlertError("Failed to save the new Employee.")
			Response.End
		end if

	end if
	
else

	iNewEmployeeId = oAssociate.SaveAssociate

	if iNewEmployeeId = 0 then

		'The save failed.
		if not oAssociate.VerifyUniqueNMLS(oAssociate.NMLSNumber) then
			AlertError("The NMLS Number provided for this employee is already in use in " & _
				"the system.  Saving the employee requires a NMLS Number " & _
				"address.")
			Response.End
		else
			AlertError("Failed to save the existing Employee.")
			Response.End
		end if

	elseif Clng(iNewEmployeeId) <> Clng(iEmployeeId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		AlertError("Unexpected results while saving existing Employee.")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Employee -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Employees</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> an Employee</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if sMode = "Add" then
%>
<b>The new Employee "<% = oAssociate.FirstName & " " & oAssociate.LastName %>" was successfully created.</b>
<%
else
%>
<b>The Employee "<% = oAssociate.FirstName & " " & oAssociate.LastName %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="EmployeeList.asp">Return to Employee Listing</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oAssociate = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
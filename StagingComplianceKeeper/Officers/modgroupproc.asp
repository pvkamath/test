<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Group.Class.asp" -------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Group Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim iGroupId 
iGroupId = ScrubForSql(request("GroupId"))

dim oGroup
set oGroup = new Group
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")

dim sMode
if iGroupId <> "" then 
	if oGroup.LoadGroupById(iGroupId) = 0 then

		'The load was unsuccessful.  End.
		AlertError("Failed to load the passed Group ID.")
		Response.End
	
	end if

	sMode = "Edit"

else

    oGroup.Deleted = 0
	sMode = "Add"

end if

'Assign the properties based on our passed parameters
oGroup.CompanyId = ScrubForSql(session("UserCompanyId"))
oGroup.GroupName = ScrubForSql(request("GroupName"))
oGroup.AssociateID = ScrubForSql(request("AssociateID"))

'If this is a new group we need to save it before it will have a unique ID.
dim iNewGroupId
if oGroup.GroupId = "" then

	iNewGroupId = oGroup.SaveGroup

	if iNewGroupId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the group.  Stop.
		AlertError("Failed to save the new Group.")
		Response.End
	end if
	
else

	iNewGroupId = oGroup.SaveGroup

	if iNewGroupId = 0 then

		'The save failed.
		AlertError("Failed to save the existing Group.")
		Response.End

	elseif Clng(iNewGroupId) <> Clng(iGroupId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		AlertError("Unexpected results while saving existing Group.")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Group -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Groups" align="absmiddle" vspace="10"> Groups</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> a Group</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if sMode = "Add" then
%>
<b>The new Group "<% = oGroup.GroupName %>" was successfully created.</b>
<%
else
%>
<b>The Group "<% = oGroup.GroupName %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="GroupList.asp">Return to Group Listing</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oGroup = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
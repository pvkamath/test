<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" -->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iCandidateId
dim iCompanyId 
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
dim iAssociateBranchId
dim iLicenseId
dim iSearchLicenseStateID 
dim sLegalName
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerId 'Holds the id of whatever type of owner the license has
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iCandidateId = ScrubForSQL(request("oid"))
iCompanyId = ScrubForSql(request("cid"))
iCompanyL2Id = ScrubForSql(request("c2id"))
iCompanyL3Id = ScrubForSql(request("c3id"))
iBranchId = ScrubForSql(request("bid"))
iLicenseId = ScrubForSql(request("lid"))
iSearchLicenseStateID = ScrubForSql(request("ls"))

dim oLicense 'License Object
dim oCompany 'Company object
dim oCompanyL2 'CompanyL2 object
dim oCompanyL3 'CompanyL3 object
dim oBranch 'Branch object
dim oAssociate 'Officer object

dim iAssociateTypeID
dim sAssociateTerm
iAssociateTypeID = ScrubForSql(request("AssociateTypeID"))
sAssociateTerm = ScrubForSql(request("AssociateTerm"))
if sAssociateTerm = "" then
    sAssociateTerm = "Officer"
end if


'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'through each of the owner type possibilities and see if we were given an owner
'to whom we'll assign this license.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed License ID.")
		Response.End
	
	'If this is a company license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 3 and (oLicense.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
		AlertError("Unable to load the passed License ID.")
		Response.End
		
	'If this is a companyL2 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if				
	
	'If this is a branch license, verify that its branch belongs to the user's company.
	elseif oLicense.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	'If this is an officer license, verify that the officer belongs to the user's company.
	elseif oLicense.OwnerTypeId = 1 then

		if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
			AlertError("Unable to load the passed " & sAssociateTerm & " ID.")
			Response.End
		end if
		
	end if 	
	iOwnerId = oLicense.OwnerId
	iOwnerTypeId = oLicense.OwnerTypeId
	sMode = "Edit"

elseif iCandidateId <> "" then 

	iOwnerId = iCandidateId	
	iOwnerTypeId = 1
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisAssociateAdmin(iCandidateId) then
		AlertError("Failed to load the passed " & sAssociateTerm & " ID.")
		Response.End
	end if
	
	set oAssociate = new Associate
	oAssociate.ConnectionString = application("sDataSourceName")
	if oAssociate.LoadAssociateById(iOwnerId) <> 0 then
		sLegalName = oAssociate.FullName
	end if
	set oAssociate = nothing
	
elseif iCompanyId <> "" then
	
	iOwnerId = iCompanyId
	iOwnerTypeId = 3
	sMode = "Add"

	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed Company ID.")
		Response.End
	end if
	
	set oCompany = new Company
	oCompany.ConnectionString = application("sDataSourceName")
	if oCompany.LoadCompanyById(iOwnerId) <> 0 then
		sLegalName = oCompany.LegalName
	end if
	set oCompany = nothing
	
elseif iCompanyL2Id <> "" then
	
	iOwnerId = iCompanyL2Id
	iOwnerTypeId = 4
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL2Admin(iCompanyL2Id) then
		AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if
	
	set oCompanyL2 = new CompanyL2
	oCompanyL2.ConnectionString = application("sDataSourceName")
	if oCompanyL2.LoadCompanyById(iOwnerId) <> 0 then
		sLegalName = oCompanyL2.LegalName
	end if
	set oCompanyL2 = nothing
	
elseif iCompanyL3Id <> "" then

	iOwnerId = iCompanyL3Id
	iOwnerTypeId = 5
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL3Admin(iCompanyL3Id) then
		AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if
	
	set oCompanyL3 = new CompanyL3
	oCompanyL3.ConnectionString = application("sDataSourceName")
	if oCompanyL3.LoadCompanyById(iOwnerId) <> 0 then
		sLegalName = oCompanyL3.LegalName
	end if
	set oCompanyL3 = nothing
	
elseif iBranchId <> "" then

	iOwnerId = iBranchId
	iOwnerTypeId = 2
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisBranchAdmin(iBranchId) then
		AlertError("Failed to load the passed Branch ID.")
		Response.End
	end if
	
	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	if oBranch.LoadBranchById(iOwnerId) <> 0 then
		sLegalName = oBranch.LegalName
	end if
	set oBranch = nothing
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'Configure the administration submenu options
if iOwnerTypeId = 1 then
	bAdminSubmenu = true
	sAdminSubmenuType = "OFFICERS"
elseif iOwnerTypeId = 2 or iOwnerTypeId = 4 or iOwnerTypeId = 5 then
	bAdminSubmenu = true
	sAdminSubmenuType = "BRANCHES"
elseif iOwnerTypeId = 3 then
	bAdminSubmenu = true
	sAdminSubmenuType = "COMPANY"
end if


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------



    
%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.LicenseNum, "License Number")
			|| !checkIsDate(FORM.LicenseExpDate, "License Expiration Date must be in the proper format.", 1)
		   )
			return false;

		return true;
	}
	
	function ConfirmDocumentDelete(p_iDocId)
	{
		if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
			window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}

    var _isDirty = false;
    $(document).ready(function () {
        $(":input").change(function () {
            _isDirty = true;
        });
    });

	function ConfirmDocumentNavigate(p_sNavigateURL) {
	    if (!_isDirty || (confirm("You are about to navigate away from this page and have unsaved changes. If you continue, your changes will NOT be saved. Do you wish to continue?"))) {
	        window.location.href = '<% = application("URL") %>' + p_sNavigateURL;
	    }
	}
    


</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Licensing -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<% if iOwnerTypeId = 1 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <%=sAssociateTerm%> Licensing: <% = oLicense.LookupAssociateNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 2 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Branch Licensing: <% = oLicense.LookupBranchNameById(iOwnerId) %></td>
					<% elseif iOwnerTypeId = 4 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL2Name") %> Licensing</td>
					<% elseif iOwnerTypeId = 5 then %>
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> <% = session("CompanyL3Name") %> Licensing</td>
					<% else %>
					<td class="sectiontitle"><img src="/media/images/navIcon-brokers.gif" width="17" alt="Company" align="absmiddle" vspace="10"> Corporate Licensing: <% = oLicense.LookupCompanyNameById(iOwnerId) %></td>
					<% end if %>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a License</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="LicenseForm" action="modlicenseproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iOwnerId %>" name="OwnerId">
<input type="hidden" value="<% = iOwnerTypeId %>" name="OwnerTypeId">
<input type="hidden" value="<% = iLicenseId %>" name="LicenseId">
<input type="hidden" value="<% = iSearchLicenseStateID %>" name="SearchLicenseStateID">
<input type="hidden" value="<% = iAssociateTypeID %>" name="AssociateTypeID" />

<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span> License Number: </td>
		<td>
			<input type="text" name="LicenseNum" value="<% = oLicense.LicenseNum %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Holder's Legal Name: </td>
		<td>
			<input type="text" name="LegalName" value="<% if sLegalName <> "" then Response.Write(sLegalName) else Response.Write(oLicense.LegalName) end if %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formreqheading"><span class="formreqstar">*</span>License State: </td>
		<td>
			<% call DisplayStatesDropDown(oLicense.LicenseStateId, 1, "LicenseStateId") 'functions.asp %>
		</td>
	</tr>
    <%if iOwnerTypeID=2 then %>
    <tr>
        <td class="formheading">Agency Type: </td>
        <td>
            <% call DisplayAgencyTypeDropDown(oLicense.AgencyTypeID, 1, session("UserCompanyId")) %>
        </td>
    </tr>
    <tr>
        <td class="formheading">Agency Website: </td>
        <td>
            <input type="text" name="AgencyWebsite" value="<%= oLicense.AgencyWebsite %>" />
        </td>
    </tr>
    <%end if %>
	<tr>
		<td class="formheading">License Type: </td>
		<td>
			<input type="text" name="LicenseType" value="<% = oLicense.LicenseType %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">License Status: </td>
		<td>
			<% call DisplayLicenseStatusDropDown(oLicense.LicenseStatusId, 2, "LicenseStatus") %>
		</td>
	</tr>	

    <tr>
		<td class="formheading">License Status Date: </td>
		<td>
			<input type="text" name="LicenseStatusDate" value="<% = oLicense.LicenseStatusDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.LicenseStatusDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.LicenseStatusDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>


	<tr>
		<td class="formheading">License Expiration: </td>
		<td>
			<input type="text" name="LicenseExpDate" value="<% = oLicense.LicenseExpDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.LicenseExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.LicenseExpDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheader">Renewal Application Deadline: </td>
		<td>
			<input type="text" name="LicenseAppDeadline" value="<% = oLicense.LicenseAppDeadline %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.LicenseAppDeadline');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.LicenseAppDeadline');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td class="formheading">Renewal Submission Date: </td>
		<td>
			<input type="text" name="SubmissionDate" value="<% = oLicense.SubmissionDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.SubmissionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.SubmissionDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheading">Renewal Received Date: </td>
		<td>
			<input type="text" name="ReceivedDate" value="<% = oLicense.ReceivedDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.ReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.ReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheading">Issue Date: </td>
		<td>
			<input type="text" name="IssueDate" value="<% = oLicense.IssueDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.IssueDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.IssueDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheading">Submission Date: </td>
		<td>
			<input type="text" name="SubDate" value="<% = oLicense.SubDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.SubDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.SubDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheading">Cancellation Date: </td>
		<td>
			<input type="text" name="CancellationDate" value="<% = oLicense.CancellationDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('LicenseForm.CancellationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('LicenseForm.CancellationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td class="formheading" valign="top">Notes: </td>
		<td>
			<textarea name="Notes" cols="50" rows="5"><% = oLicense.Notes %></textarea>
		</td>
	</tr>	
	<% if sMode = "Edit" then %>
	<tr>
		<td class="formheading" valign="top">File Attachment: </td>
		<td>
		<%
		dim oAssociateDoc
		dim oDocRs
		set oAssociateDoc = new AssociateDoc
		oAssociateDoc.ConnectionString = application("sDataSourceName")
		oAssociateDoc.VocalErrors = application("bVocalErrors")
		oAssociateDoc.CompanyId = session("UserCompanyId")
		oAssociateDoc.OwnerId = oLicense.LicenseId
		oAssociateDoc.OwnerTypeId = 7
		set oDocRs = oAssociateDoc.SearchDocs
										
		if not oDocRs.EOF then
			do while not oDocRs.EOF
				if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
				%>
				<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document"></a>&nbsp;&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
				<%
				end if
				oDocRs.MoveNext
			loop
			%><br><%
		end if
				%>
				<a href="/officers/moddocument.asp?otid=7&oid=<% = oLicense.LicenseId %>"></a><a href="javascript:ConfirmDocumentNavigate('/officers/moddocument.asp?otid=7&oid=<% = oLicense.LicenseId %>')"<img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
				<%
		%>
		</td>
	<tr>
	<% end if %>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
'set oPreference = nothing
set oLicense = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
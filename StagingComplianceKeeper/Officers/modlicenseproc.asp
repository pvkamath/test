<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/License.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" --------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ----------------------------------------------------->
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for license type/id
dim iLicenseId 'ID of the licenese in question
dim iSearchLicenseStateID 'ID of the license state from the detail page
dim iOwnerId 'ID of whatever owner type owns the license
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerBranchId 'Is there a specific branch to which this license belongs?
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iOwnerId = ScrubForSql(request("OwnerId"))
iOwnerTypeId = ScrubForSql(request("OwnerTypeId"))
iLicenseId = ScrubForSql(request("LicenseId"))
iSearchLicenseStateID = ScrubForSql(request("SearchLicenseStateID"))

dim oLicense 'License Object

'Initialize the license object
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'Declare the Associate object and the Company object.  We'll use these to
'verify permissions for new licenses.
dim oAssociate
dim oCompany
dim oCompanyL2
dim oCompanyL3

'If we were passed a License ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iLicenseId <> "" then

	if oLicense.LoadLicenseById(iLicenseId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed License ID.")
		Response.End

	'If this is a company license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 3 and (oLicense.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
		AlertError("Unable to load the passed License ID.")
		Response.End
		
	'If this is a companyL2 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license, verify that it belongs to the user's company.
	elseif oLicense.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if
	
	'If this is a branch license, verify that it's branch belongs to the user's company.
	elseif oLicense.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oLicense.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End	
		end if 
		
	'If this is an officer license, verify that the officer belongs to the user's company.
	elseif oLicense.OwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(oLicense.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
	
	end if 
	
	sMode = "Edit"
	
elseif iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	oLicense.OwnerId = iOwnerId
	oLicense.OwnerTypeId = iOwnerTypeId
	
	'If this is an Officer license...
	if iOwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(iOwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	'If this is a Branch license...
	elseif iOwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	'If this is a Company license...
	elseif iOwnerTypeId = 3 then
	
		if (clng(iOwnerId) <> session("UserCompanyId") or not (CheckIsAdmin())) then
			AlertError("Unable to load the passed Company ID.(" & iOwnerId & "," & session("UserCompanyId") & "," & CheckIsAdmin() & ")")
			Response.End
		end if 
		
	'If this is a companyL2 license
	elseif iOwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license
	elseif iOwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
		
	else
	
		AlertError("Invalid Owner Type specified.")
		Response.End
	
	end if 

	sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'We have a valid license.  Go ahead and set the properties based on our passed 
'form data.
if sMode = "New" then
	oLicense.OwnerId = iOwnerId
	oLicense.OwnerTypeId = iOwnerTypeId
	'oLicense.OwnerCompanyId = session("UserCompanyId")
end if
if safecstr(oLicense.LicenseExpDate) <> ScrubForSql(request("LicenseExpDate")) or _
    safecstr(oLicense.LicenseStatusDate) <> ScrubForSql(request("LicenseStatusDate")) or _
	safecstr(oLicense.LicenseAppDeadline) <> ScrubForSql(request("LicenseAppDeadline")) or _
	safecstr(oLicense.LicenseStatusId) <> ScrubForSql(request("LicenseStatus")) then
	oLicense.Triggered30 = 0
	oLicense.Triggered60 = 0
	oLicense.Triggered90 = 0
end if
oLicense.OwnerCompanyId = session("UserCompanyId")
oLicense.OwnerBranchId = iOwnerBranchId
oLicense.LicenseNum = ScrubForSql(request("LicenseNum"))

oLicense.LicenseStatusDate = ScrubForSql(request("LicenseStatusDate"))
oLicense.LicenseExpDate = ScrubForSql(request("LicenseExpDate"))

oLicense.LicenseAppDeadline = ScrubForSql(request("LicenseAppDeadline"))
oLicense.LicenseStatusId = ScrubForSql(request("LicenseStatus"))
oLicense.LicenseType = ScrubForSql(request("LicenseType"))
oLicense.LicenseStateId = ScrubForSql(request("LicenseStateId"))
oLicense.LegalName = ScrubForSql(request("LegalName"))
oLicense.ReceivedDate = ScrubForSql(request("ReceivedDate"))
oLicense.SubmissionDate = ScrubForSql(request("SubmissionDate"))
oLicense.SubDate = ScrubForSql(request("SubDate"))
oLicense.CancellationDate = ScrubForSql(request("CancellationDate"))
oLicense.IssueDate = ScrubForSql(request("IssueDate"))
oLicense.Notes = ScrubForSql(request("Notes"))

if iOwnerTypeID = 2 then
    oLicense.AgencyTypeID = ScrubForSql(request("AgencyTypeID"))
    oLicense.AgencyWebsite = ScrubForSql(request("AgencyWebsite"))
end if

'Save the license

if oLicense.SaveLicense("","") = 0 then

	AlertError("Failed to save the license.  Please try again.")
	Response.End
	
end if 


'Clean up
set oAssociate = nothing
set oCompany = nothing


'Redirect back to the appropriate page.
if oLicense.OwnerTypeId = 1 then
    if ScrubForSql(request("AssociateTypeID")) = 1 then
	    Response.Redirect("/officers/officerdetail.asp?id=" & oLicense.OwnerId)
    else
        Response.Redirect("/officers/employeedetail.asp?id=" & oLicense.OwnerId)
    end if

elseif oLicense.OwnerTypeId = 2 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oLicense.OwnerId & "&ls=" & iSearchLicenseStateID)

elseif oLicense.OwnerTypeId = 4 then

	Response.Redirect("/branches/companyl2detail.asp?id=" & oLicense.OwnerId & "&ls=" & iSearchLicenseStateID)
	
elseif oLicense.OwnerTypeId = 5 then

	Response.Redirect("/branches/companyl3detail.asp?id=" & oLicense.OwnerId & "&ls=" & iSearchLicenseStateID)
	
elseif oLicense.OwnerTypeId = 3 then

	Response.Redirect("/company/companydetail.asp?id=" & oLicense.OwnerId & "&ls=" & iSearchLicenseStateID)
	
end if 



%>

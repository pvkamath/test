<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Note.Class.asp" --------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Declare the variables for license type/id
dim iCandidateId
dim iCompanyId 
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
dim iGroupId
dim iNoteId
dim iOwnerTypeId 'What kind of license?  Officer, Company, etc.
dim iOwnerId 'Holds the id of whatever type of owner the license has
dim iAssociateBranchId 'Is the owner assigned to a specific branch?
dim iAssociateTypeId
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iCandidateId = ScrubForSQL(request("oid"))
iCompanyId = ScrubForSql(request("cid"))
iBranchId = ScrubForSql(request("bid"))
iCompanyL2Id = ScrubForSql(request("c2id"))
iCompanyL3Id = ScrubForSql(request("c3id"))
iGroupId = ScrubForSql(request("gid"))
iNoteId = ScrubForSql(request("nid"))
iAssociateTypeID = ScrubForSql(request("AssociateTypeID"))


dim oNote 'Note Object
dim oCompany 'Company object
dim oAssociate 'Associate object

'Initialize the note object
set oNote = new Note
oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")

'If we were passed a Note ID, try to load it now.  Otherwise, we'll go
'through each of the owner type possibilities and see if we were given an owner
'to whom we'll assign this note.
if iNoteId <> "" then

	if oNote.LoadNoteById(iNoteId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed Note ID.")
		Response.End
	
	'If this is a company note, verify that it belongs to the user's company.
	elseif oNote.OwnerTypeId = 3 and (oNote.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
		AlertError("Unable to load the passed Note ID.")
		Response.End
		
	'If this is a companyl2 note, verify the appropriate access
	elseif oNote.OwnerTypeId = 4 then
	
		if not CheckIsThisCompanyL2Admin(oNote.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyl3 note, verify the appropriate access
	elseif oNote.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oNote.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if		
	
	'If this is a branch note, verify that it's branch belongs to the user's company.
	elseif oNote.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oNote.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if
		
	'If this is an officer note, verify that the officer belongs to the user's company.
	elseif oNote.OwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(oNote.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	end if
	
	iOwnerId = oNote.OwnerId
	iOwnerTypeId = oNote.OwnerTypeId
	sMode = "Edit"

elseif iCandidateId <> "" then 

	iOwnerId = iCandidateId	
	iOwnerTypeId = 1
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisAssociateAdmin(iCandidateId) then
		AlertError("Failed to load the passed Officer ID.")
		Response.End
	end if

elseif iCompanyId <> "" then
	
	iOwnerId = iCompanyId
	iOwnerTypeId = 3
	sMode = "Add"
	
	'Verify access
	if not CheckIsAdmin() then
		AlertError("Failed to load the passed Company ID.")
		Response.End
	end if
	
elseif iCompanyL2Id <> "" then
	
	iOwnerId = iCompanyL2Id
	iOwnerTypeId = 4
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL2Admin(iCompanyL2Id) then
		AlertError("Failed to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if
	
elseif iCompanyL3Id <> "" then

	iOwnerId = iCompanyL3Id
	iOwnerTypeId = 5
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisCompanyL3Admin(iCompanyL3Id) then
		AlertError("Failed to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if
	
elseif iBranchId <> "" then

	iOwnerId = iBranchId
	iOwnerTypeId = 2
	sMode = "Add"
	
	'Verify access
	if not CheckIsThisBranchAdmin(iBranchId) then
		AlertError("Failed to load the passed Branch ID.")
		Response.End
	end if

elseif iGroupID <> "" then
    
    iOwnerId = iGroupID
    iOwnerTypeId = 6
    sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 



'Configure the administration submenu options
if iOwnerTypeId = 1 then
	bAdminSubmenu = true
	sAdminSubmenuType = "OFFICERS"
elseif iOwnerTypeId = 2 or iOwnerTypeId = 4 or iOwnerTypeId = 5 then
	bAdminSubmenu = true
	sAdminSubmenuType = "BRANCHES"
elseif iOwnerTypeId = 3 then
	bAdminSubmenu = true
	sAdminSubmenuType = "COMPANY"
end if


%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.NoteText, "Note Text"))
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Record Note</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Note</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="LicenseForm" action="modnoteproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = iOwnerId %>" name="OwnerId">
<input type="hidden" value="<% = iOwnerTypeId %>" name="OwnerTypeId">
<input type="hidden" value="<% = iNoteId %>" name="NoteId">
<input type="hidden" value="<% = iAssociateTypeID %>" name="AssociateTypeID" />
<table border="0" cellpadding="5" cellspacing="5">
<% if UCase(sMode) = "EDIT" then %>
	<tr>
		<td valign="top"><b>Date: </b></td>
		<td>
			<% = oNote.NoteDate %>
		</td>
	</tr>
<% end if %>	
	<tr>
		<td valign="top"><b>Note Text: </b></td>
		<td>
			<textarea name="NoteText" cols="50" rows="5"><% = oNote.NoteText %></textarea>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
'set oPreference = nothing
set oNote = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
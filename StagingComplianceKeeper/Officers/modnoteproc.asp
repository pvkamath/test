<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Note.Class.asp" --------------------------->
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the variables for note type/id
dim iNoteId 'ID of the note in question
dim iOwnerId 'ID of whatever owner type owns the note
dim iOwnerTypeId 'What kind of note?  Officer, Company, etc.
dim iAssociateBranchId 'What is the owning associate's branch ID?
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iOwnerId = ScrubForSql(request("OwnerId"))
iOwnerTypeId = ScrubForSql(request("OwnerTypeId"))
iNoteId = ScrubForSql(request("NoteId"))

dim oNote 'Note Object

'Initialize the note object
set oNote = new Note
oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")

'Declare the Associate object and the Company object.  We'll use these to
'verify permissions for new licenses.
dim oAssociate
dim oCompany
dim oBranch

'If we were passed a Note ID, try to load it now.  Otherwise, we'll go
'with the owner ID and owner type ID to create a new one.
if iNoteId <> "" then

	if oNote.LoadNoteById(iNoteId) = 0 then
	
		'The Load was unsuccessful.  Stop.
		AlertError("Failed to load the passed Note ID.")
		Response.End
		
	'If this is a company note, verify that it belongs to the user's company.
	elseif oNote.OwnerTypeId = 3 and (oNote.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
		AlertError("Unable to load the passed Note ID.")
		Response.End
	
	'If this is a companyl2 note, verify the appropriate access
	elseif oNote.OwnerTypeId = 4 then
	
		if not CheckIsThisCompanyL2Admin(oNote.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyl3 note, verify the appropriate access
	elseif oNote.OwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(oNote.OwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if		
	
	'If this is a branch note, verify that it's branch belongs to the user's company.
	elseif oNote.OwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(oNote.OwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if
		
	'If this is an officer note, verify that the officer belongs to the user's company.
	elseif oNote.OwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(oNote.OwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	end if
	
	sMode = "Edit"

elseif iOwnerId <> "" and iOwnerTypeId <> "" then 
	
	oNote.OwnerId = iOwnerId
	oNote.OwnerTypeId = iOwnerTypeId
	
	'If this is an Officer license...
	if iOwnerTypeId = 1 then
		
		if not CheckIsThisAssociateAdmin(iOwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
	
	'If this is a Branch license...
	elseif iOwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
	
	
	'If this is a Company license...
	elseif iOwnerTypeId = 3 then
	
		if (clng(iOwnerId) <> clng(session("UserCompanyId")) or not (CheckIsAdmin())) then
			AlertError("Unable to load the passed Company ID." & iOwnerId & "," & session("UserCompanyId"))
			Response.End
		end if 
		
	'If this is a companyL2 license
	elseif iOwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license
	elseif iOwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
		
    'If this is a group note
    elseif iOwnerTypeId = 6 then
    
	else
	
		AlertError("Invalid Owner Type specified.")
		Response.End
	
	end if 

	sMode = "Add"
	
else
	
	'We needed one of the above to make this useful.
	AlertError("Failed to load a passed Owner ID.")
	Response.End

end if 






'We have a valid note.  Go ahead and set the properties based on our passed 
'form data.
if sMode = "Add" then
	oNote.OwnerId = iOwnerId
	oNote.OwnerTypeId = iOwnerTypeId
	oNote.NoteDate = now()
	oNote.UserId = session("User_Id")
end if
oNote.NoteText = ScrubForSql(request("NoteText"))



'Save the note
if oNote.SaveNote = 0 then

	AlertError("Failed to save the Note.  Please try again.")
	Response.End
	
end if 


'Clean up
set oAssociate = nothing
set oCompany = nothing
set oBranch = nothing

'Redirect back to the appropriate page.
if oNote.OwnerTypeId = 1 then
    if ScrubForSql(request("AssociateTypeID")) = 1 then
    	Response.Redirect("/officers/officerdetail.asp?id=" & oNote.OwnerId)
    else
        Response.Redirect("/officers/employeedetail.asp?id=" & oNote.OwnerId)
    end if

elseif oNote.OwnerTypeId = 2 then

	Response.Redirect("/branches/branchdetail.asp?branchid=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 4 then

	Response.Redirect("/branches/companyl2detail.asp?id=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 5 then

	Response.Redirect("/branches/companyl3detail.asp?id=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 3 then

	Response.Redirect("/company/companydetail.asp?id=" & oNote.OwnerId)
	
elseif oNote.OwnerTypeId = 6 then

    Response.Redirect("/officers/groupdetail.asp?id=" & oNote.OwnerId)

end if 



%>

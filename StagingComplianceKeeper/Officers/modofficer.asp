<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim i
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

dim iOfficerId
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
iOfficerId = ScrubForSql(request("id"))
iCompanyL2Id = ScrubForSql(request("companyl2id"))
iCompanyL3Id = ScrubForSql(request("companyl3id"))
iBranchId = ScrubForSql(request("branchid"))

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 

if iOfficerId <> "" then 

	if oAssociate.LoadAssociateById(iOfficerId) = 0 then

		'The load was unsuccessful.  End.
		Response.Write("Failed to load the passed Officer ID.")
		Response.End
	
	end if
	
	
	if not CheckIsThisAssociateAdmin(oAssociate.UserId) then
	
		AlertError("Unable to load the passed Officer ID.")
		Response.End
	
	end if
	
	sMode = "Edit"

else

	if iCompanyL2Id <> "" then
		if not CheckIsThisCompanyL2Admin(iCompanyL2Id) then
			AlertError("The " & session("CompanyL2Name") & " specified is invalid.")
			Response.End
		end if
	elseif iCompanyL3Id <> "" then 
		if not CheckIsThisCompanyL3Admin(iCompanyL3Id) then
			AlertError("The " & session("CompanyL3Name") & " specified is invalid.")
			Response.End
		end if
	elseif iBranchId <> "" then
		if not CheckIsThisBranchAdmin(iBranchId) then
			AlertError("The Branch specified is invalid.")
			Response.End
		end if
	elseif not (CheckIsAdmin() or CheckIsCompanyL2Admin() or CheckIsCompanyL3Admin() or CheckIsBranchAdmin()) then
		AlertError("Unable to create the new Officer.  Please try your request again.")
		Response.End
	end if	

	sMode = "Add"

end if
%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.FirstName, "First Name")
			|| !checkString(FORM.LastName, "Last Name")
			|| !checkString(FORM.Email, "Email")
		   )
			return false;

		if (FORM.OwnerId.selectedIndex == 0)
		{
			alert("Please select an Association.");
			FORM.OwnerId.focus();
			return false;
		}
			
		return true;
}

function HideField(controlName) {
    var ctrl;
    ctrl = document.getElementById(controlName);
    if (ctrl != null) {
        if (document.activeElement.id != ctrl.id && !ctrl.mouseIsOver) {
            ctrl.style.color = "#cccccc";
            ctrl.style.backgroundColor = "#cccccc";
        }
    }
}

function ShowField(controlName) {
    var ctrl;
    ctrl = document.getElementById(controlName);
    if (ctrl != null) {
        ctrl.style.color = "";
        ctrl.style.backgroundColor = "";
    }
}

</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> an Officer</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="OfficerForm" action="modofficerproc.asp" method="POST" onSubmit="return Validate(this)">
<%
'Pass the OfficerID on to the next step so we know we're working with an existing
'loan officer.
if iOfficerId <> "" then
%>
<input type="hidden" value="<% = iOfficerId %>" name="OfficerId">
<%
end if

%>
<table border="0" cellpadding="5" cellspacing="5">
       <tr>
		<td class="formheading">Officer is Verified: </td>
		<td align="left" valign="top">
			<input type="checkbox" name="Verified" value="1"<% if oAssociate.Verified then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td class="formreqheading">
			<span class="formreqstar">*</span> First Name: 
		</td>
		<td align="left" valign="top">
			<input type="text" name="FirstName" value="<% = oAssociate.FirstName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">
			Middle Name: 
		</td>
		<td align="left" valign="top">
			<input type="text" name="MiddleName" value="<% = oAssociate.MiddleName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formreqheading">
			<b><span class="formreqstar">*</span> Last Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="LastName" value="<% = oAssociate.LastName %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formreqheading">
			<span class="formreqstar">*</span> Email: 
		</td>
		<td align="left" valign="top">
			<input type="text" name="Email" value="<% = oAssociate.Email %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">SSN (Hover to View): </td>
		<td align="left" valign="top">
			<input type="text" name="SSN" value="<% = enDeCrypt(oAssociate.Ssn,application("RC4Pass")) %>" size="12" maxlength="11"  id="SSN" 
                onmouseover="this.mouseIsOver=true;ShowField(this.id);" onmouseout="this.mouseIsOver=false;HideField(this.id);" 
                onfocus="ShowField(this.id);" onblur="HideField(this.id);" style="color:#cccccc;background-color:#cccccc;">
		</td>
	</tr>
	<tr>
		<td class="formreqheading">
			<span class="formreqstar">*</span> Association:
		</td>
		<td align="left" valign="top">
			<% call DisplayL2sL3sBranchesDropdown(oAssociate.OwnerId, oAssociate.OwnerTypeId, session("UserCompanyId"), false, 1) %>
		</td>
	</tr>
	<tr>
		<td class="formheading">Date Of Birth: </td>
		<td align="left" valign="top">
			<input type="text" name="DateOfBirth" value="<% = oAssociate.DateOfBirth %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('OfficerForm.DateOfBirth');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('OfficerForm.DateOfBirth');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td class="formheading">NMLS #: </td>
		<td align="left" valign="top">
			<input type="text" name="NMLSNumber" value="<% = oAssociate.NMLSNumber %>" size="25" maxlength="25">
		</td>
	</tr>
	<tr>
		<td class="formheading">Drivers License State: </td>
		<td align="left" valign="top">
			<% call DisplayStatesDropDown(oAssociate.DriversLicenseStateId,1,"DriversLicenseStateId") 'functions.asp %>
		</td>
	</tr>	
	<tr>
		<td class="formheading">Drivers License Number: </td>
		<td align="left" valign="top">
			<input type="text" name="DriversLicenseNo" value="<% = oAssociate.DriversLicenseNo %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Hire Date: </td>
		<td align="left" valign="top">
			<input type="text" name="HireDate" value="<% = oAssociate.HireDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('OfficerForm.HireDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('OfficerForm.HireDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>
	<tr>
		<td class="formheading">Beginning Education<br>History Date: </td>
		<td align="left">
			<input type="text" name="BeginningEducationDate" value="<% = oAssociate.BeginningEducationDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('OfficerForm.BeginningEducationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('OfficerForm.BeginningEducationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>	
	<tr>
		<td class="formheading">Officer is Inactive: </td>
		<td align="left" valign="top">
			<input type="checkbox" name="Inactive" value="1"<% if oAssociate.Inactive then %> CHECKED<% end if %>>
		</td>
	</tr>
	<tr>
		<td class="formheading">Inactive Date: </td>
		<td align="left" valign="top">
			<input type="text" name="TerminationDate" value="<% = oAssociate.TerminationDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('OfficerForm.TerminationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('OfficerForm.TerminationDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>	
	<tr>
		<td class="formheading">Fingerprint Deadline Date: </td>
		<td align="left" valign="top">
			<input type="text" name="FingerprintDeadlineDate" value="<% = oAssociate.FingerprintDeadlineDate %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('OfficerForm.FingerprintDeadlineDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('OfficerForm.FingerprintDeadlineDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
		</td>
	</tr>		
	<tr>
		<td class="formheading">Manager: </td>
		<td align="left" valign="top">
			<input type="text" name="Manager" value="<% = oAssociate.Manager %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Out of State Origination: </td>
		<td align="left" valign="top">
			<input type="checkbox" name="OutOfStateOrig" value="1"<% if oAssociate.OutOfStateOrig then %> CHECKED<% end if %>>
		</td>
	</tr>	
		
	<tr>
		<td class="formheading">Employee ID: </td>
		<td align="left" valign="top">
			<input type="text" name="EmployeeID" value="<% = oAssociate.EmployeeId %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Title: </td>
		<td align="left" valign="top">
			<input type="text" name="Title" value="<% = oAssociate.Title %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Department: </td>
		<td align="left" valign="top">
			<input type="text" name="Department" value="<% = oAssociate.Department %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #1: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone" value="<% = oAssociate.Phone %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #1 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="PhoneExt" value="<% = oAssociate.PhoneExt %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #2: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone2" value="<% = oAssociate.Phone2 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #2 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone2Ext" value="<% = oAssociate.Phone2Ext %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #3: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone3" value="<% = oAssociate.Phone3 %>" size="50" maxlength="20">
		</td>
	</tr>
	<tr>
		<td class="formheading">Phone #3 Ext: </td>
		<td align="left" valign="top">
			<input type="text" name="Phone3Ext" value="<% = oAssociate.Phone3Ext %>" size="10" maxlength="10">
		</td>
	</tr>
	<tr>
		<td class="formheading">Fax: </td>
		<td align="left" valign="top">
			<input type="text" name="Fax" value="<% = oAssociate.Fax %>" size="12" maxlength="15">
		</td>
	</tr>
	<% call DisplayAssociateAddresses(1,oAssociate.Address1)  'located in functions.asp %>
	<% call DisplayAssociateAddresses(2,oAssociate.Address2)  'located in functions.asp %>
	<% call DisplayAssociateAddresses(3,oAssociate.Address3)  'located in functions.asp %>
	<% call DisplayAssociateAddresses(4,oAssociate.Address4)  'located in functions.asp %>
	<tr>
		<td class="formheading">License Payment: </td>
		<td align="left" valign="top">
			<input type="text" name="LicensePayment" value="<% = oAssociate.LicensePayment %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>
		<td colspan="2"><p></td>
	</tr>
	<%
	 for i = 1 to 15
		 if session("OfficerCustField" & i & "Name") <> "" then
	%>
	<tr>
		<td class="formheading"><% = session("OfficerCustField" & i & "Name") %>: </td>
		<td align="left" valign="top">
			<input type="text" name="OfficerCustField<%=i%>" value="<% = oAssociate.GetOfficerCustField(i) %>" size="50" maxlength="50">
		</td>
	</tr>	
	<%
		 end if
	 next
	%>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%
'set saRS = nothing
set oCompany = nothing
set oAssociate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

dim iOfficerId
dim iAssocId
iOfficerId = ScrubForSql(request("id"))
iAssocId = ScrubForSql(request("aid"))


dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 

dim sMode
if iOfficerId <> "" then 

	if oAssociate.LoadAssociateById(iOfficerId) = 0 then

		'The load was unsuccessful.  End.
		Response.Write("Failed to load the passed Officer ID.")
		Response.End
	
	end if
	
	if oAssociate.CompanyId <> oCompany.CompanyId then
	
		'This is not their officer!
		Response.Write("You do not have access to this Officer.")
		Response.End
		
	elseif CheckIsBranchAdmin() then
		
		'Make sure that this associate actually belongs to this branchadmin.
		if not CheckIsThisBranchAdmin(oAssociate.BranchId) then
		
			AlertError("You do not have access to this Officer.")
			Response.End
		
		end if
		
	end if 

	'sMode = "Edit"

else

	'sMode = "Add"
	Response.Write("Failed to load the Officer.")
	Response.End

end if

dim iCourseId
iCourseId = ScrubForSql(request("CourseId"))
dim oCourseRs 'Recordset object

if iAssocId <> "" then
	
	set oCourseRs = oAssociate.GetCourse(iAssocId)
	if oCourseRs.State <> 0 then
	
		if not (oCourseRs.EOF and oCourseRs.BOF) then
		
			sMode = "Edit"
			
		else
		
			Response.Write("Failed to load the Course information")
			Response.End
			
		end if
		
	else
		
		Response.Write("Failed to load the Course information.")
		Response.End
		
	end if
	
else

	sMode = "Add"
	
end if 


dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

'The course id will have been set in the request statement above if we are
'changing the course or setting a new one.  Otherwise we use the existing
'association value.
if iCourseId <> "" then
	iCourseId = oCourse.LoadCourseById(iCourseId)
else
	iCourseId = oCourse.LoadCourseById(oCourseRs("CourseId"))
end if 

if iCourseId = 0 then

	Response.Write("Failed to load the Course information.")
	Response.End
	
elseif iCourseId > 0 then

	Response.Write("This course record may not be edited.")
	Response.End
	
end if

			
dim dExpDate
dim dCompletionDate
dExpDate = ScrubForSql(request("ExpirationDate"))
dCompletionDate = ScrubForSql(request("CompletionDate"))
			

if sMode = "Edit" then

	'Assign the properties based on our passed parameters
	if oAssociate.UpdateCourse(iAssocId, iCourseId, _
		dExpDate, dCompletionDate) = 0 then
		
		Response.Write("Unable to update the Officer's course information.")
		Response.End
		
	end if
	
elseif sMode = "Add" then

	'Assign the properties based on our passed parameters
	if oAssociate.AddCourse(iCourseId, dExpDate, dCompletionDate) = 0 then
	
		Response.Write("Unable to add the Officer's course information.")
		Response.End
		
	end if 
		
end if 

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officer -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officer Education: <% = oAssociate.FullName %></td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> an Officer Course Listing</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if sMode = "Add" then
%>
<b>The record for the course "<% = oCourse.Name %>" was successfully created.</b>
<%
else
%>
<b>The record for the course "<% = oCourse.Name %>" was successfully updated.</b>
<%
end if
%>
<p>
<%if ScrubForSql(request("AssociateTypeID")) = 1 then %>
<a href="officerdetail.asp?id=<% = iOfficerId %>">Return to Officer Listing</a>
<%else %>
<a href="employeedetail.asp?id=<% = iOfficerId %>">Return to Employee Listing</a>
<%end if %>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCourseRs = nothing
set oCourse = nothing
set oCompany = nothing
set oAssociate = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
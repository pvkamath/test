<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable

	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()
	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%
dim i
dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

dim iOfficerId
dim sOwnerId
dim iCompanyL2Id
dim iCompanyL3Id
dim iBranchId
iOfficerId = ScrubForSql(request("OfficerId"))
sOwnerId = ScrubForSql(request("OwnerId"))

if left(sOwnerId, 1) = "a" then
	iCompanyL2Id = mid(sOwnerId, 2)
elseif left(sOwnerId, 1) = "b" then
	iCompanyL3Id = mid(sOwnerID, 2)
elseif left(sOwnerId, 1) = "c" then
	iBranchId = mid(sOwnerId, 2)
end if

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	AlertError("Failed to load the passed Company ID.")
	Response.end
		
end if 

dim sMode
if iOfficerId <> "" then 
	if oAssociate.LoadAssociateById(iOfficerId) = 0 then

		'The load was unsuccessful.  End.
		AlertError("Failed to load the passed Officer ID.")
		Response.End
	
	end if
	

	if not CheckIsThisAssociateAdmin(oAssociate.UserId) then
	
		AlertError("Unable to load the passed Officer ID.")
		Response.End
	
	end if
	
	sMode = "Edit"

else

	if iCompanyL2Id <> "" then
		if not CheckIsThisCompanyL2Admin(iCompanyL2Id) then
			AlertError("The " & session("CompanyL2Name") & " specified is invalid.")
			Response.End
		end if
	elseif iCompanyL3Id <> "" then 
		if not CheckIsThisCompanyL3Admin(iCompanyL3Id) then
			AlertError("The " & session("CompanyL3Name") & " specified is invalid.")
			Response.End
		end if
	elseif iBranchId <> "" then
		if not CheckIsThisBranchAdmin(iBranchId) then
			AlertError("The Branch specified is invalid.")
			Response.End
		end if
	elseif not CheckIsAdmin() then
		AlertError("Unable to create the new Officer.  Please try your request again.")
		Response.End
	end if
	sMode = "Add"

end if


'Assign the properties based on our passed parameters
oAssociate.CompanyId = ScrubForSql(session("UserCompanyId"))
if iCompanyL2Id <> "" then
	oAssociate.CompanyL2Id = iCompanyL2Id
elseif iCompanyL3Id <> "" then
	oAssociate.CompanyL3Id = iCompanyL3Id
elseif iBranchId <> "" then
	oAssociate.BranchId = iBranchId
else
	oAssociate.BranchId = ""
	oAssociate.CompanyL2Id = ""
	oAssociate.CompanyL3Id = ""
end if
oAssociate.UserStatus = 1
oAssociate.AssociateTypeID = 1
oAssociate.FirstName = ScrubForSql(request("FirstName"))
oAssociate.MiddleName = ScrubForSql(request("MiddleName"))
oAssociate.LastName = ScrubForSql(request("LastName"))
oAssociate.SSN = enDeCrypt(ScrubForSql(request("SSN")),application("RC4Pass"))
oAssociate.NMLSNumber = ScrubForSql(request("NMLSNumber"))
oAssociate.DateOfBirth = ScrubForSql(request("DateOfBirth"))
oAssociate.DriversLicenseStateId = ScrubForSql(request("DriversLicenseStateId"))
oAssociate.DriversLicenseNo = ScrubForSql(request("DriversLicenseNo"))
oAssociate.HireDate = ScrubForSql(request("HireDate"))
oAssociate.BeginningEducationDate = ScrubForSql(request("BeginningEducationDate"))
oAssociate.TerminationDate = ScrubForSql(request("TerminationDate"))
oAssociate.FingerprintDeadlineDate = ScrubForSql(request("FingerprintDeadlineDate"))
if ScrubForSql(request("Inactive")) = "1" then
	oAssociate.Inactive = true
else
	oAssociate.Inactive = false
end if

if ScrubForSql(request("Verified")) = "1" then
	oAssociate.Verified = true
else
	oAssociate.Verified = false
end if


oAssociate.Manager = ScrubForSql(request("Manager"))
if ScrubForSql(request("OutOfStateOrig")) = "1" then
	oAssociate.OutOfStateOrig = true
else
	oAssociate.OutOfStateOrig = false
end if
oAssociate.EmployeeID = ScrubForSql(request("EmployeeID"))
oAssociate.Title = ScrubForSql(request("Title"))
oAssociate.Department = ScrubForSql(request("Department"))
oAssociate.Email = ScrubForSql(request("Email"))
oAssociate.Phone = ScrubForSql(request("Phone"))
oAssociate.PhoneExt = ScrubForSql(request("PhoneExt"))
oAssociate.Phone2 = ScrubForSql(request("Phone2"))
oAssociate.Phone2Ext = ScrubForSql(request("Phone2Ext"))
oAssociate.Phone3 = ScrubForSql(request("Phone3"))
oAssociate.Phone3Ext = ScrubForSql(request("Phone3Ext"))
oAssociate.Fax = ScrubForSql(request("Fax"))

'Addresses
oAssociate.Address1.AddressLine1 = ScrubForSql(request("Address1_AddressLine1"))
oAssociate.Address1.AddressLine2 = ScrubForSql(request("Address1_AddressLine2"))
oAssociate.Address1.City = ScrubForSql(request("Address1_City"))
oAssociate.Address1.StateId = ScrubForSql(request("Address1_StateId"))
oAssociate.Address1.Zipcode = ScrubForSql(request("Address1_Zipcode"))
oAssociate.Address1.ZipcodeExt = ScrubForSql(request("Address1_ZipcodeExt"))

oAssociate.Address2.AddressLine1 = ScrubForSql(request("Address2_AddressLine1"))
oAssociate.Address2.AddressLine2 = ScrubForSql(request("Address2_AddressLine2"))
oAssociate.Address2.City = ScrubForSql(request("Address2_City"))
oAssociate.Address2.StateId = ScrubForSql(request("Address2_StateId"))
oAssociate.Address2.Zipcode = ScrubForSql(request("Address2_Zipcode"))
oAssociate.Address2.ZipcodeExt = ScrubForSql(request("Address2_ZipcodeExt"))

oAssociate.Address3.AddressLine1 = ScrubForSql(request("Address3_AddressLine1"))
oAssociate.Address3.AddressLine2 = ScrubForSql(request("Address3_AddressLine2"))
oAssociate.Address3.City = ScrubForSql(request("Address3_City"))
oAssociate.Address3.StateId = ScrubForSql(request("Address3_StateId"))
oAssociate.Address3.Zipcode = ScrubForSql(request("Address3_Zipcode"))
oAssociate.Address3.ZipcodeExt = ScrubForSql(request("Address3_ZipcodeExt"))

oAssociate.Address4.AddressLine1 = ScrubForSql(request("Address4_AddressLine1"))
oAssociate.Address4.AddressLine2 = ScrubForSql(request("Address4_AddressLine2"))
oAssociate.Address4.City = ScrubForSql(request("Address4_City"))
oAssociate.Address4.StateId = ScrubForSql(request("Address4_StateId"))
oAssociate.Address4.Zipcode = ScrubForSql(request("Address4_Zipcode"))
oAssociate.Address4.ZipcodeExt = ScrubForSql(request("Address4_ZipcodeExt"))

oAssociate.CellPhone = ScrubForSql(request("CellPhone"))
oAssociate.HomePhone = ScrubForSql(request("HomePhone"))
oAssociate.HomeEmail = ScrubForSql(request("HomeEmail"))
oAssociate.LicensePayment = ScrubForSql(request("LicensePayment"))

for i = 1 to 15
	if session("OfficerCustField" & i & "Name")  <> "" then
		call oAssociate.SetOfficerCustField(i,ScrubForSql(request("OfficerCustField" & i)))
	end if
next

'If this is a new associate we need to save it before it will have a unique ID.
dim iNewOfficerId
if oAssociate.UserId = "" then

	iNewOfficerId = oAssociate.SaveAssociate

	if iNewOfficerId = 0 then

		'Zero indicates failure.  We should have been returned the valid new ID
		'of the Officer.  Stop.
		if not oAssociate.VerifyUniqueEmail(oAssociate.Email) then
			AlertError("The email address provided for this officer is already in use in " & _
				"the system.  Saving the officer requires a unique email " & _
				"address.")
			Response.End
		else
			AlertError("Failed to save the new Officer.")
			Response.End
		end if

	end if
	
else

	iNewOfficerId = oAssociate.SaveAssociate

	if iNewOfficerId = 0 then

		'The save failed.
		if not oAssociate.VerifyUniqueNMLS(oAssociate.NMLSNumber) then
			AlertError("The NMLS Number provided for this officer is already in use in " & _
				"the system.  Saving the officer requires a NMLS Number " & _
				"address.")
			Response.End
		else
			AlertError("Failed to save the existing Officer.")
			Response.End
		end if

	elseif Clng(iNewOfficerId) <> Clng(iOfficerId) then
	
		'This shouldn't happen.  There shouldn't be a new ID assigned if
		'we already had one passed into this page via the querystring and
		'used to load our Company object.
		AlertError("Unexpected results while saving existing Officer.")
		Response.End
		
	end if

end if

%>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officer -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>



<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><% = sMode %> an Officer</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<%
if sMode = "Add" then
%>
<b>The new Officer "<% = oAssociate.FirstName & " " & oAssociate.LastName %>" was successfully created.</b>
<%
else
%>
<b>The Officer "<% = oAssociate.FirstName & " " & oAssociate.LastName %>" was successfully updated.</b>
<%
end if
%>
<p>
<a href="OfficerList.asp">Return to Officer Listing</a>
<br>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oCompany = nothing
set oAssociate = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Test.Class.asp" --------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

' Template Constants -- Modify as needed per each page:
const USES_FORM_VALIDATION = False			' Template Constant
const SHOW_PAGE_BACKGROUND = True			' Template Constant
const TRIM_PAGE_MARGINS = True			' Template Constant
const SHOW_MENUS = True					' Template Constant
const SIDE_MENU_SELECTED = ""				' Template Constant
const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
const MENU_NUMBER = 1
dim iPage
dim sPgeTitle							' Template Variable
dim sMode 'as string
dim iUserID, iTestID 'as integer
dim iAssociateTypeID, sAssociateTerm
dim oTest 'as object
dim sName, sTestDate 'as string
dim iStateID 'as integer
dim iStatusID 'as integer
dim iTestTypeID 'as integer

' Set Page Title:
sPgeTitle = ""
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()	

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

set oTest = New Test
oTest.ConnectionString = application("sDataSourceName")

iUserID = GetFormElementAndScrub("oid")
iAssociateTypeID = GetFormElementAndScrub("AssociateTypeID")

sAssociateTerm = GetFormElementAndScrub("AssociateTerm")
if sAssociateTerm = "" then
    sAssociateTerm = "Officer"
end if

'Validate UserID

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if iUserId <> "" then 

	if oAssociate.LoadAssociateById(iUserId) = 0 then

		'The load was unsuccessful.  End.
		Response.Write("Failed to load the passed User ID.")
		Response.End
	
	end if
	
	if oAssociate.CompanyId <> oCompany.CompanyId then
	
		'This is not their officer!
		Response.Write("You do not have access to this Officer.")
		Response.End
		
	elseif CheckIsBranchAdmin() then
		
		'Make sure that this associate actually belongs to this branchadmin.
		if not CheckIsThisBranchAdmin(oAssociate.BranchId) then
		
			AlertError("You do not have access to this Officer.")
			Response.End
		
		end if
		
	end if 

	'sMode = "Edit"

else

	'sMode = "Add"
	Response.Write("Failed to load the Officer.")
	Response.End

end if

iTestID = GetFormElementAndScrub("TestID")	
if trim(iTestID) <> "" then
	sMode = "Edit"
	
	'Validate TestID
	if not isnumeric(iTestID) then
		response.redirect("/error.asp?message=The Test does not exist.")
	end if
	
	oTest.UserID = iUserID	
	oTest.TestID = iTestID
	
	if not oTest.LoadTest() then
		set oTest = nothing
		response.redirect("/error.asp?message=The Test does not exist.")		
	end if
	
	sName = oTest.name
	iStateID = oTest.StateID
	sTestDate = oTest.TestDate
	iStatusID = oTest.StatusID
    iTestTypeID = oTest.TestTypeID
else
	sMode = "Add"
	iStatusID = ""
end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//-->
</script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">
	$(function() {
		$(".datepicker").datepicker({dateFormat: 'mm/dd/yy'});
	});

function ValidateForm(FORM) 
    {
        var bValid;
        bValid = true;
        if (FORM.AssociateTestTypeID.value != 1 && FORM.StateID.value != 0) 
        {
            window.alert("Non-National exams must have a test type of Normal.");
            bValid = false;
        }

        return (checkString(FORM.Name, "Name") && checkIsDate(FORM.TestDate,'Please enter a valid Test Date.') && checkString(FORM.StatusID, "Status") && bValid)
    }

</script>

<%
FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>

<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">				
			<!-- Main Box -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <%=sAssociateTerm%> Test: <% = oAssociate.FullName %></td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td>
												<span class="activeTitle"><%= sMode %> a Test</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
											</td>
										</tr>
									</table>
									<br>					
									<form name="frm1" id="frm1" method="POST" action="modtestproc.asp" onsubmit="return ValidateForm(this)">
										<input type="hidden" name="UserID" id="UserID" value="<%= iUserID %>">
										<input type="hidden" name="TestID" id="TestID" value="<%= iTestID %>">
										<input type="hidden" name="Mode" id="Mode" value="<%= sMode %>">										
                                        <input type="hidden" value="<% = iAssociateTypeID %>" name="AssociateTypeID" />
									<table>
										<tr>
											<td><b>Name:</b></td>
											<td>
												<input type="text" name="Name" id="Name" value="<%= sName %>" maxlength="100" size="30">
											</td>
										</tr>
										<tr>
											<td><b>State:</b></td>
											<td>
												<% call DisplayStatesDropDown(iStateID,5,"StateID") 'functions.asp %>
											</td>
										</tr>				
										<tr>
											<td><b>Test Type:</b></td>
											<td>
												<% call DisplayAssociateTestTypeDropDown(iTestTypeID,1) 'functions.asp %>
											</td>
										</tr>				
										<tr>
											<td><b>Test Date:</b></td>
											<td>
												<input type="textbox" name="TestDate" value="<%= sTestDate %>" size="10" maxlength="10" class="datepicker"></td>
											</td>
										</tr>										
										<tr>
											<td><b>Status:</b></td>
											<td>
												<% call DisplayTestStatusesDropDown("StatusID", oTest, iStatusID, 1) 'function.asp %>
											</td>
										</tr>				
										<tr>
											<td></td>
											<td align="left" valign="top">
												<p><br>
												<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
												<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
											</td>
										</tr>																									
									</table>										
									</form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
			<!-- space-->
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
	</tr>
	<tr>
		<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
	</tr>
</table>

<%
set oTest = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
PrintShellFooter SHOW_MENUS										' From shell.asp
EndPage														' From htmlElements.asp
%>
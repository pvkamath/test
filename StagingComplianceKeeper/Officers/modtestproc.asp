<%
option explicit
%>

<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Test.Class.asp" --------------------------->

<%
dim iUserID, iTestID 'as integer
dim oTest 'as object
dim sName, sTestDate 'as string
dim iStateID 'as integer
dim iStatusID 'as boolean
dim iTestTypeID 'as integer
dim sMode 'as string
dim bSuccess 'as boolean

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()	

sMode = UCase(GetFormElement("Mode"))

iUserID = GetFormElementAndScrub("UserID")
'Validate UserID
if not isnumeric(iUserID) then
	response.redirect("/error.asp?message=The Associate does not exist.")
end if

iTestID = GetFormElementAndScrub("TestID")

'If Edit or Delete, validate TestID
if sMode = "EDIT" or sMode = "DELETE" then
	'Validate TestID
	if trim(iTestID) = "" or not isnumeric(iTestID) then
		response.redirect("/error.asp?message=The Test does not exist.")
	end if
end if

'Get Info
sName =  GetFormElementAndScrub("Name")
iStateID =  GetFormElementAndScrub("StateID")
sTestDate =  GetFormElementAndScrub("TestDate")
iStatusID =  GetFormElementAndScrub("StatusID")
iTestTypeID = GetFormElementAndScrub("AssociateTestTypeID")

set oTest = New Test
oTest.ConnectionString = application("sDataSourceName")
oTest.UserID = iUserID	

'Determine whether to Add, Edit or Delete the test
bSuccess = false 'default
if sMode = "ADD" or sMode = "EDIT" then
	if sMode = "EDIT" then
		oTest.TestID = iTestID
	end if	

	oTest.UserID = iUserID
	oTest.Name = sName
	oTest.StateID = iStateID
	oTest.TestDate = sTestDate
	oTest.StatusID = iStatusID
	oTest.TestTypeID = iTestTypeID

	if oTest.SaveTest > 0 then
		bSuccess = true
	end if	
elseif sMode = "DELETE" then
	oTest.TestID = iTestID
	
	bSuccess = oTest.DeleteTest
end if

set oTest = nothing

if bSuccess then
    if ScrubForSql(request("AssociateTypeID")) = 1 then
	    response.redirect("/officers/officerdetail.asp?id=" & iUserID)  
    else
	    response.redirect("/officers/employeedetail.asp?id=" & iUserID)  
    end if
else
	response.redirect("/error.asp?message=There was a processing error.")
end if
%>
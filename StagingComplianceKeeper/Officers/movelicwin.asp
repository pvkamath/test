<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>
<!-- #include virtual="/includes/License.Class.asp" -------------------------->


<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


'Get the passed type value to see which type of files we're looking for.
dim sPageType
dim sLetter
sPageType = request("type")
sLetter = request("let")
if sLetter = "" then
	sLetter = "A"
end if 

dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")
oCompanyL2.CompanyId = session("UserCompanyId")

dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")
oCompanyL3.CompanyId = session("UserCompanyId")

dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")
oBranch.CompanyId = session("UserCompanyId")

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1
oAssociate.SearchLastNameStart = sLetter

'Restrict the list if the user does not have company-wide access.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
end if


dim iLicenseId
iLicenseId = ScrubForSql(request("lid"))

dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

if oLicense.LoadLicenseById(iLicenseId) = 0 then

	'The Load was unsuccessful.  Stop.
	AlertError("Failed to load the passed License ID.")
	Response.End
	
'If this is a company license, verify that it belongs to the user's company.
elseif oLicense.OwnerTypeId = 3 and (oLicense.OwnerId <> session("UserCompanyId") or not (CheckIsAdmin())) then
		
	AlertError("Unable to load the passed License ID.")
	Response.End
		
'If this is a companyL2 license, verify that it belongs to the user's company.
elseif oLicense.OwnerTypeId = 4 then 
	
	if not CheckIsThisCompanyL2Admin(oLicense.OwnerId) and not CheckIsThisCompanyL2Viewer(oLicense.OwnerId) then			
		AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
		Response.End
	end if

'If this is a companyL3 license, verify that it belongs to the user's company.
elseif oLicense.OwnerTypeId = 5 then
	
	if not CheckIsThisCompanyL3Admin(oLicense.OwnerId) and not CheckIsThisCompanyL3Viewer(oLicense.OwnerId) then
		AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
		Response.End
	end if				
	
'If this is a branch license, verify that its branch belongs to the user's company.
elseif oLicense.OwnerTypeId = 2 then
	
	if not CheckIsThisBranchAdmin(oLicense.OwnerId) and not CheckIsThisBranchViewer(oLicense.OwnerId) then
		AlertError("Unable to load the passed Branch ID.")
		Response.End
	end if 
		
'If this is an officer license, verify that the officer belongs to the user's company.
elseif oLicense.OwnerTypeId = 1 then
	
	if not CheckIsThisAssociateAdmin(oLicense.OwnerId) and not CheckIsThisAssociateViewer(oLicense.OwnerId) then
		AlertError("Unable to load the passed Officer ID.")
		Response.End
	end if
		
end if 
	
dim iOrigOwnerId
dim iOrigOwnerTypeId
dim iOwnerId
dim iOwnerTypeId
	
iOrigOwnerId = oLicense.OwnerId
iOrigOwnerTypeId = oLicense.OwnerTypeId

	
dim bFoundFiles
dim iStateId
dim oRs

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
	function UpdateParent() {
		window.opener.location.reload(true);
		window.close();
	}
		
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	'PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" alt="Select a File" align="absmiddle" vspace="10"> Move a License</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<form name="OwnerTypes" method="POST" action="dllist.asp">
							<tr>
								<td class="newstitle"><a href="movelicwin.asp?lid=<% = iLicenseId %>"><% = session("CompanyName") %></a></td>
								<td class="newstitle"><a href="movelicwin.asp?lid=<% = iLicenseId %>&type=l2"><% = session("CompanyL2Name") %></a></td>
								<td class="newstitle"><a href="movelicwin.asp?lid=<% = iLicenseId %>&type=l3"><% = session("CompanyL3Name") %></a></td>
								<td class="newstitle"><a href="movelicwin.asp?lid=<% = iLicenseId %>&type=br">Branches</a></td>
								<td class="newstitle"><a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of">Officers</a></td>
							</tr>
							</form>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">

<%
if sPageType = "go" then

	iOwnerId = request("oid")
	iOwnerTypeId = request("otid")
	
	'If this is an Officer license...
	if iOwnerTypeId = 1 then
	
		if not CheckIsThisAssociateAdmin(iOwnerId) then
			AlertError("Unable to load the passed Officer ID.")
			Response.End
		end if
		
	'If this is a Branch license...
	elseif iOwnerTypeId = 2 then
	
		if not CheckIsThisBranchAdmin(iOwnerId) then
			AlertError("Unable to load the passed Branch ID.")
			Response.End
		end if 
		
	'If this is a Company license...
	elseif iOwnerTypeId = 3 then
	
		if (clng(iOwnerId) <> session("UserCompanyId") or not (CheckIsAdmin())) then
			AlertError("Unable to load the passed Company ID.(" & iOwnerId & "," & session("UserCompanyId") & "," & CheckIsAdmin() & ")")
			Response.End
		end if 
		
	'If this is a companyL2 license
	elseif iOwnerTypeId = 4 then 
	
		if not CheckIsThisCompanyL2Admin(iOwnerId) then			
			AlertError("Unable to load the passed " & session("CompanyL2Name") & " ID.")
			Response.End
		end if

	'If this is a companyL3 license
	elseif iOwnerTypeId = 5 then
	
		if not CheckIsThisCompanyL3Admin(iOwnerId) then
			AlertError("Unable to load the passed " & session("CompanyL3Name") & " ID.")
			Response.End
		end if	
		
	else
	
		AlertError("Invalid Owner Type specified.")
		Response.End
	
	end if 
	
	oLicense.OwnerId = iOwnerId
	oLicense.OwnerTypeId = iOwnerTypeId
	
	if oLicense.SaveLicense("", "") = 0 then
		AlertError("Unable to update the requested license.")
		Response.End
	else
		%><script type="text/javascript">UpdateParent();</script><%
	end if 

elseif sPageType = "" then 
%>

	<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyName") %> List</td>
		</tr>
		<tr>
			<td colspan="2"><b><a href="movelicwin.asp?type=go&lid=<% = iLicenseId %>&oid=<% = oCompany.CompanyId %>&otid=3"><% = oCompany.Name %></a></b></td>
		</tr>
	</table>
	
<%
elseif sPageType = "l2" then


	'Find the files for any second-tier companies.
	set oRs = oCompanyL2.SearchCompanies()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyL2Name") %> List</td>
		</tr>
		<%
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisCompanyL2Admin(oRs("CompanyL2Id")) or CheckIsThisCompanyL2Viewer(oRs("CompanyL2Id")) then
	
			iResult = oCompanyL2.LoadCompanyById(oRs("CompanyL2Id"))
			
			%>	
		<tr>
			<td colspan="2"><a href="movelicwin.asp?type=go&lid=<% = iLicenseId %>&oid=<% = oCompanyL2.CompanyL2Id %>&otid=4"><% = oCompanyL2.Name %></a></td>
		</tr>
			<%

			end if

			oRs.MoveNext
		loop
		%></table><%
	end if


elseif sPageType = "l3" then


	'Find the files for any third-tier companies.
	set oRs = oCompanyL3.SearchCompanies()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyL3Name") %> List</td>
		</tr>
		<%
		
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisCompanyL2Admin(oRs("CompanyL3Id")) or CheckIsThisCompanyL3Viewer(oRs("CompanyL3Id")) then
	
				iResult = oCompanyL3.LoadCompanyById(oRs("CompanyL3Id"))
				
				%>	
		<tr>
			<td colspan="2"><a href="movelicwin.asp?type=go&lid=<% = iLicenseId %>&oid=<% = oCompanyL3.CompanyL3Id %>&otid=5"><% = oCompanyL3.Name %></a></td>
		</tr>
				<%

			end if
			oRs.MoveNext
		loop
		%></table><%
	end if
	
	
elseif sPageType = "br" then
	
	
	'Find the files for any branches.
	set oRs = oBranch.SearchBranches()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle">Branch List</td>
		</tr>
		<%	
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisBranchAdmin(oRs("BranchId")) or CheckIsThisBranchViewer(oRs("BranchId")) then
	
				iResult = oBranch.LoadBranchById(oRs("BranchId"))
			
				%>	
		<tr>
			<td colspan="2"><a href="movelicwin.asp?type=go&lid=<% = iLicenseId %>&oid=<% = oBranch.BranchId %>&otid=2"><% = oBranch.Name %></a></td>
		</tr>
				<%

			end if
			oRs.MoveNext
		loop
		%></table><%
	end if




elseif sPageType = "of" then
	
	
	'Find the files for any branches.
	set oRs = oAssociate.SearchAssociates()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">
			<tr>
				<td>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=a"<% if sLetter = "a" then %> class="newsTitle"<% end if %>>A</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=b"<% if sLetter = "b" then %> class="newsTitle"<% end if %>>B</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=c"<% if sLetter = "c" then %> class="newsTitle"<% end if %>>C</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=d"<% if sLetter = "d" then %> class="newsTitle"<% end if %>>D</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=e"<% if sLetter = "e" then %> class="newsTitle"<% end if %>>E</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=f"<% if sLetter = "f" then %> class="newsTitle"<% end if %>>F</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=g"<% if sLetter = "g" then %> class="newsTitle"<% end if %>>G</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=h"<% if sLetter = "h" then %> class="newsTitle"<% end if %>>H</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=i"<% if sLetter = "i" then %> class="newsTitle"<% end if %>>I</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=j"<% if sLetter = "j" then %> class="newsTitle"<% end if %>>J</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=k"<% if sLetter = "k" then %> class="newsTitle"<% end if %>>K</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=l"<% if sLetter = "l" then %> class="newsTitle"<% end if %>>L</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=m"<% if sLetter = "m" then %> class="newsTitle"<% end if %>>M</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=n"<% if sLetter = "n" then %> class="newsTitle"<% end if %>>N</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=o"<% if sLetter = "o" then %> class="newsTitle"<% end if %>>O</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=p"<% if sLetter = "p" then %> class="newsTitle"<% end if %>>P</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=q"<% if sLetter = "q" then %> class="newsTitle"<% end if %>>Q</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=r"<% if sLetter = "r" then %> class="newsTitle"<% end if %>>R</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=s"<% if sLetter = "s" then %> class="newsTitle"<% end if %>>S</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=t"<% if sLetter = "t" then %> class="newsTitle"<% end if %>>T</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=u"<% if sLetter = "u" then %> class="newsTitle"<% end if %>>U</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=v"<% if sLetter = "v" then %> class="newsTitle"<% end if %>>V</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=w"<% if sLetter = "w" then %> class="newsTitle"<% end if %>>W</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=x"<% if sLetter = "x" then %> class="newsTitle"<% end if %>>X</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=y"<% if sLetter = "y" then %> class="newsTitle"<% end if %>>Y</a>
				<a href="movelicwin.asp?lid=<% = iLicenseId %>&type=of&let=z"<% if sLetter = "z" then %> class="newsTitle"<% end if %>>Z</a>
				</td>
			</tr>
		</table>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle">Officer List</td>
		</tr>
		<%	
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisAssociateAdmin(oRs("UserId")) or CheckIsThisAssociateViewer(oRs("UserId")) then
	
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
			
				%>	
		<tr>
			<td colspan="2"><a href="movelicwin.asp?type=go&lid=<% = iLicenseId %>&oid=<% = oAssociate.UserId %>&otid=1"><% = oAssociate.FullName %></a></td>
		</tr>
				<%

			end if
			end if 
			oRs.MoveNext
		loop
		%></table><%
	end if	

end if
%>
				<p><br>
			</td>
		</tr>
	</table>
</body>
</html>
<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"


'Verify that the user has logged in.
CheckIsCourseProvider()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Course information variables
dim dExpDate
dim dCompletionDate
dim iCourseId
'dExpDate = ScrubForSql(request("ExpirationDate"))
'dCompletionDate = ScrubForSql(request("CompletionDate"))
'iCourseId = ScrubForSql(request("CourseId"))


'Configure the administration submenu options
bAdminSubmenu = false
'sAdminSubmenuType = "OFFICERS"

	
	
'We need to find and parse the binary information uploaded
dim iBinFormData
dim sFormData
dim iCount
	
iBinFormData = Request.BinaryRead(Request.TotalBytes)
	
for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next

	
dim sFormArray
sFormArray = split(sFormData, "-----------------------------")
	
dim sName
dim sValue
dim iLocStart
dim iLocEnd
	
dim sForm()
	
dim sCSVLines
dim sCSVSingleLine
dim sCSVData
dim iLineCount

for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next
	

'Loop through the array of form values to parse out the data.
for iCount = 0 to ubound(sFormArray)
	
	'Response.Write(sFormArray(iCount) & "<p><br>")
	if instr(1, sFormArray(iCount), "name=") > 0 then
		'Response.Write(mid(sFormArray(iCount), instr(1, sFormArray(iCount), "name=")) & "<br>")
		iLocStart = instr(1, sFormArray(iCount), "name=""") + 6
		iLocEnd = instr(iLocStart, sFormArray(iCount), """")
		sName = mid(sFormArray(iCount), iLocStart, iLocEnd - iLocStart)
			
		'Find the CSV file within the data
		if sName = "AddUserCSV" then 
			
			'+28 accounts for mime type text and two CR/LF pairs.
			sValue = mid(sFormArray(iCount), instr(1, sFormArray(iCount), "Content-Type: ") + 14)
			'response.write("X" & sValue & "X, " & instr(1, sValue, vbCrLf) & "<br>")
			sValue = mid(sValue, instr(1, sValue, vbCrLf) + 4)
			sValue = left(sValue, len(sValue) - 2)
			'Response.Write("'" & sName & "' : '" & sValue & "'<p>")
				
			sCSVLines = split(sValue, vbCrLf)
				
			redim sCSVData(4, ubound(sCSVLines))
				
			for iLineCount = 0 to ubound(sCSVLines)

				sCSVSingleLine = split(sCSVLines(iLineCount), ",")

				'response.write(uBound(sCSVSingleLine))				

				if ubound(sCSVSingleLine) = 2 then
					
				sCSVData(0, iLineCount) = trim(sCSVSingleLine(0))
				sCSVData(1, iLineCount) = trim(sCSVSingleLine(1))
				sCSVData(2, iLineCount) = trim(sCSVSingleLine(2))
				'sCSVData(3, iLineCount) = trim(sCSVSingleLine(3))
					
				'Response.Write(sCSVData(0, iLineCount) & " " & sCSVData(1, iLineCount) & ": " & sCSVData(2, iLineCount) & "<p>")
				
				else
					
					'We'll get here if we get some line in our CSV that's not in
					'the correct format.  Rather than bother with removing just
					'that line from the array, we'll just make a blank entry that
					'we can ignore later.
					sCSVData(0, iLineCount) = ""
					sCSVData(1, iLineCount) = ""
					sCSVData(2, iLineCount) = ""
					'sCSVData(3, iLineCount) = ""
				
				end if
				
			next 
			
		else

			sValue = trim(mid(sFormArray(iCount), iLocEnd + 1))
			sValue = mid(left(sValue, len(sValue) - 2), 5)
			'Response.Write("'" & sName & "' : '" & sValue & "' : " & len(sValue) & "<p>")
			if sName = "CourseId" then
				iCourseId = ScrubForSql(sValue)
			elseif sName = "ExpirationDate" then
				dExpDate = ScrubForSql(sValue)
			elseif sName = "CompletionDate" then
				dCompletionDate = ScrubForSql(sValue)
			end if 
			redim preserve sForm(2, iCount + 1)
			sForm(0, iCount) = sName
			sForm(1, iCount) = sValue
			
		end if
						
	end if
				
next


dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 


'Verify the chosen course.  
dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

if iCourseId <> "" then
	iCourseId = oCourse.LoadCourseById(iCourseId)
	if iCourseId = 0 then

		HandleError("Failed to load the Course information.")
		Response.End
		
	elseif oCourse.CourseId < 0 and oCourse.CompanyId <> iCompanyId then
		
		'Response.write(oCourse.CompanyId & ", " & iCompanyId & "<br>")
		HandleError("The course specified is unavailable.")
		Response.End
		
	elseif oCourse.ProviderId <> session("UserProviderId") then
		
		HandleError("The course specified is unavailable.")
		Response.End
		
	end if
else
	
	HandleError("This operation requires a specified course.")
	Response.End
	
end if


%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Loan Officers</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Add an Course Loan Officer Listing</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
<%

'Initialize the associate object.
dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")


'dim iCount
dim sFirstName
dim sLastName
dim sSsn
dim bErrors 
dim bErrorsInList
dim bBlankSkip

response.write("<ul>")

for iCount = 0 to ubound(sCSVData, 2)

	'Pull our values from the array
	sSsn = ScrubForSql(sCsvData(2, iCount))
	sFirstName = ScrubForSql(sCsvData(0, iCount))
	sLastName = ScrubForSql(sCsvData(1, iCount))
	
	'Check if this is a black array entry
	if sSsn = "" and sFirstName = "" and sLastName = "" then
		
		'Set a boolean so we know to skip the rest of the operations for this
		'time through the loop.
		bBlankSkip = true
		'response.write("Y")
		
	end if 
	
	'Verify that we got at least a SSN and Last Name, which we'll need to look up the officer.
	if sSsn = "" and sLastName = "" and not bBlankSkip then
		
		bErrors = true
		Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): No SSN or Last Name specified.<br>")
		
	end if 
	
	'Load the officer so that we can add the course record.
	if not bErrors and not bBlankSkip then
	
		if oAssociate.LoadAssociateByLastNameSsn(enDeCrypt(sSsn,application("RC4Pass")), sLastName, session("UserCompanyId")) <> 0 then
		
				if oAssociate.AddCourse(iCourseId, dExpDate, dCompletionDate) = 0 then
					
					bErrors = true
					Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Could not add the course record.<br>")
			
				else 
	
					Response.write("<li>Added " & sFirstname & " " & sLastName & "<br>")


				end if 
		
		else
		
			bErrors = true
			Response.Write("<li>Entry " & iCount + 1 & " (" & sFirstName & " " & sLastName & "): Could not load the Officer information.<br>")
		
		end if 
	
	end if 
	
	
	if bErrors then
		bErrorsInList = true
	end if 
	
	bErrors = false
	bBlankSkip = false
	
next 

'Clean up
set oAssociate = nothing

if not bErrorsInList then

	Response.Write("</ul><p>Thank you.  The loan officer list was processed successfully.")

end if 
%>
		</td>
	</tr>
</table>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>


<%

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
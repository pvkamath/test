<% option explicit %>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----------------->

<%

' Template Constants -- Modify as needed per each page:
const USES_FORM_VALIDATION = False			' Template Constant
const SHOW_PAGE_BACKGROUND = True			' Template Constant
const TRIM_PAGE_MARGINS = True				' Template Constant
const SHOW_MENUS = False					' Template Constant
const SIDE_MENU_SELECTED = ""				' Template Constant
dim sPgeTitle								' Template Variable
	
' Set Page Title:
sPgeTitle = "Compliance Management Solutions"
	
	
'Make sure we were passed a candidate ID so we have something useful to do with this page.
dim oCompany
dim iCompanyId
iCompanyId = session("UserCompanyId")


set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(iCompanyId) = 0 then

	Response.Write("Error: A valid Company ID is required to load this page.<br>" & vbCrLf)
	Response.End
	
end if


'Make sure we were passed a valid AssociateId
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
iAssociateId = ScrubForSql(request("id"))

if oAssociate.LoadAssociateById(iAssociateID) = 0 then

	AlertError("This page requires a valid Officer ID.")
	Response.End

end if 


'Configure the administration submenu options
bAdminSubmenu = false
'sAdminSubmenuType = "BROKERS"


	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>

<%

dim iCourseID 'as integer
dim oCourseObj, oUserObj 'as object
dim rs 'as object
dim sBackgroundImage, sStampImage, sSignatureImage 'as string
dim sTextBlock1, sTextBlock2 'as string
dim sSignerName, sSignerPosition 'as string
dim iCertificateID 'as integer
dim oPDF 'as object
dim oDoc 'as object
dim oPage 'as object
dim oDest 'as object
dim oTimesFont, oTimesFontBold 'as object
dim oCanvas 'as object
dim oImage 'as object
dim oParam 'as object
dim oTable 'as object
dim sTempText 'as string
dim sUserText 'as string
dim bCertificateReleased 'as boolean
dim iUserCourseID 'as integer
dim iUserID 'as integer
dim sName 'as string
dim sCompanyName 'as string
dim sCompanyNameOnCert 'as string
dim sCompletionDate 'as string
dim sCertificateExpirationDate 'as string
dim bSuccess 'as boolean
dim iXCord, iYCord 'as integer
dim iStateID 'as integer
dim sLicenseNo 'as string


set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

iUserCourseId = ScrubForSql(request("UserCourseId")
iUserId = iAssociateId



'Get User Info
set rs = oAssociate.GetUser()

if not rs.eof then
	if trim(rs("MidInitial")) <> "" then
		sName = rs("FirstName") & " " & rs("MidInitial") & ". " & rs("LastName")
	else
		sName = rs("FirstName") & " " & rs("LastName")
	end if		
	
	sCompanyName = rs("CompanyName")
	if isnull(sCompanyName) then
		sCompanyName = ""
	end if	
		
	sCompanyNameOnCert = rs("CertCompanyName")
	if isnull(sCompanyNameOnCert) then
		sCompanyNameOnCert = ""
	end if	
	
	sUserText = sName
	
	'Add company info
	if trim(sCompanyNameOnCert) <> "" then
		sUserText = sUserText & " of " & sCompanyNameOnCert	
	elseif trim(sCompanyName) <> "" then
		sUserText = sUserText & " of " & sCompanyName
	end if
	
	sLicenseNo = rs("LicenseNo")	
else
	set rs = nothing
	response.redirect("/error.asp?message=The User Info. could not be retrieved.")
end if


'Get Course Info
set rs = oAssociate.GetTpCourse(iUserCourseId)

iCourseID = rs("CourseID")
sCompletionDate = rs("CompletionDate")
sCertificateExpirationDate = rs("CertificateExpirationDate")
bCertificateReleased = rs("CertificateReleased")

if clng(iCourseID) = 283 then
	set rs = nothing
	response.redirect("/error.asp?message=Please contact TrainingPro at 877-878-3600 for Utah State required certifcate.")
end if



'make sure the certificate has been released
if not bCertificateReleased then
	set rs = nothing
	response.redirect("/error.asp?message=The Certificate has not been released.")
end if



'Get the StateId for the course.
if oCourse.LoadCourseById(iCourseId) = 0 then
	response.redirect("/admin/error.asp?message=The Certifacte could not be retrieved.")
else
	iStateId = oCourse.StateId
end if 


'get certificate info
set rs = oCourse.GetCertificate()

if not rs.eof then
	iCertificateID = rs("CertificateID")
	sTextBlock1 = trim(rs("TextBlock1"))
	sTextBlock2 = trim(rs("TextBlock2"))
	sSignerName = trim(rs("SignerName"))
	sSignerPosition = trim(rs("SignerPosition"))
	sBackgroundImage = trim(rs("BackgroundImage"))	
	sStampImage = trim(rs("StampImage"))
	sSignatureImage = trim(rs("SignatureImage"))
else
	set rs = nothing
	set oCourse = nothing
	response.redirect("/error.asp?message=The Certifacte could not be retrieved.")
end if


set rs = nothing
set oCourse = nothing
set oAssociate = nothing



'Create PDF
Set oPDF = Server.CreateObject("Persits.PDFManager")
oPDF.RegKey = "IWez17RlvL6PIQc213KacCXF1eD2xzmp+C8FGLDD04ptU96y5Mi8KtfWC9XKP4Nl6qpjPqlOSwG3"
Set oDoc = oPDF.CreateDocument

'Make the Doc Print Only
oDoc.Encrypt "", "", 40, pdfFull And (Not pdfModify) And (Not pdfCopy) And (Not pdfExtract) And (Not pdfAssemble) And (not pdfAnnotations) And (not pdfForm)

Set oPage = oDoc.Pages.Add(792,612)

Set oDest = oDoc.CreateDest(oPage, "Fit=True")
oDoc.OpenAction = oDest

Set oTimesFont = oDoc.Fonts("Times-Roman")
Set oTimesFontBold = oDoc.Fonts("Times-Bold")
set oCanvas = oPage.Canvas

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_logo.gif") )
Set oParam = oPdf.CreateParam

oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 525
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam

Set oImage = oDoc.OpenImage( Server.MapPath("\media\images\certificate_header_1.gif") )
oParam("x") = (oPage.Width - oImage.Width * .75) / 2
oParam("y") = 485
oParam("ScaleX") = .75
oParam("ScaleY") = .75
oPage.Canvas.DrawImage oImage, oParam


if sBackgroundImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sBackgroundImage) )
	oParam("x") = (oPage.Width - oImage.Width) / 2
	oParam("y") = (477 - 275) + ((275 - oImage.Height) / 2)
	oParam("ScaleX") = oImage.ResolutionX / 72
	oParam("ScaleY") = oImage.ResolutionY / 72
	oPage.Background.DrawImage oImage, oParam
end if

set oTable = oDoc.CreateTable("rows=3; cols=2; cellspacing=1; cellpadding=2; height=465; width=680; border=1;bordercolor=white;")
oTable.Font = oTimesFont

With oTable.Rows(1)
	.Height = "275"
	.Cells(1).ColSpan = 2
	.Cells(1).AddText sTextBlock1 & " <b>" & sUserText & "</b> " & FormatTextForHTML(sTextBlock2), "alignment=left;color=black;size=14;html=true;"
end with

With oTable.Rows(2)
	.Height = "120"
	.Cells(1).width = 400
	.Cells(2).width = 280		

if sStampImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sStampimage) )
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(1).Canvas.DrawImage oImage, oParam
else
	.Cells(1).AddText "", "alignment=left;color=black;html=true;"
end if

if sSignatureImage <> "" then
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & sSignatureImage) )
else
	Set oImage = oDoc.OpenImage( Server.MapPath(Application("sDynCertificateImageFolder") & Application("DefaultSignatureImage")) )
end if
	oParam("x") = 1
	oParam("y") = 1
	oParam("ScaleX") = .75
	oParam("ScaleY") = .75
	oParam("expand") = true
	.Cells(2).Canvas.DrawImage oImage, oParam
end with

With oTable.Rows(3)
	.Height = "70"
	.Cells(1).width = 400	
	.Cells(2).width = 280		

	if cint(iStateID) = 38 then 'Special code for Oregon
		.Cells(1).AddText "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br><b>" & "Authentication Number: 1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid & "</b><br><b>This certification is valid for the two (2) year period following<br>your last License Renewal</b>", "alignment=left;color=black;size=12;html=true;"
	elseif cint(iCourseID) = application("LACourseID1") or cint(iCourseID) = application("LACourseID2") then 'louisiana courses
		.Cells(1).AddText "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br><b>" & "Authentication Number: 1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid & "</b>", "alignment=left;color=black;size=12;html=true;"	
	elseif cint(iCourseID) = application("Utah14hrCourseID") and trim(sLicenseNo) <> "" then 'Utah 14hr course
		.Cells(1).AddText "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br><b>License Number: " & sLicenseNo & "</b><br><b>" & "Authentication Number: 1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid & "</b><br><b>This certification is valid for Renewal Period Ending: " & formatdatetime(sCertificateExpirationDate,2) & "</b>", "alignment=left;color=black;size=12;html=true;"						
	else
		.Cells(1).AddText "<b>Issue Date: " & formatdatetime(sCompletionDate,2) & "</b><br><b>" & "Authentication Number: 1000DOI" & month(now()) & day(now()) & year(now()) & "UD" & iUserID & "CCD" & iCertificateid & "</b><br><b>This certification is valid for Renewal Period Ending: " & formatdatetime(sCertificateExpirationDate,2) & "</b>", "alignment=left;color=black;size=12;html=true;"	
	end if
	
	.Cells(2).AddText "<font color=""#295385"" style=""font-size:16pt;"" face=""Verdana, Arial, helvetica""><b>" & sSignerName & "</b></font><br><b>" & sSignerPosition & "</b>", "alignment=right;color=black;size=12;html=true;"
end with

oCanvas.DrawTable oTable, "x=54;y=480"


' Send directly to browser, do not save a temporary copy on disk
oDoc.SaveHttp "attachment;filename=certificate.pdf"

set oTable = nothing
set oPDF = nothing
set oDoc = nothing
set oPage = nothing
set oDest = nothing
set oTimesFont = nothing
Set oTimesFontBold = nothing
set oImage = nothing
%>

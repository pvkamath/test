<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Note.Class.asp" ----------------------------->
<!-- #include virtual="/includes/Course.Class.asp" --------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Profile"
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Declare the hierarchy objects
dim oCompany
dim oBranch
dim oCompanyL2
dim oCompanyL3


'Declare and initialize the Associate object
dim oAssociate
set oAssociate = new Associate
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")

dim iAssociateId
dim bCheckIsThisAssociateAdmin
dim bCheckisThisAssociateViewer
iAssociateId = ScrubForSql(request("id"))


'Check that we were passed a valid Associate ID to load.
if oAssociate.LoadAssociateById(iAssociateID) = 0 then
	AlertError("This page requires a valid Officer ID.")
	Response.End
end if 
	

'Check that we have viewer or admin access to this officer
if CheckIsThisAssociateAdmin(iAssociateId) then
	bCheckIsThisAssociateAdmin = true	
elseif CheckIsThisAssociateViewer(iAssociateId) then
	bCheckIsThisAssociateViewer = true
else
	AlertError("Unable to load the passed Officer ID.")
	Reponse.End
end if 


'Initialize associate docs object
dim oAssociateDoc 
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")



'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "OFFICERS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmLicenseDelete(p_iLicenseId,p_sLicenseNum)
{
	if (confirm("Are you sure you wish to delete the following License:\n  " + p_sLicenseNum + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletelicense.asp?id=' + p_iLicenseId;
}
function ConfirmOfficerDelete(p_iCandidateId,p_sCandidateName)
{
	if (confirm("Are you sure you wish to delete the following Officer:\n  " + p_sCandidateName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficer.asp?id=' + p_iCandidateId;
}
function ConfirmNoteDelete(p_iNoteId)
{
	if (confirm("Are you sure you wish to delete this Note?  All information will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletenote.asp?id=' + p_iNoteId;
}
function ConfirmCourseAssocDelete(p_iAssocId, p_iOfficerId)
{
	if (confirm("Are you sure you wish to delete this Course record?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deleteofficercourse.asp?aid=' + p_iAssocId + '&id=' + p_iOfficerId;
}
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}

</script>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Officer Profile -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <% = oAssociate.FirstName & " " & oAssociate.LastName %></td>
				</tr>		

						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td nowrap align="left" valign="center"><a href="modofficer.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"> &nbsp;Edit Officer</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="javascript:ConfirmOfficerDelete(<% = oAssociate.UserId %>,'<% = oAssociate.FirstName & " " & oAssociate.LastName %>')" class="nav"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" align="top" border="0" title="Delete This Officer" alt="Delete This Officer"> Delete Officer</a></td>
										<td align="left" valign="center"></td>
										<td nowrap align="left" valign="center"><a href="emailofficer.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnMail.gif" align="top" border="0" title="Email Notices" alt="Email Notices"> &nbsp;Email Notices</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0">
										<% if bCheckIsThisAssociateAdmin then %>
										<tr>
											<td class="newstitle" nowrap>SSN: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Ssn %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>Date Of Birth: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.DateOfBirth %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Drivers License State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = GetStateName(oAssociate.DriversLicenseStateId, 0) %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Drivers License Number: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.DriversLicenseNo %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Hire Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HireDate %></td>
										</tr>
										<% if oAssociate.Inactive then %>
										<tr>
											<td class="newstitle" nowrap>Inactive Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.TerminationDate %></td>
										</tr>
										<% end if %>
										<tr>
											<td class="newstitle" nowrap>Employee ID: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.EmployeeId %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Title: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Title %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Department: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Department %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Email %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone %> <% if oAssociate.PhoneExt <> "" then %> Ext. <% = oAssociate.PhoneExt %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone 2: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone2 %> <% if oAssociate.Phone2Ext <> "" then %> Ext. <% = oAssociate.Phone2Ext %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Phone 3: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Phone3 %> <% if oAssociate.Phone3Ext <> "" then %> Ext. <% = oAssociate.Phone3Ext %><% end if %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Fax %></td>
										</tr>
										<% if 1 = 2 then %>
										<tr>
											<td class="newstitle" nowrap>Cell Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.CellPhone %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Home Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HomePhone %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Home Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.HomeEmail %></td>
										</tr>
										<% end if %>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
									</table>
											</td>
											<td valign="top" width="50%">
									<table border="0" cellpadding="5" cellspacing="0" valign="top">
										<tr>
											<td valign="top" class="newstitle" nowrap>Manager: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Manager %></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Out of State<br> Origination: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td valign="top">
											<%
											if oAssociate.OutOfStateOrig then 
												Response.Write("Yes")
											else
												Response.Write("No")
											end if
											%>
											</td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap>Work Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><%
											'We're going to lookup the address of the business unit to which the officer belongs,
											'starting with the Branch and moving up to the Company.
											if oAssociate.BranchId <> "" then
											
												set oBranch = new Branch
												oBranch.ConnectionString = application("sDataSourceName")
												oBranch.VocalErrors = application("bVocalErrors")
												
												if oBranch.LoadBranchById(oAssociate.BranchId) <> 0 then
												
													Response.Write(oBranch.Name & "<br>")
													if not oBranch.Address2 = "" then 
														Response.Write(oBranch.Address2 & "<br>")
													end if
													Response.Write(oBranch.City & "<br>")
													Response.Write(GetStateName(oBranch.StateId, 0) & "<br>")
													Response.Write(oBranch.Zipcode)
													if oBranch.ZipcodeExt <> "" then
														Response.Write(" - " & oBranch.ZipcodeExt)
													end if
												
												end if
				
											elseif oAssociate.CompanyL3Id <> "" then
											
												set oCompanyL3 = new CompanyL3
												oCompanyL3.ConnectionString = application("sDataSourceName")
												oCompanyL3.VocalErrors = application("bVocalErrors")
												
												if oCompanyL3.LoadCompanyById(oAssociate.CompanyL3Id) <> 0 then
												
													Response.Write(oCompanyL3.Name & "<br>")
													if not oCompanyL3.Address2 = "" then 
														Response.Write(oCompanyL3.Address2 & "<br>")
													end if
													Response.Write(oCompanyL3.City & "<br>")
													Response.Write(GetStateName(oCompanyL3.StateId, 0) & "<br>")
													Response.Write(oCompanyL3.Zipcode)
													if oCompanyL3.ZipcodeExt <> "" then
														Response.Write(" - " & oCompanyL3.ZipcodeExt)
													end if												
													
												end if
											
											elseif oAssociate.CompanyL2Id <> "" then
											
												set oCompanyL2 = new CompanyL2
												oCompanyL2.ConnectionString = application("sDataSourceName")
												oCompanyL2.VocalErrors = application("bVocalErrors")
												
												if oCompanyL2.LoadCompanyById(oAssociate.CompanyL2Id) <> 0 then
												
													Response.Write(oCompanyL2.Name & "<br>")
													if not oCompanyL2.Address2 = "" then 
														Response.Write(oCompanyL2.Address2 & "<br>")
													end if
													Response.Write(oCompanyL2.City & "<br>")
													Response.Write(GetStateName(oCompanyL2.StateId, 0) & "<br>")
													Response.Write(oCompanyL2.Zipcode)
													if oCompanyL2.ZipcodeExt <> "" then
														Response.Write(" - " & oCompanyL2.ZipcodeExt)
													end if
													
												end if
											
											else
											
												set oCompany = new Company
												oCompany.ConnectionString = application("sDataSourceName")
												oCompany.VocalErrors = application("bVocalErrors")
												
												if oCompany.LoadCompanyById(session("UserCompanyId")) <> 0 then
												
													Response.Write(oCompany.Name & "<br>")
													if not oCompany.Address2 = "" then 
														Response.Write(oCompany.Address2 & "<br>")
													end if
													Response.Write(oCompany.City & "<br>")
													Response.Write(GetStateName(oCompany.StateId, 0) & "<br>")
													Response.Write(oCompany.Zipcode)
													if oCompany.ZipcodeExt <> "" then
														Response.Write(" - " & oCompany.ZipcodeExt)
													end if
													
												end if
											
											end if								 
											 %>
											 </td>
										</tr>
										<%
										if oAssociate.Address <> "" then 
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Specific Address: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oAssociate.Address %><br>
											<% if not oAssociate.Address2 = "" then %><% = oAssociate.Address2 %><br><% end if %>
											<% = oAssociate.City %><br>
											<% = GetStateName(oAssociate.StateId, 0) %><br>
											<% = oAssociate.Zipcode %><% if oAssociate.ZipcodeExt <> "" then %> - <% = oAssociate.ZipcodeExt %><% end if %>
											</td>
										</tr>
										<% 
										end if 
										%>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										oAssociateDoc.CompanyId = session("UserCompanyId")
										oAssociateDoc.OwnerId = oAssociate.UserId
										oAssociateDoc.OwnerTypeId = 1
										set oRs = oAssociateDoc.SearchDocs
										
										'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<% if bCheckIsThisAssociateAdmin then %>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>"><% = oAssociateDoc.DocName %></a><br>													
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsThisAssociateAdmin then %>
											<a href="moddocument.asp?otid=1&oid=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											<% end if %>
											</td>
										</tr>											
									</table>
										</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						
						
			
			<!-- Officer Licenses -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Licenses</td>
				</tr>

						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modlicense.asp?oid=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New License" alt="Add New License"> New License</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
								
							<tr>
								<td class="bckRight">
									<table border=0 cellpadding=15 cellspacing=0 width="100%">
										<tr>
											<td>
<%
dim oLicense
dim oLicRs
set oLicense = new License

oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerId = oAssociate.UserId
oLicense.OwnerTypeId = 1


set oRs = oLicense.SearchLicensesByState()

if not (oRs.EOF and oRs.BOF) then
	do while not oRs.EOF
	
		Response.Write("<b>(" & oRs("LicenseCount") & ") <a href=""officerdetail.asp?id=" & oAssociate.UserId & "&ls=" & oRs("LicenseStateId") & """>" & oLicense.LookupState(oRs("LicenseStateId")))
		if oRs("LicenseCount") > 1 then 
			Response.Write(" Licenses ")
		else
			Response.Write(" License ")
		end if
		Response.Write("</a></b><br>")
		
		'Get the full list if we've selected this state.
		if request("ls") = cstr(oRs("LicenseStateId")) then
				
			oLicense.LicenseStateId = oRs("LicenseStateId")
			set oLicRs = oLicense.SearchLicenses()
			
			%>
			<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
					<% if bCheckIsThisAssociateAdmin then %>
					<td></td>
					<td></td>
					<% end if %>
					<td><b>License Number</b></td>
					<td><b>License Type</b></td>
					<td><b>License Status</b></td>
					<td><b>Expiration Date</b></td>
				</tr>
			<%
				do while not oLicRs.EOF

					if oLicense.LoadLicenseById(oLicRs("LicenseId")) <> 0 then

						if sColor = "" then
							sColor = " bgcolor=""#ffffff"""
						else
							sColor = ""
						end if


						Response.Write("<tr>" & vbCrLf)
						if bCheckIsThisAssociateAdmin then
							Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
							Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
						end if 
						Response.Write("<td " & sColor & ">" & oLicense.LicenseNum & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseType & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LookupLicenseStatus(oLicense.LicenseStatusId) & "</td>" & vbCrLf)
						Response.Write("<td " & sColor & ">" & oLicense.LicenseExpDate)
						if oLicense.LicenseExpDate < date() + 120 and oLicense.LicenseExpDate > date() then
							Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicense.LicenseExpDate - date(), 0) & " Days</font>)</b>")
						end if
						Response.Write("</td>" & vbCrLf)
						Response.Write("</tr>" & vbCrLf)
							
					end if 

					oLicRs.MoveNext

				loop

			%>
			</table>
			<%
			
		end if

		oRs.MoveNext
	
	loop

else
	
	Response.Write("No Licenses found.")
	
end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<%
'						end if 
'						set oPreference = nothing
						%>
						
						
			<!-- Officer Notes -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Notes</td>
				</tr>
						<% if bCheckIsThisAssociateAdmin then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td><a href="<% = application("URL") %>/officers/modnote.asp?oid=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Note" alt="Add New Note"> New Note</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
							<tr>
								<td class="bckRight">
									<table width="100%" border="0" cellpadding="15" cellspacing="0">
										<tr>
											<td>
<%
dim oNote
set oNote = new Note

oNote.ConnectionString = application("sDataSourceName")
oNote.VocalErrors = application("bVocalErrors")
oNote.OwnerId = oAssociate.UserId
oNote.OwnerTypeId = 1

set oRs = oNote.SearchNotes()

if not oRs.State = 0 then
if not (oRs.EOF and oRs.BOF) then
sColor = ""
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if bCheckIsThisAssociateAdmin then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Date</b></td>
		<td></td>
		<td width="100"><b>Poster</b></td>
		<td></td>
		<td width="400"><b>Note Contents</b></td>
	</tr>
	<tr>
<%
	do while not oRs.EOF

		if oNote.LoadNoteById(oRs("NoteId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if


			Response.Write("<tr>" & vbCrLf)
			if bCheckIsThisAssociateAdmin and oNote.UserId = session("User_Id") then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modnote.asp?nid=" & oNote.NoteId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Note"" alt=""Edit This Note""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmNoteDelete(" & oNote.NoteId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Note"" alt=""Delete This Note""></a></td>" & vbCrLf)
			elseif bCheckIsThisAssociateAdmin then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td valign=""top"" " & sColor & ">" & month(oNote.NoteDate) & "/" & day(oNote.NoteDate) & "/" & year(oNote.NoteDate) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.LookupUserName(oNote.UserId) & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & "></td>")
			Response.Write("<td valign=""top"" " & sColor & ">" & oNote.NoteText & "</td>" & vbCrLf)
			Response.Write("</tr>" & vbCrLf)
				
		end if 

		oRs.MoveNext

	loop

%>
</table>
<%
else
	
	Response.Write("No Notes found.")
	
end if 
else

	Response.Write("No Notes found.")

end if 
%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>





			<!-- Officer Course History -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> Officer Education History</td>
				</tr>
				<% if bCheckIsThisAssociateAdmin then %>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td><a href="<% = application("URL") %>/officers/modofficercourse.asp?id=<% = oAssociate.UserId %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Course" alt="Add New Course"> New Course</a></td>
							</tr>
						</table>
					</td>
				</tr>
				<% end if %>
				<tr>
					<td class="bckRight">
						<table width="100%" border="0" cellpadding="15" cellspacing="0">
							<tr>
								<td>
<%
dim oRs
dim oCourseInfoRs
dim oCourse

set oCourse = new Course
oCourse.VocalErrors = application("bVocalErrors")
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")

set oRs = oAssociate.GetCourses

'dim sColor
sColor = ""

if oRs.State <> 0 then

	if not oRs.EOF then
		bHasContent = true
	%>
									<table width="100%" cellpadding="5" cellspacing="0" border="0">
										<tr>
											<% if bCheckIsThisAssociateAdmin then %>
											<td width="25"></td>
											<td></td>
											<% end if %>
											<td><b>Course</b></td>
											<td><b>Provider</b></td>
											<td><b>Date</b></td>
											<td><b>Renewal Date</b></td>
											<td><b>Progress</b></td>
											<td><b>Certificate</b></td>
										</tr>
	<%
	else
	%>
	No Courses found.
	<%
	end if 

	do while not oRs.EOF


		if oCourse.LoadCourseById(oRs("CourseId")) <> 0 then

			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if

			Response.Write("<tr>" & vbCrLf)
			if bCheckIsThisAssociateAdmin and oCourse.CourseId < 0 then
				Response.Write("<td valign=""top"" " & sColor & "><a href=""" & application("URL") & "/officers/modofficercourse.asp?id=" & oAssociate.UserId & "&aid=" & oRs("Id") & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Education Record"" alt=""Edit This Education Record ""></a></td>" & vbCrLf)
				Response.Write("<td valign=""top"" " & sColor & "><a href=""javascript:ConfirmCourseAssocDelete(" & oRs("Id") & ", " & oAssociate.UserId & ")""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Education Record"" alt=""Delete This Education Record""></a></td>" & vbCrLf)
			elseif bCheckIsThisAssociateAdmin then
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
				Response.Write("<td " & sColor & "></td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oCourse.Name & "</td>" & vbCrLf)
			if oCourse.ProviderId <> "" then 
				Response.Write("<td " & sColor & " nowrap>" & oCourse.LookupProvider(oCourse.ProviderId) & "</td>" & vbCrLf)
			else
				Response.Write("<td " & sColor & " nowrap>TrainingPro</td>" & vbCrLf)
			end if 
			Response.Write("<td " & sColor & ">" & oRs("CompletionDate") & "</td>" & vbCrLf)
			'Response.Write("<td " & sColor & ">" & oCourse.ContEdHours & "</td>" & vbCrLf)
			Response.Write("<td " & sColor & " nowrap>" & oRs("CertificateExpirationDate"))
			if oRs("CertificateExpirationDate") < date() + 120 and oRs("CertificateExpirationDate") > date() then
				Response.Write("   <b>(<font color=""#cc0000"">" & round(oRs("CertificateExpirationDate") - date(), 0) & " Days</font>)</b>")
			end if			
			Response.Write("</td>" & vbCrLf)
			Response.Write("<td " & sColor & ">")
			if oCourse.CourseId > 0 and oRs("Completed") then
				Response.Write("Completed")
			elseif (not oRs("Completed")) and (oCourse.CourseId > 0) then
				'Response.Write(oAssociate.LookupTpUserId(oAssociate.Email) & "," & oCourse.CourseId & " - ")
				Response.Write(oAssociate.GetTpCourseProgress(oCourse.CourseId, oAssociate.LookupTpUserId(oAssociate.Email)) & "%")
			else
				Response.Write("Completed")
			end if
			Response.Write("</td>" & vbCrLf)
			if oCourse.CourseId > 0 and oRs("CertificateReleased") then
				'set oCourseInfoRs = oAssociate.GetTpCourse(oRs("Id"))
				'if not oCourseInfoRs.EOF then
'					if oCourseInfoRs("CertificateReleased") then
				Response.Write("<td " & sColor & " align=""center""><a href=""/courses/viewcertificate.asp?id=" & oAssociate.UserId & "&usercourseid=" & oRs("Id") & """>View</a></td>")
			else
				Response.Write("<td " & sColor & "></td>")
			end if
'				else
'					Response.Write("<td " & sColor & "></td>")
'				end if 
'			else
'				Response.Write("<td " & sColor & "></td>")
'			end if 
			Response.Write("</tr>" & vbCrLf)
		
		end if 

		oRs.MoveNext

	loop
	
	if bHasContent = true then
		%></table><%
	end if 

else

end if
%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>



							
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

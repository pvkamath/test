<%
'option explicit
server.ScriptTimeout = 1000
response.buffer = false
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Provider.Class.asp" -->
<!-- #include virtual = "/includes/rc4.asp" --> 

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

dim oRs
dim oAssociateSearch
dim oAssociate
dim oLicense
dim oCurLicense
dim sOffLicText

dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
dim oBranch

dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access

dim bLicenseSearch
dim bOfficerSearch
dim iShowCompleted

dim objConn
dim oBranchesRS
dim oCompany

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCurLicense = new License
oCurLicense.ConnectionString = application("sDataSourceName")
oCurLicense.VocalErrors = application("bVocalErrors")

set oAssociateSearch = new Associate
oAssociateSearch.ConnectionString = application("sDataSourceName")
oAssociateSearch.TpConnectionString = application("sTpDataSourceName")
oAssociateSearch.VocalErrors = application("bVocalErrors")

oAssociateSearch.CompanyId = session("UserCompanyId")
oAssociateSearch.UserStatus = 1

if (trim(session("SearchReportLastName")) <> "") then
	oAssociateSearch.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociateSearch.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportHireDateFrom")) <> "") then
	oAssociateSearch.SearchHireDateFrom = session("SearchReportHireDateFrom")
end if

if (trim(session("SearchReportHireDateTo")) <> "") then
	oAssociateSearch.SearchHireDateTo = session("SearchReportHireDateTo")
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociateSearch.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociateSearch.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociateSearch.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociateSearch.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociateSearch.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociateSearch.Inactive = session("SearchReportInactive")
end if

sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList

sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next

'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

'If Structure List set to All, populate structure lists with default values
if trim(sAssocIdList) = "" then
	sCompanyL2IdList = GetUserCompanyL2IdList()
	sCompanyL3IdList = GetUserCompanyL3IdList()
	sBranchIdList = GetUserBranchIdList()
end if

oAssociateSearch.SearchBranchIdList = sBranchIdList
if (oAssociateSearch.SearchBranchIdList = "") then
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociateSearch.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociateSearch.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociateSearch.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociateSearch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociateSearch.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociateSearch.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociateSearch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociateSearch.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociateSearch.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociateSearch.SearchCourseCreditHours = session("SearchReportCreditHours")
end if
	
if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	oAssociateSearch.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	oAssociateSearch.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociateSearch.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociateSearch.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociateSearch.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociateSearch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociateSearch.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociateSearch.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociateSearch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociateSearch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociateSearch.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if
	
if session("SearchReportShowOfficers") <> "" then
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if
if iShowCompleted = 1 then
	oAssociateSearch.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociateSearch.SearchCourseCompleted = false
else
	oAssociateSearch.SearchCourseCompleted = ""
end if

'Update the license object properties based on the search we just did.
'We'll use these properties later to search for the specific license records.
oLicense.LicenseStatusId = oAssociateSearch.SearchLicenseStatusId
if oLicense.LicenseStatusId = 0 then
	oLicense.LicenseStatusId = ""
end if
oLicense.SearchStatusIdList = oAssociateSearch.SearchLicenseStatusIdList
oLicense.LicenseNum = oAssociateSearch.SearchLicenseNumber
oLicense.SearchLicenseExpDateFrom = oAssociateSearch.SearchLicenseExpDateFrom
oLicense.SearchLicenseExpDateTo = oAssociateSearch.SearchLicenseExpDateTo
oLicense.SearchAppDeadline = oAssociateSearch.SearchAppDeadline
	
'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociateSearch.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
end if
	
'Response.ContentType = "application/vnd.ms-excel"
Response.ContentType = "application/download"
Response.AddHeader "Content-Disposition", "attachment; filename=" & "report.xls"	
'Response.AddHeader "Content-Disposition", "attachment; filename=" & oAssociateDoc.FileName

'Write opening XML statements
%>
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>13170</WindowHeight>
  <WindowWidth>18285</WindowWidth>
  <WindowTopX>690</WindowTopX>
  <WindowTopY>60</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <NumberFormat/>
  </Style>
  <Style ss:ID="s22">
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s23">
	<Font ss:Bold="1"/>
  </Style>
 </Styles>
 
<%
'Set Branch list to loop through branches (Each branch will be on a separate workbook)
if trim(sBranchIdList) = "" then
	sBranchIdList = GetUserBranchIdList
end if

set objConn = New HandyADO

sSQL = "SELECT BranchID FROM CompanyBranches " & _
	   "WHERE CompanyID = '" & session("UserCompanyId") & "' " & _
	   "AND NOT Deleted = '1' " 
bFirstBranch = true
aBranches = split(sBranchIdList, ", ")
for each iBranchId in aBranches
	if bFirstBranch then
		sSql = sSql & "AND ("
		bFirstBranch = false
	else
		sSql = sSql & "OR "
	end if
	sSql = sSql & "BranchID = '" & iBranchId & "' "
next	
if not bFirstBranch then
	sSql = sSql & ") "
end if

sSql = sSql & "ORDER BY Name, BranchNum "
set oBranchesRS = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

if not oBranchesRS.EOF then
	do while not oBranchesRS.EOF
		'load the branch
		if oBranch.LoadBranchByID(oBranchesRS("BranchID")) = 0 then
			'The load was unsuccessful.  End.
			Response.Write("Failed to load the passed Branch ID.")
			Response.End	
		end if
	
		oAssociateSearch.BranchID = oBranch.BranchID
	
		set oRs = oAssociateSearch.SearchAssociatesCoursesLicenses()

		'in case their are 2 branches named the same, append the branchID to the branch name
		'Excel does not allow worksheets to be named the same
		
		'Officer licenses XML
		sOffLicText = "<Worksheet ss:Name=""" & left(replace(replace(oBranch.Name,":"," "),"/","_"),25) & oBranch.BranchID & """><Table><Row>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
		sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
		sOffLicText = sOffLicText & "</Row>"

		if bOfficerSearch then 
			if not (oRs.BOF and oRs.EOF) then
				set oAssociate = new Associate
				oAssociate.ConnectionString = application("sDataSourceName")
				oAssociate.TpConnectionString = application("sTpDataSourceName")
				oAssociate.VocalErrors = true
		
				oAssociate.SearchCourseName = session("SearchReportCourseName")
				oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
				oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
				oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
				oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
				oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
				if iShowCompleted = 1 then
					oAssociate.SearchCourseCompleted = true
				elseif iShowCompleted = 2 then
					oAssociate.SearchCourseCompleted = false
				else
					oAssociate.SearchCourseCompleted = ""
				end if

				do while not oRs.EOF
					if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
						if bLicenseSearch then 			
							oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
							oLicense.OwnerId = oAssociate.UserId
							set oAssocRs = oLicense.SearchLicenses					
						
							do while not oAssocRs.EOF 
								if oCurLicense.LoadLicenseById(oAssocRs("LicenseId")) <> 0 then
									sOffLicText = sOffLicText & "<Row>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupAssociateNameById(oCurLicense.OwnerId) & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
									sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
									sOffLicText = sOffLicText & "</Row>"
								end if
								oAssocRs.MoveNext
							loop		
							set oAssocRs = nothing
						end if
					end if 
					oRs.MoveNext	
				loop	
				set oAssociate = nothing			
			end if
		end if 
	
		'Write out the report.
		sOffLicText = sOffLicText & "</Table></Worksheet>"
		Response.Write sOffLicText
	
		oBranchesRS.MoveNext
	loop
else
	response.write("<Worksheet ss:Name=""Spreadsheet 1""><Table><Row>")
	response.write("</Row></Table></Worksheet>")
end if
Response.Write "</Workbook>"

set objConn = nothing
set oBranchesRS = nothing
set oBranch = nothing
set oAssociateSearch = nothing
set oRs = nothing
%>
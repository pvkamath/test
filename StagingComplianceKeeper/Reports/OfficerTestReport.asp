<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/Test.Class.asp" --------------------------->

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
' Template Constants -- Modify as needed per each page:

const USES_FORM_VALIDATION = False			' Template Constant
const SHOW_PAGE_BACKGROUND = True			' Template Constant
const TRIM_PAGE_MARGINS = True			' Template Constant
const SHOW_MENUS = True					' Template Constant
const SIDE_MENU_SELECTED = ""				' Template Constant
const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
dim sPgeTitle							' Template Variable
dim iPage
dim oTest 'as object

' Set Page Title:
sPgeTitle = "Events Report"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"

set oTest = New Test
oTest.ConnectionString = application("sDataSourceName")

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">

	$(function() {
		$(".datepicker").datepicker({dateFormat: 'mm/dd/yy'});
	});
		
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
	function InitializePage() {
	}
//-->
</script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>	
	
<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10">Officer Test Report</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top" align="center">							
									<form name="frm1" id="frm1" action="/Reports/OfficerTestReportProc.asp" method="POST">
										<table cellpadding="5" cellspacing="0" border="0">
											<tr>
												<td class="newstitle" nowrap>Last Name: </td>
												<td nowrap>
													<input type="text" name="SearchName" id="SearchName" value="" maxlength="100" size="30">
												</td>	
												<td class="newstitle" nowrap>Test Type</td>	
                                                <td nowrap><% DisplayAssociateTestTypeDropdown "", 0  %></td>
											</tr>											
											<tr>
												<td class="newstitle" nowrap>Test Start Date (From): </td>
												<td nowrap>
													<input type="text" id="SearchTestDateFrom" name="SearchTestDateFrom" value="" size="10" maxlength="10" class="datepicker" readonly>
												</td>													

												<td class="newstitle" nowrap>Test End Date (To): </td>
												<td nowrap>
													<input type="text" id="SearchTestDateTo" name="SearchTestDateTo" value="" size="10" maxlength="10" class="datepicker" readonly>
												</td>												
											</tr>					
											<tr>
												<td valign="top" class="newstitle" nowrap>State: </td>
												<td nowrap>
													<% call DisplayStatesListBox("", 0, "SearchStateIDList", true) 'functions.asp %>
												</td>	
												<td  valign="top" class="newstitle" nowrap>Status: </td>
												<td nowrap>
													<% call DisplayTestStatusesDropDown("SearchStatusIDs", oTest, "", 0) 'function.asp %>
												</td>														
											</tr>				
											<tr><td colspan="4">&nbsp;</td></tr>	
											<tr>
												<td colspan="4" align="center">
													<br>
													<input type="image" src="/media/images/blue_bttn_search.gif" width="79" height="23" alt="Execute Search" border="0">
													<img src="/media/images/btn_clear.gif" width="79" height="23" alt="Clear Search" border="0" onclick="javascript:document.frm1.reset();">															
												</td>
											</tr>																												
										</table>
									</form>											
								</td>
							</tr>    
				   		</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
<!-- space-->
<table width="100%" border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
	</tr>
</table>
</td>
<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>

<%	
set oTest = nothing

	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

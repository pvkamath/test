<%
option explicit
%>

<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual = "/includes/Test.Class.asp" --------------------------->

<%
dim sSearchName 'as string
dim sSearchStateIDList 'as string
dim sSearchTestDateFrom, sSearchTestDateTo 'as string
dim sSearchStatusIDs 'as string
dim oTest 'as object
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList 'as string
dim sReportDetails 'as string
dim sSearchTestTypeID 'as string

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()	

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()

'Get Info
sSearchName =  GetFormElementAndScrub("SearchName")
sSearchStateIDList =  GetFormElementAndScrub("SearchStateIDList")
sSearchTestDateFrom =  GetFormElementAndScrub("SearchTestDateFrom")
sSearchTestDateTo =  GetFormElementAndScrub("SearchTestDateTo")
sSearchStatusIDs =  GetFormElementAndScrub("SearchStatusIDs")
sSearchTestTypeID = GetFormElementAndScrub("AssociateTestTypeID")

set oTest = New Test
oTest.ConnectionString = application("sDataSourceName")

oTest.SearchCompanyID = session("UserCompanyId") 
oTest.SearchName = sSearchName
oTest.SearchStateIDList = sSearchStateIDList
oTest.SearchTestDateFrom = sSearchTestDateFrom
oTest.SearchTestDateTo = sSearchTestDateTo
oTest.SearchStatusIDs = sSearchStatusIDs
oTest.SearchCompanyL2IdList = sCompanyL2IdList
oTest.SearchCompanyL3IdList = sCompanyL3IdList
oTest.SearchBranchIDList = sBranchIdList
oTest.SearchTestTypeID = sSearchTestTypeID

'Retrieve Report Details
sReportDetails = oTest.SearchTests()

set oTest = nothing

response.ContentType = "application/download"
response.AddHeader "Content-Disposition", "attachment; filename=" & "EventsReport.csv"	
response.write(sReportDetails)
%>
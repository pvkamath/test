<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/SavedReport.Class.asp" -------------------->
<!-- #include virtual = "/includes/Manager.Class.asp" ------------------------>
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

dim bCheckL2Access
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	bCheckL2Access = true
end if

dim bCheckL3Access
if CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	bCheckL3Access = true
end if

dim bCheckBranchAccess
if CheckIsBranchAdmin() or CheckIsBranchViewer() then
	bCheckBranchAccess = true
end if

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oAssociate
dim oCourse
dim oLicense
dim oCompany
dim oBranch 'Branch object
dim oSavedReport
dim oManager
dim oManagerRs

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1



set oSavedReport = new SavedReport
dim iSavedReportId 'Which report are we loading?
dim sSavedReportName 'What is this report named?
oSavedReport.ConnectionString = application("sDataSourceName")
oSavedReport.VocalErrors = application("bVocalErrors")
oSavedReport.CompanyId = session("UserCompanyId")

set oManager = new Manager
oManager.ConnectionString = application("sDataSourceName")
oManager.VocalErrors = application("bVocalErrors")
oManager.CompanyId = session("UserCompanyId")



dim iShowDeleted
iShowDeleted = ScrubForSql(request("ShowDeleted"))
if (iShowDeleted = "3") then
	iShowDeleted = 3
else
	iShowDeleted = 1
end if 



dim sStateIdList
sStateIdList = replace(request("StateIdList"), ", ", ",")

dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
'sBranchIdList = replace(request("BranchIdList"), ", ", ",")
'sBranchIdList = request("BranchIdList")
'sCompanyL2IdList = request("CompanyL2IdList")
'sCompanyL3IdList = request("CompanyL3IdList")


dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID


dim bLicenseSearch
dim bLicCompanyL2Search
dim bLicCompanyL3Search
dim bLicBranchSearch
dim bLicIssuedCompanyL2Search
dim bLicIssuedCompanyL3Search
dim bLicIssuedBranchSearch
dim bLicIssuedSearch
	
dim iShowInactive
dim iShowCompleted
dim iAppDeadline

dim bAdminSearch	
dim bManagerSearch
dim bCourseSearch
dim bOfficerSearch
dim bCompanySearch
dim bBranchSearch
dim bCompanyL2Search
dim bCompanyL3Search

dim bFirstNameSearch
dim bLastNameSearch
dim bSsnSearch
dim bEmployeeIdSearch
dim bPhoneSearch
dim bEmailSearch
dim bWorkAddressSearch
dim bWorkCitySearch
dim bWorkStateSearch
dim bWorkZipSearch
dim bSpecAddressSearch
dim bSpecCitySearch
dim bSpecStateSearch
dim bSpecZipSearch


dim oAdminRs






if ucase(sAction) = "NEW" then

	ClearReportSessionVars()
	
	session("SearchReportShowBranches") = true
	session("SearchReportShowCompanyL2s") = true
	session("SearchReportShowCompanyL3s") = true
	
	session("SearchReportShowLicBranches") = true
	session("SearchReportShowLicCompanyL2s") = true
	session("SearchReportShowLicCompanyL3s") = true
	
	Response.Redirect("branchlicreport.asp")

elseif request("SavedReportName") <> "" then

	'We are saving a report.  
	
	session("SearchReportSavedName") = ScrubForSql(request("SavedReportName"))
	oSavedReport.Name = ScrubForSql(request("SavedReportName"))
	oSavedReport.LastName = ScrubForSql(session("SearchReportLastName"))
	oSavedReport.Ssn = enDeCrypt(ScrubForSql(session("SearchReportSsn")),application("RC4Pass"))
	oSavedReport.StateIdList = ScrubForSql(session("SearchReportStateIdList"))
	oSavedReport.StructureIdList = ScrubForSql(session("SearchReportStructureIdList")) 'sAssocIdList 'ScrubForSql(session("SearchReportBranchIdList"))
	oSavedReport.ShowInactive = session("SearchReportInactive")
	oSavedReport.ShowCompleted = session("SearchReportCompleted")
	oSavedReport.Course = ScrubForSql(session("SearchReportCourseName"))
	oSavedReport.ProviderId = ScrubForSql(session("SearchReportProviderId"))
	oSavedReport.CreditHours = ScrubForSql(session("SearchReportCreditHours"))
	oSavedReport.CourseCompletionDateFrom = ScrubForSql(session("SearchReportCourseCompletionDateFrom"))
	oSavedReport.CourseCompletionDateTo = ScrubForSql(session("SearchReportCourseCompletionDateTo"))	
	oSavedReport.CourseExpFrom = ScrubForSql(session("SearchReportCourseExpDateFrom"))
	oSavedReport.CourseExpTo = ScrubForSql(session("SearchReportCourseExpDateTo"))
	oSavedReport.LicenseStatusId = ScrubForSql(session("SearchReportLicenseStatusId"))
	oSavedReport.LicenseStatusIdList = ScrubForSql(session("SearchReportLicenseStatusIdList"))
	oSavedReport.LicenseNumber = ScrubForSql(session("SearchReportLicenseNumber"))
	oSavedReport.LicExpFrom = ScrubForSql(session("SearchReportLicenseExpDateFrom"))
	oSavedReport.LicExpTo = ScrubForSql(session("SearchReportLicenseExpDateTo"))
	oSavedReport.AppDeadline = ScrubForSql(session("SearchReportAppDeadline"))
	oSavedReport.ShowLicenses = session("SearchReportShowLicenses")
	oSavedReport.ShowLicIssued = session("SearchReportShowLicIssued")
	oSavedReport.ShowCourses = session("SearchReportShowCourses")
	oSavedReport.ShowOfficers = session("SearchReportShowOfficers")
	oSavedReport.ShowCompanies = session("SearchReportShowCompany")
	oSavedReport.ShowBranches = session("SearchReportShowBranches")
    oSavedReport.ShowAdmins = session("SearchReportShowAdmins")
	oSavedReport.ShowCompanyL2s = session("SearchReportShowCompanyL2s")
	oSavedReport.ShowCompanyL3s = session("SearchReportShowCompanyL3s")
	oSavedReport.ShowLicBranches = session("SearchReportShowLicBranches")
	oSavedReport.ShowLicIssuedBranches = session("SearchReportShowLicIssuedBranches")
	oSavedReport.ShowLicCompanyL2s = session("SearchReportShowLicCompanyL2s")
	oSavedReport.ShowLicIssuedCompanyL2s = session("SearchReportShowLicIssuedCompanyL2s")
	oSavedReport.ShowLicCompanyL3s = session("SearchReportShowLicCompanyL3s")
	oSavedReport.ShowLicIssuedCompanyL3s = session("SearchReportShowLicIssuedCompanyL3s")
	oSavedReport.ShowFirstName = session("SearchReportShowFirstName")
	oSavedReport.ShowSsn = session("SearchReportShowSsn")
	oSavedReport.ShowEmployeeId = session("SearchReportShowEmployeeId")
	oSavedReport.ShowPhone = session("SearchReportShowPhone")
	oSavedReport.ShowEmail = session("SearchReportShowEmail")
	oSavedReport.ShowWorkAddress = session("SearchReportShowWorkAddress")
	oSavedReport.ShowWorkCity = session("SearchReportShowWorkCity")
	oSavedReport.ShowWorkState = session("SearchReportShowWorkState")
	oSavedReport.ShowWorkZip = session("SearchReportShowWorkZip")
	oSavedReport.ShowAddress = session("SearchReportShowSpecAddress")
	oSavedReport.ShowCity = session("SearchReportShowSpecCity")
	oSavedReport.ShowState = session("SearchReportShowSpecState")
	oSavedReport.ShowZip = session("SearchReportShowSpecZip")
	
	if oSavedReport.SaveReport = 0 then
		AlertError("Failed to save the report """ & request("SavedReportName") & """.  Please try your request again.")
		Response.End
	else
		Response.Redirect("officerreport.asp?savedreport=" & oSavedReport.ReportId)
	end if


elseif request("SavedReport") <> "" then

	'We are loading a saved report.  Take the properties from the report and
	'load them.
	
	if oSavedReport.LoadReportById(ScrubForSql(request("SavedReport"))) = 0 then
		AlertError("Failed to load the saved Report record.  Please try your request again.")
		Response.End
	elseif oSavedReport.CompanyId <> session("UserCompanyId") then
		AlertError("Unable to load the saved Report record.  Please try your request again.")
		Response.End
	end if 
	
	if request("sub2") = "Delete" then
		oSavedReport.Deleted = 1
		oSavedReport.SaveReport
		Response.Redirect("officerreport.asp")
		Response.End
	end if

	ClearReportSessionVars()
	
	iSavedReportId = oSavedReport.ReportId
	sSavedReportName = oSavedReport.Name 
	session("SearchReportSavedName") = sSavedReportName
	
	
	'We need to take the grouped-together list we got and break it into separate
	'lists for L2s, L3s, and Branches.  Where we get the list from depends on
	'where we are getting the search criteria.
	sAssocIdList = oSavedReport.StructureIdList
	aiAssocIdList = split(sAssocIdList, ", ")
	for each sAssocId in aiAssocIdList
		if left(sAssocId, 1) = "a" then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
		elseif left(sAssocId, 1) = "b" then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
		elseif left(sAssocId, 1) = "c" then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	next
	'Remove the trailing commas
	if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
	if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
	if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)
	
	
	oAssociate.UserStatus = 1

	oAssociate.LastName = oSavedReport.LastName
	session("SearchReportLastName") = oSavedReport.LastName
	
	oAssociate.Ssn = oSavedReport.Ssn
	session("SearchReportSsn") = enDeCrypt(oSavedReport.Ssn,application("RC4Pass"))
	
	oAssociate.SearchStateIdList = oSavedReport.StateIdList
	session("SearchReportStateIdList") = oSavedReport.StateIdList
	
	oAssociate.SearchBranchIdList = sBranchIdList
	session("SearchReportBranchIdList") = sBranchIdList
	
	oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
	session("SearchReportCompanyL2IdList") = sCompanyL2IdList	
	
	oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
	session("SearchReportCompanyL3IdList") = sCompanyL3IdList

	oAssociate.Inactive = oSavedReport.ShowInactive
	session("SearchReportInactive") = oSavedReport.ShowInactive
	
	oAssociate.SearchCourseName = oSavedReport.Course
	session("SearchReportCourseName") = oSavedReport.Course
	
	oAssociate.SearchCourseProviderId = oSavedReport.ProviderId
	session("SearchReportProviderId") = oSavedReport.ProviderId
	
	oAssociate.SearchCourseCreditHours = oSavedReport.CreditHours
	session("SearchReportCreditHours") = oSavedReport.CreditHours
	
	oAssociate.SearchCourseCompletionDateFrom = oSavedReport.CourseCompletionDateFrom
	session("SearchReportCourseCompletionDateFrom") = oSavedReport.CourseCompletionDateFrom

	oAssociate.SearchCourseCompletionDateTo = oSavedReport.CourseCompletionDateTo
	session("SearchReportCourseCompletionDateTo") = oSavedReport.CourseCompletionDateTo
	
	oAssociate.SearchCourseExpDateFrom = oSavedReport.CourseExpFrom
	session("SearchReportCourseExpDateFrom") = oSavedReport.CourseExpFrom
	
	oAssociate.SearchCourseExpDateTo = oSavedReport.CourseExpTo
	session("SearchReportCourseExpDateTo") = oSavedReport.CourseExpTo
	
	oAssociate.SearchLicenseStatusId = oSavedReport.LicenseStatusId
	session("SearchReportLicenseStatusId") = oSavedReport.LicenseStatusId

	oAssociate.SearchLicenseStatusIdList = oSavedReport.LicenseStatusIdList
	session("SearchReportLicenseStatusIdList") = oSavedReport.LicenseStatusIdList	
	
	oAssociate.SearchLicenseNumber = oSavedReport.LicenseNumber
	session("SearchReportLicenseNumber") = oSavedReport.LicenseNumber
	
	oAssociate.SearchLicenseExpDateFrom = oSavedReport.LicExpFrom
	session("SearchReportLicenseExpDateFrom") = oSavedReport.LicExpFrom

	oAssociate.SearchLicenseExpDateTo = oSavedReport.LicExpTo
	session("SearchReportLicenseExpDateTo") = oSavedReport.LicExpTo
	
	oAssociate.SearchAppDeadline = oSavedReport.AppDeadline
	session("SearchReportAppDeadline") = oSavedReport.AppDeadline
	
	bLicenseSearch = oSavedReport.ShowLicenses
	session("SearchReportShowLicenses") = oSavedReport.ShowLicenses
	
	bLicIssuedSearch = oSavedReport.ShowLicIssued
	session("SearchReportShowLicIssued") = oSavedReport.ShowLicIssued
	
	bCourseSearch = oSavedReport.ShowCourses
	session("SearchReportShowCourses") = oSavedReport.ShowCourses
	
	bOfficerSearch = oSavedReport.ShowOfficers
	session("SearchReportShowOfficers") = oSavedReport.ShowOfficers
	
	bCompanySearch = oSavedReport.ShowCompanies
	session("SearchReportShowCompany") = oSavedReport.ShowCompanies
	
	bBranchSearch = oSavedReport.ShowBranches
	session("SearchReportShowBranches") = oSavedReport.ShowBranches
	
	bCompanyL2Search = oSavedReport.ShowCompanyL2s
	session("SearchReportShowCompanyL2s") = oSavedReport.ShowCompanyL2s
	
	bCompanyL3Search = oSavedReport.ShowCompanyL3s
	session("SearchReportShowCompanyL3s") = oSavedReport.ShowCompanyL3s
	
	iShowInactive = oSavedReport.ShowInactive
	session("SearchReportInactive") = oSavedReport.ShowInactive
	
	iShowCompleted = oSavedReport.ShowCompleted
	session("SearchReportCompleted") = oSavedReport.ShowCompleted
	
	bAdminSearch = oSavedReport.ShowAdmins
	session("SearchReportShowAdmins") = oSavedReport.ShowAdmins
	
	bLicBranchSearch = oSavedReport.ShowLicBranches
	session("SearchReportShowLicBranches") = oSavedReport.ShowLicBranches
	
	bLicIssuedBranchSearch = oSavedReport.ShowLicIssuedBranches
	session("SearchReportShowLicIssuedBranches") = oSavedReport.ShowLicIssuedBranches
	
	bLicCompanyL2Search = oSavedReport.ShowLicCompanyL2s
	session("SearchReportShowLicCompanyL2s") = oSavedReport.ShowLicCompanyL2s
	
	bLicIssuedCompanyL2Search = oSavedReport.ShowLicIssuedCompanyL2s
	session("SearchReportShowLicIssuedCompanyL2s") = oSavedReport.ShowLicIssuedCompanyL2s
	
	bLicCompanyL3Search = oSavedReport.ShowLicCompanyL3s
	session("SearchReportShowLicCompanyL3s") = oSavedReport.ShowLicCompanyL3s
	
	bLicIssuedCompanyL3Search = oSavedReport.ShowLicIssuedCompanyL3s
	session("SearchReportShowLicIssuedCompanyL3s") = oSavedReport.ShowLicIssuedCompanyL3s
	
	bFirstNameSearch = oSavedReport.ShowFirstName
	session("SearchReportShowFirstName") = oSavedReport.ShowFirstName
	
	bSsnSearch = oSavedReport.ShowSsn
	session("SearchReportShowSsn") = oSavedReport.ShowSsn
	
	bEmployeeIdSearch = oSavedReport.ShowEmployeeId
	session("SearchReportShowEmployeeId") = oSavedReport.ShowEmployeeId
	
	bPhoneSearch = oSavedReport.ShowPhone
	session("SearchReportShowPhone") = oSavedReport.ShowPhone
	
	bEmailSearch = oSavedReport.ShowEmail
	session("SearchReportShowEmail") = oSavedReport.ShowEmail
	
	bWorkAddressSearch = oSavedReport.ShowWorkAddress
	session("SearchReportShowWorkAddress") = oSavedReport.ShowWorkAddress
	
	bWorkCitySearch = oSavedReport.ShowWorkCity
	session("SearchReportShowWorkCity") = oSavedReport.ShowWorkCity
	
	bWorkStateSearch = oSavedReport.ShowWorkState
	session("SearchReportShowWorkState") = oSavedReport.ShowWorkState
	
	bWorkZipSearch = oSavedReport.ShowWorkZip
	session("SearchReportShowWorkZip") = oSavedReport.ShowWorkZip
	
	bSpecAddressSearch = oSavedReport.ShowAddress
	session("SearchReportShowSpecAddress") = oSavedReport.ShowAddress
	
	bSpecCitySearch = oSavedReport.ShowCity
	session("SearchReportShowSpecCity") = oSavedReport.ShowCity
	
	bSpecStateSearch = oSavedReport.ShowState
	session("SearchReportShowSpecState") = oSavedReport.ShowState
	
	bSpecZipSearch = oSavedReport.ShowZip
	session("SearchReportShowSpecZip") = oSavedReport.ShowZip
	
'	iListType = oSavedReport.ShowBranchAdmins
'	session("SearchReportListType") = oSavedReport.ShowBranchAdmins
	
	'bCompanyL2Search = oSavedReport.ShowCompanyL2s
	'session("SearchReportShowCompanyL2") = oSavedReport.ShowCompanyL2s
	
	'bCompanyL3Search = oSavedReport.ShowCompanyL3s
	'session("SearchReportShowCompanyL3") = oSavedReport.ShowCompanyL3s
	

else

	oAssociate.LastName = ScrubForSql(request("LastName"))
	if (oAssociate.LastName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLastName")) <> "") then
			oAssociate.LastName = session("SearchReportLastName")
		end if
	else
		session("SearchReportLastName") = oAssociate.LastName
	end if

	oAssociate.Ssn = enDeCrypt(ScrubForSql(request("Ssn")),application("RC4Pass"))
	if (oAssociate.Ssn = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportSsn")) <> "") then
			oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
		end if
	else
		session("SearchReportSsn") = enDeCrypt(oAssociate.Ssn,application("RC4Pass"))
	end if

'	oAssociate.StateId = ScrubForSql(request("StateId"))
'	if (oAssociate.StateId = "") and (ucase(sAction) <> "SEARCH") then
'		if (trim(session("SearchReportStateId")) <> "") then
'			oAssociate.StateId = session("SearchReportStateId")
'		end if
'	else
'		session("SearchReportStateId") = oAssociate.StateId
'	end if
	
	oAssociate.SearchStateIdList = sStateIdList
	if (oAssociate.SearchStateIdList = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(sessioN("SearchReportStateIdList")) <> "") then
			oAssociate.SearchStateIdList = session("SearchReportStateIdList")
		end if
	else
		session("SearchReportStateIdList") = oAssociate.SearchStateIdList
	end if	


	oAssociate.BranchId = ScrubForSql(request("BranchId"))
	if (oAssociate.BranchId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportBranchId")) <> "") then
			oAssociate.BranchId = session("SearchReportBranchId")
		end if
	else
		session("SearchReportBranchId") = oAssociate.BranchId
	end if
	
	'if ScrubForSql(request("ShowInactive")) <> "" then
	'	oAssociate.Inactive = true
	'else
	'	oAssociate.Inactive = false
	'end if
	'oAssociate.Inactive = ScrubForSql(request("ShowInactive"))
	'if (oAssociate.Inactive = "") and (ucase(sAction) <> "SEARCH") then
	'	if (trim(session("SearchReportInactive")) <> "") then
	'		oAssociate.Inactive = session("SearchReportInactive")
	'	else
	'		oAssociate.Inactive = false
	'		session("SearchReportInactive") = false
	'	end if
	'elseif oAssociate.Inactive = "" then
	'	oAssociate.Inactive = false
	'else
	'	session("SearchReportInactive") = oAssociate.Inactive
	'end if
	
	iShowInactive = ScrubForSql(request("ShowInactive"))
	if iShowInactive = "0" then
		oAssociate.Inactive = false
	elseif iShowInactive = "1" then
		oAssociate.Inactive = true
	elseif iShowInactive = "2" then
		oAssociate.Inactive = ""
	end if
	if session("SearchReportInactive") <> "" and ucase(sAction) <> "SEARCH" then
		oAssociate.Inactive = session("SearchReportInactive")
	else
		session("SearchReportInactive") = oAssociate.Inactive
	end if
	
	
	sAssocIdList = ScrubForSql(request("StructureIdsList"))
	session("SearchReportStructureIdList") = sAssocIdList
	aiAssocIdList = split(sAssocIdList, ", ")
	for each sAssocId in aiAssocIdList
		if left(sAssocId, 1) = "a" then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
		elseif left(sAssocId, 1) = "b" then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
		elseif left(sAssocId, 1) = "c" then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	next
	'Remove the trailing commas
	if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
	if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
	if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

	
	oAssociate.SearchBranchIdList = sBranchIdList
	if (oAssociate.SearchBranchIdList = "") and (ucase(sAction) <> "SEARCH") then	
		if (trim(session("SearchReportBranchIdList")) <> "") then
			oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
		end if
	else
		session("SearchReportBranchIdList") = oAssociate.SearchBranchIdList
	end if


	oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
	if (oAssociate.SearchCompanyL2IdList = "") and (ucase(sAction) <> "SEARCH") then	
		if (trim(session("SearchReportCompanyL2IdList")) <> "") then
			oAssociate.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
		end if
	else
		session("SearchReportCompanyL2IdList") = oAssociate.SearchCompanyL2IdList
	end if
	
	
	oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
	if (oAssociate.SearchCompanyL3IdList = "") and (ucase(sAction) <> "SEARCH") then	
		if (trim(session("SearchReportCompanyL3IdList")) <> "") then
			oAssociate.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
		end if
	else
		session("SearchReportCompanyL3IdList") = oAssociate.SearchCompanyL3IdList
	end if
		

	oAssociate.SearchCourseName = ScrubForSql(request("CourseName"))
	if (oAssociate.SearchCourseName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseName")) <> "") then
			oAssociate.SearchCourseName = session("SearchReportCourseName")
		end if
	else
		session("SearchReportCourseName") = oAssociate.SearchCourseName
	end if
	
	oAssociate.SearchCourseProviderId = ScrubForSql(request("Provider"))
	if (oAssociate.SearchCourseProviderId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportProviderId")) <> "") then
			oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		end if
	else
		session("SearchReportProviderId") = oAssociate.SearchCourseProviderId
	end if
	
	oAssociate.SearchCourseCreditHours = ScrubForSql(request("CreditHours"))
	if (oAssociate.SearchCourseCreditHours = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCreditHours")) <> "") then
			oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
		end if
	else
		session("SearchReportCreditHours") = oAssociate.SearchCourseCreditHours
	end if
	
	oAssociate.SearchCourseCompletionDateFrom = ScrubForSql(request("CourseCompletionDateFrom"))
	if (oAssociate.SearchCourseCompletionDateFrom = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
			oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
		end if
	else
		session("SearchReportCourseCompletionDateFrom") = oAssociate.SearchCourseCompletionDateFrom
	end if
	
	oAssociate.SearchCourseCompletionDateTo = ScrubForSql(request("CourseCompletionDateTo"))
	if (oAssociate.SearchCourseCompletionDateTo = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
			oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
		end if
	else
		session("SearchReportCourseCompletionDateTo") = oAssociate.SearchCourseCompletionDateTo
	end if	
	
	oAssociate.SearchCourseExpDateFrom = ScrubForSql(request("CourseExpDateFrom"))
	if (oAssociate.SearchCourseExpDateFrom = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
			oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
		end if
	else
		session("SearchReportCourseExpDateFrom") = oAssociate.SearchCourseExpDateFrom
	end if
	
	oAssociate.SearchCourseExpDateTo = ScrubForSql(request("CourseExpDateTo"))
	if (oAssociate.SearchCourseExpDateTo = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseExpDateTo")) <> "") then
			oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
		end if
	else
		session("SearchReportCourseExpDateTo") = oAssociate.SearchCourseExpDateTo
	end if	

'	oAssociate.SearchLicenseStatusId = ScrubForSql(request("LicenseStatus"))
'	if (oAssociate.SearchLicenseStatusId = "") and (ucase(sAction) <> "SEARCH") then
'		if (trim(session("SearchReportLicenseStatusId")) <> "") then
'			oAssociate.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
'		end if
'	else
'		session("SearchReportLicenseStatusId") = oAssociate.SearchLicenseStatusId
'	end if
	oAssociate.SearchLicenseStatusId = "" 'set to empty string
	Session("SearchReportLicenseStatusId") = "" 'set to empty string
	
	oAssociate.SearchLicenseStatusIdList = ScrubForSql(request("LicenseStatus"))
	if (oAssociate.SearchLicenseStatusIdList = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
			oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
		end if
	else
		session("SearchReportLicenseStatusIdList") = oAssociate.SearchLicenseStatusIdList
	end if
	
	oAssociate.SearchLicenseNumber = ScrubForSql(request("LicenseNum"))
	if (oAssociate.SearchLicenseNumber = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseNumber")) <> "") then
			oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
		end if
	else
		session("SearchReportLicenseNumber") = oAssociate.SearchLicenseNumber
	end if
	
	oAssociate.SearchLicenseExpDateFrom = ScrubForSql(request("ExpDateFrom"))
	if (oAssociate.SearchLicenseExpDateFrom = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
			oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
		end if
	else
		session("SearchReportLicenseExpDateFrom") = oAssociate.SearchLicenseExpDateFrom
	end if
	
	oAssociate.SearchLicenseExpDateTo = ScrubForSql(request("ExpDateTo"))
	if (oAssociate.SearchLicenseExpDateTo = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
			oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
		end if
	else
		session("SearchReportLicenseExpDateTo") = oAssociate.SearchLicenseExpDateTo
	end if	

	oAssociate.SearchAppDeadline = ScrubForSql(left(request("AppDeadline"), 1))
	if (oAssociate.SearchAppDeadline = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportAppDeadline")) <> "") then
			oAssociate.SearchAppDeadline = session("SearchReportAppDeadline")
		end if
	else
		session("SearchReportAppDeadline") = oAssociate.SearchAppDeadline
	end if


	bLicenseSearch = request("ShowLicenses")
	if session("SearchReportShowLicenses") <> "" and ucase(sAction) <> "SEARCH" then
		bLicenseSearch = session("SearchReportShowLicenses")
	else
		session("SearchReportShowLicenses") = bLicenseSearch
	end if
	
	bLicIssuedSearch = request("ShowLicIssued")
	if session("SearchReportShowLicIssued") <> "" and ucase(sAction) <> "SEARCH" then
		bLicIssuedSearch = session("SearchReportShowLicIssued")
	else
		session("SearchReportShowLicIssued") = bLicIssuedSearch
	end if
	
	bAdminSearch = request("ShowAdmins")
	if session("SearchReportShowAdmins") <> "" and ucase(sAction) <> "SEARCH" then
		bAdminSearch = session("SearchReportShowAdmins")
	else
		session("SearchReportShowAdmins") = bAdminSearch
	end if		
	
	bManagerSearch = request("ShowManagers")
	if session("SearchReportShowManagers") <> "" and ucase(sAction) <> "SEARCH" then
		bManagerSearch = session("SearchReportShowManagers")
	else
		session("SearchReportShowManagers") = bManagerSearch
	end if
	
	bCourseSearch = request("ShowCourses")
	if session("SearchReportShowCourses") <> "" and ucase(sAction) <> "SEARCH" then
		bCourseSearch = session("SearchReportShowCourses")
	else
		session("SearchReportShowCourses") = bCourseSearch
	end if		

	bOfficerSearch = request("ShowOfficers")
	if session("SearchReportShowOfficers") <> "" and ucase(sAction) <> "SEARCH" then
		bOfficerSearch = session("SearchReportShowOfficers")
	else
		session("SearchReportShowOfficers") = bOfficerSearch
	end if

	bCompanySearch = request("ShowCompany")
	if session("SearchReportShowCompany") <> "" and ucase(sAction) <> "SEARCH" then
		bCompanySearch = session("SearchReportShowCompany")
	else
		session("SearchReportShowCompany") = bCompanySearch
	end if
	
	bBranchSearch = request("ShowBranches")
	if session("SearchReportShowBranches") <> "" and ucase(sAction) <> "SEARCH" then
		bBranchSearch = session("SearchReportShowBranches")
	else
		session("SearchReportShowBranches") = bBranchSearch
	end if
	
	bLicBranchSearch = request("ShowLicBranches")
	if session("SearchReportShowLicBranches") <> "" and ucase(sAction) <> "SEARCH" then
		bLicBranchSearch = session("SearchReportShowLicBranches")
	else
		session("SearchReportShowLicBranches") = bLicBranchSearch
	end if
	
	bLicIssuedBranchSearch = request("ShowLicIssuedBranches")
	if session("SearchReportShowLicIssuedBranches") <> "" and ucase(sAction) <> "SEARCH" then
		bLicIssuedBranchSearch = session("SearchReportShowLicIssuedBranches")
	else
		session("SearchReportShowLicIssuedBranches") = bLicIssuedBranchSearch
	end if
	
	bCompanyL2Search = request("ShowCompanyL2s")
	if session("SearchReportShowCompanyL2s") <> "" and ucase(sAction) <> "SEARCH" then
		bCompanyL2Search = session("SearchReportShowCompanyL2s")
	else
		session("SearchReportShowCompanyL2s") = bCompanyL2Search
	end if
	
	bLicCompanyL2Search = request("ShowLicCompanyL2s")
	if session("SearchReportShowLicCompanyL2s") <> "" and ucase(sAction) <> "SEARCH" then
		bLicCompanyL2Search = session("SearchReportShowLicCompanyL2s")
	else
		session("SearchReportShowLicCompanyL2s") = bLicCompanyL2Search
	end if
	
	bLicIssuedCompanyL2Search = request("ShowLicIssuedCompanyL2s")
	if session("SearchReportShowLicIssuedCompanyL2s") <> "" and ucase(sAction) <> "SEARCH" then
		bLicIssuedCompanyL2Search = session("SearchReportShowLicIssuedCompanyL2s")
	else
		session("SearchReportShowLicIssuedCompanyL2s") = bLicIssuedCompanyL2Search
	end if	
	
	bCompanyL3Search = request("ShowCompanyL3s")
	if session("SearchReportShowCompanyL3s") <> "" and ucase(sAction) <> "SEARCH" then
		bCompanyL3Search = session("SearchReportShowCompanyL3s")
	else
		session("SearchReportShowCompanyL3s") = bCompanyL3Search
	end if
	
	bLicCompanyL3Search = request("ShowLicCompanyL3s")
	if session("SearchReportShowLicCompanyL3s") <> "" and ucase(sAction) <> "SEARCH" then
		bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
	else
		session("SearchReportShowLicCompanyL3s") = bLicCompanyL3Search
	end if		

	bLicIssuedCompanyL3Search = request("ShowLicIssuedCompanyL3s")
	if session("SearchReportShowLicIssuedCompanyL3s") <> "" and ucase(sAction) <> "SEARCH" then
		bLicIssuedCompanyL3Search = session("SearchReportShowLicIssuedCompanyL3s")
	else
		session("SearchReportShowLicIssuedCompanyL3s") = bLicIssuedCompanyL3Search
	end if		
	
	bFirstNameSearch = request("ShowFirstName")
	if session("SearchReportShowFirstName") <> "" and ucase(sAction) <> "SEARCH" then
		bFirstNameSearch = session("SearchReportShowFirstName")
	else
		session("SearchReportShowFirstName") = bFirstNameSearch
	end if

	bLastNameSearch = request("ShowLastName")
	if session("SearchReportShowLastName") <> "" and ucase(sAction) <> "SEARCH" then
		bLastNameSearch = session("SearchReportShowLastName")
	else
		session("SearchReportShowLastName") = bLastNameSearch
	end if

	bSsnSearch = request("ShowSsn")
	if session("SearchReportShowSsn") <> "" and ucase(sAction) <> "SEARCH" then
		bSsnSearch = session("SearchReportShowSsn")
	else
		session("SearchReportShowSsn") = bSsnSearch
	end if
	
	bEmployeeIdSearch = request("ShowEmployeeId")
	if session("SearchReportShowEmployeeId") <> "" and ucase(sAction) <> "SEARCH" then
		bEmployeeIdSearch = session("SearchReportShowEmployeeId")
	else
		session("SearchReportShowEmployeeId") = bEmployeeIdSearch
	end if
	
	bPhoneSearch = request("ShowPhone")
	if session("SearchReportShowPhone") <> "" and ucase(sAction) <> "SEARCH" then
		bPhoneSearch = session("SearchReportShowPhone")
	else
		session("SearchReportShowPhone") = bPhoneSearch
	end if

	bEmailSearch = request("ShowEmail")
	if session("SearchReportShowEmail") <> "" and ucase(sAction) <> "SEARCH" then
		bEmailSearch = session("SearchReportShowEmail")
	else
		session("SearchReportShowEmail") = bEmailSearch
	end if
	
	bWorkAddressSearch = request("ShowWorkAddress")
	if session("SearchReportShowWorkAddress") <> "" and ucase(sAction) <> "SEARCH" then
		bWorkAddressSearch = session("SearchReportShowWorkAddress")
	else
		session("SearchReportShowWorkAddress") = bWorkAddressSearch
	end if
	
	bWorkCitySearch = request("ShowWorkCity")
	if session("SearchReportShowWorkCity") <> "" and ucase(sAction) <> "SEARCH" then
		bWorkCitySearch = session("SearchReportShowWorkCity")
	else
		session("SearchReportShowWorkCity") = bWorkCitySearch
	end if

	bWorkStateSearch = request("ShowWorkState")
	if session("SearchReportShowWorkState") <> "" and ucase(sAction) <> "SEARCH" then
		bWorkStateSearch = session("SearchReportShowWorkState")
	else
		session("SearchReportShowWorkState") = bWorkStateSearch
	end if

	bWorkZipSearch = request("ShowWorkZip")
	if session("SearchReportShowWorkZip") <> "" and ucase(sAction) <> "SEARCH" then
		bWorkZipSearch = session("SearchReportShowWorkZip")
	else
		session("SearchReportShowWorkZip") = bWorkZipSearch
	end if
	
	bSpecAddressSearch = request("ShowSpecAddress")
	if session("SearchReportShowSpecAddress") <> "" and ucase(sAction) <> "SEARCH" then
		bSpecAddressSearch = session("SearchReportShowSpecAddress")
	else
		session("SearchReportShowSpecAddress") = bSpecAddressSearch
	end if
	
	bSpecCitySearch = request("ShowSpecCity")
	if session("SearchReportShowSpecCity") <> "" and ucase(sAction) <> "SEARCH" then
		bSpecCitySearch = session("SearchReportShowSpecCity")
	else
		session("SearchReportShowSpecCity") = bSpecCitySearch
	end if

	bSpecStateSearch = request("ShowSpecState")
	if session("SearchReportShowSpecState") <> "" and ucase(sAction) <> "SEARCH" then
		bSpecStateSearch = session("SearchReportShowSpecState")
	else
		session("SearchReportShowSpecState") = bSpecStateSearch
	end if

	bSpecZipSearch = request("ShowSpecZip")
	if session("SearchReportShowSpecZip") <> "" and ucase(sAction) <> "SEARCH" then
		bSpecZipSearch = session("SearchReportShowSpecZip")
	else
		session("SearchReportShowSpecZip") = bSpecZipSearch
	end if

	iShowCompleted = request("ShowCompleted")
	if session("SearchReportCompleted") <> "" and ucase(sAction) <> "SEARCH" then
		iShowCompleted = session("SearchReportCompleted")
	else
		session("SearchReportCompleted") = iShowCompleted
	end if	
	
	
	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if
	

'	iListType = ScrubForSql(request("ListType"))
'	if iListType = "" and ucase(sAction) <> "SEARCH" then
'		if trim(session("SearchReportListType")) <> "" then
'			iListType = session("SearchReportListType")
'		else
'			iListType = 0
'		end if
''	else
'		if trim(iListType) = "" then
''			iListType = 0
'		end if
'		session("SearchReportListType") = iListType
'	end if

end if	


	'Update the license object properties based on the search we just did.
	'We'll use these properties later to search for the specific license records.
	oLicense.LicenseStatusId = oAssociate.SearchLicenseStatusId
	oLicense.SearchStatusIdList = oAssociate.SearchLicenseStatusIdList
	oLicense.LicenseNum = oAssociate.SearchLicenseNumber
	oLicense.SearchLicenseExpDateFrom = oAssociate.SearchLicenseExpDateFrom
	oLicense.SearchLicenseExpDateTo = oAssociate.SearchLicenseExpDateTo
	oLicense.SearchAppDeadline = oAssociate.SearchAppDeadline
	

	
	

'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
end if


	
	set oRs = oAssociate.SearchAssociatesCoursesLicenses()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20

dim oWorkAddressRs
dim oCompanyLicRs
dim oBranchLicRs
dim oCompanyL2LicRs
dim oCompanyL3LicRs

'If we're searching Company licenses, run the search.
if bCompanySearch and not bCheckIsAdmin then
	bCompanySearch = false
end if
'if bCompanySearch then
'	oLicense.SearchStateIdList = session("SearchReportStateIdList")
'	oLicense.OwnerId = session("UserCompanyId")
'	oLicense.OwnerTypeId = 3
'	set oCompanyLicRs = oLicense.SearchLicenses
'	oLicense.OwnerTypeId = 1
'end if
	
	
'If we're searching CompanyL2 licenses, run the search.
dim oCompanyL2
dim oCompanyL2Rs
if bCompanyL2Search then
	if session("SearchReportCompanyL2IdList") = "" and not bCheckIsAdmin then
		set oCompanyL2 = new CompanyL2
		oCompanyL2.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		'bCompanyL2Search = false
	'elseif session("SearchReportCompanyL2IdList") = "" and (session("SearchReportCompanyL3IdList") <> "" or session("SearchReportBranchIdList") <> "") then
	'	bCompanyL2Search = false
	end if
end if
if bCompanyL2Search then
	set oCompanyL2 = new CompanyL2
	oCompanyL2.ConnectionString = application("sDataSourceName")
	oCompanyL2.VocalErrors = application("bVocalErrors")
	oCompanyL2.CompanyId = session("UserCompanyId")
	
	oCompanyL2.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL2.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL2.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL2.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL2.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL2.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL2.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL2.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL2.Inactive = session("SearchReportInactive")
	
	set oCompanyL2Rs = oCompanyL2.SearchCompaniesLicenses
end if


'If we're searching CompanyL3 licenses, run the search.
dim oCompanyL3
dim oCompanyL3Rs
if bCompanyL3Search then
	if session("SearchReportCompanyL3IdList") = "" and not bCheckIsAdmin then
		bCompanyL3Search = false
	'elseif session("SearchReportCompanyL3IdList") = "" and (session("SearchReportCompanyL2IdList") <> "" or session("SearchReportBranchIdList") <> "") then
'		bCompanyL3Search = false
	end if
end if
if bCompanyL3Search then
	set oCompanyL3 = new CompanyL3
	oCompanyL3.ConnectionString = application("sDataSourceName")
	oCompanyL3.VocalErrors = application("bVocalErrors")
	oCompanyL3.CompanyId = session("UserCompanyId")
	
	oCompanyL3.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL3.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL3.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oCompanyL3.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL3.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL3.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL3.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL3.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL3.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL3.Inactive = session("SearchReportInactive")
	
	set oCompanyL3Rs = oCompanyL3.SearchCompaniesLicenses
end if
	
	
'If we're searching Branch licenses, run the search.
dim oBranchRs
if bBranchSearch then
	if session("SearchReportBranchIdList") = "" and not bCheckIsAdmin then
		bBranchSearch = false
	end if
end if
if bBranchSearch then
	set oBranch = nothing
	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	oBranch.TpConnectionString = application("sTpDataSourceName")
	oBranch.VocalErrors = application("bVocalErrors")
	oBranch.CompanyId = session("UserCompanyId")
			
	oBranch.SearchStateIdList = session("SearchReportStateIdList")
	oBranch.SearchBranchIdList = session("SearchReportBranchIdList")
	oBranch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oBranch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oBranch.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oBranch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oBranch.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oBranch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oBranch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oBranch.SearchAppDeadline = session("SearchReportAppDeadline")
	oBranch.Inactive = session("SearchReportInactive")

	set oBranchRs = oBranch.SearchBranchesLicenses
end if
	
%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Branch License Reporting 
					<% if session("SearchReportSavedName") <> "" then %> - <% = session("SearchReportSavedName") %><% end if %>
					</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0" width="100%">
										<form name="frm1" action="branchlicreport.asp" method="POST">
										<input type="hidden" name="action" value="SEARCH">
										<input type="hidden" name="page_number" value="1">
										<tr>
											<td valign="top" class="newstitle" nowrap>State: </td>
											<td valign="middle"><% call DisplayStatesListBox(oAssociate.SearchStateIdList,0,"StateIdList", false) 'functions.asp %></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td valign="top" class="newstitle" nowrap>Structure: </td>
											<td valign="top"><% call DisplayL2sL3sBranchesListBox(sAssocIdList, session("UserCompanyId"), false, 0, "StructureIdsList") %></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>License Number: </td>
											<td><input type="text" name="LicenseNum" size="20" value="<% = oAssociate.SearchLicenseNumber %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td class="newstitle" nowrap>Lic. Expiring (From): </td>
											<td nowrap>
			<input type="text" name="ExpDateFrom" value="<% = oAssociate.SearchLicenseExpDateFrom %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('frm1.ExpDateFrom');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>License Status: </td>
											<td>
											<% call DisplayLicenseStatusDropdown(oAssociate.SearchLicenseStatusIdList, 1, "LicenseStatus") %>
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td class="newstitle" nowrap>Lic. Expiring (To): </td>
											<td>
			<input type="text" name="ExpDateTo" value="<% = oAssociate.SearchLicenseExpDateTo %>" size="10" maxlength="10">
			<a href="javascript:show_calendar('frm1.ExpDateTo');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td valign="top" class="newstitle" nowrap><br>Inactive Records: </td>
											<td valign="top"><br>
											<select name="ShowInactive">
												<option value="0"<% if cstr(session("SearchReportInactive")) = "False" then %> SELECTED<% end if %>>Show Active Only</option>
												<option value="1"<% if cstr(session("SearchReportInactive")) = "True" then %> SELECTED<% end if %>>Show Inactive Only</option>
												<option value="2"<% if session("SearchReportInactive") = "" then %> SELECTED<% end if %>>Show All Records</option>
											</select>
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td valign="top" class="newstitle" nowrap>Expiration Type: </td>
											<td>
											<input type="radio" name="AppDeadline" value="1"<% if oAssociate.SearchAppDeadline = "1" then %>CHECKED<% end if %>> Renewal Application Deadline & License Expiration Date
											<br/>
											<input type="radio" name="AppDeadline" value=""<% if oAssociate.SearchAppDeadline = "" then %>CHECKED<% end if %>> License Expiration Date
											<br/>
											<input type="radio" name="AppDeadline" value="2"<% if oAssociate.SearchAppDeadline = "2" then %>CHECKED<% end if %>> Renewal Application Deadline
											<br/>
											<input type="radio" name="AppDeadline" value="3"<% if oAssociate.SearchAppDeadline = "3" then %>CHECKED<% end if %>> Renewal Submission Date
											<br/>
											<input type="radio" name="AppDeadline" value="4"<% if oAssociate.SearchAppDeadline = "4" then %>CHECKED<% end if %>> Submission Date
											<br/>
											<input type="radio" name="AppDeadline" value="5"<% if oAssociate.SearchAppDeadline = "5" then %>CHECKED<% end if %>> Issue Date
											</td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											
											
											
										</tr>
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="6">
												<span class="newstitle">Show in Results: </span>
											</td>
										</tr>
										<tr>
											<td colspan="3" valign="top">
												&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowCompany" value="1"<% if bCompanySearch then %> CHECKED<% end if %>> <% = session("CompanyName") %> Licenses
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowCompanyL2s" value="1"<% if bCompanyL2Search then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Matches
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicCompanyL2s" value="1"<% if bLicCompanyL2Search then %> CHECKED<% end if %>> <% = session("CompanyL2Name") %> Licenses
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicIssuedCompanyL2s" value="1"<% if bLicIssuedCompanyL2Search then %> CHECKED<% end if %>> Date <% = session("CompanyL2Name") %> Licenses were issued
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowCompanyL3s" value="1"<% if bCompanyL3Search then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Matches
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicCompanyL3s" value="1"<% if bLicCompanyL3Search then %> CHECKED<% end if %>> <% = session("CompanyL3Name") %> Licenses
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicIssuedCompanyL3s" value="1"<% if bLicIssuedCompanyL3Search then %> CHECKED<% end if %>> Date <% = session("CompanyL3Name") %> Licenses were issued
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowBranches" value="1"<% if bBranchSearch then %> CHECKED<% end if %>> Branch Matches
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicBranches" value="1"<% if bLicBranchSearch then %> CHECKED<% end if %>> Branch Licenses
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicIssuedBranches" value="1"<% if bLicIssuedBranchSearch then %> CHECKED<% end if %>> Date Branch Licenses were issued
											</td>
											<td colspan="3" valign="top">
												&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowSpecAddress" value="1"<% if bSpecAddressSearch then %> CHECKED<% end if %>> Address
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowSpecCity" value="1"<% if bSpecCitySearch then %> CHECKED<% end if %>> City
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowSpecState" value="1"<% if bSpecStateSearch then %> CHECKED<% end if %>> State
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowSpecZip" value="1"<% if bSpecZipSearch then %> CHECKED<% end if %>> Zip
											</td>
										</tr>
										<tr>
											<td colspan="6" align="center"><input type="image" src="/media/images/blue_bttn_search.gif" width="79" height="23" alt="Execute Search" border="0"></td>
										</tr>
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>
				
				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value
dim sPrevUserId

dim oAssocRs 'Recordset to use within the associate listing

	if bCompanySearch and iCurPage = 1 then
	
		oLicense.SearchStateIdList = session("SearchReportStateIdList")
		oLicense.OwnerId = session("UserCompanyId")
		oLicense.OwnerTypeId = 3
		set oCompanyLicRs = oLicense.SearchLicenses
		oLicense.OwnerTypeId = 1
	
		if not (oCompanyLicRs.BOF and oCompanyLicRs.EOF) then
	
			%>
								<tr>
									<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/company/modcompany.asp"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Company Information" alt="Edit Company Information"></a>&nbsp;&nbsp;<% end if %><a href="/company/companydetail.asp" title="View Company Information"><% = oCompany.Name %></a></td>
								</tr>
			<%
						
			sColor = GetNextColor(sColor)
					
			
			if bAdminSearch then
				set oAdminRs = oCompany.GetAdmins(session("UserCompanyId"))
				if not oAdminRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Administrators: 
					<%
					do while not oAdminRs.EOF 
						Response.Write(oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")")
						oAdminRs.MoveNext
						if not oAdminRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if 
			
			if bPhoneSearch and oCompany.Phone <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Phone: <% = oCompany.Phone %>
							<% if oCompany.PhoneExt <> "" then %>
							ext. <% = oCompany.PhoneExt %>
							<% end if %>
							</td>
						</tr>			
				<%
				sColor = GetNextColor(sColor)
			end if 
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				sColor=GetNextColor(sColor)
				if oCompany.Address <> "" or oCompany.City <> "" or oCompany.StateId <> 0 or oCompany.Zipcode <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Address:
							<%
							if bSpecAddressSearch and oCompany.Address <> "" then
								Response.Write(oCompany.Address)
								if oCompany.Address2 <> "" then
									Response.Write(" " & oCompany.Address2)
								end if
								if (bSpecCitySearch and oCompany.City <> "") or (bSpecStateSearch and oCompany.StateId <> "") or _
									(bSpecZipSearch and oCompany.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecCitySearch and oCompany.City <> "" then
								Response.Write(oCompany.City)
								if (bSpecStateSearch and oCompany.StateId <> "") or (bSpecZipSearch and oCompany.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecStateSearch and oCompany.StateId <> "" then
								Response.Write(GetStateName(oCompany.StateId, 1))
								if (bSpecZipSearch and oCompany.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecZipSearch and oCompany.Zipcode <> "" then
								Response.Write(oCompany.Zipcode)
								if oCompany.ZipcodeExt <> "" then	
									Response.Write("-" & oCompany.ZipcodeExt)
								end if
							end if										
							%>
							</td>
						</tr>			
				<%
				end if 
				sColor = GetNextColor(sColor)
			end if 
	
	
			do while not oCompanyLicRs.EOF
				%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">License #<% = oCompanyLicRs("LicenseNum") %> - <% = oLicense.LookupState(oCompanyLicRs("LicenseStateId")) %><% if oCompanyLicRs("LicenseExpDate") <> "" then %>, Expires: <% = oCompanyLicRs("LicenseExpDate") %><% end if %>
							<%
							if oCompanyLicRs("LicenseExpDate") < date() + 90 and oCompanyLicRs("LicenseExpDate") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oCompanyLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% if oCompanyLicRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oCompanyLicRs("LicenseAppDeadline") %>
							<%
							if oCompanyLicRs("LicenseAppDeadline") < date() + 90 and oCompanyLicRs("LicenseAppDeadline") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oCompanyLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% end if %>
							<% if oLicense.HasDocuments(oCompanyLicRs("LicenseId")) then %> (<a href="/officers/licensedetail.asp?lid=<% = oCompanyLicRs("LicenseId") %>">attachments</a>) <% end if %>
						</td>
					</tr>						
				<%
				sColor = GetNextColor(sColor)
				oCompanyLicRs.MoveNext
			loop
			
		end if 
	end if 
	
	
	
	
	if bCompanyL2Search and iCurPage = 1 then
		do while not oCompanyL2Rs.EOF
		
			%>
			<tr>
				<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modcompanyl2.asp?id=<% = oCompanyL2Rs("CompanyL2Id") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyL2Name") %> Information" alt="Edit <% = session("CompanyL2Name") %> Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/companyl2detail.asp?id=<% = oCompanyL2Rs("CompanyL2Id") %>" title="View <% = session("CompanyL2Name") %> Information"><% = oCompanyL2Rs("Name") %></a></td>
			</tr>
			<%
						
			sColor = GetNextColor(sColor)
			
			if bAdminSearch then
				set oAdminRs = oCompanyL2.GetAdmins(oCompanyL2Rs("CompanyL2Id"))
				if not oAdminRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Administrators: 
					<%
					do while not oAdminRs.EOF 
						Response.Write(oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")")
						oAdminRs.MoveNext
						if not oAdminRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oManager.OwnerTypeId = 2
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Managers: 
					<%
					do while not oManagerRs.EOF
						Response.Write(oManagerRs("FullName"))
						oManagerRs.MoveNext
						if not oManagerRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if
			
			oCompanyL2.LoadCompanyById(oCompanyL2Rs("CompanyL2Id"))
			
			if bPhoneSearch and oCompanyL2.Phone <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Phone: <% = oCompanyL2.Phone %>
							<% if oCompanyL2.PhoneExt <> "" then %>
							ext. <% = oCompanyL2.PhoneExt %>
							<% end if %>
							</td>
						</tr>			
				<%
				sColor = GetNextColor(sColor)
			end if 
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				sColor=GetNextColor(sColor)
				if oCompanyL2.Address <> "" or oCompanyL2.City <> "" or oCompanyL2.StateId <> 0 or oCompanyL2.Zipcode <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Address:
							<%
							if bSpecAddressSearch and oCompanyL2.Address <> "" then
								Response.Write(oCompanyL2.Address)
								if oCompanyL2.Address2 <> "" then
									Response.Write(" " & oCompanyL2.Address2)
								end if
								if (bSpecCitySearch and oCompanyL2.City <> "") or (bSpecStateSearch and oCompanyL2.StateId <> "") or _
									(bSpecZipSearch and oCompanyL2.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecCitySearch and oCompanyL2.City <> "" then
								Response.Write(oCompanyL2.City)
								if (bSpecStateSearch and oCompanyL2.StateId <> "") or (bSpecZipSearch and oCompanyL2.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecStateSearch and oCompanyL2.StateId <> "" then
								Response.Write(GetStateName(oCompanyL2.StateId, 1))
								if (bSpecZipSearch and oCompanyL2.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecZipSearch and oCompanyL2.Zipcode <> "" then
								Response.Write(oCompanyL2.Zipcode)
								if oCompanyL2.ZipcodeExt <> "" then	
									Response.Write("-" & oCompanyL2.ZipcodeExt)
								end if
							end if										
							%>
							</td>
						</tr>			
				<%
					sColor = GetNextColor(sColor)
				end if 
			end if 
	
		
			if bLicCompanyL2Search then
			
				oLicense.SearchStateIdList = session("SearchReportStateIdList")
				oLicense.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oLicense.OwnerTypeId = 4
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					%>
						<tr>
							<td colspan="2"<% = sColor %>>
							<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %><% if oBranchLicRs("LicenseExpDate") <> "" then %>, Expires: <% = oBranchLicRs("LicenseExpDate") %><% end if %>
							<%
							if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% if oBranchLicRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oBranchLicRs("LicenseAppDeadline") %>
							<%
							if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% end if %>
							<% if bLicIssuedCompanyL2Search and oBranchLicRs("IssueDate") <> "" then %>, Issued: <% = oBranchLicRs("IssueDate") %><% end if %>
							<% if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then %> (<a href="/officers/licensedetail.asp?lid=<% = oBranchLicRs("LicenseId") %>">attachments</a>) <% end if %>
							</td>
						</tr>						
					<%
					sColor = GetNextColor(sColor)					
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL2Rs.MoveNext
					
		loop
	end if 
	
	
	
	if bCompanyL3Search and iCurPage = 1 then
		do while not oCompanyL3Rs.EOF
		
			%>
			<tr>
				<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modcompanyL3.asp?id=<% = oCompanyL3Rs("CompanyL3Id") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyL3Name") %> Information" alt="Edit <% = session("CompanyL3Name") %> Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/companyL3detail.asp?id=<% = oCompanyL3Rs("CompanyL3Id") %>" title="View <% = session("CompanyL3Name") %> Information"><% = oCompanyL3Rs("Name") %></a></td>
			</tr>
			<%
						
			sColor = GetNextColor(sColor)
			
			if bAdminSearch then
				set oAdminRs = oCompanyL3.GetAdmins(oCompanyL3Rs("CompanyL3Id"))
				if not oAdminRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Administrators: 
					<%
					do while not oAdminRs.EOF 
						Response.Write(oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")")
						oAdminRs.MoveNext
						if not oAdminRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oManager.OwnerTypeId = 3
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Managers: 
					<%
					do while not oManagerRs.EOF
						Response.Write(oManagerRs("FullName"))
						oManagerRs.MoveNext
						if not oManagerRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if
			
			oCompanyL3.LoadCompanyById(oCompanyL3Rs("CompanyL3Id"))
			
			if bPhoneSearch and oCompanyL3.Phone <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Phone: <% = oCompanyL3.Phone %>
							<% if oCompanyL3.PhoneExt <> "" then %>
							ext. <% = oCompanyL3.PhoneExt %>
							<% end if %>
							</td>
						</tr>			
				<%
				sColor = GetNextColor(sColor)
			end if 
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oCompanyL3.Address <> "" or oCompanyL3.City <> "" or oCompanyL3.StateId <> 0 or oCompanyL3.Zipcode <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Address:
							<%
							if bSpecAddressSearch and oCompanyL3.Address <> "" then
								Response.Write(oCompanyL3.Address)
								if oCompanyL3.Address2 <> "" then
									Response.Write(" " & oCompanyL3.Address2)
								end if
								if (bSpecCitySearch and oCompanyL3.City <> "") or (bSpecStateSearch and oCompanyL3.StateId <> "") or _
									(bSpecZipSearch and oCompanyL3.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecCitySearch and oCompanyL3.City <> "" then
								Response.Write(oCompanyL3.City)
								if (bSpecStateSearch and oCompanyL3.StateId <> "") or (bSpecZipSearch and oCompanyL3.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecStateSearch and oCompanyL3.StateId <> "" then
								Response.Write(GetStateName(oCompanyL3.StateId, 1))
								if (bSpecZipSearch and oCompanyL3.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecZipSearch and oCompanyL3.Zipcode <> "" then
								Response.Write(oCompanyL3.Zipcode)
								if oCompanyL3.ZipcodeExt <> "" then	
									Response.Write("-" & oCompanyL3.ZipcodeExt)
								end if
							end if										
							%>
							</td>
						</tr>			
				<%
					sColor = GetNextColor(sColor)
				end if 
			end if 
	
			
			if bLicCompanyL3Search then
			
				oLicense.SearchStateIdList = session("SearchReportStateIdList")
				oLicense.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oLicense.OwnerTypeId = 5
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					%>
						<tr>
							<td colspan="2"<% = sColor %>>
							<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %><% if oBranchLicRs("LicenseExpDate") <> "" then %>, Expires: <% = oBranchLicRs("LicenseExpDate") %><% end if %>
							<%
							if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% if oBranchLicRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oBranchLicRs("LicenseAppDeadline") %>
							<%
							if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% end if %>
							<% if bLicIssuedCompanyL3Search and oBranchLicRs("IssueDate") <> "" then %>, Issued: <% = oBranchLicRs("IssueDate") %><% end if %>
							<% if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then %> (<a href="/officers/licensedetail.asp?lid=<% = oBranchLicRs("LicenseId") %>">attachments</a>) <% end if %>
							</td>
						</tr>						
					<%
					sColor = GetNextColor(sColor)					
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL3Rs.MoveNext
					
		loop
	end if 
	
	
	
	
	if bBranchSearch and iCurPage = 1 then
	
		do while not oBranchRs.EOF
		
			%>
			<tr>
				<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modbranch.asp?id=<% = oBranchRs("BranchId") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Branch Information" alt="Edit Branch Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>" title="View Branch Information"><% = oBranchRs("Name") %><% if oBranchRs("BranchNum") <> "" then %> (#<% = oBranchRs("BranchNum") %>)<% end if %></a></td>
			</tr>
			<%
						
			sColor = GetNextColor(sColor)
			
			if bAdminSearch then
				set oAdminRs = oBranch.GetAdmins(oBranchRs("BranchId"))
				if not oAdminRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Administrators: 
					<%
					do while not oAdminRs.EOF 
						Response.Write(oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")")
						oAdminRs.MoveNext
						if not oAdminRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oBranchRs("BranchId")
				oManager.OwnerTypeId = 1
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					%>
					<tr>
						<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Managers: 
					<%
					do while not oManagerRs.EOF
						Response.Write(oManagerRs("FullName"))
						oManagerRs.MoveNext
						if not oManagerRs.EOF then Response.Write(", ")
					loop
					%>
						</td>
					</tr>
					<%
					sColor = GetNextColor(sColor)
				end if
			end if
			
			oBranch.LoadBranchById(oBranchRs("BranchId"))
			
			if bPhoneSearch and oBranch.Phone <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Phone: <% = oBranch.Phone %>
							<% if oBranch.PhoneExt <> "" then %>
							ext. <% = oBranch.PhoneExt %>
							<% end if %>
							</td>
						</tr>			
				<%
				sColor = GetNextColor(sColor)
			end if 
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oBranch.Address <> "" or oBranch.City <> "" or oBranch.StateId <> 0 or oBranch.Zipcode <> "" then
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Address:
							<%
							if bSpecAddressSearch and oBranch.Address <> "" then
								Response.Write(oBranch.Address)
								if oBranch.Address2 <> "" then
									Response.Write(" " & oBranch.Address2)
								end if
								if (bSpecCitySearch and oBranch.City <> "") or (bSpecStateSearch and oBranch.StateId <> "") or _
									(bSpecZipSearch and oBranch.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecCitySearch and oBranch.City <> "" then
								Response.Write(oBranch.City)
								if (bSpecStateSearch and oBranch.StateId <> "") or (bSpecZipSearch and oBranch.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecStateSearch and oBranch.StateId <> "" then
								Response.Write(GetStateName(oBranch.StateId, 1))
								if (bSpecZipSearch and oBranch.Zipcode <> "") then
									Response.Write(", ")
								end if
							end if
							if bSpecZipSearch and oBranch.Zipcode <> "" then
								Response.Write(oBranch.Zipcode)
								if oBranch.ZipcodeExt <> "" then	
									Response.Write("-" & oBranch.ZipcodeExt)
								end if
							end if										
							%>
							</td>
						</tr>			
				<%
					sColor = GetNextColor(sColor)
				end if 
			end if 

			
			if bLicBranchSearch then
			
				oLicense.SearchStateIdList = session("SearchReportStateIdList")
				oLicense.OwnerId = oBranchRs("BranchId")
				oLicense.OwnerTypeId = 2
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					%>
						<tr>
							<td colspan="2"<% = sColor %>>
							<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %><% if oBranchLicRs("LicenseExpDate") <> "" then %>, Expires: <% = oBranchLicRs("LicenseExpDate") %><% end if %>
							<%
							if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% if oBranchLicRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oBranchLicRs("LicenseAppDeadline") %>
							<%
							if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
								Response.Write("   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
							end if
							%>
							<% end if %>
							<% if bLicIssuedBranchSearch and oBranchLicRs("IssueDate") <> "" then %>, Issued: <% = oBranchLicRs("IssueDate") %><% end if %>
							<% if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then %> (<a href="/officers/licensedetail.asp?lid=<% = oBranchLicRs("LicenseId") %>">attachments</a>) <% end if %>
							</td>
						</tr>						
					<%
					sColor = GetNextColor(sColor)					
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oBranchRs.MoveNext
					
		loop	
	end if 


	if bOfficerSearch then 
	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
		
		oAssociate.SearchCourseName = session("SearchReportCourseName")
		oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
		oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")		
		oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
		oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
	
		'Set the number of records displayed on a page
		oRs.PageSize = iMaxRecs
		oRs.CacheSize = iMaxRecs
		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
		if clng(iCurPage) <= 0 then iCurPage = 1
		
		'Set the beginning record to be displayed on the page
		oRs.AbsolutePage = iCurPage
		
		'Initialize the row/col style values
		bCol = false
		'sColor = ""
		
	
		iCount = 0
		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
				'if oAssociate.UserId <> sPrevUserId then 
					%>
										<tr>
											<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/officers/modofficer.asp?id=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"></a>&nbsp;&nbsp;<% end if %><a href="/officers/officerdetail.asp?id=<% = oRs("UserId") %>" title="View Officer Information"><% = oAssociate.LastName %><% if bFirstNameSearch then %>, <% = oAssociate.FirstName %><% end if %></a>
											<% if bSsnSearch and oAssociate.Ssn <> "" then %> <% = enDeCrypt(oAssociate.Ssn,application("RC4Pass")) %><% end if %>
											<% if bEmailSearch and oAssociate.Email <> "" then %> (<% = oAssociate.Email %>)<% end if %>												
											</td>
										</tr>
					<%
					
					if bEmployeeIdSearch and oAssociate.EmployeeId <> "" then
						sColor=GetNextColor(sColor)
						%>
								<tr>
									<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Employee ID: <% = oAssociate.EmployeeId %></td>
								</tr>			
						<%
					end if 
					
					if bPhoneSearch and oAssociate.Phone <> "" then
						sColor=GetNextColor(sColor)
						%>
								<tr>
									<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Phone: <% = oAssociate.Phone %></td>
								</tr>			
						<%
					end if 
					
					if bWorkAddressSearch or bWorkCitySearch or bWorkStateSearch or bWorkZipSearch then
						sColor=GetNextColor(sColor)
						set oWorkAddressRs = oAssociate.GetWorkAddress
						if not oWorkAddressRs.EOF then
						if oWorkAddressRs("Address") <> "" or oWorkAddressRs("City") <> "" or oWorkAddressRs("StateId") <> 0 or oWorkAddressRs("Zipcode") <> "" then
						%>
								<tr>
									<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Office Address:
									<%
									'if not oWorkAddressRs.EOF then
										if bWorkAddressSearch and oWorkAddressRs("Address") <> "" then
											Response.Write(oWorkAddressRs("Address"))
											if oWorkAddressRs("Address2") <> "" then
												Response.Write(" " & oWorkAddressRs("Address2"))
											end if
											if (bWorkCitySearch and oWorkAddressRs("City") <> "") or (bWorkStateSearch and oWorkAddressRs("StateId") <> "") or _
												(bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												Response.Write(", ")
											end if
										end if
										if bWorkCitySearch and oWorkAddressRs("City") <> "" then
											Response.Write(oWorkAddressRs("City"))
											if (bWorkStateSearch and oWorkAddressRs("StateId") <> "") or (bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												Response.Write(", ")
											end if
										end if
										if bWorkStateSearch and oWorkAddressRs("StateId") <> "" then
											Response.Write(GetStateName(oWorkAddressRs("StateId"), 1))
											if (bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												Response.Write(", ")
											end if
										end if
										if bWorkZipSearch and oWorkAddressRs("Zipcode") <> "" then
											Response.Write(oWorkAddressRs("Zipcode"))
											if oWorkAddressRs("ZipcodeExt") <> "" then	
												Response.Write("-" & oWorkAddressRs("ZipcodeExt"))
											end if
										end if										
									'end if 
									%>
									</td>
								</tr>			
						<%
						end if 
						end if 
					end if 
					
					if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
						sColor=GetNextColor(sColor)
						if oAssociate.Address1.AddressLine1 <> "" or oAssociate.Address1.City <> "" or oAssociate.Address1.StateId <> 0 or oAssociate.Address1.Zipcode <> "" then
						%>
								<tr>
									<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">Address:
									<%
									if bSpecAddressSearch and oAssociate.Address1.AddressLine1 <> "" then
										Response.Write(oAssociate.Address1.AddressLine1)
										if oAssociate.Address1.AddressLine2 <> "" then
											Response.Write(" " & oAssociate.Address1.AddressLine2)
										end if
										if (bSpecCitySearch and oAssociate.Address1.City <> "") or (bSpecStateSearch and oAssociate.Address1.StateId <> "") or _
											(bSpecZipSearch and oAssociate.Address1.Zipcode <> "") then
											Response.Write(", ")
										end if
									end if
									if bSpecCitySearch and oAssociate.Address1.City <> "" then
										Response.Write(oAssociate.Address1.City)
										if (bSpecStateSearch and oAssociate.Address1.StateId <> "") or (bSpecZipSearch and oAssociate.Address1.Zipcode <> "") then
											Response.Write(", ")
										end if
									end if
									if bSpecStateSearch and oAssociate.Address1.StateId <> "" then
										Response.Write(GetStateName(oAssociate.Address1.StateId, 1))
										if (bSpecZipSearch and oAssociate.Address1.Zipcode <> "") then
											Response.Write(", ")
										end if
									end if
									if bSpecZipSearch and oAssociate.Address1.Zipcode <> "" then
										Response.Write(oAssociate.Address1.Zipcode)
										if oAssociate.Address1.ZipcodeExt <> "" then	
											Response.Write("-" & oAssociate.Address1.ZipcodeExt)
										end if
									end if										
									%>
									</td>
								</tr>			
						<%
						end if 
					end if 
					sColor = GetNextColor(sColor)
					
					
					
					
					if bLicenseSearch then 
					
						oLicense.SearchStateIdList = session("SearchReportStateIdList")
						oLicense.OwnerId = oAssociate.UserId
						set oAssocRs = oLicense.SearchLicenses					
					
						do while not oAssocRs.EOF 
						
							%>
								<tr>
									<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">License #<% = oAssocRs("LicenseNum") %> - <% = oLicense.LookupState(oAssocRs("LicenseStateId")) %>, Expires: <% = oAssocRs("LicenseExpDate") %>
									<%
									if oAssocRs("LicenseExpDate") < date() + 90 and oAssocRs("LicenseExpDate") > date() then
										Response.Write("   <b>(<font color=""#cc0000"">" & round(oAssocRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
									end if
									%>
									<% if oAssocRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oAssocRs("LicenseAppDeadline") %>
									<%
									if oAssocRs("LicenseAppDeadline") < date() + 90 and oAssocRs("LicenseAppDeadline") > date() then
										Response.Write("   <b>(<font color=""#cc0000"">" & round(oAssocRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
									end if
									%>
									<% end if %>
									</td>
								</tr>						
							<%
							oAssocRs.MoveNext
							
							sColor = GetNextColor(sColor)
							
						loop
					
					end if
					
										
					if bCourseSearch then
					
						set oAssocRs = oAssociate.SearchCourses(oAssociate.SearchCourseName, oAssociate.SearchCourseProviderId, oAssociate.SearchCourseCompletionDateFrom, oAssociate.SearchCourseCompletionDateTo, oAssociate.SearchCourseExpDateFrom, oAssociate.SearchCourseExpDateTo)
						do while not oAssocRs.EOF
						
							if oCourse.LoadCourseById(oAssocRs("CourseId")) <> 0 then
							
								if oAssocRs("UserSpecRenewalDate") <> "" then
									dCourseExpDate = oAssocRs("UserSpecRenewalDate")
								else
									dCourseExpDate = oAssocRs("CertificateExpirationDate")
								end if
							
								if session("SearchReportStateIdList") = "" or instr(1, "," & session("SearchReportStateIdList") & ",", "," & oCourse.StateId & ",") then
								if (iShowCompleted = 1 and oAssocRs("Completed")) or (iShowCompleted = 2 and not oAssocRs("Completed")) or (iShowCompleted = "" or iShowCompleted = 0) then
									%>
									<tr>
										<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1"><% = oCourse.Name %>, <% if oAssocRs("Completed") then response.write("Completion Date: " & oAssocRs("CompletionDate") & ",") end if %> Expires: <% = dCourseExpDate %>
										<%
									if dCourseExpDate < date() + 120 and dCourseExpDate > date() then
										Response.Write("   <b>(<font color=""#cc0000"">" & round(dCourseExpDate - date(), 0) & " Days</font>)</b>")
									elseif dCourseExpDate = date() then
										Response.Write("   <b>(<font color=""#cc0000"">Today</font>)</b>")
									end if	
										%>
										</td>
									</tr>						
									<%					

									sColor = GetNextColor(sColor)	
								end if 
								end if 				
							
							end if 
							oAssocRs.MoveNext
					
						loop

					end if
			else
			
				Response.Write("NOPE")
				
			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

				

		
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""officerreport.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""officerreport.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)	
		'response.write("</table>" & vbcrlf)
	
		'display Page number
		'response.write("<table width=""100%"">" & vbcrlf)	
		'response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		'response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)		
		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%
	else
		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;</a></td>
										</tr>
		<%
		'response.write("<tr><td colspan=""4"">There are currently no Loan Officers that matched your search criteria.</td></tr>" & vbcrlf)
	end if
	end if 
									
%>
									</table>
								</td>
							</tr>
						</table>





								</td>
                    <tr>
					<td class="bckWhiteTopBorder" align="center">
						<table cellpadding="4" cellspacing="10" border="0">
							<tr>
								<td align="center" valign="top" width="125">
									<a href="officerreportpf.asp"><img src="/media/images/icon_pdf.gif" width="50" height="50" alt="View PDF" border="0"><br><b>View Results<br> PDF</a></b>
								</td>
								<td align="center" valign="top" width="125">
									<a href="officerreportexcel.asp"><img src="/media/images/icon_excel.gif" width="50" height="50" alt="View Spreadsheet" border="0"><br><b>View Results<br> Spreadsheet</a></b>
								</td>						
							<% if bLicenseSearch and GetUserBranchIdList <> "" then %>
								<td align="center" valign="top" width="125">
									<a href="OfficerLicenseReportByBranchPDF.asp"><img src="/media/images/icon_pdf.gif" width="50" height="50" alt="View PDF" border="0"><br><b>View Officer Licenses<br>By Branch<br>PDF</a></b>
								</td>								
								<td align="center" valign="top" width="125">
									<a href="OfficerLicenseReportByBranchExcel.asp"><img src="/media/images/icon_excel.gif" width="50" height="50" alt="View PDF" border="0"><br><b>View Officer Licenses<br>By Branch<br>Spreadsheet</a></b>
								</td>								
							<% end if %>
							</tr>
							<tr>
								<td colspan="4" align="center">
									<b><a href="emailpdf.asp">Email Results PDF</a></b><br>
									<b><a href="OfficerReportAttachments.asp?page_number=1">View All Attachments</a></b><br>
								</td>
							</tr>
						</table>															
					</td></tr>
							</tr>
						</table>

						
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oAssociate = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>


<%
sub ClearReportSessionVars()

	session("SearchReportSavedName") = empty

	session("SearchReportLastName") = empty
	session("SearchReportSsn") = empty
	session("SearchReportStateIdList") = empty
	session("SearchReportBranchIdList") = empty
	session("SearchReportCompanyL2IdList") = empty
	session("SearchReportCompanyL3IdList") = empty
	
	session("SearchReportStructureIdList") = empty

	session("SearchReportCourseName") = empty
	session("SearchReportProviderId") = empty
	session("SearchReportCreditHours") = empty
	
	session("SearchReportLicenseStatusId") = empty
	session("SearchReportLicenseStatusIdList") = empty
	session("SearchReportLicenseNumber") = empty
	session("SearchReportLicenseExpDateFrom") = empty
	session("SearchReportLicenseExpDateTo") = empty

	session("SearchReportShowLicenses") = empty
	session("SearchReportShowLicIssued") = empty
	session("SearchReportShowCourses") = empty

	session("SearchReportShowAdmins") = empty
	session("SearchReportShowManagers") = empty
	session("SearchReportShowOfficers") = empty
	session("SearchReportShowCompany") = empty
	session("SearchReportShowBranches") = empty
	session("SearchReportShowLicBranches") = empty
	session("SearchReportShowLicIssuedBranches") = empty
	session("SearchReportShowCompanyL2s") = empty
	session("SearchReportShowLicCompanyL2s") = empty
	session("SearchReportShowLicIssuedCompanyL2s") = empty
	session("SearchReportShowCompanyL3s") = empty
	session("SearchReportShowLicCompanyL3s") = empty
	session("SearchReportShowLicIssuedCompanyL3s") = empty
	
	session("SearchReportShowFirstName") = empty
	session("SearchReportShowLastName") = empty
	session("SearchReportShowSsn") = empty
	session("SearchReportShowEmployeeId") = empty
	session("SearchReportShowPhone") = empty
	session("SearchReportShowEmail") = empty 
	session("SearchReportShowWorkAddress") = empty
	session("SearchReportShowWorkCity") = empty
	session("SearchReportShowWorkState") = empty
	session("SearchReportShowWorkZip") = empty
	session("SearchReportShowSpecAddress") = empty
	session("SearchReportShowSpecCity") = empty
	session("SearchReportShowSpecState") = empty
	session("SearchReportShowSpecZip") = empty
	
	session("SearchReportInactive") = empty

	'this one is on the way out...
	session("SearchReportListType") = empty

end sub


function GetNextColor(p_sColor)

	if p_sColor = "" then
		GetNextColor = " bgcolor=""#ffffff"""
	else
		GetNextColor = ""
	end if
			
end function

%>
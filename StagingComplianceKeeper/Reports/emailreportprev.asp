<%
'option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

dim iOfficerId
'iOfficerId = ScrubForSql(request("OfficerId"))

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	AlertError("Failed to load the passed Company ID.")
	Response.end
		
end if 


oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1

if (trim(session("SearchReportLastName")) <> "") then
	oAssociate.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociate.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociate.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociate.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociate.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociate.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociate.Inactive = session("SearchReportInactive")
end if
	
sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList
	
sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next
'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

oAssociate.SearchBranchIdList = sBranchIdList
if (oAssociate.SearchBranchIdList = "") then	
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociate.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociate.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociate.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociate.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociate.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
end if
	
if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociate.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociate.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociate.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if
if iShowCompleted = 1 then
	oAssociate.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociate.SearchCourseCompleted = false
else
	oAssociate.SearchCourseCompleted = ""
end if

'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
end if

dim oRs 'Recordset

set oRs = oAssociate.SearchAssociatesCoursesLicenses()

dim sOfficerList 'List of officer names to whom we'll send the email.
dim sExcludeList 'List of officer names with invalid emails.

if oRs.EOF and oRs.BOF then

	HandleError("There is no report list for this email.  You must first generate an officer report before emailing the officers.")
	Response.End

else

	do while not oRs.EOF
	
		if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
			if oAssociate.Email = "" then 
			
				sExcludeList = sExcludeList & "<a href=""/officers/officerdetail.asp?id=" & oAssociate.UserId & """>" & oAssociate.FullName & "</a>, "
				
			else
		
				sOfficerList = sOfficerList & "<a href=""/officers/officerdetail.asp?id=" & oAssociate.UserId & """>" & oAssociate.FullName & "</a> (" & oAssociate.Email & "), "
				
			end if
			
		end if			
			
		oRs.MoveNext
		
	loop
	
	if sOfficerList <> "" then
		sOfficerList = left(sOfficerList, len(sOfficerList) - 2)
	else
		AlertError("There were no officers with valid email addresses in the report results.  Please update the officer email address records and try again.")
	end if
	if sExcludeList <> "" then
		sExcludeList = left(sExcludeList, len(sExcludeList) - 2)
	end if 
	
end if


dim oBranch 'Branch class object
dim sBranchAdminList 'String to report on the gathered IDs

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

'Search out the branch admins for any branches that have been specifically
'selected in the report.
if session("SearchReportBranchIdList") <> "" then

	set oRs = oBranch.GetBranchAdmins(session("SearchReportBranchIdList"), session("UserCompanyId"))
	do while not oRs.EOF
	
		if oRs("Email") <> "" then

			sBranchAdminList = sBranchAdminList & oRs("FirstName") & " "
			if trim(oRs("MiddleName")) <> "" then
				sBranchAdminList = sBranchAdminList & oRs("MiddleName") & ". "
			end if
			sBranchAdminList = sBranchAdminList & oRs("LastName") & " (" & oRs("Email") & "), "
		
		end if
		
		oRs.MoveNext
		
	loop
	
	'Clear out the trailing comma
	if sBranchAdminList <> "" then
		sBranchAdminList = left(sBranchAdminList, len(sBranchAdminList) - 2)
	end if
	
end if


'Create the Fileup object that will allow us to upload files
dim oFileUp
set oFileUp = server.CreateObject("softartisans.fileup")

if oFileUp.Form("EmailAttachment").UserFileName <> "" then
	oFileUp.Form("EmailAttachment").SaveAs application("sAbsAttachmentsFolder") & session("User_Id") & oFileUp.Form("EmailAttachment").UserFileName
end if 


%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Email -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Send Email</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Email Officers</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="EmailOfficerForm" action="emailreportproc.asp" method="POST" enctype="multipart/Form-Data" onSubmit="return Validate(this)">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>Email Recipients: </b>
		</td>
		<td align="left" valign="top">
			<% = sOfficerList %>
		</td>
	</tr>
	<% if sExcludeList <> "" then %>
	<tr>
		<td align="left" valign="top">
			<b>Invalid Recipients: </b>
		</td>
		<td align="left" valign="top">
			<i>The following recipients were excluded due to missing or invalid email addresses:</i> <br><% = sExcludeList %>
		</td>
	</tr>
	<% end if %>
	<% if sBranchAdminList <> "" then %>
	<tr>
		<td align="left" valign="top">
			<b>Administrative Recipients: </b>
		</td>
		<td align="left" valign="top">
			<i>The following branch administrators will be copied on the email:</i> <br><% = sBranchAdminList %>
		</td>
	</tr>
	<% end if %>
	<tr>
		<td align="left" valign="top">
			<b>Subject: </b>
		</td>
		<td align="left" valign="top">
			<% = oFileUp.Form("Subject") %>
			<input type="hidden" name="Subject" value="<% = oFileUp.Form("Subject") %>">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Body: </b>
		</td>
		<td align="left" valign="top">
			<% = oFileUp.Form("EmailText") %>
			<input type="hidden" name="EmailText" value="<% = oFileUp.Form("EmailText") %>">
		</td>
	</tr>
	<% if oFileUp.Form("EmailAttachment").UserFileName <> "" then %>
	<tr>
		<td align="left" valign="top">
			<b>Attachment: </b>
		</td>
		<td align="left" valign="top">
			<% = oFileUp.Form("EmailAttachment").UserFileName %>
			<!--<input type="file" name="EmailAttachment">-->
			<input type="hidden" name="EmailAttachment" value="<% = oFileUp.Form("EmailAttachment").UserFileName %>">
		</td>
	</tr>
	<% end if %>
	<% if oFileUp.Form("LocalFileAttachmentId") <> "" then %>
	<tr>
		<td align="left" valign="top">
			<b>Local Attachment: </b>
		</td>
		<td align="left" valign="top">
			<img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oFileUp.Form("LocalFileAttachmentName") %>
			<input type="hidden" name="LocalFileAttachmentId" value="<% = oFileUp.Form("LocalFileAttachmentId") %>">
			<input type="hidden" name="LocalFileAttachmentName" value="<% = oFileUp.Form("LocalFileAttachmentName") %>">
		</td>
	</tr>
	<% end if %>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<b>Are you sure you would like to send the above email?</b>
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
set oAssociate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
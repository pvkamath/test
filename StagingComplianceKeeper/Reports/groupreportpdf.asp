<%
'option explicit
server.ScriptTimeout = 1000
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Group.Class.asp" -------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oGroup
dim oCompany
dim oLicense

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oGroup = new Group
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")
oGroup.CompanyID = session("UserCompanyId")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

dim bGroupSearch
dim bEmployeeSearch
dim bLicenseSearch
dim bGroupLeaderSearch
dim bEmailSearch
dim bAssociateTypeSearch
dim bExpirationDateSearch
	
if (trim(session("SearchReportGroupName")) <> "") then
	oGroup.GroupName = session("SearchReportGroupName")
end if

if (trim(session("SearchReportLastName")) <> "") then
	oGroup.EmployeeLastNameSearch = session("SearchReportLastName")
end if

if (trim(session("SearchReportAssociateTypeID")) <> "") then
	oGroup.AssociateTypeIDSearch = session("SearchReportAssociateTypeID")
end if

if (trim(session("SearchReportLicenseNumber")) <> "") then
	oGroup.LicenseNumSearch = session("SearchReportLicenseNumber")
end if

if session("SearchReportShowGroups") <> "" then
	bGroupSearch = session("SearchReportShowGroups")
end if
	
if session("SearchReportShowEmployees") <> "" then
	bEmployeeSearch = session("SearchReportShowEmployees")
end if
	
if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if		

if session("SearchReportShowGroupLeader") <> "" then
	bGroupLeaderSearch = session("SearchReportShowGroupLeader")
end if
	
if session("SearchReportShowEmail") <> "" then
	bEmailSearch = session("SearchReportShowEmail")
end if		

if session("SearchReportShowAssociateType") <> "" then
	bAssociateTypeSearch = session("SearchReportShowAssociateType")
end if

if session("SearchReportShowExpirationDate") <> "" then
	bExpirationDateSearch = session("SearchReportShowExpirationDate")
end if
	
dim oWorkAddressRs
dim oCompanyLicRs
dim oBranchLicRs
dim oCompanyL2LicRs
dim oCompanyL3LicRs


dim oPdf
dim oDoc
dim oPage
dim oDest
dim oTimesFont
dim oFimesFontBold
dim oCanvas
dim oImage
dim sParams
dim oParam
dim sText

set oPdf = Server.CreateObject("Persits.PDF")
oPDF.RegKey = "IWez17RlvL6PIQc213KacCXF1eD2xzmp+C8FGLDD04ptU96y5Mi8KtfWC9XKP4Nl6qpjPqlOSwG3"
set oDoc = oPdf.CreateDocument

oDoc.Creator = "ComplianceKeeper.com"

set oPage = oDoc.Pages.Add(612, 792)
set oFont = oDoc.Fonts("Arial")


'Write title
sParams = "x=0; y=630; width=612; alignment=center; size=25"
oPage.Canvas.DrawText "Compliance Report", sParams, oFont
sParams = "x=0; y=600; width=612; alignment=center; size=10"
oPage.Canvas.DrawText "Report Generated: " & Now(), sParams, oFont



'Draw line
with oPage.Canvas
	.MoveTo 50, 580
	.LineTo 562, 580
	.ClosePath
	.Stroke
end with


'Print CK Logo
set oImage = oDoc.OpenImage(server.MapPath("/Media/Images/logo.gif"))
set oParam = oPdf.CreateParam
oParam("x") = 30
oParam("y") = 650
oPage.Canvas.DrawImage oImage, oParam


'Print Company logo
if session("CorpLogoFileName") <> "" then
	set oImage = oDoc.OpenImage(server.MapPath("/usermedia/images/uploads/AssocLogos/" & session("CorpLogoFileName")))
	oParam.Clear
	oParam("x") = 306
	oParam("y") = 650
	oPage.Canvas.DrawImage oImage, oParam
end if


dim aList
dim iListId
dim oListRs
dim iCharsPrinted



sText = "<b>Reporting Criteria: </b><br>"

if (trim(session("SearchReportGroupName")) <> "") then
	sText = sText & "Group name contains: " & session("SearchReportGroupName") & "<br>"
end if

if (trim(session("SearchReportLastName")) <> "") then
	sText = sText & "Employee last name contains: " & session("SearchReportLastName") & "<br>"
end if

if (trim(session("SearchReportAssociateTypeID")) <> "") then
	sText = sText & "Associate Type is: " & session("SearchReportAssociateTypeID") & "<br>"
end if

if (trim(session("SearchReportLicenseNumber")) <> "") then
	sText = sText & "License number contains: " & session("SearchReportLicenseNumber") & "<br>"
end if

if bGroupSearch then
	sText = sText & "Show Groups<br>"
end if		

if bEmployeeSearch then
	sText = sText & "Show Employees<br>"
end if

if bLicenseSearch then
	sText = sText & "Show Licenses<br>"
end if		

if bGroupLeaderSearch then
	sText = sText & "Show group leader<br>"
end if

if bEmailSearch then 
	sText = sText & "Show employee email address<br>"
end if
	
if bAssociateTypeSearch then 
	sText = sText & "Show employee type<br>"
end if

if bExpirationDateSearch then
	sText = sText & "Show license expiration date<br>"
end if
	
'Write out the criteria across multiple pages
sParams = "x=50; y=560; width=500; height=550; alignment=center; size=10; html=true"
do while len(sText) > 0
	iCharsPrinted = oPage.Canvas.DrawText(sText, sParams, oFont)
	
	if iCharsPrinted = len(sText) then exit do
	
	set oPage = oPage.NextPage
	
	sText = "<b>Reporting Criteria (continued): </b><br>" & right(sText, len(sText) - iCharsPrinted)
	sParams = "x=50; y=750; width=500; height=600; alignment=center; size=10; html=true"
loop
	

set oPage = oPage.NextPage
sText = ""






dim sPrevUserId
dim sSp
sSp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

dim oGroupRs 'Recordset to use within the associate listing
dim oAssociateRs
dim oLicenseRs

if bGroupSearch then
	set oGroupRs = oGroup.SearchGroups
	
	do while not oGroupRs.EOF

		sText = sText & "<p><b>" & oGroupRs("GroupName") & "</b>"
			
		if bGroupLeaderSearch then
			sText = sText & " - " & oGroupRs("LastName") & ", " & oGroupRs("FirstName")
		end if 
			
        sText = sText & "</p><br>"

        if bEmployeeSearch then
            oGroup.GroupID = oGroupRs("GroupID")
            set oAssociateRs = oGroup.GetEmployees

            do while not oAssociateRs.EOF
                sText = sText & "<p>&nbsp;&nbsp;" & oAssociateRs("LastName") & ", " & oAssociateRs("FirstName")

                if bEmailSearch and oAssociateRs("Email") <> "" then
                    sText = sText & " - " & oAssociateRs("Email")
                end if

                if bAssociateTypeSearch and oAssociateRs("AssociateTypeName") <> "" then
                    sText = sText & " - " & oAssociateRs("AssociateTypeName")
                end if

                sText = sText & "</p>"
                oAssociateRs.MoveNext
            loop
            set oAssociateRs = nothing
        end if

        if bLicenseSearch then
            oGroup.GroupID = oGroupRs("GroupID")
            set oLicenseRs = oGroup.GetLicenses

            do while not oLicenseRs.EOF
                sText = sText & "<p>&nbsp;&nbsp;License #" & oLicenseRs("LicenseNum") & " - " & oLicense.LookupState(oLicenseRs("LicenseStateId"))

                if bExpirationDateSearch then
                    if oLicenseRs("LicenseExpDate") <> "" then 
                        sText = sText & ", Expires: " & oLicenseRs("LicenseExpDate") 
                    end if
				    
				    if oLicenseRs("LicenseExpDate") < date() + 90 and oLicenseRs("LicenseExpDate") > date() then
					    sText = sText & "   <b>(<font color=""#cc0000"">" & round(oLicenseRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
				    end if
				    
				    if oLicenseRs("LicenseAppDeadline") <> "" then 
                        sText = sText & ", Renewal App Deadline: " & oLicenseRs("LicenseAppDeadline")
    				    if oLicenseRs("LicenseAppDeadline") < date() + 90 and oLicenseRs("LicenseAppDeadline") > date() then
	    				    sText = sText & "   <b>(<font color=""#cc0000"">" & round(oLicenseRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
		    		    end if
				    end if
			
                end if

                oLicenseRs.MoveNext							    

            loop

            set oLicenseRs = nothing
        end if

		oGroupRs.MoveNext
	loop
end if 
	
	

	
'Write out the records across multiple pages
sParams = "x=50; y=750; width=500; height=700; alignment=center; size=10; html=true"
do while len(sText) > 0
	iCharsPrinted = oPage.Canvas.DrawText(sText, sParams, oFont)
	
	if iCharsPrinted = len(sText) then exit do
	
	set oPage = oPage.NextPage
	
	sText = right(sText, len(sText) - iCharsPrinted)
	sParams = "x=50; y=750; width=500; height=700; alignment=center; size=10; html=true"
loop
	

oDoc.SaveHttp "attachment;filename=report.pdf"


set oGroup = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
'	PrintShellFooter SHOW_MENUS										' From shell.asp
'	EndPage														' From htmlElements.asp
%>
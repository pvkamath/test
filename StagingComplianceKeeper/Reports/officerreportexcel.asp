<%
'option explicit
server.ScriptTimeout = 1000
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Provider.Class.asp" -->
<!-- #include virtual = "/includes/Note.Class.asp" -->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
'	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
'	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
'	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs, oNoteRs
dim oAssociate
dim oCourse
dim oLicense
dim oCurLicense
dim oCompany
dim oBranch 'Branch object
dim oSavedReport
dim oProvider
dim oNote

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCurLicense = new License
oCurLicense.ConnectionString = application("sDataSourceName")
oCurLicense.VocalErrors = application("bVocalErrors")


set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1



dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList

dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access

dim bLicenseSearch
dim bLicCompanyL2Search
dim bLicCompanyL3Search
dim bLicBranchSearch
dim bLicIssuedCompanyL2Search
dim bLicIssuedCompanyL3Search
dim bLicIssuedBranchSearch
dim bLicIssuedSearch

dim bAgencyTypeSearch
dim bAgencyWebsiteSearch
	
dim iShowInactive
	
dim bAdminSearch	
dim bCourseSearch
dim bOfficerSearch
dim bCompanySearch
dim bBranchSearch
dim bCompanyL2Search
dim bCompanyL3Search

dim bFirstNameSearch
dim bLastNameSearch
dim bSsnSearch
dim bNMLSNumberSearch
dim bEmployeeIdSearch
dim bTitleSearch
dim bDepartmentSearch
dim bPhoneSearch
dim bEmailSearch
dim bWorkAddressSearch
dim bWorkCitySearch
dim bWorkStateSearch
dim bWorkZipSearch
dim bSpecAddressSearch
dim bSpecCitySearch
dim bSpecStateSearch
dim bSpecZipSearch
dim bAddress2AddressSearch
dim bAddress2CitySearch
dim bAddress2StateSearch
dim bAddress2ZipSearch
dim bAddress3AddressSearch
dim bAddress3CitySearch
dim bAddress3StateSearch
dim bAddress3ZipSearch
dim bAddress4AddressSearch
dim bAddress4CitySearch
dim bAddress4StateSearch
dim bAddress4ZipSearch
dim bCompanyNotesSearch
dim bDivisionNotesSearch
dim bRegionNotesSearch
dim bBranchNotesSearch
dim bOfficerNotesSearch

dim iShowCompleted

dim bFingerprintDeadlineSearch

dim oAdminRs

dim sProviderName
dim sProviderAddress
dim sProviderAddress2
dim sProviderCity
dim iProviderStateID
dim sProviderZipcode
dim sProviderPhoneNo
dim sProviderContactName
dim sProviderContactTitle
dim sInstructorName
dim sInstructorEducationLevel

if (trim(session("SearchReportLastName")) <> "") then
	oAssociate.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportHireDateFrom")) <> "") then
	oAssociate.SearchHireDateFrom = session("SearchReportHireDateFrom")
end if

if (trim(session("SearchReportHireDateTo")) <> "") then
	oAssociate.SearchHireDateTo = session("SearchReportHireDateTo")
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociate.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociate.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociate.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociate.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociate.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociate.Inactive = session("SearchReportInactive")
end if

sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList
	
sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next
'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

oAssociate.SearchBranchIdList = sBranchIdList
if (oAssociate.SearchBranchIdList = "") then	
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociate.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociate.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociate.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociate.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociate.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
end if
	
if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociate.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociate.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociate.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if
	
if session("SearchReportShowLicIssued") <> "" then
	bLicIssuedSearch = session("SearchReportShowLicIssued")
end if
	
if session("SearchReportShowAdmins") <> "" then
	bAdminSearch = session("SearchReportShowAdmins")
end if		
	
if session("SearchReportShowCourses") <> "" then
	bCourseSearch = session("SearchReportShowCourses")
end if		

if session("SearchReportShowOfficers") <> "" then
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if session("SearchReportShowCompany") <> "" then
	bCompanySearch = session("SearchReportShowCompany")
end if
	
if session("SearchReportShowBranches") <> "" then
	bBranchSearch = session("SearchReportShowBranches")
end if
	
if session("SearchReportShowLicBranches") <> "" then
	bLicBranchSearch = session("SearchReportShowLicBranches")
end if
	
if session("SearchReportShowLicIssuedBranches") <> "" then
	bLicIssuedBranchSearch = session("SearchReportShowLicIssuedBranches")
end if
	
if session("SearchReportShowCompanyL2s") <> "" then
	bCompanyL2Search = session("SearchReportShowCompanyL2s")
end if
	
if session("SearchReportShowLicCompanyL2s") <> "" then
	bLicCompanyL2Search = session("SearchReportShowLicCompanyL2s")
end if
	
if session("SearchReportShowLicIssuedCompanyL2s") <> "" then
	bLicIssuedCompanyL2Search = session("SearchReportShowLicIssuedCompanyL2s")
end if	
	
if session("SearchReportShowCompanyL3s") <> "" then
	bCompanyL3Search = session("SearchReportShowCompanyL3s")
end if
	
if session("SearchReportShowLicCompanyL3s") <> "" then
	bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
end if		

if session("SearchReportShowLicIssuedCompanyL3s") <> "" then
	bLicIssuedCompanyL3Search = session("SearchReportShowLicIssuedCompanyL3s")
end if		
	
if session("SearchReportShowFirstName") <> "" then
	bFirstNameSearch = session("SearchReportShowFirstName")
end if

if session("SearchReportShowLastName") <> "" then
	bLastNameSearch = session("SearchReportShowLastName")
end if

if session("SearchReportShowSsn") <> "" then
	bSsnSearch = session("SearchReportShowSsn")
end if
	
if session("SearchReportShowNMLSNumber") <> "" then
	bNMLSNumberSearch = session("SearchReportShowNMLSNumber")
end if

if session("SearchReportShowEmployeeId") <> "" then
	bEmployeeIdSearch = session("SearchReportShowEmployeeId")
end if

if session("SearchReportShowFingerprintDeadline") <> "" then
	bFingerprintDeadlineSearch = session("SearchReportShowFingerprintDeadline")
end if

if session("SearchReportShowTitle") <> "" then
	bTitleSearch = session("SearchReportShowTitle")
end if

if session("SearchReportShowDepartment") <> "" then
	bDepartmentSearch = session("SearchReportShowDepartment")
end if

if session("SearchReportShowPhone") <> "" then
	bPhoneSearch = session("SearchReportShowPhone")
end if

if session("SearchReportShowEmail") <> "" then
	bEmailSearch = session("SearchReportShowEmail")
end if
	
if session("SearchReportShowWorkAddress") <> "" then
	bWorkAddressSearch = session("SearchReportShowWorkAddress")
end if
	
if session("SearchReportShowWorkCity") <> "" then
	bWorkCitySearch = session("SearchReportShowWorkCity")
end if

if session("SearchReportShowWorkState") <> "" then
	bWorkStateSearch = session("SearchReportShowWorkState")
end if

if session("SearchReportShowWorkZip") <> "" then
	bWorkZipSearch = session("SearchReportShowWorkZip")
end if
	
'Address 1	
if session("SearchReportShowSpecAddress") <> "" then
	bSpecAddressSearch = session("SearchReportShowSpecAddress")
end if
	
if session("SearchReportShowSpecCity") <> "" then
	bSpecCitySearch = session("SearchReportShowSpecCity")
end if

if session("SearchReportShowSpecState") <> "" then
	bSpecStateSearch = session("SearchReportShowSpecState")
end if

if session("SearchReportShowSpecZip") <> "" then
	bSpecZipSearch = session("SearchReportShowSpecZip")
end if

'Address 2	
if session("SearchReportShowAddress2Address") <> "" then
	bAddress2AddressSearch = session("SearchReportShowAddress2Address")
end if
	
if session("SearchReportShowAddress2City") <> "" then
	bAddress2CitySearch = session("SearchReportShowAddress2City")
end if

if session("SearchReportShowAddress2State") <> "" then
	bAddress2StateSearch = session("SearchReportShowAddress2State")
end if

if session("SearchReportShowAddress2Zip") <> "" then
	bAddress2ZipSearch = session("SearchReportShowAddress2Zip")
end if

'Address 3	
if session("SearchReportShowAddress3Address") <> "" then
	bAddress3AddressSearch = session("SearchReportShowAddress3Address")
end if
	
if session("SearchReportShowAddress3City") <> "" then
	bAddress3CitySearch = session("SearchReportShowAddress3City")
end if

if session("SearchReportShowAddress3State") <> "" then
	bAddress3StateSearch = session("SearchReportShowAddress3State")
end if

if session("SearchReportShowAddress3Zip") <> "" then
	bAddress3ZipSearch = session("SearchReportShowAddress3Zip")
end if

'Address 4
if session("SearchReportShowAddress4Address") <> "" then
	bAddress4AddressSearch = session("SearchReportShowAddress4Address")
end if
	
if session("SearchReportShowAddress4City") <> "" then
	bAddress4CitySearch = session("SearchReportShowAddress4City")
end if

if session("SearchReportShowAddress4State") <> "" then
	bAddress4StateSearch = session("SearchReportShowAddress4State")
end if

if session("SearchReportShowAddress4Zip") <> "" then
	bAddress4ZipSearch = session("SearchReportShowAddress4Zip")
end if

if session("SearchReportShowSpecAddress") <> "" then
	bSpecAddressSearch = session("SearchReportShowSpecAddress")
end if
	
if session("SearchReportShowSpecCity") <> "" then
	bSpecCitySearch = session("SearchReportShowSpecCity")
end if

if session("SearchReportShowSpecState") <> "" then
	bSpecStateSearch = session("SearchReportShowSpecState")
end if

if session("SearchReportShowSpecZip") <> "" then
	bSpecZipSearch = session("SearchReportShowSpecZip")
end if

if session("SearchReportShowCompanyNotes") <> "" then
	bCompanyNotesSearch = session("SearchReportShowCompanyNotes")
end if

if session("SearchReportShowDivisionNotes") <> "" then
	bDivisionNotesSearch = session("SearchReportShowDivisionNotes")
end if

if session("SearchReportShowRegionNotes") <> "" then
	bRegionNotesSearch = session("SearchReportShowRegionNotes")
end if

if session("SearchReportShowBranchNotes") <> "" then
	bBranchNotesSearch = session("SearchReportShowBranchNotes")
end if

if session("SearchReportShowOfficerNotes") <> "" then
	bOfficerNotesSearch = session("SearchReportShowOfficerNotes")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if

if session("SearchReportShowAgencyType") <> "" then
    bAgencyTypeSearch = session("SearchReportShowAgencyType")
end if

if session("SearchReportShowAgencyWebsite") <> "" then
    bAgencyWebsiteSearch = session("SearchReportShowAgencyWebsite")
end if

if iShowCompleted = 1 then
	oAssociate.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociate.SearchCourseCompleted = false
else
	oAssociate.SearchCourseCompleted = ""
end if

'Update the license object properties based on the search we just did.
'We'll use these properties later to search for the specific license records.
oLicense.LicenseStatusId = oAssociate.SearchLicenseStatusId
if oLicense.LicenseStatusId = 0 then
	oLicense.LicenseStatusId = ""
end if
oLicense.SearchStatusIdList = oAssociate.SearchLicenseStatusIdList
oLicense.LicenseNum = oAssociate.SearchLicenseNumber
oLicense.SearchLicenseExpDateFrom = oAssociate.SearchLicenseExpDateFrom
oLicense.SearchLicenseExpDateTo = oAssociate.SearchLicenseExpDateTo
oLicense.SearchAppDeadline = oAssociate.SearchAppDeadline
	

'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
end if

set oRs = oAssociate.SearchAssociatesCoursesLicenses()
	

dim oWorkAddressRs
dim oCompanyLicRs
dim oBranchLicRs
dim oCompanyL2LicRs
dim oCompanyL3LicRs

'If we're searching Company licenses, run the search.
if bCompanySearch and not bCheckIsAdmin then
	bCompanySearch = false
end if
	
	
'If we're searching CompanyL2 licenses, run the search.
dim oCompanyL2
set oCompanyL2 = new CompanyL2
dim oCompanyL2Rs
if bCompanyL2Search then
	if session("SearchReportCompanyL2IdList") = "" and not bCheckIsAdmin then
		bCompanyL2Search = false
	end if
end if
if bCompanyL2Search then
	set oCompanyL2 = new CompanyL2
	oCompanyL2.ConnectionString = application("sDataSourceName")
	oCompanyL2.VocalErrors = application("bVocalErrors")
	oCompanyL2.CompanyId = session("UserCompanyId")
	
	oCompanyL2.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL2.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL2.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL2.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL2.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL2.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL2.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL2.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL2.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL2.Inactive = session("SearchReportInactive")
	
	set oCompanyL2Rs = oCompanyL2.SearchCompaniesLicenses
end if


'If we're searching CompanyL3 licenses, run the search.
dim oCompanyL3
set oCompanyL3 = new CompanyL3
dim oCompanyL3Rs
if bCompanyL3Search then
	if session("SearchReportCompanyL3IdList") = "" and not bCheckIsAdmin then
		bCompanyL3Search = false
	end if
end if
if bCompanyL3Search then
	set oCompanyL3 = new CompanyL3
	oCompanyL3.ConnectionString = application("sDataSourceName")
	oCompanyL3.VocalErrors = application("bVocalErrors")
	oCompanyL3.CompanyId = session("UserCompanyId")
	
	oCompanyL3.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL3.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL3.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL3.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oCompanyL3.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL3.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL3.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL3.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL3.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL3.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL3.Inactive = session("SearchReportInactive")
	
	set oCompanyL3Rs = oCompanyL3.SearchCompaniesLicenses
end if
	
	
'If we're searching Branch licenses, run the search.
dim oBranchRs
if bBranchSearch then
	if session("SearchReportBranchIdList") = "" and not bCheckIsAdmin then
		bBranchSearch = false
	end if
end if
if bBranchSearch then
	set oBranch = nothing
	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	oBranch.TpConnectionString = application("sTpDataSourceName")
	oBranch.VocalErrors = application("bVocalErrors")
	oBranch.CompanyId = session("UserCompanyId")
			
	oBranch.SearchStateIdList = session("SearchReportStateIdList")
	oBranch.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oBranch.SearchBranchIdList = session("SearchReportBranchIdList")
	oBranch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oBranch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oBranch.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oBranch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oBranch.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oBranch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oBranch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oBranch.SearchAppDeadline = session("SearchReportAppDeadline")
	oBranch.Inactive = session("SearchReportInactive")

	set oBranchRs = oBranch.SearchBranchesLicenses
end if

%>

<%
dim sText


'Response.ContentType = "application/vnd.ms-excel"
Response.ContentType = "application/download"
Response.AddHeader "Content-Disposition", "attachment; filename=" & "report.xls"	

'Write opening XML statements
%>
<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>13170</WindowHeight>
  <WindowWidth>18285</WindowWidth>
  <WindowTopX>690</WindowTopX>
  <WindowTopY>60</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <NumberFormat/>
  </Style>
  <Style ss:ID="s22">
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s23">
	<Font ss:Bold="1"/>
  </Style>
 </Styles>
 <Names>
  <NamedRange ss:Name="Branches" ss:RefersTo="=Branches!R1C1:R7C22"/>
  <NamedRange ss:Name="Company" ss:RefersTo="=Company!R1C1:R2C32"/>
  <NamedRange ss:Name="Licenses" ss:RefersTo="=Licenses!R1C1:R20C15"/>
  <NamedRange ss:Name="Officers" ss:RefersTo="=Officers!R1C1:R17C28"/>
 </Names>


<%

dim sSummaryText
dim sCoText
dim sCoNoteText
dim sBrText
dim sBrNoteText
dim sCo2Text
dim sCo2NoteText
dim sCo3Text
dim sCo3NoteText
dim sOffText
dim sCEText
dim sCoLicText
dim sBrLicText
dim sCo2LicText
dim sCo3LicText
dim sOffLicText
dim sOffNoteText
dim sCompanyL2WorksheetName
dim sCompanyL3WorksheetName

'Construct worksheet names of CompanyL2 and CompanyL3 sheets
if len(session("CompanyL2Name")) > 19 then
	sCompanyL2WorksheetName = left(session("CompanyL2Name"),19) & "..."
else
	sCompanyL2WorksheetName = session("CompanyL2Name")
end if

if len(session("CompanyL3Name")) > 19 then
	sCompanyL3WorksheetName = left(session("CompanyL3Name"),19) & "..."
else
	sCompanyL3WorksheetName = session("CompanyL3Name")
end if

'Company worksheet XML
sCoText = sCoText & "<Worksheet ss:Name=""Company""><Table><Row>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Legal Name</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp State</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp Date</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Org Type</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Ein</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">City</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Website</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone Ext</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2 Ext</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3 Ext</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fax</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address 2</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing City</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing State</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Zipcode</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Name</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone Ext</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Email</Data></Cell>"
sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Fax</Data></Cell>"
sCoText = sCoText & "</Row>"

'Company Lvl2 worksheet XML
sCo2Text = sCo2Text & "<Worksheet ss:Name=""" & sCompanyL2WorksheetName & " List""><Table><Row>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Legal Name</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp State</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp Date</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Org Type</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Ein</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">City</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Website</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone Ext</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2 Ext</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3 Ext</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fax</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address 2</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing City</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing State</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Zipcode</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Name</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone Ext</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Email</Data></Cell>"
sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Fax</Data></Cell>"
if session("CustField1Name") <> "" then 
	sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField1Name") & "</Data></Cell>"
end if
if session("CustField2Name") <> "" then 
	sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField2Name") & "</Data></Cell>"
end if
if session("CustField3Name") <> "" then 
	sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField3Name") & "</Data></Cell>"
end if
if session("CustField4Name") <> "" then 
	sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField4Name") & "</Data></Cell>"
end if
if session("CustField5Name") <> "" then 
	sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField5Name") & "</Data></Cell>"
end if
sCo2Text = sCo2Text & "</Row>"

'Company Lvl3 worksheet XML
sCo3Text = sCo3Text & "<Worksheet ss:Name=""" & sCompanyL3WorksheetName & " List""><Table><Row>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Legal Name</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp State</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Incorp Date</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Org Type</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Ein</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">City</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Website</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone Ext</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2 Ext</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3 Ext</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fax</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address 2</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing City</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing State</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Zipcode</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Name</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Phone Ext</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Email</Data></Cell>"
sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Main Contact Fax</Data></Cell>"
if session("CustField1Name") <> "" then 
	sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField1Name") & "</Data></Cell>"
end if
if session("CustField2Name") <> "" then 
	sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField2Name") & "</Data></Cell>"
end if
if session("CustField3Name") <> "" then 
	sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField3Name") & "</Data></Cell>"
end if
if session("CustField4Name") <> "" then 
	sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField4Name") & "</Data></Cell>"
end if
if session("CustField5Name") <> "" then 
	sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField5Name") & "</Data></Cell>"
end if
sCo3Text = sCo3Text & "</Row>"

'Branch worksheet XML
sBrText = "<Worksheet ss:Name=""Branch List""><Table><Row>"              
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Branch Number</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Legal Name</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">City</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode Ext</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone Ext</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fax</Data></Cell>"
'sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Address 2</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing City</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing State</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Zipcode</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Mailing Zipcode Ext</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Entity Lic Num</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">FHA Branch Id</Data></Cell>"
sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">VA Branch Id</Data></Cell>"
if session("CustField1Name") <> "" then 
	sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField1Name") & "</Data></Cell>"
end if
if session("CustField2Name") <> "" then 
	sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField2Name") & "</Data></Cell>"
end if
if session("CustField3Name") <> "" then 
	sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField3Name") & "</Data></Cell>"
end if
if session("CustField4Name") <> "" then 
	sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField4Name") & "</Data></Cell>"
end if
if session("CustField5Name") <> "" then 
	sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CustField5Name") & "</Data></Cell>"
end if
sBrText = sBrText & "</Row>"


'Officer worksheet XML
sOffText = "<Worksheet ss:Name=""Officer List""><Table><Row>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Association</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">FirstName</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">MiddleName</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Last Name</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">SSN</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date Of Birth</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Drivers License State</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Drivers License Number</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Hire Date</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Termination Date</Data></Cell>"

sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">NMLS Number</Data></Cell>"

sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Employee Id</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Title</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fingerprint Deadline Date</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Department</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone Ext</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 2 Ext</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Phone 3 Ext</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Fax</Data></Cell>"
if bWorkAddressSearch or bWorkCitySearch or bWorkStateSearch or bWorkZipSearch then
    sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address</Data></Cell>"
    sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address Line 2</Data></Cell>"
    sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">City</Data></Cell>"
    sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">StateId</Data></Cell>"
    sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Zipcode</Data></Cell>"
end if
'Added Address 1 back in to make room for work address
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 1 Address</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 1 Address Line 2</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 1 City</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 1 StateId</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 1 Zipcode</Data></Cell>"


sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2 Address</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2 Address Line 2</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2 City</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2 StateId</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 2 Zipcode</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 3 Address</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 3 Address Line 2</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 3 City</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 3 StateId</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 3 Zipcode</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 4 Address</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 4 Address Line 2</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 4 City</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 4 StateId</Data></Cell>"
sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Address 4 Zipcode</Data></Cell>"

for i = 1 to 15
	if session("OfficerCustField" & i & "Name") <> "" then 
		sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("OfficerCustField" & i & "Name") & "</Data></Cell>"
	end if
next
sOffText = sOffText & "</Row>"

'Company licensese worksheet XML
sCoLicText = "<Worksheet ss:Name=""Company Licenses""><Table><Row>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Cancellation Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status Date</Data></Cell>"
sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
sCoLicText = sCoLicText & "</Row>"

'Company L2 Licenses Worksheet XML
sCo2LicText = "<Worksheet ss:Name=""" & sCompanyL2WorksheetName & " Licenses""><Table><Row>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Cancellation Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status Date</Data></Cell>"
sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
sCo2LicText = sCo2LicText & "</Row>"

'Company L3 Licenses Worksheet XML
sCo3LicText = "<Worksheet ss:Name=""" & sCompanyL3WorksheetName & " Licenses""><Table><Row>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Cancellation Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status Date</Data></Cell>"
sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
sCo3LicText = sCo3LicText & "</Row>"

'Branch Licenses Worksheet XML
sBrLicText = "<Worksheet ss:Name=""Branch Licenses""><Table><Row>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Cancellation Date</Data></Cell>"
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status Date</Data></Cell>"
if bAgencyTypeSearch then
    sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Agency Type</Data></Cell>"
end if
if bAgencyWebsiteSearch then
    sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Agency Website</Data></Cell>"
end if
sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
sBrLicText = sBrLicText & "</Row>"

'Officer licenses XML
sOffLicText = "<Worksheet ss:Name=""Officer Licenses""><Table><Row>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Num</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Type</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Owner</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>"

sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Association</Data></Cell>"

'KAH
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">NMLS Number</Data></Cell>"


sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Exp Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Application Deadline</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Submission Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Received Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Issue Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Submission Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Cancellation Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Status Date</Data></Cell>"
sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Notes</Data></Cell>"
sOffLicText = sOffLicText & "</Row>"

'Officer courses XML
sCeText = "<Worksheet ss:Name=""Education""><Table><Row>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">First Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Last Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Credit Hours</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Completion Date</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Renewal Date</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Address</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Address2</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider City</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider State</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Zipcode</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Phone No.</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Contact Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Contact Title</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Instructor Name</Data></Cell>"
sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Instructor Education Level</Data></Cell>"
sCeText = sCeText & "</Row>"

'If Company Notes Search, create the workbook XML
if bCompanySearch and bCompanyNotesSearch then
	sCoNoteText = "<Worksheet ss:Name=""Company Notes""><Table><Row>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Poster</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Note</Data></Cell>" & _							 
							 "</Row>"	
end if

'If Division Notes Search, create the workbook XML
if bCompanyL2Search and bDivisionNotesSearch then
	sCo2NoteText = "<Worksheet ss:Name=""Division Notes""><Table><Row>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Poster</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Note</Data></Cell>" & _							 
							 "</Row>"	
end if

'If Region Notes Search, create the workbook XML
if bCompanyL3Search and bRegionNotesSearch then
	sCo3NoteText = "<Worksheet ss:Name=""Region Notes""><Table><Row>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Poster</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Note</Data></Cell>" & _							 
							 "</Row>"	
end if

'If Branch Notes Search, create the workbook XML
if bBranchSearch and bBranchNotesSearch then
	sBrNoteText = "<Worksheet ss:Name=""Branch Notes""><Table><Row>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Branch Number</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Branch Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Holder's Legal Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Poster</Data></Cell>" & _
	                         "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Note</Data></Cell>" & _							 
							 "</Row>"	
end if

'If Officer Notes Search, create the workbook XML
if bOfficerSearch and bOfficerNotesSearch then
	sOffNoteText = "<Worksheet ss:Name=""Officer Notes""><Table><Row>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">FirstName</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Last Name</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Email</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Date</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Poster</Data></Cell>" & _
							 "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Note</Data></Cell>" & _							 
							 "</Row>"	
end if

'Write title
sSummaryText = "<Worksheet ss:Name=""Summary""><Table>"
sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s23""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Name & "</Data></Cell></Row>"
sSummaryText = sSummaryText & "<Row></Row>"
sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s23""><Data ss:Type=""String"" x:Ticked=""1"">Compliance Report</Data></Cell></Row>"
sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Report Generated " & Now() & "</Data></Cell></Row>"
 


dim aList
dim iListId
dim oListRs
dim iCharsPrinted

sSummaryText = sSummaryText & "<Row></Row>"
sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s23""><Data ss:Type=""String"" x:Ticked=""1"">Reporting Criteria: </Data></Cell></Row>"
sSummaryText = sSummaryText & "<Row></Row>"

if (trim(session("SearchReportLastName")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Last name contains: " & session("SearchReportLastName") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportSsn")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">SSN contains: " & session("SearchReportSsn") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportHireDateFrom")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Hire Date from: " & session("SearchReportHireDateFrom") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportHireDateTo")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Hire Date To: " & session("SearchReportHireDateTo") & "</Data></Cell></Row>"
end if

if (trim(sessioN("SearchReportStateIdList")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">State is: "
	aList = split(session("SearchReportStateIdList"), ",")
	for each iListId in aList 
		sSummaryText = sSummaryText & GetStateName(iListId, 1) & ", "
	next
	sSummaryText = left(sSummaryText, len(sSummaryText) - 2)
	sSummaryText = sSummaryText & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportBranchIdList")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Branch is: " 
	aList = split(session("SearchReportBranchIdList"), ",")
	for each iListId in aList
		if oBranch.LoadBranchById(iListId) <> 0 then
			sSummaryText = sSummaryText & oBranch.Name & ", "
		end if
	next		
	sSummaryText = left(sSummaryText, len(sSummaryText) - 2)
	sSummaryText = sSummaryText & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportCompanyL3IdList")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CompanyL3Name") & " is: " 
	aList = split(session("SearchReportCompanyL3IdList"), ",")
	for each iListId in aList
		if oCompanyL3.LoadCompanyById(iListId) <> 0 then
			sSummaryText = sSummaryText & oCompanyL3.Name & ", "
		end if
	next		
	sSummaryText = left(sSummaryText, len(sSummaryText) - 2)
	sSummaryText = sSummaryText & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportCompanyL2IdList")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("CompanyL2Name") & " is: " 
	aList = split(session("SearchReportCompanyL2IdList"), ",")
	for each iListId in aList
		if oCompanyL2.LoadCompanyById(iListId) <> 0 then
			sSummaryText = sSummaryText & oCompanyL2.Name & ", "
		end if
	next			
	sSummaryText = left(sSummaryText, len(sSummaryText) - 2)
	sSummaryText = sSummaryText & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportCourseName")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course Name is: " & session("SearchReportCourseName") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	set oProvider = new Provider
	oProvider.ConnectionString = application("sDataSourceName")
	oProvider.VocalErrors = application("bVocalErrors")
	if oProvider.LoadProviderById(session("SearchReportProviderId")) <> 0 then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Provider Name is: " & oProvider.Name & "</Data></Cell></Row>"
	end if
	set oProvider = nothing
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Credit hours are: " & session("SearchReportCreditHours") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course completion date from: " & session("SearchReportCourseCompletionDateFrom") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course completion date to: " & session("SearchReportCourseCompletionDateTo") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course expiration date from: " & session("SearchReportCourseExpDateFrom") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Course expiration date to: " & session("SearchReportCourseExpDateTo") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License status is: " & oLicense.LookupLicenseStatus(session("SearchReportLicenseStatusId")) & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License status is: "
	aList = split(session("SearchReportLicenseStatusIdList"), ",")
	for each iListId in aList
		sSummaryText = sSummaryText & oLicense.LookupLicenseStatus(iListId) & ", "
	next
	sSummaryText = left(sSummaryText, len(sSummaryText) - 2)
	sSummaryText = sSummaryText & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License number contains: " & session("SearchReportLicenseNumber") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportLicensePayment")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License Payment contains: " & session("SearchReportLicensePayment") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date from: " & session("SearchReportLicenseExpDateFrom") & "</Data></Cell></Row>"
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date to: " & session("SearchReportLicenseExpDateTo") & "</Data></Cell></Row>"
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	if session("SearchReportAppDeadline") = "1" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to Renewal Application Deadline and License Exp. Date</Data></Cell></Row>"
	elseif session("SearchReportAppDeadline") = "3" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to Renewal Submission Date</Data></Cell></Row>"
	elseif session("SearchReportAppDeadline") = "4" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to Submission Date</Data></Cell></Row>"
	elseif session("SearchReportAppDeadline") = "5" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to Issue Date</Data></Cell></Row>"
	elseif session("SearchReportAppDeadline") = "2" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to Renewal Application Deadline</Data></Cell></Row>"
	else
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">License date range applies to License Expiration Date</Data></Cell></Row>"
	end if
end if

if session("SearchReportInactive") <> "" then
	if session("SearchReportInactive") = "1" then
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show only inactive records</Data></Cell></Row>"
	else
		sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show only active records</Data></Cell></Row>"
	end if
end if
	
if bAdminSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show administrators</Data></Cell></Row>"
end if		
	
if bCourseSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show courses</Data></Cell></Row>"
end if		

if bOfficerSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show loan officer matches</Data></Cell></Row>"
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if bLicenseSearch then 
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show loan officer licenses</Data></Cell></Row>"
end if
	
if bLicIssuedSearch then 
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & session("SearchReportShowLicIssued") & "Show loan officer license issue date</Data></Cell></Row>"
end if

if bCompanySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show company licenses</Data></Cell></Row>"
end if
	
if bBranchSearch <> "" then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show branch matches</Data></Cell></Row>"
end if
	
if bLicBranchSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show branch licenses</Data></Cell></Row>"
end if

if bAgencyTypeSearch then
    sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Agency Type</Data></Cell></Row>"
end if

if bAgencyWebsiteSearch then
    sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Agency Website</Data></Cell></Row>"
end if
	
if bLicIssuedBranchSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show branch license issue date</Data></Cell></Row>"
end if
	
if bCompanyL2Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL2Name")) & " matches</Data></Cell></Row>"
end if
	
if bLicCompanyL2Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL2Name")) & " licenses</Data></Cell></Row>"
end if
	
if bLicIssuedCompanyL2Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL2Name")) & " license issue date</Data></Cell></Row>"
end if	
	
if bCompanyL3Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL3Name")) & " matches</Data></Cell></Row>"
end if
	
if bLicCompanyL3Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL3Name")) & " licenses</Data></Cell></Row>"
	bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
end if		

if bLicIssuedCompanyL3Search then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show " & lcase(session("CompanyL3Name")) & " license issue date</Data></Cell></Row>"
end if		
	
if bFirstNameSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show First Name</Data></Cell></Row>"
end if

if bSsnSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show SSN</Data></Cell></Row>"
end if
	
if bNMLSNumberSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show NMLS Number</Data></Cell></Row>"
end if
	
if bEmployeeIdSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Employee ID</Data></Cell></Row>"
end if

if bTitleSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Title</Data></Cell></Row>"
end if

if bDepartmentSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Department</Data></Cell></Row>"
end if	

if bPhoneSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Phone Number</Data></Cell></Row>"
end if

if bEmailSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Email Address</Data></Cell></Row>"
end if
	
if bWorkAddressSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show work address</Data></Cell></Row>"
end if
	
if bWorkCitySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show work city</Data></Cell></Row>"
end if

if bWorkStateSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show work state</Data></Cell></Row>"
end if

if bWorkZipSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show work zipcode</Data></Cell></Row>"
end if
	
'Address 1	
if bSpecAddressSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show address</Data></Cell></Row>"
end if
	
if bSpecCitySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show city</Data></Cell></Row>"
end if

if bSpecStateSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show state</Data></Cell></Row>"
end if

if bSpecZipSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show zipcode</Data></Cell></Row>"
end if

'Address 2	
if bAddress2AddressSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show address 2</Data></Cell></Row>"
end if
	
if bAddress2CitySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show city 2</Data></Cell></Row>"
end if

if bAddress2StateSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show state 2</Data></Cell></Row>"
end if

if bAddress2ZipSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show zipcode 2</Data></Cell></Row>"
end if

'Address 3	
if bAddress3AddressSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show address 3</Data></Cell></Row>"
end if
	
if bAddress3CitySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show city 3</Data></Cell></Row>"
end if

if bAddress3StateSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show state 3</Data></Cell></Row>"
end if

if bAddress3ZipSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show zipcode 3</Data></Cell></Row>"
end if

'Address 4
if bAddress4AddressSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show address 4</Data></Cell></Row>"
end if
	
if bAddress4CitySearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show city 4</Data></Cell></Row>"
end if

if bAddress4StateSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show state 4</Data></Cell></Row>"
end if

if bAddress4ZipSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show zipcode 4</Data></Cell></Row>"
end if

if bFingerprintDeadlineSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Fingerprint Deadline</Data></Cell></Row>"
end if

if bOfficerSearch and bOfficerNotesSearch then
	sSummaryText = sSummaryText & "<Row><Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">Show Loan Officer Notes</Data></Cell></Row>"
end if

sSummaryText = sSummaryText & "</Table></Worksheet>"

Response.Write(sSummaryText)







dim sPrevUserId
dim sSp
sSp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

dim oAssocRs 'Recordset to use within the associate listing

	if bCompanySearch then
	
		oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
		oLicense.OwnerId = session("UserCompanyId")
		oLicense.OwnerTypeId = 3
		set oCompanyLicRs = oLicense.SearchLicenses
		oLicense.OwnerTypeId = 1

		'Instantiate Notes Class
		if bCompanyNotesSearch then
			set oNote = new Note

			oNote.ConnectionString = application("sDataSourceName")
			oNote.VocalErrors = application("bVocalErrors")
			oNote.OwnerTypeId = 3
		end if
	
		if not (oCompanyLicRs.BOF and oCompanyLicRs.EOF) then
			
			sCoText = sCoText & "<Row>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Name & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.LegalName & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompany.IncorpStateId, 1) & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.IncorpDate & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.LookupOrgType(oCompany.OrgTypeId) & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Ein & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Address & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Address2 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.City & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompany.StateId, 1) & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Zipcode & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Website & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Email & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Phone & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.PhoneExt & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Phone2 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.PhoneExt2 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Phone3 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.PhoneExt3 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Fax & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MailingAddress & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MailingAddress2 & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MailingCity & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompany.MailingStateId, 1) & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MailingZipcode & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MainContactName & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MainContactPhone & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MainContactPhoneExt & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MainContactEmail & "</Data></Cell>"
			sCoText = sCoText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.MainContactFax & "</Data></Cell>"
			sCoText = sCoText & "</Row>"
			
			'if bAdminSearch then
			'	set oAdminRs = oCompany.GetAdmins(session("UserCompanyId"))
			'	if not oAdminRs.EOF then
			'		sText = sText & sSp & "Administrators: "
			'		do while not oAdminRs.EOF 
			'			sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
			'			oAdminRs.MoveNext
			'			if not oAdminRs.EOF then sText = sText & ", "
			'		loop
			'		sText = sText & "<br>"
			'	end if
			'end if 

			if bCompanyNotesSearch then 
				oNote.OwnerId = oCompany.CompanyId
				oNote.NoteText = ""
				oNote.UserID = ""

				set oNoteRs = oNote.SearchNotes()
				oNote.ReleaseNote()

				do while not oNoteRs.EOF
					if oNote.LoadNoteById(oNoteRs("NoteId")) <> 0 then						
						'Retrieve any notes for this Associate
						sCoNoteText = sCoNoteText & "<Row>" & _ 
											"<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.Name & "</Data></Cell>" & _
											"<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompany.LegalName & "</Data></Cell>" & _
											"<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteDate & "</Data></Cell>" & _
											"<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.LookupUserName(oNote.UserId) & "</Data></Cell>" & _	
											"<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteText  & "</Data></Cell>" & _
											"</Row>"
					end if
							
					oNoteRs.MoveNext
				loop
																			
				set oNoteRs = nothing						
			end if		
			do while not oCompanyLicRs.EOF
			
				if oCurLicense.LoadLicenseById(oCompanyLicRs("LicenseId")) <> 0 then
					sCoLicText = sCoLicText & "<Row>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupCompanyNameById(oCurLicense.OwnerId) & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.CancellationDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseStatusDate & "</Data></Cell>"
					sCoLicText = sCoLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
					sCoLicText = sCoLicText & "</Row>"
				end if

				oCompanyLicRs.MoveNext
			loop
			
		end if 
	end if 
	
	
	if bCompanyL2Search then
		'Instantiate Notes Class
		if bDivisionNotesSearch then
			set oNote = new Note

			oNote.ConnectionString = application("sDataSourceName")
			oNote.VocalErrors = application("bVocalErrors")
			oNote.OwnerTypeId = 4
		end if
		do while not oCompanyL2Rs.EOF
		
			if oCompanyL2.LoadCompanyById(oCompanyL2Rs("CompanyL2Id")) <> 0 then
				sCo2Text = sCo2Text & "<Row>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Name & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.LegalName & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL2.IncorpStateId, 1) & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.IncorpDate & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.LookupOrgType(oCompanyL2.OrgTypeId) & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Ein & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Address & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Address2 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.City & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL2.StateId, 1) & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Zipcode & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Website & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Email & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Phone & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.PhoneExt & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Phone2 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.PhoneExt2 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Phone3 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.PhoneExt3 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Fax & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MailingAddress & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MailingAddress2 & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MailingCity & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL2.MailingStateId, 1) & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MailingZipcode & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MainContactName & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MainContactPhone & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MainContactPhoneExt & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MainContactEmail & "</Data></Cell>"
				sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.MainContactFax & "</Data></Cell>"
				if session("CustField1Name") <> "" then 
					sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.CustField1 & "</Data></Cell>"
				end if
				if session("CustField2Name") <> "" then 
					sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.CustField2 & "</Data></Cell>"
				end if
				if session("CustField3Name") <> "" then 
					sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.CustField3 & "</Data></Cell>"
				end if
				if session("CustField4Name") <> "" then 
					sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.CustField4 & "</Data></Cell>"
				end if
				if session("CustField5Name") <> "" then 
					sCo2Text = sCo2Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.CustField5 & "</Data></Cell>"
				end if				
				sCo2Text = sCo2Text & "</Row>"

			    if bDivisionNotesSearch then 
				    oNote.OwnerId = oCompanyL2.CompanyL2Id
				    oNote.NoteText = ""
				    oNote.UserID = ""

				    set oNoteRs = oNote.SearchNotes()
				    oNote.ReleaseNote()

				    do while not oNoteRs.EOF
					    if oNote.LoadNoteById(oNoteRs("NoteId")) <> 0 then						
						    'Retrieve any notes for this Associate
						    sCo2NoteText = sCo2NoteText & "<Row>" & _ 
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.Name & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL2.LegalName & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteDate & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.LookupUserName(oNote.UserId) & "</Data></Cell>" & _	
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteText  & "</Data></Cell>" & _
											    "</Row>"
					    end if
							
					    oNoteRs.MoveNext
				    loop
																			
				    set oNoteRs = nothing						
			    end if	
			end if

			'if bAdminSearch then
			'	set oAdminRs = oCompanyL2.GetAdmins(oCompanyL2Rs("CompanyL2Id"))
			'	if not oAdminRs.EOF then
			'		sText = sText & sSp & "Administrators: "
			'		do while not oAdminRs.EOF 
			'			sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
			'			oAdminRs.MoveNext
			'			if not oAdminRs.EOF then sText = sText & ", "
			'		loop
			'		sText = sText & "<br>"
			'	end if
			'end if 
			
			
			if bLicCompanyL2Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oLicense.OwnerTypeId = 4
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					if oCurLicense.LoadLicenseById(oBranchLicRs("LicenseId")) <> 0 then
						sCo2LicText = sCo2LicText & "<Row>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupCompanyL2NameById(oCurLicense.OwnerId) & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.CancellationDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseStatusDate & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
						sCo2LicText = sCo2LicText & "</Row>"
					end if
					oBranchLicRs.MoveNext
				loop
					
			end if 
			oCompanyL2Rs.MoveNext
					
		loop
	end if 
	
	
	if bCompanyL3Search then
		'Instantiate Notes Class
		if bRegionNotesSearch then
			set oNote = new Note

			oNote.ConnectionString = application("sDataSourceName")
			oNote.VocalErrors = application("bVocalErrors")
			oNote.OwnerTypeId = 5
		end if

		do while not oCompanyL3Rs.EOF
		
			if oCompanyL3.LoadCompanyById(oCompanyL3Rs("CompanyL3Id")) <> 0 then
				sCo3Text = sCo3Text & "<Row>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Name & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.LegalName & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL3.IncorpStateId, 1) & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.IncorpDate & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.LookupOrgType(oCompanyL3.OrgTypeId) & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Ein & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Address & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Address2 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.City & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL3.StateId, 1) & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Zipcode & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Website & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Email & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Phone & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.PhoneExt & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Phone2 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.PhoneExt2 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Phone3 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.PhoneExt3 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Fax & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MailingAddress & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MailingAddress2 & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MailingCity & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCompanyL3.MailingStateId, 1) & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MailingZipcode & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MainContactName & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MainContactPhone & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MainContactPhoneExt & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MainContactEmail & "</Data></Cell>"
				sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.MainContactFax & "</Data></Cell>"
				if session("CustField1Name") <> "" then 
					sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.CustField1 & "</Data></Cell>"
				end if
				if session("CustField2Name") <> "" then 
					sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.CustField2 & "</Data></Cell>"
				end if
				if session("CustField3Name") <> "" then 
					sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.CustField3 & "</Data></Cell>"
				end if
				if session("CustField4Name") <> "" then 
					sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.CustField4 & "</Data></Cell>"
				end if
				if session("CustField5Name") <> "" then 
					sCo3Text = sCo3Text & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.CustField5 & "</Data></Cell>"
				end if
				sCo3Text = sCo3Text & "</Row>"

			    if bRegionNotesSearch then 
				    oNote.OwnerId = oCompanyL3.CompanyL3Id
				    oNote.NoteText = ""
				    oNote.UserID = ""

				    set oNoteRs = oNote.SearchNotes()
				    oNote.ReleaseNote()

				    do while not oNoteRs.EOF
					    if oNote.LoadNoteById(oNoteRs("NoteId")) <> 0 then						
						    'Retrieve any notes for this Associate
						    sCo3NoteText = sCo3NoteText & "<Row>" & _ 
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.Name & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCompanyL3.LegalName & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteDate & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.LookupUserName(oNote.UserId) & "</Data></Cell>" & _	
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteText  & "</Data></Cell>" & _
											    "</Row>"
					    end if
							
					    oNoteRs.MoveNext
				    loop
																			
				    set oNoteRs = nothing						
			    end if	
			end if
			
			'if bAdminSearch then
			'	set oAdminRs = oCompanyL3.GetAdmins(oCompanyL3Rs("CompanyL3Id"))
			'	if not oAdminRs.EOF then
			'		sText = sText & sSp & "Administrators: "
			'		do while not oAdminRs.EOF 
			'			sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
			'			oAdminRs.MoveNext
			'			if not oAdminRs.EOF then sText = sText & ", "
			'		loop
			'		sText = sText & "<br>"
			'	end if
			'end if 
			
			if bLicCompanyL3Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oLicense.OwnerTypeId = 5
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					if oCurLicense.LoadLicenseById(oBranchLicRs("LicenseId")) <> 0 then
						sCo3LicText = sCo3LicText & "<Row>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupCompanyL3NameById(oCurLicense.OwnerId) & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.CancellationDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseStatusDate & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
						sCo3LicText = sCo3LicText & "</Row>"
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			oCompanyL3Rs.MoveNext
					
		loop
	end if 
	
	
	if bBranchSearch then
		'Instantiate Notes Class
		if bBranchNotesSearch then
			set oNote = new Note

			oNote.ConnectionString = application("sDataSourceName")
			oNote.VocalErrors = application("bVocalErrors")
			oNote.OwnerTypeId = 2
		end if

		do while not oBranchRs.EOF

			if oBranch.LoadBranchById(oBranchRs("BranchId")) <> 0 then		
				sBrText = sBrText & "<Row>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.BranchNum & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.BranchName & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.LegalName & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Address & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Address2 & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.City & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oBranch.StateId, 1) & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Zipcode & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.ZipcodeExt & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Phone & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.PhoneExt & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Fax & "</Data></Cell>"
				'sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.Email & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.MailingAddress & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.MailingAddress2 & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.MailingCity & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oBranch.MailingStateId, 1) & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.MailingZipcode & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.MailingZipcodeExt & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.EntityLicNum & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.FHABranchId & "</Data></Cell>"
				sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.VABranchId & "</Data></Cell>"
				if session("CustField1Name") <> "" then 
					sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.CustField1 & "</Data></Cell>"
				end if
				if session("CustField2Name") <> "" then 
					sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.CustField2 & "</Data></Cell>"
				end if
				if session("CustField3Name") <> "" then 
					sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.CustField3 & "</Data></Cell>"
				end if
				if session("CustField4Name") <> "" then 
					sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.CustField4 & "</Data></Cell>"
				end if
				if session("CustField5Name") <> "" then 
					sBrText = sBrText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.CustField5 & "</Data></Cell>"
				end if
				sBrText = sBrText & "</Row>"
			
			    if bBranchNotesSearch then 
				    oNote.OwnerId = oBranch.BranchId
				    oNote.NoteText = ""
				    oNote.UserID = ""

				    set oNoteRs = oNote.SearchNotes()
				    oNote.ReleaseNote()

				    do while not oNoteRs.EOF
					    if oNote.LoadNoteById(oNoteRs("NoteId")) <> 0 then						
						    'Retrieve any notes for this Associate
						    sBrNoteText = sBrNoteText & "<Row>" & _ 
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.BranchNum & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.BranchName & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oBranch.LegalName & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteDate & "</Data></Cell>" & _
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.LookupUserName(oNote.UserId) & "</Data></Cell>" & _	
											    "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteText  & "</Data></Cell>" & _
											    "</Row>"
					    end if
							
					    oNoteRs.MoveNext
				    loop
																			
				    set oNoteRs = nothing						
			    end if	
            
            end if
			
			'if bAdminSearch then
			'	set oAdminRs = oBranch.GetAdmins(oBranchRs("BranchId"))
			'	if not oAdminRs.EOF then
			'		sText = sText & sSp & "Administrators: "
			'		do while not oAdminRs.EOF 
			'			sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
			'			oAdminRs.MoveNext
			'			if not oAdminRs.EOF then sText = sText & ", "
			'		loop
			'		sText = sText & "<br>"
			'	end if
			'end if 
			
			if bLicBranchSearch then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oBranchRs("BranchId")
				oLicense.OwnerTypeId = 2
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					if oCurLicense.LoadLicenseById(oBranchLicRs("LicenseId")) <> 0 then
						sBrLicText = sBrLicText & "<Row>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupBranchNameById(oCurLicense.OwnerId) & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.CancellationDate & "</Data></Cell>"
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseStatusDate & "</Data></Cell>"
                        if bAgencyTypeSearch then
                            sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.AgencyTypeName & "</Data></Cell>"
                        end if
                        if bAgencyWebsiteSearch then
                            sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.AgencyWebsite & "</Data></Cell>"
                        end if
						sBrLicText = sBrLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
						sBrLicText = sBrLicText & "</Row>"
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if
			
            oBranchRs.MoveNext
					
		loop	
	end if 


	if bOfficerSearch then 
	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
		
		oAssociate.SearchCourseName = session("SearchReportCourseName")
		oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
		oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")		
		oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
		oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
		if iShowCompleted = 1 then
			oAssociate.SearchCourseCompleted = true
		elseif iShowCompleted = 2 then
			oAssociate.SearchCourseCompleted = false
		else
			oAssociate.SearchCourseCompleted = ""
		end if

		'Instantiate Notes Class
		if bOfficerNotesSearch then
			set oNote = new Note

			oNote.ConnectionString = application("sDataSourceName")
			oNote.VocalErrors = application("bVocalErrors")
			oNote.OwnerTypeId = 1
		end if
		
		iCount = 0
		do while not oRs.EOF
		
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then

				sOffText = sOffText & "<Row>"		
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">"
				if oAssociate.BranchId <> "" then												
					sOffText = sOffText & oAssociate.LookupBranchName(oAssociate.BranchId)
				elseif oAssociate.CompanyL3Id <> "" then
					sOffText = sOffText & oAssociate.LookupCompanyL3Name(oAssociate.CompanyL3Id)
				elseif oAssociate.CompanyL2Id <> "" then
					sOffText = sOffText & oAssociate.LookupCompanyL2Name(oAssociate.CompanyL2Id)
				end if	
				sOffText = sOffText & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Email & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.FirstName & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.MiddleName & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.LastName & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & enDeCrypt(oAssociate.SSN,application("RC4Pass")) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.DateOfBirth & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oAssociate.DriversLicenseStateId, 1) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.DriversLicenseNo & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.HireDate & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.TerminationDate & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.NMLSNumber & "</Data></Cell>"								
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.EmployeeId & "</Data></Cell>"				
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Title & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.FingerprintDeadlineDate & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Department & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Phone & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.PhoneExt & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Phone2 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Phone2Ext & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Phone3 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Phone3Ext & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Fax & "</Data></Cell>"


					if bWorkAddressSearch or bWorkCitySearch or bWorkStateSearch or bWorkZipSearch then
						set oWorkAddressRs = oAssociate.GetWorkAddress
						if not oWorkAddressRs.EOF then
								
							sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oWorkAddressRs("Address") & "</Data></Cell>"
							sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oWorkAddressRs("Address2") & "</Data></Cell>"
							sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oWorkAddressRs("City") & "</Data></Cell>"
							sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oWorkAddressRs("StateId"), 1) & "</Data></Cell>"
							
							sText = oWorkAddressRs("Zipcode")
							
							if oWorkAddressRs("ZipcodeExt") <> "" then	
								sText = sText & "-" & oWorkAddressRs("ZipcodeExt")
							end if
	
							sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sText & "</Data></Cell>"
									
						
						end if 
					end if 
	

				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address1.AddressLine1 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address1.AddressLine2 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address1.City & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oAssociate.Address1.StateId, 1) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address1.Zipcode				
				if oAssociate.Address1.ZipcodeExt <> "" then sOffText = sOffText & "-" & oAssociate.Address1.ZipcodeExt end if
				sOffText = sOffText & "</Data></Cell>"				
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address2.AddressLine1 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address2.AddressLine2 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address2.City & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oAssociate.Address2.StateId, 1) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address2.Zipcode				
				if oAssociate.Address2.ZipcodeExt <> "" then sOffText = sOffText & "-" & oAssociate.Address2.ZipcodeExt end if
				sOffText = sOffText & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address3.AddressLine1 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address3.AddressLine2 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address3.City & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oAssociate.Address3.StateId, 1) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address3.Zipcode				
				if oAssociate.Address3.ZipcodeExt <> "" then sOffText = sOffText & "-" & oAssociate.Address3.ZipcodeExt end if
				sOffText = sOffText & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address4.AddressLine1 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address4.AddressLine2 & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address4.City & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oAssociate.Address4.StateId, 1) & "</Data></Cell>"
				sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Address4.Zipcode				
				if oAssociate.Address4.ZipcodeExt <> "" then sOffText = sOffText & "-" & oAssociate.Address4.ZipcodeExt end if												
				sOffText = sOffText & "</Data></Cell>"

				for i = 1 to 15
					if session("OfficerCustField" & i & "Name") <> "" then 
						sOffText = sOffText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.GetOfficerCustField(i) & "</Data></Cell>"
					end if
				next
				sOffText = sOffText & "</Row>"
		
				
					if bLicenseSearch then 
					
						oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
						oLicense.OwnerId = oAssociate.UserId
						set oAssocRs = oLicense.SearchLicenses					
					
						do while not oAssocRs.EOF 
							if oCurLicense.LoadLicenseById(oAssocRs("LicenseId")) <> 0 then
								sOffLicText = sOffLicText & "<Row>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseNum & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupLicenseStatus(oCurLicense.LicenseStatusId) & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseType & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCurLicense.LicenseStateId, 1) & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LookupAssociateNameById(oCurLicense.OwnerId) & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LegalName & "</Data></Cell>"	

                                sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">"
				                if oAssociate.BranchId <> "" then												
					                sOffLicText = sOffLicText & oAssociate.LookupBranchName(oAssociate.BranchId)
				                elseif oAssociate.CompanyL3Id <> "" then
					                sOffLicText = sOffLicText & oAssociate.LookupCompanyL3Name(oAssociate.CompanyL3Id)
				                elseif oAssociate.CompanyL2Id <> "" then
					                sOffLicText = sOffLicText & oAssociate.LookupCompanyL2Name(oAssociate.CompanyL2Id)
				                end if	                                					
                                sOffLicText = sOffLicText & "</Data></Cell>"

                                'KAH
                                sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.NMLSNumber & "</Data></Cell>"
                                'sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"

                                sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseExpDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseAppDeadline & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubmissionDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.ReceivedDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.IssueDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.SubDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.CancellationDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.LicenseStatusDate & "</Data></Cell>"
								sOffLicText = sOffLicText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCurLicense.Notes & "</Data></Cell>"
								sOffLicText = sOffLicText & "</Row>"
							end if
							oAssocRs.MoveNext
						loop
					
					end if
					
										
					if bCourseSearch then
						set oProvider = new Provider
						oProvider.ConnectionString = application("sDataSourceName")
						oProvider.VocalErrors = application("bVocalErrors")
						
						set oAssocRs = oAssociate.SearchCourses(oAssociate.SearchCourseName, oAssociate.SearchCourseProviderId, oAssociate.SearchCourseCompletionDateFrom, oAssociate.SearchCourseCompletionDateTo, oAssociate.SearchCourseExpDateFrom, oAssociate.SearchCourseExpDateTo, oAssociate.SearchCourseCompleted, oAssociate.SearchCourseAccreditationTypeID)
						do while not oAssocRs.EOF
							sProviderName = ""
							sProviderAddress = ""
							sProviderAddress2 = ""
							sProviderCity = ""
							sProviderState = ""
							sProviderZipcode = ""
							sProviderPhoneNo = ""
							sProviderContactName = ""
							sProviderContactTitle = ""
							
							if oCourse.LoadCourseById(oAssocRs("CourseId")) <> 0 then
							
								if oAssocRs("UserSpecRenewalDate") <> "" then
									dCourseExpDate = oAssocRs("UserSpecRenewalDate")
								else
									dCourseExpDate = oAssocRs("CertificateExpirationDate")
								end if
								
								if session("SearchReportCourseStateIdList") = "" or instr(1, "," & session("SearchReportCourseStateIdList") & ",", "," & oCourse.StateId & ",") then
									'Get Provider Information

									'If ProviderID is zero, the course is from TrainingPro
									if oCourse.ProviderID = 0 then 
										sProviderName = "TrainingPro"
										sProviderAddress = "11350 McCormick Road"
										sProviderAddress2 = "Executive Plaza III, Suite 1001"
										sProviderCity = "Hunt Valley"
										sProviderState = "MD"
										sProviderZipcode = "21031"
										sProviderPhoneNo = "877-878-3600"
										sProviderContactName = "Chris Nickerson"
										sProviderContactTitle = "Chief Executive Officer"
										sInstructorName = "Online"
										sInstructorEducationLevel = "Online"
									elseif oProvider.LoadProviderById(oCourse.ProviderID) <> 0 then
										sProviderName = oProvider.Name
										sProviderAddress = oProvider.Address
										sProviderAddress2 = oProvider.Address2
										sProviderCity = oProvider.City
										iProviderStateID = oProvider.StateID
										sProviderZipcode = oProvider.Zipcode
										sProviderPhoneNo = oProvider.Phone
										sProviderContactName = oProvider.ContactName
										sProviderContactTitle = oProvider.ContactTitle
										sInstructorName = oCourse.InstructorName
										sInstructorEducationLevel = oCourse.InstructorEducationLevel
									end if
									
									sCeText = sCeText & "<Row>"							
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.FirstName & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.LastName & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Email & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderName & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCourse.Name & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oCourse.ContEdHours & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(oCourse.StateId, 1) & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssocRs("CompletionDate") & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & dCourseExpDate & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderAddress & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderAddress2 & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderCity & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & GetStateName(iProviderStateID, 1) & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderZipcode & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderPhoneNo & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderContactName & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sProviderContactTitle & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sInstructorName & "</Data></Cell>"
									sCeText = sCeText & "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & sInstructorEducationLevel & "</Data></Cell>"
									sCeText = sCeText & "</Row>"
								end if
							
							end if 
														
							oAssocRs.MoveNext
						loop
						set oProvider = nothing
					end if

					if bOfficerNotesSearch then 
						oNote.OwnerId = oAssociate.UserId
						oNote.NoteText = ""
						oNote.UserID = ""

						set oNoteRs = oNote.SearchNotes()
						oNote.ReleaseNote()

						do while not oNoteRs.EOF
							if oNote.LoadNoteById(oNoteRs("NoteId")) <> 0 then						
								'Retrieve any notes for this Associate
								sOffNoteText = sOffNoteText & "<Row>" & _ 
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.FirstName & "</Data></Cell>" & _
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.LastName & "</Data></Cell>" & _
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oAssociate.Email & "</Data></Cell>" & _
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteDate & "</Data></Cell>" & _
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.LookupUserName(oNote.UserId) & "</Data></Cell>" & _	
											      "<Cell ss:StyleID=""s21""><Data ss:Type=""String"" x:Ticked=""1"">" & oNote.NoteText  & "</Data></Cell>" & _
											      "</Row>"
							end if
							
							oNoteRs.MoveNext
						loop
																			
						set oNoteRs = nothing						
					end if	


								
			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

		if bOfficerNotesSearch then 
			set oNote = nothing
		end if

	else
	
		sText = sText & "<p> </p>"

	end if
	end if 
	
set oAssociate = nothing
set oRs = nothing
							
'Write out the report.
sCoText = sCoText & "</Table></Worksheet>"
sCo2Text = sCo2Text & "</Table></Worksheet>"
sCo3Text = sCo3Text & "</Table></Worksheet>"
sBrText = sBrText & "</Table></Worksheet>"
sOffText = sOffText & "</Table></Worksheet>"
sCoLicText = sCoLicText & "</Table></Worksheet>"
sCo2LicText = sCo2LicText & "</Table></Worksheet>"
sCo3LicText = sCo3LicText & "</Table></Worksheet>"
sBrLicText = sBrLicText & "</Table></Worksheet>"
sOffLicText = sOffLicText & "</Table></Worksheet>"
sCeText = sCeText & "</Table></Worksheet>"
sCoNoteText = sCoNoteText & "</Table></Worksheet>"
sBrNoteText = sBrNoteText & "</Table></Worksheet>"
sCo2NoteText = sCo2NoteText & "</Table></Worksheet>"
sCo3NoteText = sCo3NoteText & "</Table></Worksheet>"
if bCompanySearch then
	Response.Write sCoText
	Response.Write sCoLicText
    if bCompanyNotesSearch then
        Response.Write sCoNoteText
    end if
end if
if bCompanyL2Search then
	Response.Write sCo2Text
	if bLicCompanyL2Search then
		Response.Write sCo2LicText
	end if 
    if bDivisionNotesSearch then
        Response.Write sCo2NoteText
    end if
end if
if bCompanyL3Search then
	Response.Write sCo3Text
	if bLicCompanyL3Search then
		Response.Write sCo3LicText
	end if
    if bRegionNotesSearch then
        Response.Write sCo3NoteText
    end if
end if
if bBranchSearch then
	Response.Write sBrText
	if bLicBranchSearch then
		Response.Write sBrLicText
	end if
    if bBranchNotesSearch then
        Response.Write sBrNoteText
    end if
end if
if bOfficerSearch then
	Response.Write sOffText
	if bLicenseSearch then
		Response.Write sOffLicText
	end if
	if bCourseSearch then
		Response.Write sCeText
	end if
end if

if bOfficerSearch and bOfficerNotesSearch then
	Response.write sOffNoteText & "</Table></Worksheet>"
end if

Response.Write "</Workbook>"
%>
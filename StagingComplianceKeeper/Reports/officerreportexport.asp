<%
option explicit
server.ScriptTimeout = 1000
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------

dim oRs
dim oAssociate
dim oCourse
dim oLicense

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")

dim iShowDeleted
iShowDeleted = ScrubForSql(request("ShowDeleted"))
if (iShowDeleted = "3") then
	iShowDeleted = 3
else
	iShowDeleted = 1
end if 


'dim bSearchCourses
'dim bSearchLicenses


dim sStateIdList
sStateIdList = replace(request("StateIdList"), ", ", ",")

dim sBranchIdList
'sBranchIdList = replace(request("BranchIdList"), ", ", ",")
sBranchIdList = request("BranchIdList")



if ucase(sAction) = "NEW" then

	'clear values
	session("SearchReportLastName") = ""
	session("SearchReportSsn") = ""
	session("SearchReportLicenseNum") = ""
	session("SearchReportCompanyId") = ""
	session("SearchReportDeletedOfficers") = ""
	session("SearchReportStateIdList") = ""
	session("SearchReportBranchIdList") = ""
	session("SearchReportProviderId") = ""
	session("SearchReportCourseName") = ""
	session("SearchReportCreditHours") = ""
	session("SearchReportLicenseStatusId") = ""
	session("SearchReportLicenseNumber") = ""
	session("SearchReportLicenseExpDateFrom") = ""
	session("SearchReportLicenseExpDateTo") = ""
		
	session("SearchReportCourses") = ""
	session("SearchReportLicenses") = ""
	session("SearchReportInactive") = ""
	session("CurPage") = ""

else

	oAssociate.LastName = ScrubForSql(request("LastName"))
	if (oAssociate.LastName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLastName")) <> "") then
			oAssociate.LastName = session("SearchReportLastName")
		end if
	else
		session("SearchReportLastName") = oAssociate.LastName
	end if

	oAssociate.Ssn = enDeCrypt(ScrubForSql(request("Ssn")),application("RC4Pass"))
	if (oAssociate.Ssn = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportSsn")) <> "") then
			oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
		end if
	else
		session("SearchReportSsn") = enDeCrypt(oAssociate.Ssn,application("RC4Pass"))
	end if

	oAssociate.SearchStateIdList = sStateIdList
	if (oAssociate.SearchStateIdList = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(sessioN("SearchReportStateIdList")) <> "") then
			oAssociate.SearchStateIdList = session("SearchReportStateIdList")
		end if
	else
		session("SearchReportStateIdList") = oAssociate.SearchStateIdList
	end if	

	oAssociate.UserStatus = iShowDeleted
	if (oAssociate.UserStatus = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportDeletedOfficers")) <> "") then
			oAssociate.UserStatus = session("SearchReportDeletedOfficers")
		end if
	else
		session("SearchReportDeletedOfficers") = oAssociate.UserStatus
	end if
	
	oAssociate.BranchId = ScrubForSql(request("BranchId"))
	if (oAssociate.BranchId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportBranchId")) <> "") then
			oAssociate.BranchId = session("SearchReportBranchId")
		end if
	else
		session("SearchReportBranchId") = oAssociate.BranchId
	end if
	
	
	oAssociate.Inactive = ScrubForSql(request("ShowInactive"))
	if oAssociate.Inactive = "" then
		oAssociate.Inactive = false
	end if
	if session("SearchReportInactive") <> "" and ucase(sAction) <> "SEARCH" then
		oAssociate.Inactive = session("SearchReportInactive")
	else
		session("SearchReportInactive") = oAssociate.Inactive
	end if

	
	
	oAssociate.SearchBranchIdList = sBranchIdList
	if (oAssociate.SearchBranchIdList = "") and (ucase(sAction) <> "SEARCH") then	
		if (trim(session("SearchReportBranchIdList")) <> "") then
			oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
		end if
	else
		session("SearchReportBranchIdList") = oAssociate.SearchBranchIdList
	end if
	

	oAssociate.SearchCourseName = ScrubForSql(request("CourseName"))
	if (oAssociate.SearchCourseName = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCourseName")) <> "") then
			oAssociate.SearchCourseName = session("SearchReportCourseName")
		end if
	else
		session("SearchReportCourseName") = oAssociate.SearchCourseName
	end if
	
	oAssociate.SearchCourseProviderId = ScrubForSql(request("Provider"))
	if (oAssociate.SearchCourseProviderId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportProviderId")) <> "") then
			oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		end if
	else
		session("SearchReportProviderId") = oAssociate.SearchCourseProviderId
	end if
	
	oAssociate.SearchCourseCreditHours = ScrubForSql(request("CreditHours"))
	if (oAssociate.SearchCourseCreditHours = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportCreditHours")) <> "") then
			oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
		end if
	else
		session("SearchReportCreditHours") = oAssociate.SearchCourseCreditHours
	end if

	oAssociate.SearchLicenseStatusId = ScrubForSql(request("LicenseStatus"))
	if (oAssociate.SearchLicenseStatusId = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseStatusId")) <> "") then
			oAssociate.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
		end if
	else
		session("SearchReportLicenseStatusId") = oAssociate.SearchLicenseStatusId
	end if
	
	oAssociate.SearchLicenseNumber = ScrubForSql(request("LicenseNum"))
	if (oAssociate.SearchLicenseNumber = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseNumber")) <> "") then
			oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
		end if
	else
		session("SearchReportLicenseNumber") = oAssociate.SearchLicenseNumber
	end if
	
	oAssociate.SearchLicenseExpDateFrom = ScrubForSql(request("ExpDateFrom"))
	if (oAssociate.SearchLicenseExpDateFrom = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
			oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
		end if
	else
		session("SearchReportLicenseExpDateFrom") = oAssociate.SearchLicenseExpDateFrom
	end if
	
	oAssociate.SearchLicenseExpDateTo = ScrubForSql(request("ExpDateTo"))
	if (oAssociate.SearchLicenseExpDateTo = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
			oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
		end if
	else
		session("SearchReportLicenseExpDateTo") = oAssociate.SearchLicenseExpDateTo
	end if	
	
	
	dim bLicenseSearch
	dim bCourseSearch
	
	bLicenseSearch = request("ShowLicenses")
	if session("SearchReportShowLicenses") <> "" and ucase(sAction) <> "SEARCH" then
		bLicenseSearch = session("SearchReportShowLicenses")
	else
		session("SearchReportShowLicenses") = bLicenseSearch
	end if
	
	
	bCourseSearch = request("ShowCourses")
	if session("SearchReportShowCourses") <> "" and ucase(sAction) <> "SEARCH" then
		bCourseSearch = session("SearchReportShowCourses")
	else
		session("SearchReportShowCourses") = bCourseSearch
	end if		
	


	'Update the license object properties based on the search we just did.
	'We'll use these properties later to search for the specific license records.
	oLicense.LicenseStatusId = oAssociate.SearchLicenseStatusId
	oLicense.LicenseNum = oAssociate.SearchLicenseNumber
	oLicense.SearchLicenseExpDateFrom = oAssociate.SearchLicenseExpDateTo
	oLicense.SearchLicenseExpDateTo = oAssociate.SearchLicenseExpDateTo
	


	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if
	
	
	dim aiBranchIdList 'List of branch IDs we got from the search
	dim iBranchId 'Individual branch ID
	dim oBranch 'Branch object
	dim sBranchAdminList 'String to report on the gathered IDs

	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	oBranch.TpConnectionString = application("sTpDataSourceName")
	oBranch.VocalErrors = application("bVocalErrors")
	
	
	dim iListType
	iListType = ScrubForSql(request("ListType"))
	if iListType = "" and ucase(sAction) <> "SEARCH" then
		if trim(session("SearchReportListType")) <> "" then
			iListType = session("SearchReportListType")
		else
			iListType = 0
		end if
	else
		if trim(iListType) = "" then
			iListType = 0
		end if
		session("SearchReportListType") = iListType
	end if

	
	'We need to search out the branch admins for any Branches that have been
	'specifically selected, but only if the user has chosen this option.
	if session("SearchReportBranchIdList") <> "" and cint(iListType) >= 1 and CheckIsAdmin() then
	
		dim oBranchRs 'Recordset object to hold the branches for the user
		dim bFirstBranch 'Is this the first branch we're listing?  Used for text formatting.
	
		set oRs = oBranch.GetBranchAdmins(session("SearchReportBranchIdList"), session("UserCompanyId"))
		do while not oRs.EOF
				
			sBranchAdminList = sBranchAdminList & oRs("FirstName") & " "
			if trim(oRs("MidInitial")) <> "" then
				sBranchAdminList = sBranchAdminList & oRs("MidInitial") & ". "
			end if
			sBranchAdminList = sBranchAdminList & oRs("LastName")
				
			'Get the list of branches for this admin.
			bFirstBranch = true
			set oBranchRs = oBranch.GetAdminBranches(oRs("UserId"))
			if oBranchRs.State <> 0 then
				sBranchAdminList = sBranchAdminList & " ("					
				do while not oBranchRs.EOF 
					if not bFirstBranch then
						sBranchAdminList = sBranchAdminList & ", "
					else
						bFirstBranch = false
					end if
					sBranchAdminList = sBranchAdminList & oBranchRs("Name")
					oBranchRs.MoveNext
				loop				
				sBranchAdminList = sBranchAdminList & ")"
			end if 
				
			sBranchAdminList = sBranchAdminList & ", "
							
			oRs.MoveNext
						
		loop
	
		'Clear out the trailing comma
		if sBranchAdminList <> "" then
			sBranchAdminList = left(sBranchAdminList, len(sBranchAdminList) - 2)
		end if
		
	end if

end if

'Restrict the list if the user does not have company-wide access.
if CheckIsBranchAdmin() or CheckIsBranchViewer() then
	oAssociate.SearchBranchIdList = session("UserBranchIdList")
end if

	
	set oRs = oAssociate.SearchAssociatesCoursesLicenses()
	
	if iCurPage = "" then iCurPage = 1
	if iMaxRecs = "" then iMaxRecs = 20
	
%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Reporting</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top">
								&nbsp;
								</td>
							</tr>
							</table>
						</td>
				</tr>
				
				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim sPrevUserId

dim oCurrentLicense
set oCurrentLicense = new License
oCurrentLicense.ConnectionString = application("sDataSourceName")
oCurrentLicense.VocalErrors = application("bVocalErrors")

dim oFs 'Filesystem object
dim oFile 'File object
dim sReportPath 'Directory where reports are stored
dim sDynReportPath 'Dynamic directory link
dim sDynFullFilePath 'Dynamic full file path
dim sFullFilePath 'File path 
dim sFileName 'Report CSV file name only
dim iReportNumber 'Identifier of the report
dim sLine 'Single line to be inserted in the CSV

dim oAssocRs 'Recordset to use within the associate listing

	if iListType <> 2 then

	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
	
		iReportNumber = Year(Now) & Month(Now) & Day(Now) & Hour(Now) & _
			Minute(Now) & Second(Now) & session("User_Id") & session("UserCompanyId")
	
		sReportPath = application("sAbsReportsDirectory")
		sDynReportPath = application("sDynReportsDirectory")
		sFullFilePath = sReportPath & "report_" & iReportNumber & ".csv"
		sDynFullFilePath = sDynReportPath & "report_" & iReportNumber & ".csv"
		sFileName = "report_" & iReportNumber & ".csv"
		
		'Initialize the file system objects
		set oFs = CreateObject("Scripting.FileSystemObject")
		set oFile = oFs.OpenTextFile(sFullFilePath, 2, true)
		
		'Write the file header
		oFile.WriteLine("ComplianceKeeper.com Report Data Export")
		oFile.WriteLine("Report Generated: " & now())
		oFile.WriteLine("")

		do while (not oRs.EOF)
		
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
					
					'Write the CSV-formatted line for this officer record
					'First Name, Middle Initial, Last Name, SSN, Date of Birth, Drivers License State ID, Drivers License Number,
					'Branch Number, Hire Date, Termination Date, Employee ID, Title, Department, Email, 
					'Phone, Phone Extension, Phone #2, Phone #2 Extension, Phone #3, Phone #3 Extension, Fax, 
					'Address, Address Line 2, City, State, Zipcode, Zipcode Extension 

					'Response.Write("<p>")

					sLine = oAssociate.FirstName & "," & oAssociate.MidInitial & "," & oAssociate.LastName & "," & _
						enDeCrypt(oAssociate.SSN,application("RC4Pass")) & "," & oAssociate.DateOfBirth & "," & GetStateName(oAssociate.DriversLicenseStateId, true) & "," & _
						oAssociate.DriversLicenseNo & ","

					if oAssociate.BranchId <> "" then												
						sLine = sLine & oAssociate.LookupBranchNumber(oAssociate.BranchId)
					elseif oAssociate.CompanyL3Id <> "" then
						sLine = sLine & oAssociate.LookupCompanyL3Name(oAssociate.CompanyL3Id)
					elseif oAssociate.CompanyL2Id <> "" then
						sLine = sLine & oAssociate.LookupCompanyL2Name(oAssociate.CompanyL2Id)
					end if
										
					sLine = sLine & "," & _
						oAssociate.HireDate & "," & oAssociate.TerminationDate & ",""" & oAssociate.EmployeeId & """,""" & _
						oAssociate.Title & """,""" & oAssociate.Department & """,""" & oAssociate.Email & """," & _
						oAssociate.Phone & "," & oAssociate.PhoneExt & "," & _
						oAssociate.Phone2 & "," & oAssociate.Phone2Ext & "," & _
						oAssociate.Phone3 & "," & oAssociate.Phone3Ext & "," & _
						oAssociate.Fax & ",""" & oAssociate.Address & """,""" & oAssociate.Address2 & """,""" & oAssociate.City & """," & _
						GetStateName(oAssociate.StateId, true) & "," & oAssociate.Zipcode & "," & oAssociate.ZipcodeExt
					oFile.WriteLine(sLine)						
		
										
					if bLicenseSearch then 
					
						oLicense.OwnerId = oAssociate.UserId
						set oAssocRs = oLicense.SearchLicenses					
					
						do while not oAssocRs.EOF 
						
							if oCurrentLicense.LoadLicenseById(oAssocRs("LicenseId")) <> 0 then
						
								'Write the CSV-formatted line for this license record
								'License Identifier Field, Last Name, SSN, 
								'License Number, License State, License Type, License Status, License Expiration Date
							
								'Response.Write("<br>")
							
								sLine = "*LICENSE," & oAssociate.LastName & "," & enDeCrypt(oAssociate.SSN,application("RC4Pass")) & "," & _
									oCurrentLicense.LicenseNum & "," & GetStateName(oCurrentLicense.LicenseStateId, true) & ",""" & _
									oCurrentLicense.LicenseType & """,""" & oCurrentLicense.LookupLicenseStatus(oCurrentLicense.LicenseStatusId) & """," & _
									oCurrentLicense.LicenseExpDate
								oFile.WriteLine(sLine)
											
							end if
								
							oAssocRs.MoveNext
							
						loop
					
					end if
					
															
					if bCourseSearch then
					
						set oAssocRs = oAssociate.SearchCourses(oAssociate.SearchCourseName, oAssociate.SearchCourseProviderId, "", "")
						do while not oAssocRs.EOF
					
							if oCourse.LoadCourseById(oAssocRs("CourseId")) <> 0 then
							
								'Write the CSV-formatted line for this license record
								'Course Identifier Field, Last Name, SSN, Provider Name, Course Name, 
								'Credit Hours, State, Completion Date, Expiration Date
								
								'Response.Write("<br>")
								
								sLine = "*COURSE," & oAssociate.LastName & "," & enDeCrypt(oAssociate.SSN,application("RC4Pass")) & ",""" & _
									oCourse.LookupProvider(oCourse.ProviderId) & """,""" & oCourse.Name & """," & _
									oCourse.ContEdHours & "," & GetStateName(oCourse.StateId, true) & "," & _
									oCourse.CompletionDate & "," & oCourse.ExpDate
								oFile.WriteLine(sLine)
							
							end if 
							oAssocRs.MoveNext
					
						loop

					end if
					
			end if 
		
			oRs.MoveNext	
	
		loop	
		
		oFile.WriteLine(sCsvText)
		oFile.Close
		set oFile = nothing
		set oFs = nothing
		
		Response.Write("<b>The Report Export has completed successfully.</b><p>")
		Response.Write("<a href=""" & sDynFullFilePath & """>Download Now</a>")

	else
	
		Response.Write("<b>Failed to complete the Report Export.  Please try again.</b>")

	end if
	
	end if

%>
									</table>
								</td>
							</tr>
						</table>


								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%

set oAssociate = nothing
set oLicense = nothing
set oCourse = nothing
set oAssocRs = nothing
set oRs = nothing

	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

%>
<%
'option explicit
server.ScriptTimeout = 1000
response.buffer = false
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Provider.Class.asp" -->
<!-- #include virtual = "/includes/rc4.asp" --> 

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

dim oRs
dim oAssociateSearch
dim oAssociate
dim oLicense
dim oCurLicense
dim sOffLicText

dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
dim oBranch

dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access

dim bLicenseSearch
dim bOfficerSearch
dim iShowCompleted

dim objConn
dim oBranchesRS
dim oCompany

dim oPdf
dim oDoc
dim oPage
dim oDest
dim oTimesFont
dim oFimesFontBold
dim oCanvas
dim oImage
dim sParams
dim oParam
dim sText

dim aList
dim iListId
dim oListRs
dim iCharsPrinted
dim sBranchName

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCurLicense = new License
oCurLicense.ConnectionString = application("sDataSourceName")
oCurLicense.VocalErrors = application("bVocalErrors")

set oAssociateSearch = new Associate
oAssociateSearch.ConnectionString = application("sDataSourceName")
oAssociateSearch.TpConnectionString = application("sTpDataSourceName")
oAssociateSearch.VocalErrors = application("bVocalErrors")

oAssociateSearch.CompanyId = session("UserCompanyId")
oAssociateSearch.UserStatus = 1

if (trim(session("SearchReportLastName")) <> "") then
	oAssociateSearch.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociateSearch.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportHireDateFrom")) <> "") then
	oAssociateSearch.SearchHireDateFrom = session("SearchReportHireDateFrom")
end if

if (trim(session("SearchReportHireDateTo")) <> "") then
	oAssociateSearch.SearchHireDateTo = session("SearchReportHireDateTo")
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociateSearch.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociateSearch.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociateSearch.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociateSearch.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociateSearch.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociateSearch.Inactive = session("SearchReportInactive")
end if

sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList

sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next

'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

'If Structure List set to All, populate structure lists with default values
if trim(sAssocIdList) = "" then
	sCompanyL2IdList = GetUserCompanyL2IdList()
	sCompanyL3IdList = GetUserCompanyL3IdList()
	sBranchIdList = GetUserBranchIdList()
end if

oAssociateSearch.SearchBranchIdList = sBranchIdList
if (oAssociateSearch.SearchBranchIdList = "") then
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociateSearch.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociateSearch.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociateSearch.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociateSearch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociateSearch.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociateSearch.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociateSearch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociateSearch.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociateSearch.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociateSearch.SearchCourseCreditHours = session("SearchReportCreditHours")
end if

if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	oAssociateSearch.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	oAssociateSearch.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
end if

if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociateSearch.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociateSearch.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociateSearch.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociateSearch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociateSearch.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociateSearch.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociateSearch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociateSearch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociateSearch.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if
	
if session("SearchReportShowOfficers") <> "" then
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if
if iShowCompleted = 1 then
	oAssociateSearch.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociateSearch.SearchCourseCompleted = false
else
	oAssociateSearch.SearchCourseCompleted = ""
end if

'Update the license object properties based on the search we just did.
'We'll use these properties later to search for the specific license records.
oLicense.LicenseStatusId = oAssociateSearch.SearchLicenseStatusId
if oLicense.LicenseStatusId = 0 then
	oLicense.LicenseStatusId = ""
end if
oLicense.SearchStatusIdList = oAssociateSearch.SearchLicenseStatusIdList
oLicense.LicenseNum = oAssociateSearch.SearchLicenseNumber
oLicense.SearchLicenseExpDateFrom = oAssociateSearch.SearchLicenseExpDateFrom
oLicense.SearchLicenseExpDateTo = oAssociateSearch.SearchLicenseExpDateTo
oLicense.SearchAppDeadline = oAssociateSearch.SearchAppDeadline
	
'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociateSearch.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociateSearch.SearchCompanyL2IdList = "" and _
	oAssociateSearch.SearchCompanyL3IdList = "" and _
	oAssociateSearch.SearchBranchIdList = "" then
		oAssociateSearch.SearchBranchIdList = GetUserBranchIdList()
	end if
end if
	
'Create PDF Doc
set oPdf = Server.CreateObject("Persits.PDF")
oPDF.RegKey = "IWez17RlvL6PIQc213KacCXF1eD2xzmp+C8FGLDD04ptU96y5Mi8KtfWC9XKP4Nl6qpjPqlOSwG3"
set oDoc = oPdf.CreateDocument

oDoc.Creator = "ComplianceKeeper.com"

set oPage = oDoc.Pages.Add(612, 792)
set oFont = oDoc.Fonts("Arial")

'Write title
sParams = "x=0; y=630; width=612; alignment=center; size=25"
oPage.Canvas.DrawText "Officer Licenses By Branch", sParams, oFont
sParams = "x=0; y=600; width=612; alignment=center; size=10"
oPage.Canvas.DrawText "Report Generated: " & Now(), sParams, oFont

'Draw line
with oPage.Canvas
	.MoveTo 50, 580
	.LineTo 562, 580
	.ClosePath
	.Stroke
end with

'Print CK Logo
set oImage = oDoc.OpenImage(server.MapPath("/Media/Images/logo.gif"))
set oParam = oPdf.CreateParam
oParam("x") = 30
oParam("y") = 650
oPage.Canvas.DrawImage oImage, oParam

'Set Branch list to loop through branches (Each branch will be on a separate workbook)
if trim(sBranchIdList) = "" then
	sBranchIdList = GetUserBranchIdList
end if

set objConn = New HandyADO

sSQL = "SELECT BranchID FROM CompanyBranches " & _
	   "WHERE CompanyID = '" & session("UserCompanyId") & "' " & _
	   "AND NOT Deleted = '1' " 
bFirstBranch = true
aBranches = split(sBranchIdList, ", ")
for each iBranchId in aBranches
	if bFirstBranch then
		sSql = sSql & "AND ("
		bFirstBranch = false
	else
		sSql = sSql & "OR "
	end if
	sSql = sSql & "BranchID = '" & iBranchId & "' "
next	
if not bFirstBranch then
	sSql = sSql & ") "
end if

sSql = sSql & "ORDER BY Name, BranchNum "
set oBranchesRS = objConn.GetDisconnectedRecordset(application("sDataSourceName"), sSQL, false)

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

if not oBranchesRS.EOF then
	bFirstBranch = true
	do while not oBranchesRS.EOF
		'Load the branch
		if oBranch.LoadBranchByID(oBranchesRS("BranchID")) = 0 then
			'The load was unsuccessful.  End.
			Response.Write("Failed to load the passed Branch ID.")
			Response.End	
		end if
	
		sBranchName = oBranch.Name
		oAssociateSearch.BranchID = oBranch.BranchID
		
		set oRs = oAssociateSearch.SearchAssociatesCoursesLicenses()
		
		sOffLicText = ""
		if bOfficerSearch then 

			if not (oRs.BOF and oRs.EOF) then
				set oAssociate = new Associate
				oAssociate.ConnectionString = application("sDataSourceName")
				oAssociate.TpConnectionString = application("sTpDataSourceName")
				oAssociate.VocalErrors = true
		
				oAssociate.SearchCourseName = session("SearchReportCourseName")
				oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
				oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
				oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
				oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
				oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
				if iShowCompleted = 1 then
					oAssociate.SearchCourseCompleted = true
				elseif iShowCompleted = 2 then
					oAssociate.SearchCourseCompleted = false
				else
					oAssociate.SearchCourseCompleted = ""
				end if
				
				do while not oRs.EOF
					if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
						if bLicenseSearch then 			
							oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
							oLicense.OwnerId = oAssociate.UserId
							set oAssocRs = oLicense.SearchLicenses					
						
							do while not oAssocRs.EOF 
								if oCurLicense.LoadLicenseById(oAssocRs("LicenseId")) <> 0 then
									sOffLicText = sOffLicText & "<p>"								
									sOffLicText = sOffLicText & "<b>" & oCurLicense.LookupAssociateNameById(oCurLicense.OwnerId) & "</b><br>"
									sOffLicText = sOffLicText & "License #" & oCurLicense.LicenseNum
									sOffLicText = sOffLicText & " - " & oCurLicense.LookupState(oCurLicense.LicenseStateId)
									
									if oCurLicense.LicenseExpDate <> "" then 
										sOffLicText = sOffLicText & ", Expires: " & oCurLicense.LicenseExpDate
									end if
									
									if oCurLicense.LicenseExpDate < date() + 90 and oCurLicense.LicenseExpDate > date() then
										sOffLicText = sOffLicText & " <b>(<font color=""#cc0000"">" & round(oCurLicense.LicenseExpDate - date(), 0) & " Days</font>)</b>"
									end if									
									
									if oCurLicense.LicenseAppDeadline <> "" then 
										sOffLicText = sOffLicText & ", Renewal App Deadline: " & oCurLicense.LicenseAppDeadline
									end if
									
									if oCurLicense.LicenseAppDeadline < date() + 90 and oCurLicense.LicenseAppDeadline > date() then
										sOffLicText = sOffLicText & " <b>(<font color=""#cc0000"">" & round(oCurLicense.LicenseAppDeadline - date(), 0) & " Days</font>)</b>"
									end if
									
									sOffLicText = sOffLicText & "</p>"
								end if
								oAssocRs.MoveNext
							loop		
							set oAssocRs = nothing
						end if
					end if 
					oRs.MoveNext	
				loop	
				set oAssociate = nothing			
			end if
		end if 
		
		'Write out the records across multiple pages
		'if this is the 1st branch, start details underneath the report heading (1st page)
		if bFirstBranch then
			sParams = "x=0; y=570; width=612; alignment=center; size=14"
			oPage.Canvas.DrawText sBranchName, sParams, oFont	
			sParams = "x=50; y=520; width=500; height=520; alignment=center; size=10; html=true"
		else
			set oPage = oPage.NextPage
			sParams = "x=0; y=750; width=612; alignment=center; size=14"
			oPage.Canvas.DrawText sBranchName, sParams, oFont	
			sParams = "x=50; y=700; width=500; height=700; alignment=center; size=10; html=true"
		end if
		
		do while len(sOffLicText) > 0
			iCharsPrinted = oPage.Canvas.DrawText(sOffLicText, sParams, oFont)
	
			if iCharsPrinted = len(sOffLicText) then exit do
	
			'Continue on new page
			set oPage = oPage.NextPage
			sParams = "x=0; y=750; width=612; alignment=center; size=14"
			oPage.Canvas.DrawText sBranchName & " (continued)", sParams, oFont		
			
			sOffLicText = right(sOffLicText, len(sOffLicText) - iCharsPrinted)
			sParams = "x=50; y=700; width=500; height=700; alignment=center; size=10; html=true"
		loop
	
		oBranchesRS.MoveNext
		bFirstBranch = false
	loop
end if

oDoc.SaveHttp "attachment;filename=report.pdf"


set objConn = nothing
set oBranchesRS = nothing
set oBranch = nothing
set oCompany = nothing
set oAssociateSearch = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
'	PrintShellFooter SHOW_MENUS										' From shell.asp
'	EndPage														' From htmlElements.asp
%>
<%
'option explicit
'server.scripttimeout = 900
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/SavedReport.Class.asp" -------------------->
<!-- #include virtual = "/includes/Manager.Class.asp" ------------------------>
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------

dim oRs
dim oAssociate
dim oCourse
dim oLicense
dim oCompany
dim oBranch 'Branch object
dim oSavedReport
dim oManager
dim oManagerRs

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate
dim i
dim bSelected
dim oAssociateDoc
dim oDocRs
		
sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1

set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")
oAssociateDoc.CompanyId = session("UserCompanyId")

set oSavedReport = new SavedReport
dim iSavedReportId 'Which report are we loading?
dim sSavedReportName 'What is this report named?
oSavedReport.ConnectionString = application("sDataSourceName")
oSavedReport.VocalErrors = application("bVocalErrors")
oSavedReport.CompanyId = session("UserCompanyId")

set oManager = new Manager
oManager.ConnectionString = application("sDataSourceName")
oManager.VocalErrors = application("bVocalErrors")
oManager.CompanyId = session("UserCompanyId")



dim iShowDeleted
iShowDeleted = ScrubForSql(request("ShowDeleted"))
if (iShowDeleted = "3") then
	iShowDeleted = 3
else
	iShowDeleted = 1
end if 


dim sLicStateIdList
dim sCourseStateIdList

sLicStateIdList = replace(request("LicStateIdList"), ", ", ",")
sCourseStateIdList = replace(request("CourseStateIdList"), ", ", ",")

dim sStateIdList
sStateIdList = replace(request("StateIdList"), ", ", ",")



dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
'sBranchIdList = replace(request("BranchIdList"), ", ", ",")
'sBranchIdList = request("BranchIdList")
'sCompanyL2IdList = request("CompanyL2IdList")
'sCompanyL3IdList = request("CompanyL3IdList")


dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access


dim bLicenseSearch
dim bLicCompanyL2Search
dim bLicCompanyL3Search
dim bLicBranchSearch
dim bLicIssuedCompanyL2Search
dim bLicIssuedCompanyL3Search
dim bLicIssuedBranchSearch
dim bLicIssuedSearch
	
dim iShowInactive
dim iShowCompleted
dim iAppDeadline

dim bAdminSearch	
dim bManagerSearch
dim bCourseSearch
dim bOfficerSearch
dim bCompanySearch
dim bBranchSearch
dim bCompanyL2Search
dim bCompanyL3Search

dim bFirstNameSearch
dim bLastNameSearch
dim bSsnSearch
dim bEmployeeIdSearch
dim bPhoneSearch
dim bEmailSearch
dim bWorkAddressSearch
dim bWorkCitySearch
dim bWorkStateSearch
dim bWorkZipSearch
dim bSpecAddressSearch
dim bSpecCitySearch
dim bSpecStateSearch
dim bSpecZipSearch

dim oAdminRs

if (trim(session("SearchReportLastName")) <> "") then
	oAssociate.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportHireDateFrom")) <> "") then
	oAssociate.SearchHireDateFrom = session("SearchReportHireDateFrom")
end if

if (trim(session("SearchReportHireDateTo")) <> "") then
	oAssociate.SearchHireDateTo = session("SearchReportHireDateTo")
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociate.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociate.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociate.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociate.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociate.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociate.Inactive = session("SearchReportInactive")
end if
	
sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList
	
sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next
'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

oAssociate.SearchBranchIdList = sBranchIdList
if (oAssociate.SearchBranchIdList = "") then	
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociate.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociate.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociate.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociate.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociate.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
end if
	
if (trim(session("SearchReportCourseCompletionDateFrom")) <> "") then
	oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
end if
	
if (trim(session("SearchReportCourseCompletionDateTo")) <> "") then
	oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociate.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociate.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociate.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if
	
if session("SearchReportShowLicIssued") <> "" then
	bLicIssuedSearch = session("SearchReportShowLicIssued")
end if
	
if session("SearchReportShowAdmins") <> "" then
	bAdminSearch = session("SearchReportShowAdmins")
end if		
	
if session("SearchReportShowManagers") <> "" then
	bManagerSearch = session("SearchReportShowManagers")
end if
	
if session("SearchReportShowCourses") <> "" then
	bCourseSearch = session("SearchReportShowCourses")
end if		

if session("SearchReportShowOfficers") <> "" then
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if session("SearchReportShowCompany") <> "" then
	bCompanySearch = session("SearchReportShowCompany")
end if
	
if session("SearchReportShowBranches") <> "" then
	bBranchSearch = session("SearchReportShowBranches")
end if
	
if session("SearchReportShowLicBranches") <> "" then
	bLicBranchSearch = session("SearchReportShowLicBranches")
end if
	
if session("SearchReportShowLicIssuedBranches") <> "" then
	bLicIssuedBranchSearch = session("SearchReportShowLicIssuedBranches")
end if
	
if session("SearchReportShowCompanyL2s") <> "" then
	bCompanyL2Search = session("SearchReportShowCompanyL2s")
end if
	
if session("SearchReportShowLicCompanyL2s") <> "" then
	bLicCompanyL2Search = session("SearchReportShowLicCompanyL2s")
end if
	
if session("SearchReportShowLicIssuedCompanyL2s") <> "" then
	bLicIssuedCompanyL2Search = session("SearchReportShowLicIssuedCompanyL2s")
end if	
	
if session("SearchReportShowCompanyL3s") <> "" then
	bCompanyL3Search = session("SearchReportShowCompanyL3s")
end if
	
if session("SearchReportShowLicCompanyL3s") <> "" then
	bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
end if		

if session("SearchReportShowLicIssuedCompanyL3s") <> "" then
	bLicIssuedCompanyL3Search = session("SearchReportShowLicIssuedCompanyL3s")
end if		
	
if session("SearchReportShowFirstName") <> "" then
	bFirstNameSearch = session("SearchReportShowFirstName")
end if

if session("SearchReportShowLastName") <> "" then
	bLastNameSearch = session("SearchReportShowLastName")
end if

if session("SearchReportShowSsn") <> "" then
	bSsnSearch = session("SearchReportShowSsn")
end if
	
if session("SearchReportShowEmployeeId") <> "" then
	bEmployeeIdSearch = session("SearchReportShowEmployeeId")
end if
	
if session("SearchReportShowPhone") <> "" then
	bPhoneSearch = session("SearchReportShowPhone")
end if

if session("SearchReportShowEmail") <> "" then
	bEmailSearch = session("SearchReportShowEmail")
end if
	
if session("SearchReportShowWorkAddress") <> "" then
	bWorkAddressSearch = session("SearchReportShowWorkAddress")
end if
	
if session("SearchReportShowWorkCity") <> "" then
	bWorkCitySearch = session("SearchReportShowWorkCity")
end if

if session("SearchReportShowWorkState") <> "" then
	bWorkStateSearch = session("SearchReportShowWorkState")
end if

if session("SearchReportShowWorkZip") <> "" then
	bWorkZipSearch = session("SearchReportShowWorkZip")
end if
	
if session("SearchReportShowSpecAddress") <> "" then
	bSpecAddressSearch = session("SearchReportShowSpecAddress")
end if
	
if session("SearchReportShowSpecCity") <> "" then
	bSpecCitySearch = session("SearchReportShowSpecCity")
end if

if session("SearchReportShowSpecState") <> "" then
	bSpecStateSearch = session("SearchReportShowSpecState")
end if

if session("SearchReportShowSpecZip") <> "" then
	bSpecZipSearch = session("SearchReportShowSpecZip")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if
if iShowCompleted = 1 then
	oAssociate.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociate.SearchCourseCompleted = false
else
	oAssociate.SearchCourseCompleted = ""
end if

'Update the license object properties based on the search we just did.
'We'll use these properties later to search for the specific license records.
oLicense.LicenseStatusId = oAssociate.SearchLicenseStatusId
if oLicense.LicenseStatusId = 0 then
	oLicense.LicenseStatusId = ""
end if
oLicense.SearchStatusIdList = oAssociate.SearchLicenseStatusIdList
oLicense.LicenseNum = oAssociate.SearchLicenseNumber
oLicense.SearchLicenseExpDateFrom = oAssociate.SearchLicenseExpDateFrom
oLicense.SearchLicenseExpDateTo = oAssociate.SearchLicenseExpDateTo
oLicense.SearchAppDeadline = oAssociate.SearchAppDeadline


'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
end if
	
set oRs = oAssociate.SearchAssociatesCoursesLicenses()

'Restrict display to only 20 records per page
'Get page number of which records are showing
iCurPage = trim(request("page_number"))
if (iCurPage = "") then
	if (trim(Session("CurPage")) <> "") then
		iCurPage = Session("CurPage")
	end if
else
	Session("CurPage") = iCurPage
end if

'if iCurPage = "" then iCurPage = 1
'if iMaxRecs = "" then iMaxRecs = 20

dim oWorkAddressRs
dim oCompanyLicRs
dim oBranchLicRs
dim oCompanyL2LicRs
dim oCompanyL3LicRs

'If we're searching Company licenses, run the search.
if bCompanySearch and not bCheckIsAdmin then
	bCompanySearch = false
end if
'if bCompanySearch then
'	oLicense.SearchStateIdList = session("SearchReportStateIdList")
'	oLicense.OwnerId = session("UserCompanyId")
'	oLicense.OwnerTypeId = 3
'	set oCompanyLicRs = oLicense.SearchLicenses
'	oLicense.OwnerTypeId = 1
'end if
	
	
'If we're searching CompanyL2 licenses, run the search.
dim oCompanyL2
dim oCompanyL2Rs
if bCompanyL2Search then
	if session("SearchReportCompanyL2IdList") = "" and not bCheckIsAdmin then
		bCompanyL2Search = false
	'elseif session("SearchReportCompanyL2IdList") = "" and (session("SearchReportCompanyL3IdList") <> "" or session("SearchReportBranchIdList") <> "") then
	'	bCompanyL2Search = false
	end if
end if
if bCompanyL2Search then
	set oCompanyL2 = new CompanyL2
	oCompanyL2.ConnectionString = application("sDataSourceName")
	oCompanyL2.VocalErrors = application("bVocalErrors")
	oCompanyL2.CompanyId = session("UserCompanyId")
	
	oCompanyL2.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL2.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL2.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL2.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL2.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL2.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL2.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL2.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL2.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL2.Inactive = session("SearchReportInactive")
	
	set oCompanyL2Rs = oCompanyL2.SearchCompaniesLicenses
end if


'If we're searching CompanyL3 licenses, run the search.
dim oCompanyL3
dim oCompanyL3Rs
if bCompanyL3Search then
	if session("SearchReportCompanyL3IdList") = "" and not bCheckIsAdmin then
		bCompanyL3Search = false
	'elseif session("SearchReportCompanyL3IdList") = "" and (session("SearchReportCompanyL2IdList") <> "" or session("SearchReportBranchIdList") <> "") then
'		bCompanyL3Search = false
	end if
end if
if bCompanyL3Search then
	set oCompanyL3 = new CompanyL3
	oCompanyL3.ConnectionString = application("sDataSourceName")
	oCompanyL3.VocalErrors = application("bVocalErrors")
	oCompanyL3.CompanyId = session("UserCompanyId")
	
	oCompanyL3.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL3.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL3.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL3.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oCompanyL3.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL3.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL3.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL3.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL3.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL3.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL3.Inactive = session("SearchReportInactive")
	
	set oCompanyL3Rs = oCompanyL3.SearchCompaniesLicenses
end if
	
	
'If we're searching Branch licenses, run the search.
dim oBranchRs
if bBranchSearch then
	if session("SearchReportBranchIdList") = "" and not bCheckIsAdmin then
		bBranchSearch = false
	end if
end if
if bBranchSearch then
	set oBranch = nothing
	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	oBranch.TpConnectionString = application("sTpDataSourceName")
	oBranch.VocalErrors = application("bVocalErrors")
	oBranch.CompanyId = session("UserCompanyId")
			
	oBranch.SearchStateIdList = session("SearchReportStateIdList")
	oBranch.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oBranch.SearchBranchIdList = session("SearchReportBranchIdList")
	oBranch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oBranch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oBranch.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oBranch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oBranch.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oBranch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oBranch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oBranch.SearchAppDeadline = session("SearchReportAppDeadline")
	oBranch.Inactive = session("SearchReportInactive")

	set oBranchRs = oBranch.SearchBranchesLicenses
end if

%>



<table width=760 border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
		<td width="100%" valign="top">			
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Compliance Report Attachments</td>
				</tr>	
				
				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value
dim sPrevUserId
dim bFirstPass
dim oAssocRs 'Recordset to use within the associate listing

	if bCompanySearch and iCurPage = 1 then

		oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
		oLicense.OwnerId = session("UserCompanyId")
		oLicense.OwnerTypeId = 3
		set oCompanyLicRs = oLicense.SearchLicenses
		oLicense.OwnerTypeId = 1
		
		bFirstPass = true
		if not (oCompanyLicRs.BOF and oCompanyLicRs.EOF) then			
			do while not oCompanyLicRs.EOF
				if oLicense.HasDocuments(oCompanyLicRs("LicenseId")) then
					oAssociateDoc.OwnerId = oCompanyLicRs("LicenseId")
					oAssociateDoc.OwnerTypeId = 7
					set oDocRs = oAssociateDoc.SearchDocs

					if not oDocRs.EOF then
						'Print the Name of the Company
						if bFirstPass then
%>
						<tr>
							<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/company/modcompany.asp"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Company Information" alt="Edit Company Information"></a>&nbsp;&nbsp;<% end if %><a href="/company/companydetail.asp" title="View Company Information"><% = oCompany.Name %></a></td>
						</tr>
<%					
							bFirstPass = false
							sColor = GetNextColor(sColor)
						end if
						
						do while not oDocRs.EOF
							if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then								
				%>
						<tr>
							<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">License #<% = oCompanyLicRs("LicenseNum") %> - <% = oLicense.LookupState(oCompanyLicRs("LicenseStateId")) %>
								-&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a>
							</td>
						</tr>						
				<%
								sColor = GetNextColor(sColor)
							end if
							'clear the docname 
							oAssociateDoc.DocName = ""
							oDocRs.MoveNext
						loop
					end if
				end if
				oCompanyLicRs.MoveNext
			loop
			
		end if 
		set oCompanyLicRs = nothing
	end if 
	
	
	
	
	if bCompanyL2Search and iCurPage = 1 then		
		do while not oCompanyL2Rs.EOF			
			if bLicCompanyL2Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oLicense.OwnerTypeId = 4
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
				
				bFirstPass = true				
				do while not oBranchLicRs.EOF
					if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then
						oAssociateDoc.OwnerId = oBranchLicRs("LicenseId")
						oAssociateDoc.OwnerTypeId = 7
						set oDocRs = oAssociateDoc.SearchDocs
										
						if not oDocRs.EOF then
							'Print the Name of the CompanyL2
							if bFirstPass then
%>
							<tr>
								<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modcompanyl2.asp?id=<% = oCompanyL2Rs("CompanyL2Id") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyL2Name") %> Information" alt="Edit <% = session("CompanyL2Name") %> Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/companyl2detail.asp?id=<% = oCompanyL2Rs("CompanyL2Id") %>" title="View <% = session("CompanyL2Name") %> Information"><% = oCompanyL2Rs("Name") %></a></td>
							</tr>
<%						
								bFirstPass = false
								sColor = GetNextColor(sColor)
							end if
							
							do while not oDocRs.EOF
								if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then				
					%>
									<tr>
										<td colspan="2"<% = sColor %>>
											<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %>
											-&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a>
										</td>
									</tr>						
					<%
									sColor = GetNextColor(sColor)					
								end if
								'clear the docname 
								oAssociateDoc.DocName = ""
								oDocRs.MoveNext
							loop
						end if
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL2Rs.MoveNext
					
		loop
	end if 
	
	
	
	if bCompanyL3Search and iCurPage = 1 then		
		do while not oCompanyL3Rs.EOF
			if bLicCompanyL3Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oLicense.OwnerTypeId = 5
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				bFirstPass = true
				do while not oBranchLicRs.EOF
					if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then
						oAssociateDoc.OwnerId = oBranchLicRs("LicenseId")
						oAssociateDoc.OwnerTypeId = 7
						set oDocRs = oAssociateDoc.SearchDocs
										
						if not oDocRs.EOF then
							'Print the Name of the CompanyL3
							if bFirstPass then
%>
									<tr>
										<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modcompanyL3.asp?id=<% = oCompanyL3Rs("CompanyL3Id") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit <% = session("CompanyL3Name") %> Information" alt="Edit <% = session("CompanyL3Name") %> Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/companyL3detail.asp?id=<% = oCompanyL3Rs("CompanyL3Id") %>" title="View <% = session("CompanyL3Name") %> Information"><% = oCompanyL3Rs("Name") %></a></td>
									</tr>
<%						
								bFirstPass = false
								sColor = GetNextColor(sColor)
							end if
							
							do while not oDocRs.EOF
								if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
					%>
									<tr>
										<td colspan="2"<% = sColor %>>
											<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %>
											-&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a>
										</td>
									</tr>						
					<%
									sColor = GetNextColor(sColor)					
								end if
								'clear the docname 
								oAssociateDoc.DocName = ""								
								oDocRs.MoveNext
							loop
						end if
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL3Rs.MoveNext
					
		loop
	end if 
	
	
	
	
	if bBranchSearch and iCurPage = 1 then		
		do while not oBranchRs.EOF
			if bLicBranchSearch then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oBranchRs("BranchId")
				oLicense.OwnerTypeId = 2
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
				
				bFirstPass = true				
				do while not oBranchLicRs.EOF
					if oLicense.HasDocuments(oBranchLicRs("LicenseId")) then
						oAssociateDoc.OwnerId = oBranchLicRs("LicenseId")
						oAssociateDoc.OwnerTypeId = 7
						set oDocRs = oAssociateDoc.SearchDocs
										
						if not oDocRs.EOF then
							'Print the Name of the Branch
							if bFirstPass then
%>
								<tr>
									<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/branches/modbranch.asp?id=<% = oBranchRs("BranchId") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Branch Information" alt="Edit Branch Information"></a>&nbsp;&nbsp;<% end if %><a href="/branches/branchdetail.asp?branchid=<% = oBranchRs("BranchId") %>" title="View Branch Information"><% = oBranchRs("Name") %><% if oBranchRs("BranchNum") <> "" then %> (#<% = oBranchRs("BranchNum") %>)<% end if %></a></td>
								</tr>							
<%						
								bFirstPass = false
								sColor = GetNextColor(sColor)								
							end if
							
							do while not oDocRs.EOF
								if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
					%>
									<tr>
										<td colspan="2"<% = sColor %>>
											<img src="/media/images/clear.gif" width="30" height="1">License #<% = oBranchLicRs("LicenseNum") %> - <% = oLicense.LookupState(oBranchLicRs("LicenseStateId")) %>
											-&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a>
										</td>
									</tr>						
					<%
									sColor = GetNextColor(sColor)
								end if
								'clear the docname 
								oAssociateDoc.DocName = ""								
								oDocRs.MoveNext
							loop
						end if
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oBranchRs.MoveNext
					
		loop	
	end if 


	if bOfficerSearch then 
	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
		
		oAssociate.SearchCourseName = session("SearchReportCourseName")
		oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		oAssociate.SearchCourseCompletionDateFrom = session("SearchReportCourseCompletionDateFrom")
		oAssociate.SearchCourseCompletionDateTo = session("SearchReportCourseCompletionDateTo")		
		oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
		oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
		if iShowCompleted = 1 then
			oAssociate.SearchCourseCompleted = true
		elseif iShowCompleted = 2 then
			oAssociate.SearchCourseCompleted = false
		else
			oAssociate.SearchCourseCompleted = ""
		end if
	
		'Set the number of records displayed on a page
'		oRs.PageSize = iMaxRecs
'		oRs.CacheSize = iMaxRecs
'		iPageCount = oRs.PageCount
		
		'Determine which search page the user has requested
'		if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
'		if clng(iCurPage) <= 0 then iCurPage = 1
		
		'Set the beginning record to be displayed on the page
'		oRs.AbsolutePage = iCurPage
		
		'Initialize the row/col style values
		bCol = false
		'sColor = ""
		
	
'		iCount = 0
'		do while (iCount < oRs.PageSize) and (not oRs.EOF)
		do while not oRs.EOF	
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
				'if oAssociate.UserId <> sPrevUserId then 
										
					if bLicenseSearch then 
					
						oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
						oLicense.OwnerId = oAssociate.UserId
						set oAssocRs = oLicense.SearchLicenses					
					
						bFirstPass = true
						do while not oAssocRs.EOF 
							if oLicense.HasDocuments(oAssocRs("LicenseId")) then
								oAssociateDoc.OwnerId = oAssocRs("LicenseId")
								oAssociateDoc.OwnerTypeId = 7
								set oDocRs = oAssociateDoc.SearchDocs
											
								if not oDocRs.EOF then
									if bFirstPass then
%>
										<tr>
											<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<% if CheckIsAdmin() then %><a href="/officers/modofficer.asp?id=<% = oAssociate.UserId %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Officer Information" alt="Edit Officer Information"></a>&nbsp;&nbsp;<% end if %><a href="/officers/officerdetail.asp?id=<% = oRs("UserId") %>" title="View Officer Information"><% = oAssociate.LastName %>, <% = oAssociate.FirstName %></a></td>
										</tr>
<%								
										bFirstPass = false
										sColor = GetNextColor(sColor)
									end if
									
									do while not oDocRs.EOF
										if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
							%>
										<tr>
											<td colspan="2"<% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">License #<% = oAssocRs("LicenseNum") %> - <% = oLicense.LookupState(oAssocRs("LicenseStateId")) %>
												-&nbsp;<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a>
											</td>
										</tr>						
							<%
											sColor = GetNextColor(sColor)
										end if
										'clear the docname 
										oAssociateDoc.DocName = ""
										oDocRs.MoveNext
									loop
								end if
							end if
							
							oAssocRs.MoveNext
							
						loop
					
					end if
					
										
			else
			
				Response.Write("NOPE")
				
			end if 
		
			oRs.MoveNext	
'			iCount = iCount + 1
	
		loop	

				

		
		%>
										<tr>
											<td colspan="3" align="center"><p><br>
		<%
		
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
'		if iCurPage > 1 then
'			response.write("<a href=""OfficerReportAttachments.asp?page_number=" & iCurPage-1 & """>Previous</a>" & vbcrlf)
'		end if
'		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
'			response.write("&nbsp;|&nbsp;")
'		end if 
'		if trim(iCurPage) <> trim(iPageCount) then
'			response.write("<a href=""OfficerReportAttachments.asp?page_number=" & iCurPage+1 & """>Next</a>" & vbcrlf)
'		end if
'		Response.Write("<p>")

		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)	
		'response.write("</table>" & vbcrlf)
	
		'display Page number
		'response.write("<table width=""100%"">" & vbcrlf)	
		'response.write("<tr bgcolor=""#FFFFFF"">" & vbcrlf)
		'response.write("<td align=""center"" colspan=""5""><br>" & vbcrlf)		
'		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		'response.write("</td>" & vbcrlf)
		'response.write("</tr>" & vbcrlf)		
		%>
											</td>
										</tr>
		<%
	else
		%>
										<tr>
											<td colspan="2" width="100%" align="left">&nbsp;&nbsp;</a></td>
										</tr>
		<%
		'response.write("<tr><td colspan=""4"">There are currently no Loan Officers that matched your search criteria.</td></tr>" & vbcrlf)
	end if
	end if 
									
%>
									</table>
								</td>
							</tr>
						</table>										
					</td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oLicense = nothing
set oAssociate = nothing
set oAssociateDoc = nothing
set oDocRs = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/AssociateDoc.Class.asp" ------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Settings.Class.asp" ----------------------->
<!-- #include virtual = "/includes/EmailFunctions.asp" ----------------------->
<!-- #include virtual = "/includes/EmailLog.Class.asp" ----------------------->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim sUserEmail
dim sUserFullName
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")

dim iOfficerId
'iOfficerId = ScrubForSql(request("OfficerId"))

'Get User email address
sUserEmail = session("UserEmail")
sUserFullName = session("User_FullName")
if sUserEmail = "" then
	AlertError("This account requires a valid email address to send emails.")
	Response.End
end if


dim oAssociateDoc
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")

	
dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	AlertError("Failed to load the passed Company ID.")
	Response.end
		
end if 


oAssociate.CompanyId = session("UserCompanyId")

if trim(session("SearchReportLastName")) <> "" then
	oAssociate.LastName = session("SearchReportLastName")
end if
if trim(session("SearchReportSsn")) <> "" then 
	oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if
if trim(session("SearchReportDeletedOfficers")) <> "" then
	oAssociate.UserStatus = session("SearchReportDeletedOfficers")
end if
if trim(session("SearchReportStateIdList")) <> "" then
	oAssociate.SearchStateIdList = session("SearchReportStateIdList")
end if
if trim(session("SearchReportLicStateIdList")) <> "" then
	oAssociate.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if
if trim(session("SearchReportCourseStateIdList")) <> "" then
	oAssociate.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if
if trim(session("SearchReportBranchIdList")) <> "" then
	oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
end if 
if trim(session("SearchReportInactive")) <> "" then
	oAssociate.Inactive = session("SearchReportInactive")
end if
if trim(session("SearchReportCourseName")) <> "" then
	oAssociate.SearchCourseName = session("SearchReportCourseName")
end if 
if trim(session("SearchReportProviderId")) <> "" then
	oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
end if
if trim(session("SearchReportCreditHours")) <> "" then
	oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
end if 
if trim(session("SearchReportLicenseStatusId")) <> "" then
	oAssociate.SearchLicenseStatusid = session("SearchReportLicenseStatusId")
end if
if trim(session("SearchReportLicenseStatusIdList")) <> "" then
	oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
if trim(session("SearchReportLicenseNumber")) <> "" then
	oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
if trim(session("SearchReportLicenseExpDateFrom")) <> "" then
	oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
if trim(session("SearchReportLicenseExpDateTo")) <> "" then
	oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

'Restrict the list if the user does not have company-wide access.
if CheckIsBranchAdmin() or CheckIsBranchViewer() then
	oAssociate.SearchBranchIdList = session("UserBranchIdList")
	'oAssociate.BranchId = session("UserBranchId")
end if

dim oRs 'Recordset

set oRs = oAssociate.SearchAssociatesCoursesLicenses()

dim sOfficerList 'List of officer names to whom we'll send the email.
dim sTextOfficerList 'Same as sOfficerList without HTML code.
dim sExcludeList 'List of officer names with invalid emails.



'Create the Fileup object that will allow us to upload files
dim oFileUp
set oFileUp = server.CreateObject("softartisans.fileup")



	dim oJmail
	dim oSettings
	dim sSuccessURL
	dim sBody
	dim sAttachment
	dim oLocalFileRs
	
	'Create an instance of the jmail object and set its initial values
	set oJmail = server.CreateObject("Jmail.Message")
	'oJmail.ContentType = "text/html"
	'oJmail.Silent = false 'true
	oJmail.Logging = true
	
	'Set the Jmail values with the information collected
	oJmail.Body = oFileUp.Form("EmailText")
	'oJmail.AddRecipient oAssociate.Email
	oJmail.From = sUserEmail
	oJmail.FromName = sUserFullName
	oJmail.Subject = "ComplianceKeeper.com: " & oFileUp.Form("Subject")
	
	'Process the attachment
	if oFileUp.Form("EmailAttachment") <> "" then 
	'	oJmail.AddAttachment oFileUp.Form("EmailAttachment")
	'end if
		'oFileUp.Form("EmailAttachment").SaveAs application("sAbsAttachmentsFolder") & oFileUp.Form("EmailAttachment").UserFileName
		oJmail.AddAttachment application("sAbsAttachmentsFolder") & session("User_ID") & oFileUp.Form("EmailAttachment")
	end if 
	
	
	'Process the local file attachment
	if oFileUp.Form("LocalFileAttachmentId") <> "" then
		if oAssociateDoc.LoadDocById(oFileUp.Form("LocalFileAttachmentId")) then
			set oLocalFileRs = oAssociateDoc.LoadDocFile
			
			dim oStream
			set oStream = server.CreateObject("ADODB.Stream")
			oStream.Type = 1 'adTypeBinary
			oStream.Open
			'Response.Write("<br>x" & oStream.Size & "x")
			oStream.Write oLocalFileRs("FileContents")
			'Response.Write("<br>x" & oStream.Size & "x")
			oStream.SaveToFile(application("sAbsTempFilesFolder") & oAssociateDoc.FileName)
			
			'oJmail.AddCustomAttachment oAssociateDoc.FileName, oLocalFileRs("FileContents")
			oJmail.AddAttachment application("sAbsTempFilesFolder") & oAssociateDoc.FileName
			
			'Delete the file.
			dim oFs
			dim oFile
			set oFs = server.CreateObject("Scripting.FileSystemObject")
			set oFile = oFs.GetFile(application("sAbsTempFilesFolder") & oAssociateDoc.FileName)
			oFile.Delete(true)
			set oFs = nothing
			set oFile = nothing
		end if 
	end if




if oRs.EOF and oRs.BOF then

	AlertError("There is no report list for this email.  You must first generate an officer report before emailing the officers.")
	Response.End

else

	do while not oRs.EOF
	
		if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
			if oAssociate.Email = "" then 
			
				sExcludeList = sExcludeList & "<a href=""/officers/officerdetail.asp?id=" & oAssociate.UserId & """>" & oAssociate.FullName & "</a>, "
				
			else
		
				sOfficerList = sOfficerList & "<a href=""/officers/officerdetail.asp?id=" & oAssociate.UserId & """>" & oAssociate.FullName & "</a> (" & oAssociate.Email & "), "
				sTextOfficerList = sTextOfficerList & oAssociate.FullName & " (" & oAssociate.Email & "), " 
				oJmail.AddRecipientBCC oAssociate.Email
				
			end if
			
		end if			
			
		oRs.MoveNext
		
	loop
	
	if sOfficerList <> "" then
		sOfficerList = left(sOfficerList, len(sOfficerList) - 2)
		sTextOfficerList = left(sTextOfficerList, len(sTextOfficerList) - 2)
	else
		AlertError("There were no officers with valid email addresses in the report results.  Please update the officer email address records and try again.")
	end if
	if sExcludeList <> "" then
		sExcludeList = left(sExcludeList, len(sExcludeList) - 2)
	end if 
	
end if


'Copy the relevant branch administrators if necessary
dim oBranch
dim sBranchAdminList 'String to report on the gathered IDs

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

'Search out the branch admins for any branches that have been specifically
'selected in the report.
if session("SearchReportBranchIdList") <> "" then

	set oRs = oBranch.GetBranchAdmins(session("SearchReportBranchIdList"), session("UserCompanyId"))
	do while not oRs.EOF
	
		if oRs("Email") <> "" then

			sBranchAdminList = sBranchAdminList & oRs("FirstName") & " "
			if trim(oRs("MiddleName")) <> "" then
				sBranchAdminList = sBranchAdminList & oRs("MiddleName") & ". "
			end if
			sBranchAdminList = sBranchAdminList & oRs("LastName") & " (" & oRs("Email") & "), "
			oJmail.AddRecipientBCC oRs("Email")
		
		end if
		
		oRs.MoveNext
		
	loop
	
	'Clear out the trailing comma
	if sBranchAdminList <> "" then
		sBranchAdminList = left(sBranchAdminList, len(sBranchAdminList) - 2)
	end if
	
end if



	'Mail it
	oJmail.Send(application("sMailServerAddress"))
	
	
	'Initialize the EmailLog object and save a log record of this sent email.
	dim oLog
	set oLog = new EmailLog
	oLog.ConnectionString = application("sDataSourceName")
	oLog.VocalErrors = application("bVocalErrors")
	oLog.SenderId = session("User_ID")
	oLog.CompanyId = session("UserCompanyId")
	oLog.Subject = ScrubForSql(oFileUp.Form("Subject"))
	oLog.BodyText = ScrubForSql(oFileUp.Form("EmailText"))
	oLog.RecipientList = sTextOfficerList
	if sBranchAdminList <> "" then
		oLog.RecipientList = oLog.RecipientList & ", " & sBranchAdminList
	end if 
	if oFileUp.Form("EmailAttachment") <> "" then
		oLog.Attachment = session("User_ID") & ScrubForSql(oFileUp.Form("EmailAttachment"))
	end if 
	oLog.SendDate = now()
	oLog.SendLog = oJmail.Log
	oLog.SaveEmailLog

	
	oJmail.Close
	
	'If true, no errors occurred while sending
	if oJMail.ErrorCode <> 0 then
		AlertError("The Alert Email was not sent successfully.  " & oJMail.ErrorCode & ": " & oJMail.ErrorMessage)
	end if


%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Email -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Send Email</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Email Officers</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<%

'call SendCustomEmail(iOfficerId, request("EmailText"), request("Subject"))

%>

Your email has been sent successfully.

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCompany = nothing
set oAssociate = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
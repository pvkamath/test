<%
option explicit
server.scripttimeout = 900
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Group.Class.asp" -------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Note.Class.asp" --------------------------->
<!-- #include virtual = "/includes/SavedReport.Class.asp" -------------------->
<!-- #include virtual = "/includes/Manager.Class.asp" ------------------------>
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oAssociate
dim oCourse
dim oLicense
dim oCompany
dim oGroup 'Branch object
dim oSavedReport
dim oNote

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate
dim dCourseCompletionDate
dim i
dim bSelected
dim bFirstField 

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oGroup = new Group
oGroup.ConnectionString = application("sDataSourceName")
oGroup.TpConnectionString = application("sTpDataSourceName")
oGroup.VocalErrors = application("bVocalErrors")
oGroup.CompanyID = oCompany.CompanyID

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1

dim iShowDeleted
iShowDeleted = ScrubForSql(request("ShowDeleted"))
if (iShowDeleted = "3") then
	iShowDeleted = 3
else
	iShowDeleted = 1
end if 


dim sLicStateIdList
dim sCourseStateIdList

sLicStateIdList = replace(request("LicStateIdList"), ", ", ",")
sCourseStateIdList = replace(request("CourseStateIdList"), ", ", ",")

dim sStateIdList
sStateIdList = replace(request("StateIdList"), ", ", ",")



dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList
'sBranchIdList = replace(request("BranchIdList"), ", ", ",")
'sBranchIdList = request("BranchIdList")
'sCompanyL2IdList = request("CompanyL2IdList")
'sCompanyL3IdList = request("CompanyL3IdList")


dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access


dim bGroupSearch
dim bEmployeeSearch
dim bLicenseSearch
dim bGroupLeaderSearch
dim bEmailSearch
dim bAssociateTypeSearch
dim bLicenseStatusSearch
dim bLicenseStatusDateSearch
dim bExpirationDateSearch
	
if ucase(sAction) = "NEW" then

	ClearReportSessionVars()

else

    oGroup.GroupName = ScrubForSql(request("GroupName"))
    if (oGroup.GroupName = "") and (ucase(sAction) <> "SEARCH") then
        if (trim(session("SearchReportGroupName")) <> "") then
            oGroup.GroupName = session("SearchReportGroupName")
        end if
    else
        session("SearchReportGroupName") = oGroup.GroupName
    end if

	oGroup.EmployeeLastNameSearch = ScrubForSql(request("LastName"))
	if (oGroup.EmployeeLastNameSearch = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLastName")) <> "") then
			oGroup.EmployeeLastNameSearch = session("SearchReportLastName")
		end if
	else
		session("SearchReportLastName") = oGroup.EmployeeLastNameSearch
	end if

	oGroup.AssociateTypeIDSearch = ScrubForSql(request("AssociateTypeID"))
    if (oGroup.AssociateTypeIDSearch = "") and (ucase(sAction) <> "SEARCH") then
        if (trim(session("SearchReportAssociateTypeID")) <> "") then
            oGroup.AssociateTypeIDSearch = session("SearchReportAssociateTypeID")
        end if
    else
        session("SearchReportAssociateTypeID") = oGroup.AssociateTypeIDSearch
    end if

	oGroup.LicenseNumSearch = ScrubForSql(request("LicenseNumber"))
	if (oGroup.LicenseNumSearch = "") and (ucase(sAction) <> "SEARCH") then
		if (trim(session("SearchReportLicenseNumber")) <> "") then
			oGroup.LicenseNumSearch = session("SearchReportLicenseNumber")
		end if
	else
		session("SearchReportLicenseNumber") = oGroup.LicenseNumSearch
	end if

    bGroupSearch = request("ShowGroups")
	if session("SearchReportShowGroups") <> "" and ucase(sAction) <> "SEARCH" then
		bGroupSearch = session("SearchReportShowGroups")
	else
		session("SearchReportShowGroups") = bGroupSearch
	end if

    bEmployeeSearch = request("ShowEmployees")
	if session("SearchReportShowEmployees") <> "" and ucase(sAction) <> "SEARCH" then
		bEmployeeSearch = session("SearchReportShowEmployees")
	else
		session("SearchReportShowEmployees") = bEmployeeSearch
	end if

	bLicenseSearch = request("ShowLicenses")
	if session("SearchReportShowLicenses") <> "" and ucase(sAction) <> "SEARCH" then
		bLicenseSearch = session("SearchReportShowLicenses")
	else
		session("SearchReportShowLicenses") = bLicenseSearch
	end if
	
	bGroupLeaderSearch = request("ShowGroupLeader")
	if session("SearchReportShowGroupLeader") <> "" and ucase(sAction) <> "SEARCH" then
		bGroupLeaderSearch = session("SearchReportShowGroupLeader")
	else
		session("SearchReportShowGroupLeader") = bGroupLeaderSearch
	end if
	
	bEmailSearch = request("ShowEmail")
	if session("SearchReportShowEmail") <> "" and ucase(sAction) <> "SEARCH" then
		bEmailSearch = session("SearchReportShowEmail")
	else
		session("SearchReportShowEmail") = bEmailSearch
	end if

	bAssociateTypeSearch = request("ShowAssociateType")
	if session("SearchReportShowAssociateType") <> "" and ucase(sAction) <> "SEARCH" then
		bAssociateTypeSearch = session("SearchReportShowAssociateType")
	else
		session("SearchReportShowAssociateType") = bAssociateTypeSearch
	end if

	bExpirationDateSearch = request("ShowExpirationDate")
	if session("SearchReportShowExpirationDate") <> "" and ucase(sAction) <> "SEARCH" then
		bExpirationDateSearch = session("SearchReportShowExpirationDate")
	else
		session("SearchReportShowExpirationDate") = bExpirationDateSearch
	end if

	'Restrict display to only 20 records per page
	'Get page number of which records are showing
	iCurPage = trim(request("page_number"))
	if (iCurPage = "") then
		if (trim(Session("CurPage")) <> "") then
			iCurPage = Session("CurPage")
		end if
	else
		Session("CurPage") = iCurPage
	end if
	

'	iListType = ScrubForSql(request("ListType"))
'	if iListType = "" and ucase(sAction) <> "SEARCH" then
'		if trim(session("SearchReportListType")) <> "" then
'			iListType = session("SearchReportListType")
'		else
'			iListType = 0
'		end if
''	else
'		if trim(iListType) = "" then
''			iListType = 0
'		end if
'		session("SearchReportListType") = iListType
'	end if

end if	


	'Update the license object properties based on the search we just did.
	'We'll use these properties later to search for the specific license records.
	oLicense.LicenseNum = oAssociate.SearchLicenseNumber

if iCurPage = "" then iCurPage = 1
if iMaxRecs = "" then iMaxRecs = 20

	
	
'If we're searching CompanyL2 licenses, run the search.
dim oGroupRs
if bGroupSearch then
	set oGroupRs = oGroup.SearchGroups
end if

%>

<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Loan Officers -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Compliance Reporting 
					<% if session("SearchReportSavedName") <> "" then %> - <% = session("SearchReportSavedName") %><% end if %>
					</td>
				</tr>		
				<tr>
					<td class="bckWhiteBottomBorder">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="5" cellspacing="0" width="100%">
										<form name="frm1" action="groupreport.asp" method="POST">
										<input type="hidden" name="action" value="SEARCH">
										<input type="hidden" name="page_number" value="1">
										<tr>
											<td class="newstitle" nowrap>Group Name: </td>
											<td><input type="text" name="GroupName" size="20" maxlength="100" value="<% = oGroup.GroupName %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td valign="top" class="newstitle" nowrap>License Number: </td>
											<td><input type="text" name="LicenseNumber" size="20" maxlength="100" value="<% = oGroup.LicenseNumSearch %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Employee Last Name: </td>
											<td><input type="text" name="LastName" size="20" maxlength="100" value="<% = oGroup.EmployeeLastNameSearch %>"></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td class="newstitle" nowrap>Employee Type: </td>
											<td><%call DisplayAssociateTypeDropDown(oGroup.AssociateTypeIDSearch,0,session("UserCompanyId"),"frm1",false,false )%></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
										</tr>
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="6">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="6">
												<span class="newstitle">Show in Results: </span>
											</td>
										</tr>
										<tr>
											<td colspan="3" valign="top">
												&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowGroups" value="1"<% if bGroupSearch then %> CHECKED<% end if %>> Group Matches
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowEmployees" value="1"<% if bEmployeeSearch then %> CHECKED<% end if %>> Employee Matches
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowLicenses" value="1"<% if bLicenseSearch then %> CHECKED<% end if %>> License Matches
											</td>
											<td colspan="3" valign="top">												
												&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowGroupLeader" value="1"<% if bGroupLeaderSearch then %> CHECKED<% end if %>> Group Leader
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowEmail" value="1"<% if bEmailSearch then %> CHECKED<% end if %>> Employee Email
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowAssociateType" value="1"<% if bAssociateTypeSearch then %> CHECKED<% end if %>> Employee Type
												<br>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="ShowExpirationDate" value="1"<% if bExpirationDateSearch then %> CHECKED<% end if %>> Expiration Date
											</td>
										</tr>
										<tr>
											<td colspan="6" align="center"><input type="image" src="/media/images/blue_bttn_search.gif" width="79" height="23" alt="Execute Search" border="0"></td>
										</tr>
									</table>
								</td>
								</form>
							</tr>
							</table>
						</td>
				</tr>
				
				<tr>
					<td class="bckRight"> 
					
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<tr>
								<td>
									<table height="100%" width="100%" border=0 cellpadding=4 cellspacing=0>
<%

dim bCol 'Boolean for which column is being written currently
dim sColor 'Current row color value
dim sPrevUserId

dim oLicRs 'Recordset to use within the associate listing
dim oNotesRs
	if bGroupSearch then
	    do while not oGroupRs.EOF 
			%>
								<tr>
									<td colspan="2" width="100%" align="left"<% = sColor %>>&nbsp;&nbsp;<a href="/officers/modgroup.asp?GroupID=<%=oGroupRs("GroupID") %>"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="top" border="0" title="Edit Group Information" alt="Edit Group Information"></a>&nbsp;&nbsp;<a href="/officers/groupdetail.asp?ID=<%=oGroupRs("GroupID") %>" title="View Group Information"><% = oGroupRs("GroupName") %></a>
                                    <%if bGroupLeaderSearch then%>
                                        - <%=oGroupRs("LastName") %>, <%= oGroupRs("FirstName") %>
                                    <%end if %>
                                    </td>
								</tr>
			<%
						
			sColor = GetNextColor(sColor)

            if bEmployeeSearch then
                'dim oGroupEmployee
                'set oGroupEmployee = new Group
                'oGroupEmployee.ConnectionString = application("sDataSourceName")
                'oGroupEmployee.TpConnectionString = application("sTpDataSourceName")
                'oGroupEmployee.VocalErrors = application("bVocalErrors")
                'oGroupEmployee.CompanyID = oCompany.CompanyID
                'oGroupEmployee.GroupID = oGroupRs("GroupID")

                oGroup.GroupID = oGroupRs("GroupID")

                dim oGroupsEmployeesRs
                set oGroupsEmployeesRs = oGroup.GetEmployees

        	    do while not oGroupsEmployeesRs.EOF 
                    %>
                    <tr>
                        <td <% = sColor %>></td>
                        <td <% = sColor %>>
                            <img src="/media/images/clear.gif" width="30" height="1">
                            <a href="/officers/employeedetail.asp?ID=<%=oGroupsEmployeesRs("UserID") %>"><%=oGroupsEmployeesRs("LastName") %>, <%=oGroupsEmployeesRs("FirstName") %></a>
                            <%
                            if bEmailSearch then 
                                if oGroupsEmployeesRs("Email") <> "" then
                                    Response.Write(" - " & oGroupsEmployeesRs("Email"))
                                end if
                            end if
                            if bAssociateTypeSearch then 
                                if oGroupsEmployeesRs("AssociateTypeName") <> "" then
                                    Response.Write(" - " & oGroupsEmployeesRs("AssociateTypeName"))
                                end if
                            end if
                            %>
                        </td>
                    </tr>
                    <%
                    oGroupsEmployeesRs.MoveNext
        			sColor = GetNextColor(sColor)
                loop

            end if

            if bLicenseSearch then
				set oLicRs = oGroup.GetLicenses					
					
				do while not oLicRs.EOF 
						
					%>
						<tr>
                            <td></td>
							<td <% = sColor %>><img src="/media/images/clear.gif" width="30" height="1">License #<% = oLicRs("LicenseNum") %> - <% = oLicense.LookupState(oLicRs("LicenseStateId")) %>
                            <% if bExpirationDateSearch then %>
                                <% if oLicRs("LicenseExpDate") <> "" then %>, Expires: <% = oLicRs("LicenseExpDate") %><% end if %>
							    <%
							    if oLicRs("LicenseExpDate") < date() + 90 and oLicRs("LicenseExpDate") > date() then
								    Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>")
							    end if
							    %>
							    <% if oLicRs("LicenseAppDeadline") <> "" then %>, Renewal App Deadline: <% = oLicRs("LicenseAppDeadline") %>
							    <%
							    if oLicRs("LicenseAppDeadline") < date() + 90 and oLicRs("LicenseAppDeadline") > date() then
								    Response.Write("   <b>(<font color=""#cc0000"">" & round(oLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>")
							    end if
							    %>
							    <% end if %>
                            <% end if %>
							</td>
						</tr>						
					<%
					oLicRs.MoveNext
							
					sColor = GetNextColor(sColor)
							
				loop

            end if
            oGroupRs.MoveNext
		loop
	end if 
%>	

<% if bGroupSearch or bEmployeeSearch or bLicenseSearch then %>	
					    <tr>
					    <td colspan=2 class="bckWhiteTopBorder" align="center">
						    <table cellpadding="4" cellspacing="10" border="0">
							    <tr>
								    <td align="center" valign="top" width="125">
									    <a href="groupreportpdf.asp"><img src="/media/images/icon_pdf.gif" width="50" height="50" alt="View PDF" border="0"><br><b>View Results<br> PDF</a></b>
								    </td>
							    </tr>
							    <tr>
								    <td colspan="4" align="center">
									    <b><a href="emailpdf.asp">Email Results PDF</a></b><br>
								    </td>
							    </tr>
    						</table>															
					</td>
<%end if %>
							</tr>
						</table>

						
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<!-- include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set oLicense = nothing
set oAssociate = nothing
set oGroup = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

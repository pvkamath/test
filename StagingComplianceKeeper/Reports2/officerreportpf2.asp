<%
'option explicit
server.ScriptTimeout = 1000
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Associate.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/CompanyL2.Class.asp" ---------------------->
<!-- #include virtual = "/includes/CompanyL3.Class.asp" ---------------------->
<!-- #include virtual = "/includes/Branch.Class.asp" ------------------------->
<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/License.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Provider.Class.asp" -->
<!-- #include virtual = "/includes/Manager.Class.asp" -->
<!-- #include virtual = "/includes/rc4.asp" --> 
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Reporting"


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin
bCheckIsAdmin = CheckIsAdmin()

'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "REPORTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------




dim oRs
dim oAssociate
dim oCourse
dim oLicense
dim oCompany
dim oBranch 'Branch object
dim oSavedReport
dim oProvider
dim oManager
dim oManagerRs

dim sAction
dim sDisplay
dim iCurPage
dim iMaxRecs
dim iPageCount
dim iCount
dim sClass
dim dCourseExpDate

sAction = ScrubForSQL(request("Action"))
sDisplay = ScrubForSQL(request("Display"))

set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")
oLicense.OwnerTypeId = 1

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = application("bVocalErrors")
if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then
	AlertError("A valid Company ID is required to load this page.")
	Response.End
end if 

set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")

set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")

oAssociate.CompanyId = session("UserCompanyId")
oAssociate.UserStatus = 1


set oManager = new Manager
oManager.ConnectionString = application("sDataSourceName")
oManager.VocalErrors = application("bVocalErrors")
oManager.CompanyId = session("UserCompanyId")


dim sBranchIdList
dim sCompanyL2IdList
dim sCompanyL3IdList

dim sAssocIdList 'Grouped results set from hierarchy listbox
dim aiAssocIdList 'Results array
dim sAssocId 'Individual ID

dim sUserAllowedCompanyL2IdList 'Comma separated list of the L2s to which the user has access
dim sUserAllowedCompanyL3IdList 'Comma separated list of the l3s to which the user has access
dim sUserAllowedBranchIdList 'Comma separated list of the branches to which the user has access

dim bLicenseSearch
dim bLicCompanyL2Search
dim bLicCompanyL3Search
dim bLicBranchSearch
dim bLicIssuedCompanyL2Search
dim bLicIssuedCompanyL3Search
dim bLicIssuedBranchSearch
dim bLicIssuedSearch
	
dim iShowInactive
	
dim bAdminSearch
dim bManagerSearch	
dim bCourseSearch
dim bOfficerSearch
dim bCompanySearch
dim bBranchSearch
dim bCompanyL2Search
dim bCompanyL3Search

dim bFirstNameSearch
dim bLastNameSearch
dim bSsnSearch
dim bEmployeeIdSearch
dim bPhoneSearch
dim bEmailSearch
dim bWorkAddressSearch
dim bWorkCitySearch
dim bWorkStateSearch
dim bWorkZipSearch
dim bSpecAddressSearch
dim bSpecCitySearch
dim bSpecStateSearch
dim bSpecZipSearch
dim iShowCompleted


dim oAdminRs



if (trim(session("SearchReportLastName")) <> "") then
	oAssociate.LastName = session("SearchReportLastName")
end if

if (trim(session("SearchReportSsn")) <> "") then
	oAssociate.Ssn = enDeCrypt(session("SearchReportSsn"),application("RC4Pass"))
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	oAssociate.SearchStateIdList = session("SearchReportStateIdList")
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	oAssociate.SearchLicStateIdList = session("SearchReportLicStateIdList")
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	oAssociate.SearchCourseStateIdList = session("SearchReportCourseStateIdList")
end if

if (trim(session("SearchReportStateIdType")) <> "") then
	oAssociate.SearchStateIdType = session("SearchReportStateIdType")
end if

if (trim(session("SearchReportBranchId")) <> "") then
	oAssociate.BranchId = session("SearchReportBranchId")
end if
	
if session("SearchReportInactive") <> "" then
	oAssociate.Inactive = session("SearchReportInactive")
end if
	
sUserAllowedCompanyL2IdList = GetUserCompanyL2IdList
sUserAllowedCompanyL3IdList = GetUserCompanyL3IdList
sUserAllowedBranchIdList = GetUserBranchIdList
	
sAssocIdList = session("SearchReportStructureIdList")
aiAssocIdList = split(sAssocIdList, ", ")
for each sAssocId in aiAssocIdList
	if left(sAssocId, 1) = "a" then
		if instr(1,sUserAllowedCompanyL2IdList,mid(sAssocId, 2)) then
			sCompanyL2IdList = sCompanyL2IdList & mid(sAssocId, 2) & ", "
			AddL2ChildrenToList mid(sAssocId, 2), sCompanyL3IdList, sBranchIdList
		end if
	elseif left(sAssocId, 1) = "b" then
		if instr(1, sUserAllowedCompanyL3IdList, mid(sAssocId, 2)) then
			sCompanyL3IdList = sCompanyL3IdList & mid(sAssocId, 2) & ", "
			AddL3ChildrenToList mid(sAssocId, 2), sBranchIdList
		end if 
	elseif left(sAssocId, 1) = "c" then
		if instr(1, sUserAllowedBranchIdList, mid(sAssocId, 2)) then
			sBranchIdList = sBranchIdList & mid(sAssocId, 2) & ", "
		end if
	end if
next
'Remove the trailing commas
if sCompanyL2IdList <> "" then sCompanyL2IdList = mid(sCompanyL2IdList, 1, len(sCompanyL2IdList) - 2)
if sCompanyL3IdList <> "" then sCompanyL3IdList = mid(sCompanyL3IdList, 1, len(sCompanyL3IdList) - 2)
if sBranchIdList <> "" then sBranchIdList = mid(sBranchIdList, 1, len(sBranchIdList) - 2)

oAssociate.SearchBranchIdList = sBranchIdList
if (oAssociate.SearchBranchIdList = "") then	
	if (trim(session("SearchReportBranchIdList")) <> "") then
		oAssociate.SearchBranchIdList = session("SearchReportBranchIdList")
	end if
end if

oAssociate.SearchCompanyL2IdList = sCompanyL2IdList
if (oAssociate.SearchCompanyL2IdList = "") then
	if (trim(session("SearchReportCompanyL2IdList")) <> "") then
		oAssociate.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	end if
end if
	
oAssociate.SearchCompanyL3IdList = sCompanyL3IdList
if (oAssociate.SearchCompanyL3IdList = "") then
	if (trim(session("SearchReportCompanyL3IdList")) <> "") then
		oAssociate.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	end if
end if
		
if (trim(session("SearchReportCourseName")) <> "") then
	oAssociate.SearchCourseName = session("SearchReportCourseName")
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	oAssociate.SearchCourseCreditHours = session("SearchReportCreditHours")
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	oAssociate.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	oAssociate.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	oAssociate.SearchLicenseNumber = session("SearchReportLicenseNumber")
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	oAssociate.SearchLicensePayment = session("SearchReportLicensePayment")
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	oAssociate.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	oAssociate.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	oAssociate.SearchAppDeadline = session("SearchReportAppDeadline")
end if

if session("SearchReportShowLicenses") <> "" then
	bLicenseSearch = session("SearchReportShowLicenses")
end if
	
if session("SearchReportShowLicIssued") <> "" then
	bLicIssuedSearch = session("SearchReportShowLicIssued")
end if
	
if session("SearchReportShowAdmins") <> "" then
	bAdminSearch = session("SearchReportShowAdmins")
end if		

if session("SearchReportShowManagers") <> "" then
	bManagerSearch = session("SearchReportShowManagers")
end if
	
if session("SearchReportShowCourses") <> "" then
	bCourseSearch = session("SearchReportShowCourses")
end if		

if session("SearchReportShowOfficers") <> "" then
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if session("SearchReportShowCompany") <> "" then
	bCompanySearch = session("SearchReportShowCompany")
end if
	
if session("SearchReportShowBranches") <> "" then
	bBranchSearch = session("SearchReportShowBranches")
end if
	
if session("SearchReportShowLicBranches") <> "" then
	bLicBranchSearch = session("SearchReportShowLicBranches")
end if
	
if session("SearchReportShowLicIssuedBranches") <> "" then
	bLicIssuedBranchSearch = session("SearchReportShowLicIssuedBranches")
end if
	
if session("SearchReportShowCompanyL2s") <> "" then
	bCompanyL2Search = session("SearchReportShowCompanyL2s")
end if
	
if session("SearchReportShowLicCompanyL2s") <> "" then
	bLicCompanyL2Search = session("SearchReportShowLicCompanyL2s")
end if
	
if session("SearchReportShowLicIssuedCompanyL2s") <> "" then
	bLicIssuedCompanyL2Search = session("SearchReportShowLicIssuedCompanyL2s")
end if	
	
if session("SearchReportShowCompanyL3s") <> "" then
	bCompanyL3Search = session("SearchReportShowCompanyL3s")
end if
	
if session("SearchReportShowLicCompanyL3s") <> "" then
	bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
end if		

if session("SearchReportShowLicIssuedCompanyL3s") <> "" then
	bLicIssuedCompanyL3Search = session("SearchReportShowLicIssuedCompanyL3s")
end if		
	
if session("SearchReportShowFirstName") <> "" then
	bFirstNameSearch = session("SearchReportShowFirstName")
end if

if session("SearchReportShowLastName") <> "" then
	bLastNameSearch = session("SearchReportShowLastName")
end if

if session("SearchReportShowSsn") <> "" then
	bSsnSearch = session("SearchReportShowSsn")
end if
	
if session("SearchReportShowEmployeeId") <> "" then
	bEmployeeIdSearch = session("SearchReportShowEmployeeId")
end if
	
if session("SearchReportShowPhone") <> "" then
	bPhoneSearch = session("SearchReportShowPhone")
end if

if session("SearchReportShowEmail") <> "" then
	bEmailSearch = session("SearchReportShowEmail")
end if
	
if session("SearchReportShowWorkAddress") <> "" then
	bWorkAddressSearch = session("SearchReportShowWorkAddress")
end if
	
if session("SearchReportShowWorkCity") <> "" then
	bWorkCitySearch = session("SearchReportShowWorkCity")
end if

if session("SearchReportShowWorkState") <> "" then
	bWorkStateSearch = session("SearchReportShowWorkState")
end if

if session("SearchReportShowWorkZip") <> "" then
	bWorkZipSearch = session("SearchReportShowWorkZip")
end if
	
if session("SearchReportShowSpecAddress") <> "" then
	bSpecAddressSearch = session("SearchReportShowSpecAddress")
end if
	
if session("SearchReportShowSpecCity") <> "" then
	bSpecCitySearch = session("SearchReportShowSpecCity")
end if

if session("SearchReportShowSpecState") <> "" then
	bSpecStateSearch = session("SearchReportShowSpecState")
end if

if session("SearchReportShowSpecZip") <> "" then
	bSpecZipSearch = session("SearchReportShowSpecZip")
end if

if session("SearchReportCompleted") <> "" then
	iShowCompleted = session("SearchReportCompleted")
end if
if iShowCompleted = 1 then
	oAssociate.SearchCourseCompleted = true
elseif iShowCompleted = 2 then
	oAssociate.SearchCourseCompleted = false
else
	oAssociate.SearchCourseCompleted = ""
end if


'Update the license object properties based on the search we just did.
'We'll use these properties later to search for the specific license records.
oLicense.LicenseStatusId = oAssociate.SearchLicenseStatusId
if oLicense.LicenseStatusId = 0 then
	oLicense.LicenseStatusId = ""
end if
oLicense.SearchStatusIdList = oAssociate.SearchLicenseStatusIdList
oLicense.LicenseNum = oAssociate.SearchLicenseNumber
oLicense.SearchLicenseExpDateFrom = oAssociate.SearchLicenseExpDateFrom
oLicense.SearchLicenseExpDateTo = oAssociate.SearchLicenseExpDateTo
oLicense.SearchAppDeadline = oAssociate.SearchAppDeadline


'Restrict the list if the user does not have company-wide access.  The lists
'will only be blank if specific entities were not selected, at which point we
'restrict the search to only the entities to which the user has access.  If 
'specific entities are being searched, they are checked for access before being
'added to the list, so we don't need to do it here.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	if oAssociate.SearchCompanyL2IdList = "" and _
	oAssociate.SearchCompanyL3IdList = "" and _
	oAssociate.SearchBranchIdList = "" then
		oAssociate.SearchBranchIdList = GetUserBranchIdList()
	end if
end if

	
set oRs = oAssociate.SearchAssociatesCoursesLicenses()
	

dim oWorkAddressRs
dim oCompanyLicRs
dim oBranchLicRs
dim oCompanyL2LicRs
dim oCompanyL3LicRs

'If we're searching Company licenses, run the search.
if bCompanySearch and not bCheckIsAdmin then
	bCompanySearch = false
end if
	
	
'If we're searching CompanyL2 licenses, run the search.
dim oCompanyL2
set oCompanyL2 = new CompanyL2
dim oCompanyL2Rs
if bCompanyL2Search then
	if session("SearchReportCompanyL2IdList") = "" and not bCheckIsAdmin then
		bCompanyL2Search = false
	end if
end if
if bCompanyL2Search then
	set oCompanyL2 = new CompanyL2
	oCompanyL2.ConnectionString = application("sDataSourceName")
	oCompanyL2.VocalErrors = application("bVocalErrors")
	oCompanyL2.CompanyId = session("UserCompanyId")
	
	oCompanyL2.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL2.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL2.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL2.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL2.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL2.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL2.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL2.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL2.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL2.Inactive = session("SearchReportInactive")
	
	set oCompanyL2Rs = oCompanyL2.SearchCompaniesLicenses
end if


'If we're searching CompanyL3 licenses, run the search.
dim oCompanyL3
set oCompanyL3 = new CompanyL3
dim oCompanyL3Rs
if bCompanyL3Search then
	if session("SearchReportCompanyL3IdList") = "" and not bCheckIsAdmin then
		bCompanyL3Search = false
	end if
end if
if bCompanyL3Search then
	set oCompanyL3 = new CompanyL3
	oCompanyL3.ConnectionString = application("sDataSourceName")
	oCompanyL3.VocalErrors = application("bVocalErrors")
	oCompanyL3.CompanyId = session("UserCompanyId")
	
	oCompanyL3.SearchStateIdList = session("SearchReportStateIdList")
	oCompanyL3.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oCompanyL3.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oCompanyL3.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oCompanyL3.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oCompanyL3.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oCompanyL3.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oCompanyL3.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oCompanyL3.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oCompanyL3.SearchAppDeadline = session("SearchReportAppDeadline")
	oCompanyL3.Inactive = session("SearchReportInactive")
	
	set oCompanyL3Rs = oCompanyL3.SearchCompaniesLicenses
end if
	
	
'If we're searching Branch licenses, run the search.
dim oBranchRs
if bBranchSearch then
	if session("SearchReportBranchIdList") = "" and not bCheckIsAdmin then
		bBranchSearch = false
	end if
end if
if bBranchSearch then
	set oBranch = nothing
	set oBranch = new Branch
	oBranch.ConnectionString = application("sDataSourceName")
	oBranch.TpConnectionString = application("sTpDataSourceName")
	oBranch.VocalErrors = application("bVocalErrors")
	oBranch.CompanyId = session("UserCompanyId")
			
	oBranch.SearchStateIdList = session("SearchReportStateIdList")
	oBranch.SearchLicStateIdList = session("SearchReportLicStateIdList")
	oBranch.SearchBranchIdList = session("SearchReportBranchIdList")
	oBranch.SearchCompanyL2IdList = session("SearchReportCompanyL2IdList")
	oBranch.SearchCompanyL3IdList = session("SearchReportCompanyL3IdList")
	oBranch.SearchLicenseStatusId = session("SearchReportLicenseStatusId")
	oBranch.SearchLicenseStatusIdList = session("SearchReportLicenseStatusIdList")
	oBranch.SearchLicenseNumber = session("SearchReportLicenseNumber")
	oBranch.SearchLicenseExpDateFrom = session("SearchReportLicenseExpDateFrom")
	oBranch.SearchLicenseExpDateTo = session("SearchReportLicenseExpDateTo")
	oBranch.SearchAppDeadline = session("SearchReportAppDeadline")
	oBranch.Inactive = session("SearchReportInactive")

	set oBranchRs = oBranch.SearchBranchesLicenses
end if

%>

<%
dim oPdf
dim oDoc
dim oPage
dim oDest
dim oTimesFont
dim oFimesFontBold
dim oCanvas
dim oImage
dim sParams
dim oParam
dim sText

set oPdf = Server.CreateObject("Persits.PDF")
set oDoc = oPdf.CreateDocument

oDoc.Creator = "ComplianceKeeper.com"

set oPage = oDoc.Pages.Add(612, 792)
set oFont = oDoc.Fonts("Arial")


'Write title
sParams = "x=0; y=630; width=612; alignment=center; size=25"
oPage.Canvas.DrawText "Compliance Report", sParams, oFont
sParams = "x=0; y=600; width=612; alignment=center; size=10"
oPage.Canvas.DrawText "Report Generated: " & Now(), sParams, oFont



'Draw line
with oPage.Canvas
	.MoveTo 50, 580
	.LineTo 562, 580
	.ClosePath
	.Stroke
end with


'Print CK Logo
set oImage = oDoc.OpenImage(server.MapPath("/Media/Images/logo.gif"))
set oParam = oPdf.CreateParam
oParam("x") = 30
oParam("y") = 650
oPage.Canvas.DrawImage oImage, oParam


'Print Company logo
if session("CorpLogoFileName") <> "" then
	set oImage = oDoc.OpenImage(server.MapPath("/usermedia/images/uploads/AssocLogos/" & session("CorpLogoFileName")))
	oParam.Clear
	oParam("x") = 306
	oParam("y") = 650
	oPage.Canvas.DrawImage oImage, oParam
end if


dim aList
dim iListId
dim oListRs
dim iCharsPrinted



sText = "<b>Reporting Criteria: </b><br>"

if (trim(session("SearchReportLastName")) <> "") then
	sText = sText & "Last name contains: " & session("SearchReportLastName") & "<br>"
end if

if (trim(session("SearchReportSsn")) <> "") then
	sText = sText & "SSN contains: " & session("SearchReportSsn") & "<br>"
end if

if (trim(session("SearchReportStateIdList")) <> "") then
	sText = sText & "State is: "
	aList = split(session("SearchReportStateIdList"), ",")
	for each iListId in aList 
		sText = sText & GetStateName(iListId, 1) & ", "
	next
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if

if (trim(session("SearchReportLicStateIdList")) <> "") then
	sText = sText & "License State is: "
	aList = split(session("SearchReportLicStateIdList"), ",")
	for each iListId in aList 
		sText = sText & GetStateName(iListId, 1) & ", "
	next
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if

if (trim(session("SearchReportCourseStateIdList")) <> "") then
	sText = sText & "Course State is: "
	aList = split(session("SearchReportCourseStateIdList"), ",")
	for each iListId in aList 
		sText = sText & GetStateName(iListId, 1) & ", "
	next
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if

if (trim(session("SearchReportBranchIdList")) <> "") then
	sText = sText & "Branch is: " 
	aList = split(session("SearchReportBranchIdList"), ",")
	for each iListId in aList
		if oBranch.LoadBranchById(iListId) <> 0 then
			sText = sText & oBranch.Name & ", "
		end if
	next		
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if

if (trim(session("SearchReportCompanyL3IdList")) <> "") then
	sText = sText & session("CompanyL3Name") & " is: " 
	aList = split(session("SearchReportCompanyL3IdList"), ",")
	for each iListId in aList
		if oCompanyL3.LoadCompanyById(iListId) <> 0 then
			sText = sText & oCompanyL3.Name & ", "
		end if
	next		
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if

if (trim(session("SearchReportCompanyL2IdList")) <> "") then
	sText = sText & session("CompanyL2Name") & " is: " 
	aList = split(session("SearchReportCompanyL2IdList"), ",")
	for each iListId in aList
		if oCompanyL2.LoadCompanyById(iListId) <> 0 then
			sText = sText & oCompanyL2.Name & ", "
		end if
	next			
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if
	
if (trim(session("SearchReportCourseName")) <> "") then
	sText = sText & "Course Name is: " & session("SearchReportCourseName") & "<br>"
end if
	
if (trim(session("SearchReportProviderId")) <> "") then
	set oProvider = new Provider
	oProvider.ConnectionString = application("sDataSourceName")
	oProvider.VocalErrors = application("bVocalErrors")
	if oProvider.LoadProviderById(session("SearchReportProviderId")) <> 0 then
		sText = sText & "Provider Name is: " & oProvider.Name & "<br>"
	end if
	set oProvider = nothing
end if
	
if (trim(session("SearchReportCreditHours")) <> "") then
	sText = sText & "Credit hours are: " & session("SearchReportCreditHours") & "<br>"
end if
	
if (trim(session("SearchReportCourseExpDateFrom")) <> "") then
	sText = sText & "Course expiration date from: " & session("SearchReportCourseExpDateFrom") & "<br>"
end if
	
if (trim(session("SearchReportCourseExpDateTo")) <> "") then
	sText = sText & "Course expiration date to: " & session("SearchReportCourseExpDateTo") & "<br>"
end if

if (trim(session("SearchReportLicenseStatusId")) <> "") then
	sText = sText & "License status is: " & oLicense.LookupLicenseStatus(session("SearchReportLicenseStatusId")) & "<br>"
end if

if (trim(session("SearchReportLicenseStatusIdList")) <> "") then
	sText = sText & "License status is " 
	aList = split(session("SearchReportLicenseStatusIdList"), ",")
	for each iListId in aList
		sText = sText & oLicense.LookupLicenseStatus(iListId) & ", "
	next
	sText = left(sText, len(sText) - 2)
	sText = sText & "<br>"
end if
	
if (trim(session("SearchReportLicenseNumber")) <> "") then
	sText = sText & "License number contains: " & session("SearchReportLicenseNumber") & "<br>"
end if
	
if (trim(session("SearchReportLicensePayment")) <> "") then
	sText = sText & "License Payment contains: " & session("SearchReportLicensePayment") & "<br>"
end if
	
if (trim(session("SearchReportLicenseExpDateFrom")) <> "") then
	sText = sText & "License date from: " & session("SearchReportLicenseExpDateFrom") & "<br>"
end if
	
if (trim(session("SearchReportLicenseExpDateTo")) <> "") then
	sText = sText & "License date to: " & session("SearchReportLicenseExpDateTo") & "<br>"
end if

if (trim(session("SearchReportAppDeadline")) <> "") then
	if session("SearchReportAppDeadline") = "1" then
		sText = sText & "License date range applies to Renewal Application Deadline and License Exp. Date<br>"
	elseif session("SearchReportAppDeadline") = "2" then
		sText = sText & "License date range applies to Renewal Application Deadline<br>"
	elseif session("SearchReportAppDeadline") = "3" then
		sText = sText & "License date range applies to Renewal Submission Date<br>"
	elseif session("SearchReportAppDeadline") = "4" then
		sText = sText & "License date range applies to Submission Date<br>"
	elseif session("SearchReportAppDeadline") = "5" then
		sText = sText & "License date range applies to Issue Date<br>"
	else
		sText = sText & "License date range applies to License Expiration Date"
	end if
end if

if session("SearchReportInactive") <> "" then
	if session("SearchReportInactive") = "1" then
		sText = sText & "Show only inactive records<br>"
	else
		sText = sText & "Show only active records<br>"
	end if
end if
	
if bAdminSearch then
	sText = sText & "Show administrators<br>"
end if		

if bManagerSearch then
	sText = sText & "Show managers<br>"
end if

if bCourseSearch then
	sText = sText & "Show courses<br>"
end if		

if bOfficerSearch then
	sText = sText & "Show loan officer matches<br>"
	bOfficerSearch = session("SearchReportShowOfficers")
end if

if bLicenseSearch then 
	sText = sText & "Show loan officer licenses<br>"
end if
	
if bLicIssuedSearch then 
	sText = sText & session("SearchReportShowLicIssued") & "Show loan officer license issue date<br>"
end if

if bCompanySearch then
	sText = sText & "Show company licenses<br>"
end if
	
if bBranchSearch <> "" then
	sText = sText & "Show branch matches<br>"
end if
	
if bLicBranchSearch then
	sText = sText & "Show branch licenses<br>"
end if
	
if bLicIssuedBranchSearch then
	sText = sText & "Show branch license issue date<br>"
end if
	
if bCompanyL2Search then
	sText = sText & "Show " & lcase(session("CompanyL2Name")) & " matches<br>"
end if
	
if bLicCompanyL2Search then
	sText = sText & "Show " & lcase(session("CompanyL2Name")) & " licenses<br>"
end if
	
if bLicIssuedCompanyL2Search then
	sText = sText & "Show " & lcase(session("CompanyL2Name")) & " license issue date<br>"
end if	
	
if bCompanyL3Search then
	sText = sText & "Show " & lcase(session("CompanyL3Name")) & " matches<br>"
end if
	
if bLicCompanyL3Search then
	sText = sText & "Show " & lcase(session("CompanyL3Name")) & " licenses<br>"
	bLicCompanyL3Search = session("SearchReportShowLicCompanyL3s")
end if		

if bLicIssuedCompanyL3Search then
	sText = sText & "Show " & lcase(session("CompanyL3Name")) & " license issue date<br>"
end if		
	
if bFirstNameSearch then
	sText = sText & "Show first name<br>"
end if

if bSsnSearch then
	sText = sText & "Show SSN<br>"
end if
	
if bEmployeeIdSearch then
	sText = sText & "Show employee ID<br>"
end if
	
if bPhoneSearch then
	sText = sText & "Show phone number<br>"
end if

if bEmailSearch then
	sText = sText & "Show email address<br>"
end if
	
if bWorkAddressSearch then
	sText = sText & "Show work address<br>"
end if
	
if bWorkCitySearch then
	sText = sText & "Show work city<br>"
end if

if bWorkStateSearch then
	sText = sText & "Show work state<br>"
end if

if bWorkZipSearch then
	sText = sText & "Show work zipcode<br>"
end if
	
if bSpecAddressSearch then
	sText = sText & "Show address<br>"
end if
	
if bSpecCitySearch then
	sText = sText & "Show city<br>"
end if

if bSpecStateSearch then
	sText = sText & "Show state<br>"
end if

if bSpecZipSearch then
	sText = sText & "Show zipcode<br>"
end if



'Write out the criteria across multiple pages
sParams = "x=50; y=560; width=500; height=550; alignment=center; size=10; html=true"
do while len(sText) > 0
	iCharsPrinted = oPage.Canvas.DrawText(sText, sParams, oFont)
	
	if iCharsPrinted = len(sText) then exit do
	
	set oPage = oPage.NextPage
	
	sText = "<b>Reporting Criteria (continued): </b><br>" & right(sText, len(sText) - iCharsPrinted)
	sParams = "x=50; y=750; width=500; height=600; alignment=center; size=10; html=true"
loop
	

set oPage = oPage.NextPage
sText = ""






dim sPrevUserId
dim sSp
sSp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"

dim oAssocRs 'Recordset to use within the associate listing

	if bCompanySearch then
	
		oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
		oLicense.OwnerId = session("UserCompanyId")
		oLicense.OwnerTypeId = 3
		set oCompanyLicRs = oLicense.SearchLicenses
		oLicense.OwnerTypeId = 1
	
		if not (oCompanyLicRs.BOF and oCompanyLicRs.EOF) then

			sText = sText & "<p>&nbsp;&nbsp;<b>" & oCompany.Name & "</b><br>"
			
			if bAdminSearch then
				set oAdminRs = oCompany.GetAdmins(session("UserCompanyId"))
				if not oAdminRs.EOF then
					sText = sText & sSp & "Administrators: "
					do while not oAdminRs.EOF 
						sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
						oAdminRs.MoveNext
						if not oAdminRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if 
			
			if bPhoneSearch and oCompany.Phone <> "" then
				sText = sText & sSp & "Phone: " & oCompany.Phone
				if oCompany.PhoneExt <> "" then
					sText = sText & " ext. " & oCompany.PhoneExt
				end if
				sText = sText & "<br>"
			end if
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oCompany.Address <> "" or oCompany.City <> "" or oCompany.StateId <> 0 or oCompany.Zipcode <> "" then
					sText = sText & sSp & "Address: "
					if bSpecAddressSearch and oCompany.Address <> "" then
						sText = sText & oCompany.Address
						if oCompany.Address2 <> "" then
							sText = sText & " " & oCompany.Address2
						end if
						if (bSpecCitySearch and oCompany.City <> "") or (bSpecStateSearch and oCompany.StateId <> "") or _
							(bSpecZipSearch and oCompany.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecCitySearch and oCompany.City <> "" then
						sText = sText & oCompany.City
						if (bSpecStateSearch and oCompany.StateId <> "") or (bSpecZipSearch and oCompany.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecStateSearch and oCompany.StateId <> "" then
						sText = sText & GetStateName(oCompany.StateId, 1)
						if (bSpecZipSearch and oCompany.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecZipSearch and oCompany.Zipcode <> "" then
						sText = sText & oCompany.Zipcode
						if oCompany.ZipcodeExt <> "" then	
							sText = sText & "-" & oCompany.ZipcodeExt
						end if
					end if										
					sText = sText & "<br>"
				end if 
			end if 
	
			do while not oCompanyLicRs.EOF
				sText = sText & sSp & "License #" & oCompanyLicRs("LicenseNum") & " - " & oLicense.LookupState(oCompanyLicRs("LicenseStateId"))
				if oCompanyLicRs("LicenseExpDate") <> "" then 
					sText = sText & ", Expires: " & oCompanyLicRs("LicenseExpDate")
				end if 
				if oCompanyLicRs("LicenseExpDate") < date() + 90 and oCompanyLicRs("LicenseExpDate") > date() then
					sText = sText & "   <b>(<font color=""#cc0000"">" & round(oCompanyLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
				end if
				sText = sText & "<br>"
				if oCompanyLicRs("LicenseAppDeadline") <> "" then
					sText = sText & sSp & sSp & "Renewal App Deadline: " & oCompanyLicRs("LicenseAppDeadline")
					if oCompanyLicRs("LicenseAppDeadline") < date() + 90 and oCompanyLicRs("LicenseAppDeadline") > date() then
						sText = sText & "   <b>(<font color=""#cc0000"">" & round(oCompanyLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
					end if
					sText = sText & "<br>"
				end if
				oCompanyLicRs.MoveNext
			loop
			
		end if 
	end if 
	
	
	if bCompanyL2Search then
		do while not oCompanyL2Rs.EOF

			sText = sText & "<p>&nbsp;&nbsp;<b>" & oCompanyL2Rs("Name") & "</b><br>"
			
			if bAdminSearch then
				set oAdminRs = oCompanyL2.GetAdmins(oCompanyL2Rs("CompanyL2Id"))
				if not oAdminRs.EOF then
					sText = sText & sSp & "Administrators: "
					do while not oAdminRs.EOF 
						sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
						oAdminRs.MoveNext
						if not oAdminRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oManager.OwnerTypeId = 2
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					sText = sText & sSp & "Managers: "
					do while not oManagerRs.EOF
						sText = sText & oManagerRs("FullName")
						oManagerRs.MoveNext
						if not oManagerRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if
			
			oCompanyL2.LoadCompanyById(oCompanyL2Rs("CompanyL2Id"))
			
			if bPhoneSearch and oCompanyL2.Phone <> "" then
				sText = sText & sSp & "Phone: " & oCompanyL2.Phone 
				if oCompanyL2.PhoneExt <> "" then
					sText = sText & " ext. " & oCompanyL2.PhoneExt
				end if
				sText = sText & "<br>"
			end if
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oCompanyL2.Address <> "" or oCompanyL2.City <> "" or oCompanyL2.StateId <> 0 or oCompanyL2.Zipcode <> "" then
					sText = sText & sSp & "Address: "
					if bSpecAddressSearch and oCompanyL2.Address <> "" then
						sText = sText & oCompanyL2.Address
						if oCompanyL2.Address2 <> "" then
							sText = sText & " " & oCompanyL2.Address2
						end if
						if (bSpecCitySearch and oCompanyL2.City <> "") or (bSpecStateSearch and oCompanyL2.StateId <> "") or _
							(bSpecZipSearch and oCompanyL2.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecCitySearch and oCompanyL2.City <> "" then
						sText = sText & oCompanyL2.City
						if (bSpecStateSearch and oCompanyL2.StateId <> "") or (bSpecZipSearch and oCompanyL2.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecStateSearch and oCompanyL2.StateId <> "" then
						sText = sText & GetStateName(oCompanyL2.StateId, 1)
						if (bSpecZipSearch and oCompanyL2.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecZipSearch and oCompanyL2.Zipcode <> "" then
						sText = sText & oCompanyL2.Zipcode
						if oCompanyL2.ZipcodeExt <> "" then	
							sText = sText & "-" & oCompanyL2.ZipcodeExt
						end if
					end if								
					sText = sText & "<br>"		
				end if 
			end if 
						
			if bLicCompanyL2Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL2Rs("CompanyL2Id")
				oLicense.OwnerTypeId = 4
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					sText = sText & sSp & "License #" & oBranchLicRs("LicenseNum") & " - " & oLicense.LookupState(oBranchLicRs("LicenseStateId"))
					if oBranchLicRs("LicenseExpDate") <> "" then
						sText = sText & ", Expires: " & oBranchLicRs("LicenseExpDate") 
					end if
					if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
						sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
					end if
					sText = sText & "<br>"
					if oBranchLicRs("LicenseAppDeadline") <> "" then
						sText = sText & sSp & sSp & "Renewal App Deadline: " & oBranchLicRs("LicenseAppDeadline")
						if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
							sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
						end if
						sText = sText & "<br>"
					end if
					if bLicIssuedCompanyL2Search and oBranchLicRs("IssueDate") <> "" then 
						sText = sText & sSp & sSp & "Issued: " & oBranchLicRs("IssueDate") & "<br>"
					else
						sText = sText & "<br>"
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL2Rs.MoveNext
					
		loop
	end if 
	
	
	if bCompanyL3Search then
		do while not oCompanyL3Rs.EOF
		
			sText = sText & "<p>&nbsp;&nbsp;<b>" & oCompanyL3Rs("Name") & "</b><br>"
			
			if bAdminSearch then
				set oAdminRs = oCompanyL3.GetAdmins(oCompanyL3Rs("CompanyL3Id"))
				if not oAdminRs.EOF then
					sText = sText & sSp & "Administrators: "
					do while not oAdminRs.EOF 
						sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
						oAdminRs.MoveNext
						if not oAdminRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oManager.OwnerTypeId = 3
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					sText = sText & sSp & "Managers: "
					do while not oManagerRs.EOF
						sText = sText & oManagerRs("FullName")
						oManagerRs.MoveNext
						if not oManagerRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if
			
			oCompanyL3.LoadCompanyById(oCompanyL3Rs("CompanyL3Id"))
			
			if bPhoneSearch and oCompanyL3.Phone <> "" then
				sText = sText & sSp & "Phone: " & oCompanyL3.Phone
				if oCompanyL3.PhoneExt <> "" then
					sText = sText & " ext. " & oCompanyL3.PhoneExt
				end if
				sText = sText & "<br>"
			end if
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oCompanyL3.Address <> "" or oCompanyL3.City <> "" or oCompanyL3.StateId <> 0 or oCompanyL3.Zipcode <> "" then
					sText = sText & sSp & "Address: "
					if bSpecAddressSearch and oCompanyL3.Address <> "" then
						sText = sText & oCompanyL3.Address
						if oCompanyL3.Address2 <> "" then
							sText = sText & " " & oCompanyL3.Address2
						end if
						if (bSpecCitySearch and oCompanyL3.City <> "") or (bSpecStateSearch and oCompanyL3.StateId <> "") or _
							(bSpecZipSearch and oCompanyL3.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecCitySearch and oCompanyL3.City <> "" then
						sText = sText & oCompanyL3.City
						if (bSpecStateSearch and oCompanyL3.StateId <> "") or (bSpecZipSearch and oCompanyL3.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecStateSearch and oCompanyL3.StateId <> "" then
						sText = sText & GetStateName(oCompanyL3.StateId, 1)
						if (bSpecZipSearch and oCompanyL3.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecZipSearch and oCompanyL3.Zipcode <> "" then
						sText = sText & oCompanyL3.Zipcode
						if oCompanyL3.ZipcodeExt <> "" then	
							sText = sText & "-" & oCompanyL3.ZipcodeExt
						end if
					end if						
					sText = sText & "<br>"				
				end if 
			end if 
			
			if bLicCompanyL3Search then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oCompanyL3Rs("CompanyL3Id")
				oLicense.OwnerTypeId = 5
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					sText = sText & sSp & "License #" & oBranchLicRs("LicenseNum") & " - " & oLicense.LookupState(oBranchLicRs("LicenseStateId"))
					if oBranchLicRs("LicenseExpDate") <> "" then 
						sText = sText & ", Expires: " & oBranchLicRs("LicenseExpDate")
					end if
					if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
						sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
					end if
					sText = sText & "<br>"
					if oBranchLicRs("LicenseAppDeadline") <> "" then
						sText = sText & sSp & sSp & "Renewal App Deadline: " & oBranchLicRs("LicenseAppDeadline")
						if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
							sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
						end if
						sText = sText & "<br>"
					end if
					if bLicIssuedCompanyL3Search and oBranchLicRs("IssueDate") <> "" then 
						sText = sText & sSp & sSp & "Issued: " & oBranchLicRs("IssueDate") & "<br>"
					else
						sText = sText & "<br>"
					end if	
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oCompanyL3Rs.MoveNext
					
		loop
	end if 
	
	
	if bBranchSearch then
	
		do while not oBranchRs.EOF
		
			sText = sText & "<p>&nbsp;&nbsp;<b>" & oBranchRs("Name") 
			if oBranchRs("BranchNum") <> "" then
				sText = sText & " (#" & oBranchRs("BranchNum") & ") "
			end if
			sText = sText & "</b><br>"
			
			if bAdminSearch then
				set oAdminRs = oBranch.GetAdmins(oBranchRs("BranchId"))
				if not oAdminRs.EOF then
					sText = sText & sSp & "Administrators: "
					do while not oAdminRs.EOF 
						sText = sText & oAdminRs("FirstName") & " " & oAdminRs("LastName") & " (" & oAdminRs("Email") & ")"
						oAdminRs.MoveNext
						if not oAdminRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if 
			
			if bManagerSearch then
				oManager.OwnerId = oBranchRs("BranchId")
				oManager.OwnerTypeId = 1
				set oManagerRs = oManager.SearchManagers
				if not oManagerRs.EOF then
					sText = sText & sSp & "Managers: "
					do while not oManagerRs.EOF
						sText = sText & oManagerRs("FullName")
						oManagerRs.MoveNext
						if not oManagerRs.EOF then sText = sText & ", "
					loop
					sText = sText & "<br>"
				end if
			end if
			
			oBranch.LoadBranchById(oBranchRs("BranchId"))
			
			if bPhoneSearch and oBranch.Phone <> "" then
				sText = sText & sSp & "Phone: " & oBranch.Phone
				if oBranch.PhoneExt <> "" then
					sText = sText & " ext. " & oBranch.PhoneExt
				end if
				sText = sText & "<br>"
			end if
			
			if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
				if oBranch.Address <> "" or oBranch.City <> "" or oBranch.StateId <> 0 or oBranch.Zipcode <> "" then
					sText = sText & sSp & "Address: "
					if bSpecAddressSearch and oBranch.Address <> "" then
						sText = sText & oBranch.Address
						if oBranch.Address2 <> "" then
							sText = sText & " " & oBranch.Address2
						end if
						if (bSpecCitySearch and oBranch.City <> "") or (bSpecStateSearch and oBranch.StateId <> "") or _
							(bSpecZipSearch and oBranch.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecCitySearch and oBranch.City <> "" then
						sText = sText & oBranch.City
						if (bSpecStateSearch and oBranch.StateId <> "") or (bSpecZipSearch and oBranch.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecStateSearch and oBranch.StateId <> "" then
						sText = sText & GetStateName(oBranch.StateId, 1)
						if (bSpecZipSearch and oBranch.Zipcode <> "") then
							sText = sText & ", "
						end if
					end if
					if bSpecZipSearch and oBranch.Zipcode <> "" then
						sText = sText & oBranch.Zipcode
						if oBranch.ZipcodeExt <> "" then	
							sText = sText & "-" & oBranch.ZipcodeExt
						end if
					end if	
					sText = sText & "<br>"									
				end if 
			end if 			
			
			if bLicBranchSearch then
			
				oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
				oLicense.OwnerId = oBranchRs("BranchId")
				oLicense.OwnerTypeId = 2
				set oBranchLicRs = oLicense.SearchLicenses
				oLicense.OwnerTypeId = 1
			
				do while not oBranchLicRs.EOF
					sText = sText & sSp & "License #" & oBranchLicRs("LicenseNum") & " - " & oLicense.LookupState(oBranchLicRs("LicenseStateId"))
					if oBranchLicRs("LicenseExpDate") <> "" then 
						sText = sText & ", Expires: " & oBranchLicRs("LicenseExpDate")
					end if
					if oBranchLicRs("LicenseExpDate") < date() + 90 and oBranchLicRs("LicenseExpDate") > date() then
						sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
					end if
					sText = sText & "<br>"
					if oBranchLicRs("LicenseAppDeadline") <> "" then
						sText = sText & sSp & sSp & "Renewal App Deadline: " & oBranchLicRs("LicenseAppDeadline")
						if oBranchLicRs("LicenseAppDeadline") < date() + 90 and oBranchLicRs("LicenseAppDeadline") > date() then
							sText = sText & "   <b>(<font color=""#cc0000"">" & round(oBranchLicRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
						end if
						sText = sText & "<br>"
					end if
					if bLicIssuedBranchSearch and oBranchLicRs("IssueDate") <> "" then 
						sText = sText & sSp & sSp & "Issued: " & oBranchLicRs("IssueDate") & "<br>"
					else
						sText = sText & "<br>"
					end if
					oBranchLicRs.MoveNext
				loop
				
			end if 
			
			oBranchRs.MoveNext
					
		loop	
	end if 


	if bOfficerSearch then 
	if not (oRs.BOF and oRs.EOF) then
	
		set oAssociate = nothing
		set oAssociate = new Associate
		oAssociate.ConnectionString = application("sDataSourceName")
		oAssociate.TpConnectionString = application("sTpDataSourceName")
		oAssociate.VocalErrors = true
		
		oAssociate.SearchCourseName = session("SearchReportCourseName")
		oAssociate.SearchCourseProviderId = session("SearchReportProviderId")
		oAssociate.SearchCourseExpDateFrom = session("SearchReportCourseExpDateFrom")
		oAssociate.SearchCourseExpDateTo = session("SearchReportCourseExpDateTo")
		if iShowCompleted = 1 then
			oAssociate.SearchCourseCompleted = true
		elseif iShowCompleted = 2 then
			oAssociate.SearchCourseCompleted = false
		else
			oAssociate.SearchCourseCompleted = ""
		end if
	
	
		iCount = 0
		do while not oRs.EOF
		
		
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
		
				sText = sText & "<p>&nbsp;&nbsp;<b>" & oAssociate.LastName & "</b>"
				if bFirstNameSearch then 
					sText = sText & "<b>, " & oAssociate.FirstName & "</b>"
				end if
				if bSsnSearch and oAssociate.Ssn <> "" then
					sText = sText & " " & enDeCrypt(oAssociate.Ssn,application("RC4Pass"))
				end if
				if bEmailSearch and oAssociate.Email <> "" then
					sText = sText & " (" & oAssociate.Email & ")"
				end if
				sText = sText & "<br>"
					
				if bEmployeeIdSearch and oAssociate.EmployeeId <> "" then
					sText = sText & sSp & "Employee ID: " & oAssociate.EmployeeId & "<br>"
				end if 
					
				if bPhoneSearch and oAssociate.Phone <> "" then
					sText = sText & sSp & "Phone: " & oAssociate.Phone & "<br>"
				end if 
					
					if bWorkAddressSearch or bWorkCitySearch or bWorkStateSearch or bWorkZipSearch then
						set oWorkAddressRs = oAssociate.GetWorkAddress
						if not oWorkAddressRs.EOF then
						if oWorkAddressRs("Address") <> "" or oWorkAddressRs("City") <> "" or oWorkAddressRs("StateId") <> 0 or oWorkAddressRs("Zipcode") <> "" then
								sText = sText & sSp & "Office Address: "
									'if not oWorkAddressRs.EOF then
										if bWorkAddressSearch and oWorkAddressRs("Address") <> "" then
											sText = sText & oWorkAddressRs("Address")
											if oWorkAddressRs("Address2") <> "" then
												sText = sText & " " & oWorkAddressRs("Address2")
											end if
											if (bWorkCitySearch and oWorkAddressRs("City") <> "") or (bWorkStateSearch and oWorkAddressRs("StateId") <> "") or _
												(bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												sText = sText & ", "
											end if
										end if
										if bWorkCitySearch and oWorkAddressRs("City") <> "" then
											sText = sText & oWorkAddressRs("City")
											if (bWorkStateSearch and oWorkAddressRs("StateId") <> "") or (bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												sText = sText & ", "
											end if
										end if
										if bWorkStateSearch and oWorkAddressRs("StateId") <> "" then
											sText = sText & GetStateName(oWorkAddressRs("StateId"), 1)
											if (bWorkZipSearch and oWorkAddressRs("Zipcode") <> "") then
												sText = sText & ", "
											end if
										end if
										if bWorkZipSearch and oWorkAddressRs("Zipcode") <> "" then
											sText = sText & oWorkAddressRs("Zipcode")
											if oWorkAddressRs("ZipcodeExt") <> "" then	
												sText = sText & "-" & oWorkAddressRs("ZipcodeExt")
											end if
										end if										
									'end if 
								sText = sText & "<br>"
						end if 
						end if 
					end if 
					
					if bSpecAddressSearch or bSpecCitySearch or bSpecStateSearch or bSpecZipSearch then
						if oAssociate.Address <> "" or oAssociate.City <> "" or oAssociate.StateId <> 0 or oAssociate.Zipcode <> "" then
							sText = sText & sSp & "Address: " 
									if bSpecAddressSearch and oAssociate.Address <> "" then
										sText = sText & oAssociate.Address
										if oAssociate.Address2 <> "" then
											sText = sText & " " & oAssociate.Address2
										end if
										if (bSpecCitySearch and oAssociate.City <> "") or (bSpecStateSearch and oAssociate.StateId <> "") or _
											(bSpecZipSearch and oAssociate.Zipcode <> "") then
											sText = sText & ", "
										end if
									end if
									if bSpecCitySearch and oAssociate.City <> "" then
										sText = sText & oAssociate.City
										if (bSpecStateSearch and oAssociate.StateId <> "") or (bSpecZipSearch and oAssociate.Zipcode <> "") then
											sText = sText & ", "
										end if
									end if
									if bSpecStateSearch and oAssociate.StateId <> "" then
										sText = sText & GetStateName(oAssociate.StateId, 1)
										if (bSpecZipSearch and oAssociate.Zipcode <> "") then
											sText = sText & ", "
										end if
									end if
									if bSpecZipSearch and oAssociate.Zipcode <> "" then
										sText = sText & oAssociate.Zipcode
										if oAssociate.ZipcodeExt <> "" then	
											sText = sText & "-" & oAssociate.ZipcodeExt
										end if
									end if	
									
							sText = sText & "<br>"									
						end if 
					end if 

					

					if bLicenseSearch then 
					
						oLicense.SearchStateIdList = session("SearchReportLicStateIdList")
						oLicense.OwnerId = oAssociate.UserId
						set oAssocRs = oLicense.SearchLicenses					
					
						do while not oAssocRs.EOF 
							sText = sText & sSp & "License #" & oAssocRs("LicenseNum") & " - " & oLicense.LookupState(oAssocRs("LicenseStateId"))
							if oAssocRs("LicenseExpDate") <> "" then
								sText = sText & ", Expires: " & oAssocRs("LicenseExpDate")
							end if
							if oAssocRs("LicenseExpDate") < date() + 90 and oAssocRs("LicenseExpDate") > date() then
								sText = sText & "   <b>(<font color=""#cc0000"">" & round(oAssocRs("LicenseExpDate") - date(), 0) & " Days</font>)</b>"
							end if
							sText = sText & "<br>"
							if oAssocRs("LicenseAppDeadline") <> "" then
								sText = sText & sSp & sSp & "Renewal App Deadline: " & oAssocRs("LicenseAppDeadline")
								if oAssocRs("LicenseAppDeadline") < date() + 90 and oAssocRs("LicenseAppDeadline") > date() then
									sText = sText & "   <b>(<font color=""#cc0000"">" & round(oAssocRs("LicenseAppDeadline") - date(), 0) & " Days</font>)</b>"
								end if
								sText = sText & "<br>"
							end if
							oAssocRs.MoveNext
						loop
					
					end if
					
										
					if bCourseSearch then
					
						set oAssocRs = oAssociate.SearchCourses(oAssociate.SearchCourseName, oAssociate.SearchCourseProviderId, oAssociate.SearchCourseExpDateFrom, oAssociate.SearchCourseExpDateTo, oAssociate.SearchCourseCompleted)
						do while not oAssocRs.EOF
						
							if oCourse.LoadCourseById(oAssocRs("CourseId")) <> 0 then
							
								if oAssocRs("UserSpecRenewalDate") <> "" then
									dCourseExpDate = oAssocRs("UserSpecRenewalDate")
								else
									dCourseExpDate = oAssocRs("CertificateExpirationDate")
								end if
							
								if session("SearchReportStateIdList") = "" or instr(1, "," & session("SearchReportStateIdList") & ",", "," & oCourse.StateId & ",") then
									sText = sText & sSp & oCourse.Name & ", Expires: " & dCourseExpDate & "<br>"
								end if 				
							
							end if 
							oAssocRs.MoveNext
					
						loop

					end if
								
			end if 
		
			oRs.MoveNext	
			iCount = iCount + 1
	
		loop	

	else
	
		sText = sText & "<p> </p>"

	end if
	end if 
	
	
	
'Write out the records across multiple pages
sParams = "x=50; y=750; width=500; height=700; alignment=center; size=10; html=true"
do while len(sText) > 0
	iCharsPrinted = oPage.Canvas.DrawText(sText, sParams, oFont)
	
	if iCharsPrinted = len(sText) then exit do
	
	set oPage = oPage.NextPage
	
	sText = right(sText, len(sText) - iCharsPrinted)
	sParams = "x=50; y=750; width=500; height=700; alignment=center; size=10; html=true"
loop
	

oDoc.SaveHttp "attachment;filename=report.pdf"


set oAssociate = nothing
set oRs = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
'	PrintShellFooter SHOW_MENUS										' From shell.asp
'	EndPage														' From htmlElements.asp
%>
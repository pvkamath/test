UPDATE StateRequirements SET RequirementsText =
'<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: large">Alaska Requirements</SPAN></P>
<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375><SPAN style="FONT-FAMILY: Verdana,Arial,Helvetica,sans-serif"><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Department of Commerce, Community&nbsp;and Economic Development: Division of Banking &amp; Securities </SPAN></SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Juneau Office: 150 3rd Street, Suite 217, Juneau, AK&nbsp; 99801 Anchorage Office: 550 West Seventh Ave, Suite 940,&nbsp;Anchorage, AK 99501 <BR></SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Juneau: (907) 465-2521, (888) 925-2521/(907) 465-2549 <BR></SPAN></SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Anchorage: (907) 269-8140, (888) 925-2521/(907) 269-8146 </SPAN></SPAN></P></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><A href="http://www.dced.state.ak.us/bsc/home.htm"><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">http://www.dced.state.ak.us/bsc/home.htm</SPAN></SPAN></A></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=ak#Type_1" target=_self>Mortgage Broker</A><BR><A href="stateguide.asp?state=ak#Type_2" target=_self>Mortgage Lender</A><BR><A href="stateguide.asp?state=ak#Type_3" target=_self>Mortgage Loan Originator</A><BR></STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Broker<A name=Type_1></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0"><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">Updating Information (In Progress)</TD></TR>
<TR>
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center" width=360>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Lender<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center">&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Oirginators<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center" scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">$680 &nbsp;( includes NMLS processing fee)</DIV></DIV></TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH width=170 scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">20&nbsp;Hours NMLS&nbsp;Approved Pre-Licensing Education and Sponsorship by a Licensed AK Company</DIV></TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and AK Test Components</DIV></TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">8 Hours NMLS Approved CE, Annually</DIV></DIV></TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">*Must pay a fee into the surety fund</DIV></TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR style="BACKGROUND-COLOR: #c0c0c0">
<TH scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">Licenses Expire Annually on December 31st</DIV></DIV></TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG> 4/13/2011</SPAN></P>'
WHERE StateAbbrev = 'AK'
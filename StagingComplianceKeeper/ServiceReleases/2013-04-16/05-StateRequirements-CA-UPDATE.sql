UPDATE StateRequirements SET RequirementsText =
'<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: large">California Requirements</SPAN></P>
<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375><SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Department of Real Estate and the Department of Corporations</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small"><SPAN style="FONT-WEIGHT: bold">Department of Real Estate:<BR><SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN></SPAN></SPAN></SPAN>2201 Broadway <BR>Sacremento, CA 95818-2500<BR><BR><SPAN style="FONT-WEIGHT: bold">Department of Corporations:<BR></SPAN>1515 K Street, Suite 200<BR>Sacremento, CA 95814-4052<SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN></SPAN></SPAN><BR></SPAN></SPAN></SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">&nbsp;</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small"><SPAN style="FONT-WEIGHT: bold">Department of Real Estate: </SPAN><BR>(916)227-0770/ (916) 227-0777<BR><BR><SPAN style="FONT-WEIGHT: bold">Department of Corporations:<BR><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN></SPAN>(916) 445-7205<SPAN style="FONT-WEIGHT: bold"><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN></SPAN></SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><A href="http://www.mld.nv.gov/index.htm"><SPAN style="COLOR: #000000"><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN></SPAN></A><A href="http://www.dre.ca.gov/">http://www.dre.ca.gov/</A> and <A href="http://www.corp.ca.gov/">http://www.corp.ca.gov/</A></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=ca#Type_1">Mortgage Loan Originator</A><BR><A href="stateguide.asp?state=ca#Type_2">Real Estate Salesperson</A><BR><A href="stateguide.asp?state=ca#Type_3">Real Estate Broker License</A><BR></STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Originator (DOC)</SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">License</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV>
<DIV style="TEXT-ALIGN: center">$330 (includes NMLS processing fee)</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">20 Hours NMLS Approved Pre-Licensing Education and Sponsorship by a Licensed CA Company</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and CA Test Components</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">8 Hours NMLS Approved CE, Annnually</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center">By December 31st</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Covered by Employer''s Surety Bond</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center">Yes</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></DIV></TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Real Estate Salesperson<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Infomation (In Progress)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center">&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Real Estate Broker License<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG> 4/13/2011</SPAN></P>'
WHERE StateAbbrev = 'CA'
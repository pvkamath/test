UPDATE StateRequirements SET RequirementsText =
'<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: large">Georgia Requirements</SPAN></P>
<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375><SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Georgia Department of Banking and Finance </SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">2990 Brandywine Road, Suite 200<BR>Atlanta GA, 30341-5565</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">(770) 986-1633, (888) 986-1633&nbsp;/ 770-986-1654</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><A href="http://disr.dc.gov/disr/site/default.asp"></A><A href="http://disr.dc.gov/disr/site/default.asp"></A><A href="http://disr.dc.gov/disr/site/default.asp"></A><A href="http://dbf.georgia.gov/02/dbf/home/0,2477,43414745,00.html">http://dbf.georgia.gov/02/dbf/home/0,2477,43414745,00.html</A></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=ga#Type_1">Mortgage Brokers</A><BR><A href="stateguide.asp?state=ga#Type_2">Mortgage Lenders</A><BR><A href="stateguide.asp?state=ga#Type_3">Mortgage Loan Servicers</A><BR><A href="stateguide.asp?state=ga#Type_4">Mortgage Loan Originator</A> </STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Brokers<A name=Type_1></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">Updating Information (In Progress)</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Lenders<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Servicers<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV>
<DIV>
<DIV>&nbsp;</DIV></DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>&nbsp;</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Originator <A name=Type_4></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #cccccc" scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV style="TEXT-ALIGN: center">$130 (includes NMLS processing fee)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">20&nbsp;Hours NMLS&nbsp;Approved Pre-Licensing Education and&nbsp;Sponsorship&nbsp;by a Licensed GA Company</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and GA Test Components</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">8 Hours NMLS Approved CE, Annually (Including 1 hr. GA Specific Content)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">October&nbsp;31st</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">$10,000 (up to $10,000,000 in origination) $15,000 (over $10,000,000 in origination)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">November 30th (Renewal accepted through 12/31 with $100 late fee)</DIV></TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG>&nbsp;5/12/2011</SPAN></P>'
WHERE StateAbbrev = 'GA'
UPDATE StateRequirements SET RequirementsText =
'<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375><SPAN style="FONT-FAMILY: Verdana,Arial,Helvetica,sans-serif">&nbsp;</SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Kentucky Department of Financial Institutions</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">1025 Capitol Center Drive, Suite 200<BR>Frankfort, KY 40601</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">(502) 573-3390, (800) 223-2579&nbsp;/ (502) 573-8787</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small"><A href="http://www.kfi.ky.gov/">http://www.kfi.ky.gov/</A></SPAN></SPAN></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=ky#Type_1">Motrgage Loan Broker</A><BR><A href="stateguide.asp?state=ky#Type_2">Mortgage Loan Company</A><BR><A href="stateguide.asp?state=ky#Type_3">Mortgage Loan Originator</A><BR><A href="stateguide.asp?state=ky#Type_4">Loan Processor</A> </STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Broker<A name=Type_1></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">Updating Information (In Progress)</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD width=360>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV>&nbsp;</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Company<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Originator<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">$119 (includes NMLS processing fee and Federal Criminal Background Check)</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">20 Hours NMLS Approved Pre-Licensing Education</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and KY Test Components</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">12 Hours NMLS Approved CE, Annually (Including 4 hrs. KY Specific Content (every 2 years))</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">$50,000</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></DIV></TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Loan Processor<A name=Type_4></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">$119 (includes NMLS processing fee and Federal Criminal Background Check)</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">20 Hours NMLS Approved Pre-Licensing Education</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and KY Test Components</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">12 Hours NMLS Approved CE, Annually (Including 4 hrs. KY Specific Content (every 2 years))</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">&nbsp;By December 31st</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Covered by Employer''s Surety Bond</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">&nbsp; By December 31st</DIV></TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG> 5/12/2011</SPAN></P>'
WHERE StateAbbrev = 'KY'
UPDATE StateRequirements SET RequirementsText =
'<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: large">Nevada Requirements</SPAN></P>
<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375><SPAN><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Nevada Department of Business and Industry:<BR>Nevada Division of Mortgage Lending</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Las Vegas:<BR>7220 Bermuda Road, Suite A<BR>Las Vegas, NV&nbsp; 89119<BR><BR>Carson City:<BR>400 West King Street, Suite 101<BR>Casrson City, NV&nbsp; 89703</SPAN></SPAN></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Las Vegas: (702) 486-0780 / (702) 486-0785</SPAN></SPAN></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="FONT-SIZE: x-small">Carson City: (775) 684-7060 / (775) 684-7061 </SPAN></SPAN></P></TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><A href="http://www.mld.nv.gov/">http://www.mld.nv.gov/</A><A href="http://www.mld.nv.gov/index.htm"></A></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=nv#Type_1">Mortgage Banker</A><BR><A href="stateguide.asp?state=nv#Type_2">Mortgage Broker</A><BR><A href="stateguide.asp?state=nv#Type_3">Mortgage Agents</A><BR></STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Banker<A name=Type_1></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">Updating Information (In Progress)</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD width=360>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Broker<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Agents<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">$240 (includes NMLS processing fee)</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">30 Hours NMLS Approved Pre-Licensing Education and Sponsorship by a Licensed NV Company</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and NV Test Components</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">10 Hours NMLS Approved CE, Annually (Including 3 hrs. NV Specific Content)</DIV></DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">$50,000 or $75,000 (Depending on Loan Volume)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></DIV></TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG>&nbsp;5/12/2011</SPAN></P>'
WHERE StateAbbrev = 'NV'
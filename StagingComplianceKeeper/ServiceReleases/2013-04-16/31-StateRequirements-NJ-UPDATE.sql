UPDATE StateRequirements SET RequirementsText =
'<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: large">New Jersey Requirements</SPAN></P>
<TABLE border=0 cellSpacing=10 cellPadding=0 width=536>
<TBODY>
<TR>
<TD width=131><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Regulated By:</SPAN></STRONG></SPAN></TD>
<TD width=375>New Jersey Department of Banking and Insurance</TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Address:</SPAN></STRONG></SPAN></TD>
<TD>Licensing Services,&nbsp;P.O. Box 040<BR>Trenton, NJ 08625</TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Phone/Fax Numbers:</SPAN></STRONG></SPAN></TD>
<TD>(609) 292-5360 / (609) 292-5461</TD></TR>
<TR>
<TD><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><SPAN style="FONT-SIZE: x-small">Website Address:</SPAN></STRONG></SPAN></TD>
<TD><A href="http://www.state.nj.us/dobi/index.html">http://www.state.nj.us/dobi/index.html</A><A href="http://www.state.nj.us/dobi/division_banking/index.htm"></A></TD></TR></TBODY></TABLE>
<HR>

<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Please Note: The contents contained on this web page are for informational purposes only. State requirements often change. Please visit your state web site for the most up-to-date information. This information should not be interpreted as legal advice.</SPAN><BR><BR><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small">Please Select Which Requirements You Would Like To See</SPAN></STRONG></P>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><STRONG><A href="stateguide.asp?state=nj#Type_1">Individual Residential Mortgage Lender License</A><BR><A href="stateguide.asp?state=nj#Type_2">Individual Correspondent Residential Mortgage Lender License</A><BR><A href="stateguide.asp?state=nj#Type_3">Individual Residential Mortgage Broker License</A><BR><A href="stateguide.asp?state=nj#Type_4">Mortgage Loan Originator License</A> </STRONG></SPAN></P>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Individual Residential Mortgage Lender License<A name=Type_1></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TD style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top><SPAN style="FONT-WEIGHT: bold">&nbsp;</SPAN><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></TD>
<TD style="TEXT-ALIGN: center">Updating Information (In Progress)</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD style="TEXT-ALIGN: center" width=360>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Individual Correspondent Residental Mortgage Lender License<A name=Type_2></A></SPAN></SPAN></STRONG></P>
<TABLE border=1 cellSpacing=0 cellPadding=0 width=536>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth</SPAN></STRONG><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Individual Residential Mortgage Broker License<A name=Type_3></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">Updating Information (In Progress)</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD style="TEXT-ALIGN: center">&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>&nbsp;</TD></TR></TBODY></TABLE>
<P><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: small"><SPAN style="TEXT-DECORATION: underline">Mortgage Loan Originator License <A name=Type_4></A></SPAN></SPAN></STRONG></P>
<TABLE style="WIDTH: 536px" border=1 cellSpacing=0 cellPadding=0>
<TBODY>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0; WIDTH: 170px" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Type:</SPAN></STRONG></DIV></TH>
<TD width=360>
<DIV style="TEXT-ALIGN: center">License</DIV></TD></TR>
<TR>
<TH style="TEXT-ALIGN: center; BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Application Fee:</SPAN></STRONG></TH>
<TD>
<DIV style="TEXT-ALIGN: center">$180 (includes NMLS processing fee)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Initial Education <BR>and/or Experience:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">20 Hours NMLS Approved Pre-Licensing Education (Including 4 hrs. NJ Specific Content) and Sponsorship by a Licensed NJ Company</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Testing:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">A Score of 75% on National and NJ Test Components</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Requirement:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">12 Hours NMLS Approved CE, Annually (Including 2 hrs. NJ Specific Content)</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">CE Deadline:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">By December 31st</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Surety Bond:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Covered by Employer''s Surety Bond</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Finger Prints /<BR>Criminal Background:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes/Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Net Worth:</SPAN></STRONG></DIV></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">N/A</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Credit Report:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">Yes</DIV></TD></TR>
<TR>
<TH style="BACKGROUND-COLOR: #c0c0c0" vAlign=top scope=row>
<DIV><STRONG><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; FONT-SIZE: x-small">Renewals:</SPAN></STRONG></DIV></TH>
<TD>
<DIV style="TEXT-ALIGN: center">By December 31st (Late Renewals: January 1st - Last Day of February)</DIV></TD></TR></TBODY></TABLE>
<P><SPAN style="FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; COLOR: #0000cc; FONT-SIZE: x-small"><STRONG>REVISION DATE:</STRONG> 4/15/2011</SPAN></P>'
WHERE StateAbbrev = 'NJ'
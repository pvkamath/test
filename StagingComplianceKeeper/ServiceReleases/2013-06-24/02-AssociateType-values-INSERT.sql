INSERT INTO [AssociateType]
           ([AssociateTypeName]
           ,[OrderBy]
           ,[Active])
     VALUES
           ('Loan Officer'
           ,10
           ,1)

INSERT INTO [AssociateType]
           ([AssociateTypeName]
           ,[OrderBy]
           ,[Active])
     VALUES
           ('Underwriter'
           ,20
           ,1)

INSERT INTO [AssociateType]
           ([AssociateTypeName]
           ,[OrderBy]
           ,[Active])
     VALUES
           ('Processor'
           ,30
           ,1)


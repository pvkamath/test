/*
   Monday, June 24, 20132:21:12 PM
   User: 
   Server: VM-DEV-01
   Database: ComplianceKeeper
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.Associates ADD CONSTRAINT
	FK_Associates_AssociateType FOREIGN KEY
	(
	AssociateTypeID
	) REFERENCES dbo.AssociateType
	(
	AssociateTypeID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.Associates', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Associates', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Associates', 'Object', 'CONTROL') as Contr_Per 
/*
   Monday, July 08, 201311:49:58 AM
   User: 
   Server: VM-DEV-01
   Database: ComplianceKeeper
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Groups
	(
	GroupID int NOT NULL IDENTITY (1, 1),
	GroupName varchar(50) NOT NULL,
	AssociateID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Groups ADD CONSTRAINT
	PK_Groups PRIMARY KEY CLUSTERED 
	(
	GroupID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.Groups', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.Groups', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.Groups', 'Object', 'CONTROL') as Contr_Per 
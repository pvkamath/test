/*
   Monday, July 15, 20134:14:36 PM
   User: 
   Server: VM-DEV-01\SQL2008
   Database: ComplianceKeeperCorp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.GroupsEmployees
	(
	Id int NOT NULL IDENTITY (1, 1),
	GroupID int NOT NULL,
	EmployeeID int NOT NULL,
	Deleted bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.GroupsEmployees ADD CONSTRAINT
	DF_GroupsEmployees_Deleted DEFAULT 0 FOR Deleted
GO
ALTER TABLE dbo.GroupsEmployees ADD CONSTRAINT
	PK_GroupsEmployees PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.GroupsEmployees SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.GroupsEmployees', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.GroupsEmployees', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.GroupsEmployees', 'Object', 'CONTROL') as Contr_Per 
DECLARE @PageID INT

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,20,'/reports/groupreport.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,20,'/reports/groupreportpdf.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,20,'/reports/groupreportexcel.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

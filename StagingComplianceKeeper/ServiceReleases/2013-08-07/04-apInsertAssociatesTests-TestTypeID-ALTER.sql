/****** Object:  StoredProcedure [dbo].[apInsertAssociatesTests]    Script Date: 08/07/2013 14:22:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apInsertAssociatesTests]
(
@p_iUserID INT,
@p_sName VARCHAR(100),
@p_iStateID INT,
@p_dTestDate DATETIME,
@p_iStatusID INT,
@p_iTestTypeID INT
)
AS
SET NOCOUNT ON

INSERT INTO AssociatesTests(UserID, Name, StateID, TestDate, StatusID,AssociateTestTypeID)
VALUES(@p_iUserID, @p_sName, @p_iStateID, @p_dTestDate, @p_iStatusID, @p_iTestTypeID)

SELECT SCOPE_IDENTITY()

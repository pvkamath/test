/****** Object:  StoredProcedure [dbo].[apUpdateAssociatesTests]    Script Date: 08/07/2013 14:22:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apUpdateAssociatesTests]
(
@p_iTestID INT,
@p_sName VARCHAR(100),
@p_iStateID INT,
@p_dTestDate DATETIME,
@p_iStatusID INT,
@p_iTestTypeID INT
)
AS
SET NOCOUNT ON

UPDATE AssociatesTests
SET	[Name] = @p_sName,
	StateID = @p_iStateID, 
	TestDate = @p_dTestDate,
	StatusID = @p_iStatusID,
	AssociateTestTypeID = @p_iTestTypeID
WHERE TestID = @p_iTestID

SELECT @@ROWCOUNT

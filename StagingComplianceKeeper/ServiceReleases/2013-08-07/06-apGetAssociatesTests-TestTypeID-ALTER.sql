/****** Object:  StoredProcedure [dbo].[apGetAssociatesTests]    Script Date: 08/07/2013 15:07:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apGetAssociatesTests]
(
@p_iUserID INT,
@p_iTestID INT = NULL
)
AS
DECLARE @sSQL VARCHAR(500)
SET NOCOUNT ON

SET @sSQL = 'SELECT AT.*, S.Status, ATT.TestTypeName FROM AssociatesTests AS AT
			 INNER JOIN AssociateTestStatuses AS S ON (AT.StatusID = S.StatusID)
			 LEFT JOIN AssociateTestTypes AS ATT ON (AT.AssociateTestTypeID = ATT.AssociateTestTypeID)
			 WHERE UserID = ' + CAST(@p_iUserID AS VARCHAR(10))

IF isnull(@p_iTestID,'') != ''
	SET @sSQL = @sSQL + ' AND TestID = ' + CAST(@p_iTestID AS VARCHAR(10))

SET @sSQL = @sSQL + ' ORDER BY Name'

EXEC(@sSQL) 

/*
   Monday, September 09, 20133:49:43 PM
   User: 
   Server: vm-dev-01\sql2008
   Database: ComplianceKeeperCorp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AssociateType ADD
	OwnerCompanyID int NULL
GO
ALTER TABLE dbo.AssociateType
	DROP COLUMN OrderBy
GO
ALTER TABLE dbo.AssociateType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.AssociateType', 'Object', 'CONTROL') as Contr_Per 
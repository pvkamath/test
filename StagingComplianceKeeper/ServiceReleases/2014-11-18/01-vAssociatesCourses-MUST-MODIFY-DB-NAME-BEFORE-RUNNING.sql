/****** Object:  View [dbo].[vAssociatesCourses]    Script Date: 11/18/2014 11:01:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vAssociatesCourses]
AS
SELECT     ACs.Id, ACs.UserId, Us.Email, Us.Ssn, Cs.OwnerCompanyId AS CompanyId, Cs.Name, ACs.CourseId, Cs.ProviderId, ACs.CompletionDate, 
                      ACs.CourseExpirationDate, NULL AS CertificateExpirationDate, NULL AS UserSpecRenewalDate, NULL AS CertificateReleased, ACs.Completed AS Completed, 
                      ACs.Deleted, NULL AS PurchaseDate, Cs.StateID
FROM         dbo.AssociatesCourses AS ACs LEFT OUTER JOIN
                      dbo.Courses AS Cs ON Cs.CourseId = ACs.CourseId LEFT OUTER JOIN
                      dbo.Associates AS Us ON Us.UserId = ACs.UserId
UNION
-- Will need to change all occurences of TrainingPro in the section below to match the proper database name
SELECT     UCs.ID, UCs.UserID, Us.UserName, Us.SSN, Us.CompanyID, Cs.Name, UCs.CourseID, 0 AS Expr1, UCs.CompletionDate, UCs.CourseExpirationDate, 
                      UCs.CertificateExpirationDate, CKTPUCDs.CkRenewalDate, UCs.CertificateReleased, UCs.Completed, 0 AS Expr2, UCs.PurchaseDate, Cs.StateID
FROM         TrainingPro.dbo.UsersCourses AS UCs LEFT OUTER JOIN
                      TrainingPro.dbo.Courses AS Cs ON Cs.CourseID = UCs.CourseID LEFT OUTER JOIN
                      TrainingPro.dbo.Users AS Us ON Us.UserID = UCs.UserID LEFT OUTER JOIN
                      dbo.TPUsersCoursesDates AS CKTPUCDs ON CKTPUCDs.UserCourseId = UCs.ID
WHERE     (NOT (UCs.CompletionDate IS NULL)) AND (UCs.CertificateReleased = 1)

GO



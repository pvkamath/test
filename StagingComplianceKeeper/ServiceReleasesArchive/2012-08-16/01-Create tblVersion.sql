

/****** Object:  Table [dbo].[tblVersion]    Script Date: 08/16/2012 11:46:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblVersion](
	[VersionID] [int] IDENTITY(1,1) NOT NULL,
	[VersionDate] [datetime] NULL,
	[DateOriginallyApplied] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_tblVersion] PRIMARY KEY CLUSTERED 
(
	[VersionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



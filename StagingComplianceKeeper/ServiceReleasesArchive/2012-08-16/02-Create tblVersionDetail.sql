

/****** Object:  Table [dbo].[tblVersionDetail]    Script Date: 08/16/2012 11:47:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblVersionDetail](
	[VersionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[VersionID] [int] NOT NULL,
	[isCode] [bit] NOT NULL,
	[FileName] [varchar](512) NULL,
	[Success] [bit] NOT NULL,
	[StatusMessage] [text] NULL,
	[DateApplied] [datetime] NULL,
 CONSTRAINT [PK_tblVersionDetail] PRIMARY KEY CLUSTERED 
(
	[VersionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblVersionDetail] ADD  CONSTRAINT [DF_tblVersionDetail_isCode]  DEFAULT ((0)) FOR [isCode]
GO

ALTER TABLE [dbo].[tblVersionDetail] ADD  CONSTRAINT [DF_tblVersionDetail_Status]  DEFAULT ((0)) FOR [Success]
GO



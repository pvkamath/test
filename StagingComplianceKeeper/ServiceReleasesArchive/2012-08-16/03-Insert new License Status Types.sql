INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (19,'Transition Requested')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (20,'Transition Cancelled')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (21,'Transition Rejected')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (22,'Pending Incomplete')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (23,'Pending Review')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (24,'Pending Deficient')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (25,'Pending - Withdraw Requested')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (26,'Withdrawn - Application Abandoned')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (27,'Withdrawn - Voluntary without Licensure')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (28,'Denied')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (29,'Denied - On Appeal')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (30,'Approved - Conditional')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (31,'Approved - Deficient')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (32,'Approved - Failed to Renew')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (33,'Approved - Inactive')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (34,'Approved - On Appeal')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (35,'Approved - Surrender/Cancellation Requested')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (36,'Revoked - On Appeal')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (37,'Suspended on Appeal')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (38,'Temporary Cease and Desist')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (39,'Terminated - Expired')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (40,'Terminated - Failed to Renew')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (41,'Terminated - Ordered to Surrender')
GO
INSERT INTO [LicenseStatusTypes] ([LicenseStatusID],[LicenseStatus]) VALUES (42,'Terminated - Surrendered/Cancelled')
GO
--Revoked
--Suspended



/*
   Monday, August 27, 20125:08:51 PM
   User: 
   Server: VM-DEV-01\SQL2008
   Database: ComplianceKeeperCorp
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.LicenseStatusTypes ADD
	LicenseStatusAutoUpdate bit NOT NULL CONSTRAINT DF_LicenseStatusTypes_LicenseStatusAutoUpdate DEFAULT 0
GO
ALTER TABLE dbo.LicenseStatusTypes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.LicenseStatusTypes', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.LicenseStatusTypes', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.LicenseStatusTypes', 'Object', 'CONTROL') as Contr_Per 
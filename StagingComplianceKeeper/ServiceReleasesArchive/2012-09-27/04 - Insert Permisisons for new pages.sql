

-- Create security for new pages.
-- Completed 2012-09-27

DECLARE @PageID INT
--Deactivate Officers
SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/deactivateofficers.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/deactivateofficersproc.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)


--Officer List
SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficerlist_v2.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficerlistprocV2.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

-- Officer License
SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficerlicense.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficerlicenseproc.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

-- Officer Course
SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficercourse.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,18,'/officers/addofficercourseproc.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

-- BRANCH
SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,20,'/branches/addBranches.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

SET @PageID = (SELECT TOP 1 [PageID]+1 FROM [afxSecurityPages] ORDER BY Pageid DESC)
INSERT INTO [afxSecurityPages] ([PageID],[PageDirectoryID],[PageName])VALUES (@PageID,20,'/branches/addBranchesproc.asp')
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,2)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,3)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,5)
INSERT INTO [afxSecurityPagesRolesX] ([PageID],[RoleID]) VALUES (@PageID,10)

INSERT INTO [States]
           ([StateID]
           ,[State]
           ,[Abbrev]
           ,[Active])
     VALUES
           (54
           ,'Northern Mariana Islands'
           ,'MP'
           ,NULL)

INSERT INTO [States]
           ([StateID]
           ,[State]
           ,[Abbrev]
           ,[Active])
     VALUES
           (55
           ,'American Samoa'
           ,'AS'
           ,NULL)

INSERT INTO [States]
           ([StateID]
           ,[State]
           ,[Abbrev]
           ,[Active])
     VALUES
           (56
           ,'Guam'
           ,'GU'
           ,NULL)


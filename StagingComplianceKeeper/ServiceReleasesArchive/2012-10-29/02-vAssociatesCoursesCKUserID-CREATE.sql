/****** Object:  View [dbo].[vAssociatesCoursesCKUserID]    Script Date: 10/29/2012 12:31:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vAssociatesCoursesCKUserID]
AS
SELECT     ACs.Id, ACs.UserId, Us.Email, Us.Ssn, Cs.OwnerCompanyId AS CompanyId, Cs.Name, ACs.CourseId, Cs.ProviderId, ACs.CompletionDate, ACs.CourseExpirationDate, 
                      ACs.CourseExpirationDate AS CertificateExpirationDate, NULL AS UserSpecRenewalDate, NULL AS CertificateReleased, ACs.Completed, ACs.Deleted, NULL 
                      AS PurchaseDate
FROM         dbo.AssociatesCourses AS ACs LEFT OUTER JOIN
                      dbo.Courses AS Cs ON Cs.CourseId = ACs.CourseId LEFT OUTER JOIN
                      dbo.Associates AS Us ON Us.UserId = ACs.UserId
UNION
SELECT     UCs.ID, CKUs.UserId, Us.UserName, Us.SSN, Us.CompanyID, Cs.Name, UCs.CourseID, 0 AS Expr2, UCs.CompletionDate, UCs.CourseExpirationDate, 
                      Cs.CkRenewalDate, CKTPUCDs.CkRenewalDate AS Expr1, UCs.CertificateReleased, UCs.Completed, 0 AS Expr3, UCs.PurchaseDate
FROM         TrainingPro2.dbo.UsersCourses AS UCs LEFT OUTER JOIN
                      TrainingPro2.dbo.Courses AS Cs ON Cs.CourseID = UCs.CourseID LEFT OUTER JOIN
                      TrainingPro2.dbo.Users AS Us ON Us.UserID = UCs.UserID LEFT OUTER JOIN
                      dbo.Associates AS CKUs ON ((CKUs.NMLSNumber IS NOT NULL AND CKUs.NMLSNumber = Us.NMLSNumber) OR (CKUs.NMLSNumber IS NULL AND CKUs.Email LIKE Us.UserName)) AND CKUs.UserStatus = '1' LEFT OUTER JOIN
                      dbo.TPUsersCoursesDates AS CKTPUCDs ON CKTPUCDs.UserCourseId = UCs.ID
WHERE     (UCs.CourseStatus = 1)

GO


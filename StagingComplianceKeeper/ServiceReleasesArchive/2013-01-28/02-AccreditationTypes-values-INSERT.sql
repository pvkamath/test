INSERT INTO [AccreditationTypes] ([ID],[Type]) VALUES (1,'Continuing Education')
INSERT INTO [AccreditationTypes] ([ID],[Type]) VALUES (2,'Professional Development')
INSERT INTO [AccreditationTypes] ([ID],[Type]) VALUES (3,'Pre-Licensing')
INSERT INTO [AccreditationTypes] ([ID],[Type]) VALUES (4,'Late Continuing Education')
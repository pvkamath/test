ALTER VIEW [dbo].[vCourses]
AS
SELECT     CourseId, Name, Number, ProviderId, StateId, TypeId, OwnerCompanyId, ContEdHours, ExpirationDate, StateApproved, NonApproved, Deleted, InstructorName, 
                      InstructorEducationLevel, AccreditationTypeID
FROM         dbo.Courses




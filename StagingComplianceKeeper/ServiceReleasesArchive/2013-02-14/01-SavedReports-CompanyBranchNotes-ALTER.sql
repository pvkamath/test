ALTER TABLE SavedReports ADD ShowCompanyNotes bit NULL
ALTER TABLE SavedReports ADD ShowDivisionNotes bit NULL
ALTER TABLE SavedReports ADD ShowRegionNotes bit NULL
ALTER TABLE SavedReports ADD ShowBranchNotes bit NULL
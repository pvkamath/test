<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - State Requirements"

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/StateRequirements.asp?State=" & getFormElement("State"))
	end if	
	
	if getFormElement("State") <> "" then
		sKeywords = "req" & getFormElement("State")
	else
		sKeywords = "StateRequirements"
	end if
	
	'sKeywords = getFormElement("keywords")
	
	iPage = 1

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

setGlobalContentData(sKeywords)

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>											
											<td><span class="pagetitle"><% = g_sHeadline%></span>
												
												
												<p><% = g_sText%>																								
											</td>								
										</tr>
<% if getFormElement("State") = "" then %>
										<tr>
											<td valign="top">	
											<form name="StateRequirements" method="POST" action="StateRequirements.asp">																									
												<table border=0>											
													<tr>
														<td valign="top"><% call DisplayStatesDropDown(0,2,0) %></td>
														<td valign="top">&nbsp;&nbsp;<input type="image" src="<% = application("sDynMediaPath") %>bttnContinue.gif"></td>
													</tr>
												</table>		
												<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"  codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="472" height="382" id="map-v3" align="middle" VIEWASTEXT>
												<param name="allowScriptAccess" value="sameDomain" />
												<param name="movie" value="map-v3.swf" />
												<param name="quality" value="high" />
												<param name="bgcolor" value="#cccccc" />
												<embed src="map-v3.swf" quality="high" bgcolor="#F4F1E9" width="472" height="382" name="map-v3" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
												</object>																															
											</form>													
											</td>
										</tr>
<% else %>
										<tr>
											<td align="center" colspan="2">
												<a href="javascript:history.back()"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" border=0></a>
											</td>
										</tr>		 
<% end if %>										
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

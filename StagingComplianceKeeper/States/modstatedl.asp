<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->

<!-- #include virtual = "/includes/Course.Class.asp" ------------------------->
<!-- #include virtual = "/includes/State.Class.asp" -------------------------->
<!-- #include virtual = "/includes/StateDeadline.Class.asp" ------------------>
<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

	
'Configure the administration submenu options
bAdminSubmenu = false
'sAdminSubmenuType = "BROKERS"

	
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
function ProviderChange()
{
	document.DeadlineForm.CourseId.value = '';	
									
	RefreshScreen();
}

function RefreshScreen()
{
	document.DeadlineForm.ScreenRefresh.value = 1;
	document.DeadlineForm.action = "modstatedl.asp";
	document.DeadlineForm.submit();	
}
		
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------
%>



<%

dim sMode
dim iDeadline

'Load the passed StateDeadline information
dim oDeadline
set oDeadline = new StateDeadline
oDeadline.VocalErrors = application("bVocalErrors")
oDeadline.ConnectionString = application("sDataSourceName")
oDeadline.TpConnectionString = application("sTpDataSourceName")

iDeadline = ScrubForSql(request("id"))

if iDeadline <> "" then
	
	if oDeadline.LoadDeadlineById(iDeadline) = 0 then
	
		HandleError("Could not load the requested Deadline.  Please try again.")
	
	else
	
		sMode = "Edit"
	
	end if
	
else

	sMode = "Add"

end if 


%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- State Guide -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="States" align="absmiddle" vspace="10"> State Requirement Profile</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
								

<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Edit a State Expiration Deadline</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<%
dim oCourse
dim iProviderId
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = false 'application("bVocalErrors")

if sMode = "Edit" and oDeadline.CourseId <> "" then
	if oCourse.LoadCourseById(oDeadline.CourseId) <> 0 then
		iProviderId = oCourse.ProviderId
	end if
end if


%>

<form name="DeadlineForm" action="modstatedlproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="id" value="<% = iDeadline %>">
<input type="hidden" name="ScreenRefresh" value="">
									<table border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>Expiration Type: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td align="left" valign="top">
												<input type="radio" name="statewide" value="1"<% if oDeadline.Statewide then %> CHECKED<% end if %>> Statewide<br>
												<input type="radio" name="statewide" value="0"<% if not oDeadline.Statewide then %> CHECKED<% end if %>> Course Specific
											</td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Provider: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td align="left" valign="top">
												<% 
												if request("ScreenRefresh") <> "1" then 
													DisplayProvidersDropdown oCourse.ProviderId, session("UserCompanyId"), 0, 3, 0, "onChange=ProviderChange();"
												else
													DisplayProvidersDropdown ScrubForSql(request("Provider")), session("UserCompanyId"), 0, 1, 0, "onChange=ProviderChange();"
												end if 
												%>
											</td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Course: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td align="left" valign="top">
												<%
												if request("Provider") = "" then
													DisplayProviderCoursesDropdown oCourse.ProviderId, oDeadline.CourseId, 1, 1
												elseif request("ScreenRefresh") <> "1" then
													DisplayProviderCoursesDropdown oCourse.ProviderId, oDeadline.CourseId, 0, 1
												else
													DisplayProviderCoursesDropdown ScrubForSql(request("Provider")), oDeadline.CourseId, 0, 1
												end if 
												%>
											</td>
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% call DisplayStatesDropDown(oDeadline.StateId, 1, "DeadlineStateId") 'functions.asp %></td>
										</tr>
										<tr>
											<td class="newstitle" nowrap>Expire Date: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
												<input type="text" name="ExpireDate" value="<% = oDeadline.ExpDate %>" size="10" maxlength="10">
												<a href="javascript:show_calendar('DeadlineForm.ExpireDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('DeadlineForm.ExpireDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>
											</td>
										</tr>			
	<tr>
		<td colspan="2"></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>
								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
'set saRS = nothing
set oCourse = nothing
set oDeadline = nothing

'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
<!-- #include virtual = "/includes/misc.asp" --------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------->
<!-- #include virtual = "/includes/shell.asp" -------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---------------------->
<!-- #include virtual="/includes/State.Class.asp" ---------------------------->
<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	
'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()

dim bCheckIsAdmin 'Is this an administrator?
if CheckIsAdmin then
	bCheckIsAdmin = true
end if


'Load the passed State Requirements
dim oState
dim iStateId
dim sStateAbbrev
sStateAbbrev = ScrubForSql(request("state"))
set oState = new State
oState.ConnectionString = application("sDataSourceName")
oState.VocalErrors = false 'application("bVocalErrors")
oState.StateAbbrev = sStateAbbrev

if oState.LoadStateByAbbrev(sStateAbbrev) = 0 then

	iStateId = GetStateIdByAbbrev(sStateAbbrev)
	oState.StateName = GetStateName(iStateId, 0)
	oState.StateAbbrev = GetStateName(iStateId, 1)
	
end if 



'Configure the administration submenu options
bAdminSubmenu = false
'sAdminSubmenuType = "BROKERS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<% if bCheckIsAdmin then %>
<script language="JavaScript">
function ConfirmDocumentDelete(p_iDocId)
{
	if (confirm("Are you sure you wish to delete this Document?  All information in the record will be deleted."))
		window.location.href = '<% = application("URL") %>/officers/deletedocument.asp?docid=' + p_iDocId;
}
</script>
<% end if %>


			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- State Guide -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="States" align="absmiddle" vspace="10"> State Requirement Profile</td>
				</tr>	

						<% if CheckIsAdmin() and 1 = 2 then %>
						<tr>
							<td class="bckWhiteBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
										<td align="left" valign="center"><a href="modstate.asp?state=<% = sStateAbbrev %>" class="nav"><img src="<% = application("sDynMediaPath") %>bttnEdit.gif" align="center" border="0" title="Edit State Information" alt="Edit State Information"> &nbsp;Edit State Record</a></td>
									</tr>
								</table>
							</td>
						</tr>
						<% end if %>
									
							<tr>
								<td class="bckRight">
									<table cellpadding="10" cellspacing="0" border="0" width="100%">
										<tr>
											<td valign="top" width="50%">
									<table border="0" width="75%" cellpadding="5" cellspacing="0">
										<tr>
											<td class="newstitle" nowrap>State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.StateName %> (<% = oState.StateAbbrev %>)</td>
										</tr>
										<tr>
											<td colspan="3"><% = oState.RequirementsText %></td>
										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
										</tr>
										<%
										dim oAssociateDoc
										set oAssociateDoc = new AssociateDoc
										oAssociateDoc.ConnectionString = application("sDataSourceName")
										oAssociateDoc.VocalErrors = application("bVocalErrors")
										oAssociateDoc.CompanyId = session("UserCompanyId")
										oAssociateDoc.OwnerId = oState.StateId
										oAssociateDoc.OwnerTypeId = 6
										set oRs = oAssociateDoc.SearchDocs
										
										'if not (oRs.BOF and oRs.EOF) then
										%>
										<tr>
											<td valign="top" class="newstitle" nowrap>Documents: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td>
											<%
											do while not oRs.EOF
											
												if oAssociateDoc.LoadDocById(oRs("DocId")) <> 0 then
												
													%>
													<% if bCheckIsAdmin then %>
													<a href="javascript:ConfirmDocumentDelete(<% = oAssociateDoc.DocId %>)"><img src="<% = application("sDynMediaPath") %>bttnDelete.gif" border="0" title="Delete This Document" alt="Delete This Document"></a>&nbsp;&nbsp;
													<% end if %>
													<a href="/officers/getdocumentproc.asp?docid=<% = oAssociateDoc.DocId %>"><% = oAssociateDoc.DocName %></a><br>
													<%
												
												end if
												
												oRs.MoveNext
												
											loop
											%>
											<p>
											<% if bCheckIsAdmin then %>
											<a href="/officers/moddocument.asp?otid=6&oid=<% = oState.StateId %>"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> Add New Document</a>
											<% end if %>
											</td>
										</tr>
										
										<% if 1 = 2 then %>
										<% if oState.RegulatedBy <> "" then %>
										<tr>
											<td class="newstitle" valign="top" nowrap>Regulated By: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.RegulatedBy %></td>
										</tr>
										<% end if %>
										<% if oState.CeRequirements <> "" then %>
										<tr>
											<td class="newstitle" valign="top" nowrap>CE Requirements: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.CeRequirements %></td>
										</tr>
										<% end if %>
										<% if oState.Licenses <> "" then %>
										<tr>
											<td class="newstitle" valign="top" nowrap>Licenses: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.Licenses %></td>
										</tr>
										<% end if %>
										<% if oState.LicenseRequirements <> "" then %>
										<tr>
											<td class="newstitle" valign="top" nowrap>License Requirements: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.LicenseRequirements %></td>
										</tr>
										<% end if %>
										<% if oState.Fingerprints <> "" then %>
										<tr>
											<td class="newstitle" nowrap>Fingerprints: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.Fingerprints %></td>
										</tr>
										<% end if %>
										<% if oState.Renewals <> "" then %>
										<tr>
											<td class="newstitle" valign="top" nowrap>Renewals: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.Renewals %></td>
										</tr>
										<% end if %>
										<% if oState.Website <> "" then %>
										<tr>
											<td class="newstitle" nowrap>Website: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><a href="<% = oState.Website %>"><% = oState.Website %></a></td>
										</tr>
										<% end if %>
										<% if oState.MailingAddress <> "" or oState.MailingCity <> "" or oState.MailingZipcode <> "" or oState.MailingZipcode <> "" then %>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3"><b>Mailing Address</b></td>
										</tr>
										<% if oState.MailingAddress <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.MailingAddress %></td>
										</tr>
										<% if not oState.MailingAddress2 = "" then %>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.MailingAddress2 %></td>
										</tr>
										<% end if %>
										<% end if %>
										<% if oState.MailingCity <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.MailingCity %></td>
										</tr>
										<% end if %>
										<% if oState.MailingStateId <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = GetStateName(oState.MailingStateId, 1) %></td>
										</tr>
										<% end if %>
										<% if oState.MailingZipcode <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.MailingZipcode %><% if oState.MailingZipcodeExt <> "" then %> - <% = oState.MailingZipcodeExt %><% end if %></td>
										</tr>
										<% end if %>
										<% end if %>
										<% if oState.ContactName <> "" or oState.ContactPhone <> "" or oState.ContactFax <> "" or oState.ContactEmail <> "" or oState.ContactAddress <> "" then %>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="3"><b>State Contact</b></td>
										</tr>
										<% if oState.ContactName <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Name: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactName %></td>
										</tr>
										<% end if %>
										<% if oState.ContactPhone <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactPhone %> <% if oState.ContactPhoneExt <> "" then %> Ext. <% = oState.ContactPhoneExt %><% end if %></td>
										</tr>
										<% end if %>
										<% if oState.ContactPhone2 <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactPhone2 %> <% if oState.ContactPhone2Ext <> "" then %> Ext. <% = oState.ContactPhone2Ext %><% end if %></td>
										</tr>
										<% end if %>
										<% if oState.ContactFax <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fax: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactFax %></td>
										</tr>
										<% end if %>
										<% if oState.ContactEmail <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactEmail %></td>
										</tr>
										<% end if %>
										<% if oState.ContactEmail2 <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alternate Email: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactEmail2 %></td>
										</tr>
										<% end if %>
										<% if oState.ContactAddress <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Street: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactAddress %></td>
										</tr>
										<% if not oState.ContactAddress2 = "" then %>
										<tr>
											<td class="newstitle" nowrap></td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactAddress2 %></td>
										</tr>
										<% end if %>
										<% end if %>
										<% if oState.ContactCity <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;City: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactCity %></td>
										</tr>
										<% end if %>
										<% if oState.ContactStateId <> "" then %> 
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;State: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = GetStateName(oState.ContactStateId, 1) %></td>
										</tr>
										<% end if %>
										<% if oState.ContactZipcode <> "" then %>
										<tr>
											<td class="newstitle" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Zip: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><% = oState.ContactZipcode %><% if oState.ContactZipcodeExt <> "" then %> - <% = oState.ContactZipcodeExt %><% end if %></td>
										</tr>
										<% end if %>
										<% end if %>
										<% end if %>
									</table>
										</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<% if oState.LastEdit <> "" then %>
						This record was last updated on <% = formatdatetime(oState.LastEdit, 2) %>
						<% end if %>
						
							
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

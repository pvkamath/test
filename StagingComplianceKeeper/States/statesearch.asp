<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/State.Class.asp" ---------------------------->
<!-- #include virtual="/includes/StateDeadline.Class.asp" -------------------->
<!-- #include virtual="/includes/StateUpdate.Class.asp" ---------------------->


<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	

'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Get the content items for the page.
sKeywords = "statenotice"
setGlobalContentData(sKeywords)

	
'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 



	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
						<!-- State Guide -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="States" align="absmiddle" vspace="10"> State Requirements</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
													<% if g_sText <> "" then %>
													<tr>
														<td>
															<b><% = g_sHeadline %></b><br>
															<% = g_sText %>
														</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
													<% end if %>
												</table>
												<table width="100%" border=0 cellpadding=4 cellspacing=0>
													<tr>
														<td align="center">
			<a href="stateguide.asp?state=al">Alabama (AL)</a><br>
			<a href="stateguide.asp?state=ak">Alaska (AK)</a><br>
			<a href="stateguide.asp?state=az">Arizona (AZ)</a><br>
			<a href="stateguide.asp?state=ar">Arkansas (AR)</a><br>
			<a href="stateguide.asp?state=ca">California (CA)</a><br>
			<a href="stateguide.asp?state=co">Colorado (CO)</a><br>
			<a href="stateguide.asp?state=ct">Connecticut (CT)</a><br>
			<a href="stateguide.asp?state=de">Delaware (DE)</a><br>
			<a href="stateguide.asp?state=dc">Washington D.C.</a><br>
			<a href="stateguide.asp?state=fl">Florida (FL)</a><br>
			<a href="stateguide.asp?state=ga">Georgia (GA)</a><br>
			<a href="stateguide.asp?state=hi">Hawaii (HI)</a><br>
			<a href="stateguide.asp?state=id">Idaho (ID)</a><br>
			<a href="stateguide.asp?state=il">Illinois (IL)</a><br>
			<a href="stateguide.asp?state=in">Indiana (IN)</a><br>
			<a href="stateguide.asp?state=ia">Iowa (IA)</a><br>
			<a href="stateguide.asp?state=ks">Kansas (KS)</a><br>
														</td>
														<td align="center">
			<a href="stateguide.asp?state=ky">Kentucky (KY)</a><br>
			<a href="stateguide.asp?state=la">Louisiana (LA)</a><br>
			<a href="stateguide.asp?state=me">Maine (ME)</a><br>
			<a href="stateguide.asp?state=md">Maryland (MD)</a><br>
			<a href="stateguide.asp?state=ma">Massachusetts (MA)</a><br>
			<a href="stateguide.asp?state=mi">Michigan (MI)</a><br>
			<a href="stateguide.asp?state=mn">Minnesota (MN)</a><br>
			<a href="stateguide.asp?state=ms">Mississippi (MS)</a><br>
			<a href="stateguide.asp?state=mo">Missouri (MO)</a><br>
			<a href="stateguide.asp?state=mt">Montana (MT)</a><br>
			<a href="stateguide.asp?state=ne">Nebraska (NE)</a><br>
			<a href="stateguide.asp?state=nv">Nevada (NV)</a><br>
			<a href="stateguide.asp?state=nh">New Hampshire (NH)</a><br>
			<a href="stateguide.asp?state=nj">New Jersey (NJ)</a><br>
			<a href="stateguide.asp?state=nm">New Mexico (NM)</a><br>
			<a href="stateguide.asp?state=ny">New York (NY)</a><br>
			<a href="stateguide.asp?state=nc">North Carolina (NC)</a><br>
														</td>
														<td align="center">
			<a href="stateguide.asp?state=nd">North Dakota (ND)</a><br>
			<a href="stateguide.asp?state=oh">Ohio (OH)</a><br>
			<a href="stateguide.asp?state=ok">Oklahoma OK</a><br>
			<a href="stateguide.asp?state=or">Oregon (OR)</a><br>
			<a href="stateguide.asp?state=pa">Pennsylvania (PA)</a><br>
			<a href="stateguide.asp?state=pr">Puerto Rico (PR)</a><br>
			<a href="stateguide.asp?state=ri">Rhode Island (RI)</a><br>
			<a href="stateguide.asp?state=sc">South Carolina (SC)</a><br>
			<a href="stateguide.asp?state=sd">South Dakota (SD)</a><br>
			<a href="stateguide.asp?state=tn">Tennessee (TN)</a><br>
			<a href="stateguide.asp?state=tx">Texas (TX)</a><br>
			<a href="stateguide.asp?state=ut">Utah (UT)</a><br>
			<a href="stateguide.asp?state=vt">Vermont (VT)</a><br>
			<a href="stateguide.asp?state=va">Virginia (VA)</a><br>
			<a href="stateguide.asp?state=wa">Washington (WA)</a><br>
			<a href="stateguide.asp?state=wv">West Virginia (WV)</a><br>
			<a href="stateguide.asp?state=wi">Wisconsin (WI)</a><br>
			<a href="stateguide.asp?state=wy">Wyoming (WY)</a><br>
														</td>
													</tr>
													</table>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>


						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>



			<!-- State Requirements Updates -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-courses.gif" width="17" alt="States" align="absmiddle" vspace="10"> State Requirements Updates</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">
						<table cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">
						<table border="0" cellpadding="10" cellspacing="0" width="100%">
							<tr>
								<td>
									<table border="0" cellpadding="5" cellspacing="0" width="100%">						
<%
'dim oRs
dim oUpdate

set oUpdate = new StateUpdate
oUpdate.ConnectionString = application("sDataSourceName")
oUpdate.VocalErrors = application("bVocalErrors")
oUpdate.SearchDateStart = now() - 60
oUpdate.SearchDateEnd = now()

set oRs = oUpdate.SearchUpdates()

sColor = ""

if oRs.State <> 0 then

if not oRs.EOF then

%>
										<tr>
											<td nowrap><b>State Record</b></td>
											<td nowrap><b>Date of Update</b></td>
											<td><b>Notice</b></td>
										</tr>
<%

	do while not oRs.EOF
	
		if oUpdate.LoadUpdateById(oRs("UpdateId")) <> 0 then
		
			if sColor = "" then
				sColor = " bgcolor=""#ffffff"""
			else
				sColor = ""
			end if
			
			Response.Write("<tr>" & vbCrLf)
			Response.Write("<td " & sColor & " valign=""top"" nowrap>" & oUpdate.LookupState(oUpdate.StateId) & "</td>")
			Response.Write("<td " & sColor & " valign=""top"" nowrap>" & oUpdate.UpdateDate & "</td>")
			Response.Write("<td " & sColor & ">" & oUpdate.Text & "</td>")
			Response.Write("</tr>" & vbCrLf)
			
		end if
		
		oRs.MoveNext
		
	loop
	
else

%>
										<tr>
											<td>No state requirements updates currently posted.</td>
										</tr>
<%
	
end if 
end if
%>

									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>




						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

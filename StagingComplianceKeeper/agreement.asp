<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	

	'Verify that the user has logged in.
	CheckIsActiveUser()


	'Get the content items for the page.
	sKeywords = "license"
	setGlobalContentData(sKeywords)



	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Message box -->
			<table class="bckBox" border="0" cellpadding="10" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle" valign="center"><img src="/media/images/spacer.gif" width="1" height="20" alt=""> <% = g_sHeadline %></td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						<% if request("r") = "1" then %>
						<b><font color="#990000">You must accept the User Agreement to login to the site.</font></b><p>
						<% end if %>
						<% = g_sText %>
						<p>
						<form name="AgreementForm" method="POST" action="agreementproc.asp">
						<input type="hidden" name="referrer" value="<%= request("referrer") %>">
						<input type="hidden" name="EventID" value="<%= request("eventid") %>">
						<input type="hidden" name="lid" value="<%= request("lid") %>">	
						<input name="accepted" type="checkbox" value="1"> I accept this agreement.  
						<p>
						<input type="submit" value="Proceed">
						</form>
					</td>
				</tr>
			</table>
			
						<img src="<% = application("sDynMediaPath") %>spacer.gif" width="10" height="20" alt="" border="0"><br>					

			
			<img src="/Media/Images/spacer.gif" width="10" height="50" alt="" border="0">
			
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					</td>
				</tr>
		</table>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------


	'Verify that the user has logged in.
	CheckIsActiveUser()


	dim bAgreed
	dim oUser
	
	if ScrubForSql(request("Accepted")) = "1" then
		bAgreed = true
	else
		bAgreed = false
	end if
	
	set oUser = new User
	oUser.ConnectionString = application("sDataSourceName")
	oUser.UserId = session("User_ID")
	
	if bAgreed then
	
		if oUser.SetAgreementSigned then
	
			session("NeedsAgreement") = false
			Response.Redirect("companyhomeupdate.asp?lid=" & request("lid") & "&EventID=" & request("EventID") & "&referrer=" & request("referrer"))
		
		else
	
			HandleError("Unable to process your agreement.  Please login again.")
	
		end if 
		
	else
	
		Response.Redirect("agreement.asp?r=1&EventID=" & request("EventID") & "&referrer=" & request("referrer"))
		
	end if 
	
%>
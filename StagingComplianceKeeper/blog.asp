<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/CalendarDisplay.Class.asp" ------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Document.Class.asp" ------------------------->
<!-- #include virtual="/includes/StateDeadline.Class.asp" -------------------->
<!-- #include virtual = "/includes/Blog.Class.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
		
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	iPage = 1


'Verify that the user has logged in.
CheckIsLoggedIn()


'Calendar Object initialization
dim oCalendarDisplay
set oCalendarDisplay = new CalendarDisplay
dim sEventType
dim iSearchStateId
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList

'User Access Lists
sCompanyL2IdList = GetUserCompanyL2IdList()
sCompanyL3IdList = GetUserCompanyL3IdList()
sBranchIdList = GetUserBranchIdList()

sEventType = ScrubForSql(request("SearchEventType"))
iSearchStateId = ScrubForSql(request("SearchState"))

oCalendarDisplay.TemporaryRecordsetFile = application("sAbsWebroot") & "includes\TempRecordsetDefinition.rst"
oCalendarDisplay.StartOfBusinessDay = "7:00 AM"
oCalendarDisplay.EndOfBusinessDay = "9:00 PM"
oCalendarDisplay.HighlightReferenceDate = false
oCalendarDisplay.DateLink = application("sDynWebroot") & "calendar/calendar.asp?SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & "&View=day&refdate="
if CheckIsAdmin() then
	oCalendarDisplay.EmptyDateLink = application("sDynWebroot") & "calendar/editevent.asp?editmode=Add&refdate="
else
	oCalendarDisplay.EmptyDateLink = ""
end if
oCalendarDisplay.TimeLink = ""
oCalendarDisplay.ShowEmptyDaysInWeekView = true

'Get search criteria
dim dCalendarStartDateRange, dCalendarEndDateRange, dRefDate, sView
dim dEventStartDate, dEventEndDate, dTmpDate
dim bDisplayEventListing
dim sMode

'Show the monthly view if there is not specified view
sView = request("view")
if sView = "" then
	sView = "month"
end if

'Display the event listing if this is a monthly view
if sView = "month" then
	bDisplayEventListing = true
else
	bDisplayEventListing = false
end if

'If we do not have a specified reference date, use the current one
dRefDate = request("refDate")
if dRefDate = "" then
	dRefDate = Date
end if 

sMode = request("mode")

'Depending on our chosen view mode, set the start and end date accordingly
select case sView
	case "month"
		
		'Set startdate to the beginning of the month, then subract one month from that date
		dCalendarStartDateRange = month(dRefDate) & "/1/" & year(dRefDate)
'		dCalendarStartDateRange = oCalendarDisplay.SubtractOneMonth(dCalendarStartDateRange)
		
		'Set end date to the last day of the month, then add one month to that date
		dCalendarEndDateRange = getLastDayOfMonth(Month(dRefDate), year(dRefDate))
'		dCalendarEndDateRange = oCalendarDisplay.AddOneMonth(dCalendarEndDateRange)
		
	case else
	
		'Assign these the the refdate, which if unspecified is the current date
		dCalendarStartDateRange = dRefDate
		dCalendarEndDateRange = dRefDate
	
end select
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmDocumentDelete(p_iDocId,p_sDocName)
{
	if (confirm("Are you sure you wish to delete the following Document:\n  " + p_sDocName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/documents/deletedocument.asp?did=' + p_iDocId;
}
</script>

<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
<!--- column #3 --->
		<td class="bckRight" width="460" valign="top" align="center">
			<p><br clear="all"><br>
						
			<% Call DisplaySiteBlog() 'located below %>

<!--- center border column --->			
		<td class="border"><img src="/Media/Images/spacer.gif" width="1" height="10" alt="" border="0"></td>
		

		<td class="bckLeft" width="299" valign="top" align="center">
			<img src="/Media/Images/spacer.gif" width="1" height="10" alt="" border="0">
			<br clear="all">
			
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="250">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
<%
'===========================================
'New Caledar code

'Get events that are in the displayed calendar range, active, and have not ended (enddate greater than current date)
sSql = "SELECT E.Event, E.Directions, E.RedirectUrl, EDDX.DateID, EDDX.DisplayStartdate, EDDX.DisplayEnddate FROM afxEvents AS E " & _
	   "INNER JOIN afxEventDisplayDateX AS EDDX ON (EDDX.EventID = E.EventID) " & _
	   "LEFT OUTER JOIN afxEventsEventTypeX EET ON (E.EventId = EET.EventId) " & _
	   "LEFT OUTER JOIN afxEventType ET ON (EET.EventTypeId = ET.EventTypeId) " & _
	   "LEFT OUTER JOIN States AS S ON (E.StateId = S.StateId) " & _
	   "WHERE EDDX.DisplayEndDate >= '" & date & "' AND EDDX.DisplayEnddate >= '" & dCalendarStartDateRange & "' AND EDDX.DisplayStartdate <= '" & dCalendarEndDateRange & " 23:59:59' AND E.Active = 1 AND E.CompanyId = '" & session("UserCompanyId") & "' "

'Restrict the list of events to only the entities to which the user has access.
'Division Clause
sSql = sSql & "AND (CompanyL2Id is null"
if trim(sCompanyL2IdList) <> "" then
	sSql = sSql & " OR CompanyL2Id in (" & sCompanyL2IdList & ")"
end if
sSql = sSql & ") "

'Region Clause
sSql = sSql & "AND (CompanyL3Id is null"
if trim(sCompanyL3IdList) <> "" then
	sSql = sSql & " OR CompanyL3Id in (" & sCompanyL3IdList & ")"
end if
sSql = sSql & ") "

'Branch Clause
sSql = sSql & "AND (BranchId is null"
if trim(sBranchIdList) <> "" then
	sSql = sSql & " OR BranchID in (" & sBranchIdList & ")"
end if
sSql = sSql & ") "
	   
'Filter by search state if provided
if trim(iSearchStateId) <> "" then
	sSql = sSql & " AND S.StateId = '" & iSearchStateId & "'"
end if

sSql = sSql & "ORDER BY EDDX.DisplayStartdate ASC "

dim oConn
set oConn = new HandyAdo
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

'Add the retrieved events to the calendar.
dim sEvent
dim sRedirectUrl

if not (oRs.BOF and oRs.EOF) then
	do while not oRs.EOF
		dEventStartDate = oRs("DisplayStartdate")
		dEventEndDate = oRs("DisplayEnddate")
		sRedirectUrl = oRs("RedirectUrl")
		
		if isdate(dEventStartDate) and isdate(dEventEndDate) then
			'set the interval date to the greater of the Event Start date or Calendar Range Start Date
			if DateDiff("D", FormatDateTime(dEventStartDate, 2), FormatDateTime(dCalendarStartDateRange, 2)) <= 0 then
				dTmpDate = dEventStartDate
			else
				dTmpDate = dCalendarStartDateRange 
			end if 
			
			'Loop through the dates from start date to end date within the displayed Calendar ramge
			do while DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dEventEndDate, 2)) >= 0 and DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dCalendarEndDateRange, 2)) >= 0 
				sEvent = "<a class=""newsTitle"" href=""calendardetail.asp?id=" & oRs("DateID") & "&SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & """>" & oRs("Event") & "</a>"
				oCalendarDisplay.AddEvent cdate(dTmpDate), sEvent, oRs("Directions")
				dTmpDate = DateAdd("D", 1, dTmpDate)
			loop
		end if 
		oRs.MoveNext
	loop
	oRs.MoveFirst
end if 

'===========================================

dim sCalendarRow

%>
			<td valign="top" class="bckLtGrayBottomBorder"><span class="date">
<% = formatdatetime(Now, 1) %>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<table bgcolor="#ffffff" cellpadding="3" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="center" colspan="2">
<%
if sView = "month" then

	dim sPrevDate, sNextDate
	dim bDisplayPrevMonth, bDisplayNextMonth
	
	sPrevDate = oCalendarDisplay.SubtractOneMonth(dRefDate)
	sNextDate = oCalendarDisplay.AddOneMonth(dRefDate)
	if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
		bDisplayPrevMonth = false
	else
		bDisplayPrevMonth = true
	end if
	if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
		bDispalyNextMonth = false
	else
		bDisplayPrevMonth = true
	end if 	
	
	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
	oCalendarDisplay.HighlightReferenceDate = false

	oCalendarDisplay.PrintCalendar(dRefDate)	

end if

set oCalendarDisplay = nothing

%>
							</td>
						</tr>
						<tr>
							<td>
<%










if oRs.EOF and oRs.BOF then
%>
No Events Listed
<%
end if 
%>
									<ul class="bullet">
<%

do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""F6F4EE"""
	else
		sCalendarRow = ""
	end if
	
	Response.Write("<li type=""circle""><b>" & FormatDateTime(oRs("DisplayStartdate"), 2))
	if (oRs("DisplayEnddate") <> "") and (oRs("DisplayEnddate") <> oRs("DisplayStartdate")) then 
		if FormatDateTime(oRs("DisplayEnddate"), 2) = "1/1/2500" then
			response.write " to On-Going"
		else
			response.Write(" - " & FormatDateTime(oRs("DisplayEnddate"), 2))
		end if
	end if
	Response.Write("</b><br><a href=""calendar/calendardetail.asp?id=" & oRs("DateId") & """>" & oRs("Event") & "</a></li>" & vbCrLf)

	oRs.MoveNext

loop

set oConn = nothing
set oRs = nothing
%>
							</tr>							
						</table>
					</td>
				</tr>
			</table>	
			<br>
			
			<!-- Document Tracker -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="250">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" height="17" alt="Search" align="absmiddle" vspace="10"> Document Tracking</td>
				</tr>
				<tr>
					<td valign="top" class="bckLtGrayBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
<% if CheckIsAdmin() then %>
<td><a href="<% = application("URL") %>/documents/moddocument.asp" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> New Document</a></td>
<% else %>
&nbsp;
<% end if %>
									</tr>
								</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
<%
'Declare and initialize the document object
dim oDoc
dim iDocCounter
set oDoc = new Document
oDoc.ConnectionString = application("sDataSourceName")
oDoc.VocalErrors = application("bVocalErrors")
oDoc.OwnerId = session("UserCompanyId")

'Declare and initialize the license object
dim oLicense
set oLicense = new License
oLicense.VocalErrors = application("bVocalErrors")
oLicense.ConnectionString = application("sDataSourceName")

set oRs = oDoc.SearchDocs

if not (oRs.BOF and oRs.EOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Document</b></td>
		<td><b>Status</b></td>
	</tr>
<%
	iDocCounter = 1
	do while not oRs.EOF and not iDocCounter > 3
	
		if oRs(0) = "DOC" then 
		
			if oDoc.LoadDocById(oRs("DocId")) <> 0 then

				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & " valign=""top""><a href=""" & application("URL") & "/documents/moddocument.asp?did=" & oDoc.DocId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Document"" alt=""Edit This Document""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & " valign=""top""><a href=""javascript:ConfirmDocumentDelete(" & oDoc.DocId & ",'" & oDoc.DocName & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Document"" alt=""Delete This Document""></a></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.DocName)
				if oDoc.DocSentDate <> "" then
					Response.Write("<br><b>Sent: </b>" & oDoc.DocSentDate)
				end if
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.LookupDocStateId(oDoc.DocStateId) & "</td>" & vbCrLf)
				
			end if 
			
		elseif oRs(0) = "LIC" then
		
			if oLicense.LoadLicenseById(oRs("DocId")) <> 0 then
			
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & " valign=""top""><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & " valign=""top""><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">Renewal For " & oLicense.LookupState(oLicense.LicenseStateId) & " License #" & oLicense.LicenseNum)
				Response.Write("<br><b>Sent: </b>" & oLicense.SubmissionDate)
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">Pending</td>" & vbCrLf)
			
			end if 		
		
		end if 
		
		
		oRs.MoveNext
		iDocCounter = iDocCounter + 1
	
	loop
%>
	<tr>
		<% if CheckIsAdmin() then %>
		<td colspan="2"></td>
		<% end if %>
		<td colspan="2"><a href="documents/documentsearch.asp">View more documents...</a></td>
</table>
<%

else
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td>No Documents Found.</td>
	</tr>
</table>
<%
end if 

set oRs = nothing
%>

					</td>
				</tr>
			</table>
				
			<br>
		</td>





<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next

		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate

		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")

			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if

			iMonth = cint(p_month) - 1
			iYear = cint(p_year)

			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if

		if err then
			sReturn = ""
		end if

		sTestDate = p_month&"/"&sReturn&"/"&p_year

		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if

		getLastDayofmonth = sReturn

	end function
	
	
	
Sub DisplaySiteBlog()
	dim oBlog 'as object
	dim iBlogID,iPostID 'as integer
	dim rs 'as object
	dim sDate 'as string
	dim sTitle 'as string
	dim iTotalPostComments 'as integer
	dim iMemberID
	
	iMemberID = 0 'Site Mod
	
	set oBlog = New Blog
	oBlog.connectionString = application("sDataSourceName")

	'Get Blog info
	if not (oBlog.GetBlogByMember(iMemberID)) then
		response.redirect("/error.asp?message=The Blog could not be retrieved.")
	end if
			
	iBlogID = oBlog.BlogID
	
	'Get Posts
	oBlog.BlogID = iBlogID
	oBlog.PostDate = now()
	oBlog.PostExpirationDate = now()		
	oBlog.PostStatusID = 1 'active
	set rs = oBlog.SearchPosts("")	
	
	%>
			<!-- Site Blog -->
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td class="bluetitle" valign="bottom">ComplianceKeeper News</td>
					<td align="right" valign="bottom">
					</td>					
				</tr>
				<tr>
					<td colspan="2"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">

	<%	
	
	if not rs.eof then
	iCount = 0
	do while not rs.eof
		iPostID = rs("PostID")
		sTitle = rs("PostTitle")
		sText = rs("PostText")
		sDate = datepart("M",rs("PostDate")) & "." & datepart("D",rs("PostDate")) & "." & right(datepart("YYYY",rs("PostDate")),2)
		
		'Get the total Comments
		iTotalPostComments = oBlog.TotalPostComments(rs("PostID"))		

%>
	<tr>
		<td valign="top"><a name="<% = iPostId %>"></a><span class="smallTitle"><%= sDate %> | <a class="smallTitle" href="viewpost.asp?id=<% = iPostId %>"><%= sTitle %></a></span><br>
			<% = sText %>			
			<p>
			<span class="bluebold">
			<a href="viewpost.asp?id=<%=iPostID%>" class="bluebold">comments (<%= iTotalPostComments %>)</a>
			| <a href="viewpost.asp?id=<%=iPostID%>#comment" class="bluebold">post reply</a><br>
			</span>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
<%
	
		iCount = iCount + 1
		rs.movenext
	loop
	end if
	
	%>
						</table>
					</td>
				</tr>
			</table>
	<%
	
	set rs = nothing
	set oBlog = nothing
end sub
%>
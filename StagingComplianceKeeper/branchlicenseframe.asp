<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual="/includes/Company.Class.asp" -------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = true
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


'Make sure we were passed a valid BranchId
dim iBranchId
iBranchId = ScrubForSql(request("BranchId"))
if oCompany.Branches.GetSpecificBranch(iBranchId) = 0 then

	AlertError("This page requires a valid Branch ID.")
	Response.End

end if 


	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	'PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
<tr><td valign="top" align="left">
<table width="640" cellpadding="5" cellspacing="0" border="0" bgcolor="#ffffff">
<%
dim oRs 'Recordset object

set oRs = oCompany.Branches.GetBranchLicenses(iBranchId)

if oRs.State <> 0 then

	if not oRs.EOF then
		
		do while not oRs.EOF
%>
	<tr>
		<td width="100" align="left"><% = GetStateName(oRs("LicenseStateId"), 0) %></td>
		<td width="100" align="left"><% = oRs("LicenseNum") %></td>
		<td width="110" align="left"><% = oRs("LicenseExpDate") %></td>
		<td width="100" align="left"><% = oRs("LicenseTermMonths") %></td>
		<td width="100" align="left"><% = oRs("LicenseStatusId") %></td>
		<td width="130" align="left"><% = oRs("LastActionDate") %></td>
	</tr>
<%	
			oRs.MoveNext
		
		loop
%>
	<tr>
		<td width="100" align="left"><img src="/Media/Images/spacer.gif" width="100" height="1" alt="" border="0"></td>
		<td width="100" align="left"><img src="/Media/Images/spacer.gif" width="100" height="1" alt="" border="0"></td>
		<td width="110" align="left"><img src="/Media/Images/spacer.gif" width="110" height="1" alt="" border="0"></td>
		<td width="100" align="left"><img src="/Media/Images/spacer.gif" width="100" height="1" alt="" border="0"></td>
		<td width="100" align="left"><img src="/Media/Images/spacer.gif" width="100" height="1" alt="" border="0"></td>
		<td width="130" align="left"><img src="/Media/Images/spacer.gif" width="130" height="1" alt="" border="0"></td>
	</tr>
<%	
	else
%>
	<tr>
		<td height="100%" width="100%" valign="top" align="left">No State Licenses Issued for this Branch</td>
	</tr>
<%
	end if 
	
else
%>
	<tr>
		<td align="center">No State Licenses Issued for this Branch</td>
	</tr>
<%

end if 
%>

</table>
</td></tr>
</table>
</BODY></HTML>

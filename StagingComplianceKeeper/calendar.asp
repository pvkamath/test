<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/CalendarDisplay.class.asp" ---->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Event Calendar"

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/Calendar.asp")
	end if	
	
	if session("User_ID") = "" then
		iPage = 1
	else
		iPage = 0
	end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	'get calendar display
	dim oCalendarDisplay
	set oCalendarDisplay = New CalendarDisplay
	dim sEventType
	dim iSearchStateID

	sEventType = ScrubForSQL(getFormElement("SearchEventtype"))
	iSearchStateID = ScrubForSQL(getFormElement("SearchState"))	
		
	oCalendarDisplay.TemporaryRecordsetFile = Application("sAbsWebroot") & "includes\TempRecordsetDefinition.rst"
	oCalendarDisplay.StartOfBusinessDay = "7:00 AM"
	oCalendarDisplay.EndOfBusinessDay = "9:00 PM"
	oCalendarDisplay.HighlightReferenceDate = False
	oCalendarDisplay.DateLink = Application("sDynWebroot") & "calendar.asp?SearchState=" & iSearchStateID & "&SearchEventType=" & sEventType & "&View=day&refdate="
'	oCalendarDisplay.ReturnPage = sReturnPage
	oCalendarDisplay.TimeLink = ""
	oCalendarDisplay.ShowEmptyDaysInWeekView = True

	
	'get search criteria
	dim dStartDate, dEndDate, dRefDate, sView
	dim dRsStartDate, dRsEndDate, dTmpDate
	dim bDisplayEventListing
	dim sMode
	
	sView = request("view")
	if sView = "" then 
		sView = "month"
	end if
	
	if sView = "month" then
		bDisplayEventListing = true
	else
		bDisplayEventListing = false
	end if
	
	dRefDate = request("refDate")
	if dRefDate = "" then
		dRefDate = Date
	end if
	
	sMode = request("mode")
	
	dim rs
	set rs = Server.CreateObject("ADODB.Recordset")
	'get search results
	
	select case sView
		case "month"
		
		
			dStartDate = Month(dRefDate) & "/1/" & Year(dRefDate)					'make the rs start at the beginning of da month
			dStartDate = oCalendarDisplay.SubtractOneMonth(dStartDate)
			
			dEndDate = getLastDayOfMonth(Month(dRefDate), Year(dRefDate))
			dEndDate = oCalendarDisplay.AddOneMonth(dEndDate)
		case "week"
'			dRsStartDate = DateAdd("d", -(DatePart("W", dRefDate) - 1), dRefDate)	'first day of week
		case else
			dStartDate = dRefDate
			dEndDate = dRefDate
'			dRsStartDate = dRefDate
	end select
	
	
	'get events --------------------------------------------------------------------
	dim sSQL, sMonthYear
	sSQL = " SELECT DISTINCT E.EventID, E.Event, E.StartDate, E.EndDate, E.Active, E.ImgPath, E.Location, E.Directions, EET.EventID, ET.EventType, E.RedirectURL " & _
		   " FROM afxEvents E " &_
		   " LEFT OUTER JOIN afxEventsEventTypeX EET ON E.EventID = EET.EventID " & _
		   " LEFT OUTER JOIN afxEventType ET ON EET.EventTypeID = ET.EventTypeID " &_
		   " LEFT OUTER JOIN States as S on (E.StateID = S.StateID) "  & _		   
		   " WHERE E.EndDate >= '" & dStartDate & "' AND E.StartDate <= '" & dEndDate & " 23:59:59'" &_
		   " AND E.active = 1 " 

	'if true, filter by state
	If trim(iSearchStateID) <> "" Then											
		sSQL = sSQL & " AND S.StateID = '" & iSearchStateID & "'"
	End If
		   
	if sMode = "past" then
		sSQL = sSQL & " AND E.EndDate < '" & Date & "' " &_
					  " ORDER BY E.StartDate Desc "
	else
		sSQL = sSQL & " AND E.EndDate >= '" & Date & "' " &_
	    			  " ORDER BY E.StartDate ASC "
	end if
			
'	If sEventType <> "" Then											
'		sSQL = sSQL & " AND ET.EventType = '" & sEventType & "'"
'	End If
	
	
'	If sMode = "coming" Then
'		sActivitySQL = " AND ( (datepart(mm, E.EndDate) = " & CurrMonth & "  AND datepart(yyyy, E.EndDate) = " & CurrYear & "  AND datepart(dd, E.EndDate) >= " & CurrDate & "  ) OR (datepart(mm, E.EndDate) > " & CurrMonth & "   AND datepart(yyyy, E.EndDate) = " & CurrYear & "  ) OR datepart(yyyy, E.EndDate) > " & CurrYear & "  )"
'		sMonthActivitySQL = "  AND datepart(dd, E.EndDate) >= " & CurrDate   
'	Else 
'		sActivitySQL = " AND ( (datepart(mm, E.EndDate) = " & CurrMonth & "  AND datepart(yyyy, E.EndDate) = " & CurrYear & "  AND datepart(dd, E.EndDate) < " & CurrDate & "  ) OR (datepart(mm, E.EndDate) < " & CurrMonth & "   AND datepart(yyyy, E.EndDate) = " & CurrYear & "  ) OR datepart(yyyy, E.EndDate) < " & CurrYear & "  )"
'		sMonthActivitySQL = "  AND datepart(dd, E.EndDate) < " & CurrDate  
'	End If
	
'	If sMonthYear = "" Then						
'		sSQL = sSQL & sActivitySQL '& " AND E.active = 1"
'	Else
'		sSQL = sSQL & sActivitySQL & " AND datepart(mm, E.StartDate) = '" & Month(sMonthYear) & "'" & _
'				" AND datepart(yyyy, E.StartDate) = '" & Year(sMonthYear) & "'" 'AND E.active = 1"
'	End If
	

'	response.write sSQL
	'end get events --------------------------------------------------------------------
	
	rs.Open sSQL, g_objDBConn, 3, 1

	
	'add events to calendar
	dim sEvent
	dim sRedirectURL
	do while not rs.eof
		dRsStartDate = rs("StartDate")
		dRsEndDate = rs("EndDate")
		sRedirectURL = rs("RedirectURL")
		if isDate(dRsStartDate) and isDate(dRsEndDate) then
			dTmpDate = dRsStartDate
			do while DateDiff("D", FormatDateTime(dTmpDate,2), FormatDateTime(dRsEndDate,2)) >=0
				sEvent = "<a class=""newsTitle"" href=""/event.asp?id=" & rs("EventID") & "&SearchState=" & iSearchStateID & "&SearchEventType=" & sEventType & """>" & rs("Event") & "</a>"
				oCalendarDisplay.AddEvent cDate(dTmpDate), sEvent, rs("directions")
				dTmpDate = DateAdd("D", 1, dTmpDate) 
			loop
		end if
		rs.MoveNext
	loop
	rs.Close
	
	'get events for upcoming/previous events listing
	if bDisplayEventListing then
		sSQL = " SELECT DISTINCT E.EventID, E.Event, E.StartDate, E.EndDate, E.Active, E.ImgPath, E.Location, E.Directions, EET.EventID, ET.EventType, E.RedirectURL " & _
			   " FROM afxEvents E " &_
			   " LEFT OUTER JOIN afxEventsEventTypeX EET ON E.EventID = EET.EventID " & _
			   " LEFT OUTER JOIN afxEventType ET ON EET.EventTypeID = ET.EventTypeID " &_
			   " LEFT OUTER JOIN States as S on (E.StateID = S.StateID) "  & _		   
			   " WHERE E.EndDate >= '" & dStartDate & "' AND E.StartDate <= '" & dEndDate & " 23:59:59'" &_
			   " AND E.active = 1 " 

		'if true, filter by state
		If trim(iSearchStateID) <> "" Then											
			sSQL = sSQL & " AND S.StateID = '" & iSearchStateID & "'"
		End If
		   
		if sMode = "past" then
			sSQL = sSQL & " AND E.EndDate < '" & Date & "' " &_
						  " ORDER BY E.StartDate Desc "
		else
			sSQL = sSQL & " AND E.EndDate >= '" & Date & "' " &_
		    			  " ORDER BY E.StartDate ASC "
		end if
		rs.Open sSQL, g_objDBConn, 3, 1
	end if

	'set current date
	sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
%>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td>											
											<span class="pagetitle"><%=sCalendarType %> Calendar</span><p>
		<% if sView = "month" then %>												
											<form name="monthform" method="post" action="calendar.asp">
											<input type="hidden" name="SearchEventtype" value="<%=sEventType%>">
											<input type="hidden" name="refdate" value="<%= dRefDate %>">
											<table cellpadding="4">
												<tr>
													<td>View By State:</td>
													<td>
														<% call DisplayStatesDropDown(iSearchStateID,3,0) %>	
													</td>
												</tr>
											</table>
											</form>		
		<% end if %>
										
											<table width="100%" cellpadding=0 cellspacing=0 border=0>
		<% if sView = "month" then %>	
		<tr>
			<td width="100%" align="center" valign="top">
				<% 
				dim sPrevDate, sNextDate
				dim bDisplayPrevMonth, bDisplayNextMonth

				sPrevDate = oCalendarDisplay.SubtractOneMonth(dRefDate)
				sNextDate = oCalendarDisplay.AddOneMonth(dRefDate)
				if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
					bDisplayPrevMonth = false
				else
					bDisplayPrevMonth = true
				end if
				if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
					bDisplayNextMonth = false
				else
					bDisplayNextMonth = true
				end if
				%>
				<!--
				<table width="100%">
					<tr>
						<td width="38%" align="right"><% if bDisplayPrevMonth then %><a class=""newsTitle"" href="/calendar.asp?refDate=<%=sPrevDate%>"><%=MonthName(Month(sPrevDate)) & " " & Year(sPrevDate)%></a><% end if %></td>
						<td width="2%" align="center">|</td>
						<td width="20%" align="center"><%=MonthName(Month(dRefDate)) & " " & Year(dRefDate)%></td>
						<td width="2%" align="center">|</td>
						<td width="38%" align="left"><% if bDisplayNextMonth then %><a class=""newsTitle"" href="/calendar.asp?refDate=<%=sNextDate%>"><%=MonthName(Month(sNextDate)) & " " & Year(sNextDate)%></a><% end if %></td>
					</tr>
				</table>
				-->
<%
	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
	oCalendarDisplay.HighlightReferenceDate = False
%>
				<table border="0" cellpadding=0 cellspacing=0>
					
						<td valign="top" align="center"><% oCalendarDisplay.PrintCalendar(oCalendarDisplay.SubtractOneMonth(dRefDate)) %></td>
					
						<td><img src="/media/images/clear.gif" width="10" height="10"></td>
					
						<td valign="top" align="center"><% oCalendarDisplay.HighlightReferenceDate = True
							   oCalendarDisplay.PrintCalendar(dRefDate)
							   oCalendarDisplay.HighlightReferenceDate = False
							 %></td>
					
						<td><img src="<%=Application("sLeadtrackerSiteURL")%>media/images/clear.gif" width="10" height="10"></td>
					
						<td valign="top" align="center"><% oCalendarDisplay.PrintCalendar(oCalendarDisplay.AddOneMonth(dRefDate))  %></td>
					</tr>
				</table>
				
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<% end if %>

		<tr>
			<td width="100%" align="left" valign="top">


<%	

	
	'print event listing
	if bDisplayEventListing then	
		'display month view - listing of events
		if not rs.eof then
			dim sEventStartDate
			dim sEventEndDate
			dim sEventDateDuration

			
			response.write("<table>")
			response.write("<tr><td><b>Events:</b></td></tr>")
			do while not rs.eof
				sEventStartDate = FormatDateTime(rs("StartDate"),2)
				iEventStartMonth = month(sEventStartDate)
				iEventStartDay = day(sEventStartDate)
				sEventStartYear = year(sEventStartDate)
				sEventEndDate = FormatDateTime(rs("EndDate"),2)
				iEventEndMonth = month(sEventEndDate)
				iEventEndDay = day(sEventEndDate)
				sEventEndYear = year(sEventEndDate)
				
				'add leading zero to month, if necessary
				if iEventStartMonth < 10 then
					sEventStartMonth = "0" & iEventStartMonth
				else
					sEventStartMonth = iEventStartMonth
				end if
		
				'add leading zero to day, if necessary
				if iEventStartDay < 10 then
					sEventStartDay = "0" & iEventStartDay
				else
					sEventStartDay = iEventStartDay
				end if
				
				sEventStartDate = sEventStartMonth & "/" & sEventStartDay & "/" & sEventStartYear

				'add leading zero to month, if necessary
				if iEventEndMonth < 10 then
					sEventEndMonth = "0" & iEventEndMonth
				else
					sEventEndMonth = iEventEndMonth
				end if
		
				'add leading zero to day, if necessary
				if iEventEndDay < 10 then
					sEventEndDay = "0" & iEventEndDay
				else
					sEventEndDay = iEventEndDay
				end if
				
				sEventEndDate = sEventEndMonth & "/" & sEventEndDay & "/" & sEventEndYear
				
				if sEventStartDate = sEventEndDate then
					sEventDateDuration = "<td width=""70"" align=right>" & sEventStartDate & "</td><td colspan=""2"">&nbsp;</td>"
				else
					sEventDateDuration = "<td width=""70"" align=right>" & sEventStartDate & "</td><td>-</td><td width=""60"" align=right>" & sEventEndDate & "</td>"
				end if
				sRedirectURL = rs("RedirectURL")
					
				response.write("<tr>" & sEventDateDuration & "<td><td><img src=""media/images/clear.gif"" width=""10""></td><td>")
				response.write("<a class=""newsTitle"" href=""/event.asp?id=" & rs("EventID") & "&SearchState=" & iSearchStateID & "&SearchEventType=" & sEventType & "&refdate=" & dRefDate & """>" & rs("Event") & "</a>")
				response.write("</td></tr>")
				rs.MoveNext
			loop
			response.write("</table>")
		end if
		
		rs.Close
	else
		'display day view
		if sView <> "" then
			oCalendarDisplay.Mode = sView
		else
			oCalendarDisplay.Mode = "day"
			oCalendarDisplay.ShowAllTimeSlotsInDayView = False
			oCalendarDisplay.DayViewTimeInterval = 60
			oCalendarDisplay.StartOfBusinessDay = Session("sStartOfBusinessDay")
			oCalendarDisplay.EndOfBusinessDay = Session("sEndOfBusinessDay")
		end if
		
		if (FormatDateTime(dRefDate,2) = FormatDateTIme(Date,2)) then
			oCalendarDisplay.HighlightReferenceDate = True
		end if
		
		oCalendarDisplay.PrintCalendar(dRefDate)
	end if
	set rs = Nothing
%>
			</td>


		</tr>
	</table>

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<%
	function getLastDayOfMonth(p_month, p_year) 
		on error resume next
		
		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate
	
		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")
		
			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if
	
			iMonth = cint(p_month) - 1
			iYear = cint(p_year)
		
			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if
	
		if err then
			sReturn = ""
		end if
		
		sTestDate = p_month&"/"&sReturn&"/"&p_year
		
		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if
		
		getLastDayofmonth = sReturn
	
	end function
	
	
	function PrintMiniCalendars()
%>
			<td><img src="/media/images/clear.gif" width="10" height="10"></td>
			<td valign="top">
<%
	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
	oCalendarDisplay.HighlightReferenceDate = False
%>

				<table border="0" cellpadding=0 cellspacing=0>
					<tr>
						<td valign="top" align="center"><% oCalendarDisplay.PrintCalendar(oCalendarDisplay.SubtractOneMonth(dRefDate)) %></td>
					</tr>
					<tr>
						<td><img src="/media/images/clear.gif" width="10" height="10"></td>
					</tr>
					<tr>
						<td valign="top" align="center">
							<% oCalendarDisplay.HighlightReferenceDate = True
							   oCalendarDisplay.PrintCalendar(dRefDate)
							   oCalendarDisplay.HighlightReferenceDate = False
							 %></td>
					</tr>
					<tr>
						<td><img src="/media/images/clear.gif" width="10" height="10"></td>
					</tr>
					<tr>
						<td valign="top" align="center"><% oCalendarDisplay.PrintCalendar(oCalendarDisplay.AddOneMonth(dRefDate))  %></td>
					</tr>
				</table>
			</td>
			<td><img src="/media/images/clear.gif" width="10" height="10"></td>
			<td valign="top">
			</td>
<%	
	end function
%>	

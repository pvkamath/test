<%
Sub GetConnectionString()
end Sub

' Places commas into a number:
Function PlaceCommasInNumber(p_nValue) 

	dim sTempString, nDecimalPosition, nCommaPlace, sDecimal, bDecimalExists
	
	' Convert to String and round the number to the nearest penny:
	sTempString = CStr(Round(p_nValue, 2))
	
	' Find the position of the Decimal Place:
	bDecimalExists = False
	if InStr(sTempString, ".") then bDecimalExists = True
	
	nDecimalPosition = (len(sTempString) - InStr(sTempString, ".")) + 1
	
	' Save the decimal place in a variable:
	if bDecimalExists then
		sDecimal = right(sTempString, nDecimalPosition)
		sTempString = left(sTempString, len(sTempString) - nDecimalPosition)
		
		' Place a trailing zero on the end if is a decimal like .5
		if len(sDecimal) = 2 then sDecimal = sDecimal & "0"
	end if
	
	' Get the Length of the Value passed in:
	nCommaPlace = len(sTempString)
	
	' Find the position of where the first comma should be (from the dec place):
	nCommaPlace = nCommaPlace - 4
	
	' If we haven't reached the end of the string:
	do while nCommaPlace >= 0
		
		' Add a comma by splitting the string at the comma position and placing a comma in:
		sTempString = left(sTempString, nCommaPlace + 1) & "," & right(sTempString, (len(sTempString) - nCommaPlace) - 1)
		
		' Find where the next comma should be:
		nCommaPlace = nCommaPlace - 3
	Loop
	
	' Put the decimal place back on and return
	PlaceCommasInNumber = sTempString & sDecimal
End Function
%>
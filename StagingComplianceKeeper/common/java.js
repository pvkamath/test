<script language="JavaScript">
<!--
    function changeNewused(obj)
    {
      document.forms[0].newused.value = obj.options[obj.selectedIndex].value
      
      reload()
    }
    
    function changeMake(obj)
    {
      document.forms[0].make.value = obj.options[obj.selectedIndex].value
      
      reload()
    }
    
    function changeModel(obj)
    {
      document.forms[0].model.value = obj.options[obj.selectedIndex].value
      
      reload()
    }
    
    function changeColor(obj)
    {
      document.forms[0].color.value = obj.options[obj.selectedIndex].value
      
      reload()
    }
    
    function changePrice(obj)
    {
      document.forms[0].maxprice.value = obj.options[obj.selectedIndex].value
      
      reload()
    }
    
	function reload(){ 	
		var dbname = escape('<%= db %>');
		var newused = escape(document.forms[0].newused.value);
		var make = escape(document.forms[0].make.value);
		var model = escape(document.forms[0].model.value);
		var color = escape(document.forms[0].color.value);
		var maxprice = escape(document.forms[0].maxprice.value);
		var winloc = window.location.search;
		var sortIndex = winloc.indexOf("sort=");
		var sort = ""
		
		if (sortIndex != -1) {
			var endUrl = winloc.indexOf("&", sortIndex + 5)
			
			if (endUrl = -1) {
				endUrl = winloc.length;
			}
			
			sort = winloc.substring(sortIndex + 5, endUrl);
		}
		
		url="search.asp?newused=" + newused + "&make=" + make + "&model=" +
			model + "&color=" + color + 
			"&maxprice=" + maxprice + "&sort=" + sort + "&dbname=" + dbname;
		
		window.location=url;
		
	};
	
	function reset(){
			document.carform.reset;
	};
			
	function openHelp_SearchWindow(){
	        var width = screen.availWidth
	        var height = screen.availHeight
	        var screenX
	        var screenY
	        
	        screenX = width / 2 - 200
	        screenY = height / 2 - 200
	        
	        if (screenX < 0)
	        {
	          screenX = 0
	        }
	        
	        if (screenY < 0)
	        {
	          screenY = 0
	        }
	        
			window.open("/autoid/help_search.htm", "Help", "toolbar=0,width=400,height=400,scrollbars=1,resizable=0,screenX=" + screenX + ",left=" + screenX + ",screenY=" + screenY + ",top=" + screenY);
	}
			
	function openHelp_resultsWindow(){
	        var width = screen.availWidth
	        var height = screen.availHeight
	        var screenX
	        var screenY
	        
	        screenX = width / 2 - 200
	        screenY = height / 2 - 200
	        
	        if (screenX < 0)
	        {
	          screenX = 0
	        }
	        
	        if (screenY < 0)
	        {
	          screenY = 0
	        }
	        
			window.open("/autoid/help_results.htm", "Help", "toolbar=0,width=400,height=400,scrollbars=1,resizable=0,screenX=" + screenX + ",left=" + screenX + ",screenY=" + screenY + ",top=" + screenY);
	}
	
	function openHelp_profileWindow(){
	        var width = screen.availWidth
	        var height = screen.availHeight
	        var screenX
	        var screenY
	        
	        screenX = width / 2 - 200
	        screenY = height / 2 - 200
	        
	        if (screenX < 0)
	        {
	          screenX = 0
	        }
	        
	        if (screenY < 0)
	        {
	          screenY = 0
	        }
	        
			window.open("/autoid/help_profile.htm", "Help", "toolbar=0,width=400,height=400,scrollbars=1,resizable=0,screenX=" + screenX + ",left=" + screenX + ",screenY=" + screenY + ",top=" + screenY);
	}
	
	
    function dosubmit(){
		form1.submit()
	}

	function openCalculator(nCar_id){
	        var width = screen.availWidth
	        var height = screen.availHeight
	        var screenX
	        var screenY
	        
	        screenX = width / 2 - 135
	        screenY = height / 2 - 375
	        
	        if (screenX < 0)
	        {
	          screenX = 0
	        }
	        
	        if (screenY < 0)
	        {
	          screenY = 0
	        }
	        
			window.open("/autoid/calc.asp?car_id=" + nCar_id + "&db=<%=session("dbname")%>", "Calculator", "toolbar=0,width=270,height=375,resizable=0,screenX=" + screenX + ",left=" + screenX + ",screenY=" + screenY + ",top=" + screenY);
	}
		
		
	function openPrintWindow(nCar_id){
	        var width = screen.availWidth
	        var height = screen.availHeight
	        var screenX
	        var screenY
	        
	        screenX = width / 2 - 160
	        screenY = height / 2 - 275
	        
	        if (screenX < 0)
	        {
	          screenX = 0
	        }
	        
	        if (screenY < 0)
	        {
	          screenY = 0
	        }
	        
			window.open("/autoid/includes/printprofile.asp?car_id=" + nCar_id + "&db=<%=session("dbname")%>", "Print", "toolbar=0,width=320,height=550,resizable=0,scrollbars=1,screenX=" + screenX + ",left=" + screenX + ",screenY=" + screenY + ",top=" + screenY);
	}
	
	// Validation Functions
		
	function validateEmail(strEmail) {
		
			strInvalidChars = " /:,;";
			if (strEmail == "") {
				return false;
			}
			
			for (i=0; i<strInvalidChars.length; i++) {
				strInvalidCharMatch = strInvalidChars.charAt(i);
				if (strEmail.indexOf(strInvalidCharMatch,0) > -1) {
					return false;
				}
			}
			atPos = strEmail.indexOf("@",1)
			if (atPos == -1) {
				return false;
			}
			if (strEmail.indexOf("@",atPos+1) > -1) {
				return false;
			}
			periodPos = strEmail.indexOf(".",atPos)
			if (periodPos == -1) {
				return false;
			}
			if (periodPos+3 > strEmail.length) {
				return false;
			}
			return true;
		}
	
		function validateContact(strContactField) {
			selectedButton = -1
			for (i=0; i<strContactField.length; i++) {
				if (strContactField[i].checked) {
					selectedButton = i
				}
			}
			if (selectedButton == -1) {
				return false;
			}
			return true;
		}		
				
		
		function validateView()
		{
		  //return true
		  
		  var i
		  var bSelected = false
		  
		  if (document.form1.selected.length)
			{ // collection
			  for (i=0; i<document.form1.selected.length; i=i+1)
			  {
			    if (document.form1.selected[i].checked)
				{
					bSelected = true
				}
			  }
		   
			} else {
				// not a collection
				if (document.form1.selected.checked)
					{
						bSelected = true
					}
			}  
		  
		  if (!bSelected)
		  {
		    alert("No vehicles are selected for viewing.")
		    
		    return false
		  }
		  
		  return true
		}
				
	//-->

</script>
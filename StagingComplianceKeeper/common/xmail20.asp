<%


' Option Explicit
Response.Buffer = TRUE

' Xmail is the foundation for a generic form mailing component.
' It is designed to accept form fields from standard HTML and mail them
' out using the w3 Jmail Component.

'#####################################
'#
'# Global Variables
'#
'#####################################
dim strMailServerAddress	'The SMTP server that Jmail will use
dim strSuccessURL		'The page to redirect the user to after successfully sending mail.
dim strMailServer1, strMailServer2			

dim intFormNumber
dim i

	'form values
	dim strSenderNameFull, strSenderNameFirst, strSenderNameMI
	dim strSenderNameLast, strSenderEmail
	dim strRecipientNameFull, strRecipientNameFirst, strRecipientNameMI
	dim strRecipientNameLast, strRecipientEmail
	dim strBody, strBodyAppend, strNewUsed
	
	' Jmail values
	dim objJmail
	dim strEmailSubject, strEmailBody, strEmailRecipientName
	dim strEmailRecipientAddress, strEmailSenderName, strEmailSenderAddress


	dim intTotalFields 	'Total number of form fields
	dim strFieldName	'Form Field Name

'#####################################
'#
'# Global Constants
'#
'#####################################
				

sServerName = ucase(Request.ServerVariables("SERVER_NAME"))

select case mid(sServerName,1,4)

	case "ITEA"
		strMailServer1 = "192.168.1.15:25"
		strMailServer2 = "192.168.1.15"
	case "JUPI"
		strMailserver1 = "smtp.mindspring.com"
		strMailserver2 = "smtp.mindspring.com"
	case "DEV."
		strMailserver1 = "192.168.1.15:25"
		strMailServer2 = "192.168.1.15"
	case else
		strMailServer1 = "mail.xigroup.com:25" 
		strMailServer2 = "mail.mrproducer.com:25"
		
End Select
	
	
	' * DEBUG CODE *
	' Response.Write("MailServer: " & strMailServer1 & "<BR>")
	' Response.Write(Request.ServerVariables ("remote_addr") )
	
'#####################################
'#
'# Functions
'#
'#####################################
Function CheckRequiredFields
	' TO DO...
	' Before processing the form, ensure that all required fields
	' have been submitted
	
	CheckRequiredFields = TRUE
End Function

'#####################################
'#
'# Subs
'#
'#####################################


'#####################################
'#
'# Main
'#
'#####################################

' DEBUG - display all form variables
 'intFormNumber = Request.Form.Count
 'for i = 1 to intFormNumber
'	Response.Write(Request.Form.Key(i) & ": " & Request.Form(i) & "<br>")
 'next

' Response.End

'Create an instance of the Jmail object and set it's initial values

set objJmail = Server.CreateObject("JMail.message")
objJMail.silent = false
objJMail.logging = true

objJMail.ISOEncodeHeaders = false 
objJMail.ContentTransferEncoding = "8bit" 
	
	'Cycle through each field in the form, pulling out required values, and appending 
	'additional field names and values to the append variable
	
	' ********************
	' REQUIRED FIELDS
	' ********************

	strRecipientEmail = request("RecipientEmail")

	strSenderEmail = request("SenderEmail")
	strSenderName = request("SenderName")
	strSubject = request("Subject")
	strRecipientName = request("RecipientName")
	strSuccessURL =  request("SuccessURL")
	
	'*********************
	' VALIDATE THE RECIPIENT EMAIL
	' ****************************
	
	' bValidEmail = ValidateEmail(strRecipientEmail)
	
	'Response.Write ("recips: " &  strRecipientName)

	' DEBUG
	' Response.Write(strRecipientName)
	
	if not(strRecipientEmail = "") then
		objJmail.AddRecipient strRecipientEmail, strRecipientName
	else
		Response.write("XMAIL20: Error 001 - No RecipientEmail Provided")
		Response.end
	end if
	
	if strSenderEmail <> "" then
		objJmail.From = strSenderEmail
		objJmail.FromName = strSenderName
	else
		Response.write("XMAIL20: Error 002 - No SenderEmail Provided")
		Response.end
	end if
	
	' objJmail.FromName = strSenderNameFull
	' objJmail.AddHeader "Originating-IP", Request.ServerVariables ("REMOTE_ADDR")
	objJmail.Subject = strSubject
	
	' **************************************************
	' Throw all fields into the body of the message
	' **************************************************
	' find out how many fields are in the form, and pass each through the Case statement,
	' to extract system variables and append additional fields to the body of the email

	intTotalFields = Request.Form.Count
	'strBody = request("Body")
	'strNewUsed = request("NewUsed")
	
	For i = 1 to intTotalFields
		
		strFieldName = Request.Form.Key(i)
		
		strBody = strBody & strFieldName & ": " & Request.Form(i) & vbCrLf
		' DEBUG
		'Response.write (strFieldName & ": " & Request.Form(i) & "<BR>")
		
	Next

	objJmail.Body = strBody
	
	'mail it
	
	' DEBUG
	' ********************************							  
	' Response.write (" SERVER 1: " & strMailServer1 & "<BR>")
	' Response.write (" SERVER 2: " & strMailServer2 & "<BR>")
	
	if strMailServer1 = "" then
		Response.write("XMAIL20: Error 003 - No Primary Mail Server Specified")
		Response.end
	end if
	
	if strMailServer2 = "" then
		Response.write("XMAIL20: Error 003 - No Secondary Mail Server Specified")
		Response.end
	end if
	
	if not objJmail.Send( strMailServer1 ) then
		if not objJmail.Send( strMailServer2 ) then
			Response.write "<pre>log" & objjmail.log & "</pre>"
		else
			Response.write "Message sent succesfully using MailServer2!"
			
		end if
	else
		Response.write "Message sent succesfully using MailServer1! "
		
	end if

	objJmail.Close
	set objJmail = Nothing
	if instr (strSuccessURL, "?") then
		strSuccessURL = strSuccessURL & "&newused=" & strNewUsed 
	else
		strSuccessURL = strSuccessURL & "?newused=" & strNewUsed 
	end if
		Response.Redirect(strSuccessURL)

	
%>

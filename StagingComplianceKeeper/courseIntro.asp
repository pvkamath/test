<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
			
			<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date">Thursday 09/2704 at 3:26PM (Eastern)</span></td>
										</tr>
										<tr>
											<td class="courseTitle">Course Title 001 Overview</td>
										</tr>
										<tr>
											<td><b>Welcome Walter M. Broker.</b>  This is an overview of Course 001.  
												Below you will find a review of the sections you have completed 
												and have yet to complete.</td>
										</tr>
										<tr>
											<td><hr color="D8CAA9">
												<table width=100% border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td nowrap><b>Course Key:</b></td>
														<td width="100%"><img src="/Media/Images/keyCompleted.gif" width="117" height="21" alt="" border="0" hspace="5"><img src="Media/Images/keyIncomplete.gif" width="120" height="21" alt="" border="0" hspace="5"></td>
													</tr>
												</table>
												<hr color="D8CAA9">
											</td>
										</tr>
										<tr>
											<td>
												<table width=100% border=0 cellpadding=8 cellspacing=0>
													<tr>
														<td><img src="/Media/Images/iconComplete.gif" alt="Completed Course" border="0"></td>
														<td width="100%"><a href="#" class="newstitle">Course Section 1</a></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/iconComplete.gif" alt="Completed Course" border="0"></td>
														<td width="100%"><a href="#" class="newstitle">Course Section 2</a></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/iconIncomplete.gif" alt="Incompleted Course" border="0"></td>
														<td width="100%"><a href="#" class="newstitle">Course Section 3</a></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro"
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
			
			<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date">Thursday 09/2704 at 3:26PM (Eastern)</span></td>
										</tr>
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td class="blueTitle">Course Progress</td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td>
															<table border=0 cellpadding=6 cellspacing=1>
																<tr>
																	<td class="courseProgress"><a href="" class="courseProgress">1</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">2</a></td>
																	<td class="courseProgressH"><a href="" class="courseProgressH">3</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">4</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">5</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">6</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">7</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">8</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">9</a></td>
																	<td class="courseProgress"><a href="" class="courseProgress">10</a></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td class="courseTitle">Course Title 001 Overview</td>
										</tr>
										<tr>
											<td><p>This is the beginning of Course 001.  You may use the links above 
												to help you along.  Take your time and enjoy.   This is the beginning 
												of Course 001.  You may use the links below to help you along.  Take 
												your time and enjoy.  This is the beginning of Course 001.  You may 
												use the links below to help you along.  Take your time and enjoy.</p>
												
												<p>This is the beginning of Course 001.  You may use the links above to 
												help you along.  Take your time and enjoy.   This is the beginning of 
												Course 001.  You may use the links below to help you along.  Take your time 
												and enjoy.  This is the beginning of Course 001.  You may use the links below 
												to help you along.  Take your time and enjoy.</p></td>
										</tr>
										<tr>
											<td class="sectionTitle">Section Title Here</td>
										</tr>
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="2"><b>1.  Multiple Choice Question One?</b></td>
													</tr>
													<tr>
														<td colspan="2"><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 1</td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 2 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 3 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 4 </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="2"><b>2.  Multiple Choice Question Two?</b></td>
													</tr>
													<tr>
														<td colspan="2"><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 1</td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 2 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 3 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 4 </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="2"><b>3.  Multiple Choice Question Three?</b></td>
													</tr>
													<tr>
														<td colspan="2"><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 1</td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 2 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 3 </td>
													</tr>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="2" height="10" alt="" border="0"></td>
														<td><input type="radio" name="radio1" value="one"> Answer 4 </td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table width="100%" border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td width="50%"><a href=""><img src="Media/Images/bttnTip.gif" width="49" height="25" alt="Tip!" border="0"></a><img src="/Media/Images/spacer.gif" width="10" height="25" alt="" border="0"><a href=""><img src="Media/Images/bttnSaveProgress.gif" width="116" height="25" alt="Save My Progress" border="0"></a></td>
														<td align="right" width="50%"><a href=""><img src="Media/Images/bttnBack.gif" width="51" height="25" alt="Back" border="0"></a><img src="/Media/Images/spacer.gif" width="10" height="25" alt="" border="0"><a href=""><img src="Media/Images/bttnContinue.gif" width="70" height="25" alt="Continue" border="0"></a></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

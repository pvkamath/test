<%
		response.write("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">" & vbCrLf)
		response.write("<!-- Site Developed by XIGroup -->" & vbCrLf)
		response.write("<!-- www.xigroup.com -->" & vbCrLf)
		
		response.write("<html>")
		
		' Begin head tag:
		response.write("<head>" & vbCrLf)
		response.write PrintTextFile(Application("sDynWebroot") & "includes/designShellHeadInformation.asp")
		response.write("<title>" & Application("sDealer") & ": " & p_sTitle & "</title>" & vbCrLf)
	'	response.write("<title>" & Application("sSiteTitle") & "</title>" & vbCrLf)
				
		' Redirect if refresh url and refresh time are specified parameters
		if p_nRefreshTime <> "" and p_sRefreshURL <> "" then
			' Print Meta Refresh Info
			response.write("<META HTTP-EQUIV=""Refresh"" " &_
							"CONTENT=""" & p_nRefreshTime & ";" &_
							"URL=" & p_sRefreshURL & """>" & vbCrLf)
		end if
		
		response.write("<meta NAME=""Xchange Interactive Group"" Content=""XIG"">" & vbCrLf)
		response.write("<meta NAME=""Keywords"" Content="""&Application("sMetaKeyword")&""">" & vbCrLF)
		response.write("<meta NAME=""Description"" Content="""&Application("sMetaDescription")&""">" & vbCrLf)
	
		response.write("<meta HTTP-EQUIV=""Content-Type"" CONTENT=""text/html;CHARSET=iso-8859-1"">" & vbCrLf)
		
		response.write("<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""" & Application("sDynWebroot") & "includes/")
		if Session("browser") = "IE" then
			response.write Application("sDefaultIEStyle")
		else
			response.write Application("sDefaultNSStyle")
		end if
		response.write(""">" & vbCrLf)
				
		response.write("<script language=""JavaScript"" src=""" & Application("sDynWebroot") & "includes/common.js""></script>" & vbCrLf)
		
		if p_bUsesFormValidation then response.write("<script language=""JavaScript"" src=""" & Application("sStandardFormValidationLibrary") & """></script>")
		
		' Add in Author, Company, etc.
	
%>
<script language=javascript>
	function OpenNewsWindow(NewsID){
		window.open("/popupNews.asp?NewsID=" + NewsID,"NewsWindow","Width=350,Height=350,Scrollbars=1,Resizable=yes")
	}
</script>
<LINK REL="stylesheet" TYPE="text/css" HREF="/includes/Calendar.css">

<!-- include virtual="/includes/StatePreferences.Class.asp" ----------------->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!-- Site Developed by XIGroup -->
<!-- www.xigroup.com -->
<html><head>
<title>: Compliance Management Solutions</title>
<meta NAME="Xchange Interactive Group" Content="XIG">
<meta NAME="Keywords" Content="">
<meta NAME="Description" Content="">
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=iso-8859-1">

<LINK REL="stylesheet" TYPE="text/css" HREF="/includes/style_ns.css">
<script language="JavaScript" src="/includes/common.js"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
</head>
<body bgcolor="#EAEAEA" onLoad="JavaScript:InitializePage();" background="" class="" topmargin="0" leftmargin="0"  marginwidth="0" marginheight="0"><script language="javascript">
<!-- // Begin IMAGE rollovers

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

function changeImages() {
	if (document.images && (preloadFlag == true)) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}

var preloadFlag = false;
function preloadImages() {
	if (document.images) {		
		navIcon_Shield = newImage("/media/images/navIcon-Shield-over.gif");
		preloadFlag = true;
	}
}
preloadImages() 
// end IMAGE rollovers
// -->
</script>
<table width=760 align="center" bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td>
			<table width=760 bgcolor="FFFFFF" border=0 cellpadding=5 cellspacing=0>
				<tr>

					<td>
					<img src="/Media/Images/logocorp2.gif" alt="" border="0">
					
					</td>
					<td width="100%" class="pagetitle"></td>
					<td><img align="right" src="/Media/Images/logotext.gif" alt="ComplianceKeeper.com" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td>
			<table width="760" bgcolor="999999" border="0" cellpadding="0" cellspacing="0"><tr><td><img src="/media/images/spacer.gif" width="1" height="2" alt="" border="0"></td></tr></table>
		</td>
	</tr>
	<tr>
		<td>
			<table width=760 bgcolor="FFFFFF" background="/media/images/bgr-nav.gif" border=0 cellpadding=0 cellspacing=0>
				<tr>

					
					<td width="50%"><img src="/Media/Images/spacer.gif" width="10" height="28" alt="" border="0"></td>
					<td nowrap><a href="#" class="nav" target="_top"
						onmouseover="changeImages('home', '/media/images/navIcon-shield-over.gif'); return true;"
						onmouseout="changeImages('home', '/media/images/navIcon-shield.gif'); return true;">
						<img src="/media/Images/navIcon-shield.gif" name="home" id="home" width="22" height="20" alt="" border="0" align="absmiddle" hspace="5">Home</a></td>
					<td><img src="/Media/Images/spacer.gif" width="30" height="28" alt="" border="0"></td>
					<td nowrap><a href="#" class="nav" onmouseover="changeImages('brokers', '/media/images/navIcon-shield-over.gif'); return true;"
						onmouseout="changeImages('calendar', '/media/images/navIcon-shield.gif'); return true;"><img src="/media/Images/navIcon-shield.gif" name="calendar" id="calendar" width="22" height="20" alt="" border="0" align="absmiddle" hspace="5">Events</a></td>
					<td><img src="/Media/Images/spacer.gif" width="30" height="28" alt="" border="0"></td>
					<td nowrap><a href="#" class="nav" onmouseover="changeImages('brokers', '/media/images/navIcon-shield-over.gif'); return true;"
						onmouseout="changeImages('brokers', '/media/images/navIcon-shield.gif'); return true;"><img src="/media/Images/navIcon-shield.gif" name="brokers" id="brokers" width="22" height="20" alt="" border="0" align="absmiddle" hspace="5">Company</a></td>

					<td><img src="/Media/Images/spacer.gif" width="30" height="28" alt="" border="0"></td>
					<td nowrap><a href="#" class="nav" onmouseover="changeImages('branches', '/media/images/navIcon-shield-over.gif'); return true;"
						onmouseout="changeImages('branches', '/media/images/navIcon-shield.gif'); return true;"><img src="/media/Images/navIcon-shield.gif" name="branches" id="branches" width="22" height="20" alt="" border="0" align="absmiddle" hspace="5">Branches</a></td>
					<td><img src="/Media/Images/spacer.gif" width="30" height="28" alt="" border="0"></td>
					<td nowrap><a href="#" class="nav" onmouseover="changeImages('officers', '/media/images/navIcon-shield-over.gif'); return true;"
						onmouseout="changeImages('officers', '/media/images/navIcon-shield.gif'); return true;"><img src="/media/Images/navIcon-shield.gif" name="officers" id="officers" width="22" height="20" alt="" border="0" align="absmiddle" hspace="5">Officers</a></td>
					<td><img src="/Media/Images/spacer.gif" width="30" height="28" alt="" border="0"></td>
					
					<td width="50%"><img src="/Media/Images/spacer.gif" width="10" height="28" alt="" border="0"></td>
				
				</tr>
			</table>

		</td>
	</tr>
	<tr>
		<td>
			<table width="760" bgcolor="999999" border="0" cellpadding="0" cellspacing="0"><tr><td><img src="/media/images/spacer.gif" width="1" height="2" alt="" border="0"></td></tr></table>
		</td>
	</tr>
	<tr>
		<td>

			<table width=760 bgcolor="dadada" border=0 cellpadding=10 cellspacing=0>
				<tr>
					<td class="date" align="left"></td>
					<td class="date" align="right"><% = Now() %></td>
				</tr>
			</table>
		</td>
	</tr>

	<!-- Body Table  -->
	<tr>

		<td height="100%" valign="top">
			<table width=760 bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="40%" valign="top">
						<!-- Event Calendar -->

						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%"><tr><td>
						<table bgcolor="FFFFFF" border=0  cellpadding=10 cellspacing=0>
							<tr bgcolor="#8A99B6">
								<td><img src="/Media/Images/photo-calendar.jpg" width="40" height="40" alt="" border="0"></td>
								<td width="100%" class="sectiontitle">Event Calendar</td>
							</tr>
						</table>
						</td></tr>

						<tr><td>
						<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">
							<tr>

								<td colspan="2" valign="top"><span class="date">
April 2005
									</span>
								</td>
							</tr>
						</table>

						</td></tr>
						<tr><td>
						<table bgcolor="#ffffff" cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
<center>

		<table>
		<tr><td class="MonthView_CalendarWrapper">

			<table border="0" cellspacing="3" cellpadding="1" class="MonthView_CalendarMini" bgcolor="#eeeeee">
<tr><td colspan="7" class="MonthView_DayHeaderMini"><a href="calendar/calendar.asp?refDate=3/5/2005&mode=&SearchState=&SearchEventType=" class="MonthView_DayHeaderMini">&lt;&lt;</a>&nbsp;&nbsp;April 2005&nbsp;&nbsp;<a href="calendar/calendar.asp?refDate=5/5/2005&mode=&SearchState=&SearchEventType=" class="MonthView_DayHeaderMini">&gt;&gt;</a></td></tr>

				<tr>
					<td class="MonthView_DayHeaderMini">S</td>
					<td class="MonthView_DayHeaderMini">M</td>
					<td class="MonthView_DayHeaderMini">T</td>
					<td class="MonthView_DayHeaderMini">W</td>

					<td class="MonthView_DayHeaderMini">T</td>
					<td class="MonthView_DayHeaderMini">F</td>
					<td class="MonthView_DayHeaderMini">S</td>
				</tr>
<tr>
<td class="MonthView_DayEmptyMini">&nbsp;</td>
<td class="MonthView_DayEmptyMini">&nbsp;</td>
<td class="MonthView_DayEmptyMini">&nbsp;</td>
<td class="MonthView_DayEmptyMini">&nbsp;</td>
<td class="MonthView_DayEmptyMini">&nbsp;</td>

<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F1%2F2005">1</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F2%2F2005">2</a></td>
</tr>
<tr>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F3%2F2005">3</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F4%2F2005">4</a></td>
<td class="MonthView_DayCurrentMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F5%2F2005">5</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F6%2F2005">6</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F7%2F2005">7</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F8%2F2005">8</a></td>

<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F9%2F2005">9</a></td>
</tr>
<tr>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F10%2F2005">10</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F11%2F2005">11</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F12%2F2005">12</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F13%2F2005">13</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F14%2F2005">14</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F15%2F2005">15</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F16%2F2005">16</a></td>

</tr>
<tr>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F17%2F2005">17</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F18%2F2005">18</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F19%2F2005">19</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F20%2F2005">20</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F21%2F2005">21</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F22%2F2005">22</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F23%2F2005">23</a></td>
</tr>

<tr>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F24%2F2005">24</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F25%2F2005">25</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F26%2F2005">26</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F27%2F2005">27</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F28%2F2005">28</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F29%2F2005">29</a></td>
<td class="MonthView_DayEmptyMini"><a class="newsTitle" href="/calendar/calendar.asp?SearchState=&SearchEventType=&View=day&refdate=4%2F30%2F2005">30</a></td>
</tr>

			</table>
		</td></tr>
		</table>

</center>
<p>

No Events Listed

									<ul class="bullet">

									</ul>
								</td>

							</tr>							
						</table>
						</td></tr></table>
						
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- Listed Companies -->
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="100%">
									<table bgcolor="FFFFFF" border=0 cellpadding=10 cellspacing=0>

										<tr bgcolor="#8A99B6">
											<td><img src="/Media/Images/photo-companies.jpg" width="40" height="40" alt="" border="0"></td>
											<td width="100%"><span class="sectiontitle">Search Database</span><br></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>

								<td width="100%">
									<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td class="newstitle" nowrap>Search Database</td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>

														<form name="DbSearch" method="POST" action="#">
														<td><input type="text" name="DbSearchText" size="20" value=""></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image3 name=image3></td>
														</form>
													</tr>
												</table>
											</td>
										</tr>

									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td></td>
										</tr>

									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>

							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="6" height="10" alt="" border="0"></td>
					<td width="60%" valign="top">
					
						<!-- Renewal Alerts -->
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="100%">

									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0">	
										<tr bgcolor="#8a99b6">
											<td><img src="/Media/Images/photo-timer.jpg" width="40" height="40" alt="" border="0"></td>
											<td width="100%"><span class="sectiontitle">License Renewal Alerts</span><br></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>

								<td width="100%">
									<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width="100%"></td>
														<td class="newstitle" nowrap>Search Renewals</td>

														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<form name="90DaySearch" method="POST" action="#">
														<td><input type="text" name="90DaySearchText" size="20" value=""></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
														</form>
													</tr>
												</table>
											</td>

										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>

												<table width="100%" bgcolor="FFFFFF" border=0 cellpadding=4 cellspacing=0>

													<tr>
														<td>&nbsp;&nbsp;</b>Expiring in 90 Days:</b> <b>0</b> Licenses</b> - <a href="#">View List</a></td>
													</tr>

													<tr>
														<td>&nbsp;&nbsp;</b>Expiring in 60 Days:</b> <b>0</b> Licenses</b> - <a href="#">View List</a></td>
													</tr>

													<tr>

														<td>&nbsp;&nbsp;</b>Expiring in 30 Days:</b> <b>0</b> Licenses</b> - <a href="#">View List</a></td>
													</tr>

												</table>
											</td>

										</tr>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="5" height="5" alt="" border="0"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>

						<!-- Renewal Alerts -->
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0">	
										<tr bgcolor="#8a99b6">
											<td><img src="/Media/Images/photo-tasks.jpg" width="40" height="40" alt="" border="0"></td>
											<td width="100%"><span class="sectiontitle">Licenses Pending</span><!--<br>
												Display
												<a href="default.asp?pendinglist=20" class="newstitle">20</a> <a href="default.asp?pendinglist=40" class="newstitle">40</a> <a href="default.asp?pendinglist=60" class="newstitle">60</a> 
												Listings
												-->
											</td>

										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>

												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td width="100%">&nbsp;</td>
														<td class="newstitle" nowrap>Search Pending Renewals</td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<form name="PendingSearch" method="POST" action="#">
														<td><input type="text" name="PendingSearchText" size="20" value=""></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>

														<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
														</form>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table width="100%" bgcolor="FFFFFF" border=0 cellpadding=4 cellspacing=0>

													<tr>
														<td colspan="2" width="100%" align="left">&nbsp;<b>0</b> Pending Licenses - <a href="#">View List</a></td>

													</tr>


												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- Individual Brokers -->
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td width="100%">
									<table bgcolor="FFFFFF" border=0 cellpadding=10 cellspacing=0>
										<tr bgcolor="#8A99B6">
											<td><img src="/Media/Images/photo-brokers.jpg" width="40" height="40" alt="" border="0"></td>
											<td width="100%"><span class="sectiontitle">Active Licenses</span><!-- <br>
												Display
												<a href="default.asp?officerlist=20" class="newstitle">20</a> <a href="default.asp?officerlist=40" class="newstitle">40</a> <a href="default.asp?officerlist=60" class="newstitle">60</a> 
												Listings
												-->

												<!--
												  <a href="#" class="newstitle">20</a>  <a href="#" class="newstitle">40</a>  <a href="#" class="newstitle">60</a>  Listings
												-->
												</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#eeeeee" border="0" cellpadding="10" cellspacing="0" width="100%">

										<tr>
											<td>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td width="100%">&nbsp;</td>
														<td class="newstitle" nowrap>Search Active Licensees</td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<form name="BrokerSearch" method="POST" action="#">											
														<td><input type="text" name="BrokerSearchText" size="20" value=""></td>

														<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
														<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0"></td>
														</form>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>

							</tr>
							<tr>
								<td width="100%">
									<table bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table width="100%" bgcolor="FFFFFF" border=0 cellpadding=4 cellspacing=0>

													<tr>

														<td colspan="2" width="100%" align="left">&nbsp;<b>0</b> Active Licenses - <a href="#">View List</a></td>
													</tr>

												</table>
											</td>
										</tr>
									</table>

								</td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>

			</table>
		</td>
	</tr>

	<tr>
		<td colspan="2" bgcolor="113C93" background="/media/images/footerBgr.jpg" style="background-repeat: no-repeat">
			<table width="100%" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="45" alt="" border="0"></td>

					<td class="footer" align="center" valign="bottom"><a href="" class="footer">Home</a> &nbsp; | &nbsp; <a href="" class="footer">Contact Us</a> &nbsp; | &nbsp; <a href="" class="footer">Links</a> &nbsp; | &nbsp; <a href="" class="footer">Privacy and Security</a></td>

				</tr>
				<tr>
					<td bgcolor="#113C93"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
</table></body>
</html>
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shellnosec.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<%

'Initialization Content

dim sPgeTitle
sPgeTitle = "Compliance Management Solutions"

if session("User_ID") <> "" then
	Response.Redirect("companyhome.asp")
end if


BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp

%>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>

<script type="text/javascript" src="/includes/styleswitcher.js"></script>
<script language="JavaScript">
<!--
function changeImages() {
	if (document.images) {
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
		}
	}
}
-->
</script>
 
</HEAD>
<BODY topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">

<center>
<table width="812" height="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td rowspan="5" width="6" background="/media/images/shadow.gif"><img src="/media/images/spacer.gif" width="6" height="25" alt=""></td>
		<!-- <td rowspan="5" width="1" bgcolor="#cccccc"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td> -->
		<td valign="top"><img src="/media/images/hometopbanner.gif" width="800" height="126" alt="ComplianceKeeper.com"></td>
		<!-- <td rowspan="5" width="1" bgcolor="#cccccc"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td> -->
		<td rowspan="5" width="6" background="/media/images/shadowr.gif"><img src="/media/images/spacer.gif" width="6" height="25" alt=""></td>
	</tr>
	<tr>
		<td valign="top"><a href="/default.asp"><img src="/media/images/tabHome.gif" width="57" height="31" alt="Home" border="0"></a><a href="/displayContent.asp?keywords=faqs"><img src="/media/images/tabFAQs.gif" width="56" height="31" alt="FAQs" border="0"></a><a href="/displayContent.asp?keywords=pressroom"><img src="/media/images/tabPressRoom.gif" width="95" height="31" alt="Press Room" border="0"></a><a href="/displayContent.asp?keywords=testimonials"><img src="/media/images/tabTestimonials.gif" width="97" height="31" alt="Testimonials" border="0"></a><a href="/displayContent.asp?keywords=partners"><img src="/media/images/tabPartners.gif" width="74" height="31" alt="Partners" border="0"></a><a href="/displayContent.asp?keywords=links"><img src="/media/images/tabLinks.gif" width="54" height="31" alt="Links" border="0"></a><a href="/displayContent.asp?keywords=privacy"><img src="/media/images/tabPrivacySecurity.gif" width="132" height="31" alt="Privacy & Security" border="0"></a><a href="/contactform.asp"><img src="/media/images/tabContactUs.gif" width="89" height="31" alt="Contact Us" border="0"></a><a href="<% = application("SECUREURL") %>/defaultlogin.asp"><img src="/media/images/tabSecureClientLogin.gif" width="142" height="31" alt="Secure Client Login" border="0"></a></td>
	</tr>
	<tr>
		<td valign="top"><img src="/media/images/homeimagebanner.jpg" width="800" height="122" alt=""></td>
	</tr>
	<tr>
		<td valign="top" height="100%">
			<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="40" class="bckHomeGradient"><img src="/media/images/spacer.gif" width="40" height="40" alt=""></td>
					<td valign="top">
						<img src="/media/images/spacer.gif" width="1" height="75" alt=""><br>
						<a href="/displaycontent.asp?keywords=learnmore" onmouseover="changeImages('homenavLearnMore', '/media/images/homenavLearnMore_over.gif'); return true;" onmouseout="changeImages('homenavLearnMore', '/media/images/homenavLearnMore.gif'); return true;"><img src="/media/images/homenavLearnMore.gif" id="homenavLearnMore" name="homenavLearnMore" width="230" height="25" border="0" alt="Learn More"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
						<a href="/displaycontent.asp?keywords=new" onmouseover="changeImages('homenavNew', '/media/images/homenavNew_over.gif'); return true;" onmouseout="changeImages('homenavNew', '/media/images/homenavNew.gif'); return true;"><img src="/media/images/homenavNew.gif" id="homenavNew" name="homenavNew" width="230" height="25" border="0" alt="New"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
						<a href="/displaycontent.asp?keywords=datetrackingsolutions" onmouseover="changeImages('homenavDataTrackingSolutions', '/media/images/homenavDataTrackingSolutions_over.gif'); return true;" onmouseout="changeImages('homenavDataTrackingSolutions', '/media/images/homenavDataTrackingSolutions.gif'); return true;"><img src="/media/images/homenavDataTrackingSolutions.gif" id="homenavDataTrackingSolutions" name="homenavDataTrackingSolutions" width="230" height="25" border="0" alt="Date Tracking Solutions"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
						<a href="/displaycontent.asp?keywords=askquestions" onmouseover="changeImages('homenavAskQuestions', '/media/images/homenavAskQuestions_over.gif'); return true;" onmouseout="changeImages('homenavAskQuestions', '/media/images/homenavAskQuestions.gif'); return true;"><img src="/media/images/homenavAskQuestions.gif" id="homenavAskQuestions" name="homenavAskQuestions" width="230" height="25" border="0" alt="Ask Questions"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
						<a href="/displaycontent.asp?keywords=systemsecurityinformation" onmouseover="changeImages('homenavSystemSecurityInformation', '/media/images/homenavSystemSecurityInformation_over.gif'); return true;" onmouseout="changeImages('homenavSystemSecurityInformation', '/media/images/homenavSystemSecurityInformation.gif'); return true;"><img src="/media/images/homenavSystemSecurityInformation.gif" id="homenavSystemSecurityInformation" name="homenavSystemSecurityInformation" border="0" width="230" height="25" alt="System Security Information"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
						<a href="/displaycontent.asp?keywords=Features" onmouseover="changeImages('homenavFeatures', '/media/images/homenavFeatures_over.gif'); return true;" onmouseout="changeImages('homenavFeatures', '/media/images/homenavFeatures.gif'); return true;"><img src="/media/images/homenavFeatures.gif" id="homenavFeatures" name="homenavFeatures" width="230" height="25" border="0" alt="Features & Benefits"></a><br>
						<img src="/media/images/spacer.gif" width="1" height="4" alt=""><br>
					</td>
					<td bgcolor="#cccccc" width="1"><img src="/media/images/spacer.gif" width="1" height="421" alt=""></td>
					<td width="39"><img src="/media/images/spacer.gif" width="39" height="1" alt=""></td>
					<td width="100%" valign="top" class="bckHomeGradient">
						<table width="100%" cellpadding="15" cellspacing="0" border="0">
							<tr>
								<td valign="top">
									<p class="sectionTitleBlack"><b>Corporate Client Login</b></p>
									<p>
									<form name="Login" method="POST" action="loginproc.asp">
									<input type="hidden" name="referrer" value="<%= request("referrer") %>">
									<input type="hidden" name="EventID" value="<%= request("eventid") %>">
									<input type="hidden" name="lid" value="<%= request("lid") %>">									
									<input type="hidden" name="id" value="<%= request("id") %>">									
									<table border=0 cellpadding=0 cellspacing=0>
										<tr>
											<td align="right" class="newstitle" nowrap>Login: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="text" name="User_Login" size="20" value=""></td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										<tr>
											<td align="right" class="newstitle" nowrap>Password: </td>
											<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
											<td><input type="password" name="User_Password" size="20" value=""></td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
											<td align="left" class="newstitle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0">
										</tr>
									</table>
									</form>
									</p>
								</td>
							</tr>
						</table>
					</td>						
				</tr>
			</table>
		</td>
	</tr>
</table>
</center>
</BODY>
<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	'PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>


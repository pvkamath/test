<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Event Calendar"

	Dim rs, objConn, strSQLConnectionString, searching

	strSQLConnectionString = application("sDataSourceName")

	Set objConn = Server.CreateObject("ADODB.Connection")
	objConn.Open strSQLConnectionString

	Set rs = Server.CreateObject("ADODB.Recordset")

	iEventID = Request("eventid")
	sEventType = request("EventType")
	sMonthYear = request("eventdate")
	sLocation = Request("location")
	iNumEvent = request("numevent")
	iPageNumber = request("page_number")
	sDisplayStartDate = request("DisplayStartDate")
	sDisplayEndDate = request("DisplayEndDate")
	
	If sEventType <> "" Then
	sSQL = "SELECT *, ET.EventType " & _
			"FROM afxEvents E, afxEventType ET, afxEventsEventTypeX EET " & _
			"WHERE E.EventID = EET.EventID AND EET. EventTypeID = ET.EventTypeID " & _
			"AND E.EventID =  '" & iEventID & "'"
	Else 
		sSQL = "Select * From afxEvents Where EventID = '" & iEventID & "'"
	End If
	
	rs.Open sSQL, objConn, 3, 1
	
	set searching = server.CreateObject("NewsFXV2.Searching")

	if isnull(rs("directions")) then
		sDirections = " " 
	else
		sDirections = rs("directions")
	end if
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//-->
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
</script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS, bDisplayPopUp ' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

	
<%setGlobalContentData("Calendar")%>
<span class="title"><%= g_sHeadline%></span>
<p><%= g_sText%>
<p>
<a href="/Calendar.asp?EventDate=<%=sMonthYear%>&EventType=<%=sEventType%>&location=<%=slocation%>&numevent=<%=iNumEvent%>&page_number=<%=iPageNumber%>"
onmouseover="changeImages('calendar', 'media/images/btn_calendar_over.gif'); return true;"
onmouseout="changeImages('calendar', 'media/images/btn_calendar.gif'); return true;">
<img name="calendar" src="media/images/btn_calendar.gif" width="160" height="12" alt="List All Events" border="0"></a><p>

<span class="subhead"><%=rs("Event")%></span>

<p>
<%
	Dim sDateArray, sEventStartDate, sEventTime, sTime, sTimeStart, sTimeEnd 
									
	'Compute and Display Start Date
	
	sDateArray = Split(rs("StartDate"), " ")											
	sEventStartDate = sDisplayStartDate
	
								
	'Must set a check for when the time is equal to 12AM,
	'because the Database does not store the time value for 12AM
	if (UBound(sDateArray) > 0) then
		sEventTime = sDateArray(1)
		sTime = Split(sEventTime, ":")
		sTimeStart = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
	else
		'sTimeStart = "12:00 AM"
		sTimeStart = ""
	end if
									
	'response.write FormatDateTime(sEventStartDate, 1) & "<br>" & sTimeStart
	Response.Write "<b>"
	response.write FormatDateTime(sEventStartDate, 1)  
	Response.Write "</b>"

	'Compute and Display End Date
	sDateArray = Split(rs("EndDate"), " ")
	sEventEndDate = sDisplayEndDate
	
	'Must set a check for when the time is equal to 12AM,
	'because the Database does not store the time value for 12AM
	if (UBound(sDateArray) > 0) then
		sEventTime = sDateArray(1)
		sTime = Split(sEventTime, ":")
		sTimeEnd = sTime(0) & ":" & sTime(1) & " " & sDateArray(2)
	else
		'sTimeEnd = "12:00 AM"
		sTimeEnd = ""
	end if

	If sEventEndDate = sEventStartDate Then
		if len(sTimeStart) <> 0 then 
			Response.Write "<br>" & sTimeStart
			if len(sTimeEnd) <> 0 then
				response.write " - "
			end if
		end if
		if len(sTimeEnd) <> 0 then 
			if len(sTimeStart) = 0 then
				Response.Write "<br>"
			end if
			response.write sTimeEnd
		end if
	Else
		if sEventEndDate = "1/1/2020" then
			response.write " to On-Going"
		else
			response.write " to <b>" & FormatDateTime(sEventEndDate, 1) & "</b><br>" 
		end if
					
		if (sTimeStart = sTimeEnd) then	'if times are the same only display 1 time
			response.write sTimeStart
		else
			if len(sTimeStart) <> 0 then 
				Response.Write sTimeStart
				if len(sTimeEnd) <> 0 then
					response.write " - "
				end if
			end if
			if len(sTimeEnd) <> 0 then 
				response.write sTimeEnd
			end if
		end if
	End If
%>

<!-- content here -->
		
<p><%= searching.TruncateLength( sDirections, 100000)%></p>

<p>
<%If rs("ImgPath") <> "" Then%>
<img src="<%=Application("sDynWebroot")%>usermedia/images/calendarfx/<%=rs("ImgPath")%>" alt="calendar of events" hspace=5 vspace=5 border="0">
<%End If%>

<%	
set searching = nothing

	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

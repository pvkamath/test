<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual= "/survey/includes/SurveyConfig.asp"-->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Virginia new car, used car, and certified used car dealer"
	sKeywords = getFormElement("keywords")
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<%	
	dim id, answerID, sSubmit, sMode, bPageBreak, nAbsPosition, nPrevAbsPosition, nUserID, nPopup
	dim bEmpty
	dim sChecked, sUserMiscText

	id = request("id")	'this is the Survey id
	sSubmit = request("submit")
	sMode = request("mode")
	nUserID = session("nSurveyUserID")
	nPopup = request("popup")
	
	if nUserID = "" then
		bEmpty = true
	end if	
	
	'get survey
	dim objSurveySystemObject, objSurvey, rsQuestions, rsAnswers, rs, rsUserResults, rsJavascript, rsTextResults
	set objSurveySystemObject = New SurveySystemObject
	objSurveySystemObject.ConnectionString = application("sDataSourceName")
	set objSurvey = objSurveySystemObject.GetSurvey(id)
	
	'if the user has completed this survey already, redirect to the survey listing page
	if not bEmpty and sSubmit <> "GO" AND nPopup <> 1 then
		if objSurvey.IsCompletedByUser(nUserID) then
			set objSurvey = Nothing
			set objSurveySystemObject = Nothing
			response.redirect("/survey/UserSurveys.asp")
		end if
	end if
	
	'these are used for page breaks
	bPageBreak = false
	nAbsPosition = request("AbsPosition")
	if nAbsPosition = "" then
		nAbsPosition = 1
	else
		nAbsPosition = cint(nAbsPosition)
		nPrevAbsPosition = objSurvey.GetPreviousPage(nAbsPosition)
	end if

	'if this is the first page, then do not display a prev button
	if nAbsPosition = 1 then
		nPrevAbsPosition = -1
	end if
	
	'get survey results settings
	dim bShowBarGraph, bShowPercentages, bShowNumVotes, sBarGraphColor, numColumns
	numColumns = 1
	
	bShowBarGraph = objSurvey.showBarGraph
	if bShowBarGraph = 1 then
		numColumns = numColumns + 1
		bShowBarGraph = true
	else
		bShowBarGraph = false
	end if
	
	bShowPercentages = objSurvey.showPercentages
	if bShowPercentages = 1 then
		numColumns = numColumns + 1
		bShowPercentages = true
	else
		bShowPercentages = false
	end if
	
	bShowNumVotes = objSurvey.showNumVotes
	if bShowNumVotes = 1 then
		numColumns = numColumns + 1
		bShowNumVotes = true
	else
		bShowNumVotes = false
	end if
	
	sBarGraphColor = objSurvey.BarGraphColor
	
	'this will be used for the rs of questions in the survey
	set rsQuestions = Server.CreateObject("ADODB.Recordset")
	set rsQuestions = objSurvey.GetQuestions(0)	'0 to get all questions
	if not rsQuestions.eof then 
		rsQuestions.AbsolutePosition = nAbsPosition
	end if
	
	'this will be used to get the answers for each question
	set rsAnswers = Server.CreateObject("ADODB.Recordset")

	'this is for the javascript
	set rsJavascript = objSurvey.GetFormValidation(nAbsPosition)
%>
<script TYPE="text/javascript" src="/includes/formCheck2.js"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		<% if nPopup = 1 then %>		
			window.open('/survey/SurveysResults.asp?id=<%=request("id")%>', 'SurveyResults', 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=1,resizable=1,dependent=1,width=760,height=400,left=50,top=50');
		<% end if %>		
			

		}
		// Enter Javascript Validation and Functions below: ----------------------
		function ValidateForm(frm) {
			var sMsg;
			var i;

			if (frm.save.value != "TRUE") {
<%	
questionID = 0


do while not rsJavascript.eof
	dim nJSquestionID, nJSinputType, nJSisRequired, nJSmaxResponses, nJSquestion, nJSanswerID
	
	nJSquestionID = rsJavascript("questionID")
	nJSinputType = rsJavascript("inputtype")
	nJSisRequired = rsJavascript("isRequired")
	nJSmaxResponses = rsJavascript("maxResponses")
	nJSquestion = rsJavascript("question")
	nJSanswerID = rsJavascript("answerID")
	
	if questionID <> nJSquestionID then
		questionID = nJSquestionID
		if (nJSinputType = 5) or (nJSinputType = 6) then	'this is a textbox or textarea %>
			if (isEmpty(frm.text<%=nJSanswerID%>.value)) {
				alert("Question: <%=nJSquestion%>\nPlease enter a response for this question.");
				frm.text<%=nJSanswerID%>.focus();
				frm.text<%=nJSanswerID%>.select();
				return false;
			}
<%		elseif (nJSinputType = 7) then	'this is a file upload %>
			var bFile<%=nJSanswerID%>Exists = false;
			for (i = 0; i < frm.elements.length; i++) {
				if (frm.elements[i].name == "file<%=nJSanswerID%>") {
					bFile<%=nJSanswerID%>Exists = true;
				}
			}
			if (bFile<%=nJSanswerID%>Exists) {
				if (isEmpty(frm.file<%=nJSanswerID%>.value)) {
					alert("Question: <%=nJSquestion%>\nPlease select a file to upload.");
					frm.file<%=nJSanswerID%>.focus();
					frm.file<%=nJSanswerID%>.select();
					return false;
				}
			}
<%		else 							'this is either a radio button or checkbox %>				
				sMsg = "Question: <%=nJSquestion%>\n"
<%			if nJSisRequired = 1 then 	'this is a required field%>
				sMsg = sMsg + "Please choose a response for this question.\n";
<%			end if %>				
<%			if nJSmaxResponses <> 0 then	'max number of responses is allowed 
%>				sMsg = sMsg + " -- no more than <%=nJSmaxResponses%> response(s) "
<%			end if %>				
				if (!isValidNumberOfResponses(frm.questionID<%=nJSquestionID%>, <%=nJSisRequired%>, <%=nJSmaxResponses%>)) {
					alert(sMsg);
					return false;
				}
				
<%		end if
	end if
	rsJavascript.moveNext
loop
rsJavascript.close
set rsJavascript = nothing
%>		
		}
		} // end if frm.submitbtn.value != "Save Progress"
		function isValidNumberOfResponses(p_oFormElement, p_iMinResponses, p_iMaxResponses) {
			var iCount = 0;
			for (x = 0; x < p_oFormElement.length; x++) {
				if (p_oFormElement[x].checked) {
					iCount++;
				}
			}
			if (p_iMinResponses != 0) {
				if (p_iMinResponses > iCount)
					return false;
			}
			if (p_iMaxResponses != 0) {
				if (p_iMaxResponses < iCount)
					return false;
			}
			return true;
		}
		
		function GetIndexOfValueInCheckOrRadio(p_oFormElement, p_sValue) {
			for (x = 0; x < p_oFormElement.length; x++) {
				if (p_oFormElement[x].value == p_sValue) {
					return x;
				}
			}	
			return -1;
		}
		
		function checkOther(sInputName, iAnswerID) {
			var iIndex;
			iIndex = GetIndexOfValueInCheckOrRadio(eval("document.forms[0]." + sInputName), iAnswerID)
			if (iIndex != -1) 
				eval("document.forms[0]." + sInputName + "[" + iIndex + "].checked = true");
		}
		
		function GoToPreviousPage() {
			window.location.href = "<%=Request.ServerVariables("SCRIPT_NAME")%>?id=<%=id%>&mode=<%=sMode%>&AbsPosition=<%=nPrevAbsPosition%>";
		}

		function OpenFile(p_sFileName) {
			window.open('/usermedia/survey/' + p_sFileName, 'File', "");
		}

		function ChangeFile(p_nSurveyID, p_nQuestionID) {
			window.open('Survey/SurveysChangeFile.asp?id=' + p_nSurveyID + '&questionID=' + p_nQuestionID, 'ChangeFile', "");
		}
		
		function SaveProgress() {
			document.frm.save.value = "TRUE";
			document.frm.SubmitMode.value = "Save Progress";
			
<% if bEmpty then %>			
			if (document.frm.email.value != "") 
<% end if %>			
				document.frm.submit();
		}
		
		function OpenSubmitPopup() {
			window.open("/Survey/SubmitPopup.asp", "Submit_Form", 'menubar=1,toolbar=0,location=0,directories=0,status=0,scrollbars=0,resizable=0,dependent=1,width=400,height=400');
		}
		
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS, bDisplayPopUp ' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<%setGlobalContentData(sKeywords)%>
<span class="title"><%= g_sHeadline%></span>
<p>
<%= g_sText%>	

<p>	
<span class="title"><%=objSurvey.Name%></span>
<p>	

<p>Please review our <a href="/displaycontent.asp?keywords=privacypolicy">Privacy Statement</a>
<p><font color="RED">*</font>&nbsp;Required Field
<%
if sSubmit <> "" then	'this form was submitted
	
	'********************************************************************************************************
	'********************************************************************************************************
	'THIS FORM WAS SUBMITTED - DISPLAY COLLECTIVE RESULTS

	dim item, nCount, iTotalVotes, sMiscText

%>
<TABLE border="0" width="600" cellpadding="2" cellspacing="0">

<%	do while not rsQuestions.eof 
		
		questionID = rsQuestions("id")
		sQuestion = rsQuestions("question")

		nCount = objSurvey.GetRecordCount("A", questionID)
%>									<!-- Display survey question -->
	<TR>
		<TD colspan="<%=numCOlumns%>" class="tblhdr"><%=sQuestion%></TD>
	</TR>
									
<%		
		if nCount <> 0 then
			'this questions has answers
			if nCount <> 1 then	'get results if there's more than one response
				iTotalVotes = objSurvey.GetTotalVotes(questionID)
				if iTotalVotes <> 0 then
					set rs = objSurvey.GetResults(questionID, iTotalVotes) 
					Set rs.ActiveConnection = Nothing 					

 					do while not rs.eof 
						sMiscText = objSurvey.GetTextResults(questionID, rs("answerID"))
					
					
					%>							
									<!-- DISPLAY ALL RESULTS FOR THIS QUESTION -->										
									<TR>
										<TD width="300"><%=rs("answer")%></TD>
<% if bShowBarGraph then %>				<TD><% if rs("percentage") <> 0 then %>											
<!--										<img src="/Survey/SurveyFX/media/images/rouge.gif" width="<%=rs("percentage")*100%>" height="10">
-->											<table width="<%=cint(rs("percentage")*100)%>" cellpadding="0" cellspacing="0">
												<tr bgcolor="<%=sBarGraphColor%>">
													<td>&nbsp;</td>
												</tr>
											</table>
											<% end if%></TD>
<% end if%>
<% if bShowPercentages then %>			<TD align="right"><%=FormatPercent(rs("percentage"), 1, -1) %></TD><% end if %>
<% if bShowNumVotes then %>				<TD align="right"><%=rs("numVotes") %> votes</TD><% end if %>
									</TR>
									<!-- text results for this particular answer -->
<%									if sMiscText <> "" then %>									
									<TR>
										<TD colspan="<%=numCOlumns%>">---&nbsp;&nbsp;<%= sMiscText %></TD>
									</TR>
<%									end if
						rs.movenext
					loop	'rs loop
				else
					set rs = objSurvey.GetAnswers(questionID)
					do while not rs.eof
					
%>									<!-- noone submitted responses to this question... default results to 0 -->
									<TR>
										<TD width="300"><%=rs("answer")%></TD>
<% if bShowBarGraph then %>				<TD>&nbsp;</TD><% end if %>
<% if bShowPercentages then %>			<TD align="right">0.0%</TD><% end if %>
<% if bShowNumVotes then %>				<TD align="right">0 votes</TD><% end if %>
									</TR>

<%						rs.moveNext
					loop
				end if 	'end if iTotalVotes <> 0
			else
				sMiscText = objSurvey.GetTextResults(questionID, vbblank)
				if sMiscText <> "" then 
%>									<TR>
										<TD colspan="<%=numCOlumns%>">---&nbsp;&nbsp;<%= sMiscText%></TD>
									</TR>
						
<%				end if
			end if		'end if nCount <> 1
		end if			'end if nCount <> 0
		rsQuestions.MoveNext
%>
									<TR>
										<TD colspan="<%=numCOlumns%>"><BR></TD>
									</TR>
<%		
	loop
%>	
								</TABLE>
<%
	'END DISPLAY COLLECTIVE RESULTS
	'********************************************************************************************************
	'********************************************************************************************************

else	'this form wasn't submitted

	'********************************************************************************************************	
	'********************************************************************************************************
	'DISPLAY FORM

	dim questionID, sQuestion, sInputType, sInputName


'	set rs = objSurvey.GetAnswers(id)
%>
<form action="/Survey/SurveysFormProc.asp" method="post" name="frm" id="frm" enctype="multipart/form-data" onSubmit="return ValidateForm(this)">
<input type="hidden" name="save" value="">
<input type="hidden" name="mode" value="<%=sMode%>">
<input type="hidden" name="id" value="<%=id%>">
<input type="hidden" name="formScriptName" value="<%=Request.ServerVariables("SCRIPT_NAME")%>">
<input type="hidden" name="SubmitMode" value="">
								
															
<TABLE border="0" width="550" cellpadding="3" cellspacing="0">

<%	do while not rsQuestions.eof and not bPageBreak


		questionID = rsQuestions("id")
		sQuestion = rsQuestions("question")
		iRequired = trim(rsQuestions("IsRequired"))
		


%>	
	<!-- display question -->
	<tr>
		<td align="right" width="175">
			<% if iRequired = 1 then%><font class="red">*</font>&nbsp;<% end if %><%=sQuestion%>
		</td>

<% 		
		set rsAnswers = objSurvey.GetAnswers(questionID)
		if not bEmpty then			
			set rsUserResults = objSurvey.GetUserResponse(questionID, nUserID)
		end if

	
		do while not rsAnswers.eof 
			sInputType = rsAnswers("inputType")
			sInputName = "questionID" & questionID
			select case sInputType
				case 1
					sInputType = "radio"
				case 2
					sInputType = "radio"	'with textbox
				case 3
					sInputType = "checkbox"
				case 4
					sInputType = "checkbox"	'with textbox
				case 5
					sInputType = "text"
					sInputName = "text" & rsAnswers("id")
				case 6
					sInputType = "textarea"
					sInputName = "text" & rsAnswers("id")
				case 7
					sInputType = "file"
					sInputName = "file" & rsAnswers("id")
				case else
					sInputType = "radio"
			end select
			
			'check to see if user's response matches
			if not bEmpty then 
				if not rsUserResults.eof then
					if rsAnswers("id") = rsUserResults("answerID") then
						sChecked = " checked"
						sUserMiscText = rsUserResults("miscText")
						rsUserResults.MoveNext
					else
						sChecked = ""
						sUserMiscText = ""
					end if
				else
					sChecked = ""
					sUserMiscText = ""
				end if
			end if
		
			if sInputType = "text" then		'display text box
%>								
		<td colspan="2" width="325"><input type="text" name="<%=sInputName%>" size="30" value="<%=sUserMiscText%>"></td>
	</tr>
<%			elseif sInputType = "textarea" then 'display text area%>
		<td colspan="2"><textarea name="<%=sInputName%>" rows="5" cols="30"><%=sUserMiscText%></textarea></td>
	</tr>
<%			elseif sInputType = "file" then 	'display file upload%>
		<td colspan="2">
			<% if sUserMiscText = "" then %>
						<input type="file" name="<%=sInputName%>" size="50">
					<% else
						response.write("You have uploaded this file: <a href=""javascript:OpenFile('" & sUserMiscText & "')"">" & sUserMiscText & "</a><br>" &_
									   "<a href=""javascript:ChangeFile(" & id & ", " & questionID & ");"">Change file</a>")
					   end if 
			%>
			<br><img src="/media/images/clear.gif" width="10" height="8" alt="" border="0">	
		</td>
	</tr>
<%	else 'display radio/checkbox%>
		<td valign="top"><input type="<%=sInputType%>" name="<%=sInputName%>" value="<%=rsAnswers("id")%>" <%=sChecked%>>
			<%=rsAnswers("answer")%><% if rsAnswers("inputType") = 2 or rsAnswers("inputType") = 4 then %>&nbsp;&nbsp;<input type="text" name="othertext<%=rsAnswers("id")%>" value="<%=sUserMiscText%>" onChange="javascript:checkOther('<%=sInputName%>', <%=rsAnswers("id")%>);"><%end if%>
			<br><img src="/media/images/clear.gif" width="10" height="8" alt="" border="0"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>		
<% 			end if %>
							
<%			rsAnswers.movenext
		loop	'rsAnswers loop
%>
		
<% 
		rsQuestions.MoveNext

		'this is to be used for any page breaks
		nAbsPosition = nAbsPosition + 1
		
%>	
		
<%		'check to see whether the next question is actually a page break
		if not rsQuestions.eof then
			if rsQuestions("userInput") = 9 then
				bPageBreak = true
'				nAbsPosition = nAbsPosition + 1	'need to increment so that we can pass it to the next page
			end if
		end if
	loop		'rsQuestions loop
%>
		<tr>
			<td>&nbsp;</td>
			<td>
				<input type="hidden" name="UserID" value="<%=nUserID%>">										
<%	if nPrevAbsPosition <> -1 then 	'display prev button%>
				<a href="javascript:GoToPreviousPage();"><img src="/media/images/btn-back.gif" width="65" height="24" alt="" border="0"></a>
<% 	end if %>				
<!--							
				<a href="javascript:SaveProgress();"><img src="/media/images/btn-save-exit.gif" width="130" height="24" alt="" border="0"></a>
-->											
<%
	if bPageBreak then	'this is a page break... put a back/next button
%>

				<input type="image" src="/media/images/btn-nextpage.gif" width="106" height="24" alt="" border="0">
				<input type="hidden" name="AbsPosition" value="<%=nAbsPosition%>">

<%	
	else				'this is the end of the form
%>	
				<input type="image" src="/media/images/btn_submit.gif" alt="Submit Form" border="0">
<%	
	end if
%>	
		</td>
	</tr>

</TABLE>
</form>
<% 
	'END DISPLAY FORM
	'********************************************************************************************************
	'********************************************************************************************************
end if 
%>
<% if sMode = "PREVIEW" then %>
	<a href="Survey/SurveysListQuestions.asp?id=<%=id%>">Back to Setup</a>
<% end if %>								
<!-- END CODE -->	


<!-- END PAGE CONTENT -->		
	
<%
set objSurvey = nothing
set objSurveySystemObject = nothing
set rs = nothing
set rsQuestions = nothing
set rsAnswers = nothing
set rsTextresults = nothing
set rsUserResults = Nothing
%>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

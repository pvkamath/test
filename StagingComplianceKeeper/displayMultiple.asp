<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	sKeywords = getFormElement("keywords")

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/displayMultiple.asp?Keywords=" & sKeywords)
	end if	
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - " & sKeywords
	
	if session("User_ID") = "" then
		iPage = 1
	elseif session("Access_Level") = 4 then
		iPage = 3
	else
		iPage = 0
	end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td>
												<span class="pagetitle"><%= sKeywords %></span>
											</td>
										</tr>										
										<tr>
											<td>
												<table>
<%
	'Retrieve any content
	set oContentAdmin = New ContentReport 'Found in include contentManager.Class.asp
	oContentAdmin.ConnectionString = Application("sDataSourceName")
	oContentAdmin.Keywords = sKeywords

	bRecordsFound = oContentAdmin.getContentList("Active","")
						
	if bRecordsFound then
		set rsContentList = oContentAdmin.ContentListRS
		rsContentList.Open
				
		while not rsContentList.EOF
			iNewsItemID = rsContentList.fields("newsitemid").Value
			sHeadline = rsContentList.fields("headline").Value
%>													
													<tr>
														<td>
															<a class="newsTitle" href="displayContent.asp?NewsItemID=<%=iNewsItemID%>&Keywords=<%=sKeywords%>"><%= sHeadline %></a>
														</td>
													</tr>
<%
			rsContentList.MoveNext
		wend
	else
%>
													<tr>
														<td>
															There is currently no information.  Please check back later.
														</td>
													</tr>
<%		
	end if 
	
	set oContentAdmin = nothing
	set rsContentList = nothing
%>													
												</table>											
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
	
	
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	sKeywords = getFormElement("keywords")

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/displayTrainingCourses.asp?Keywords=" & sKeywords)
	end if	
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - " & sKeywords
	
	if sKeywords = "conews" then
		sKeywords = session("User_CompanyId") & "conews"
	end if
	
	
	if session("User_ID") = "" then
		iPage = 1
	elseif session("Access_Level") = 4 then
		iPage = 3
	else
		iPage = 0
	end if

	setGlobalContentData(sKeywords)
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% height="100%" valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><a href="#"><img src="/Media/Images/step1-h.gif" width="94" height="41" alt="" border="0" hspace="0" vspace="0"></a><a href="#"><img src="/Media/Images/step2.gif" width="65" height="41" alt="" border="0" hspace="0" vspace="0"></a><a href="#"><img src="/Media/Images/step3.gif" width="123" height="41" alt="" border="0" hspace="0" vspace="0"></a><a href="#"><img src="/Media/Images/step4.gif" width="85" height="41" alt="" border="0" hspace="0" vspace="0"></a><a href="#"><img src="/Media/Images/step5.gif" width="83" height="41" alt="" border="0" hspace="0" vspace="0"></a><a href="#"><img src="/Media/Images/step6.gif" width="102" height="41" alt="" border="0" hspace="0" vspace="0"></a></td>
										</tr>
										<tr>
											<td><span class="pagetitle"><%= g_sHeadline %></span></td>
										</tr>
										<% if UCASE(sKeywords) = "TRAINING COURSES DESCRIPTION" then %>
										<tr>
											<td align="right">
												<img src="/Media/Images/step1SelectCourseType.gif" width="552" height="50" alt="Please Select a Course Type" border="0">
											</td>
										</tr>
										<% end if %>
										<tr>											
											<td>																															
												<p>											
												<% if UCASE(sKeywords) = "TRAINING COURSES DESCRIPTION" then %>
													Please click which program you would like to take
													<BR><BR>
													<a href="/Purchase/SelCourseType.asp?SearchCourseType=1&SearchProgram=1" class="newstitlebig">Online</a><BR>
													<a href="/Purchase/SelCourseType.asp?SearchCourseType=2&SearchProgram=1" class="newstitlebig">Home Study</a><br>
													<a href="/Calendar.asp" class="newstitlebig">View Live Class Calendar</a>											
												<% elseif UCASE(sKeywords) = "ONLINE" then %>
													<a href="/Purchase/SelCourseType.asp?SearchCourseType=1&SearchProgram=1" class="newstitlebig">Click here to purchase Online Courses</a>
												<% elseif UCASE(sKeywords) = "HOMESTUDY" then %>
													<a href="/Purchase/SelCourseType.asp?SearchCourseType=2&SearchProgram=1" class="newstitlebig">Click here to purchase Home Study Courses</a>
												<% elseif UCASE(sKeywords) = "LIVE CLASSROOM" then %>
													<a href="/Calendar.asp" class="newstitle">Click here to view Live Class Calendar</a>													
												<% end if %>
												
												<p><% = g_sText%>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

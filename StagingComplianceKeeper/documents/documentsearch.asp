<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Document.Class.asp" ------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "DOCUMENTS"

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmDocumentDelete(p_iDocId,p_sDocName)
{
	if (confirm("Are you sure you wish to delete the following Document:\n  " + p_sDocName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/documents/deletedocument.asp?did=' + p_iDocId;
}
</script>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Listed Branches -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-branches.gif" width="17" alt="Branches" align="absmiddle" vspace="10"> Document Tracker</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">		
						<table border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td class="newstitle" nowrap>Search Documents: </td>
								<td><img src="/Media/Images/spacer.gif" width="5" alt="" border="0"></td>
								<form name="DocSearch" method="POST" action="documentsearch.asp">
								<td><input type="text" name="DocSearchText" size="20" value="<% = ScrubForSql(request("DocSearchText")) %>"></td>
								<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0"></td>
								</form>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td>
												<table border="0" cellpadding="0" cellspacing="0">						
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
													</tr>
												</table>
<%


'Track the row colors
dim sColor


'Declare and initialize the license object
dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")



'Declare and initialize the document object
dim oDoc
set oDoc = new Document
oDoc.ConnectionString = application("sDataSourceName")
oDoc.VocalErrors = application("bVocalErrors")
oDoc.OwnerId = session("UserCompanyId")

'Search by our parameters
if request("DocSearchText") <> "" then
	oDoc.DocName = ScrubForSql(request("DocSearchText"))
end if

set oRs = oDoc.SearchDocs



'Set the page size based on the user-selected values.
oRs.PageSize = 40
oRs.CacheSize = 40
dim iPageCount
iPageCount = oRs.PageCount

'Get page number of which records are showing
dim iCurPage
dim iCount
iCurPage = trim(request("page_number"))
if (iCurPage = "") then
	if (trim(Session("CurPage")) <> "") then
		iCurPage = Session("CurPage")
	end if
else
	Session("CurPage") = iCurPage
end if

'Determine which search page the user has requested
if iCurPage = "" then iCurPage = 1

if clng(iCurPage) > clng(iPageCount) then iCurPage = iPageCount
		
if clng(iCurPage) <= 0 then iCurPage = 1





if not (oRs.BOF and oRs.EOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Document</b></td>
		<td><b>Status</b></td>
		<td><b>Date Sent</b></td>
	</tr>
<%
	'Set the beginning record to be displayed on the page
	oRs.AbsolutePage = iCurPage
	iCount = 0
	
	dim bLicense 'Is this a license record rather than a document?
	
	'do while not oRs.EOF
	do while (iCount < oRs.PageSize) and (not oRs.EOF)
	
		if oRs(0) = "DOC" then
			
			if oDoc.LoadDocById(oRs("DocId")) <> 0 then

				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & " valign=""top""><a href=""" & application("URL") & "/documents/moddocument.asp?did=" & oDoc.DocId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Document"" alt=""Edit This Document""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & " valign=""top""><a href=""javascript:ConfirmDocumentDelete(" & oDoc.DocId & ",'" & oDoc.DocName & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Document"" alt=""Delete This Document""></a></td>" & vbCrLf)
				elseif CheckIsAdmin() then
					Response.Write("<td " & sColor & "></td>" & vbCrLf)
					Response.Write("<td " & sColor & "></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.DocName)
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.LookupDocStateId(oDoc.DocStateId) & "</td>" & vbCrLf)
				if oDoc.DocSentDate <> "" then
					Response.Write("<td " & sColor & " valign=""top"">" & oDoc.DocSentDate & "</td>" & vbCrLf)
				else
					Response.Write("<td " & sColor & " valign=""top""></td>" & vbCrLf)
				end if
				
			end if 
			
		elseif oRs(0) = "LIC" then
		
			if oLicense.LoadLicenseById(oRs("DocId")) <> 0 then
			
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & "><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & "><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">Renewal For " & oLicense.LookupState(oLicense.LicenseStateId) & " License #" & oLicense.LicenseNum)
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">Pending</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">" & oLicense.SubmissionDate & "</td>" & vbCrLf)
			
			end if 					
		
		end if 
		
		oRs.MoveNext
		iCount = iCount + 1
	
	loop
%>
	</tr>
										<tr>
											<td colspan="5" align="center"><p><br>
		<%
		
		'Get the current querystring and strip and page references
		dim asQueryString, sQueryItem, sQueryString
		asQueryString = split(Request.QueryString, "&")
		for each sQueryItem in asQueryString
		
			if left(sQueryItem, 11) <> "page_number" then
			
				sQueryString = sQueryString & sQueryItem & "&"
			
			end if 
				
		next
		
	
		'Display the proper Next and/or Previous page links to view any records not contained on the present page
		if iCurPage > 1 then
			response.write("<a href=""documentsearch.asp?" & sQueryString & "page_number=" & iCurPage - 1 & """>Previous</a>" & vbcrlf)
		end if
		if (iCurPage > 1) AND (trim(iCurPage) <> trim(iPageCount)) then 	'if true, Add divider 
			response.write("&nbsp;|&nbsp;")
		end if 
		if trim(iCurPage) <> trim(iPageCount) then
			response.write("<a href=""documentsearch.asp?" & sQueryString & "page_number=" & iCurPage + 1 & """>Next</a>" & vbcrlf)
		end if
		Response.Write("<p>")

		response.write("<b>Page " & iCurPage & " of " & iPageCount & "</b>" & vbcrlf)
		%>
											</td>
										</tr>
	
	
</table>
<%

else
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td>No Documents Found.</td>
	</tr>
</table>
<%
end if 

set oRs = nothing

%>
												</td>
											</tr>
									</table>
								</td>
							</tr>
						</table>

						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

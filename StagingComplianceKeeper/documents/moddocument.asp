<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Document.Class.asp" ---------------------------------------------------->
<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


'Configure the administration submenu options
bAdminSubmenu = true
sAdminSubmenuType = "DOCUMENTS"


' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>


<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Declare the variables for license type/id
dim iOwnerId 'Company that owns this document
dim iDocId 'Passed ID of the document to edit
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iDocId = ScrubForSql(request("did"))
iOwnerId = session("UserCompanyId")


'Declare and initialize the document object
dim oDoc
set oDoc = new Document
oDoc.ConnectionString = application("sDataSourceName")
oDoc.VocalErrors = application("bVocalErrors")

'If we were passed a document ID, try to laod it now.
if iDocId <> "" then

	if oDoc.LoadDocById(iDocId) = 0 then
	
		'The load was unsuccessful.
		Response.Write("Failed to load the passed Document ID.<br>" & vbCrLf)
		Response.End

	end if 
	
	'Make sure that the loaded document belongs to the currently logged-in company.
	if oDoc.OwnerId <> iOwnerId then
	
		'This document does not belong to this company account.
		Response.Write("You do not have permission to edit this document.<br>" & vbCrLf)
		Response.End
	
	end if 
	
	sMode = "Edit"

else

	oDoc.OwnerId = iOwnerId
	sMode = "Add"
	
end if 


%>
<script language="Javascript" src="/admin/includes/DatePicker.js" type="text/javascript"></script>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.DocName, "Document Sent Date")
			|| !checkIsDate(FORM.DocSentDate, "Document Sent Date must be in the proper format.", 1)
			|| !checkIsDate(FORM.DocReceivedDate, "Document Received Date must be in the proper format.", 1)
		   )
			return false;

		return true;
	}
</script>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Licensing -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> Document Management</td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">

						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>


<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%= sMode %> a Document</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="DocumentForm" action="moddocumentproc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" value="<% = oDoc.DocId %>" name="did">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td><b>Document Name: </b></td>
		<td>
			<input type="text" name="DocName" value="<% = oDoc.DocName %>" size="30" maxlength="50">
		</td>
	</tr>
	<tr>
		<td><b>Document Status: </b></td>
		<td>
			<% call DisplayDocStateDropDown(oDoc.DocStateId, 1, "DocStateId") %>
		</td>
	</tr>
	<tr>
		<td><b>Document Sent Date: </b></td>
		<td>
			<input type="text" name="DocSentDate" value="<% = oDoc.DocSentDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('DocumentForm.DocSentDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('DocumentForm.DocSentDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td><b>Document Received Date: </b></td>
		<td>
			<input type="text" name="DocReceivedDate" value="<% = oDoc.DocReceivedDate %>" size="30" maxlength="30">
			<a href="javascript:show_calendar('DocumentForm.DocReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;"><img src="/admin/login/media/images/icon-calendar.gif" border=0 alt="Calendar" align="absmiddle"></a>&nbsp;&nbsp;<a href="javascript:show_calendar('DocumentForm.DocReceivedDate');" onmouseover="window.status='Click to choose the Date.';return true;" onmouseout="window.status='';return true;">Select a Date</a>			
		</td>		
	</tr>
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>

</table>
</form>

								</td>
							</tr>
						</table>
						</td></tr></table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%
set oRs = nothing
'set oPreference = nothing
set oDoc = nothing
'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>
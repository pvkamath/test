<%
option explicit
%>
<!-- #include virtual = "/includes/misc.asp" -------------------------------------------------------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------------------------------------------------------>
<!-- #include virtual = "/includes/shell.asp" ------------------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->

<!-- #include virtual = "/includes/Document.Class.asp" ---------------------------------------------------->

<%

' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1

	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = ""



'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()



'Declare the variables for license type/id
dim iOwnerId 'Company that owns this document
dim iDocId 'Passed ID of the document to edit
dim sMode 'Are we adding or editing?
dim oRs 'Recordset object

'Get the passed Ids
iDocId = ScrubForSql(request("did"))
iOwnerId = session("UserCompanyId")


'Declare and initialize the document object
dim oDoc
set oDoc = new Document
oDoc.ConnectionString = application("sDataSourceName")
oDoc.VocalErrors = application("bVocalErrors")


'If we were passed a document ID, try to laod it now.
if iDocId <> "" then

	if oDoc.LoadDocById(iDocId) = 0 then
	
		'The load was unsuccessful.
		Response.Write("Failed to load the passed Document ID.<br>" & vbCrLf)
		Response.End

	end if 
	
	'Make sure that the loaded document belongs to the currently logged-in company.
	if oDoc.OwnerId <> iOwnerId then
	
		'This document does not belong to this company account.
		Response.Write("You do not have permission to edit this document.<br>" & vbCrLf)
		Response.End
	
	end if 
	
	sMode = "Edit"

else

	oDoc.OwnerId = iOwnerId
	sMode = "Add"
	
end if 


'We have a valid document.  Go ahead and set the properties based on our passed
'form data.
if sMode = "New" then
	oDoc.OwnerId = iOwnerId
end if 
oDoc.DocName = ScrubForSql(request("DocName"))
oDoc.DocStateId = ScrubForSql(request("DocStateId"))
oDoc.DocSentDate = ScrubForSql(request("DocSentDate"))
oDoc.DocReceivedDate = ScrubForSql(request("DocReceivedDate"))


'Save the Document
if oDoc.SaveDoc = 0 then

	Response.Write("Failed to save the Document.  Please try again.<br>" & vbCrLf)
	Response.End
	
end if


set oDoc = nothing


'Redirect back to the home page.
Response.Redirect("documentsearch.asp")


%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shellnosec.asp" -------------->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Error"

	
	
	sMessage = trim(request("Message"))
	iCloseWin = trim(request("CloseWin"))
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top"><br>
						<table bgcolor="#7d8ba8" border="0" cellpadding="0" cellspacing="1" width="100%"><tr><td>
						<table width="100%" bgcolor="#eeeeee" border=0 cellpadding=10 cellspacing=0>
							<tr><td>
							<img src="<% = application("sDynMediaPath") %>icon-exclamation.gif" align="bottom" width="16" height="16" alt="">&nbsp;<b><font size="+1">Error</font></b>
							</td></tr>
						</table>
						</td></tr>
						<tr><td>
						<table width="100%" bgcolor="#ffffff" border="0" cellpadding="10" cellspacing="0">
							<tr><td>
												<b><%= sMessage %></b><br><br>
												<% if trim(iCloseWin) = "1" then %>
												<a class="newsTitle" href="javascript:window.close()">Close Window</a>
												<% else %>
												<a class="newsTitle" href="javascript:history.back()">Click here to go back...</a>
												<% end if %>
												
												<!-- Page Extension -->
												<br>
												<img src="<% = application("sDynMediaPath") %>clear.gif" width="1" height="150" alt="" border="0">
									
							</td></tr>			
						</table>
						</td></tr></table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->

<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Event Detail"
	
	dim iEventId
	iEventId = ScrubForSQL(request("id"))
	
	if session("User_ID") = "" then
		iPage = 1
	else
		iPage = 0
	end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 

dim oConn
dim oRs
dim sSql
dim iSearchMonth
dim sSearchEventType
dim iSearchStateID

set oConn = new HandyADO

iSearchStateID = getFormElement("SearchState")
sSearchEventType = getFormElement("SearchEventtype")

sSql = "SELECT Es.*, Cs.Name AS CourseName " & _
	   "FROM afxEvents AS Es " & _
	   "LEFT JOIN Courses AS Cs ON (Cs.CourseID = Es.CourseID) " & _
	   "WHERE Es.EventID = '" & iEventId & "' " & _
	   "AND Es.Active = '1' "
	   
set oRs = oConn.GetDisconnectedRecordSet(application("sDataSourceName"), sSql, false)

'set current date
sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()
	
%>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><%=sCurrentDate%></span></td>
										</tr>
										
<%
if oRs.EOF and oRs.BOF then

	Response.Redirect("error.asp?message=" & server.URLEncode("Could not locate the specified event."))
	Response.End

else

%>
										
										<tr>
											<td>
												<span class="pagetitle"><% = oRs("Event") %></span><p>
<% if oRs("ImgPath") <> "" then %>
<img src="<% = "/usermedia/images/CalendarFX/" & oRs("ImgPath") %>" alt="<% = oRs("Event") & " Image" %>" border="0"><p>
<% end if %>

												<table cellpadding="0" cellspacing="5" border="0">
<% if oRs("StartDate") <> "" then %> 
													<tr>
														<td><b>Starts: </b></td>
														<td><% = oRs("StartDate") %></td>
													</tr>
<% end if %>

<% if oRs("EndDate") <> "" then %>
													<tr>
														<td><b>Ends: </b></td>
														<td><% = oRs("EndDate") %></td>
													</tr>
<% end if %>
													<tr>
														<td></td>
														<td>&nbsp;</td>
													</tr>
<% if oRs("Location") <> "" then %>
													<tr>
														<td><b>Location: </b></td>
														<td><% = oRs("Location") %></td>
													</tr>
<% end if %>

<% if oRs("City") <> "" then %>
													<tr>
														<td><b>City: </b></td>
														<td><% = oRs("City") %></td>
													</tr>
<% end if %>

<% if oRs("StateID") <> "" then %>
													<tr>
														<td><b>State: </b></td>
														<td><% = GetStateName(oRs("StateID"), false) %></td>
													</tr>
<% end if %>

													<tr>
														<td></td>
														<td>&nbsp;</td>
													</tr>

<% if oRs("Organization") <> "" then %>
													<tr>
														<td><b>Organization: </b></td>
														<td><% = oRs("Organization") %></td>
													</tr>
<% end if %>


<% if oRs("Contact") <> "" then %>
													<tr>
														<td><b>Contact: </b></td>
														<td><% = oRs("Contact") %></td>
													</tr>
<% end if %>

<% if oRs("Phone") <> "" then %>
													<tr>
														<td><b>Phone: </b></td>
														<td><% = oRs("Phone") %></td>
													</tr>
<% end if %>

<% if oRs("WebAddress") <> "" then %>
													<tr>
														<td><b>Web Site: </b></td>
														<td><a href="<% = oRs("WebAddress") %>"><% = oRs("WebAddress") %></a></td>
													</tr>
<% end if %>

<% if oRs("Email") <> "" then %>
													<tr>
														<td><b>Email: </b></td>
														<td><a href="mailto:<% = oRs("Email") %>"><% = oRs("Email") %></a></td>
													</tr>
<% end if %>
													<tr>
														<td></td>
														<td>&nbsp;</td>
													</tr>

<% if oRs("CourseName") <> "" then %>
													<tr>
														<td><b>Course: </b></td>
														<td><% = oRs("CourseName") %></td>
													</tr>
<% end if %>

<% if oRs("Directions") <> "" then %>
													<tr>
														<td><b>General<br>Information: </b></td>
														<td valign="top"><% = oRs("Directions") %></td>
													</tr>
<% end if %>

<% if oRs("Registration") <> "" then %>
													<tr>
														<td><b>Registration: </b></td>
														<td><% = oRs("Registration") %></td>
													</tr>
<% end if %>
												</table>
											</td>
										</tr>
<% end if %>
										<tr>
											<td align="center" colspan="2">
												<br>
												<a class="newsTitle" href="/Calendar.asp?SearchEventType=<%=sSearchEventType%>&SearchState=<%=iSearchStateID%>"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" border=0></a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

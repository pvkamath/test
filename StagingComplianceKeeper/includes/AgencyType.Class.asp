﻿<%
'==============================================================================
' Class: AgencyType
' Controls the creation, modification, and removal of AgencyTypes.
' Create Date: 8/28/2013
' Author: Swarna
' -------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
'	- CourseId
'	- OwnerCompanyId
'	- Name
'	- Number
'	- CourseTypeId
'	- StateId
'	- ProviderId
'	- ContEdHours
'	- ExpDate
'	- StateApproved
'	- NonApproved
'	- Deleted
'	- InstructorName
'	- InstructorEducationLevel
'   - AccreditationTypeID
' Methods:
'	- LoadCourseById
'	- SaveCourse
'	- DeleteCourse
'	- DeleteCourseById
'==============================================================================

Class AgencyType

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Base course properties
	dim g_iAgencyTypeId
	dim g_sAgencyTypeName
	dim g_iOwnerCompanyId
    dim g_bDeleted
	
	' PUBLIC PROPERTIES =======================================================
	
	'Stores/retrieves the database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Stores/retrieves the trainingpro database connection string
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
		
	'Retrieves the unique Id of this course.  This is an identity in the DB,
	'so it is read-only.
	public property get AgencyTypeId()
		AgencyTypeId = g_iAgencyTypeId
	end property
	
	
	'Stores/retrieves the course name
	public property get Name()
		Name = g_sAgencyTypeName
	end property
	public property let Name(p_sName)
		g_sAgencyTypeName = left(p_sName, 100)
	end property
		
	'Company ID of the company which owns this course item
	public property get CompanyId()
		CompanyId = g_iOwnerCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iOwnerCompanyId = cint(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iOwnerCompanyId = "" 
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	'Notes whether the course has been deleted from the site.
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	

	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'nothing

	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'nothing
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		CheckConnectionString = false
	
		if isnull(g_sConnectionString) or _
			g_sConnectionString = "" then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) or _
			g_sTpConnectionString = "" then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Mark the specified Course entry as deleted
	' Preconditions: Connectionstring
	' Inputs: p_iCourseId - Course ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iAgencyTypeId)
	
		DeleteId = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iAgencyTypeId, false, "Agency Type ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iAgencyTypeId < 0 then
			
			oConn.ConnectionString = g_sTpConnectionString
			oConn.Open
			
			'Mark the course as deleted in the trainingpro database
			sSql = "UPDATE AgencyTypes SET " & _
				"Active = '1' " & _
				"WHERE TypeID = '" & p_iAgencyTypeId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the Agency Type.")
				exit function
				
			end if
			
		end if
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadAgencyTypeById
	' Description: Loads a AgencyType's information into the Agency Type object
	' Preconditions: ConnectionString
	' Inputs: p_iAgencyTypeId - ID of the Agency Type to load
	' Returns: Returns the Agency Type ID if successful, otherwise 0
	' -------------------------------------------------------------------------
	public function LoadAgencyTypeById(p_iAgencyTypeId)
		
		LoadAgencyTypeById = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then 
			exit function
		elseif not CheckIntParam(p_iAgencyTypeId, false, "TypeID") then
			exit function
		end if
		
		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		'set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iAgencyTypeId > 0 then
            sSql = "SELECT * FROM AgencyTypes " & _
                   "WHERE TypeID = '" & p_iAgencyTypeId & "'"
            set oRs = QueryDb(sSql)
        else
		   
        end if
		
				
		if not (oRs.BOF and oRs.EOF) then
			
			g_iAgencyTypeId = oRs("TypeID")
			g_sAgencyTypeName = oRs("Type")

            if p_iAgencyTypeId < 0 then
            g_iOwnerCompanyId = oRs("OwnerCompanyId")
			else
            g_iOwnerCompanyId = ""
            end if
			LoadAgencyTypeById = g_iAgencyTypeId

		else
		
			ReportError("Unable to load Agency Types.  Agency Type ID not found.")
			exit function
			
		end if 		
		'set oRs = nothing
		end function
	

    ' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadAgencyTypes
	' Description: Loads a AgencyType's information into the Agency Type object
	' Preconditions: ConnectionString
	' Inputs: p_iAgencyTypeId - ID of the Agency Type to load
	' Returns: Returns the Agency Type ID if successful, otherwise 0
	' -------------------------------------------------------------------------
	public function GetDbaRecordset()
	
		set GetDbaRecordset = Server.CreateObject("ADODB.Recordset")
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function	
		end if
	
		
		dim sSql 'SQL Query text
		
		sSql = "SELECT * FROM AgencyTypes " & _
		       "WHERE OwnerCompanyId = '" & g_iOwnerCompanyId & "'" & _
               "AND Active = 1"
		set GetDbaRecordset = QueryDb(sSql)
		
	end function

    ' -------------------------------------------------------------------------
	' Name: SearchAgencyTypes
	' Desc: Retrieves a collection of Agency Types that match the properties of 
	'	the current object.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the resulting recordset
	' -------------------------------------------------------------------------
	public function SearchAgencyTypes()
		
		set SearchAgencyTypes = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		dim bFirstClause 'Used to direct SQL phrasing
		dim sSql 'SQL Statement
		dim oRs 'Recordset object

		bFirstClause = true
		
		sSql = "SELECT Type " & _
			"FROM AgencyTypes AS Cs "

	
		'If Course Name search
		if g_sAgencyTypeName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Type LIKE '%" & g_sAgencyTypeName & "%'"
		end if 
		sSql = sSql & " AND OwnerCompanyId = '" & g_iOwnerCompanyId & "'"
		sSql = sSql & " ORDER BY Name"
		
		'response.write("<p>" & sSql & "<p>")

		set SearchAgencyTypes = QueryDb(sSql)

	end function
	
	' -------------------------------------------------------------------------
	' Name: SaveAgencyType
	' Desc: Save the Agency Type object in the database, and assign it the resulting
	'	ID.  
	' Preconditions: Connectionstring
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveAgencyType()
	
		SaveAgencyType = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sAgencyTypeName, false, "Type") then
			exit function
		end if 
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if g_iAgencyTypeId = "" then
		
			'This is a new agency type being added to CMS, so we'll INSERT a new
			'record
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "INSERT INTO AgencyTypes (" & _
				"Type, " & _
                "OwnerCompanyID " & _
				") VALUES (" & _
				"'" & g_sAgencyTypeName & "', " & _
                "'" & g_iOwnerCompanyId & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to create new AgencyType.")
				exit function
			
			end if
			
			'Attempt to retrieve the index of the newly created course.
			sSql = "SELECT TypeID FROM AgencyTypes " & _
				"WHERE Type LIKE '" & g_sAgencyTypeName & "' " & _
                "AND OwnerCompanyID = '" & g_iOwnerCompanyId & "' " & _
				"ORDER BY TypeID ASC"
			set oRs = oConn.Execute(sSql)
			
			if oRs.EOF then 
			
				ReportError("Failed to create new AgencyType.")
				exit function
				
			else
			
				g_iAgencyTypeId = oRs("TypeID")
				SaveAgencyType = g_iAgencyTypeId
				
			end if
			
		else 
		
			'This is an update to an existing course.
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "UPDATE AgencyTypes SET " & _
				"Type = '" & g_sAgencyTypeName & "', " & _
                "OwnerCompanyID = '" & g_iOwnerCompanyId & "' " & _
				"WHERE TypeID = '" & g_iAgencyTypeId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected = 1 then
				
				ReportError("Failed to save Agency Type information.")
				exit function
				
			end if
			
			SaveAgencyType = g_iAgencyTypeId
			
			
		end if 
		
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
	end function
	
	
end class
%>
		
		

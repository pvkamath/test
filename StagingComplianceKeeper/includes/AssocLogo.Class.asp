<%

'==============================================================================
' Class: AssocLogo
' Controls the creation, modification, and removal of AssocLogos
' Create Date: 09/13/04
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- 
' Methods:
'	- 
'==============================================================================

Class AssocLogo

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	
	'Base company properties
	dim g_iLogoId
	dim g_iCompanyId
	dim g_sFileName
	dim g_iSort
	
	
	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	
	
	
	'Retrieves the unique ID of this Coupon.  This is an identity in the DB,
	'so it cannot be set.
	public property get LogoId()
		LogoId = g_iLogoId
	end property
	
	'Company ID - what company is this logo associated with?
	public property let CompanyId(p_iCompanyId)
		g_iCompanyId = p_iCompanyId
	end property
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	
	'Stores/retrieves the file name
	public property let FileName(p_sFileName)
		g_sFileName = p_sFileName
	end property
	public property get FileName()
		FileName = g_sFileName
	end property

	'Stores/retrieves the Logo sort value
	public property let Sort(p_iSort)
		g_iSort = p_iSort
	end property
	public property get Sort()
		Sort = g_iSort
	end property

		
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'meh
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'meh
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				CheckConnectionString
	' Description:		Check to make sure the connectionstring has been set
	' Inputs:			none
	' Output:			none
	' -------------------------------------------------------------------------
	Private Sub CheckConnectionString()
		if isNull(g_sConnectionString) or (trim(g_sConnectionString) = "") then
			response.write "You must specify the value of the " & _
						   "ConnectionString Property value before proceeding."
			Response.End
		end if
	End Sub

	' -------------------------------------------------------------------------
	' Name:				CheckParam
	' Description:		Checks to make sure the passed in value is not null
	' Inputs:			p_sParam - the required parameter
	' Output:			none
	' -------------------------------------------------------------------------
	Private Sub CheckParam(p_sParam)
		
		'Response.write("X" & p_sParam & "X<br>")
		
		if ((isNull(p_sParam)) or (trim(p_sParam) = "")) then
			response.write "Processing Error!!!<br>You must specify the " & _
						   "value before proceeding."
			response.end
		end if
	End Sub
	
	' -------------------------------------------------------------------------
	' Name:				QueryDB
	' Description:		executes the query in the database
	' Inputs:			sSQL - the query to execute
	' Output:			recordset - returns a recordset of the executed query
	' -------------------------------------------------------------------------
	Private Function QueryDb(sSql)
		CheckParam(sSQL)
		CheckConnectionString	
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		'    objRS.ActiveConnection = Connect
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name:				LoadId
	' Description:		This method attempts to load the properties of a stored
	'					Logo based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iLogoId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadId(p_iLogoId)
	
		CheckConnectionString
		
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")

		sSql = "SELECT * FROM AssociationLogos " & _
			   "WHERE LogoID = '" & p_iLogoId & "'"

		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iLogoId = oRs("LogoID")
			g_iCompanyId = oRs("CompanyID")
			g_sFileName = oRs("FileName")
			g_iSort = oRs("Sort")
			
			LoadId = g_iLogoId
		
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.
			LoadId = 0
			exit function
	
		end if
			
		set oRs = nothing
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name:				SaveId
	' Description:		Save the Logo object in the database, and assign it
	'					the resulting Logo ID.
	' Pre-conditions:	ConnectionString, FileName, Sort
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns the new ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveId()
	
		'Verify our preconditions
		CheckConnectionString
		CheckParam(g_sFileName)
		CheckParam(g_iSort)

		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		'If we have a CouponId property assigned already, we're working on a
		'loaded Coupon, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iLogoId <> "" then
		
			'Now we can proceed with building our SQL statement.
			bIsNew = false
			sSql = "UPDATE AssociationLogos SET " & _
				   "CompanyId = '" & g_iCompanyId & "', " & _
				   "FileName = '" & g_sFileName & "', " & _
				   "Sort = '" & g_iSort & "' " & _
				   "WHERE LogoID = '" & g_iLogoId & "'"
				   
			oConn.Execute(sSQL)
			
			SaveId = g_iLogoId

		else

			'Build an SQL statement for a new Company.
			bIsNew = true
			
			sSql = "INSERT INTO AssociationLogos " & _
				   "(CompanyId, FileName, Sort) " & _
				   "VALUES (" & _
				   "'" & g_iCompanyId & "', " & _
				   "'" & g_sFileName & "', " & _
				   "'" & g_iSort & "' " & _
				   ")"
				   
			set oRs = oConn.Execute(sSQL)
				   
			'Now we'll try to pull out the new Coupon we just created.
			sSql = "SELECT LogoID FROM AssociationLogos " & _
				   "WHERE FileName = '" & g_sFileName & "' AND " & _
				   "Sort = '" & g_iSort & "' " & _
				   "ORDER BY LogoID DESC"
		
			set oRs = oConn.Execute(sSQL)
						
			if not (oRs.EOF and oRs.BOF) then
			
				g_iLogoId = oRs("LogoId")
				SaveId = g_iLogoId
			
			else
			
				'Our record was not created, or for some other reason we
				'couldn't pull the record back out.
				Response.Write("Error inserting new data: Update Unsuccessful")
				SaveId = 0
				exit function
			
			end if 
			
		end if
			   
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name:				DeleteId
	' Description:		Delete the Logo object from the relevant database
	'                   tables
	' Pre-conditions:	ConnectionString, LogoId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteId()
	
		'Verify our preconditions
		CheckConnectionString
		CheckParam(g_iLogoId)

		dim oConn 'DB connection
		dim oRs 'Recordset
		dim sSql 'SQL statement

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")

		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		'We'll clean out our records from the database tables.
		sSql = "DELETE FROM AssociationLogos WHERE LogoID = '" & g_iLogoId & "'"
		oConn.Execute(sSql)
		
		'Check for errors.
		if err <> 0 then
			
			Response.Write("Error: " & err.number & ", " & err.Description)
			DeleteId = 0
			exit function
			
		end if

		g_iLogoId = ""

		DeleteId = 1
	
	end function


	' -------------------------------------------------------------------------
	' Name:				SearchIds
	' Description:		Retrieves a collection of Logos that match the 
	'                   properties of the current object.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchIds()
	
		'Verify our preconditions
		CheckConnectionString
		
		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT DISTINCT(ALs.LogoID), ALs.Sort " & _
		       "FROM AssociationLogos AS ALs "
		       
		'if CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ALs.CompanyId = '" & g_iCompanyId & "'"
		end if
		       		
		'if FileName search
		if g_sFileName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ALs.FileName = '" & g_sFileName & "'"
		end if
		
		'if Sort search
		if g_iSort <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE"
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ALs.Sort = '" & g_iSort & "' "
		end if
		
		sSql = sSql & "ORDER BY Sort DESC"
		
		set SearchIds = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name:				SortId
	' Description:		Swaps the Logo position with the item near it in the
	'					sort order.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the new sort number, -1 if failed.
	' -------------------------------------------------------------------------
	public function SortId(p_sDirection)	
	
		Sort = -1 
	
		CheckConnectionString
		CheckParam(g_iLogoId)
		CheckParam(p_sDirection)
	
		dim oConn
		dim oRs
		dim sSql
		
		set oConn = server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		set oRs = server.CreateObject("ADODB.Recordset")
		set oRs.ActiveConnection = oConn
		oRs.CursorType = 3
		oRs.CursorLocation = 2
		oRs.LockType = 4
		
		sSql = "SELECT * "  & _
			   "FROM AssociationLogos " & _
			   "ORDER BY Sort DESC"
			   
		oRs.Open sSql
				
		dim iPrevItem, iCurrentItem, iPrevItemSort, iCurrentSort 'as integer
		dim	iNextItem, iCounter 'as integer
		dim iNextItemSort 'as integer
		

		do while not oRs.EOF
		
			iCurrentItem = oRs("LogoID")
		
			if clng(iCurrentItem) = clng(g_iLogoId) then
			
				if ucase(p_sDirection) = "UP" then
				'Move this item up.
				
					iCurrentSort = oRs("Sort")
				
					'This statement protects us from situations in which two
					'items have somehow been assigned the same sort number,
					'which would otherwise make them impossible to sort around
					'each other.  Two items with sort 0 effectively jams one of
					'them at the bottom of the page.
					if iPrevItemSort = iCurrentSort then
						iPrevItemSort = iPrevItemSort + 1
					end if
				
					sSql = "UPDATE AssociationLogos " & _
						   "SET Sort = '" & iPrevItemSort & "' " & _
						   "WHERE LogoID = '" & iCurrentItem & "'"
					oConn.Execute(sSql)
					
					sSql = "UPDATE AssociationLogos " & _
						   "SET Sort = '" & iCurrentSort & "' " & _
						   "WHERE LogoID = '" & iPrevItem & "'"
					oConn.Execute(sSql)
					
					Sort = iPrevItemSort
					
				elseif ucase(p_sDirection) = "DOWN" then
				'Move this item down
					
					iCurrentSort = oRs("Sort")
					
					oRs.MoveNext
					
					iNextItem = oRs("LogoID")
					iNextItemSort = oRs("Sort")
					
					oRs.MovePrevious
					
					'Same as above, fix duplicate sorts
					if iNextItemSort = iCurrentSort then
						iCurrentSort = iCurrentSort + 1
					end if
					
					sSql = "UPDATE AssociationLogos " & _
						   "SET Sort = '" & iNextItemSort & "' " & _
						   "WHERE LogoID = '" & iCurrentItem & "' "
					oConn.Execute sSql
					
					sSql = "UPDATE AssociationLogos " & _
						   "SET Sort = '" & iCurrentSort & "' " & _
						   "WHERE LogoId = '" & iNextItem & "' "
					oConn.Execute sSql
					
					Sort = iNextItemSort
					
				end if
			
			end if
			
			iPrevItem = iCurrentItem
			iPrevItemSort = oRs("Sort")
			
			oRs.MoveNext
		
		loop
			
		oRs.Close
		oConn.Close
			
		set oRs = nothing
		set oConn = nothing
			
	end function

	' -------------------------------------------------------------------------
	' Name:				Release
	' Description:		Clears all properties
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function Release()
	
		err.Clear
	
		g_iLogoId = ""
		g_sFileName = ""
		g_iSort = ""
		
		if err.number <> 0 then
			Release = 1
		else
			Release = 0
		end if
	
	end function
	
	
End Class

%>
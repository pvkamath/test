<%
'===============================================================================================================
' Class: Blog
' Purpose: Class used for retrieving and storing Blog info
' Create Date: 4/13/2005
' Current Version: 1.0
' ----------------------
' Includes Shared Class Code (SharedClassCode.asp)
'
' Properties:
'	- BlogID
'	- CommentID
'	- CommentText
'	- CreationDate
'	- ExactMatch
'	- MemberID
'	- MemberStatusID
'	- NumberOfBlogPosts
'	- PostCreationDate
'	- PostDate
'	- PostExpirationDate
'	- PostID
'	- PostStatusID
'	- PostText
'	- PostTitle
'	- Title
'
' Methods:
'	- DeleteComment(p_iCommentID)
'	- DeletePost(p_iPostID)
'	- GetBlogByBlogID(p_iBlogID)
'	- GetBlogByMember(p_iMemberID)
'	- GetPostByPostID(p_iPostID)
'	- IsBlogOwner(p_iMemberID,p_iBlogID)
'	- MemberHasBlog(p_iMemberID)
'	- SaveBlog()
'	- SaveComment()
'	- SavePost()
'	- SearchBlogs(p_sSortBy)
'	- SearchComments(p_sSortBy)
'	- SearchPosts(p_sSortBy)
'	- TotalPosts(p_iBlogID,p_iPostStatusID)
'	- TotalPostComments(p_iPostID)
'	- TotalViewerReadings(p_iBlogID,p_sType)
'	- TrackViewerReading(p_iBlogID,p_iMemberID)
'
'===============================================================================================================
Class Blog
	'Include the standard class code file
	%><!-- #include virtual="/includes/SharedClassCode.asp" --><%
	
	' GLOBAL VARIABLE DECLARATIONS =============================================================================	
	dim g_iBlogID 'as integer	
	dim g_iMemberID 'as integer
	dim g_sCreationDate 'as string
	dim g_sPostCreationDate	'as string
	dim g_sPostDate 'as string
	dim g_sPostExpirationDate 'as string	
	dim g_iPostID 'as integer
	dim g_sPostText 'as string
	dim g_sPostTitle 'as string
	dim g_sTitle 'as string
	dim g_iPostStatusID 'as integer
	dim g_sCommentText 'as string
	dim g_iNumberOfBlogPosts 'as integer
	dim g_bExactMatch 'as boolean
	dim g_iMemberStatusID 'as integer
	
	' PRIVATE METHODS ===========================================================================================

	' PUBLIC METHODS ===========================================================================================
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DeleteComment()
	' Purpose:				deletes a comment from the system based upon the comment id
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iCommentID - the id of the comment
	' Outputs:				boolean - the comment was deleted successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DeleteComment(p_iCommentID)
		dim sSQL 'as string
		dim objConn 'as object
		
		call checkIntParam(p_iCommentID,false,"Comment ID")			
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open		
		
		sSQL = "DELETE FROM BlogPostComments WHERE CommentID = '" & p_iCommentID & "' "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			DeleteComment = true
		else 
			DeleteComment = false
		end if
		
		set objConn = nothing
	End Function
		
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DeletePost()
	' Purpose:				deletes a Post from the system based upon the post id
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iPostID - the id of the post
	' Outputs:				boolean - the poat was deleted successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DeletePost(p_iPostID)
		dim sSQL 'as string
		dim objConn 'as object
		
		call checkIntParam(p_iPostID,false,"Post ID")			
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open		
		
		sSQL = "ap_DeletePost " & p_iPostID
		objConn.execute(sSQL)
		
		if err.number = 0 then
			DeletePost = true
		else 
			DeletePost = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetBlogByBlogID()
	' Purpose:				Gets a Blog from the system based upon the passed Blog id
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iBlogID - the id of the blog we want to retrieve
	' Outputs:				boolean - the blog was retrieved successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	function GetBlogByBlogID(p_iBlogID)
		dim rs 'as object
		dim sSQL 'as string
		
		call checkIntParam(p_iBlogID,false,"Member ID")
		
		sSQL = "SELECT B.*,M.NumberOfBlogPosts FROM Blogs AS B " & _
			   "INNER JOIN Members AS M ON (M.MemberID = B.MemberID) " & _
			   "WHERE B.BlogID = '" & p_iBlogID & "' "		
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			g_iBlogID = p_iBlogID
			g_iMemberID = rs("MemberID")
			g_sTitle = rs("Title")
			g_sCreationDate = rs("CreationDate")
			g_iNumberOfBlogPosts = rs("NumberOfBlogPosts")
			
			GetBlogByBlogID = true
		else
			GetBlogByBlogID = false
		end if
		
		set rs = nothing
	end function	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetBlogByMember()
	' Purpose:				Gets a Blog from the system based upon the passed member id
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iMember - the id of the member we want to retrieve
	' Outputs:				boolean - the blog  was retrieved successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	function GetBlogByMember(p_iMemberID)
		dim rs 'as object
		dim sSQL 'as string
		
		call checkIntParam(p_iMemberID,false,"Member ID")
		
		sSQL = "SELECT B.*,M.NumberOfBlogPosts FROM Blogs AS B " & _
			   "INNER JOIN Members AS M ON (M.MemberID = B.MemberID) " & _
			   "WHERE B.MemberID = '" & p_iMemberID & "' "
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			g_iBlogID = rs("BlogID")
			g_iMemberID = p_iMemberID
			g_sTitle = rs("Title")
			g_sCreationDate = rs("CreationDate")
			g_iNumberOfBlogPosts = rs("NumberOfBlogPosts")
						
			GetBlogByMember = true
		else
			GetBlogByMember = false
		end if
		
		set rs = nothing
	end function	
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetBlogByMemberName()
	' Purpose:				Gets a Blog from the system based upon the passed member name
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_sMember - the login of the member we want to retrieve
	' Outputs:				boolean - the blog  was retrieved successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	function GetBlogByMemberName(p_sMember)
	
		GetBlogByMemberName = false
	
		dim rs 'as object
		dim sSQL 'as string
		
		'Verify preconditions
		if not CheckConnectionString or _
			not checkStrParam(p_sMember, 0, "Member") then
			exit function
		end if 
		
		sSQL = "SELECT B.*,M.NumberOfBlogPosts FROM Blogs AS B " & _
			   "INNER JOIN Members AS M ON (M.MemberID = B.MemberID) " & _
			   "WHERE M.UserName = '" & p_sMember & "' "
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			g_iBlogID = rs("BlogID")
			g_iMemberID = p_iMemberID
			g_sTitle = rs("Title")
			g_sCreationDate = rs("CreationDate")
			g_iNumberOfBlogPosts = rs("NumberOfBlogPosts")
						
			GetBlogByMemberName = true
		else
			GetBlogByMemberName = false
		end if
		
		set rs = nothing
	end function	
	

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetPostByPostID()
	' Purpose:				Gets a Post from the system based upon the passed Post id
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iPostID - the id of the post we want to retrieve
	' Outputs:				boolean - the post was retrieved successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	function GetPostByPostID(p_iPostID)
		dim rs 'as object
		dim sSQL 'as string
		
		call checkIntParam(p_iPostID,false,"Post ID")
		
		sSQL = "SELECT * FROM BlogPosts AS BP " & _
		       "INNER JOIN Blogs AS B ON (BP.BlogID = B.BlogID) " & _
			   "WHERE PostID = '" & p_iPostID & "' "
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			g_iPostID = p_iPostID
			g_iBlogID = rs("BlogID")
			g_sTitle = rs("Title")
			g_sPostTitle = rs("PostTitle")
			g_sPostDate = rs("PostDate")			
			g_sPostExpirationDate = rs("ExpirationDate")					
			g_iPostStatusID = rs("StatusID")
			g_sPostText = rs("PostText")
			g_sPostCreationDate = rs("CreationDate")
			g_iMemberID = rs("MemberID")

			GetPostByPostID = true
		else
			GetPostByPostID = false
		end if
		
		set rs = nothing
	end function	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				IsBlogOwner()
	' Purpose:				checks to see if the specified member is the owner of the specified blog
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iMemberID - the id of the Member
	'						p_iBlogID - the id of the Blog
	' Outputs:				boolean - true if the Member is the owner of the Blog, false if not
	' ----------------------------------------------------------------------------------------------------------		
	Function IsBlogOwner(p_iMemberID,p_iBlogID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT count(*) FROM Blogs WHERE MemberID = '" & p_iMemberID & "' AND BlogID = '" & p_iBlogID & "' "
		set rs = QueryDB(sSQL)
		
		if cint(rs(0)) > 0 then
			IsBlogOwner = true
		else
			IsBlogOwner = false	
		end if		
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				MemberHasBlog()
	' Purpose:				checks to see if the passed member has a Blog created
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iMemberID - the id of the member
	' Outputs:				boolean - true if the Blog is created, false if not
	' ----------------------------------------------------------------------------------------------------------		
	Function MemberHasBlog(p_iMemberID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT count(*) FROM Blogs WHERE MemberID = '" & p_iMemberID & "' "
		set rs = QueryDB(sSQL)
		
		if cint(rs(0)) > 0 then
			MemberHasBlog = true
		else
			MemberHasBlog = false	
		end if		
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SaveBlog()
	' Purpose:				inserts or updates a blog in the system
	' Required Properties:  ConnectionString, g_stitle, g_iMemberID
	' Optional Properties:	g_iBlogID (required for Updates)
	' Parameters:			n/a
	' Outputs:				integer - the blog id if successful, -1 if not
	' ----------------------------------------------------------------------------------------------------------		
	function SaveBlog()
		dim sSQL 'as string
		dim objConn 'as object
		dim rs 'as object
		
		call checkIntParam(g_iMemberID,false,"Member ID")			
		call CheckStrParam(g_sTitle,false,"Title")		
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open				
		
		'Determine whether to insert or update		
		if trim(g_iBlogID) = "" then
			'Insert
			sSQL = "INSERT INTO Blogs(MemberID,Title) " & _
			       "VALUES('" & g_iMemberID & "','" & g_sTitle & "') " 
			objConn.execute(sSQL)
			
			if err.number = 0 then
				sSQL = "SELECT MAX(BlogID) FROM Blogs WHERE MemberID = '" & g_iMemberID & "' "
				set rs = QueryDB(sSQL)
				
				SaveBlog = rs(0)
			else
				SaveBlog = -1
			end if
		else
			'Update
			call checkIntParam(g_iBlogID,false,"Blog ID")			
			
			sSQL = "UPDATE Blogs " & _
			       "SET Title = '" & g_sTitle & "' " & _
				   "WHERE MemberID = '" & g_iMemberID & "' " & _
				   "AND BlogID = '" & g_iBlogID & "' "
			objConn.execute(sSQL)
			
			if err.number = 0 then
				SaveBlog = g_iBlogID
			else
				SaveBlog = -1
			end if				   
		end if
		
		set rs = nothing
		set objConn = nothing
	end function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SaveComment()
	' Purpose:				inserts a comment, to a blog post, in the system
	' Required Properties:  ConnectionString, g_sCommentText, g_iPostID, g_iMemberID
	' Optional Properties:	g_iPostID (required for Updates)
	' Parameters:			n/a
	' Outputs:				integer - the post id if successful, -1 if not
	' ----------------------------------------------------------------------------------------------------------		
	function SaveComment()
		dim sSQL 'as string
		dim objConn 'as object
		dim rs 'as object

		call CheckIntParam(g_iMemberID,false,"Member ID")
		call checkIntParam(g_iPostID,false,"Post ID")
		call CheckStrParam(g_sCommentText,false,"Comment Text")
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open				
		
		'Insert
		sSQL = "INSERT INTO BlogPostComments(PostID,MemberID,Comment) " & _
		       "VALUES('" & g_iPostID & "','" & g_iMemberID & "','" & g_sCommentText & "') " 
		objConn.execute(sSQL)
			
		if err.number = 0 then
			sSQL = "SELECT MAX(CommentID) FROM BlogPostComments WHERE PostID = '" & g_iPostID & "' AND MemberID = '" & g_iMemberID & "' "
			set rs = QueryDB(sSQL)
				
			SaveComment = rs(0)
		else
			SaveComment = -1
		end if
		
		set rs = nothing
		set objConn = nothing
	end function
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: SaveFeed()
	' Desc: Writes the RSS 2.0 file based on the most recent blog posts
	' Preconditions: ConnectionString, g_iBlogId
	' Returns: Integer 1 if successful, 0 if not
	' ----------------------------------------------------------------------------------------------------------
	public function SaveFeed()
	
		SaveFeed = 0
	
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iBlogId, 0, "Blog ID") then
			exit function
		end if
	
		dim sSql 'SQL Statement
		dim oRs 'Recordset
		dim oFs 'Filesystem object
		dim oWriteFile 'File to which we are writing
		dim sFilePath 'Path to the file
		dim sFileText 'Text of the RSS file
		dim iPostCount 'Number of posts we've printed
		dim sShortPostText 'Abbreviated post text
		
		
		'Initialize the filesystem object and file object
		set oFs = CreateObject("Scripting.FileSystemObject")
		sFilePath = Server.MapPath("/rss/blog" & g_iBlogId & ".xml")

		'dim oWriteFile 'File to be written to
		set oWriteFile = oFs.OpenTextFile(sFilePath, 2, true)
		
		sFileText = "<?xml version=""1.0""?>" & vbCrLf
		sFileText = sFileText & "<rss version=""2.0"">" & vbCrLf
		sFileText = sFileText & "<channel>" & vbCrLf
		sFileText = sFileText & "<title>RideOfYourLife.com: " & g_sTitle & "</title>" & vbCrLf
		sFileText = sFileText & "<link>http://" & application("URL") & _
			"Blogs/ViewBlog.asp?BlogId=" & g_iBlogId & "</link>" & vbCrLf
		sFileText = sFileText & "<description>RideOfYourLife.com: " & g_sTitle & "</description>" & vbCrLf
		sFileText = sFileText & "<language>en-us</language>" & vbCrLf
		sFileText = sFileText & "<pubDate>" & now() & "</pubDate>" & vbCrLf
		sFileText = sFileText & "<generator>RideOfYourLife.com</generator>" & vbCrLf
		
		g_iPostStatusId = 1
		set oRs = SearchPosts("")

		iPostCount = 0		
		do while (not oRs.EOF) and (iPostCount < 10)
		
			sFileText = sFileText & "<item>" & vbCrLf
			sFileText = sFileText & "<title>" & oRs("PostTitle") & "</title>" & vbCrLf
			sFileText = sFileText & "<link>http://" & application("sURL") & _
				"Blogs/ViewBlog.asp?BlogId=" & g_iBlogId & "</link>" & vbCrLf
				
			if len(oRs("PostText")) > 80 then
				sShortPostText = left(oRs("PostText"), instrrev(oRs("PostText"), " ", 80)) & "..."
			else
				sShortPostText = oRs("PostText")
			end if 
				
			sFileText = sFileText & "<description>" & sShortPostText & "</description>" & vbCrLf
			sFileText = sFileText & "<pubDate>" & oRs("PostDate") & "</pubDate>" & vbCrLf
			sFileText = sFileText & "</item>" & vbCrLf
			
			iPostCount = iPostCount + 1
			oRs.MoveNext
			
		loop 
		
		sFileText = sFileText & "</channel>" & vbCrLf
		sFileText = sFileText & "</rss>" & vbCrLf
		
		oWriteFile.WriteLine(sFileText)
		oWriteFile.Close
		
		set oWriteFile = nothing
		set oFs = nothing
		set oRs = nothing		
	
	end function
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SavePost()
	' Purpose:				inserts or updates a blog post in the system
	' Required Properties:  ConnectionString, g_sPostTitle, g_sPostText, g_sPostDate, g_iBlogID
	' Optional Properties:	g_iPostID (required for Updates), g_sPostExpirationDate
	' Parameters:			n/a
	' Outputs:				integer - the post id if successful, -1 if not
	' ----------------------------------------------------------------------------------------------------------		
	function SavePost()
		dim sSQL 'as string
		dim objConn 'as object
		dim rs 'as object
		
		call checkIntParam(g_iBlogID,false,"Blog ID")					
		call CheckStrParam(g_sPostTitle,false,"Post Title")		
		call CheckStrParam(g_sPostDate,false,"Post Date")		
		call CheckStrParam(g_sPostText,false,"Post Text")						
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open				
		
		'Determine whether to insert or update
		if trim(g_iPostID) = "" then
			'Insert
			
			'Expiration Date Set
			if trim(g_sPostExpirationDate) <> "" then
				sSQL = "INSERT INTO BlogPosts(BlogID,PostTitle,PostDate,ExpirationDate,StatusID,PostText) " & _
				       "VALUES('" & g_iBlogID & "','" & g_sPostTitle & "','" & g_sPostDate & "','" & g_sPostExpirationDate & "','" & g_iPostStatusID & "','" & g_sPostText & "') " 			
			else 
				'Expiration Date is set(default) to null on insert
				sSQL = "INSERT INTO BlogPosts(BlogID,PostTitle,PostDate,StatusID,PostText) " & _
				       "VALUES('" & g_iBlogID & "','" & g_sPostTitle & "','" & g_sPostDate & "','" & g_iPostStatusID & "','" & g_sPostText & "') " 
			end if
			objConn.execute(sSQL)
			
			if err.number = 0 then
				sSQL = "SELECT MAX(PostID) FROM BlogPosts WHERE BlogID = '" & g_iBlogID & "' "
				set rs = QueryDB(sSQL)
				
				SavePost = rs(0)
			else
				SavePost = -1
			end if
		else
			'Update
			call checkIntParam(g_iPostID,false,"Post ID")			
			
			sSQL = "UPDATE BlogPosts " & _
			       "SET PostTitle = '" & g_sPostTitle & "', " & _
				   "	PostDate = '" & g_sPostDate & "', "
			
			'Expiration Date Set
			if trim(g_sPostExpirationDate) <> "" then
				sSQL = sSQL & "	ExpirationDate = '" & g_sPostExpirationDate & "', " 
			else
				sSQL = sSQL & "	ExpirationDate = null, " 
			end if
			
			sSQL = sSQL & "	StatusID = '" & g_iPostStatusID & "', " & _
						  "	PostText = '" & g_sPostText & "' " & _
					      "WHERE BlogID = '" & g_iBlogID & "' " & _
				          "AND PostID = '" & g_iPostID & "' "
  
			objConn.execute(sSQL)
			
			if err.number = 0 then
				SavePost = g_iPostID
			else
				SavePost = -1
			end if				   
		end if
		
		set rs = nothing
		set objConn = nothing
	end function

	' -------------------------------------------------------------------------
	' Name: SearchBlogs
	' Purpose: Retrieves all blogs that match the specified criteria
	' Required Properties:  ConnectionString
	' Optional Properties:	g_iMemberID,g_sTitle
	' Parameters:			p_sSortBy - sort by
	' Returns: recordset - a recordset containing all blogs that matched the criteria
	' -------------------------------------------------------------------------	
	Function SearchBlogs(p_sSortBy)
		dim sSQL 'as string
		dim bFirstClause 'as boolean
		
		bFirstClause = false
		
		sSQL = "SELECT * FROM Blogs AS B " & _
			   "INNER JOIN Members AS M ON (M.MemberID = B.MemberID) "

		'If this is a MemerId search:
		if g_iMemberId <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "B.MemberId = '" & g_iMemberId & "' "		
		end if

		'If this is a MemerStatusId search:
		if g_iMemberStatusID <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "M.StatusID = '" & g_iMemberStatusID & "' "		
		end if		
		
		'If this is Title search:
		if g_sTitle <> "" then			
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			if g_bExactMatch then
				sSql = sSql & "B.Title like '" & g_sTitle & "' "						
			else
				sSql = sSql & "B.Title like '%" & g_sTitle & "%' "						
			end if			
		end if
		
		if trim(p_sSortBy) <> "" then
			sSQL = sSQL & "ORDER By " & p_sSortBy
		else 
			sSQL = sSQL & "ORDER By Title "
		end if
		
		set SearchBlogs = QueryDB(sSQL)
	End Function
	
	' -------------------------------------------------------------------------
	' Name: SearchComments
	' Purpose: Retrieves all comments that match the specified criteria
	' Required Properties:  ConnectionString
	' Optional Properties:	g_iMemberId, g_iPostID
	' Parameters:			p_sSortBy - sort by
	' Returns: recordset - a recordset containing all comments that matched the criteria
	' -------------------------------------------------------------------------	
	Function SearchComments(p_sSortBy)
		dim sSQL 'as string
		dim bFirstClause 'as boolean
		
		bFirstClause = false
		
		sSQL = "SELECT BPC.*, Us.FirstName, Us.LastName, Us.Email FROM BlogPostComments AS BPC " & _
			   "LEFT JOIN Users AS Us ON (Us.UserID = BPC.MemberID) "

		'If this is a MemerId search:
		if g_iMemberId <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPC.MemberId = '" & g_iMemberId & "' "		
		end if
		
		'If this is PostID search:
		if g_iPostID <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPC.PostID = '" & g_iPostID & "' "		
		end if
		
		if trim(p_sSortBy) <> "" then
			sSQL = sSQL & "ORDER By " & p_sSortBy
		else 
			sSQL = sSQL & "ORDER By PostDate "
		end if
		
		'Response.Write("<p>" & sSql & "</p>")
		
		set SearchComments = QueryDB(sSQL)
	End Function
	
	' -------------------------------------------------------------------------
	' Name: SearchPosts
	' Purpose: Retrieves all posts that match the specified criteria
	' Required Properties:  ConnectionString
	' Optional Properties:	g_iMemberID,g_iBlogID,g_iPostStatusID
	' Parameters:			p_sSortBy - sort by
	' Returns: recordset - a recordset containing all Posts that matched the criteria
	' -------------------------------------------------------------------------	
	Function SearchPosts(p_sSortBy)
		dim sSQL 'as string
		dim bFirstClause 'as boolean
		
		bFirstClause = false
		
		sSQL = "SELECT BP.*,B.Title AS BlogTitle, B.MemberID FROM BlogPosts AS BP " & _
			   "INNER JOIN Blogs AS B ON (B.BlogID = BP.BlogID) " & _
			   "INNER JOIN Members AS M ON (M.MemberID = B.MemberID) "

		'If this is a MemerId search:
		if g_iMemberId <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "B.MemberId = '" & g_iMemberId & "' "		
		end if
		
		'If this is a MemerStatusId search:
		if g_iMemberStatusID <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "M.StatusID = '" & g_iMemberStatusID & "' "		
		end if		
		
		'If this is a BlogID search:
		if g_iBlogId <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BP.BlogId = '" & g_iBlogId & "' "		
		end if

		'If this is a PostStatusID search:
		if g_iPostStatusID <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BP.StatusId = '" & g_iPostStatusId & "' "		
		end if

		'if true, search the title and the text fo the post
		if g_sPostText <> "" and g_sPostTitle <> "" then		
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "(BP.PostText like '%" & g_sPostText & "%' OR BP.PostTitle like '%" & g_sPostTitle & "%')"				
		else
			'If this is a PostText search:
			if g_sPostText <> "" then		
				if bFirstClause then
					sSql = sSql & " WHERE "
					bFirstClause = false
				else
					sSql = sSql & " AND "
				end if 
				sSql = sSql & "BP.PostText like '%" & g_sPostText & "%' "		
			end if
		
			'If this is a PostTitle search:
			if g_sPostTitle <> "" then		
				if bFirstClause then
					sSql = sSql & " WHERE "
					bFirstClause = false
				else
					sSql = sSql & " AND "
				end if 
				sSql = sSql & "BP.PostTitle like '%" & g_sPostTitle & "%' "		
			end if
		end if
		
		'if this is a PostDate search
		if g_sPostDate <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BP.PostDate <= '" & g_sPostDate & "' "
		end if

		'if this is a ExpirationDate search
		if g_sPostDate <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 

			sSql = sSql & "(BP.ExpirationDate >= '" & g_sPostExpirationDate & "' OR BP.ExpirationDate is null) "
		end if
		
		if trim(p_sSortBy) <> "" then
			sSQL = sSQL & "ORDER By " & p_sSortBy
		else 
			sSQL = sSQL & "ORDER By PostDate desc "
		end if
		
		'Response.Write(sSql)
		set SearchPosts = QueryDB(sSQL)
	End Function
	
	' -------------------------------------------------------------------------
	' Name: TotalPosts
	' Purpose: Retrieves the total number of posts a blog has received
	' Required Properties:  ConnectionString
	' Optional Properties:	N/A
	' Parameters:			p_iBlogID - the id of the blog
	'						p_iPostStatusID - the status of the posts that should be counted in the query (optional)
	' Returns: integer - the total number of posts
	' -------------------------------------------------------------------------	
	Function TotalPosts(p_iBlogID,p_iPostStatusID)
		dim sSQL 'as string
		dim rs 'as object
		
		call checkIntParam(p_iBlogID,false,"Blog ID")
		
		sSQL = "SELECT COUNT(*) FROM BlogPosts WHERE BlogID = '" & p_iBlogID & "' "
		
		if trim(p_iPostStatusID) <> "" then
			sSQL = sSQL & "AND StatusID = '" & p_iPostStatusID & "' "
		end if

		set rs = QueryDB(sSQL)
		
		TotalPosts = rs(0)
		
		set rs = nothing
	End Function
	
	' -------------------------------------------------------------------------
	' Name: TotalPostComments
	' Purpose: Retrieves the total number of comments a post has received
	' Required Properties:  ConnectionString
	' Optional Properties:	N/A
	' Parameters:			p_iPostID - the id of the post
	' Returns: integer - the total number of comments
	' -------------------------------------------------------------------------	
	Function TotalPostComments(p_iPostID)
		dim sSQL 'as string
		dim rs 'as object
		
		call checkIntParam(p_iPostID,false,"Post ID")
		
		sSQL = "SELECT COUNT(*) FROM BlogPostComments WHERE PostID = '" & p_iPostID & "' "
		set rs = QueryDB(sSQL)
		
		TotalPostComments = rs(0)
		
		set rs = nothing
	End Function

	' -------------------------------------------------------------------------
	' Name: TotalViewerReadings
	' Purpose: Retrieves the total number of viewer readings a blog has received
	' Required Properties:  ConnectionString
	' Optional Properties:	N/A
	' Parameters:			p_iBlogID - the id of the blog
	'						p_sType - the type of viewing (member or visitor or blank for both)
	' Returns: integer - the total number of viewer readings
	' -------------------------------------------------------------------------	
	Function TotalViewerReadings(p_iBlogID,p_sType)
		dim sSQL 'as string
		dim rs 'as object
		
		call checkIntParam(p_iBlogID,false,"Blog ID")
		
		sSQL = "SELECT COUNT(*) FROM TrackViewerReadings WHERE BlogID = '" & p_iBlogID & "' "
		
		if Ucase(p_sType) = "MEMBER" then
			sSQL = sSQL & " AND MemberID is not null"
		elseif Ucase(p_sType) = "VISITOR" then		
			sSQL = sSQL & " AND MemberID is null"		
		end if
		
		set rs = QueryDB(sSQL)
		
		TotalViewerReadings = rs(0)
		
		set rs = nothing
	End Function
	
	' -------------------------------------------------------------------------
	' Name: TrackViewerReading
	' Purpose: tracks a visit to a Member's Blog
	' Required Properties:  ConnectionString
	' Optional Properties:	N/A
	' Parameters:			p_iBlogID - the id of the Blog
	'						p_iMemberID - the id of the member who viewed the blog, or blank to indicate a visitor
	' Returns: boolean - true if successful, false if not
	' -------------------------------------------------------------------------		
	Function TrackViewerReading(p_iBlogID,p_iMemberID)
		dim sSQL 'as string
		dim objConn 'as object
		
		call checkIntParam(p_iBlogID,false,"Blog ID")			
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString
		objConn.Open		
		
		if trim(p_iMemberID) <> "" then
			sSQL = "INSERT INTO TrackViewerReadings(BlogID,MemberID) " & _
				   "VALUES ('" & p_iBlogID & "','" & p_iMemberID & "')"
		else 'visitor
			sSQL = "INSERT INTO TrackViewerReadings(BlogID) " & _
				   "VALUES ('" & p_iBlogID & "')"		
		end if
		objConn.execute(sSQL)
		
		if err.number = 0 then
			TrackViewerReading = true
		else 
			TrackViewerReading = false
		end if
		
		set objConn = nothing	
	End Function
	
	' PROPERTY DEFINITION ======================================================================================	
	Public Property Let BlogID(p_iBlogID)
		g_iBlogID = p_iBlogID
	End Property
	
	Public Property Get BlogID()
		BlogID = g_iBlogID
	End Property

	Public Property Let CommentText(p_sCommentText)
		g_sCommentText = p_sCommentText
	End Property
	
	Public Property Get CommentText()
		CommentText = g_sCommentText
	End Property
	
	Public Property Let CreationDate(p_sCreationDate)
		g_sCreationDate = p_sCreationDate
	End Property
	
	Public Property Get CreationDate()
		CreationDate = g_sCreationDate
	End Property

	Public Property Let ExactMatch(p_bExactMatch)
		g_bExactMatch = p_bExactMatch
	End Property
	
	Public Property Get ExactMatch()
		ExactMatch = g_bExactMatch
	End Property	
	
	Public Property Let MemberID(p_iMemberID)
		g_iMemberID = p_iMemberID
	End Property
	
	Public Property Get MemberID()
		MemberID = g_iMemberID
	End Property

	Public Property Let MemberStatusID(p_iMemberStatusID)
		g_iMemberStatusID = p_iMemberStatusID
	End Property
	
	Public Property Get MemberStatusID()
		MemberStatusID = g_iMemberStatusID
	End Property
	
	Public Property Let NumberOfBlogPosts(p_iNumberOfBlogPosts)
		g_iNumberOfBlogPosts = p_iNumberOfBlogPosts
	End Property
	
	Public Property Get NumberOfBlogPosts()
		NumberOfBlogPosts = g_iNumberOfBlogPosts
	End Property
	
	Public Property Let PostCreationDate(p_sPostCreationDate)
		g_sPostCreationDate = p_sPostCreationDate
	End Property
	
	Public Property Get PostCreationDate()
		PostCreationDate = g_sPostCreationDate
	End Property	
	
	Public Property Let PostDate(p_sPostDate)
		g_sPostDate = p_sPostDate
	End Property
	
	Public Property Get PostDate()
		PostDate = g_sPostDate
	End Property			
	
	Public Property Let PostExpirationDate(p_sPostExpirationDate)
		g_sPostExpirationDate = p_sPostExpirationDate
	End Property
	
	Public Property Get PostExpirationDate()
		PostExpirationDate = g_sPostExpirationDate
	End Property			
	
	Public Property Let PostID(p_iPostID)
		g_iPostID = p_iPostID
	End Property
	
	Public Property Get PostID()
		PostID = g_iPostID
	End Property

	Public Property Let PostStatusID(p_iPostStatusID)
		g_iPostStatusID = p_iPostStatusID
	End Property
	
	Public Property Get PostStatusID()
		PostStatusID = g_iPostStatusID
	End Property
	
	Public Property Let PostText(p_sPostText)
		g_sPostText = p_sPostText
	End Property
	
	Public Property Get PostText()
		PostText = g_sPostText
	End Property			
	
	Public Property Let PostTitle(p_sPostTitle)
		g_sPostTitle = p_sPostTitle
	End Property
	
	Public Property Get PostTitle()
		PostTitle = g_sPostTitle
	End Property				
	
	Public Property Let Title(p_sTitle)
		g_sTitle = p_sTitle
	End Property
	
	Public Property Get Title()
		Title = g_sTitle
	End Property			
End Class
%>
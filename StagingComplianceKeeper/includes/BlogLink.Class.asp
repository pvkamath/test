<%
'==============================================================================
' Class: BlogLink
' Controls the creation, modification, and removal of favorite links for a 
'	blog
' Create Date: 10/31/06
' Author: Mark Silvia
' ---------------------
' Include Shared Class Code (SharedClassCode.asp)
'
' Properties
'	- BlogId
'	- LinkId
'	- PostDate
'	- LinkTitle
'	- LinkUrl
'	- LinkDesc
'	- Deleted
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'
' Methods
'	- LoadLinkById
'	- SaveLink
'	- SearchLinks
'
'==============================================================================

class BlogLink

	'Include the standard class code file
	%><!-- #include virtual="/includes/SharedClassCode.asp" --><%
	
	
	'GLOBAL VARIABLE DECLARATIONS =============================================
	
	dim g_iBlogId
	dim g_iLinkId
	dim g_dPostDate
	dim g_sLinkTitle
	dim g_sLinkUrl
	dim g_sLinkDesc
	dim g_bDeleted
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Unique ID for this blog
	public property get BlogId()
		BlogId = g_iBlogId
	end property
	public property let BlogId(p_iBlogId)
		if isnumeric(p_iBlogId) then
			g_iBlogId = cint(p_iBlogId)
		elseif p_iBlogId = "" then
			g_iBlogId = ""
		else
			ReportError("Invalid BlogId value.  Integer required.")
		end if
	end property
	
	
	'Unique link ID
	public property get LinkId()
		LinkId = g_iLinkId
	end property
	public property let LinkId(p_iLinkId)
		if isnumeric(p_iLinkId) then
			g_iLinkId = cint(p_iLinkId)
		elseif p_iLinkId = "" then
			g_iLinkId = ""
		else
			ReportError("Invalid LinkId value.  Integer required.")
		end if
	end property
	
	
	'Date and time of link creation
	public property get PostDate()
		PostDate = g_dPostDate
	end property
	public property let PostDate(p_dPostDate)
		if isdate(p_dPostDate) then 
			g_dPostDate = cdate(p_dPostDate)
		elseif p_dPostDate = "" then
			g_dPostDate = "" 
		else
			ReportError("Invalid PostDate value.  Date required.")
		end if
	end property
	
	
	'Title/name of the link
	public property get LinkTitle()
		LinkTitle = g_sLinkTitle
	end property
	public property let LinkTitle(p_sLinkTitle)
		g_sLinkTitle = left(p_sLinkTitle, 150)
	end property
	
	
	'URL of the link
	public property get LinkUrl()
		LinkUrl = g_sLinkUrl
	end property
	public property let LinkUrl(p_sLinkUrl)
		g_sLinkUrl = left(p_sLinkUrl, 255)
	end property
	
	
	'Text description of the link
	public property get LinkDesc()
		LinkDesc = g_sLinkDesc
	end property
	public property let LinkDesc(p_sLinkDesc)
		g_sLinkDesc = p_sLinkDesc
	end property
	
	
	'Boolean value determining whether or not the blog link has been marked as
	'deleted
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = false
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name: Class_Initialize
	' Desc: Sub that runs when the object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: Class_Terminate
	' Desc: Sub that runs when the object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: FormattedLink
	' Desc: Returns a blog link with "http://" attached if needed, so that the
	'	text can be directly inserted in an anchor tag.
	' Returns: Text link, or blank string.
	' -------------------------------------------------------------------------
	public function FormattedLink()
	
		if left(g_sLinkUrl, 7) <> "http://" then
			FormattedLink = "http://" & g_sLinkUrl
		else
			FormattedLink = g_sLinkUrl
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LoadLinkById
	' Desc: Load a blog link object based on the passed LinkId
	' Preconditions: Connectionstring must be set
	' Inputs: p_iLinkId - ID of the link to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadLinkById(p_iLinkId)
	
		LoadLinkById = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iLinkId, 0, "LinkIdId") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM BlogLinks " & _
			"WHERE LinkId = '" & p_iLinkId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a record back, so load the object properties
			g_iBlogId = oRs("BlogId")
			g_iLinkId = oRs("LinkId")
			g_dPostDate = oRs("PostDate")
			g_sLinkTitle = oRs("LinkTitle")
			g_sLinkUrl = oRs("LinkUrl")
			g_sLinkDesc = oRs("LinkDesc")
			
			LoadLinkById = g_iLinkId
			
		else
		
			ReportError("Unable to load the passed Link ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveLink
	' Desc: Saves a blog link's properties to the database
	' Preconditions: Connectionstring, BlogId
	' Returns: If successful, returns saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveLink()
	
		SaveLink = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iBlogId, 0, "BlogId") then
			exit function
		end if
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement 
		dim lRecordsAffected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a LinkId property assigned already, we're working on a 
		'loaded Link, so we'll do an UPDATE.  Otherwise we do an INSERT.
		if g_iLinkId <> "" then
		
			sSql = "UPDATE BlogLinks SET " & _
				"BlogId = '" & g_iBlogId & "', " & _
				"PostDate = '" & g_dPostDate & "', " & _
				"LinkTitle = '" & g_sLinkTitle & "', " & _
				"LinkUrl = '" & g_sLinkUrl & "', " & _
				"LinkDesc = '" & g_sLinkDesc & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE LinkId = '" & g_iLinkId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				SaveLink = g_iLinkId
			else
				ReportError("Unable to save existing Link.")
				exit function
			end if
			
		else
		
			sSql = "INSERT INTO BlogLinks (" & _
				"BlogId, " & _
				"PostDate, " & _
				"LinkTitle, " & _
				"LinkUrl, " & _
				"LinkDesc, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_iBlogId & "', " & _
				"'" & g_dPostDate & "', " & _
				"'" & g_sLinkTitle & "', " & _
				"'" & g_sLinkUrl & "', " & _
				"'" & g_sLinkDesc & "', " & _
				"'" & abs(cint(g_bDeleted)) & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new blog link
			sSql = "SELECT LinkId FROM BlogLinks " & _
				"WHERE BlogId = '" & g_iBlogId & "', " & _
				"AND LinkTitle = '" & g_sLinkTitle & "' " & _
				"ORDER BY LinkId DESC"
			set oRs = oConn.Execute(sSql)
			
			'Make sure we actually found something
			if not (oRs.EOF and oRs.BOF) then
				g_iPostId = oRs("PostId")
				SavePost = g_iPostId
			else
				'The new record could not be retrieved
				ReportError("Failed to save new Link.")
				exit function
			end if
			
		end if
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchLinks
	' Desc: Retrieves a collection of LinkIds that match the properties of the
	'	current object
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchLinks(p_sSortBy)
	
		set SearchLinks = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause
		dim sSql
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT BLs.LinkId, Bs.Title AS BlogTitle " & _
			"FROM BlogLinks AS BLs " & _
			"LEFT JOIN Blogs AS Bs " & _
			"ON (Bs.BlogId = BLs.BlogId) "
			
		'If this is a BlogId search:
		if g_iBlogId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BLs.BlogId = '" & g_iBlogId & "'"
		end if 
			
		'If this is a LinkTitle search:
		if g_sLinkTitle <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BLs.LinkTitle LIKE '%" & g_sLinkTitle & "%'"
		end if 

		'If this is a LinkUrl search:
		if g_sLinkUrl <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BLs.LinkUrl LIKE '%" & g_sLinkUrl & "%'"
		end if 		
		
		'If this is a LinkDesc search:
		if g_sLinkDesc <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BLs.LinkDesc LIKE '%" & g_sLinkDesc & "%'"
		end if 
		
		'If this is a Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BLs.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if 
		
		if trim(p_sSortBy) <> "" then 
			sSql = sSql & " ORDER BY " & safe(p_sSortBy)
		else
			sSql = sSql & " ORDER BY BLs.LinkTitle DESC"
		end if 
		
		'Response.Write("<p>" & sSql & "</p>")
		
		set SearchLinks = QueryDb(sSql)
		
	end function
	
end class
%>
<%
'==============================================================================
' Class: BlogPost
' Controls the creation, modification, and removal of a post to a blog
' Create Date: 3/30/2005
' Author: Mark Silvia
' -------------------
' Includes Shared Class Code (SharedClassCode.asp)
'
' Properties
'	- BlogId
'	- PostId
'	- PostDate
'	- PostTitle
'	- PostText
'	- Deleted
'	- SearchPostDateStart
'	- SearchPostDateEnd
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'
' Methods
'	- LoadPostById
'	- SavePost
'	- SearchPosts
'
'==============================================================================

class BlogPost


	' Include the standard class code file
	%><!-- #include virtual="/includes/SharedClassCode.asp" --><%


	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	dim g_iBlogId
	dim g_iPostId
	dim g_dPostDate
	dim g_sPostTitle
	dim g_sPostText
	dim g_bDeleted
	dim g_dSearchPostDateStart
	dim g_dSearchPostDateEnd

	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Unique ID for this blog
	public property get BlogId()
		BlogId = g_iBlogId
	end property
	public property let BlogId(p_iBlogId)
		if isnumeric(p_iBlogId) then
			g_iBlogId = cint(p_iBlogId)
		elseif p_iBlogId = "" then
			g_iBlogId = ""
		else
			ReportError("Invalid BlogId value.  Integer required.")
		end if
	end property
	
	
	'Unique Post ID
	public property get PostId()
		PostId = g_iPostId
	end property
	public property let PostId(p_iPostId)
		if isnumeric(p_iPostId) then
			g_iPostId = cint(p_iPostId)
		elseif p_iPostId = "" then
			g_iPostId = ""
		else
			ReportError("Invalid PostId value.  Integer required.")
		end if
	end property
	
	
	'Date and time of post
	public property get PostDate
		PostDate = g_dPostDate
	end property
	public property let PostDate(p_dPostDate)
		if isdate(p_dPostDate) then
			g_dPostDate = p_dPostDate
		elseif p_dPostDate = "" then
			g_dPostDate = ""
		else
			ReportError("Invalid PostDate value.  Date required.")
		end if
	end property	
	
	
	'Title of the post
	public property get PostTitle()
		PostTitle = g_sPostTitle
	end property
	public property let PostTitle(p_sPostTitle)
		g_sPostTitle = left(p_sPostTitle, 100)
	end property
	
	
	'Body content of the post
	public property get PostText()
		PostText = g_sPostText
	end property
	public property let PostText(p_sPostText)
		g_sPostText = p_sPostText
	end property
	
	
	'Boolean value determining whether or not the Blog has been marked as
	'deleted.
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = false
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property


	'Earliest date and time of post allowed in SearchPosts
	public property get SearchPostDateStart
		SearchPostDateStart = g_dSearchPostDateStart
	end property
	public property let SearchPostDateStart(p_dSearchPostDateStart)
		if isdate(p_dSearchPostDateStart) then
			g_dSearchPostDateStart = p_dSearchPostDateStart
		elseif p_dSearchPostDateStart = "" then
			g_dSearchPostDateStart = ""
		else
			ReportError("Invalid SearchPostDateStart value.  Date required.")
		end if
	end property	
	

	'Latest date and time of post allowed in SearchPosts
	public property get SearchPostDateEnd
		SearchPostDateEnd = g_dSearchPostDateEnd
	end property
	public property let SearchPostDateEnd(p_dSearchPostDateStart)
		if isdate(p_dSearchPostDateStart) then
			g_dSearchPostDateStart = p_dSearchPostDateStart
		elseif p_dSearchPostDateStart = "" then
			g_dSearchPostDateStart = ""
		else
			ReportError("Invalid SearchPostDateStart value.  Date required.")
		end if
	end property	

	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub


	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadPostById
	' Desc: Load a blog post object based on the passed PostId
	' Preconditions: ConnectionString must be set
	' Inputs: p_iPostId - Id of the Post to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadPostById(p_iPostId)
	
		LoadPostById = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iPostId, 0, "PostID") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM BlogPosts " & _
			"WHERE PostId = '" & p_iPostId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a record back, so load the app properties
			g_iBlogId = oRs("BlogId")
			g_iPostId = oRs("PostId")
			g_dPostDate = oRs("PostDate")
			g_sPostTitle = oRs("PostTitle")
			g_sPostText = oRs("PostText")
			g_bDeleted = oRs("Deleted")
			
			LoadPostById = g_iPostId
			
		else
			
			ReportError("Unable to load the passed Post ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SavePost
	' Desc: Saves a blog post object's properties to the database.
	' Preconditions: Connectionstring, BlogId
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SavePost()
	
		SavePost = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iBlogId, 0, "BlogID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a PostId property assigned already, we're working on a
		'loaded Post, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iPostId <> "" then

			bIsNew = false
			sSql = "UPDATE BlogPosts SET " & _
				"BlogId = '" & g_iBlogId & "', " & _
				"PostDate = '" & g_dPostDate & "', " & _
				"PostTitle = '" & g_sPostTitle & "', " & _
				"PostText = '" & g_sPostText & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE PostId = '" & g_iPostId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				
				SavePost = g_iPostId
				
			else
				
				ReportError("Unable to save existing Post.")
				exit function
				
			end if
			
		else
		
			bIsNew = true
			sSql = "INSERT INTO Blogs (" & _
				"BlogId, " & _
				"PostDate, " & _
				"PostTitle, " & _
				"PostText, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_iBlogId & "', " & _
				"'" & g_dPostDate & "', " & _
				"'" & g_sPostTitle & "', " & _
				"'" & g_sPostText & "', " & _
				"'" & abs(cint(g_bDeleted)) & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new blog
			sSql = "SELECT PostId FROM BlogPosts " & _
				"WHERE BlogId = '" & g_iBlogId & "', " & _
				"AND PostTitle = '" & g_sPostTitle & "', " & _
				"AND PostDate = '" & g_dPostDate & "' " & _
				"ORDER BY PostId DESC" 
			set oRs = oConn.Execute(sSql)
			
			'Make sure we actually found something
			if not (oRs.EOF and oRs.BOF) then
			
				g_iPostId = oRs("PostId")
				SavePost = g_iPostId
				
			else
			
				'The record could not be retrieved.
				ReportError("Failed to save new Post.")
				exit function
				
			end if 
			
		end if 
		
		oConn.Close 
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchPosts
	' Desc: Retrieves a collection of PostIds that match the properties of the
	'	current object.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchPosts()
	
		set SearchPosts = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 
		dim sSql 
		dim oRs
		
		bFirstClause = false
		
		sSql = "SELECT DISTINCT(BPs.PostId) " & _
			"FROM BlogPosts AS BPs "
			
		'If this is a BlogId search:
		if g_iBlogId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPs.BlogId = '" & g_iBlogId & "'"
		end if 
		
		'If this is a PostTitle search:
		if g_sPostTitle <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPs.PostTitle LIKE '%" & g_sPostTitle & "%'"
		end if 
		
		'If this is a PostText search:
		if g_sPostText <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPs.PostText LIKE '%" & g_sPostText & "%'"
		end if 
		
		'If this is a SearchPostDateStart search:
		if g_dSearchPostDateStart <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPs.PostDate > '" & g_dSearchPostDateStart & "'"
		end if 
		
		'If this is a SearchPostDateEnd search:
		if g_dSearchPostDateEnd <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "BPs.PostDate < '" & g_dSearchPostDateEnd & "'"
		end if 
		
		'If this is a Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "Bs.Deleted = '" & abs(cint(g_bDeleted)) & "%'"
		end if 
		
	
		sSql = sSql & " ORDER BY BPs.PostDate DESC"
		'response.write("<p>" & sSql & "<p>")
		
		set SearchPosts = QueryDb(sSql)
		
	end function
	
end class
%>
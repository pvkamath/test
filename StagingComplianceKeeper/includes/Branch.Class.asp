<%
'==============================================================================
' Class: Branch
' Controls the creation, modification, and removal of Branches
' Create Date: 04/2005
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- VocalErrors
'	- ConnectionString
'	- TpConnectionString
'	- BOF
'	- EOF
' Branch Properties:
'	- BranchId
'	- BranchNum
'	- Name
'	- LegalName
'	- CompanyId
'	- CompanyL2Id
'	- CompanyL3Id
'	- BranchManager
'	- FormationDate
'	- TerminationDate
'	- InactiveDate
'	- Address
'	- Address2
'	- City
'	- StateId
'	- Zipcode
'	- ZipcodeExt
'	- Phone
'	- PhoneExt
'	- Fax
'	- Email
'	- MailingAddress
'	- MailingAddress2
'	- MailingCity
'	- MailingStateId
'	- MailingZipcode
'	- MailingZipcodeExt
'	- MailingCountryId
'	- CustField1
'	- CustField2
'	- CustField3
'	- CustField4
'	- CustField5
' Methods:
'	- LoadBranches
'   - GetBranches
'   - GetBranchSAs
'	- GetNextBranch
'   - GetPrevBranch
'	- GetSpecificBranch
'	- AddUpdateBranch
'	- DeleteBranch
'==============================================================================

class Branch

	' GLOBAL VARIABLE DECLARATIONS ============================================

	dim g_oRs
	dim g_bActive
	dim g_iBranchId
	dim g_sBranchNum
	dim g_iCompanyId
	dim g_iCompanyL2Id
	dim g_iCompanyL3Id
	dim g_sBranchName
	dim g_sLegalName
	dim g_sBranchManager
	dim g_dFormationDate
	dim g_dTerminationDate
	dim g_dInactiveDate
	dim g_sAddress
	dim g_sAddress2
	dim g_sCity
	dim g_iStateId
	dim g_sZipcode
	dim g_sZipcodeExt
	dim g_iCountryId
	dim g_sPhone
	dim g_sPhoneExt
	dim g_sFax
	dim g_sEmail
	dim g_sMailingAddress
	dim g_sMailingAddress2
	dim g_sMailingCity
	dim g_iMailingStateId
	dim g_sMailingZipcode
	dim g_sMailingZipcodeExt
	dim g_iMailingCountryId
	dim g_sCustField1
	dim g_sCustField2
	dim g_sCustField3
	dim g_sCustField4
	dim g_sCustField5
	dim g_sEntityLicNum
	dim g_sFhaBranchId
	dim g_sVaBranchId
	dim g_bBOF
	dim g_bEOF
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	dim g_sSearchNameStart
	dim g_sSearchInName
	dim g_sSearchStateIdList
	dim g_sSearchLicStateIdList
	dim g_sSearchBranchIdList
	dim g_sSearchCompanyL2IdList
	dim g_sSearchCompanyL3IdList
	dim g_iLicenseStatusId
	dim g_sLicenseStatusIdList
	dim g_sLicenseNumber
	dim g_dLicenseExpDateFrom
	dim g_dLicenseExpDateTo
	dim g_iAppDeadline
	dim g_bInactive
	dim g_iSearchCompanyL2Id
	dim g_iSearchCompanyL3Id
	dim g_sNMLSNumber
	
	
	' PROPERTIES ==============================================================
		
	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	


	'The Trainingpro connection string
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
		
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid value.  Boolean required.")
		end if 
	end property
	
	
	'Active boolean.
	public property get Active()
		Active = g_bActive
	end property
	public property let Active(p_bActive)
		if isnumeric(p_bActive) then
			g_bActive = cbool(p_bActive)
		elseif p_bActive = "" then
			g_bActive = false
		else
			ReportError("Invalid Active value.  Boolean required.")
		end if
	end property
	
	
	'Whether or not the company is currently inactive.
	public property get Inactive()
		Inactive = g_bInactive
	end property
	public property let Inactive(p_bInactive)
		if isnumeric(p_bInactive) then
			g_bInactive = cbool(p_bInactive)
		elseif p_bInactive = "" then
			g_bInactive = ""
		else
			ReportError("Invalid Inactive value.  Boolean required.")
		end if 	
	end property


	'Branch Id.  Read-only.
	public property get BranchId()
		BranchId = g_iBranchId
	end property
	
	
	'Branch Number
	public property get BranchNum()
		BranchNum = g_sBranchNum
	end property
	public property let BranchNum(p_sBranchNum)
		g_sBranchNum = left(p_sBranchNum, 20)
	end property
	

	'Branch parent Company Id.
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = p_iCompanyId
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'Branch assigned CompanyL2 Id.
	public property get CompanyL2Id()
		CompanyL2Id = g_iCompanyL2Id
	end property
	public property let CompanyL2Id(p_iCompanyL2Id)
		if isnumeric(p_iCompanyL2Id) then
			g_iCompanyL2Id = clng(p_iCompanyL2Id)
			g_iCompanyL3Id = ""
		elseif p_iCompanyL2Id = "" then
			g_iCompanyL2Id = ""
		else
			ReportError("Invalid CompanyL2Id value.  Integer required.")
		end if
	end property


	'Branch assigned CompanyL3 Id.
	public property get CompanyL3Id()
		CompanyL3Id = g_iCompanyL3Id
	end property
	public property let CompanyL3Id(p_iCompanyL3Id)
		if isnumeric(p_iCompanyL3Id) then
			g_iCompanyL3Id = clng(p_iCompanyL3Id)
			g_iCompanyL2Id = ""
		elseif p_iCompanyL3Id = "" then
			g_iCompanyL3Id = ""
		else
			ReportError("Invalid CompanyL3Id value.  Integer required.")
		end if
	end property
	
	
	'Name of the Branch, if the branch is named and not numbered
	public property get BranchName()
		BranchName = g_sBranchName
	end property
	public property let BranchName(p_sBranchName)
		g_sBranchName = left(p_sBranchName, 50)
	end property
	
	
	'Legal name of the company
	public property let LegalName(p_sLegalName)
		g_sLegalName = left(p_sLegalName, 100)
	end property
	public property get LegalName()
		LegalName = g_sLegalName
	end property
	

	'Name of the Branch, if the branch is named and not numbered
	public property get Name()
		Name = g_sBranchName
	end property
	public property let Name(p_sBranchName)
		g_sBranchName = left(p_sBranchName, 50)
	end property

	
	'Name of the Branch Manager
	public property get BranchManager()
		BranchManager = g_sBranchManager
	end property
	public property let BranchManager(p_sBranchManager)
		g_sBranchManager = p_sBranchManager
	end property
	
	
	'Branch main address
	public property get Address()
		Address = g_sAddress
	end property
	public property let Address(p_sAddress)
		g_sAddress = left(p_sAddress, 100)
	end property
	
	
	'Branch main address, second line
	public property get Address2()
		Address2 = g_sAddress2
	end property
	public property let Address2(p_sAddress2)
		g_sAddress2 = p_sAddress2
	end property


	'Branch main address, city
	public property get City()
		City = g_sCity
	end property
	public property let City(p_sCity)
		g_sCity = p_sCity
	end property
	
	
	'Branch main address, state ID
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then
			g_iStateId = p_iStateId
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Branch main address, zipcode
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	public property let Zipcode(p_sZipcode)
		g_sZipcode = left(p_sZipcode, 15)
	end property
	

	'Branch main address, zipcode ext
	public property get ZipcodeExt()
		ZipcodeExt = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		g_sZipcodeExt = left(p_sZipcodeExt, 4)
	end property

	
	'Phone Number
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = left(p_sPhone, 15)
	end property
	
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = left(p_sPhoneExt, 10)
	end property
	
	
	'Date the branch was formed
	public property get FormationDate()
		FormationDate = g_dFormationDate
	end property
	public property let FormationDate(p_dFormationDate)
		if isdate(p_dFormationDate) then
			g_dFormationDate = cdate(p_dFormationDate)
		elseif p_dFormationDate = "" then
			g_dFormationDate = ""
		else
			ReportError("Invalid FormationDate value.  Date required.")
		end if
	end property
	
	
	'Date the branch was closed
	public property get TerminationDate()
		TerminationDate = g_dTerminationDate
	end property
	public property let TerminationDate(p_dTerminationDate)
		if isdate(p_dTerminationDate) then
			g_dFormationDate = cdate(p_dFormationDate)
		elseif p_dFormationDate = "" then
			g_dFormationDate = ""
		else
			ReportError("Invalid TerminationDate value.  Date required.")
		end if
	end property
	
	'Date the branch was inactive
	public property get InactiveDate()
		InactiveDate = g_dInactiveDate
	end property
	public property let InactiveDate(p_dInactiveDate)
		if isdate(p_dInactiveDate) then
			g_dInactiveDate = cdate(p_dInactiveDate)
		elseif p_dInactiveDate = "" then
			g_dInactiveDate = ""
		else
			ReportError("Invalid InactiveDate value.  Date required.")
		end if
	end property
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = p_sFax
	end property
	
	
	'Entity License Number
	public property get EntityLicNum()
		EntityLicNum = g_sEntityLicNum
	end property
	public property let EntityLicNum(p_sEntityLicNum)
		g_sEntityLicNum = p_sEntityLicNum
	end property
	
	
	'FHA Branch ID
	public property get FhaBranchId()
		FhaBranchId = g_sFhaBranchId
	end property
	public property let FhaBranchId(p_sFhaBranchId)
		g_sFhaBranchId = p_sFhaBranchId
	end property
	
	
	'VA Branch ID
	public property get VABranchId()
		VABranchId = g_sVABranchId
	end property
	public property let VABranchId(p_sVABranchId)
		g_sVABranchId = p_sVABranchId
	end property
	
	
	
	'Mailing Address line 1
	public property get MailingAddress()
		MailingAddress = g_sMailingAddress
	end property
	public property let MailingAddress(p_sMailingAddress)
		g_sMailingAddress = p_sMailingAddress
	end property 
	
	
	'Mailing Address line 2
	public property get MailingAddress2()
		MailingAddress2 = g_sMailingAddress2
	end property
	public property let MailingAddress2(p_sMailingAddress2)
		g_sMailingAddress2 = p_sMailingAddress2
	end property
	
	
	'Mailing address city
	public property get MailingCity()
		MailingCity = g_sMailingCity
	end property
	public property let MailingCity(p_sMailingCity)
		g_sMailingCity = p_sMailingCity
	end property
	
	
	'Mailing address state id
	public property get MailingStateId()
		MailingStateId = g_iMailingStateId
	end property
	public property let MailingStateId(p_iMailingStateId)
		if isnumeric(p_iMailingStateId) then
			g_iMailingStateId = abs(cint(p_iMailingStateId))
		else
			ReportError("Invalid MailingStateId value.  Integer required.")
		end if 
	end property
	
	
	'Mailing address zipcode
	public property get MailingZipcode()
		MailingZipcode = g_sMailingZipcode
	end property 
	public property let MailingZipcode(p_sMailingZipcode)
		g_sMailingZipcode = p_sMailingZipcode
	end property


	'Optional zipcode extension
	public property get MailingZipcodeExt()
		MailingZipcode = g_sMailingZipcodeExt
	end property
	public property let MailingZipcodeExt(p_sMailingZipcodeExt)
		g_sMailingZipcodeExt = left(p_sMailingZipcodeExt, 4)
	end property
	
	
	'Mailing Country ID Assigned to this address
	public property get MailingCountryId
		MailingCountryId = g_iMailingCountryId
	end property
	public property let MailingCountryId(p_iMailingCountryId)
		if isnumeric(p_iMailingCountryId) then
			g_iMailingCountryId = p_iMailingCountryId
		elseif p_iMailingCountryId = "" then
			g_iMailingCountryId = ""
		else
			ReportError("Invalid MailingCountryId value.  Integer required.")
		end if 
	end property
	
	
	'Numbered custom field
	public property let CustField1(p_sCustField1)
		g_sCustField1 = left(p_sCustField1, 150)
	end property
	public property get CustField1()
		CustField1 = g_sCustField1
	end property
	
	
	'Numbered custom field
	public property let CustField2(p_sCustField2)
		g_sCustField2 = left(p_sCustField2, 150)
	end property
	public property get CustField2()
		CustField2 = g_sCustField2
	end property
	
	
	'Numbered custom field
	public property let CustField3(p_sCustField3)
		g_sCustField3 = left(p_sCustField3, 150)
	end property
	public property get CustField3()
		CustField3 = g_sCustField3
	end property
	
	
	'Numbered custom field
	public property let CustField4(p_sCustField4)
		g_sCustField4 = left(p_sCustField4, 150)
	end property
	public property get CustField4()
		CustField4 = g_sCustField4
	end property
	
	
	'Numbered custom field
	public property let CustField5(p_sCustField5)
		g_sCustField5 = left(p_sCustField5, 150)
	end property
	public property get CustField5()
		CustField5 = g_sCustField5
	end property

    public property let NMLSNumber(p_sNMLSNumber)
        g_sNMLSNumber = p_sNMLSNumber
    end property
    public property get NMLSNumber()
        NMLSNumber = g_sNMLSNumber
    end property
	
	'BOF/EOF: read-only and notify the user of the recordset's BOF/EOF status
	public property get EOF()
		EOF = g_bEOF
	end property
	
	public property get BOF()
		BOF = g_bBOF
	end property
	
	
	'Search Branch Name Start property - used to search for branch names starting
	'with this value, rather than names simply containing the value.
	public property get SearchNameStart()
		SearchNameStart = g_sSearchNameStart
	end property
	public property let SearchNameStart(p_sSearchNameStart)
		g_sSearchNameStart = p_sSearchNameStart
	end property
	
	
	'SearchInName - used to search for a substring anywhere within the branch
	'name.
	public property get SearchInName()
		SearchInName = g_sSearchInName
	end property
	public property let SearchInName(p_sSearchInName)
		g_sSearchInName = p_sSearchInName
	end property	
	
	
	'List of State IDs to search within.  Used to find associates from multiple
	'states rather than just one.
	public property get SearchStateIdList()
		SearchStateIdList = g_sSearchStateIdList
	end property
	public property let SearchStateIdList(p_sSearchStateIdList)
		g_sSearchStateIdList = p_sSearchStateIdList
	end property
	
	
	'List of State IDs to search within.  Used to find licenses from multiple
	'states rather than just one.
	public property get SearchLicStateIdList()
		SearchLicStateIdList = g_sSearchLicStateIdList
	end property
	public property let SearchLicStateIdList(p_sSearchLicStateIdList)
		g_sSearchLicStateIdList = p_sSearchLicStateIdList
	end property
	
	
	'List of Branch IDs to search within.  Used to find associates from
	'multiple branches rather than just one.
	public property get SearchBranchIdList()
		SearchBranchIdList = g_sSearchBranchIdList
	end property
	public property let SearchBranchIdList(p_sSearchBranchIdList)
		g_sSearchBranchIdList = p_sSearchBranchIdList
	end property
	
	
	'List of CompanyL2 IDs to search within.  Used to find associates from
	'multiple CL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property
	

	'List of CompanyL3 IDs to search within.  Used to find associates from
	'multiple CL3s rather than just one.
	public property get SearchCompanyL3IdList()
		SearchCompanyL3IdList = g_sSearchCompanyL3IdList
	end property
	public property let SearchCompanyL3IdList(p_sSearchCompanyL3IdList)
		g_sSearchCompanyL3IdList = p_sSearchCompanyL3IdList
	end property	


	'Search License Status 
	public property get SearchLicenseStatusId()
		SearchLicenseStatusId = g_iLicenseStatusId
	end property
	public property let SearchLicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = cint(p_iLicenseStatusId)
		elseif isnull(p_iLicenseStatusId) or p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid SearchLicenseStatusId value. " & _
				"Integer required.")
		end if
	end property
	
	
	'List of License Status IDs to search within.  Used to find licenses
	'matching any of several status types.
	public property get SearchLicenseStatusIdList()
		SearchLicenseStatusIdList = g_sLicenseStatusIdList
	end property
	public property let SearchLicenseStatusIdList(p_sLicenseStatusIdList)
		g_sLicenseStatusIdList = p_sLicenseStatusIdList
	end property
	
	
	'Search License Number
	public property get SearchLicenseNumber()
		SearchLicenseNumber = g_sLicenseNumber
	end property
	public property let SearchLicenseNumber(p_sLicenseNumber)
		g_sLicenseNumber = p_sLicenseNumber
	end property	
	
	
	'Search License Expiration from
	public property get SearchLicenseExpDateFrom()
		SearchLicenseExpDateFrom = g_dLicenseExpDateFrom
	end property
	public property let SearchLicenseExpDateFrom(p_dLicenseExpDateFrom)
		if isdate(p_dLicenseExpDateFrom) then
			g_dLicenseExpDateFrom = cdate(p_dLicenseExpDateFrom)
		elseif p_dLicenseExpDateFrom = "" then
			g_dLicenseExpDateFrom = ""
		else
			ReportError("Invalid SearchLicenseExpDateFrom value.  " & _
				"Date required.")
		end if
	end property


	'Search License Expiration from
	public property get SearchLicenseExpDateTo()
		SearchLicenseExpDateTo = g_dLicenseExpDateTo
	end property
	public property let SearchLicenseExpDateTo(p_dLicenseExpDateTo)
		if isdate(p_dLicenseExpDateTo) then
			g_dLicenseExpDateTo = cdate(p_dLicenseExpDateTo)
		elseif p_dLicenseExpDateTo = "" then
			g_dLicenseExpDateTo = ""
		else
			ReportError("Invalid SearchLicenseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	
	'How are we searching for application deadlines?
	public property get SearchAppDeadline()
		SearchAppDeadline = g_iAppDeadline
	end property
	public property let SearchAppDeadline(p_iAppDeadline)
		if isnumeric(p_iAppDeadline) then
			g_iAppDeadline = clng(p_iAppDeadline)
		elseif p_iAppDeadline = "" or isnull(p_iAppDeadline)then
			g_iAppDeadline = ""
		else
			ReportError("Invalid SearchAppDeadline value.  Integer required.")
		end if 
	end property


	'Search CompanyL2 Id - used to search for specific NULLS
	public property get SearchCompanyL2Id()
		SearchCompanyL2Id = g_iSearchCompanyL2Id
	end property
	public property let SearchCompanyL2Id(p_iCompanyL2Id)
		if isnumeric(p_iCompanyL2Id) then
			g_iSearchCompanyL2Id = clng(p_iCompanyL2Id)
		elseif p_iCompanyL2Id = "" then
			g_iSearchCompanyL2Id = ""
		else
			ReportError("Invalid SearchCompanyL2Id value.  Integer required.")
		end if
	end property
	
	
	'Search CompanyL3 Id - used to search for specific NULLS
	public property get SearchCompanyL3Id()
		SearchCompanyL3Id = g_iSearchCompanyL3Id
	end property
	public property let SearchCompanyL3Id(p_iCompanyL3Id)
		if isnumeric(p_iCompanyL3Id) then
			g_iSearchCompanyL3Id = clng(p_iCompanyL3Id)
		elseif p_iCompanyL3Id = "" then
			g_iSearchCompanyL3Id = ""
		else
			ReportError("Invalid SearchCompanyL3Id value.  Integer required.")
		end if
	end property

	

	' PRIVATE METHODS =========================================================

	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		set g_oRs = Server.CreateObject("ADODB.Recordset")
		
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		set g_oRs = nothing
	
	end sub


	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)

		set QueryDb = Server.CreateObject("ADODB.Recordset")

		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
		
		'Response.Write("<p>" & sSql & "<p>")
		
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	


	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Mark the specified Branch entry as deleted.
	' Preconditions: Connectionstring
	' Inputs: p_iUserId - User ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iBranchId)
		
		DeleteId = 0 
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iBranchId, false, "Branch ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		
'		if p_iBranchId >= 0 then
'
'			oConn.ConnectionString = g_sTpConnectionString
'			oConn.Open				
'
'			'Mark the branch as deleted in the trainingpro database
'			sSql = "UPDATE CompanyBranches SET " & _
'				   "Deleted = '1' " & _
'				   "WHERE BranchId = '" & p_iBranchId & "'" 
'			oConn.Execute sSql, lRecordsAffected
'			
'			'Verify that at least one record was affected
'			if not lRecordsAffected > 0 then
'			
'				ReportError("Failure deleting the Branch.")
'				exit function
'			
'			end if 
				
'		elseif p_iBranchId < 0 then
		
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			'Mark the user as deleted in the cms database
			sSql = "UPDATE CompanyBranches SET " & _
				   "Deleted = '1' " & _
				   "WHERE BranchId = '" & p_iBranchId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the user.")
				exit function
			
			end if 
			
'		end if 
		
		
		'We need to remove the Branch's licenses and note that they were
		'deleted via the Branch.
		oConn.Close
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE Licenses SET " & _
			"Deleted = '1', " & _
			"DeletedWithOwner = '1' " & _
			"WHERE Deleted = '0' " & _
			"AND OwnerId = '" & p_iBranchId & "' " & _
			"AND OwnerTypeId = '2' "
		oConn.Execute sSql, lRecordsAffected
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function


	
	' PUBLIC METHODS ==========================================================

	' -------------------------------------------------------------------------
	' Name: LoadBranchByName
	' Desc: Attempts to load the properties of a stored Branch that is
	'		associated with the passed name.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_sName - The Branch name to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadBranchByName(p_sName)
	
		LoadBranchByName = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckStrParam(p_sName, 0, "Name") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iBranchId 'Found Branch ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT BranchId FROM CompanyBranches " & _
			"WHERE Name LIKE '" & p_sName & "' " & _
			"AND CompanyId = '" & session("UserCompanyId") & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a recordset back
			iBranchId = oRs("BranchId")
			
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iBranchId)) or iBranchId = "" then
			
			ReportError("No valid Branch associated with this Name.")
			exit function
			
		end if 
		
				
		'Use the found Branch ID to load the Branch.
		LoadBranchByName = LoadBranchById(iBranchId)
		
	end function	

	' -------------------------------------------------------------------------
	' Name: LoadBranchByAddress
	' Desc: Attempts to load the properties of a stored Branch that is
	'		associated with the passed address.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_sAddress - The Branch address to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadBranchByAddress(p_sAddress, p_sCity, p_iStateID, p_sZipCode)
	
		LoadBranchByAddress = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckStrParam(p_sAddress, 0, "Street") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iBranchId 'Found Branch ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT BranchId FROM CompanyBranches " & _
			"WHERE Address LIKE '" & p_sAddress & "' " & _
            "AND City LIKE '" & p_sCity & "' " & _
            "AND StateID = '" & p_iStateID & "' " & _
            "AND ZipCode LIKE '" & p_sZipCode & "' " & _
			"AND CompanyId = '" & session("UserCompanyId") & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a recordset back
			iBranchId = oRs("BranchId")
			
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iBranchId)) or iBranchId = "" then
			
			ReportError("No valid Branch associated with this Name.")
			exit function
			
		end if 
		
				
		'Use the found Branch ID to load the Branch.
		LoadBranchByAddress = LoadBranchById(iBranchId)
		
	end function	

	' -------------------------------------------------------------------------
	' Name:	LoadBranchById
	' Desc:	Loads a branch associated with the passed branch ID
	' Inputs: p_iBranchId - ID of the branch to load
	' Outputs: None
	' -------------------------------------------------------------------------
	public function LoadBranchById(p_iBranchId)
	
		LoadBranchById = 0 
	
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		
		dim oRs 'Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		
		'Select the records from the correct database.  Negative IDs are
		'native to compliancekeeper, positive IDs are from TrainingPro.
		'We use a view to represent the partitioned data across the two servers
		'so two different statements are no longer necessary.
		
			sSql = "SELECT * FROM CompanyBranches " & _
				"WHERE BranchId = '" & p_iBranchId & "'"
			'Response.Write(sSql & "<br>")
			set oRs = QueryDb(sSql)
			
		
		'Make sure we got something back.
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iBranchId = oRs("BranchId")
			g_bActive = oRs("Active")
			g_bInactive = oRs("Inactive")
			g_iCompanyId = oRs("CompanyId")
			g_iCompanyL2Id = oRs("CompanyL2Id")
			g_iCompanyL3Id = oRs("CompanyL3Id")
			g_sBranchNum = oRs("BranchNum")
			g_sBranchName = oRs("Name")
			g_sLegalName = oRs("LegalName")
			g_sBranchManager = oRs("BranchManager")
			'g_dFormationDate = oRs("FormationDate")
			'g_dTerminationDate = oRs("TerminationDate")
			g_dInactiveDate = oRs("InactiveDate")
			g_sAddress = oRs("Address")
			g_sAddress2 = oRs("Address2")
			g_sCity = oRs("City")
			g_iStateId = oRs("StateId")
			g_sZipcode = oRs("Zipcode")
			g_sZipcodeExt = oRs("ZipcodeExt")
			g_iCountryId = oRs("CountryId")
			g_sPhone = oRs("Phone")
			g_sPhoneExt = oRs("PhoneExt")
			g_sFax = oRs("Fax")
			g_sEmail = oRs("Email")
			g_sEntityLicNum = oRs("EntityLicNum")
			g_sFhaBranchId = oRs("FhaBranchId")
			g_sVaBranchId = oRs("VaBranchId")
			g_sMailingAddress = oRs("MailingAddress")
			g_sMailingAddress2 = oRs("MailingAddress2")
			g_sMailingCity = oRs("MailingCity")
			g_iMailingStateId = oRs("MailingStateId")
			g_sMailingZipcode = oRs("MailingZipcode")
			g_sMailingZipcodeExt = oRs("MailingZipcodeExt")
			g_iMailingCountryId = oRs("MailingCountryId")
			g_sCustField1 = oRs("CustField1")
			g_sCustField2 = oRs("CustField2")
			g_sCustField3 = oRs("CustField3")
			g_sCustField4 = oRs("CustField4")
			g_sCustField5 = oRs("CustField5")
			g_sNMLSNumber = oRs("NMLSNumber")

			LoadBranchById = g_iBranchId
			
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.  
			LoadBranchById = 0 
			set oRs = nothing
			ReportError("Unable to load the passed Branch ID.")
			exit function
			
		end if 
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name: SaveBranch
	' Desc: Save the branch object in the database and assign it the resulting
	'	Company ID.
	' Preconditions: ConnectionString, BranchId, CompanyId
	' Inputs: None
	' Outputs: None
	' Returns: If successful, returns the (maybe new) Branch ID.  Otherwise
	'	zero.
	' -------------------------------------------------------------------------
	public function SaveBranch()
		
		SaveBranch = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			(not CheckStrParam(g_sBranchName, 0, "BranchName") and _
			not CheckStrParam(g_sBranchNum, 0, "BranchNum")) or _
			not CheckIntParam(g_iCompanyId, 0, "CompanyID") then
			exit function
		end if
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		'If we have a BranchId property assigned already, we're working on a
		'loaded company, so we'll do an UPDATE, otherwise we INSERT.
		dim bIsNew 'Track whether this is a new branch
		if g_iBranchId <> "" then
		
			'This is an existing Branch on CMS.  We'll build the SQL
			'statement accordingly.
			bIsNew = false
			sSql = "UPDATE CompanyBranches SET " & _
				"Active = '" & abs(cint(g_bActive)) & "', " & _
				"Inactive = '" & abs(cint(g_bInactive)) & "', " & _
				"Name = '" & g_sBranchName & "', " & _
				"LegalName = '" & g_sLegalName & "', " & _
				"BranchNum = '" & g_sBranchNum & "', " & _
				"CompanyId = '" & g_iCompanyId & "', "
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "CompanyL2Id = '" & g_iCompanyL2Id & "', " 
			else
				sSql = sSql & "CompanyL2Id = NULL, " 
			end if
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "CompanyL3Id = '" & g_iCompanyL3Id & "', "
			else
				sSql = sSql & "CompanyL3Id = NULL, "
			end if			

			if g_dInactiveDate <> "" then
				sSql = sSql & "InactiveDate = '" & g_dInactiveDate & "', " 
			else
				sSql = sSql & "InactiveDate = NULL, " 
			end if			
			
			sSql = sSql & "BranchManager = '" & g_sBranchManager & "', " & _
				"Address = '" & g_sAddress & "', " & _
				"Address2 = '" & g_sAddress2 & "', " & _ 
				"City = '" & g_sCity & "', " & _
				"StateId = '" & g_iStateId & "', " & _
				"Zipcode = '" & g_sZipcode & "', " & _
				"ZipcodeExt = '" & g_sZipcodeExt & "', " & _
				"CountryId = '" & g_iCountryId & "', " & _
				"Phone = '" & g_sPhone & "', " & _
				"PhoneExt = '" & g_sPhoneExt & "', " & _
				"Fax = '" & g_sFax & "', " & _
				"Email = '" & g_sEmail & "', " & _
				"EntityLicNum = '" & g_sEntityLicNum & "', " & _
				"FhaBranchId = '" & g_sFhaBranchId & "', " & _
				"VaBranchId = '" & g_sVaBranchId & "', " & _
				"MailingAddress = '" & g_sMailingAddress & "', " & _
				"MailingAddress2 = '" & g_sMailingAddress2 & "', " & _
				"MailingCity = '" & g_sMailingCity & "', " & _
				"MailingStateId = '" & g_iMailingStateId & "', " & _
				"MailingZipcode = '" & g_sMailingZipcode & "', " & _
				"MailingZipcodeExt = '" & g_sMailingZipcodeExt & "', " & _
				"MailingCountryId = '" & g_iMailingCountryId & "', " & _
				"CustField1 = '" & g_sCustField1 & "', " & _
				"CustField2 = '" & g_sCustField2 & "', " & _
				"CustField3 = '" & g_sCustField3 & "', " & _
				"CustField4 = '" & g_sCustField4 & "', " & _
				"CustField5 = '" & g_sCustField5 & "', " & _
                "NMLSNumber = '" & g_sNMLSNumber & "' " & _
				"WHERE BranchId = '" & g_iBranchId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Make sure records were affected.
			if lRecordsAffected > 0 then
				SaveBranch = g_iBranchId
			else
				ReportError("Unable to save existing CMS Branch.")
				SaveBranch = 0
			end if 
			
		else
		
			'This is a new branch of an existing CMS company.  We'll build the
			'appropriate SQL statement.
			sSql = "INSERT INTO CompanyBranches (" & _
				"Active, " & _
				"Inactive, " & _
				"Name, " & _
				"LegalName, " & _
				"BranchNum, " & _
				"CompanyId, " & _
				"CompanyL2Id, " & _
				"CompanyL3Id, " & _
				"BranchManager, " & _
				"FormationDate, " & _
				"TerminationDate, " & _
				"InactiveDate, " & _
				"Address, " & _
				"Address2, " & _
				"City, " & _
				"StateId, " & _
				"Zipcode, " & _
				"ZipcodeExt, " & _
				"CountryId, " & _
				"Phone, " & _
				"PhoneExt, " & _
				"Fax, " & _
				"Email, " & _
				"EntityLicNum, " & _
				"FhaBranchId, " & _
				"VaBranchId, " & _
				"MailingAddress, " & _
				"MailingAddress2, " & _
				"MailingCity, " & _
				"MailingStateId, " & _
				"MailingZipcode, " & _
				"MailingZipcodeExt, " & _
				"MailingCountryId, " & _
				"CustField1, " & _
				"CustField2, " & _
				"CustField3, " & _
				"CustField4, " & _
				"CustField5, " & _
                "NMLSNumber " & _
				") VALUES (" & _
				"'" & abs(cint(g_bActive)) & "', " & _
				"'" & abs(cint(g_bInactive)) & "', " & _
				"'" & g_sBranchName & "', " & _
				"'" & g_sLegalName & "', " & _
				"'" & g_sBranchNum & "', " & _
				"'" & g_iCompanyId & "', "
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "'" & g_iCompanyL2Id & "', " 
			else
				sSql = sSql & "NULL, " 
			end if
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "'" & g_iCompanyL3Id & "', "
			else
				sSql = sSql & "NULL, "
			end if		
			sSql = sSql & "'" & g_sBranchManager & "', " & _
				"'" & g_dFormationDate & "', " & _
				"'" & g_dTerminationDate & "', "
				
			if g_dInactiveDate <> "" then
				sSql = sSql & "'" & g_dInactiveDate & "', " 
			else
				sSql = sSql & "NULL, " 
			end if
				
				
			sSQL = sSql & "'" & g_sAddress & "', " & _
				"'" & g_sAddress2 & "', " & _
				"'" & g_sCity & "', " & _
				"'" & g_iStateId & "', " & _
				"'" & g_sZipcode & "', " & _
				"'" & g_sZipcodeExt & "', " & _
				"'" & g_iCountryId & "', " & _
				"'" & g_sPhone & "', " & _
				"'" & g_sPhoneExt & "', " & _
				"'" & g_sFax & "', " & _
				"'" & g_sEmail & "', " & _
				"'" & g_sEntityLicNum & "', " & _
				"'" & g_sFhaBranchId & "', " & _
				"'" & g_sVaBranchId & "', " & _
				"'" & g_sMailingAddress & "', " & _
				"'" & g_sMailingAddress2 & "', " & _
				"'" & g_sMailingCity & "', " & _
				"'" & g_iMailingStateId & "', " & _
				"'" & g_sMailingZipcode & "', " & _
				"'" & g_sMailingZipcodeExt & "', " & _
				"'" & g_iMailingCountryId & "', " & _
				"'" & g_sCustField1 & "', " & _
				"'" & g_sCustField2 & "', " & _
				"'" & g_sCustField3 & "', " & _
				"'" & g_sCustField4 & "', " & _
				"'" & g_sCustField5 & "', " & _
                "'" & g_sNMLSNumber & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected

			'Pull out the new record
			sSql = "SELECT BranchId FROM CompanyBranches " & _
				"WHERE CompanyId = '" & g_iCompanyId & "' " & _
				"ORDER BY BranchId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				g_iBranchId = oRs("BranchId")
				SaveBranch = g_iBranchId
				
			else
			
				'Our record was not created, or for some reason we couldn't
				'pull the record back out.
				ReportError("Unable to save new branch.")
				SaveBranch = 0
				exit function
				
			end if
		
		end if 
		
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: DeleteBranch
	' Description: Marks the branch as deleted.
	' Preconditions: ConnectionString, TpConnectionString, BranchId
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteBranch()

		DeleteBranch = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iBranchId, false, "Branch ID") then
			exit function
		end if 

		
		dim iResult
		
		iResult = DeleteId(g_iBranchId)
		
		if iResult = 0 then
		
			'Zero means failure
			ReportError("Could not delete the specified Branch.")
			DeleteBranch = 0 
			
		else
		
			'Success
			DeleteBranch = 1
			
		end if 

	end function
	
	' -------------------------------------------------------------------------
	' Name:				SearchBranches
	' Description:		Retrieves a collection of BranchIDs that match the 
	'                   properties of the current object.  I.e., based on the
	'                   current Name, City, etc.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchBranches()
	
		set SearchBranches = Server.CreateObject("ADODB.Recordset")
		stop
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		

		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim sSql2
		dim oRs
		
		bFirstClause = false 'true
		
		sSql = "SELECT DISTINCT Bs.BranchId, Bs.Name " & _
			"FROM CompanyBranches AS Bs " & _
			"WHERE Deleted = '0' "
			
			
		'If BranchNameStart search
		if g_sSearchNameStart <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.Name LIKE '" & g_sSearchNameStart & "%'"
		end if
				
		
		'If SearchInName search
		if g_sSearchInName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.Name LIKE '%" & g_sSearchInName & "%'"
		end if	
		

		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyId = '" & g_iCompanyId & "'"
		end if 	
		
		'If CompanyL2Id search
		if g_iCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyL2Id = '" & g_iCompanyL2Id & "'"
		end if 	

		'If CompanyL3Id search
		if g_iCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyL3Id = '" & g_iCompanyL3Id & "'"
		end if 	

		'If CompanyL2Id search
		if g_iSearchCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_iSearchCompanyL2Id = 0 then
				sSql = sSql & "Bs.CompanyL2Id IS NULL "
			else
				sSql = sSql & "(Bs.CompanyL2Id = '" & g_iSearchCompanyL2Id & "' OR Bs.CompanyL3ID IN (SELECT CompanyL3ID FROM CompaniesL3 WHERE CompanyL2ID = '" & g_iSearchCompanyL2Id & "')) "
			end if 
		end if 	

		'If CompanyL3Id search
		if g_iSearchCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_iSearchCompanyL3Id = 0 then
				sSql = sSql & "Bs.CompanyL3Id IS NULL "
			else
				sSql = sSql & "Bs.CompanyL3Id = '" & g_iSearchCompanyL3Id & "'"
			end if 
		end if 	
		
		'If CustField1 search
		if g_sCustField1 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField1 LIKE '%" & g_sCustField1 & "%'"
		end if
		'If CustField2 search
		if g_sCustField2 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField2 LIKE '%" & g_sCustField2 & "%'"
		end if
		'If CustField3 search
		if g_sCustField3 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField3 LIKE '%" & g_sCustField3 & "%'"
		end if
		'If CustField4 search
		if g_sCustField4 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField4 LIKE '%" & g_sCustField4 & "%'"
		end if
		'If CustField5 search
		if g_sCustField5 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField5 LIKE '%" & g_sCustField5 & "%'"
		end if
		
        if g_sNMLSNumber <> "" then
            if bFirstClause then
                sSql = sSql & " WHERE "
                bFirstClause = false
            else
                sSql = sSql & " AND "
            end if
            sSql = sSql & "Bs.NMLSNumber LIKE '%" & g_sNMLSNumber & "%'"
        end if
		
		'if Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if


		sSQL = sSQL & " ORDER BY Bs.Name"
		
		'Response.Write("<p>" & sSql & "</p>")
		
		set SearchBranches = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchBranchesLicenses
	' Desc: Retrieves a collection of UserIds that match the properties of the
	'	current object.  Also searches license information.  Used in reports.
	' Preconditions: ConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchBranchesLicenses()
	
		set SearchBranchesLicenses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = false 'true
		
		sSql = "SELECT DISTINCT Bs.BranchId, Bs.Name, Bs.BranchNum " & _
			"FROM CompanyBranches AS Bs " & _
			"LEFT OUTER JOIN Licenses AS Ls " & _
			"ON ((Ls.OwnerId = Bs.BranchId) AND (Ls.OwnerTypeId = '2') " & _
			"AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL)) " & _
			"WHERE Bs.Deleted = '0' "
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyId = '" & g_iCompanyId & "'"
		end if 	
		
		'if Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
		
		'If CustField1 search
		if g_sCustField1 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField1 LIKE '%" & g_sCustField1 & "%'"
		end if
		'If CustField2 search
		if g_sCustField2 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField2 LIKE '%" & g_sCustField2 & "%'"
		end if
		'If CustField3 search
		if g_sCustField3 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField3 LIKE '%" & g_sCustField3 & "%'"
		end if
		'If CustField4 search
		if g_sCustField4 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField4 LIKE '%" & g_sCustField4 & "%'"
		end if
		'If CustField5 search
		if g_sCustField5 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CustField5 LIKE '%" & g_sCustField5 & "%'"
		end if
		
		if g_sNMLSNumber <> "" then
            if bFirstClause then
                sSql = sSql & " WHERE "
                bFirstClause = false
            else
                sSql = sSql & " AND "
            end if
            sSql = sSql & "Bs.NMLSNumber LIKE '%" & g_sNMLSNumber & "%'"
        end if

		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Bs.StateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 			
		
		
		'If State Id List search
		if g_sSearchLicStateIdList <> "" then
			aStateIdList = split(g_sSearchLicStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 			
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Bs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Bs.CompanyL3Id = '" & iCompanyL3Id & "' "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Bs.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if
		
		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
		end if
		
		
		'If LicenseStatusIdList search
		dim aStatusIdList
		dim iStatusId
		dim bFirstStatus
		if g_sLicenseStatusIdList <> "" then
			aStatusIdList = split(g_sLicenseStatusIdList, ",")
			bFirstStatus = true
			for each iStatusId in aStatusIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstStatus then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStatusId = '" & iStatusId & "' "
				
				bFirstStatus = false
			
			next
			sSql = sSql & ")"
		end if 	
		
		
		'If LicenseNumber search
		if g_sLicenseNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNumber & "%'"
		end if
		
		
		'If SearchLicenseExpDateFrom/To search
		if g_dLicenseExpDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_dLicenseExpDateTo <> "" then
				if g_iAppDeadline = "1" then 
					sSql = sSql & "((Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') " & _
						"OR (" & _
						"Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "')) "
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') "
				end if
			else
				if g_iAppDeadline = "1" then
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"OR Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "')"					
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "') "  
				end if
			end if
		elseif g_dLicenseExpDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
					
			if g_iAppDeadline = "1" then
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "' " & _
					"OR Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "')"		
			elseif g_iAppDeadline = "2" then
				sSql = sSql & "(Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "') "
			else
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "') "
			end if 
		end if 
		

		sSQL = sSQL & " ORDER BY Bs.Name"
		
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchBranchesLicenses = QueryDb(sSql)
		
	end function
		
	
	
	' -------------------------------------------------------------------------
	' Name: GetAdminBranches
	' Desc: Returns a recordset containing the branches assigned to an admin
	' Preconditions: ConnectionString
	' Inputs: p_iUserId
	' Returns: Recordset
	' -------------------------------------------------------------------------
	public function GetAdminBranches(p_iUserId)
	
		set GetAdminBranches = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iUserId, 0, "User ID") then
			exit function
		end if
		
		dim sSql
		
		sSql = "SELECT UBs.BranchId, CBs.Name, CBs.BranchNum " & _
			"FROM UsersBranches AS UBs " & _
			"LEFT JOIN CompanyBranches AS CBs " & _
			"ON (UBs.BranchId = CBs.BranchId) " & _
			"WHERE UBs.UserId = '" & p_iUserId & "' "
		'Response.Write("<p>" & sSql & "<p>")
		set GetAdminBranches = QueryDb(sSql)
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: GetAdmins
	' Desc: Returns a recordset containing the admins and their emails
	' Preconditions: ConnectionString, p_iCompanyL2Id
	' Returns: Recordset
	' -------------------------------------------------------------------------
	public function GetAdmins(p_iBranchId)
		
		set GetAdmins = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		dim aBranches 'Array based on passed string
		dim iBranchId 'Single isolated branch ID
		dim bFirst 'Have we listed the first ID in the SQL query yet?
		dim sSql 'SQL Query
		
		sSql = "SELECT DISTINCT Us.UserId, Us.FirstName, Us.MiddleName, " & _
			"Us.LastName, Us.Email " & _
			"FROM Users AS Us " & _
			"LEFT JOIN UsersBranches AS UBs " & _
			"ON (UBs.UserId = Us.UserId) " & _
			"WHERE UBs.BranchId = '" & p_iBranchId & "' " & _
			"AND Us.GroupId = '10' " & _
			"AND Us.UserStatus = '1' "
				
		'Response.Write("<p>" & sSql & "<p>")
		set GetAdmins = QueryDb(sSql)
		
	end function	
		
	
	' -------------------------------------------------------------------------
	' Name: GetBranchAdmins
	' Desc: Returns a recordset containing the branch admins and their emails
	' Preconditions: ConnectionString, BranchId
	' Returns: Recordset
	' -------------------------------------------------------------------------
	public function GetBranchAdmins(p_sBranchIdList, p_iCompanyId)
		
		set GetBranchAdmins = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") or _
			not CheckStrParam(p_sBranchIdList, 0, "Branch ID") then
			exit function
		end if
		
		dim aBranches 'Array based on passed string
		dim iBranchId 'Single isolated branch ID
		dim bFirst 'Have we listed the first ID in the SQL query yet?
		dim sSql 'SQL Query		
		
		sSql = "SELECT DISTINCT Us.UserId, Us.FirstName, Us.MiddleName, " & _
			"Us.LastName, Us.Email " & _
			"FROM Users AS Us " & _
			"LEFT JOIN UsersBranches AS UBs " & _
			"ON (UBs.UserId = Us.UserId) " & _
			"WHERE (" 
		
		'Loop through the list we were passed and check for each branch ID.
		bFirst = true
		aBranches = split(p_sBranchIdList, ", ")
		for each iBranchId in aBranches
			if not bFirst then
				sSql = sSql & " OR "
			else
				bFirst = false				
			end if
			sSql = sSql & "UBs.BranchId = '" & iBranchId & "'"
		next
		
		sSql = sSql & ") " & _
			"AND Us.GroupId = '10' " & _
			"AND Us.UserStatus = '1' "
				
		'Response.Write("<p>" & sSql & "<p>")
		set GetBranchAdmins = QueryDb(sSql)
		
	end function	

    ' -------------------------------------------------------------------------
	' Name: GetCustField
	' Desc: Returns the value of the specified CustField
	' Preconditions: none
	' Inputs: p_iNo - the number of the Custom Field
	' Returns: string - the value of the Custom Field
	' -------------------------------------------------------------------------
	public function	GetCustField(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetCustField = g_sCustField1
			case 2
				GetCustField = g_sCustField2
			case 3
				GetCustField = g_sCustField3
			case 4
				GetCustField = g_sCustField4
			case 5
				GetCustField = g_sCustField5
			case else
				GetCustField = ""
		end select
	end function

	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then 
			LookupState = oRs("State")
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupBranchIdByBranchNum
	' Description:		Gets the unique branch Id based on the passed branch 
	'					number
	' Pre-conditions:	ConnectionString, CompanyId
	' Inputs:			p_iBranchNum - Num to look up
	' Outputs:			None.
	' Returns:			BranchId, or 0
	' -------------------------------------------------------------------------
	public function LookupBranchIdByBranchNum(p_iBranchNum)
	
		LookupBranchIdByBranchNum = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT BranchId FROM CompanyBranches " & _
			"WHERE BranchNum = '" & p_iBranchNum & "' " & _
			"AND CompanyId = '" & g_iCompanyId & "' " 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then 
			LookupBranchIdByBranchNum = oRs("BranchId")
		end if
		
		set oRs = nothing
		
	end function	
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupBranchIdByBranchName
	' Description:		Gets the unique branch Id based on the passed branch 
	'					name
	' Pre-conditions:	ConnectionString, CompanyId
	' Inputs:			p_iBranchNum - Num to look up
	' Outputs:			None.
	' Returns:			BranchId, or 0
	' -------------------------------------------------------------------------
	public function LookupBranchIdByBranchName(p_sBranchName)
	
		LookupBranchIdByBranchName = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT BranchId FROM CompanyBranches " & _
			"WHERE Name LIKE '" & p_sBranchName & "' " & _
			"AND CompanyId = '" & g_iCompanyId & "' " 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then 
			LookupBranchIdByBranchName = oRs("BranchId")
		end if
		
		set oRs = nothing
		
	end function	



end class
%>

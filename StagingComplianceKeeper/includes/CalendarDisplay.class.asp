<LINK REL="stylesheet" TYPE="text/css" HREF="/includes/Calendar.css">
<%
'===============================================================================================================
' Class: CalendarDisplay
' Description: Will display calendars using the CalendarStyle.css file and the ASP Response Object
'			Temporary Recordset File is needed (TempRecordsetDefinition.rst)
' Create Date: 1/9/2002
' Current Version: 1.0
' Author(s): Mike Joseph
' References to other DLL's: None
' ----------------------
' Properties:
'	- TemporaryRecordsetFile -- *** Required *** string -- Location of an empty recordset to store those values
'	- Mode -- string that equals "mini" (default), "day", "week", or "month"
'	- StartOfBusinessDay -- Time or TimeExpression.  Time of day that business will start (default 8am)
'	- EndOfBusinessDay -- Time or TimeExpression.  Time of day that business will end (default 6pm)
'	- MaxEventsPerCalendarViewGrid -- Integer -- The maximum number of events to display in a calendar view (default - 3)
'	- DateLink -- string.  The link to display on a date.  Use %PropertyName% for a propertyvalue substitution.  The date will be tacked onto the end.
'	- HighlightReferenceDate -- (default = False) boolean. Indicator of whether or not to highlight the reference date in a calendar grid view.
'	- ShowEmptyDaysInWeekView -- (default = False) boolean. Indicates whether or not to display empty days (days w/ no associated Event) in the "week" view mode
'	- * ShowSummaryHeader -- (default = True) boolean. Indicates whether or not the SummaryHeader Should appear above the printed calendars.
'	- ShowAllTimeSlotsInDayView -- (default = False) boolean. Indicates whether or not you want all the time slots shown for the day view.  If false, then only time blocks within business hours will be shown.
'	- DayViewTimeInterval -- (default = 60) Integer or integer expression. Indicates how many minutes you want split the day view up with. (Accepts: 5, 10, 15, 30, 60, 120, & 240)

'	- ReturnPage -- The page to return to after visiting the page indicated in the TimeLink Property (applies only to day view)
'	- TimeLink -- The link to display for times on the calendar.  Will tack the querystring parameters date, time and ReturnPage (applies only to day view)
'
' Public Methods:
'	- PrintCalendar -- Will print the calendar
'	- AddEvent(p_sDate, p_sDescription, p_sDetails) -- Add an event to the event recordset
'	- ClearEvents -- Reinitialize the Temp Recordset with no records
'	- GetDaysInMonth(iMonth, iYear) -- Gets the number of days in a month
'	- SubtractOneMonth(dDate) -- Shortcut to the DateAdd (-1) function
'	- AddOneMonth(dDate) -- Shortcut to the DateAdd (+1) function
'
' Private Methods:
'	- Class_Initialize() & Class_Terminate() -- Event Handlers
'	- GetWeekdayMonthStartsOn(dAnyDayInTheMonth)
'	- DestroyTemporaryRecordset()
'	- GenerateDateLink(p_dDate)
'	- GetNormalTime(p_dDateExpression)
'	- PrintMonthGrid(p_dDate)
'	- PrintWeekView(p_dDate)
'	- AlternateColor(p_nColor)
'	- PrintDayForWeekView(p_dDate)
'	- GetDayOfWeekName(p_dDate, p_bIndicateToday)
'	- FormatNiceShortDate(p_dDate)
'	- GetMonthName(p_nMonth, p_bAbbreviated)
'	- PrintDayView(p_dReferenceDate)
'===============================================================================================================
Class CalendarDisplay
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	private g_rsEvents					' Starts as Empty RS Containing fields: Date, Description, Details
	private g_bIsRSEventSorted			' Indicating whether or not the RS is in a sorted status
	private g_nDefaultErrNumber			' For Error Handling
	private g_sApplicationName			' For Error Handling

	' Property Holder Variables:
	private g_sMode					' mini (default), day, week, month
	private g_sSampleRecordsetLocation		' Location of an empty recordset to store those values
	private g_dStartOfBusinessDay			' Time of day that business will start (default 8am)
	private g_dEndOfBusinessDay			' Time of day that business will end (default 6pm)
	private g_nMaxEventsPerCalendarViewGrid	' The maximum number of events to display per day in a calendar view (default - 3)
	private g_sDateLink					' The link to display on a date.  Use %PropertyName% for a propertyvalue substitution.  The date will be tacked onto the end.
	private g_sEmptyDateLink
	private g_bHighlightReferenceDate		' Default = False
	private g_bShowEmptyDaysInWeekView		' Default = False
	private g_bShowSummaryHeader			' Default = True
	private g_bShowAllTimeSlotsInDayView	' Default = False
	private g_nDayViewTimeInterval		' Default = 60
	private g_sReturnPage				' Default = ""
	private g_sTimeLink

	' PUBLIC METHODS ===========================================================================================
	
	' PrintCalendar ------------------------------------------------------------------------------------------------------------------------ 
	' Description: 
	' Pre-conditions: 
	' Inputs: 
	'	Property Inputs -- 
	'	Parameter Inputs -- 
	' Outputs: 
	' Post-conditions: None
	' --------------------------------------------------------------------------------------------------------------------------------
	Public Sub PrintCalendar(p_dReferenceDate)
		' If the Sameple Recordset hasn't been opened, then give an error:
		if g_rsEvents.State <> adStateOpen then 
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "You must specify the location of the TemporaryRecordsetFile Property."
		end if

		' Make sure the Recordset is Sorted:
		if not g_bIsRSEventSorted then
			g_rsEvents.Sort = "Date"
			g_bIsRSEventSorted = True
		end if
		
		if g_sMode = "mini" or g_sMode = "month" then
			PrintMonthGrid(p_dReferenceDate)
		elseif g_sMode = "week" then
			PrintWeekView(p_dReferenceDate)
		elseif g_sMode = "day" then
			PrintDayView(p_dReferenceDate)
		else
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "PrintCalendar: Could not determine what action to perform."
		end if
		
		' Make sure the Recordset is reset:
		g_rsEvents.Filter = ""
		if not g_rsEvents.BOF then g_rsEvents.MoveFirst

	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	Public Sub AddEvent(p_sDate, p_sDescription, p_sDetails)
		dim sError
		sError = ""

		' Validation --------------
		if g_rsEvents.State <> adStateOpen then sError = "You must specify the location of the TemporaryRecordsetFile Property."
		if isDate(p_sDate) then 
			p_sDate = CDate(p_sDate)
		else
			sError = "You must specify a valid date-time value for the p_sDate Parameter.  (Value = '" & p_sDate & "')"
		end if
		if isEmpty(p_sDescription) or isNull(p_sDescription) then p_sDescription = ""
		if p_sDescription = "" then sError = "You cannot specify a blank value for the p_sDescription Property"

		' If there's a error that's found: Raise it
		if sError <> "" then 
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "AddEvent: " & sError
		end if

		' Done Validation --------

		' Add the New Record:
		with g_rsEvents
			.AddNew
			.Fields("Date") = p_sDate
			.Fields("Description") = p_sDescription
			.Fields("Details") = p_sDetails
			.Update
		end with

		' Indicate that the recordset is no longer in a sorted status:
		g_bIsRSEventSorted = False
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	Public Sub ClearEvents()
		if g_rsEvents.State = adStateOpen then g_rsEvents.Close
		if not isNull(g_sSampleRecordsetLocation) then g_rsEvents.Open g_sSampleRecordsetLocation
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------

	Public Function GetDaysInMonth(p_nMonth, p_nYear)
		dim dTemp
		dTemp = DateAdd("d", -1, DateSerial(p_nYear, p_nMonth + 1, 1))
		GetDaysInMonth = Day(dTemp)
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------

	Public Function SubtractOneMonth(p_dDate)
		SubtractOneMonth = DateAdd("m", -1, p_dDate)
	End Function

	' --------------------------------------------------------------------------------------------------------------------------------

	Public Function AddOneMonth(p_dDate)
		AddOneMonth = DateAdd("m", 1, p_dDate)
	End Function

	' PRIVATE METHODS ==========================================================================================
	Private Sub Class_Initialize()
		set g_rsEvents = CreateObject("ADODB.Recordset")
		g_bIsRSEventSorted = True

		g_nDefaultErrNumber = 1
		g_sApplicationName = "CalendarDisplay"

		g_sMode = "mini"
		g_sSampleRecordsetLocation = Null
		g_dStartOfBusinessDay = CDate("8:00:00 AM")
		g_dEndOfBusinessDay = CDate("6:00:00 PM")
		g_nMaxEventsPerCalendarViewGrid = 3
		g_sDateLink = Null
		g_bShowSummaryHeader = True
		g_bHighlightReferenceDate = False
		g_bShowEmptyDaysInWeekView = False
		g_bShowAllTimeSlotsInDayView = False
		g_nDayViewTimeInterval = 60
		g_sReturnPage = Null
		g_sTimeLink = Null
	End Sub

	Private Sub Class_Terminate()
		DestroyTemporaryRecordset
		g_bIsRSEventSorted = Null

		g_nDefaultErrNumber = 1
		g_sApplicationName = "CalendarDisplay"

		g_sMode = Null
		g_sSampleRecordsetLocation = Null
		g_dStartOfBusinessDay = Null
		g_dEndOfBusinessDay = Null
		g_nMaxEventsPerCalendarViewGrid = Null
		g_sDateLink = Null
		g_bHighlightReferenceDate = Null
		g_bShowEmptyDaysInWeekView = Null
		g_bShowAllTimeSlotsInDayView = Null
		g_nDayViewTimeInterval = Null
		g_sReturnPage = Null
		g_sTimeLink = Null
	End Sub

	Private Function GetWeekdayMonthStartsOn(dAnyDayInTheMonth)
		Dim dTemp
		dTemp = DateAdd("d", -(Day(dAnyDayInTheMonth) - 1), dAnyDayInTheMonth)
		GetWeekdayMonthStartsOn = WeekDay(dTemp)
	End Function

	Private Function DestroyTemporaryRecordset()
		if g_rsEvents.State = adStateOpen then g_rsEvents.Close
		set g_rsEvents = Nothing
	End Function

	Private Function GenerateDateLink(p_dDate)
		dim sRetVal, sExecString
		sRetVal = "#"

		if g_sDateLink <> "" then 
			sExecString = Replace(g_sDateLink, "%Mode%", """ & g_sMode &  """)
			sExecString = "sRetVal = """ & g_sDateLink & Server.URLEncode(FormatDateTime(p_dDate, 2)) & """"
			'response.write sExecString
			execute sExecString
		end if

		GenerateDateLink = sRetVal
	End Function
	
	
	Private Function GenerateEmptyDateLink(p_dDate)
		dim sRetVal, sExecString
		sRetVal = "#"

		if g_sEmptyDateLink <> "" then 
			sExecString = "sRetVal = """ & g_sEmptyDateLink & Server.URLEncode(FormatDateTime(p_dDate, 2)) & """"
			'response.write sExecString
			execute sExecString
		end if

		GenerateEmptyDateLink = sRetVal
	End Function
	

	Public Function GenerateTimeLink(p_dDate)
		dim sRetVal, sExecString
		sRetVal = "#"

		if isDate(p_dDate) then
			if inStr(g_sTimeLink, "?") = 0 then
				sRetVal = g_sTimeLink & "?"
			else
				sRetVal = g_sTimeLink & "&"
			end if

			sRetVal = sRetVal & "date=" & Server.URLEncode(FormatDateTime(p_dDate)) & "&Time=" & Server.URLEncode(GetNormalTime(p_dDate))

			if not isNull(g_sReturnPage) then
				sRetVal = sRetVal & "&ReturnPage=" & Server.URLEncode(sReturnPage)
			end if
		end if

		GenerateTimeLink = sRetVal
	End Function

	Public Function GetNormalTime(p_dDateExpression)
		dim sRetVal, nLastColonPosition
		
		sRetVal = FormatDateTime(p_dDateExpression, 3)
		nLastColonPosition = InStrRev(sRetVal, ":")
		sRetVal = left(sRetVal, nLastColonPosition - 1) & lcase(right(sRetVal, 2))

		GetNormalTime = UCase(sRetVal)
	End Function

	Private Sub PrintMonthGrid(p_dDate)

		' Dim dDate		' Date we're displaying calendar for
		dim nDaysInMonth, nDayOfWeekMonthStartsOn, nCurrentDay, nPositionInTable
		dim sSun, sMon, sTue, sWed, sThu, sFri, sSat
		dim bHasEvents, nEventCount

		' Class Variable Holders:
		dim sMonthView_DayHeaderClass, sSpacerCalendarCell, sTableClass, sSummaryInfoClass
		dim sDayHeaderTDClass, sCurrentDayFontClass, sCurrentDayTDClass
		
		' Get Days in the choosen month and the day of the week it starts on.
		nDaysInMonth = GetDaysInMonth(Month(p_dDate), Year(p_dDate))
		nDayOfWeekMonthStartsOn = GetWeekdayMonthStartsOn(p_dDate)

		' Prepare Visual Information:
		if g_sMode = "mini" then
			sSummaryInfoClass = "SummaryInfoMini"
			sTableClass = "MonthView_CalendarMini"
			sDayHeaderTDClass = "MonthView_DayHeaderMini"
			sSpacerCalendarCell = "MonthView_DayEmptyMini"
			sWrapperTableClass = "MonthView_CalendarWrapper"

			sSun = "S"
			sMon = "M"
			sTue = "T"
			sWed = "W"
			sThu = "T"
			sFri = "F"
			sSat = "S"
		elseif g_sMode = "month" then
			sSummaryInfoClass = "SummaryInfoMonth"
			sTableClass = "MonthView_Calendar"
			sDayHeaderTDClass = "MonthView_DayHeader"
			sSpacerCalendarCell = "MonthView_DayEmpty"

			sSun = "Sunday"
			sMon = "Monday"
			sTue = "Tuesday"
			sWed = "Wednesday"
			sThu = "Thursday"
			sFri = "Friday"
			sSat = "Saturday"
		else
			' Raise an Error here.
		end if

%>
		<table>
		<tr><td class="<% = sWrapperTableClass %>">

			<table border="0" cellspacing="3" cellpadding="1" class="<%= sTableClass %>" bgcolor="#eeeeee">
<%
			if  g_bShowSummaryHeader then
			
				dim sPrevDate, sNextDate
'				dim bDisplayPrevMonth, bDisplayNextMonth

				sPrevDate = oCalendarDisplay.SubtractOneMonth(p_dDate)
				sNextDate = oCalendarDisplay.AddOneMonth(p_dDate)
'				if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
'					bDisplayPrevMonth = false
'				else
'					bDisplayPrevMonth = true
'				end if
'				if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
'					bDisplayNextMonth = false
'				else
'					bDisplayNextMonth = true
'				end if
			
				response.write "<tr><td colspan=""7"" class=""" & sDayHeaderTDClass & """>"
'				if bDisplayPrevMonth then 
					response.write("<a href=""/calendar/calendar.asp?refDate=" & sPrevDate & "&mode=" & sMode & "&SearchState=" & iSearchStateID & "&SearchEventType=" & sEventType & """ class=""" & sDayHeaderTDClass & """>&lt;&lt;</a>&nbsp;&nbsp;")
'				end if
				response.write MonthName(Month(p_dDate)) & " " & Year(p_dDate)
'				if bDisplayNextMonth then 
					response.write("&nbsp;&nbsp;<a href=""/calendar/calendar.asp?refDate=" & sNextDate & "&mode=" & sMode  & "&SearchState=" & iSearchStateID & "&SearchEventType=" & sEventType & """ class=""" & sDayHeaderTDClass & """>&gt;&gt;</a>")
'				end if
				
				response.write "</td></tr>" & vbCrLf
			end if
%>
				<tr>
					<td class="<%= sDayHeaderTDClass %>"><%= sSun %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sMon %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sTue %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sWed %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sThu %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sFri %></td>
					<td class="<%= sDayHeaderTDClass %>"><%= sSat %></td>
				</tr>
<%

		' Write spacer cells at beginning of first row if month doesn't start on a Sunday.
		if nDayOfWeekMonthStartsOn <> 1 then
			response.write "<tr>" & vbCrLf
			nPositionInTable = 1
			do while nPositionInTable < nDayOfWeekMonthStartsOn
				response.write "<td class=""" & sSpacerCalendarCell & """>&nbsp;</td>" & vbCrLf
				nPositionInTable = nPositionInTable + 1
			Loop
		end if

		' Write days of month in proper day slots
		nCurrentDay = 1
		nPositionInTable = nDayOfWeekMonthStartsOn

		do while nCurrentDay <= nDaysInMonth

			' if we're at the begginning of a row then write TR
			if nPositionInTable = 1 then response.write "<tr>" & vbCrLf
			
			' ----------- Data for the Day ------------------------------
			if g_sMode = "month" then
				sCurrentDayTDClass = "MonthView_DayEmpty"
			else
				sCurrentDayTDClass = "MonthView_DayEmptyMini"
			end if
			sCurrentDayFontClass = ""
			bHasEvents = False
			nEventCount = 0

			' if the day we're writing is the selected day then highlight it somehow.
			if nCurrentDay = Day(Date) and Month(p_dDate) = Month(now) then
				if g_sMode = "month" then
					sCurrentDayTDClass = "MonthView_DayCurrent"
				else
					sCurrentDayTDClass = "MonthView_DayCurrentMini"
				end if
			end if

			' Filter the event recordset:
			g_rsEvents.Filter = "Date >= '" & Month(p_dDate) & "/" & nCurrentDay & "/" & Year(p_dDate) & " 12:00:00 AM' " _
					& "AND Date <= '" & Month(p_dDate) & "/" & nCurrentDay & "/" & Year(p_dDate) & " 11:59:59 PM'"

			' If events were found for this day:
			if not g_rsEvents.EOF then
				sCurrentDayFontClass = "MiniView_Filled"
				sCurrentDayTDClass = "MonthView_DayEventMini"
			end if

			' If this day is the day of the specified date parameter:
			if g_bHighlightReferenceDate then
				if nCurrentDay = Day(p_dDate) then
					' Determine the style:
					if sCurrentDayTDClass = "MonthView_DayCurrent" then
						sCurrentDayTDClass = "MonthView_DayCurrentSelected"
					elseif sCurrentDayTDClass = "MonthView_DayCurrentMini" then
						sCurerntDayTDClass = "MonthView_DayCurrentSelectedMini"
					elseif sCurrentDayTDClass = "MonthView_DayEventMini" then
						sCurrentDayTDClass = "MonthView_DayCurrentSelectedEventMini"
					else
						if g_sMode = "month" then
							sCurrentDayTDClass = "MonthView_DaySelected"
						else
'							sCurrentDayTDClass = "MonthView_DaySelectedMini"
						end if
					end if
				end if
			end if

			' Print the day -----------------------------------------------
			response.write "<td class=""" & sCurrentDayTDClass & """>"

			' If a font was specified, span it:
			if sCurrentDayFontClass <> "" then response.write "<span class=""" & sCurrentDayFontClass & """>"
			if not g_rsEvents.EOF then
				if not isNull(g_sDateLink) then response.write "<a class=""newsTitle"" href=""" & GenerateDateLink(Month(p_dDate) & "/" & nCurrentDay & "/" & Year(p_dDate)) & """>"
			else
				if not isNull(g_sEmptyDateLink) then
					Response.Write("<a class=""newsTitle"" href=""" & GenerateEmptyDateLink(Month(p_dDate) & "/" & nCurrentDay & "/" & Year(p_dDate)) & """>")
				end if
			end if 
			response.write nCurrentDay
			if not isNull(g_sDateLink) then response.write "</a>"
			if sCurrentDayFontClass <> "" then response.write "</span>"

			if g_sMode = "month" then

				' Print Events:

				do until g_rsEvents.EOF or nEventCount >= g_nMaxEventsPerCalendarViewGrid
					response.write "<br /><span class=""arial8"">" 
'					if GetNormalTime(g_rsEvents("Date")) <> "12:00am" then
'						response.write GetNormalTime(g_rsEvents("Date")) & " - "
'					end if
					response.write g_rsEvents("Description") & "</span><br />"

					nEventCount = nEventCount + 1
					g_rsEvents.MoveNext

					' If this is the last event that can be displayed, and there are more records left, display a 'all events' link
					if (nEventCount >= g_nMaxEventsPerCalendarViewGrid) and (not g_rsEvents.EOF) then
						if isNull(g_sDateLink) then
							response.write "<br /><span class=""MonthView_Footnote"">(" & g_rsEvents.RecordCount - g_nMaxEventsPerCalendarViewGrid & " more events not listed)</span>"
						else
							response.write "<br /><a class=""MonthView_Footnote"" href=""" & GenerateDateLink(Month(p_dDate) & "/" & nCurrentDay & "/" & Year(p_dDate)) & """>More (" & g_rsEvents.RecordCount - g_nMaxEventsPerCalendarViewGrid & ") . . .</a>"
						end if
					end if
				Loop
			end if
			
			' Finish the Cell
			response.write "</td>" & vbCrLf

			' Reset the Events Recordset:
			g_rsEvents.Filter = ""
			if not g_rsEvents.BOF then g_rsEvents.MoveFirst

			' ----------- End Data for the Day --------------------------

			' if we're at the endof a row then write /TR
			if nPositionInTable = 7 then
				response.write "</tr>" & vbCrLf
				nPositionInTable = 0
			end if
			
			' Increment placeholder variables:
			nCurrentDay = nCurrentDay + 1
			nPositionInTable = nPositionInTable + 1
		Loop


		' Write spacer cells at end of last row if month doesn't end on a Saturday.
		if nPositionInTable <> 1 then
			do while nPositionInTable <= 7
				response.write "<td class=""" & sSpacerCalendarCell & """>&nbsp;</td>" & vbCrLf
				nPositionInTable = nPositionInTable + 1
			Loop
			response.write "</tr>" & vbCrLf
		end if
%>
			</table>
		</td></tr>
		</table>
<%
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' --------------------------------------------------------------------------------------------------------------------------------
	' --------------------------------------------------------------------------------------------------------------------------------

	Private Sub PrintWeekView(p_dDate)
		dim nNumberOfDaysInPreviousMonth, nDayOfWeek
		dim dFirstDayOfWeek, dLastDayOfWeek, dCurrentDay
		dim sCurrentTDStyle, nColor

		nColor = 1

		' A week in Leadtracker 2.0 begins on Sunday, Ends on Saturday.
		nDayOfWeek = DatePart("W", p_dDate)
		dFirstDayOfWeek = DateAdd("d", -(nDayOfWeek - 1), p_dDate)
		dLastDayOfWeek = DateAdd("d", 6, dFirstDayOfWeek)
		dCurrentDay = dFirstDayOfWeek

		
		if not g_bIsRSEventSorted then
			g_rsEvents.Sort "Date"
			g_bIsRSEventSorted = True
		end if

		response.write "<table class=""WeekView_Calendar"">"
		
		if g_rsEvents.EOF then response.write "<tr><td>There are no events listed for the week of " & FormatDateTime(dFirstDayOfWeek) & " to " & FormatDateTime(dLastDayOfWeek) & "</td></tr>"

		nColor = 1
		do until dCurrentDay > dLastDayOfWeek

			' Find any Events for this Day:
			if not g_rsEvents.BOF then g_rsEvents.MoveFirst
			g_rsEvents.Filter = "Date >= '" & FormatDateTime(dCurrentDay) & " 12:00:00 AM' AND " _
							& "Date <= '" & FormatDateTime(dCurrentDay) & " 11:59:59 PM'"
			if (not g_rsEvents.EOF) OR (g_rsEvents.EOF and g_bShowEmptyDaysInWeekView) then

				' Determine visual information ----------
				nColor = AlternateColor(nColor)
				sCurrentTDStyle = "WeekView_Day" & nColor

				' if the dCurrentDay is equal to today's date then:
				if CDate(FormatDateTime(dCurrentDay, 2)) = Date then
					sCurrentTDStyle = sCurrentTDStyle & "Current"
				end if

				' if we are supposed to highlight the current date:
				if g_bHighlightReferenceDate then
					
					' if the current day is the same day as the reference date:
					if CDate(FormatDateTime(p_dDate, 2)) = CDate(FormatDateTime(dCurrentDay, 2)) then

						' Determine the new selected style based upon the previous style:
						if sCurrentTDStyle = "WeekView_Day" & nColor then
							sCurrentTDStyle = "WeekView_Day" & nColor & "Selected"
						elseif sCurrentTDStyle = "WeekView_Day" & nColor & "Current" then
							sCurrentTDStyle = "WeekView_Day" & nColor & "SelectedCurrent"
						end if
					end if
				end if
				
				' Print the Day:
				response.write "<tr><td class=""" & sCurrentTDStyle & """>"
				PrintDayForWeekView dCurrentDay
				response.write "</td></tr>"
			end if

			' Reset Filter:
			g_rsEvents.Filter = ""

			' Increment Placeholder variable and return to beginning of filtered RS:
			if not g_rsEvents.BOF then g_rsEvents.MoveFirst
			dCurrentDay = DateAdd("d", 1, dCurrentDay)
		Loop

		response.write "</table>"

	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------

	Private Function AlternateColor(p_nColor)
		if p_nColor = 1 then
			AlternateColor = 2
		else
			AlternateColor = 1
		end if
	End Function
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Sub PrintDayForWeekView(p_dDate)
		dim sDateStart, sDateEnd

		if isDate(p_dDate) then
			' Enforce DataType:
			'if vartype(p_dDate) <> 7 then p_dDate = CDate(p_dDate)
		else
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "PrintDayForWeekView: Invalid Date passed ('" & p_dDate & "')"
		end if
		
		if isNull(g_sDateLink) then
			sDateStart = "<div class=""WeekView_EventDate"">"
			sDateEnd = "</div>"
		else
			sDateStart = "<a class=""WeekView_EventDate"" href=""" & GenerateDateLink(p_dDate) & """>"
			sDateEnd = "</a><br>"
		end if

		response.write sDateStart & GetDayOfWeekName(p_dDate, True) & ", " & FormatNiceShortDate(p_dDate) & sDateEnd

		'response.write "<br>"
		'response.write "<hr size=""1"" width=""90%"" />" & vbCrLf
		
		if g_rsEvents.EOF then response.write "<div class=""WeekView_NoEventsFound"">No scheduled events.</div>" & vbCrLf
		
		' Go until the next date is detected:
		do until g_rsEvents.EOF
			
			response.write "<span class=""WeekView_EventTime"">"
			if GetNormalTime(g_rsEvents("Date")) = "12:00am" then 
				response.write "Task: "
			else
				response.write GetNormalTime(g_rsEvents("Date"))
			end if
			response.write "</span> - <span class=""WeekView_EventDescription"">" & g_rsEvents("Description") & "</span><br><br>" & vbCrLf

			' Next Event:
			g_rsEvents.MoveNext
		Loop
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	Private Function GetDayOfWeekName(p_dDate, p_bIndicateToday)
		if Vartype(p_dDate) <> 7 then p_dDate = CDate(p_dDate)

		dim nDayOfWeek, sRetVal

		if p_bIndicateToday then
			if CDate(FormatDateTime(p_dDate)) = Date then
				GetDayOfWeekName = "Today"
				exit function
			end if
		end if

		nDayOfWeek = DatePart("W", p_dDate)
		select case nDayOfWeek
			case 1
				sRetVal = "Sunday"
			case 2
				sRetVal = "Monday"
			case 3
				sRetVal = "Tuesday"
			case 4
				sRetVal = "Wednesday"
			case 5
				sRetVal = "Thursday"
			case 6
				sRetVal = "Friday"
			case 7
				sRetVal = "Saturday"
		end select

		GetDayOfWeekName = sRetVal
	End Function

	Private Function FormatNiceShortDate(p_dDate)
		if vartype(p_dDate) <> 7 then p_dDate = CDate(p_dDate)
		FormatNiceShortDate = Month(p_dDate) & "/" & Day(p_dDate) & "/" & Year(p_dDate)
	End Function

	Private Sub PrintDayView(p_dReferenceDate)

		dim dStartDateTime, dEndDateTime
		dim nColor, sTDClass
		dim sNextDateTime, sCurrentDateTime, dCurrentDateTime, dNextDateTime

		' Initialize Recordset:
		if not g_bIsRSEventSorted then g_rsEvents.Sort "Date"
		if not g_rsEvents.BOF then g_rsEvents.MoveFirst

		' Initialize Time Variables and Placeholders:
		dStartDateTime = CDate(FormatDateTime(p_dReferenceDate, 2) & " 12:00 AM")
		dEndDateTime = CDate(FormatDateTime(p_dReferenceDate, 2) & " 11:59:59 PM")
		dCurrentDateTime = dStartDateTime

		' Print the Table:
		response.write "<table width=""100%"" cellspacing=0 cellpadding=""5"" class=""DayView_Calendar"">" & vbCrLf

		' Print the Header Cell:
		if g_bShowSummaryHeader then
			response.write "<tr><td class=""DayView_SummaryHeader"">"
			response.write GetDayOfWeekName(p_dReferenceDate, True) & " " & FormatDateTime(p_dReferenceDate, 2)
			response.write "</td></tr>" & vbCrLf
		end if

		if g_rsEvents.eof then
			response.write("<tr><td>" & vbcrlf)
			response.write("There are no events for this date.")
			response.write("</td></tr>" & vbcrlf)
		end if
		
		' If we are only viewing business hours: - display before business hours contacts
		if not g_bShowAllTimeSlotsInDayView then
			dStartDateTime = CDate(FormatDateTime(p_dReferenceDate, 2) & " " & FormatDateTime(g_dStartOfBusinessDay, 3))
			dEndDateTime = CDate(FormatDateTime(p_dReferenceDate, 2) & " " & FormatDateTime(g_dEndOfBusinessDay, 3))
			dCurrentDateTime = dStartDateTime
	
			' See if any events are in this exception period:
			g_rsEvents.Filter = "Date >= '" & FormatDateTime(p_dReferenceDate, 2) & " 12:00:00 AM' AND " _
							& "Date < '" & FormatDateTime(p_dReferenceDate , 2) & " " & FormatDateTime(g_dStartOfBusinessDay, 3) & "'"
			
			if not g_rsEvents.EOF then
				'response.write "<tr><td class=""DayView_ExceptionTimeBlock"">"
'				response.write "<div class=""DayView_ExceptionExplanation"">Events occurring before business hours</div>"
				call PrintDayViewRecords("","DayView_TimeBlock1")
				'response.write "</td></tr>"
			end if
		end if

		nColor = 2
		Do until dCurrentDateTime > dEndDateTime
			sNextDateTime = DateAdd("n", g_nDayViewTimeInterval, dCurrentDateTime)
			dNextDateTime = CDate(sNextDateTime)
			sCurrentDateTime = FormatDateTime(dCurrentDateTime, 2) & " " & FormatDateTime(dCurrentDateTime, 3)

			' Filter Events for the current timeblock:
			g_rsEvents.Filter = "Date >= '" & sCurrentDateTime & "' AND " _
							& "Date < '" & sNextDateTime & "'"
			
			' Determine the visual information for this cell:
			nColor = AlternateColor(nColor)
			sTDClass = "DayView_TimeBlock" & nColor

			' Is this TimeBlock Selected or Highlighted (matches reference date)?
			if g_bHighlightReferenceDate then
				if (p_dReferenceDate >= dCurrentDateTime) and (p_dReferenceDate < dNextDateTime) then
					sTDClass = sTDClass & "Selected"
				end if
			end if

			' Is this TimeBlock Current (matches current date)?
			if (Now >= dCurrentDateTime) and (Now < dNextDateTime) then
				sTDClass = sTDClass & "Current"
			end if

			' Print calendar here ---------------
'			response.write "<tr><td class=""" & sTDClass & """>"
'			response.write "<div class=""DayView_BlockTime"">"
'			if not isNull(g_sTimeLink) then response.write "<a class=""DayView_BlockTime"" href=""" & GenerateTimeLink(dCurrentDateTime) & """>"
'			response.write GetNormalTime(dCurrentDateTime)
'			if not isNull(g_sTimeLink) then response.write "</a>"
'			response.write "</div>"
			call PrintDayViewRecords(dCurrentDateTime,sTDClass)
'			response.write "</td></tr>" & vbCrLf

			' Increment datetime placeholder:
			dCurrentDateTime = dNextDateTime
		Loop


		' If we are only viewing business hours: - show all after business hours contacts
		if not g_bShowAllTimeSlotsInDayView then

			' See if any events are in this exception period:
			g_rsEvents.Filter = "Date >= '" & FormatDateTime(p_dReferenceDate, 2) & " " & FormatDateTime(dCurrentDateTime, 3) & "' AND " _
							& "Date <= '" & FormatDateTime(p_dReferenceDate , 2) & " 11:59:59 PM'"
			
			if not g_rsEvents.EOF then
				response.write "<tr><td class=""DayView_ExceptionTimeBlock"">"
'				response.write "<div class=""DayView_ExceptionExplanation"">Events occurring after business hours</div>"
				call PrintDayViewRecords(dCurrentDateTime,sTDClass)
				response.write "</td></tr>"
			end if
		end if
		
		response.write "</table>"

		' Reset the Filter:
		g_rsEvents.Filter = ""
		if not g_rsEvents.BOF then g_rsEvents.MoveFirst

		response.write "<p align=""center""><a href=""javascript:history.back()""><img src=""" & application("sDynMediaPath") & "bttnBack.gif"" border=0></a>"
	End Sub

	' Will print day-view events from the event recordset:
	Private Sub PrintDayViewRecords(p_dCurrentDateTime,p_sTDClass)
		dim bMovedNext, dCurrentTime, nCount
		do until g_rsEvents.EOF
			' Init MovedNext Flag:
			'bMovedNext = False

			nCount = 0
			
			if not g_rsEvents.eof then
				response.write "<tr><td class=""" & p_sTDClass & """>"		
				if trim(p_dCurrentDateTime) <> "" then	
					'response.write "<div class=""DayView_BlockTime"">"			
					'response.write GetNormalTime(p_dCurrentDateTime)
					'response.write "</div>"	
				end if
				
				do while not g_rsEvents.eof
					if dCurrentTime <> GetNormalTime(g_rsEvents("Date")) then
						'if no time, then do not display
						if instr(g_rsEvents("Date"),":") > 0 then
							dCurrentTime = GetNormalTime(g_rsEvents("Date"))
							if nCount > 0 then response.write "<hr size=""1"" width=""90%"" />"
							response.write "<div class=""DayView_EventTime"">" & dCurrentTime & "</div>"
						end if
					end if
					response.write "<span class=""DayView_EventDescription""> - " & g_rsEvents("Description") &  "</span><br />"

					g_rsEvents.MoveNext
					nCount = nCount + 1
					'bMovedNext = True
				Loop
				response.write "</td></tr>" & vbCrLf
			end if
			
			'if not g_rsEvents.EOF then
			'	if not bMovedNext then g_rsEvents.MoveNext
			'	
			'	if g_rsEvents("Date") <> dCurrentTime then response.write "<hr size=""1"" width=""90%"" />"
			'end if
		Loop
	End Sub


	Private Function GetMonthName(p_nMonth, p_bAbbreviated)
		p_nMonth = Cint(p_nMonth)

		dim sRetVal
		sRetVal = "Unknown"

		select case p_nMonth
			case 1
				if p_bAbbreviated then sRetVal = "Jan" else sRetVal = "January"
			case 2
				if p_bAbbreviated then sRetVal = "Feb" else sRetVal = "February"
			case 3
				if p_bAbbreviated then sRetVal = "Mar" else sRetVal = "March"
			case 4
				if p_bAbbreviated then sRetVal = "Apr" else sRetVal = "April"
			case 5
				if p_bAbbreviated then sRetVal = "May" else sRetVal = "May"
			case 6
				if p_bAbbreviated then sRetVal = "Jun" else sRetVal = "June"
			case 7
				if p_bAbbreviated then sRetVal = "Jul" else sRetVal = "July"
			case 8
				if p_bAbbreviated then sRetVal = "Aug" else sRetVal = "August"
			case 9
				if p_bAbbreviated then sRetVal = "Sep" else sRetVal = "September"
			case 10
				if p_bAbbreviated then sRetVal = "Oct" else sRetVal = "October"
			case 11
				if p_bAbbreviated then sRetVal = "Nov" else sRetVal = "November"
			case 12
				if p_bAbbreviated then sRetVal = "Dec" else sRetVal = "December"
		end select
	End Function

	' PROPERTY DEFINITION ======================================================================================
	Public Property Let TemporaryRecordsetFile(p_sText)
		dim objFS
		set objFS = CreateObject("Scripting.FileSystemObject")
		
		if not objFS.FileExists(p_sText) then
			set objFS = Nothing
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "The file, '" & p_sText & "' did not exist."
			Exit property
		end if

		g_sSampleRecordsetLocation = p_sText
		if g_rsEvents.State = adStateOpen then g_rsEvents.Close
		g_rsEvents.Open g_sSampleRecordsetLocation

		set objFS = Nothing
	End Property

	Public Property Get TemporaryRecordsetFile()
		TemporaryRecordsetFile = g_sSampleRecordsetLocation
	End Property

	Public Property Let Mode(p_sText)

		' Convert Empty Values to 0 length string:
		if isEmpty(p_sText) or isNull(p_sText) then p_sText = ""

		' Convert to Lower Case:
		p_sText = lcase(p_sText)

		if p_sText = "mini" or p_sText = "day" or p_sText = "week" or p_sText = "month" then
			g_sMode = p_sText
		else
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "The mode you specified must be equal to, ""mini"", ""day"", ""week"", or ""month.""  The current mode is, """ & g_sMode & "."""
		end if
	End Property

	Public Property Get Mode()
		Mode = g_sMode
	End Property

	Public Property Let StartOfBusinessDay(p_sText)
		if isDate(p_sText) then
			g_dStartOfBusinessDay = CDate(p_sText)
		else
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "You specified an invalid value for the StartOfBusinessDay Property (Value = '" & p_sText & "')"
		end if
	End Property

	Public Property Get StartOfBusinessDay()
		StartOfBusinessDay = g_dStartOfBusinessDay
	End Property

	Public Property Let EndOfBusinessDay(p_sText)
		if isDate(p_sText) then
			g_dEndOfBusinessDay = CDate(p_sText)
		else
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "You specified an invalid value for the EndOfBusinessDay Property (Value = '" & p_sText & "')"
		end if
	End Property

	Public Property Get EndOfBusinessDay()
		EndOfBusinessDay = g_dEndOfBusinessDay
	End Property

	Public Property Let DateLink(p_sText)
		if isEmpty(p_sText) then p_sText = Null
		if p_sText = "" then p_sText = Null

		g_sDateLink = p_sText
	End Property

	Public Property Get DateLink()
		DateLink = g_sDateLink
	End Property
	
	Public Property Let EmptyDateLink(p_sText)
		if isEmpty(p_sText) then p_sText = Null
		if p_sText = "" then p_sText = Null
		
		g_sEmptyDateLink = p_sText
	end property

	Public Property Let MaxEventsPerCalendarViewGrid(p_sText)
		if isNumeric(p_sText) then
			g_nMaxEventsPerCalendarViewGrid = CInt(p_sText)
		else
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "You specified an invalid value for the MaxEventsPerCalendarViewGrid Property (Value = '" & p_sText & "')"
		end if
	End Property

	Public Property Get MaxEventsPerCalendarViewGrid()
		MaxEventsPerCalendarViewGrid = g_nMaxEventsPerCalendarViewGrid
	End Property

	Public Property Let HighlightReferenceDate(p_sText)
		if not p_sText then p_sText = False
		g_bHighlightReferenceDate = p_sText
	End Property

	Public Property Get HighlightReferenceDate()
		HighlightReferenceDate = g_bHighlightReferenceDate
	End Property
  

	Public Property Let ShowSummaryHeader(p_sText)
		if not p_sText then p_sText = False
		g_bShowSummaryHeader = p_sText
	End Property

	Public Property Get ShowSummaryHeader()
		ShowSummaryHeader = g_bShowSummaryHeader
	End Property

	Public Property Let ShowEmptyDaysInWeekView(p_sText)
		if not p_sText then p_sText = False
		g_bShowEmptyDaysInWeekView = p_sText
	End Property

	Public Property Get ShowEmptyDaysInWeekView()
		ShowEmptyDaysInWeekView = g_bShowEmptyDaysInWeekView
	End Property

	Public Property Let ShowAllTimeSlotsInDayView(p_sText)
		if not p_sText then p_sText = False
		g_bShowAllTimeSlotsInDayView = p_sText
	End Property

	Public Property Get ShowAllTimeSlotsInDayView()
		ShowAllTimeSlotsInDayView = g_bShowAllTimeSlotsInDayView
	End Property

	Public Property Let DayViewTimeIntelic (p_sText)
		if not isNumeric(p_sText) then
			DestroyTemporaryRecordset
			Err.Raise g_nDefaultErrNumber, g_sApplicationName, "You must specify a numeric value for the DayViewTimeInterval Property."
			Exit Property
		else
			p_sText = Cint(p_sText)
		end if

		' Accept 5, 10, 15, 30, 60, 120, 240
		select case p_sText
			case 5
				' we aight
			case 10
				' we aight
			case 15
				' we aight
			case 30
				' we aight
			case 60
				' we aight
			case 120
				' we aight
			case 240
				' we aight
			case else
				Err.Raise g_nDefaultErrNumber, g_sApplicationName, "DayViewTimeInterval Property: the only values accepted are 5, 10, 15, 30, 60, 120, and 240"
				Exit Property
		end select

		g_nDayViewTimeInterval = p_sText
	End Property

	Public Property Get DayViewTimeInterval()
		DayViewTimeInterval = g_nDayViewTimeInterval
	End Property

	Public Property Let TimeLink(p_sText)
		if isEmpty(p_sText) then p_sText = Null
		if p_sText = "" then p_sText = Null

		g_sTimeLink = p_sText
	End Property

	Public Property Get TimeLink()
		TimeLink = g_sTimeLink
	End Property

	Public Property Let ReturnPage(p_sText)
		if isEmpty(p_sText) then p_sText = Null
		if p_sText = "" then p_sText = Null

		g_sReturnPage = p_sText
	End Property

	Public Property Get ReturnPage()
		ReturnPage = g_sReturnPage
	End Property

	'===========================================================================================================
End Class
%>
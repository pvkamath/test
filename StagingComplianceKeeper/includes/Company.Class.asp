<%
'==============================================================================
' Class: Company
' Controls the creation, modification, and removal of Companies.
' Create Date: 12/03/04
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
' Company Properties:
'	- Active
'	- Address
'	- Address2
'	- Branches
'	- BusTypeId
'	- City
'	- CompanyId
'	- CountryId
'	- DBA
'	- EIN
'	- Email
'	- Fax
'	- FiscalYearEndMonth
'	- FiscalYearEndDay
'	- IncorpStateId
'	- IncorpDate
'	- LegalName
'	- LogoFileName
'	- MailingAddress
'	- MailingAddress2
'	- MailingCity
'	- MailingCountry
'	- MailingStateId
'	- MailingZipcode
'	- MailingZipcodeExt
'	- MainContactEmail
'	- MainContactFax
'	- MainContactName
'	- MainContactPhone
'	- MainContactPhoneExt
'	- Name
'	- OrgTypeId
'	- Phone
'	- PhoneExt
'	- Phone2
'	- Phone2Ext
'	- Phone3
'	- Phone3Ext
'	- StateId
'	- Website
'	- Zipcode
'	- ZipcodeExt
' Methods:
'	- CheckConnectionString()
' 	- CheckIntParam(p_iInt, p_bAllowNull, p_sName)
'	- CheckStrParam(p_sStr, p_bAllowNull, p_sName)
'	- DeleteId(p_iCompanyId)
'	- QueryDb(sSQL)
'	- QueryTpDb(sSQL)
'	- ReportError(p_sError)
'
'	- AddNewDba(p_sNewDba)
'	- DeleteCompany()
'	- DeleteCompanyById(p_iCompanyId)
'   - DeleteDba(p_sDba)
'	- GetAdmins(p_iCompanyId)
'   - GetDbaRecordset()
'	- LoadCompanyById(p_iBranchId)
'	- LoadCompanyByName(p_sName)
'	- LookupBusType(p_iBusTypeId)
'	- LookupCorporateLogo()
'	- LookupLicenseStatus(p_iCompanyId)
'	- LookupOrgType(p_iOrgTypeId)
'	- LookupOrgTypeByName(p_sOrgType)
'	- LookupPricingPlanMaxOfficers(p_iPlanId)
'	- LookupState(p_iStateId)
'	- LookupTpCompanyName(p_iTpCompanyId)
'	- Safe(p_sText)
'	- SaveCompany()
'	- SearchCompanies()
'==============================================================================

Class Company

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Base company properties
	dim g_bActive
	dim g_sAddress
	dim g_sAddress2
	dim g_sBranchManager
	dim g_iBusTypeId
	dim g_sCity
	dim g_sComments
	dim g_iCompanyId
	dim g_iCountryId
	dim g_sDba
	dim g_sEin
	dim g_sEmail
	dim g_sFax
	dim g_iFiscalYearEndDay
	dim g_iFiscalYearEndMonth
	dim g_dIncorpDate
	dim g_iIncorpStateId
	dim g_sLegalName
	dim g_sLogoFileName
	dim g_sMailingAddress
	dim g_sMailingAddress2
	dim g_sMailingCity
	dim g_iMailingCountryId
	dim g_iMailingStateId
	dim g_sMailingZipcode
	dim g_sMailingZipcodeExt
	dim g_sMainContactEmail
	dim g_sMainContactFax
	dim g_sMainContactName
	dim g_sMainContactPhone
	dim g_sMainContactPhoneExt	
	dim g_sName
	dim g_iOrgTypeId
	dim g_sPhone
	dim g_sPhoneExt
	dim g_sPhone2
	dim g_sPhone2Ext
	dim g_sPhone3
	dim g_sPhone3Ext
	dim g_iPricingPlan
	dim g_iStateId
	dim g_iTpLinkCompanyId
	dim g_bUseWithCms
	dim g_sWebsite
	dim g_sZipcode
	dim g_sZipcodeExt
	
	' PUBLIC PROPERTIES =======================================================

	'Misc properties
	
	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
		'We set the BranchList connectionstring from here as well
'		g_oBranchList.ConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	


	'Stores/retrieves the trainingpro database connection string.
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
		'We set the BranchList connectionstring from here as well
'		g_oBranchList.TpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property	
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
'			g_oBranchList.VocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	'Base company properties
	
	'Whether or not the company is currently enabled and active.  Disabled
	'companies do not appear on the site.
	public property get Active()
		Active = g_bActive
	end property
	public property let Active(p_bActive)
		if isnumeric(p_bActive) then
			g_bActive = cbool(p_bActive)
		else
			ReportError("Invalid Active value.  Boolean required.")
		end if 	
	end property
	
	'Stores/retrieves the Company address
	public property let Address(p_sAddress)
		g_sAddress = p_sAddress
	end property
	public property get Address()
		Address = g_sAddress
	end property
	
	'Address line two
	public property get Address2()
		Address2 = g_sAddress2
	end property
	public property let Address2(p_sAddress2)
		g_sAddress2 = p_sAddress2
	end property
	
	'Managing Principle
	public property get BranchManager()
		BranchManager = g_sBranchManager
	end property
	public property let BranchManager(p_sBranchManager)
		g_sBranchManager = left(trim(p_sBranchManager), 150)
	end property
	
	'Business type
	public property get BusTypeId()
		BusTypeId = g_iBusTypeId
	end property
	public property let BusTypeId(p_iBusTypeId)
		if isnumeric(p_iBusTypeId) then
			g_iBusTypeId = abs(cint(p_iBusTypeId))
		else
			ReportError("Invalid BusTypeId value.  Integer required.")
		end if 
	end property
	
	'Stores/retrieves the Company city
	public property let City(p_sCity)
		g_sCity = p_sCity
	end property
	public property get City()
		City = g_sCity
	end property
	
	'Retrieves the unique ID of this Company.  This is an identity in the DB, 
	'so it cannot be set.
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	
	'Administrator comments about the company.
	public property get Comments()
		Comments = g_sComments
	end property
	public property let Comments(p_sComments)
		g_sComments = p_sComments
	end property
	
	'Country ID Assigned to this address
	public property get CountryId
		CountryId = g_iCountryId
	end property
	public property let CountryId(p_iCountryId)
		if isnumeric(p_iCountryId) then
			g_iCountryId = p_iCountryId
		elseif p_iCountryId = "" then
			g_iCountryId = ""
		else
			ReportError("Invalid CountryId value.  Integer required.")
		end if 
	end property
	
	'DBA - Doing Business As.  Comma separated list of alternate company
	'names.  Read-only.  Change using AddNewDba and DeleteDba.
	public property get DbaList()
		DbaList = g_sDba
	end property
	public property let Dba(p_sDba) 
		g_sDba = p_sDba
	end property
	
	'EIN - Employer Identification Number
	public property get Ein()
		Ein = g_sEin
	end property
	public property let Ein(p_sEin)
		g_sEin = left(p_sEin, 50)
	end property
	
	'User email address
	public property get Email()
		Email = g_sEmail
	end property
	public property let Email(p_sEmail)
		g_sEmail = left(trim(p_sEmail), 50)
	end property
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = p_sFax
	end property
	
	'Date of the end of the fiscal year
	public property get FiscalYearEndDay()
		FiscalYearEndDay = g_iFiscalYearEndDay
	end property
	public property let FiscalYearEndDay(p_iFiscalYearEndDay)
		if isdate(p_iFiscalYearEndDay) then
			g_iFiscalYearEndDay = p_iFiscalYearEndDay
		else
			ReportError("Invalid FiscalYearEndDay value.  Integer required.")
		end if 
	end property
	
	'Date of the end of the fiscal year
	public property get FiscalYearEndMonth()
		FiscalYearEndMonth = g_iFiscalYearEndMonth
	end property
	public property let FiscalYearEndMonth(p_iFiscalYearEndMonth)
		if isdate(p_iFiscalYearEndMonth) then
			g_iFiscalYearEndMonth = p_iFiscalYearEndMonth
		else
			ReportError("Invalid FiscalYearEndMonth value.  Integer required.")
		end if 
	end property
	
	'Date of Incorporation
	public property get IncorpDate()
		IncorpDate = g_dIncorpDate
	end property
	public property let IncorpDate(p_dIncorpDate)
		if isdate(p_dIncorpDate) then
			g_dIncorpDate = p_dIncorpDate
		elseif p_dIncorpDate = "" then
			g_dIncorpDate = ""
		else
			ReportError("Invalid IncorpDate value.  Date required.")
		end if 
	end property
	
	'State of Incorporation
	public property get IncorpStateId()
		IncorpStateId = g_iIncorpStateId
	end property
	public property let IncorpStateId(p_iIncorpStateId)
		if isnumeric(p_iIncorpStateId) then
			g_iIncorpStateId = abs(cint(p_iIncorpStateId))
		else
			Response.Write("Invalid IncorpStateId value.  Integer required.")
		end if 
	end property
	
	'Legal name of the company
	public property let LegalName(p_sLegalName)
		g_sLegalName = left(p_sLegalName, 100)
	end property
	public property get LegalName()
		LegalName = g_sLegalName
	end property
	
	'File name of the stored company logo
	public property get LogoFileName()
		LogoFileName = g_sLogoFileName
	end property
	public property let LogoFileName(p_sLogoFileName)
		g_sLogoFileName = p_sLogoFileName
	end property
	
	'Mailing Address line 1
	public property get MailingAddress()
		MailingAddress = g_sMailingAddress
	end property
	public property let MailingAddress(p_sMailingAddress)
		g_sMailingAddress = p_sMailingAddress
	end property 
	
	'Mailing Address line 2
	public property get MailingAddress2()
		MailingAddress2 = g_sMailingAddress2
	end property
	public property let MailingAddress2(p_sMailingAddress2)
		g_sMailingAddress2 = p_sMailingAddress2
	end property
	
	'Mailing address city
	public property get MailingCity()
		MailingCity = g_sMailingCity
	end property
	public property let MailingCity(p_sMailingCity)
		g_sMailingCity = p_sMailingCity
	end property
	
	'Mailing Country ID Assigned to this address
	public property get MailingCountryId
		MailingCountryId = g_iMailingCountryId
	end property
	public property let MailingCountryId(p_iMailingCountryId)
		if isnumeric(p_iMailingCountryId) then
			g_iMailingCountryId = p_iMailingCountryId
		elseif p_iMailingCountryId = "" then
			g_iMailingCountryId = ""
		else
			ReportError("Invalid MailingCountryId value.  Integer required.")
		end if 
	end property
	
	'Mailing address state id
	public property get MailingStateId()
		MailingStateId = g_iMailingStateId
	end property
	public property let MailingStateId(p_iMailingStateId)
		if isnumeric(p_iMailingStateId) then
			g_iMailingStateId = abs(cint(p_iMailingStateId))
		else
			ReportError("Invalid MailingStateId value.  Integer required.")
		end if 
	end property
	
	'Mailing address zipcode
	public property get MailingZipcode()
		MailingZipcode = g_sMailingZipcode
	end property 
	public property let MailingZipcode(p_sMailingZipcode)
		g_sMailingZipcode = p_sMailingZipcode
	end property

	'Optional zipcode extension
	public property get MailingZipcodeExt()
		MailingZipcode = g_sMailingZipcodeExt
	end property
	public property let MailingZipcodeExt(p_sMailingZipcodeExt)
		g_sMailingZipcodeExt = left(p_sMailingZipcodeExt, 4)
	end property
	
	'Main contact email
	public property get MainContactEmail()
		MainContactEmail = g_sMainContactEmail
	end property
	public property let MainContactEmail(p_sMainContactEmail)
		g_sMainContactEmail = p_sMainContactEmail
	end property

	'Main contact fax
	public property get MainContactFax()
		MainContactFax = g_sMainContactFax
	end property 
	public property let MainContactFax(p_sMainContactFax)
		g_sMainContactFax = p_sMainContactFax
	end property
	
	'Main contact full name
	public property get MainContactName()
		MainContactName = g_sMainContactName
	end property
	public property let MainContactName(p_sMainContactName)
		g_sMainContactName = p_sMainContactName
	end property
	
	'Main contact phone number
	public property get MainContactPhone()
		MainContactPhone = g_sMainContactPhone
	end property
	public property let MainContactPhone(p_sMainContactPhone)
		g_sMainContactPhone = p_sMainContactPhone
	end property
	
	'Main contact phone extension
	public property get MainContactPhoneExt()
		MainContactPhoneExt = g_sMainContactPhoneExt
	end property
	public property let MainContactPhoneExt(p_sMainContactPhoneExt)
		g_sMainContactPhoneExt = p_sMainContactPhoneExt
	end property
	
	'Stores/retrieves the Company name.
	public property let Name(p_sName)
		g_sName = p_sName
	end property
	public property get Name()
		Name = g_sName
	end property
	
	'Organization type
	public property get OrgTypeId()
		OrgTypeId = g_iOrgTypeId
	end property
	public property let OrgTypeId(p_iOrgTypeId)
		if isnumeric(p_iOrgTypeId) then
			g_iOrgTypeId = abs(cint(p_iOrgTypeId))
		else
			ReportError("Invalid OrgTypeId value.  Integer required.")
		end if 
	end property
	
	'Phone number as a string
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = p_sPhone
	end property
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = p_sPhoneExt
	end property

	'Phone number as a string
	public property get Phone2()
		Phone2 = g_sPhone2
	end property
	public property let Phone2(p_sPhone2)
		g_sPhone2 = p_sPhone2
	end property
	
	'Phone number extension
	public property get PhoneExt2()
		PhoneExt2 = g_sPhone2Ext
	end property
	public property let PhoneExt2(p_sPhoneExt2)
		g_sPhone2Ext = p_sPhoneExt2
	end property
	
	'Phone number as a string
	public property get Phone3()
		Phone3 = g_sPhone3
	end property
	public property let Phone3(p_sPhone3)
		g_sPhone3 = p_sPhone3
	end property
	
	'Phone number extension
	public property get PhoneExt3()
		PhoneExt3 = g_sPhone3Ext
	end property
	public property let PhoneExt3(p_sPhoneExt3)
		g_sPhone3Ext = p_sPhoneExt3
	end property
	
	'ID of the pricing plan which this company is signed up to.  This value is
	'used to track the maximum allowable number of loan officers in the company
	'system.
	public property get PricingPlanId()
		PricingPlanId = g_iPricingPlan
	end property
	public property let PricingPlanId(p_iPricingPlan)
		if isnumeric(p_iPricingPlan) then
			g_iPricingPlan = clng(p_iPricingPlan)
		elseif p_iPricingPlan = "" then
			g_iPricingPlan = ""
		else
			ReportError("Invalid PricingPlan value.  Integer required.")
		end if
	end property
	
	'Retrieves the Company state name.  User should change the state via 
	'the StateId value.  StateName is read-only.
	public property get State()
		dim sSql, oRs
		
		sSql = "SELECT State FROM States WHERE StateID = '" & g_iStateId & "'"
		
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			State = oRs("State")
		
		else
		
			State = ""
		
		end if
		
		set oRs = nothing
	end property
	
	'Stores/retrieves the Company state ID
	public property let StateId(p_iStateId)
		g_iStateId = p_iStateId
	end property
	public property get StateId()
		StateId = g_iStateId
	end property

	'Is there a TrainingPro company ID which we should link with this company?
	'This is used to share course data between CMS companies and a users of a 
	'specific TrainingPro company.
	public property get TpLinkCompanyId()
		TpLinkCompanyId = g_iTpLinkCompanyId
	end property
	public property let TpLinkCompanyId(p_iTpLinkCompanyId)
		if isnumeric(p_iTpLinkCompanyId) then
			g_iTpLinkCompanyId = clng(p_iTpLinkCompanyId)
		elseif p_iTpLinkCompanyId = "" then
			g_iTpLinkCompanyId = ""
		else
			ReportError("Invalid TpCompanyLinkId value.  Integer required.")
		end if
	end property
	
	'Decides whether TrainingPro companies will be shown in CMS
	public property get UseWithCms()
		UseWithCms = g_bUseWithCms
	end property
	public property let UseWithCms(p_bUseWithCms)
		if isnumeric(p_bUseWithCms) then
			g_bUseWithCms = cbool(p_bUseWithCms)
		elseif p_bUseWithCms = "" then
			g_bUseWithCms = ""
		else
			ReportError("Invalid UseWithCms value.  Boolean required.")
		end if
	end property
	
	'Website URL
	public property get Website()
		Website = g_sWebsite
	end property
	public property let Website(p_sWebsite)
		g_sWebsite = p_sWebsite
	end property
	
	'Stores/retrieves the Company zip.
	public property let Zipcode(p_sZipcode)
		g_sZipcode = p_sZipcode
	end property
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	
	'Optional zipcode extension
	public property get ZipcodeExt()
		Zipcode = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		g_sZipcodeExt = left(p_sZipcodeExt, 4)
	end property
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
			
		elseif g_sConnectionString = "" then

			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false

'We are no longer using direct TP connections
'		
'		elseif isnull(g_sTpConnectionString) then 
'			
'			ReportError("Alternate ConnectionString property must be set.")
'			CheckConnectionString = false
'			
'		elseif g_sTpConnectionString = "" then
'			
'			ReportError("Alternate ConnectionString property must be set.")
'			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  

	' -------------------------------------------------------------------------
	' Name:				DeleteId
	' Description:		Delete the specified Company object from the relevant 
	'					database tables
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iCompanyId - Company ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iCompanyId)
	
		DeleteId = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			DeleteId = 0
			exit function
		elseif not CheckIntParam(p_iCompanyId, 1, "Company ID") then
			DeleteId = 0
			exit function		
		end if
		
	
		dim oConn 'DB connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")

		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		
		'We'll clean out our records from the database tables.
		sSql = "UPDATE Companies SET " & _
			"Active = '0' " & _
			"WHERE CompanyId = '" & p_iCompanyId & "'"
		oConn.Execute sSql, lRecordsAffected
		
		
		'Verify that at least one record was affected.
		if not lRecordsAffected > 0 then
		
			ReportError("Failure deleting the company record.")
			DeleteId = 0
			exit function		
		
		end if 

			
		DeleteId = 1
	
	end function
	
	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		'Response.Write(sSql)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function

	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    'Response.Write(sSql)
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function

	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name:				AddNewDba
	' Description:		Adds a new DBA to the DBA list variable
	' Pre-conditions:	None.
	' Inputs:			p_sDba - New DBA name
	' Outputs:			None.
	' -------------------------------------------------------------------------
	public sub AddNewDba(p_sNewDba)
	
		dim sDbaArray 'DBA list in array format
		dim sDba 'Individual DBA name
		dim bExists 'Does the passed name already exist in the list?
	
		bExists = false
		sDbaArray = split(g_sDba, ", ")
		
		for each sDba in sDbaArray 
		
			if ucase(sDba) = ucase(p_sNewDba) then
				
				bExists = true
				ReportError("This DBA already exists for this Company.")
			
			end if 
		
		next
		
		
		'Add the new DBA if it is not already in the list.
		if not bExists then 
		
			if g_sDba <> "" then 
				g_sDba = g_sDba & ", " 
			end if
			
			g_sDba = g_sDba & p_sNewDba
		
		end if
		
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				DeleteCompany
	' Description:		Delete the loaded Company object from the relevant
	'                   database tables
	' Pre-conditions:	ConnectionString, CompanyId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteCompany()
	
		DeleteCompany = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iCompanyId, 0, "Company ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(g_iCompanyId)
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete this company.")
			DeleteCompany = 0
		
		else
		
			'Hooray
			DeleteCompany = 1
		
		end if 
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				DeleteCompanyById
	' Description:		Delete the specified Company object from the relevant
	'					database tables
	' Preconditions:	ConnectionString
	' Inputs:			p_iCompanyId - Company ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteCompanyById(p_iCompanyId)
	
		DeleteCompanyById = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(p_iCompanyId)
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete the specified company.")
			DeleteCompanyById = 0
		
		else
		
			'Hooray
			DeleteCompanyById = 1
		
		end if 
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				DeleteDba
	' Description:		Removes the specified DBA from the DBA list
	' Pre-conditions:	None.
	' Inputs:			p_sDba - DBA to remove
	' Outputs:			None.
	' -------------------------------------------------------------------------
	public sub DeleteDba(p_sDba)
	
		dim sDbaArray 'DBA list in array format
		dim sDba 'Individual DBA name
		dim bExists 'Does the passed name already exist in the list?
	
		bExists = false
		sDbaArray = split(g_sDba, ", ")
		
		'Clear the original DBA variable and loop through the array.
		g_sDba = ""
		for each sDba in sDbaArray 
		
			'Rebuild the list, adding each item back if it is not the one we're
			'deleting.
			if ucase(sDba) <> ucase(p_sDba) then
				
				g_sDba = g_sDba & sDba & ", " 
			
			end if 
		
		next
		
		'Remove trailing comma.
		g_sDba = left(g_sDba, len(g_sDba) - 2)
		
	end sub
	
	' -------------------------------------------------------------------------
	' Name: GetAdmins
	' Desc: Returns a recordset containing the admins and their emails
	' Preconditions: ConnectionString, p_iCompanyId
	' Returns: Recordset
	' -------------------------------------------------------------------------
	public function GetAdmins(p_iCompanyId)
		
		set GetAdmins = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if
		
		dim sSql 'SQL Query		
		
		sSql = "SELECT DISTINCT Us.UserId, Us.FirstName, Us.MiddleName, " & _
			"Us.LastName, Us.Email " & _
			"FROM Users AS Us " & _
			"WHERE Us.CompanyId = '" & p_iCompanyId & "' " & _
			"AND Us.GroupId = '2' " & _
			"AND Us.UserStatus = '1' "
				
		'Response.Write("<p>" & sSql & "<p>")
		set GetAdmins = QueryDb(sSql)
		
	end function	
	
	' -------------------------------------------------------------------------
	' Name:				GetDbaRecordset
	' Description:		Retrieves a recordset of the CompanyDbas table where
	'					the entry matches the current companyID
	' Pre-conditions:	ConnectionString, CompanyID
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function GetDbaRecordset()
	
		set GetDbaRecordset = Server.CreateObject("ADODB.Recordset")
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iCompanyId, 0, "Company ID") then
			exit function		
		end if
	
		
		dim sSql 'SQL Query text
		
		sSql = "SELECT * FROM CompaniesDbasX " & _
		       "WHERE CompanyId = '" & g_iCompanyId & "'"
		set GetDbaRecordset = QueryDb(sSql)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LoadCompanyByBranchId
	' Desc: Attempts to load the properties of a stored company that is
	'		associated with the passed branch ID.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_iBranchId - The Branch ID to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyByBranchId(p_iBranchId)
	
		LoadCompanyByBranchId = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iCompanyId 'Found Company ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT CompanyId FROM CompanyBranches " & _
			"WHERE BranchId = '" & p_iBranchId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a recordset back, so there is a branch record in the DB for
			'the Branch ID we were passed.
			iCompanyId = oRs("CompanyId")
			
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iCompanyId)) or iCompanyId = "" then
			
			ReportError("No valid Company associated with this Branch ID.")
			exit function
			
		end if 
		
				
		'Use the found Company ID to load the company.
		LoadCompanyByBranchId = LoadCompanyById(iCompanyId)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LoadCompanyById
	' Description:		This method attempts to load the properties of a stored
	'					Company based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyById(p_iCompanyId)
	
		LoadCompanyById = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyId, 1, "Company ID") then
			exit function		
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")

		sSql = "SELECT * FROM Companies " & _
			"WHERE CompanyId = '" & p_iCompanyId & "'"
		set oRs = QueryDb(sSql)
		
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iCompanyId = oRs("CompanyID")
			g_sName = oRs("Name")
			g_sLegalName = oRs("LegalName")
			g_bActive = oRs("Active")
			g_sLogoFileName = oRs("LogoFileName")
			'g_bUseWithCms = oRs("UseWithCms")
			if oRs("TpLinkCompanyId") = 0 then
				g_iTpLinkCompanyId = ""
			else
				g_iTpLinkCompanyId = oRs("TpLinkCompanyId")
			end if
			g_iPricingPlan = oRs("PricingPlanId")
			
			'g_sDba = oRs("Dba")
			g_iIncorpStateId = oRs("IncorpStateId")
			g_dIncorpDate = oRs("IncorpDate")
			g_iFiscalYearEndMonth = oRs("FiscalYearEndMonth")
			g_iFiscalYearEndDay = oRs("FiscalYearEndDay")
			g_iOrgTypeId = oRs("OrgTypeId")
			g_iBusTypeId = oRs("BusTypeId")
			g_sEin = oRs("Ein")
			
			g_sAddress = oRs("Address")
			g_sAddress2 = oRs("Address2")
			g_sCity = oRs("City")
			g_iStateId = oRs("StateID")
			g_sZipcode = oRs("Zipcode")
			g_sZipcodeExt = oRs("ZipcodeExt")
			g_iCountryId = oRs("CountryId")
			g_sWebsite = oRs("Website")
			g_sEmail = oRs("Email")
			g_sPhone = oRs("Phone")
			g_sPhoneExt = oRs("PhoneExt")
			g_sPhone2 = oRs("Phone2")
			g_sPhone2Ext = oRs("Phone2Ext")
			g_sPhone3 = oRs("Phone3")
			g_sPhone3Ext = oRs("Phone3Ext")
			g_sFax = oRs("Fax")
			
			g_sMailingAddress = oRs("MailingAddress")
			g_sMailingAddress2 = oRs("MailingAddress2")
			g_sMailingCity = oRs("MailingCity")
			g_iMailingStateId = oRs("MailingStateId")
			g_sMailingZipcode = oRs("MailingZipcode")
			g_sMailingZipcodeExt = oRs("MailingZipcodeExt")
			g_iMailingCountryId = oRs("MailingCountryId")
			
			g_sBranchManager = oRs("BranchManager")
			g_sMainContactName = oRs("MainContactName")
			g_sMainContactPhone = oRs("MainContactPhone")
			g_sMainContactPhoneExt = oRs("MainContactPhoneExt")
			g_sMainContactEmail = oRs("MainContactEmail")
			g_sMainContactFax = oRs("MainContactFax")
			
			LoadCompanyById = g_iCompanyId
			
			
			'Now we get the DBA list if there is one.  We again need to choose
			'the right DB to search.
			'if p_iCompanyId < 0 then
			'
			'	sSql = "SELECT * FROM CompaniesDbasX " & _
			'		   "WHERE CompanyId = '" & g_iCompanyId & "'"
			'	set oRs = QueryDb(sSql)
			''	
			'elseif p_iCompanyId >= 0 then
			'
			'	sSql = "SELECT * FROM CompaniesDbasX " & _
			'		   "WHERE CompanyId = '" & g_iCompanyId & "'"
			'	set oRs = QueryTpDb(sSql)
			'
			'end if 
			'
			'if not (oRs.EOF and oRs.BOF) then
			'
			'	do while not oRs.EOF 
			'	
			'		g_sDba = g_sDba & oRs("Dba") & ", "
			'	
			''		oRs.MoveNext
			'	
			'	loop
			'	
			'	'Remove trailing comma.
			'	g_sDba = left(g_sDba, len(g_sDba) - 2)
			'
			'end if
		
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.
			LoadCompanyById = 0
			set oRs = nothing
			ReportError("Unable to load the passed company ID.")
			exit function
	
		end if
		
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LoadCompanyByName
	' Desc: Attempts to load the properties of a stored company that is
	'		associated with the passed name.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_sName - The company name to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyByName(p_sName)
	
		LoadCompanyByName = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckStrParam(p_sName, 0, "Name") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iCompanyId 'Found Company ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT CompanyId FROM Companies " & _
			"WHERE Name LIKE '" & p_sName & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			'We got a recordset back
			iCompanyId = oRs("CompanyId")		
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iCompanyId)) or iCompanyId = "" then
			
			ReportError("No valid Company associated with this Name.")
			exit function
			
		end if 
		
				
		'Use the found Company ID to load the company.
		LoadCompanyByName = LoadCompanyById(iCompanyId)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LookupBusType
	' Description:		Gets the name of the BusTypeId from the passed Id
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iBusTypeId - Business Type to look up
	' Outputs:			None.
	' Returns:			String w/ Business Type Name, or empty
	' -------------------------------------------------------------------------
	public function LookupBusType(p_iBusTypeId)
	
		LookupBusType = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM BusinessTypes " & _
		       "WHERE BusTypeId = '" & p_iBusTypeId & "'"
		set oRs = QueryDb(sSql)
		
		LookupBusType = oRs("BusinessType")
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LookupCorporateLogo
	' Description:		Gets the name of the corporate logo
	' Pre-conditions:	ConnectionString
	' Outputs:			None.
	' Returns:			String w/ Logo filename, or empty
	' -------------------------------------------------------------------------
	public function LookupCorporateLogo()
	
		LookupCorporateLogo = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM AssociationLogos " & _
		       "WHERE CompanyId = '" & g_iCompanyId & "'"
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
		
			LookupCorporateLogo = ""
			
		else
			
			LookupCorporateLogo = oRs("FileName")
			
		end if 
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LookupLicenseStatus
	' Description:		Gets the name of the LicenseStatusId from the passed
	'					LicenseStatusId
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iLicenseStatusId - License Status to look up
	' Outputs:			None.
	' Returns:			String w/ License Status Name, or empty
	' -------------------------------------------------------------------------
	public function LookupLicenseStatus(p_iLicenseStatusId)
	
		LookupLicenseStatus = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM LicenseStatusTypes " & _
		       "WHERE LicenseStatusId = '" & p_iLicenseStatusId & "'"
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
		
			LookupLicenseStatus = ""
			
		else
			
			LookupLicenseStatus = oRs("LicenseStatus")
			
		end if 
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LookupOrgType
	' Description:		Gets the name of the OrgTypeId from the passed OrgTypeId
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iOrgTypeId - Org Type to look up
	' Outputs:			None.
	' Returns:			String w/ Org Type Name, or empty
	' -------------------------------------------------------------------------
	public function LookupOrgType(p_iOrgTypeId)
	
		LookupOrgType = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM OrganizationTypes " & _
		       "WHERE OrganizationTypeId = '" & p_iOrgTypeId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupOrgType = oRs("OrganizationType")
		end if 
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:				LookupOrgTypeByName
	' Description:		Gets the name of the OrgTypeId from the passed OrgType
	' Pre-conditions:	ConnectionString
	' Inputs:			p_sOrgType - Org Type to look up
	' Outputs:			None.
	' Returns:			Int w/ Org Type Id, or 0
	' -------------------------------------------------------------------------
	public function LookupOrgTypeByName(p_sOrgType)
	
		LookupOrgTypeByName = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM OrganizationTypes " & _
		       "WHERE OrganizationType LIKE '" & p_sOrgType & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupOrgTypeByName = oRs("OrganizationTypeId")
		end if 
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupPricingPlanMaxUsers
	' Desc: Returns the number of users allowed by the passed pricing plan ID.
	' Preconditions: Connectionstring
	' Inputs: p_iPlanId - ID of the plan to look up
	' Returns: Integer value of maximum number of allowed loan officers
	' -------------------------------------------------------------------------
	public function LookupPricingPlanMaxOfficers(p_iPlanId)
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iPlanId, 0, "Plan ID") then
			exit function
		end if
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT * FROM PricingPlans " & _
			"WHERE PricingPlanId = '" & p_iPlanId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF AND oRs.EOF) then
		
			LookupPricingPlanMaxOfficers = oRs("MaxOfficers")
		
		end if 
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			LookupState = oRs("State")
		end if 
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupTpCompanyName
	' Desc: Gets the name of a TrainingPro company based on the passed ID
	' Preconditions: ConnectionString
	' Inputs: p_iTpCompanyId - ID of the TrainingPro company to look up
	' Returns: String w/ company name, or empty
	' -------------------------------------------------------------------------
	public function LookupTpCompanyName(p_iTpCompanyId)
		
		LookupTpCompanyName = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iTpCompanyId, false, "Company ID") then
			exit function
		end if
		
		
		dim sSql 'SQL query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM " & application("sTpDbName") & ".dbo.Companies " & _
			"WHERE CompanyId = '" & p_iTpCompanyId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupTpCompanyName = oRs("Name")
		
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:	Safe
	' Description: Escapes/scrubs characters to make content safe for
	'	submission into the database.
	' Inputs: p_sText - String to make database-safe.
	' Returns: Edited string
	' -------------------------------------------------------------------------
	public function Safe(p_sText)
	
		if p_sText <> "" then
	
			p_sText = replace(p_sText, "''", "'")
			p_sText = replace(p_sText, "'", "''")
		
			Safe = p_sText
			
		end if
	
	end function
	
	' -------------------------------------------------------------------------
	' Name:				SaveCompany
	' Description:		Save the Company object in the database, and assign it
	'					the resulting Company ID.
	' Pre-conditions:	ConnectionString, Name, Address, City, StateId, 
	'					Zipcode
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns the new ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveCompany()
	
		SaveCompany = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckStrParam(g_sName, 0, "Company Name") then
			exit function
		elseif not CheckStrParam(g_sAddress, 0, "Address") then
			exit function				
		elseif not CheckStrParam(g_sCity, 0, "City") then
			exit function		
		elseif not CheckStrParam(g_iStateId, 0, "State ID") then
			exit function		
		elseif not CheckStrParam(g_sZipcode, 0, "Zipcode") then
			exit function		
		end if


		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		'If we have a CompanyId property assigned already, we're working on a 
		'loaded Company, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		'dim iActive, iBranchTracking 'Hold 1/0 for value of 
		if g_iCompanyId <> "" then
				
			'Now we can proceed with building our SQL statement.
			bIsNew = false
			sSql = "UPDATE Companies SET " & _
				   "Name = '" & safe(g_sName) & "', " & _
				   "LegalName = '" & safe(g_sLegalName) & "', " & _
				   "Active = '" & abs(cint(g_bActive)) & "', " & _
				   "PricingPlanId = '" & safe(g_iPricingPlan) & "', " & _
				   "IncorpStateId = '" & safe(g_iIncorpStateId) & "', "
				   
			if g_dIncorpDate <> "" then
				sSql = sSql & "IncorpDate = '" & safe(g_dIncorpDate) & "', "
			else
				sSql = sSql & "IncorpDate = NULL, "
			end if
			
			if g_iTpLinkCompanyId <> "" and g_iTpLinkCompanyId <> "0" then
				sSql = sSql & "TpLinkCompanyId = '" & safe(g_iTpLinkCompanyId) & _
					"', "
			else
				sSql = sSql & "TpLinkCompanyId = NULL, "
			end if				
			
			sSql = sSql & "FiscalYearEndMonth = '" & _
					safe(g_iFiscalYearEndMonth) & "', " & _
				   "FiscalYearEndDay = '" & safe(g_iFiscalYearEndDay) & "', " & _
				   "OrgTypeId = '" & safe(g_iOrgTypeId) & "', " & _
				   "BusTypeId = '" & safe(g_iBusTypeId) & "', " & _
				   "Ein = '" & safe(g_sEin) & "', " & _
				   "LogoFileName = '" & safe(g_sLogoFileName) & "', " & _
				   "Address = '" & safe(g_sAddress) & "', " & _
				   "Address2 = '" & safe(g_sAddress2) & "', " & _
				   "City = '" & safe(g_sCity) & "', " & _
				   "StateId = '" & safe(g_iStateId) & "', " & _
				   "Zipcode = '" & safe(g_sZipcode) & "', " & _
				   "ZipcodeExt = '" & safe(g_sZipcodeExt) & "', " & _
				   "CountryId = '" & safe(g_iCountryId) & "', " & _
				   "Website = '" & safe(g_sWebsite) & "', " & _
				   "Email = '" & safe(g_sEmail) & "', " & _
				   "Phone = '" & safe(g_sPhone) & "', " & _
				   "PhoneExt = '" & safe(g_sPhoneExt) & "', " & _
				   "Phone2 = '" & safe(g_sPhone2) & "', " & _
				   "Phone2Ext = '" & safe(g_sPhone2Ext) & "', " & _
				   "Phone3 = '" & safe(g_sPhone3) & "', " & _
				   "Phone3Ext = '" & safe(g_sPhone3Ext) & "', " & _
				   "Fax = '" & safe(g_sFax) & "', " & _ 
				   "MailingAddress = '" & safe(g_sMailingAddress) & "', " & _
				   "MailingAddress2 = '" & safe(g_sMailingAddress2) & "', " & _
				   "MailingCity = '" & safe(g_sMailingCity) & "', " & _
				   "MailingStateId = '" & safe(g_iMailingStateId) & "', " & _
				   "MailingZipcode = '" & safe(g_sMailingZipcode) & "', " & _
				   "MailingZipcodeExt = '" & safe(g_sMailingZipcodeExt) & "', " & _
				   "MailingCountryId = '" & safe(g_iMailingCountryId) & "', " & _
				   "MainContactName = '" & safe(g_sMainContactName) & "', " & _
				   "MainContactPhone = '" & safe(g_sMainContactPhone) & "', " & _
				   "MainContactPhoneExt = '" & safe(g_sMainContactPhoneExt) & _
				   "', " & _
				   "MainContactEmail = '" & safe(g_sMainContactEmail) & "', " & _
				   "MainContactFax = '" & safe(g_sMainContactFax) & "' " & _
				   "WHERE CompanyID = '" & safe(g_iCompanyId) & "'"

			'Response.Write(sSql)

			oConn.Execute(sSQL)
			
			SaveCompany = g_iCompanyId
			
		else

			'Build an SQL statement for a new Company.
			bIsNew = true
			sSql = "INSERT INTO Companies (" & _
				   "Name, " & _
				   "LegalName, " & _
				   "Active, " & _
				   "PricingPlanId, " & _
				   "IncorpStateId, " & _
				   "IncorpDate, " & _
				   "TpLinkCompanyId, " & _
				   "FiscalYearEndMonth, " & _
				   "FiscalYearEndDay, " & _
				   "OrgTypeId, " & _
				   "BusTypeId, " & _
				   "Ein, " & _
				   "Address, " & _
				   "Address2, " & _
				   "City, " & _
				   "StateId, " & _
				   "Zipcode, " & _
				   "ZipcodeExt, " & _
				   "CountryId, " & _
				   "Website, " & _
				   "Email, " & _
				   "Phone, " & _
				   "PhoneExt, " & _
				   "Phone2, " & _
				   "Phone2Ext, " & _
				   "Phone3, " & _
				   "Phone3Ext, " & _
				   "Fax, " & _
				   "MailingAddress, " & _
				   "MailingAddress2, " & _
				   "MailingCity, " & _
				   "MailingStateId, " & _
				   "MailingZipcode, " & _
				   "MailingZipcodeExt, " & _
				   "MailingCountryId, " & _
				   "MainContactName, " & _
				   "MainContactPhone, " & _
				   "MainContactPhoneExt, " & _
				   "MainContactEmail, " & _
				   "MainContactFax, " & _
				   "LogoFileName " & _
				   ") VALUES (" & _
				   "'" & safe(g_sName) & "', " & _
				   "'" & safe(g_sLegalName) & "', " & _
				   "'" & abs(cint(g_bActive)) & "', " & _
				   "'" & safe(g_iPricingPlan) & "', " & _
				   "'" & safe(g_iIncorpStateId) & "', "
				   
			if g_dIncorpDate <> "" then
				sSql = sSql & "'" & safe(g_dIncorpDate) & "', "
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_iTpLinkCompanyId <> "" and g_iTpLinkCompanyId <> "0" then
				sSql = sSql & "'" & safe(g_iTpLinkCompanyId) & "', "
			else
				sSql = sSql & "NULL, "
			end if
			
			sSql = sSql & "'" & safe(g_iFiscalYearEndMonth) & "', " & _
				   "'" & safe(g_iFiscalYearEndDay) & "', " & _
				   "'" & safe(g_iOrgTypeId) & "', " & _
				   "'" & safe(g_iBusTypeId) & "', " & _
				   "'" & safe(g_sEin) & "', " & _
				   "'" & safe(g_sAddress) & "', " & _
				   "'" & safe(g_sAddress2) & "', " & _
				   "'" & safe(g_sCity) & "', " & _
				   "'" & safe(g_iStateId) & "', " & _
				   "'" & safe(g_sZipcode) & "', " & _
				   "'" & safe(g_sZipcodeExt) & "', " & _
				   "'" & safe(g_iCountryId) & "', " & _
				   "'" & safe(g_sWebsite) & "', " & _
				   "'" & safe(g_sEmail) & "', " & _
				   "'" & safe(g_sPhone) & "', " & _
				   "'" & safe(g_sPhoneExt) & "', " & _
				   "'" & safe(g_sPhone2) & "', " & _
				   "'" & safe(g_sPhone2Ext) & "', " & _
				   "'" & safe(g_sPhone3) & "', " & _
				   "'" & safe(g_sPhone3Ext) & "', " & _
				   "'" & safe(g_sFax) & "', " & _
				   "'" & safe(g_sMailingAddress) & "', " & _
				   "'" & safe(g_sMailingAddress2) & "', " & _
				   "'" & safe(g_sMailingCity) & "', " & _
				   "'" & safe(g_sMailingStateId) & "', " & _
				   "'" & safe(g_sMailingZipcode) & "', " & _
				   "'" & safe(g_sMailingZipcodeExt) & "', " & _
				   "'" & safe(g_sMailingCountryId) & "', " & _
				   "'" & safe(g_sMainContactName) & "', " & _
				   "'" & safe(g_sMainContactPhone) & "', " & _
				   "'" & safe(g_sMainContactPhoneExt) & "', " & _
				   "'" & safe(g_sMainContactEmail) & "', " & _
				   "'" & safe(g_sMainContactFax) & "', " & _
				   "'" & safe(g_sLogoFileName) & "' " & _
				   ")"
			set oRs = oConn.Execute(sSQL)

			'Now we'll try to pull out the new Company we just created.
			sSql = "SELECT CompanyID FROM Companies " & _
			       "WHERE Name = '" & g_sName & "'" & _
			       "ORDER BY CompanyID DESC"

			set oRs = oConn.Execute(sSQL)

			if not (oRs.EOF and oRs.BOF) then
			
				g_iCompanyId = oRs("CompanyId")
				SaveCompany = g_iCompanyId
			
			else
			
				'Our record was not created, or for some other reason we
				'couldn't pull the record back out.
				ReportError("Company creation failed.")
				SaveCompany = 0
				exit function
			
			end if 
		
		end if
		
		
		'If we were successful so far, we'll save the DBA list.
		'dim sDbaArray
		'dim sDba
		'if SaveCompany <> 0 then
		
			'First we'll clear the existing entries.
		'	sSql = "DELETE FROM CompaniesDbasX " & _
		'		   "WHERE CompanyId = '" & g_iCompanyId & "'"
		'	oConn.Execute(sSql)
			
			'Now we'll loop through our DbaList and add it's contents to the 
			'database.
		'	sDbaArray = split(g_sDba, ", ")
		'	for each sDba in sDbaArray
			
		'		sSql = "INSERT INTO CompaniesDbasX (" & _
		'			   "CompanyId, " & _
		'			   "Dba" & _
		'			   ") VALUES (" & _
		'			   "'" & g_iCompanyId & "', " & _
		'			   "'" & sDba & "'" & _
		'			   ")"
		'		oConn.Execute(sSql)
		'	
		'	next 
		'
		'end if 
				
			   
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:				SearchCompanies
	' Description:		Retrieves a collection of CompanyIDs that match the 
	'                   properties of the current object.  I.e., based on the
	'                   current Name, City, etc.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchCompanies()
	
		set SearchCompanies = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		

		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		'dim sSql2
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT Cs.CompanyId, Cs.Name FROM Companies AS Cs "
			
		'if Inactive search
		if g_bActive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.active = '" & abs(cint(g_bActive)) & "'"
		end if
		
		'if Name search
		if g_sName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Name LIKE '%" & g_sName & "%'"
		end if
		
	
		'if Address search
		if g_sAddress <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Address LIKE '%" & g_sAddress & "%'" 
		end if
		
		'if City search
		if g_sCity <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.City LIKE '%" & g_sCity & "%'" 
		end if
		
		'if StateID search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StateID = '" & g_iStateId & "'" 
		end if

		'if Zipcode search
		if g_sZipcode <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Zipcode = '" & g_sZipcode & "'" 
		end if
		
		'if UseWithCms search
		if g_bUseWithCms <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.UseWithCms = '" & abs(cint(g_bUseWithCms)) & "'"
		end if


		sSQL = sSQL & " ORDER BY Cs.Name"
		
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchCompanies = QueryDb(sSql)
		
	end function
	
End Class
%>
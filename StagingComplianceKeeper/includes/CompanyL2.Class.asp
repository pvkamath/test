<%
'==============================================================================
' Class: CompanyL2
' Controls the creation, modification, and removal of second-tier Companies.
' Create Date: 03/01/06
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- ConnectionString
'	- VocalErrors
' Company Properties:
'	- CompanyL2Id
'	- CompanyId
'	- Name
'	- LegalName
'	- EIN
'	- Deleted
'	- IncorpStateId
'	- IncorpDate
'	- Inactive
'	- InactiveDate
'	- FiscalYearEndMonth
'	- FiscalYearEndDay
'	- OrgTypeId
'	- BusTypeId
'	- Address
'	- Address2
'	- City
'	- StateId
'	- Zipcode
'	- ZipcodeExt
'	- CountryId
'	- Email
'	- Website
'	- Phone
'	- PhoneExt
'	- Phone2
'	- Phone2Ext
'	- Phone3
'	- Phone3Ext
'	- Fax
'	- MailingAddress
'	- MailingAddress2
'	- MailingCity
'	- MailingStateId
'	- MailingZipcode
'	- MailingZipcodeExt
'	- MailingCountry
'	- MainContactName
'	- MainContactPhone
'	- MainContactPhoneExt
'	- MainContactEmail
'	- MainContactFax
'	- CustField1
'	- CustField2
'	- CustField3
'	- CustField4
'	- CustField5
' Methods:
'	- LoadCompanyByBranchId
'	- LoadCompanyById
'	- SaveCompany
'	- DeleteCompany
'	- DeleteCompanyById
'	- SearchCompanies
'   - GetDbaRecordset
'	- AddNewDba
'   - DeleteDba
'	- LookupState
'	- LookupOrgType
'	- LookupLicenseStatus
'==============================================================================

Class CompanyL2

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Base company properties
	dim g_iCompanyL2Id
	dim g_iCompanyId
	dim g_sName
	dim g_sLegalName
	dim g_bDeleted
	dim g_sComments
	dim g_bInactive
	dim g_dInactiveDate
	
	'Legal info	
	dim g_iIncorpStateId
	dim g_dIncorpDate
	dim g_iFiscalYearEndMonth
	dim g_iFiscalYearEndDay
	dim g_iOrgTypeId
	dim g_iBusTypeId
	dim g_sEin
	
	'Location info
	dim g_sAddress
	dim g_sAddress2
	dim g_sCity
	dim g_iStateId
	dim g_sZipcode
	dim g_sZipcodeExt
	dim g_iCountryId
	dim g_sWebsite
	dim g_sEmail
	dim g_sPhone
	dim g_sPhoneExt
	dim g_sPhone2
	dim g_sPhone2Ext
	dim g_sPhone3
	dim g_sPhone3Ext
	dim g_sFax

	'Mailing info
	dim g_sMailingAddress
	dim g_sMailingAddress2
	dim g_sMailingCity
	dim g_iMailingStateId
	dim g_sMailingZipcode
	dim g_sMailingZipcodeExt
	dim g_iMailingCountryId
	
	'Custom Field info
	dim g_sCustField1
	dim g_sCustField2
	dim g_sCustField3
	dim g_sCustField4
	dim g_sCustField5
	
	'Main contact info
	'dim g_sBranchManager
	dim g_sMainContactName
	dim g_sMainContactPhone
	dim g_sMainContactPhoneExt
	dim g_sMainContactEmail
	dim g_sMainContactFax
	
	'Search properties
	dim g_sSearchNameStart
	dim g_sSearchStateIdList
	dim g_sSearchLicStateIdList
	dim g_sSearchCompanyL2IdList
	dim g_iLicenseStatusId
	dim g_sLicenseStatusIdList
	dim g_sLicenseNumber
	dim g_dLicenseExpDateFrom
	dim g_dLicenseExpDateTo
	dim g_iAppDeadline
	

	
	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	

	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	
	'Retrieves the unique ID of this CompanyL2.  This is an identity in the DB,
	'so it cannot be set.
	public property get CompanyL2Id()
		CompanyL2Id = g_iCompanyL2Id
	end property
	
	
	'Unique ID of the Company to which this object is assigned
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId.  Integer required.")
		end if
	end property
	

	'Stores/retrieves the Company name.
	public property let Name(p_sName)
		g_sName = p_sName
	end property
	public property get Name()
		Name = g_sName
	end property
	
	
	'Legal name of the company
	public property let LegalName(p_sLegalName)
		g_sLegalName = left(p_sLegalName, 100)
	end property
	public property get LegalName()
		LegalName = g_sLegalName
	end property
	
	
	'Whether or not the company is currently deleted.
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if 	
	end property
	
	
	'Whether or not the company is currently inactive.
	public property get Inactive()
		Inactive = g_bInactive
	end property
	public property let Inactive(p_bInactive)
		if isnumeric(p_bInactive) then
			g_bInactive = cbool(p_bInactive)
		elseif p_bInactive = "" then
			g_bInactive = ""
		else
			ReportError("Invalid Inactive value.  Boolean required.")
		end if 	
	end property

	'Date the company was inactive
	public property get InactiveDate()
		InactiveDate = g_dInactiveDate
	end property
	public property let InactiveDate(p_dInactiveDate)
		if isdate(p_dInactiveDate) then
			g_dInactiveDate = cdate(p_dInactiveDate)
		elseif p_dInactiveDate = "" then
			g_dInactiveDate = ""
		else
			ReportError("Invalid InactiveDate value.  Date required.")
		end if
	end property

	'Administrator comments about the company.
	public property get Comments()
		Comments = g_sComments
	end property
	public property let Comments(p_sComments)
		g_sComments = p_sComments
	end property
	
	
	'State of Incorporation
	public property get IncorpStateId()
		IncorpStateId = g_iIncorpStateId
	end property
	public property let IncorpStateId(p_iIncorpStateId)
		if isnumeric(p_iIncorpStateId) then
			g_iIncorpStateId = abs(cint(p_iIncorpStateId))
		else
			Response.Write("Invalid IncorpStateId value.  Integer required.")
		end if 
	end property
	
	
	'Date of Incorporation
	public property get IncorpDate()
		IncorpDate = g_dIncorpDate
	end property
	public property let IncorpDate(p_dIncorpDate)
		if isdate(p_dIncorpDate) then
			g_dIncorpDate = p_dIncorpDate
		elseif p_dIncorpDate = "" then
			g_dIncorpDate = ""
		else
			ReportError("Invalid IncorpDate value.  Date required.")
		end if 
	end property
	
	
	'Date of the end of the fiscal year
	public property get FiscalYearEndMonth()
		FiscalYearEndMonth = g_iFiscalYearEndMonth
	end property
	public property let FiscalYearEndMonth(p_iFiscalYearEndMonth)
		if isdate(p_iFiscalYearEndMonth) then
			g_iFiscalYearEndMonth = p_iFiscalYearEndMonth
		else
			ReportError("Invalid FiscalYearEndMonth value.  Integer required.")
		end if 
	end property


	'Date of the end of the fiscal year
	public property get FiscalYearEndDay()
		FiscalYearEndDay = g_iFiscalYearEndDay
	end property
	public property let FiscalYearEndDay(p_iFiscalYearEndDay)
		if isdate(p_iFiscalYearEndDay) then
			g_iFiscalYearEndDay = p_iFiscalYearEndDay
		else
			ReportError("Invalid FiscalYearEndDay value.  Integer required.")
		end if 
	end property
	
	
	'Organization type
	public property get OrgTypeId()
		OrgTypeId = g_iOrgTypeId
	end property
	public property let OrgTypeId(p_iOrgTypeId)
		if isnumeric(p_iOrgTypeId) then
			g_iOrgTypeId = abs(cint(p_iOrgTypeId))
		else
			ReportError("Invalid OrgTypeId value.  Integer required.")
		end if 
	end property


	'Business type
	public property get BusTypeId()
		BusTypeId = g_iBusTypeId
	end property
	public property let BusTypeId(p_iBusTypeId)
		if isnumeric(p_iBusTypeId) then
			g_iBusTypeId = abs(cint(p_iBusTypeId))
		else
			ReportError("Invalid BusTypeId value.  Integer required.")
		end if 
	end property
	
	
	'EIN - Employer Identification Number
	public property get Ein()
		Ein = g_sEin
	end property
	public property let Ein(p_sEin)
		g_sEin = left(p_sEin, 50)
	end property
	
	
	'Stores/retrieves the Company address
	public property let Address(p_sAddress)
		g_sAddress = p_sAddress
	end property
	public property get Address()
		Address = g_sAddress
	end property
	

	'Address line two
	public property get Address2()
		Address2 = g_sAddress2
	end property
	public property let Address2(p_sAddress2)
		g_sAddress2 = p_sAddress2
	end property
	
	
	'Stores/retrieves the Company city
	public property let City(p_sCity)
		g_sCity = p_sCity
	end property
	public property get City()
		City = g_sCity
	end property
	
	
	'Stores/retrieves the Company state ID
	public property let StateId(p_iStateId)
		g_iStateId = p_iStateId
	end property
	public property get StateId()
		StateId = g_iStateId
	end property
	
	
	'Retrieves the Company state name.  User should change the state via 
	'the StateId value.  StateName is read-only.
	public property get State()
		dim sSql, oRs
		
		sSql = "SELECT State FROM States WHERE StateID = '" & g_iStateId & "'"
		
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			State = oRs("State")
		
		else
		
			State = ""
		
		end if
		
		set oRs = nothing
	end property


	'Stores/retrieves the Company zip.
	public property let Zipcode(p_sZipcode)
		g_sZipcode = p_sZipcode
	end property
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	
	
	'Optional zipcode extension
	public property get ZipcodeExt()
		Zipcode = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		g_sZipcodeExt = left(p_sZipcodeExt, 4)
	end property
	
	
	'Country ID Assigned to this address
	public property get CountryId
		CountryId = g_iCountryId
	end property
	public property let CountryId(p_iCountryId)
		if isnumeric(p_iCountryId) then
			g_iCountryId = p_iCountryId
		elseif p_iCountryId = "" then
			g_iCountryId = ""
		else
			ReportError("Invalid CountryId value.  Integer required.")
		end if 
	end property
	
	
	'Website URL
	public property get Website()
		Website = g_sWebsite
	end property
	public property let Website(p_sWebsite)
		g_sWebsite = p_sWebsite
	end property
	
	
	'User email address
	public property get Email()
		Email = g_sEmail
	end property
	public property let Email(p_sEmail)
		g_sEmail = left(trim(p_sEmail), 50)
	end property
	
	
	'Phone number as a string
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = p_sPhone
	end property
	
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = p_sPhoneExt
	end property


	'Phone number as a string
	public property get Phone2()
		Phone2 = g_sPhone2
	end property
	public property let Phone2(p_sPhone2)
		g_sPhone2 = p_sPhone2
	end property
	
	
	'Phone number extension
	public property get PhoneExt2()
		PhoneExt2 = g_sPhone2Ext
	end property
	public property let PhoneExt2(p_sPhoneExt2)
		g_sPhone2Ext = p_sPhoneExt2
	end property
	
	
	'Phone number as a string
	public property get Phone3()
		Phone3 = g_sPhone3
	end property
	public property let Phone3(p_sPhone3)
		g_sPhone3 = p_sPhone3
	end property
	
	
	'Phone number extension
	public property get PhoneExt3()
		PhoneExt3 = g_sPhone3Ext
	end property
	public property let PhoneExt3(p_sPhoneExt3)
		g_sPhone3Ext = p_sPhoneExt3
	end property
	
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = p_sFax
	end property
	
	
	'Mailing Address line 1
	public property get MailingAddress()
		MailingAddress = g_sMailingAddress
	end property
	public property let MailingAddress(p_sMailingAddress)
		g_sMailingAddress = p_sMailingAddress
	end property 
	
	
	'Mailing Address line 2
	public property get MailingAddress2()
		MailingAddress2 = g_sMailingAddress2
	end property
	public property let MailingAddress2(p_sMailingAddress2)
		g_sMailingAddress2 = p_sMailingAddress2
	end property
	
	
	'Mailing address city
	public property get MailingCity()
		MailingCity = g_sMailingCity
	end property
	public property let MailingCity(p_sMailingCity)
		g_sMailingCity = p_sMailingCity
	end property
	
	
	'Mailing address state id
	public property get MailingStateId()
		MailingStateId = g_iMailingStateId
	end property
	public property let MailingStateId(p_iMailingStateId)
		if isnumeric(p_iMailingStateId) then
			g_iMailingStateId = abs(cint(p_iMailingStateId))
		else
			ReportError("Invalid MailingStateId value.  Integer required.")
		end if 
	end property
	
	
	'Mailing address zipcode
	public property get MailingZipcode()
		MailingZipcode = g_sMailingZipcode
	end property 
	public property let MailingZipcode(p_sMailingZipcode)
		g_sMailingZipcode = p_sMailingZipcode
	end property


	'Optional zipcode extension
	public property get MailingZipcodeExt()
		MailingZipcode = g_sMailingZipcodeExt
	end property
	public property let MailingZipcodeExt(p_sMailingZipcodeExt)
		g_sMailingZipcodeExt = left(p_sMailingZipcodeExt, 4)
	end property
	
	
	'Mailing Country ID Assigned to this address
	public property get MailingCountryId
		MailingCountryId = g_iMailingCountryId
	end property
	public property let MailingCountryId(p_iMailingCountryId)
		if isnumeric(p_iMailingCountryId) then
			g_iMailingCountryId = p_iMailingCountryId
		elseif p_iMailingCountryId = "" then
			g_iMailingCountryId = ""
		else
			ReportError("Invalid MailingCountryId value.  Integer required.")
		end if 
	end property
	
	
	'Main contact full name
	public property get MainContactName()
		MainContactName = g_sMainContactName
	end property
	public property let MainContactName(p_sMainContactName)
		g_sMainContactName = p_sMainContactName
	end property
	
	
	'Main contact phone number
	public property get MainContactPhone()
		MainContactPhone = g_sMainContactPhone
	end property
	public property let MainContactPhone(p_sMainContactPhone)
		g_sMainContactPhone = p_sMainContactPhone
	end property
	
	
	'Main contact phone extension
	public property get MainContactPhoneExt()
		MainContactPhoneExt = g_sMainContactPhoneExt
	end property
	public property let MainContactPhoneExt(p_sMainContactPhoneExt)
		g_sMainContactPhoneExt = p_sMainContactPhoneExt
	end property
	
	
	'Main contact email
	public property get MainContactEmail()
		MainContactEmail = g_sMainContactEmail
	end property
	public property let MainContactEmail(p_sMainContactEmail)
		g_sMainContactEmail = p_sMainContactEmail
	end property
	

	'Main contact fax
	public property get MainContactFax()
		MainContactFax = g_sMainContactFax
	end property 
	public property let MainContactFax(p_sMainContactFax)
		g_sMainContactFax = p_sMainContactFax
	end property
	
	
	'Numbered custom field
	public property let CustField1(p_sCustField1)
		g_sCustField1 = left(p_sCustField1, 150)
	end property
	public property get CustField1()
		CustField1 = g_sCustField1
	end property
	
	
	'Numbered custom field
	public property let CustField2(p_sCustField2)
		g_sCustField2 = left(p_sCustField2, 150)
	end property
	public property get CustField2()
		CustField2 = g_sCustField2
	end property
	
	
	'Numbered custom field
	public property let CustField3(p_sCustField3)
		g_sCustField3 = left(p_sCustField3, 150)
	end property
	public property get CustField3()
		CustField3 = g_sCustField3
	end property
	
	
	'Numbered custom field
	public property let CustField4(p_sCustField4)
		g_sCustField4 = left(p_sCustField4, 150)
	end property
	public property get CustField4()
		CustField4 = g_sCustField4
	end property
	
	
	'Numbered custom field
	public property let CustField5(p_sCustField5)
		g_sCustField5 = left(p_sCustField5, 150)
	end property
	public property get CustField5()
		CustField5 = g_sCustField5
	end property
	
	
	'Search Name Start property - used to search for names starting
	'with this value, rather than names simply containing the value.
	public property get SearchNameStart()
		SearchNameStart = g_sSearchNameStart
	end property
	public property let SearchNameStart(p_sSearchNameStart)
		g_sSearchNameStart = p_sSearchNameStart
	end property


	'List of State IDs to search within.  Used to find associates from multiple
	'states rather than just one.
	public property get SearchStateIdList()
		SearchStateIdList = g_sSearchStateIdList
	end property
	public property let SearchStateIdList(p_sSearchStateIdList)
		g_sSearchStateIdList = p_sSearchStateIdList
	end property


	'List of State IDs to search within.  Used to find licenses from multiple
	'states rather than just one.
	public property get SearchLicStateIdList()
		SearchLicStateIdList = g_sSearchLicStateIdList
	end property
	public property let SearchLicStateIdList(p_sSearchLicStateIdList)
		g_sSearchLicStateIdList = p_sSearchLicStateIdList
	end property
	
	
	'List of CompanyL2 IDs to search within.  Used to find associates from
	'multiple CL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property


	'Search License Status 
	public property get SearchLicenseStatusId()
		SearchLicenseStatusId = g_iLicenseStatusId
	end property
	public property let SearchLicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = cint(p_iLicenseStatusId)
		elseif isnull(p_iLicenseStatusId) or p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid SearchLicenseStatusId value.  " & _
				"Integer required.")
		end if
	end property
	
	
	'List of License Status IDs to search within.  Used to find licenses
	'matching any of several status types.
	public property get SearchLicenseStatusIdList()
		SearchLicenseStatusIdList = g_sLicenseStatusIdList
	end property
	public property let SearchLicenseStatusIdList(p_sLicenseStatusIdList)
		g_sLicenseStatusIdList = p_sLicenseStatusIdList
	end property
	
	
	'Search License Number
	public property get SearchLicenseNumber()
		SearchLicenseNumber = g_sLicenseNumber
	end property
	public property let SearchLicenseNumber(p_sLicenseNumber)
		g_sLicenseNumber = p_sLicenseNumber
	end property	
	
	
	'Search License Expiration from
	public property get SearchLicenseExpDateFrom()
		SearchLicenseExpDateFrom = g_dLicenseExpDateFrom
	end property
	public property let SearchLicenseExpDateFrom(p_dLicenseExpDateFrom)
		if isdate(p_dLicenseExpDateFrom) then
			g_dLicenseExpDateFrom = cdate(p_dLicenseExpDateFrom)
		elseif p_dLicenseExpDateFrom = "" then
			g_dLicenseExpDateFrom = ""
		else
			ReportError("Invalid SearchLicenseExpDateFrom value.  " & _
				"Date required.")
		end if
	end property


	'Search License Expiration from
	public property get SearchLicenseExpDateTo()
		SearchLicenseExpDateTo = g_dLicenseExpDateTo
	end property
	public property let SearchLicenseExpDateTo(p_dLicenseExpDateTo)
		if isdate(p_dLicenseExpDateTo) then
			g_dLicenseExpDateTo = cdate(p_dLicenseExpDateTo)
		elseif p_dLicenseExpDateTo = "" then
			g_dLicenseExpDateTo = ""
		else
			ReportError("Invalid SearchLicenseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	
	'How are we searching for application deadlines?
	public property get SearchAppDeadline()
		SearchAppDeadline = g_iAppDeadline
	end property
	public property let SearchAppDeadline(p_iAppDeadline)
		if isnumeric(p_iAppDeadline) then
			g_iAppDeadline = clng(p_iAppDeadline)
		elseif p_iAppDeadline = ""  or isnull(p_iAppDeadline) then
			g_iAppDeadline = ""
		else
			ReportError("Invalid SearchAppDeadline value.  Integer required.")
		end if 
	end property
		
		
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
			
		elseif g_sConnectionString = "" then

			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
				
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		'Response.Write(sSql)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
		
		'Response.Write("<p>" & sSql & "<p>")
		
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	' -------------------------------------------------------------------------
	' Name:				DeleteId
	' Description:		Delete the specified Company object from the relevant 
	'					database tables
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iCompanyId - Company ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iCompanyL2Id)
	
		DeleteId = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyL2Id, 1, "CompanyL2 ID") then
			exit function		
		end if
		
	
		dim oConn 'DB connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")

		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		
		'We'll clean out our records from the database tables.
		sSql = "UPDATE CompaniesL2 SET " & _
			"Deleted = '1' " & _
			"WHERE CompanyL2Id = '" & p_iCompanyL2Id & "'"
		oConn.Execute sSql, lRecordsAffected
		
		'Verify that at least one record was affected.
		if not lRecordsAffected > 0 then
		
			ReportError("Failure deleting the company record.")
			exit function		
		
		end if 
		
			
		DeleteId = 1
	
	end function


	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadCompanyByName
	' Desc: Attempts to load the properties of a stored company that is
	'		associated with the passed name.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_sName - The company name to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyByName(p_sName)
	
		LoadCompanyByName = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckStrParam(p_sName, 0, "Name") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iCompanyL2Id 'Found Company ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT CompanyL2Id FROM CompaniesL2 " & _
			"WHERE Name LIKE '" & p_sName & "' " & _
			"AND CompanyId = '" & session("UserCompanyId") & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a recordset back
			iCompanyL2Id = oRs("CompanyL2Id")
			
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iCompanyL2Id)) or iCompanyL2Id = "" then
			
			ReportError("No valid CompanyL2 associated with this Name.")
			exit function
			
		end if 
		
				
		'Use the found Company ID to load the company.
		LoadCompanyByName = LoadCompanyById(iCompanyL2Id)
		
	end function	
	
	' -------------------------------------------------------------------------
	' Name: LoadCompanyByBranchId
	' Desc: Attempts to load the properties of a stored company that is
	'		associated with the passed branch ID.
	' Preconditions: ConnectionString must be set.
	' Inputs: p_iBranchId - The Branch ID to load.
	' Returns: If successful, returns the loaded ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyByBranchId(p_iBranchId)
	
		LoadCompanyByBranchId = 0
		
		'Verify preconditions
		If not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSql 'SQL Statement
		dim iCompanyL2Id 'Found Company ID
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT CompanyL2Id FROM CompanyBranches " & _
			"WHERE BranchId = '" & p_iBranchId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
			
			'We got a recordset back, so there is a branch record in the DB for
			'the Branch ID we were passed.
			iCompanyL2Id = oRs("CompanyL2Id")
			
		end if 
		
		set oRs = nothing
		
		
		'Verify that this is a numeric value.
		if (not isnumeric(iCompanyL2Id)) or iCompanyL2Id = "" then
			
			ReportError("No valid CompanyL2 associated with this Branch ID.")
			exit function
			
		end if 
		
				
		'Use the found Company ID to load the company.
		LoadCompanyByBranchId = LoadCompanyById(iCompanyL2Id)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LoadCompanyById
	' Description:		This method attempts to load the properties of a stored
	'					Company based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadCompanyById(p_iCompanyL2Id)
	
		LoadCompanyById = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyL2Id, 1, "CompanyL2 ID") then
			exit function		
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")

		sSql = "SELECT * FROM CompaniesL2 " & _
			"WHERE CompanyL2Id = '" & p_iCompanyL2Id & "'"
		set oRs = QueryDb(sSql)
		
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iCompanyL2Id = oRs("CompanyL2Id")
			g_iCompanyId = oRs("CompanyID")
			g_sName = oRs("Name")
			g_sLegalName = oRs("LegalName")
			g_bDeleted = oRs("Deleted")
			g_bInactive = oRs("Inactive")
			g_dInactiveDate = oRs("InactiveDate")
			
			g_iIncorpStateId = oRs("IncorpStateId")
			g_dIncorpDate = oRs("IncorpDate")
			g_iFiscalYearEndMonth = oRs("FiscalYearEndMonth")
			g_iFiscalYearEndDay = oRs("FiscalYearEndDay")
			g_iOrgTypeId = oRs("OrgTypeId")
			g_iBusTypeId = oRs("BusTypeId")
			g_sEin = oRs("Ein")
			
			g_sAddress = oRs("Address")
			g_sAddress2 = oRs("Address2")
			g_sCity = oRs("City")
			g_iStateId = oRs("StateID")
			g_sZipcode = oRs("Zipcode")
			g_sZipcodeExt = oRs("ZipcodeExt")
			g_iCountryId = oRs("CountryId")
			g_sWebsite = oRs("Website")
			g_sEmail = oRs("Email")
			g_sPhone = oRs("Phone")
			g_sPhoneExt = oRs("PhoneExt")
			g_sPhone2 = oRs("Phone2")
			g_sPhone2Ext = oRs("Phone2Ext")
			g_sPhone3 = oRs("Phone3")
			g_sPhone3Ext = oRs("Phone3Ext")
			g_sFax = oRs("Fax")
			
			g_sMailingAddress = oRs("MailingAddress")
			g_sMailingAddress2 = oRs("MailingAddress2")
			g_sMailingCity = oRs("MailingCity")
			g_iMailingStateId = oRs("MailingStateId")
			g_sMailingZipcode = oRs("MailingZipcode")
			g_sMailingZipcodeExt = oRs("MailingZipcodeExt")
			g_iMailingCountryId = oRs("MailingCountryId")
			
			g_sMainContactName = oRs("MainContactName")
			g_sMainContactPhone = oRs("MainContactPhone")
			g_sMainContactPhoneExt = oRs("MainContactPhoneExt")
			g_sMainContactEmail = oRs("MainContactEmail")
			g_sMainContactFax = oRs("MainContactFax")
			
			g_sCustField1 = oRs("CustField1")
			g_sCustField2 = oRs("CustField2")
			g_sCustField3 = oRs("CustField3")
			g_sCustField4 = oRs("CustField4")
			g_sCustField5 = oRs("CustField5")
			
			LoadCompanyById = g_iCompanyL2Id
				
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.
			LoadCompanyById = 0
			set oRs = nothing
			ReportError("Unable to load the passed CompanyL2 ID.")
			exit function
	
		end if
		
		
		set oRs = nothing
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name:				SaveCompany
	' Description:		Save the Company object in the database, and assign it
	'					the resulting Company ID.
	' Pre-conditions:	ConnectionString, Name, Address, City, StateId, 
	'					Zipcode
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns the new ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveCompany()
	
		SaveCompany = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckStrParam(g_sName, 0, "Company Name") then
			exit function
		end if


		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		'If we have a CompanyId property assigned already, we're working on a 
		'loaded Company, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iCompanyL2Id <> "" then
				
			'Now we can proceed with building our SQL statement.
			bIsNew = false
			sSql = "UPDATE CompaniesL2 SET " & _
				   "Name = '" & safe(g_sName) & "', " & _
				   "CompanyId = '" & g_iCompanyId & "', " & _
				   "LegalName = '" & safe(g_sLegalName) & "', " & _
				   "Deleted = '" & abs(cint(g_bDeleted)) & "', " & _
				   "Inactive = '" & abs(cint(g_bInactive)) & "', " & _
				   "IncorpStateId = '" & safe(g_iIncorpStateId) & "', "
				   
			if g_dIncorpDate <> "" then
				sSql = sSql & "IncorpDate = '" & safe(g_dIncorpDate) & "', "
			else
				sSql = sSql & "IncorpDate = NULL, "
			end if
				   
			if g_dInactiveDate <> "" then
				sSql = sSql & "InactiveDate = '" & safe(g_dInactiveDate) & "', "
			else
				sSql = sSql & "InactiveDate = NULL, "
			end if
			
			sSql = sSql & "FiscalYearEndMonth = '" & _
					safe(g_iFiscalYearEndMonth) & "', " & _
				   "FiscalYearEndDay = '" & safe(g_iFiscalYearEndDay) & "', " & _
				   "OrgTypeId = '" & safe(g_iOrgTypeId) & "', " & _
				   "BusTypeId = '" & safe(g_iBusTypeId) & "', " & _
				   "Ein = '" & safe(g_sEin) & "', " & _
				   "Address = '" & safe(g_sAddress) & "', " & _
				   "Address2 = '" & safe(g_sAddress2) & "', " & _
				   "City = '" & safe(g_sCity) & "', " & _
				   "StateId = '" & safe(g_iStateId) & "', " & _
				   "Zipcode = '" & safe(g_sZipcode) & "', " & _
				   "ZipcodeExt = '" & safe(g_sZipcodeExt) & "', " & _
				   "CountryId = '" & safe(g_iCountryId) & "', " & _
				   "Website = '" & safe(g_sWebsite) & "', " & _
				   "Phone = '" & safe(g_sPhone) & "', " & _
				   "PhoneExt = '" & safe(g_sPhoneExt) & "', " & _
				   "Phone2 = '" & safe(g_sPhone2) & "', " & _
				   "Phone2Ext = '" & safe(g_sPhone2Ext) & "', " & _
				   "Phone3 = '" & safe(g_sPhone3) & "', " & _
				   "Phone3Ext = '" & safe(g_sPhone3Ext) & "', " & _
				   "Fax = '" & safe(g_sFax) & "', " & _ 
				   "MailingAddress = '" & safe(g_sMailingAddress) & "', " & _
				   "MailingAddress2 = '" & safe(g_sMailingAddress2) & "', " & _
				   "MailingCity = '" & safe(g_sMailingCity) & "', " & _
				   "MailingStateId = '" & safe(g_iMailingStateId) & "', " & _
				   "MailingZipcode = '" & safe(g_sMailingZipcode) & "', " & _
				   "MailingZipcodeExt = '" & safe(g_sMailingZipcodeExt) & "', " & _
				   "MailingCountryId = '" & safe(g_iMailingCountryId) & "', " & _
				   "MainContactName = '" & safe(g_sMainContactName) & "', " & _
				   "MainContactPhone = '" & safe(g_sMainContactPhone) & "', " & _
				   "MainContactPhoneExt = '" & safe(g_sMainContactPhoneExt) & _
				   "', " & _
				   "MainContactEmail = '" & safe(g_sMainContactEmail) & "', " & _
				   "MainContactFax = '" & safe(g_sMainContactFax) & "', " & _
				   "CustField1 = '" & g_sCustField1 & "', " & _
				   "CustField2 = '" & g_sCustField2 & "', " & _
				   "CustField3 = '" & g_sCustField3 & "', " & _
				   "CustField4 = '" & g_sCustField4 & "', " & _
				   "CustField5 = '" & g_sCustField5 & "' " & _
				   "WHERE CompanyL2ID = '" & safe(g_iCompanyL2Id) & "'"

			'Response.Write(sSql)

			oConn.Execute(sSQL)
			
			SaveCompany = g_iCompanyL2Id
			
		else

			'Build an SQL statement for a new Company.
			bIsNew = true
			sSql = "INSERT INTO CompaniesL2 (" & _
				   "CompanyId, " & _
				   "Name, " & _
				   "LegalName, " & _
				   "Deleted, " & _
				   "Inactive, " & _
				   "IncorpStateId, " & _
				   "IncorpDate, " & _
				   "InactiveDate, " & _				   
				   "FiscalYearEndMonth, " & _
				   "FiscalYearEndDay, " & _
				   "OrgTypeId, " & _
				   "BusTypeId, " & _
				   "Ein, " & _
				   "Address, " & _
				   "Address2, " & _
				   "City, " & _
				   "StateId, " & _
				   "Zipcode, " & _
				   "ZipcodeExt, " & _
				   "CountryId, " & _
				   "Website, " & _
				   "Email, " & _
				   "Phone, " & _
				   "PhoneExt, " & _
				   "Phone2, " & _
				   "Phone2Ext, " & _
				   "Phone3, " & _
				   "Phone3Ext, " & _
				   "Fax, " & _
				   "MailingAddress, " & _
				   "MailingAddress2, " & _
				   "MailingCity, " & _
				   "MailingStateId, " & _
				   "MailingZipcode, " & _
				   "MailingZipcodeExt, " & _
				   "MailingCountryId, " & _
				   "MainContactName, " & _
				   "MainContactPhone, " & _
				   "MainContactPhoneExt, " & _
				   "MainContactEmail, " & _
				   "MainContactFax, " & _
					"CustField1, " & _
					"CustField2, " & _
					"CustField3, " & _
					"CustField4, " & _
					"CustField5 " & _
				   ") VALUES (" & _
				   "'" & safe(g_iCompanyId) & "', " & _
				   "'" & safe(g_sName) & "', " & _
				   "'" & safe(g_sLegalName) & "', " & _
				   "'" & abs(cint(g_bDeleted)) & "', " & _
				   "'" & abs(cint(g_bInactive)) & "', " & _
				   "'" & safe(g_iIncorpStateId) & "', "
				   
			if g_dIncorpDate <> "" then
				sSql = sSql & "'" & safe(g_dIncorpDate) & "', "
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_dInactiveDate <> "" then
				sSql = sSql & "'" & safe(g_dInactiveDate) & "', "
			else
				sSql = sSql & "NULL, "
			end if			
			
			sSql = sSql & "'" & safe(g_iFiscalYearEndMonth) & "', " & _
				   "'" & safe(g_iFiscalYearEndDay) & "', " & _
				   "'" & safe(g_iOrgTypeId) & "', " & _
				   "'" & safe(g_iBusTypeId) & "', " & _
				   "'" & safe(g_sEin) & "', " & _
				   "'" & safe(g_sAddress) & "', " & _
				   "'" & safe(g_sAddress2) & "', " & _
				   "'" & safe(g_sCity) & "', " & _
				   "'" & safe(g_iStateId) & "', " & _
				   "'" & safe(g_sZipcode) & "', " & _
				   "'" & safe(g_sZipcodeExt) & "', " & _
				   "'" & safe(g_iCountryId) & "', " & _
				   "'" & safe(g_sWebsite) & "', " & _
				   "'" & safe(g_sEmail) & "', " & _
				   "'" & safe(g_sPhone) & "', " & _
				   "'" & safe(g_sPhoneExt) & "', " & _
				   "'" & safe(g_sPhone2) & "', " & _
				   "'" & safe(g_sPhone2Ext) & "', " & _
				   "'" & safe(g_sPhone3) & "', " & _
				   "'" & safe(g_sPhone3Ext) & "', " & _
				   "'" & safe(g_sFax) & "', " & _
				   "'" & safe(g_sMailingAddress) & "', " & _
				   "'" & safe(g_sMailingAddress2) & "', " & _
				   "'" & safe(g_sMailingCity) & "', " & _
				   "'" & safe(g_iMailingStateId) & "', " & _
				   "'" & safe(g_sMailingZipcode) & "', " & _
				   "'" & safe(g_sMailingZipcodeExt) & "', " & _
				   "'" & safe(g_iMailingCountryId) & "', " & _
				   "'" & safe(g_sMainContactName) & "', " & _
				   "'" & safe(g_sMainContactPhone) & "', " & _
				   "'" & safe(g_sMainContactPhoneExt) & "', " & _
				   "'" & safe(g_sMainContactEmail) & "', " & _
				   "'" & safe(g_sMainContactFax) & "', " & _
					"'" & g_sCustField1 & "', " & _
					"'" & g_sCustField2 & "', " & _
					"'" & g_sCustField3 & "', " & _
					"'" & g_sCustField4 & "', " & _
					"'" & g_sCustField5 & "' " & _
				   ")"
			set oRs = oConn.Execute(sSQL)

			'Now we'll try to pull out the new Company we just created.
			sSql = "SELECT CompanyL2ID FROM CompaniesL2 " & _
			       "WHERE Name = '" & g_sName & "'" & _
			       "ORDER BY CompanyL2ID DESC"

			set oRs = oConn.Execute(sSQL)

			if not (oRs.EOF and oRs.BOF) then
			
				g_iCompanyL2Id = oRs("CompanyL2Id")
				SaveCompany = g_iCompanyL2Id
			
			else
			
				'Our record was not created, or for some other reason we
				'couldn't pull the record back out.
				ReportError("CompanyL2 creation failed.")
				SaveCompany = 0
				exit function
			
			end if 
		
		end if
				
			   
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name:				DeleteCompany
	' Description:		Delete the loaded Company object from the relevant
	'                   database tables
	' Pre-conditions:	ConnectionString, CompanyId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteCompany()
	
		DeleteCompany = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iCompanyL2Id, 0, "CompanyL2 ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(g_iCompanyL2Id)
		
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete this company.")
			exit function
		
		else
		
			'Hooray
			DeleteCompany = 1
		
		end if 
		
		
		dim oConn 'DB connection
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		
		'We also need to delete, or reassign the other items associated with
		'the CompanyL2.  We'll start with deleting the Licenses.
		sSql = "UPDATE Licenses SET " & _
			"Deleted = '1', " & _
			"DeletedWithOwner = '1' " & _
			"WHERE OwnerId = '" & g_iCompanyL2Id & "' " & _
			"AND OwnerTypeId = '4'"
		oConn.Execute sSql, lRecordsAffected
		
		
		'Reassign any assigned CompanyL3s to the parent Company
		sSql = "UPDATE CompaniesL3 SET " & _
			"CompanyL2Id = NULL, " & _
			"CompanyId = '" & g_iCompanyId & "' " & _
			"WHERE CompanyL2Id = '" & g_iCompanyL2Id & "' "
		oConn.Execute sSql, lRecordsAffected
		
		
		'Reassign any assigned Branches to the parent Company
		sSql = "UPDATE CompanyBranches SET " & _
			"CompanyL2Id = NULL, " & _
			"CompanyL3Id = NULL, " & _
			"CompanyId = '" & g_iCompanyId & "' " & _
			"WHERE CompanyL2Id = '" & g_iCompanyL2Id & "' "
		oConn.Execute sSql, lRecordsAffected
		
		
		'Reassign any assigned officers to the parent Company
		sSql = "UPDATE Associates SET " & _
			"BranchId = NULL, " & _
			"CompanyL2Id = NULL, " & _
			"CompanyL3Id = NULL, " & _
			"CompanyId = '" & g_iCompanyId & "' " & _
			"WHERE CompanyL2Id = '" & g_iCompanyL2Id & "' " 
		oConn.Execute sSql, lRecordsAffected

		oConn.Close
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				DeleteCompanyById
	' Description:		Delete the specified Company object from the relevant
	'					database tables
	' Preconditions:	ConnectionString
	' Inputs:			p_iCompanyId - Company ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteCompanyById(p_iCompanyL2Id)
	
		DeleteCompanyById = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyL2Id, 0, "CompanyL2 ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(p_iCompanyL2Id)
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete the specified company.")
			exit function
			
		else
			
			DeleteCompanyById = 1

		end if 
			
		
	end function


	' -------------------------------------------------------------------------
	' Name:				SearchCompanies
	' Description:		Retrieves a collection of CompanyIDs that match the 
	'                   properties of the current object.  I.e., based on the
	'                   current Name, City, etc.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchCompanies()
	
		set SearchCompanies = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		

		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = false
		
		sSql = "SELECT Cs.CompanyL2Id, Cs.Name FROM CompaniesL2 AS Cs " & _
			"WHERE NOT Deleted = '1' "

		
		'if CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CompanyId = '" & g_iCompanyId & "'"
		end if
		
		
		'if Name search
		if g_sName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Name LIKE '%" & g_sName & "%'"
		end if
		
		
		'If NameStart search
		if g_sSearchNameStart <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Name LIKE '" & g_sSearchNameStart & "%'"
		end if
		
		
		'if Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
		
	
		'if Address search
		if g_sAddress <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Address LIKE '%" & g_sAddress & "%'" 
		end if
		
		'if City search
		if g_sCity <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.City LIKE '%" & g_sCity & "%'" 
		end if
		
		'if StateID search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StateID = '" & g_iStateId & "'" 
		end if

		'if Zipcode search
		if g_sZipcode <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Zipcode = '" & g_sZipcode & "'" 
		end if
		
		
		'If CustField1 search
		if g_sCustField1 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField1 LIKE '%" & g_sCustField1 & "%'"
		end if
		'If CustField2 search
		if g_sCustField2 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField2 LIKE '%" & g_sCustField2 & "%'"
		end if
		'If CustField3 search
		if g_sCustField3 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField3 LIKE '%" & g_sCustField3 & "%'"
		end if
		'If CustField4 search
		if g_sCustField4 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField4 LIKE '%" & g_sCustField4 & "%'"
		end if
		'If CustField5 search
		if g_sCustField5 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField5 LIKE '%" & g_sCustField5 & "%'"
		end if
		

		sSQL = sSQL & " ORDER BY Cs.Name"
		
		Response.Write(vbCrLF & "<!--" & sSql & "-->" & vbCrLF)
		
		set SearchCompanies = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name: SearchCompaniesLicenses
	' Desc: Retrieves a collection of CompanyL2 IDs that match the properties 
	'	of the current object.  Also searches license information.  Used in 
	'	reports.
	' Preconditions: ConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchCompaniesLicenses()
	
		set SearchCompaniesLicenses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = false 'true
		
		sSql = "SELECT DISTINCT Cs.CompanyL2Id, Cs.Name " & _
			"FROM CompaniesL2 AS Cs " & _
			"LEFT OUTER JOIN Licenses AS Ls " & _	
			"ON ((Ls.OwnerId = Cs.CompanyL2Id) AND (Ls.OwnerTypeId = '4') " & _
			"AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL)) " & _
			"WHERE Cs.Deleted = '0' "
	
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CompanyId = '" & g_iCompanyId & "'"
		end if 	
		
		
		'if Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
		
		
		'If CustField1 search
		if g_sCustField1 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField1 LIKE '%" & g_sCustField1 & "%'"
		end if
		'If CustField2 search
		if g_sCustField2 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField2 LIKE '%" & g_sCustField2 & "%'"
		end if
		'If CustField3 search
		if g_sCustField3 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField3 LIKE '%" & g_sCustField3 & "%'"
		end if
		'If CustField4 search
		if g_sCustField4 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField4 LIKE '%" & g_sCustField4 & "%'"
		end if
		'If CustField5 search
		if g_sCustField5 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CustField5 LIKE '%" & g_sCustField5 & "%'"
		end if
		
		
		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Cs.StateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 			
		
		
		'If License State Id List search
		if g_sSearchLicStateIdList <> "" then
			aStateIdList = split(g_sSearchLicStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 		
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Cs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				'IncludeL2ChildrenInSql sSql, iCompanyL2Id, "Cs."
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if

		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
		end if
		
		
		'If LicenseStatusIdList search
		dim aStatusIdList
		dim iStatusId
		dim bFirstStatus
		if g_sLicenseStatusIdList <> "" then
			aStatusIdList = split(g_sLicenseStatusIdList, ",")
			bFirstStatus = true
			for each iStatusId in aStatusIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstStatus then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStatusId = '" & iStatusId & "' "
				
				bFirstStatus = false
			
			next
			sSql = sSql & ")"
		end if
		
		
		'If LicenseNumber search
		if g_sLicenseNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNumber & "%'"
		end if


		'If SearchLicenseExpDateFrom/To search
		if g_dLicenseExpDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_dLicenseExpDateTo <> "" then
				if g_iAppDeadline = "1" then 
					sSql = sSql & "((Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') " & _
						"OR (" & _
						"Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "')) "
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') "
				end if
			else
				if g_iAppDeadline = "1" then
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"OR Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "')"					
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "') "  
				end if
			end if
		elseif g_dLicenseExpDateTo <> "" then
			if g_iAppDeadline = "1" then
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "' " & _
					"OR Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "')"		
			elseif g_iAppDeadline = "2" then
				sSql = sSql & "(Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "') "
			else
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "') "
			end if 
		end if 
				
		
		sSQL = sSQL & " ORDER BY Cs.Name"
		
		'Response.Write("<p>" & sSql & "</p>")
		
		set SearchCompaniesLicenses = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: GetAdmins
	' Desc: Returns a recordset containing the admins and their emails
	' Preconditions: ConnectionString, p_iCompanyL2Id
	' Returns: Recordset
	' -------------------------------------------------------------------------
	public function GetAdmins(p_iCompanyL2Id)
		
		set GetAdmins = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCompanyL2Id, 0, "CompanyL2 ID") then
			exit function
		end if
		
		dim sSql 'SQL Query		
		
		sSql = "SELECT DISTINCT Us.UserId, Us.FirstName, Us.MiddleName, " & _
			"Us.LastName, Us.Email " & _
			"FROM Users AS Us " & _
			"LEFT JOIN UsersCompaniesL2 AS UCs " & _
			"ON (UCs.UserId = Us.UserId) " & _
			"WHERE UCs.CompanyL2Id = '" & p_iCompanyL2Id & "' " & _
			"AND Us.GroupId = '3' " & _
			"AND Us.UserStatus = '1' "
				
		'Response.Write("<p>" & sSql & "<p>")
		set GetAdmins = QueryDb(sSql)
		
	end function	

    ' -------------------------------------------------------------------------
	' Name: GetCustField
	' Desc: Returns the value of the specified CustField
	' Preconditions: none
	' Inputs: p_iNo - the number of the Custom Field
	' Returns: string - the value of the Custom Field
	' -------------------------------------------------------------------------
	public function	GetCustField(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetCustField = g_sCustField1
			case 2
				GetCustField = g_sCustField2
			case 3
				GetCustField = g_sCustField3
			case 4
				GetCustField = g_sCustField4
			case 5
				GetCustField = g_sCustField5
			case else
				GetCustField = ""
		end select
	end function

	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			LookupState = oRs("State")
		end if 
		
		set oRs = nothing
		
	end function
		

	' -------------------------------------------------------------------------
	' Name:				LookupOrgType
	' Description:		Gets the name of the OrgTypeId from the passed
	'					OrgTypeId
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iOrgTypeId - Org Type to look up
	' Outputs:			None.
	' Returns:			String w/ Org Type Name, or empty
	' -------------------------------------------------------------------------
	public function LookupOrgType(p_iOrgTypeId)
	
		LookupOrgType = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM OrganizationTypes " & _
		       "WHERE OrganizationTypeId = '" & p_iOrgTypeId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupOrgType = oRs("OrganizationType")
		end if 
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:				LookupBusType
	' Description:		Gets the name of the BusTypeId from the passed Id
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iBusTypeId - Business Type to look up
	' Outputs:			None.
	' Returns:			String w/ Business Type Name, or empty
	' -------------------------------------------------------------------------
	public function LookupBusType(p_iBusTypeId)
	
		LookupBusType = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM BusinessTypes " & _
		       "WHERE BusTypeId = '" & p_iBusTypeId & "'"
		set oRs = QueryDb(sSql)
		
		LookupBusType = oRs("BusinessType")
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:				LookupLicenseStatus
	' Description:		Gets the name of the LicenseStatusId from the passed
	'					LicenseStatusId
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iLicenseStatusId - License Status to look up
	' Outputs:			None.
	' Returns:			String w/ License Status Name, or empty
	' -------------------------------------------------------------------------
	public function LookupLicenseStatus(p_iLicenseStatusId)
	
		LookupLicenseStatus = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM LicenseStatusTypes " & _
		       "WHERE LicenseStatusId = '" & p_iLicenseStatusId & "'"
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
		
			LookupLicenseStatus = ""
			
		else
			
			LookupLicenseStatus = oRs("LicenseStatus")
			
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupCompanyIdByName
	' Description:		Gets the unique Id based on the passed name
	' Pre-conditions:	ConnectionString, CompanyId
	' Inputs:			p_sName - Name to look up
	' Outputs:			None.
	' Returns:			CompanyL2Id, or 0
	' -------------------------------------------------------------------------
	public function LookupCompanyIdByName(p_sName)
	
		LookupCompanyIdByName = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT CompanyL2Id FROM CompaniesL2 " & _
			"WHERE Name LIKE '" & p_sName & "' " & _
			"AND Deleted = '0' " & _
			"AND CompanyId = '" & g_iCompanyId & "' "
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then 
			LookupCompanyIdByName = oRs("CompanyL2Id")
		end if
		
		set oRs = nothing
		
	end function	

	
	' -------------------------------------------------------------------------
	' Name:	Safe
	' Description: Escapes/scrubs characters to make content safe for
	'	submission into the database.
	' Inputs: p_sText - String to make database-safe.
	' Returns: Edited string
	' -------------------------------------------------------------------------
	public function Safe(p_sText)
	
		if p_sText <> "" then
	
			p_sText = replace(p_sText, "''", "'")
			p_sText = replace(p_sText, "'", "''")
		
			Safe = p_sText
			
		end if
	
	end function

	
End Class


%>
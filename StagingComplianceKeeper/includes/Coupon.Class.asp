<%

'==============================================================================
' Class: Coupon
' Controls the creation, modification, and removal of Coupons.
' Create Date: 09/07/04
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- 
' Methods:
'	- 
'==============================================================================

Class Coupon

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	
	'Base company properties
	dim g_iCouponId
	dim g_sCouponType '$/% character
	dim g_iCouponValue
	dim g_dCouponStartDate
	dim g_dCouponEndDate

	
	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	
	
	
	'Retrieves the unique ID of this Coupon.  This is an identity in the DB,
	'so it cannot be set.
	public property get CouponId()
		CouponId = g_iCouponId
	end property
	
	'Stores/retrieves the Coupon type.
	public property let CouponType(p_sCouponType)
		g_sCouponType = left(p_sCouponType, 1)
	end property
	public property get CouponType()
		CouponType = g_sCouponType
	end property

	'Stores/retrieves the Coupon value.
	public property let CouponValue(p_iCouponValue)
		g_iCouponValue = p_iCouponValue
	end property
	public property get CouponValue()
		CouponValue = g_iCouponValue
	end property

	'Stores/retrieves the Coupon start date.
	public property let CouponStartDate(p_dCouponStartDate)
		g_dCouponStartDate = p_dCouponStartDate
	end property
	public property get CouponStartDate()
		CouponStartDate = g_dCouponStartDate
	end property

	'Stores/retrieves the Coupon end date.
	public property let CouponEndDate(p_dCouponEndDate)
		g_dCouponEndDate = p_dCouponEndDate
	end property
	public property get CouponEndDate()
		CouponEndDate = g_dCouponEndDate
	end property

		
		
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'meh
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'meh
	
	end sub
	
	' ---------------------------------------------------------------------
	' Name:				CheckConnectionString
	' Description:		Check to make sure the connectionstring has been set
	' Inputs:			none
	' Output:			none
	' -------------------------------------------------------------------------
	Private Sub CheckConnectionString()
		if isNull(g_sConnectionString) then
			response.write "You must specify the value of the ConnectionString Property value before proceeding."
		end if
	End Sub

	' -------------------------------------------------------------------------
	' Name:				CheckParam
	' Description:		Checks to make sure the passed in value is not null
	' Inputs:			p_sParam - the required parameter
	' Output:			none
	' -------------------------------------------------------------------------
	Private Sub CheckParam(p_sParam)
		if ((isNull(p_sParam)) or (trim(p_sParam) = "")) then
			response.write "Processing Error!!!<br>You must specify the value before proceeding."
			response.end
		end if
	End Sub
	
	' -------------------------------------------------------------------------
	' Name:				QueryDB
	' Description:		executes the query in the database
	' Inputs:			sSQL - the query to execute
	' Output:			recordset - returns a recordset of the executed query
	' -------------------------------------------------------------------------
	Private Function QueryDb(sSql)
		CheckParam(sSQL)
		CheckConnectionString	
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		'    objRS.ActiveConnection = Connect
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name:				LoadId
	' Description:		This method attempts to load the properties of a stored
	'					Company based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadId(p_iCouponId)
	
		CheckConnectionString
		
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")

		sSql = "SELECT * FROM Coupons " & _
			   "WHERE CouponID = '" & p_iCouponId & "'"

		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iCouponId = oRs("CouponID")
			g_sCouponType = oRs("CouponType")
			g_iCouponValue = oRs("CouponValue")
			g_dCouponStartDate = oRs("StartDate")
			g_dCouponEndDate = oRs("EndDate")
			
			LoadId = g_iCouponId
		
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.
			LoadId = 0
			exit function
	
		end if
			
		set oRs = nothing
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name:				SaveId
	' Description:		Save the Coupon object in the database, and assign it
	'					the resulting Coupon ID.
	' Pre-conditions:	ConnectionString, CouponType, CouponValue, 
	'					CouponStartDate, CouponEndDate
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns the new ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveId()
	
		'Verify our preconditions
		CheckConnectionString
		CheckParam(g_sCouponType)
		CheckParam(g_iCouponValue)
		CheckParam(g_dCouponStartDate)
		CheckParam(g_dCouponEndDate)

		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		'If we have a CouponId property assigned already, we're working on a 
		'loaded Coupon, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iCouponId <> "" then
		
			'Now we can proceed with building our SQL statement.
			bIsNew = false
			sSql = "UPDATE Coupons SET " & _
				   "CouponType = '" & g_sCouponType & "', " & _
				   "CouponValue = '" & g_iCouponValue & "', " & _
				   "StartDate = '" & g_dCouponStartDate & "', " & _
				   "EndDate = '" & g_dCouponEndDate & "' " & _
				   "WHERE CouponID = '" & g_iCouponId & "'"
				   
			oConn.Execute(sSQL)
			
			SaveId = g_iCouponId

		else

			'Build an SQL statement for a new Company.
			bIsNew = true
			
			sSql = "INSERT INTO Coupons " & _
				   "(CouponType, CouponValue, StartDate, EndDate) " & _
				   "VALUES (" & _
				   "'" & g_sCouponType & "', " & _
				   "'" & g_iCouponValue & "', " & _
				   "'" & g_dCouponStartDate & "', " & _
				   "'" & g_dCouponEndDate & "' " & _
				   ")"
				   
			set oRs = oConn.Execute(sSQL)
				   
			'Now we'll try to pull out the new Coupon we just created.
			sSql = "SELECT CouponID FROM Coupons " & _
				   "WHERE CouponType = '" & g_sCouponType & "' AND " & _
				   "CouponValue = '" & g_iCouponValue & "' AND " & _
				   "StartDate = '" & g_dCouponStartDate & "' AND " & _
				   "EndDate = '" & g_dCouponEndDate & "' " & _
				   "ORDER BY CouponID DESC"
		
			set oRs = oConn.Execute(sSQL)
						
			if not (oRs.EOF and oRs.BOF) then
			
				g_iCouponId = oRs("CouponId")
				SaveId = g_iCouponId
			
			else
			
				'Our record was not created, or for some other reason we
				'couldn't pull the record back out.
				Response.Write("Error inserting new data: Update Unsuccessful")
				SaveId = 0
				exit function
			
			end if 
			
		end if
			   
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name:				DeleteId
	' Description:		Delete the Coupon object from the relevant database
	'                   tables, and remove the virtual directory
	' Pre-conditions:	ConnectionString, CouponId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteId()
	
		'Verify our preconditions
		CheckConnectionString
		CheckParam(g_iCouponId)

		dim oConn 'DB connection
		dim oRs 'Recordset
		dim sSql 'SQL statement

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")

		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		'We'll clean out our records from the database tables.
		sSql = "DELETE FROM Coupons WHERE CouponID = '" & g_iCouponId & "'"
		oConn.Execute(sSql)
		
		'Check for errors.
		if err <> 0 then
			
			Response.Write("Error: " & err.number & ", " & err.Description)
			DeleteId = 0
			exit function
			
		end if

		g_iCouponId = ""

		DeleteId = 1
	
	end function


	' -------------------------------------------------------------------------
	' Name:				SearchIds
	' Description:		Retrieves a collection of CouponIDs that match the 
	'                   properties of the current object.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchIds()
	
		'Verify our preconditions
		CheckConnectionString
		
		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT DISTINCT(Cs.CouponID) " & _
		       "FROM Coupons AS Cs"

		
		'if Type search
		if g_sCouponType <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CouponType = '" & g_sCouponType & "'"
		end if
		
		'if Value search
		if g_iCouponValue <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE"
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.CouponValue = '%" & g_iCouponValue & "%'"
		end if
		
		'if StartDate search
		if g_dCouponStartDate <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StartDate > '%" & g_dCouponStartDate & "%'" 
		end if
		
		'if EndDate search
		if g_dCouponEndDate <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.EndDate < '%" & g_dCouponEndDate & "%'" 
		end if
		
		set SearchIds = QueryDb(sSql)
		
	end function
	
End Class

%>
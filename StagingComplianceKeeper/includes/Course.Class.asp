<%
'==============================================================================
' Class: Course
' Controls the creation, modification, and removal of Courses.
' Create Date: 7/11/05
' Author: Mark Silvia
' -------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
'	- CourseId
'	- OwnerCompanyId
'	- Name
'	- Number
'	- CourseTypeId
'	- StateId
'	- ProviderId
'	- ContEdHours
'	- ExpDate
'	- StateApproved
'	- NonApproved
'	- Deleted
'	- InstructorName
'	- InstructorEducationLevel
'   - AccreditationTypeID
' Methods:
'	- LoadCourseById
'	- SaveCourse
'	- DeleteCourse
'	- DeleteCourseById
'	- SearchCourses
'	- LookupState
'	- LookupCourseType
'==============================================================================

Class Course

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Base course properties
	dim g_iCourseId
	dim g_sName
	dim g_sNumber
	dim g_iCourseTypeId
	dim g_iStateId
	dim g_iProviderId
	dim g_iOwnerCompanyId
	dim g_rContEdHours
	dim g_dExpDate
	dim g_dCompletionDate
	dim g_bStateApproved
	dim g_bNonApproved
	dim g_bDeleted
	dim g_sInstructorName
	dim g_sInstructorEducationLevel
	dim g_iAccreditationTypeID
	
	' PUBLIC PROPERTIES =======================================================
	
	'Stores/retrieves the database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Stores/retrieves the trainingpro database connection string
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
		
	'Retrieves the unique Id of this course.  This is an identity in the DB,
	'so it is read-only.
	public property get CourseId()
		CourseId = g_iCourseId
	end property
	
	
	'Stores/retrieves the course name
	public property get Name()
		Name = g_sName
	end property
	public property let Name(p_sName)
		g_sName = left(p_sName, 100)
	end property
	
	
	'Optional course number
	public property let Number(p_sNumber)
		g_sNumber = left(p_sNumber, 50)
	end property
	public property get Number()
		Number = g_sNumber
	end property
	
	
	'CourseType ID - Online, home study, etc.
	public property get CourseTypeId()
		CourseTypeId = g_iCourseTypeId
	end property
	public property let CourseTypeId(p_iCourseTypeId)
		if isnumeric(p_iCourseTypeId) then
			g_iCourseTypeId = abs(cint(p_iCourseTypeId))
		elseif p_iCourseTypeId = "" then
			g_iCourseTypeId = ""
		else
			ReportError("CourseTypeId requires a positive integer value.")
		end if
	end property
	
	
	'State ID - the state which offers this course
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then	
			g_iStateId = abs(cint(p_iStateId))
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
			
	
	'Provider ID - which education provider presents this course?
	public property get ProviderId()
		ProviderId = g_iProviderId
	end property
	public property let ProviderId(p_iProviderId)
		if isnumeric(p_iProviderId) then
			g_iProviderId = abs(cint(p_iProviderId))
		elseif p_iProviderId = "" then
			g_iProviderId = ""
		else
			ReportError("ProviderId requires a positive integer value.")
		end if
	end property
	
	
	'Company ID of the company which owns this course item
	public property get CompanyId()
		CompanyId = g_iOwnerCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iOwnerCompanyId = cint(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iOwnerCompanyId = "" 
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'ContEdHours - Number of hours of continuing education for which this
	'course counts.
	public property get ContEdHours()
		ContEdHours = g_rContEdHours
	end property
	public property let ContEdHours(p_rContEdHours)
		if isnumeric(p_rContEdHours) then
			g_rContEdHours = cdbl(p_rContEdHours)
		elseif p_rContEdHours = "" then	
			g_rContEdHours = ""
		else
			ReportError("Invalid ContEdHours value.  Numeric value required.")
		end if 
	end property
	
	
	'Expiration date for the course
	public property get ExpDate()
		ExpDate = g_dExpDate
	end property
	public property let ExpDate(p_dExpDate)
		if isdate(p_dExpDate) then
			g_dExpDate = p_dExpDate
		elseif p_dExpDate = "" then
			g_dExpDate = ""
		else
			ReportError("Invalid ExpDate value.  Date required.")
		end if
	end property
	
	
	'Date the user completed the course
	public property get CompletionDate()
		CompletionDate = g_dCompletionDate
	end property
	public property let CompletionDate(p_dCompletionDate)
		if isdate(p_dCompletionDate) then
			g_dCompletionDate = p_dCompletionDate
		elseif p_dCompletionDate = "" then
			g_dCompletionDate = ""
		else
			ReportError("Invalid CompletionDate value.  Date required.")
		end if
	end property


	'Notes whether the course is state approved
	public property get StateApproved()
		StateApproved = g_bStateApproved
	end property
	public property let StateApproved(p_bStateApproved)
		if isnumeric(p_bStateApproved) then
			g_bStateApproved = cbool(p_bStateApproved)
		elseif isnull(p_bStateApproved) or p_bStateApproved = "" then
			g_bStateApproved = ""
		else
			ReportError("Invalid StateApproved value.  Boolean required.")
		end if
	end property	


	'Notes whether the course is Non approved
	public property get NonApproved()
		NonApproved = g_bNonApproved
	end property
	public property let NonApproved(p_bNonApproved)
		if isnumeric(p_bNonApproved) then
			g_bNonApproved = cbool(p_bNonApproved)
		elseif isnull(p_bNonApproved) or p_bNonApproved = "" then
			g_bNonApproved = ""
		else
			ReportError("Invalid NonApproved value.  Boolean required.")
		end if
	end property	
	
	
	'Notes whether the course has been deleted from the site.
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	public property get InstructorName()
		InstructorName = g_sInstructorName
	end property
	public property let InstructorName(p_sInstructorName)
		g_sInstructorName = p_sInstructorName
	end property
	
	public property get InstructorEducationLevel()
		InstructorEducationLevel = g_sInstructorEducationLevel
	end property
	public property let InstructorEducationLevel(p_sInstructorEducationLevel)
		g_sInstructorEducationLevel = p_sInstructorEducationLevel
	end property
	
    public property get AccreditationTypeID()
        AccreditationTypeID = g_iAccreditationTypeID
    end property
    public property let AccreditationTypeID(p_iAccreditationTypeID)
        g_iAccreditationTypeID = p_iAccreditationTypeID
    end property

	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'nothing

	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'nothing
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		CheckConnectionString = false
	
		if isnull(g_sConnectionString) or _
			g_sConnectionString = "" then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) or _
			g_sTpConnectionString = "" then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Mark the specified Course entry as deleted
	' Preconditions: Connectionstring
	' Inputs: p_iCourseId - Course ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iCourseId)
	
		DeleteId = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCourseId, false, "Course ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iCourseId < 0 then
			
			oConn.ConnectionString = g_sTpConnectionString
			oConn.Open
			
			'Mark the course as deleted in the trainingpro database
			sSql = "UPDATE Courses SET " & _
				"Deleted = '1' " & _
				"WHERE CourseId = '" & p_iCourseId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the course.")
				exit function
				
			end if
			
		end if
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadCourseById
	' Description: Loads a course's information into the course object
	' Preconditions: ConnectionString
	' Inputs: p_iCourseId - ID of the course to load
	' Returns: Returns the Course ID if successful, otherwise 0
	' -------------------------------------------------------------------------
	public function LoadCourseById(p_iCourseId)
		
		LoadCourseById = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then 
			exit function
		elseif not CheckIntParam(p_iCourseId, false, "Course ID") then
			exit function
		end if
		
		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		'set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iCourseId > 0 then
            sSql = "SELECT Courses.*, Courses.AccreditationType AS AccreditationTypeID, CoursesTypesX.TypeID FROM Courses " & _
                   "LEFT JOIN CoursesTypesX ON Courses.CourseID = CoursesTypesX.CourseID " & _
                   "WHERE Courses.CourseId = '" & p_iCourseId & "'"
            set oRs = QueryTPDb(sSql)
        else
		    sSql = "SELECT * FROM vCourses AS Cs " & _
			    "WHERE Cs.CourseId = '" & p_iCourseId & "'"
		    set oRs = QueryDb(sSql)
        end if
		
				
		if not (oRs.BOF and oRs.EOF) then
			
			g_iCourseId = oRs("CourseId")
			g_sName = oRs("Name")
			g_iCourseTypeId = oRs("TypeId")
			g_iStateId = oRs("StateId")
			g_dExpDate = oRs("ExpirationDate")
            g_iAccreditationTypeID = oRs("AccreditationTypeID")
			'g_dCompletionDate = oRs("CompletionDate")
		
			
			if p_iCourseId < 0 then
			
				g_sNumber = oRs("Number") 
				g_iProviderId = oRs("ProviderId")
				g_iOwnerCompanyId = oRs("OwnerCompanyId")
				g_rContEdHours = oRs("ContEdHours")
				g_bDeleted = oRs("Deleted")
				g_bStateApproved = oRs("StateApproved")
				g_bNonApproved = oRs("NonApproved")
				g_sInstructorName = oRs("InstructorName")
				g_sInstructorEducationLevel = oRs("InstructorEducationLevel")
			else
			
				g_sNumber = ""
				g_iProviderId = "0"
				g_iOwnerCompanyId = ""
				g_rContEdHours = ""
				g_bDeleted = ""
				g_bStateApproved = "" 
				g_bNonApproved = ""
				g_sInstructorName = ""
				g_sInstructorEducationLevel = ""
			end if 
			
			LoadCourseById = g_iCourseId

		else
		
			ReportError("Unable to load Course.  Course ID not found.")
			exit function
			
		end if 
		
		
		'set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveCourse
	' Desc: Save the course object in the database, and assign it the resulting
	'	ID.  We do not save TP course items, since they cannot be modified by
	'	CMS company admins.
	' Preconditions: Connectionstring
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveCourse()
	
		SaveCourse = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sName, false, "Course Name") or _
			not CheckIntParam(g_iProviderId, false, "Provider ID") then
			exit function
		end if 
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if g_iCourseId = "" then
		
			'This is a new course being added to CMS, so we'll INSERT a new
			'record
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "INSERT INTO Courses (" & _
				"Name, " & _
				"Number, " & _
				"TypeId, " & _
				"ProviderId, " & _
				"StateId, " & _
				"ExpirationDate, " & _
				"OwnerCompanyId, " & _
				"ContEdHours, " & _
				"StateApproved, " & _
				"NonApproved, " & _
				"Deleted, " & _
				"InstructorName, " & _
				"InstructorEducationLevel, " & _
                "AccreditationTypeID" & _
				") VALUES (" & _
				"'" & g_sName & "', " & _
				"'" & g_sNumber & "', " & _
				"'" & g_iCourseTypeId & "', " & _
				"'" & g_iProviderId & "', " & _
				"'" & g_iStateId & "', "
				
				if g_dExpDate <> "" then
					sSql = sSql & "'" & g_dExpDate & "', "
				else
					sSql = sSql & "NULL, "
				end if
				
				sSql = sSql & "'" & g_iOwnerCompanyId & "', " & _
				"'" & g_rContEdHours & "', " & _
				"'" & abs(cint(g_bStateApproved)) & "', " & _
				"'" & abs(cint(g_bNonApproved)) & "', " & _
				"'" & g_bDeleted & "', " & _
				"'" & g_sInstructorName & "', " & _
				"'" & g_sInstructorEducationLevel & "', " & _
                "'" & g_iAccreditationTypeID & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to create new Course.")
				exit function
			
			end if
			
			'Attempt to retrieve the index of the newly created course.
			sSql = "SELECT CourseId FROM Courses " & _
				"WHERE Name LIKE '" & g_sName & "' " & _
				"AND OwnerCompanyId = '" & g_iOwnerCompanyId & "' " & _
				"ORDER BY CourseId ASC"
			set oRs = oConn.Execute(sSql)
			
			if oRs.EOF then 
			
				ReportError("Failed to create new Course.")
				exit function
				
			else
			
				g_iCourseId = oRs("CourseId")
				SaveCourse = g_iCourseId
				
			end if
			
		elseif g_iCourseId < 0 then
		
			'This is an update to an existing course.
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "UPDATE Courses SET " & _
				"Name = '" & g_sName & "', " & _
				"Number = '" & g_sNumber & "', " & _
				"TypeId = '" & g_iCourseTypeId & "', " & _
				"ProviderId = '" & g_iProviderId & "', " & _
				"StateId = '" & g_iStateId & "', "
				
			if g_dExpDate <> "" then
				sSql = sSql & "ExpirationDate = '" & _
				g_dExpDate & "', "
			else
				sSql = sSql & "ExpirationDate = NULL, "
			end if
			
			'if g_dCompletionDate <> "" then
			'	sSql = sSql & "CompletionDate = '" & _
			'	g_dCompletionDate & "', "
			'else
			'	sSql = sSql & "CompletionDate = NULL, "
			'end if 
				
			sSql = sSql & "OwnerCompanyId = '" & g_iOwnerCompanyId & "', " & _
				"ContEdHours = '" & g_rContEdHours & "', " & _
				"StateApproved = '" & abs(cint(g_bStateApproved)) & "', " & _
				"NonApproved = '" & abs(cint(g_bNonApproved)) & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "', " & _
				"InstructorName = '" & g_sInstructorName & "', " & _
				"InstructorEducationLevel = '" & g_sInstructorEducationLevel & "', " & _
                "AccreditationTypeID = '" & g_iAccreditationTypeID & "' " & _
				"WHERE CourseId = '" & g_iCourseId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected = 1 then
				
				ReportError("Failed to save course information.")
				exit function
				
			end if
			
			SaveCourse = g_iCourseId
			
		elseif g_iCourseId > 0 then
		
			ReportError("TrainingPro courses may not be modified.")
			exit function
			
		end if 
		
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: ReleaseCourse
	' Desc: Clears set/loaded Course properties
	' Preconditions: ConnectionString
	' -------------------------------------------------------------------------
	public sub ReleaseCourse()
	
		g_iCourseId = ""
		g_sName = ""
		g_sNumber = ""
		g_iCourseTypeId = ""
		g_iStateId = ""
		g_iProviderId = ""
		g_iOwnerCompanyId = ""
		g_rContEdHours = "" 
		g_dExpDate = ""
		g_dCompletionDate = ""
		g_bStateApproved = ""
		g_bNonApproved = ""
		g_bDeleted = ""
		g_sInstructorName = ""
		g_sInstructorEducationLevel = ""
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: SearchCourses
	' Desc: Retrieves a collection of CourseIds that match the properties of 
	'	the current object.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the resulting recordset
	' -------------------------------------------------------------------------
	public function SearchCourses()
		
		set SearchCourses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		dim bFirstClause 'Used to direct SQL phrasing
		dim sSql 'SQL Statement
		dim oRs 'Recordset object

		bFirstClause = true
		
		sSql = "SELECT Cs.CourseId, Cs.Name " & _
			"FROM vCourses AS Cs "

	
		'If Course Name search
		if g_sName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Name LIKE '%" & g_sName & "%'"
		end if 
		
		
		'If Course Number search
		if g_sNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Number LIKE '%" & g_sNumber & "%'"
		end if 
		
		
		'If Course Type search
		if g_iCourseTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.TypeId = '" & g_iCourseTypeId & "'"
		end if 
		
        'If Accreditation Type search
        if g_iAccreditationTypeID <> "" then
            if bFirstClause then
                sSql = sSql & " AND "
                bFirstClause = false
            else
                sSql = sSql & " AND "
            end if
            sSql = sSql & "Cs.AccreditationTypeID = '" & g_iAccreditationTypeID & "'"
        end if
		
		'If Provider search
		if g_iProviderId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.ProviderId = '" & g_iProviderId & "'"
		end if 


		'If Owner search
		if g_iOwnerCompanyId <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.OwnerCompanyId = '" & g_iOwnerCompanyId & "'"
		end if 


		'If ContEdHours search
		if g_rContEdHours <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.ContEdHours = '" & g_rContEdHours & "'"
		end if
		
		
		'If StateId search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StateId = '" & g_iStateId & "'"
		end if 
		
		
		'If Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if 

		sSql = sSql & " ORDER BY Name"
		
		'response.write("<p>" & sSql & "<p>")

		set SearchCourses = QueryDb(sSql)

	end function


	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCertificate()
	' Purpose:				Retrieves the Certificate Information for the specified course
	' Required Properties:  ConnectionString, iCourseID
	' Optional Properties:
	' Parameters:			n/a
	' Outputs:				recordset of the certificate information
	' ----------------------------------------------------------------------------------------------------------
	Function GetCertificate()
		dim sSQL 'as string
		
		if iCourseId <> "" then
		
			sSQL = "SELECT * FROM Certificates " & _
				   "WHERE CourseID = '" & iCourseID & "' "
		
			set GetCertificate = QueryTpDb(sSQL)

		else
			
			set GetCertificate = Server.CreateObject("ADODB.Recordset")
			
		end if
		
	End Function


	' -------------------------------------------------------------------------
	' Name: LookupProvider
	' Desc: Retrieves the name associated with a specific Provider 
	' Preconditions: ConnectionString, TpConnectionString
	' Inputs: ProviderId - ID of the Provider to retrieve
	' Returns: Returns the resulting name string
	' -------------------------------------------------------------------------
	public function LookupProvider(p_iProviderId)
	
		LookupProvider = ""
		
		'Verify preconditions/inputs
		if not CheckConnectionString then 
			exit function
		elseif not CheckIntParam(p_iProviderId, true, "Provider ID") then
			exit function
		end if
		
		'TrainingPro is always provider 0, and not a normal provider listing
		'in the database.
		if p_iProviderId = 0 then
			LookupProvider = "TrainingPro"
			exit function
		end if
		
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		
		sSql = "SELECT Name FROM Providers " & _
			"WHERE ProviderId = '" & p_iProviderId & "' "
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
		
			LookupProvider = oRs("Name")
		
		end if 
		
		set oRs = nothing	
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupProviderId
	' Desc: Retreives a unique Provider ID given a provider name and company ID
	' Preconditions: ConnectionString, TpConnectionString
	' Inputs: p_sProviderName - Name of the provider
	'		p_iCompanyId - Company Id
	' Returns: Returns the resulting Provider ID, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupProviderId(p_sProviderName, p_iCompanyId)
	
		LookupProviderId = ""
		
		'Verify proconditions/inputs
		if not CheckConnectionString or _
			not CheckStrParam(p_sProviderName, true, "Provider Name") or _
			not CheckIntParam(p_iCompanyId, true, "Company ID") then
			exit function
		end if
		
		dim oRs 'Recordset
		dim sSql 'SQL statement
		
		sSql = "SELECT ProviderId FROM Providers " & _
			"WHERE Name LIKE '" & p_sProviderName & "' " & _
			"AND OwnerCompanyId = '" & p_iCompanyId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			
			LookupProviderId = oRs("ProviderId")
			
		end if
		
		set oRs = nothing
		
	end function
	
	
end class
%>
		
		
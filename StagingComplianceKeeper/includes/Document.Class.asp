<%
'==============================================================================
' Class: Document
' Controls the creation, modification, and removal of documents and forms that
'	may be tracked by the site users.
' Create Date: 5/27/05
' Author: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- DocId
'	- DocName
'	- DocSentDate
'	- DocReceivedDate
'	- DocTypeId
'	- DocStateId
'	- OwnerId
'	- Deleted
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
'
' Methods
'	- LoadDocById
'	- SaveDoc
'	- SearchDocs
'	- LookupDocStateId
'==============================================================================

class Document

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Document properties
	dim g_iDocId
	dim g_sDocName
	dim g_dDocSentDate
	dim g_dDocReceivedDate
	dim g_iDocTypeId
	dim g_iDocStateId
	dim g_iOwnerId
	dim g_bDeleted
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Document
	public property get DocId()
		DocId = g_iDocId
	end property
	
	
	'Document name
	public property get DocName()
		DocName = g_sDocName
	end property
	public property let DocName(p_sDocName)
		g_sDocName = left(p_sDocName, 50)
	end property
	
	
	'Date the document was sent out.
	public property get DocSentDate
		DocSentDate = g_dDocSentDate
	end property
	public property let DocSentDate(p_dDocSentDate)
		if isdate(p_dDocSentDate) then
			g_dDocSentDate = cdate(p_dDocSentDate)
		elseif p_dDocSentDate = "" then
			g_dDocSentDate = ""
		else
			ReportError("Invalid DocSentDate value.  Date required.")
		end if
	end property
	
	
	'Date the document was received back.
	public property get DocReceivedDate
		DocReceivedDate = g_dDocReceivedDate
	end property
	public property let DocReceivedDate(p_dDocReceivedDate)
		if isdate(p_dDocReceivedDate) then
			g_dDocReceivedDate = cdate(p_dDocReceivedDate)
		elseif p_dDocReceivedDate = "" then
			g_dDocReceivedDate = ""
		else
			ReportError("Invalid DocReceivedDate value.  Date required.")
		end if
	end property
	
	
	'DocTypeId - Type of recognized document this is.
	public property get DocTypeId
		DocTypeId = g_iDocTypeId
	end property
	public property let DocTypeId(p_iDocTypeId)
		if isnumeric(p_iDocTypeId) then
			g_iDocTypeId = cint(p_iDocTypeId)
		elseif p_iDocTypeId = "" then
			g_iDocTypeId = ""
		else
			ReportError("Invalid DocTypeId value.  Integer required.")
		end if
	end property
	
	
	'DocStateId - Document status ID
	public property get DocStateId
		DocStateId = g_iDocStateId
	end property
	public property let DocStateId(p_iDocStateId)
		if isnumeric(p_iDocStateId) then
			g_iDocStateId = cint(p_iDocStateId)
		elseif p_iDocStateId = "" then
			g_iDocStateId = ""
		else
			ReportError("Invalid DocStateId value.  Integer required.")
		end if
	end property
	
	
	'OwnerId - UserId of the member who added this document
	public property get OwnerId()
		OwnerId = g_iOwnerId
	end property
	public property let OwnerId(p_iOwnerId)
		if isnumeric(p_iOwnerId) then
			g_iOwnerId = cint(p_iOwnerId)
		elseif p_iOwnerId = "" then
			g_iOwnerId = ""
		else
			ReportError("Invalid OwnerId value.  Integer required.")
		end if
	end property
	
	
	'Deleted - Has this record been marked as deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = ""
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(sSql & "<p>")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadDocById
	' Desc: Load a document based on the passed DocId
	' Preconditions: ConnectionString
	' Inputs: p_iDocId - ID of the document to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadDocById(p_iDocId)
		
		LoadDocById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iDocId, 0, "Doc ID") then
			exit function
		end if 
		
		
		dim oRs 'RecordSet
		dim sSql 'SQL Statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM Documents " & _
			"WHERE DocId = '" & p_iDocId & "'" 
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the document properties
			g_iDocId = oRs("DocId")
			g_sDocName = oRs("DocName")
			g_dDocSentDate = oRs("DocSentDate")
			g_dDocReceivedDate = oRs("DocReceivedDate")
			g_iDocTypeId = oRs("DocTypeId")
			g_iDocStateId = oRs("DocStateId")
			g_iOwnerId = oRs("OwnerId")
			g_bDeleted = oRs("Deleted")
			
			LoadDocById = g_iDocId
		
		else
			
			ReportError("Unable to load the passed Doc ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveDoc
	' Desc: Saves a document object's properties to the database.
	' Preconditions: ConnectionString, OwnerId, DocName
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveDoc()
		
		SaveDoc = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sDocName, 0, "Document Name") or _
			not CheckIntParam(g_iOwnerId, 0, "Owner ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a DocId property assigned already, we're working on a
		'loaded document, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iDocId <> "" then
		
			bIsNew = false
			sSql = "UPDATE Documents SET " & _
				"DocName = '" & g_sDocName & "', "

			if g_dDocSentDate <> "" then
				sSql = sSql & "DocSentDate = '" & g_dDocSentDate & _
					"', "
			else
				sSql = sSql & "DocSentDate = NULL, "
			end if 
				
			if g_dDocReceivedDate <> "" then
				sSql = sSql & "DocReceivedDate = '" & g_dDocReceivedDate & _
					"', "
			else
				sSql = sSql & "DocReceivedDate = NULL, "
			end if 
			
			sSql = sSql & "DocTypeId = '" & g_iDocTypeId & "', " & _
				"DocStateId = '" & g_iDocStateId & "', " & _
				"OwnerId = '" & g_iOwnerId & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE DocId = '" & g_iDocId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				
				SaveDoc = g_iDocId
				
			else
				
				ReportError("Unable to save existing Document.")
				exit function
				
			end if
			
		else
		
			bIsNew = true
			sSql = "INSERT INTO Documents (" & _
				"DocName, " & _
				"DocSentDate, " & _
				"DocReceivedDate, " & _
				"DocTypeId, " & _
				"DocStateId, " & _
				"OwnerId, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_sDocName & "', "
				
			if g_dDocSentDate <> "" then
				sSql = sSql & "'" & g_dDocSentDate & "', "
			else
				sSql = sSql & "NULL, "
			end if 
			
			if g_dDocReceivedDate <> "" then
				sSql = sSql & "'" & g_dDocReceivedDate & "', "
			else
				sSql = sSql & "NULL, "
			end if 
			
			sSql = sSql & "'" & g_iDocTypeId & "', " & _
				"'" & g_iDocStateId & "', " & _
				"'" & g_iOwnerId & "', " & _
				"'" & g_bDeleted & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new document
			sSql = "SELECT DocId FROM Documents " & _
				"WHERE OwnerId = '" & g_iOwnerId & "' " & _
				"AND DocName = '" & g_sDocName & "' " & _
				"ORDER BY DocId DESC" 
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				g_iDocId  = oRs("DocId")
				SaveDoc = g_iDocId 
				
			else
			
				'The record could not be retrieved
				ReportError("Failed to save new Document.")
				exit function
				
			end if 
			
		end if 
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			

	' -------------------------------------------------------------------------
	' Name: SearchDocs
	' Desc: Retrieves a collection of DocIds that match the properties of
	'		the current object instance.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchDocs()
		
		set SearchDocs = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql 'SQL statement
		dim oRs 'Recordset object

		
		bFirstClause = false
		
		sSql = "SELECT 'DOC', Ds.DocId, " & _
			"Ds.DocName, Ds.DocSentDate " & _
			"FROM Documents AS Ds " & _
			"WHERE (NOT Ds.Deleted = '1') "
			
		'If DocName search
		if g_sDocName <> "" then
			sSql = sSql & " AND " 
			sSql = sSql & "Ds.DocName LIKE '%" & g_sDocName & "%'"
		end if 
		
		'If DocTypeId search
		if g_iDocTypeId <> "" then
			sSql = sSql & " AND " 
			sSql = sSql & "Ds.DocTypeId = '" & g_iDocTypeId & "'"
		end if
		
		'If DocStateId search
		if g_iDocStateId <> "" then
			sSql = sSql & " AND "
			sSql = sSql & "Ds.DocStateId = '" & g_iDocStateId & "'"
		end if
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			sSql = sSql & " AND " 
			sSql = sSql & "Ds.OwnerId = '" & g_iOwnerId & "'"
		end if
		
		sSql = sSql & " UNION "
		
		sSql = sSql & "SELECT 'LIC', Ls.LicenseId, " & _
			"Ls.LicenseNum, Ls.SubmissionDate " & _
			"FROM Licenses AS Ls " & _
			"WHERE NOT Ls.Deleted = '1' " & _
			"AND Ls.OwnerCompanyId = '" & g_iOwnerId & "' " & _
			"AND Ls.LicenseStatusId = '10' " & _
			"AND (NOT Ls.SubmissionDate IS NULL " & _
			"AND NOT Ls.SubmissionDate = '') " & _
			"AND (Ls.ReceivedDate IS NULL " & _
			"OR Ls.ReceivedDate = '') "
		
		sSql = sSql & " ORDER BY Ds.DocSentDate DESC"
		
		'Response.Write(sSql)
		
		set SearchDocs = QueryDb(sSql) 
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name: LookupDocStateId
	' Desc: Looks up the given document state ID
	' Preconditions: ConnectionString
	' Inputs: p_iDocStateId
	' Returns: String with document state description
	' -------------------------------------------------------------------------
	public function LookupDocStateId(p_iDocStateId)
	
		LookupDocStateId = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset 
		
		sSql = "SELECT DocState FROM DocumentStates " & _
			"WHERE DocStateId = '" & g_iDocStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupDocStateId = oRs("DocState")
		end if 
		
		set oRs = nothing
		
	end function
	
	
	
end class
%>
	
<%
sub SendStoredEmail(p_iUserId, p_iEmailId)

	dim oJmail
	dim oAssociate
	dim oSettings
	dim sSuccessURL
	dim sBody
	

	'Create an instance of the jmail object and set its initial values
	set oJmail = server.CreateObject("Jmail.Message")
	oJmail.ContentType = "text/html"
	
	
	'Instantiate the associate object and load the associate based on the
	'passed UserID
	set oAssociate = new Associate
	oAssociate.ConnectionString = application("sDataSourceName")
	oAssociate.TpConnectionString = application("sTpDataSourceName")
	oAssociate.VocalErrors = application("bVocalErrors")
	
	if oAssociate.LoadAssociateById(p_iUserId) = 0 then
		exit sub
	end if
	
	
	'Instantiate the settings object and load the settings based on the 
	'current user company.  
	set oSettings = new Settings
	oSettings.ConnectionSTring = application("sDataSourceName")
	oSettings.VocalErrors = application("bVocalErrors")
	
	if oSettings.LoadSettingsByCompanyId(session("UserCompanyId")) = 0 then
		exit sub
	end if 
	
	
	'Generate the email body
	if p_iEmailId = 1 then
		sBody = oSettings.Email30DayText
	elseif p_iEmailId = 2 then
		sBody = oSettings.Email60DayText
	elseif p_iEmailId = 3 then
		sBody = oSettings.Email90DayText
	else
		exit sub
	end if
	
	
	'Set the Jmail values with the information collected
	oJmail.Body = sBody
    if application("TESTING") then
        oJmail.AddRecipient application("TestSendToEmailAddress")
    else
	    oJmail.AddRecipient oAssociate.Email
    end if
	oJmail.From = application("RenewalEmail")
	oJmail.FromName = "ComplianceKeeper"
	oJmail.Subject = "ComplianceKeeper License Expiration Alert"
	

	'Mail it
	oJmail.Send(application("sMailServerAddress"))
	oJmail.Close	
	
	'If true, no errors occurred while sending
	if oJMail.ErrorCode <> 0 then
		response.redirect("/error.asp?message=The Alert Email was not sent successfully.")
	end if

end sub



sub SendCustomEmail(p_iUserId, p_sEmailText, p_sSubject)

	dim oJmail
	dim oAssociate
	dim oSettings
	dim sSuccessURL
	dim sBody
	
	'Create an instance of the jmail object and set its initial values
	set oJmail = server.CreateObject("Jmail.Message")
	oJmail.ContentType = "text/html"
	oJmail.Silent = false 'true
	oJmail.Logging = true
	
	
	'Instantiate the associate object and load the associate based on the
	'passed UserID
	set oAssociate = new Associate
	oAssociate.ConnectionString = application("sDataSourceName")
	oAssociate.TpConnectionString = application("sTpDataSourceName")
	oAssociate.VocalErrors = application("bVocalErrors")
	
	if oAssociate.LoadAssociateById(p_iUserId) = 0 then
		exit sub
	end if
	
	
	'Instantiate the settings object and load the settings based on the 
	'current user company.  
'	set oSettings = new Settings
'	oSettings.ConnectionSTring = application("sDataSourceName")
'	oSettings.VocalErrors = application("bVocalErrors")
'	
'	if oSettings.LoadSettingsByCompanyId(session("UserCompanyId")) = 0 then
'		exit sub
'	end if 
	
	
	'Set the Jmail values with the information collected
	oJmail.Body = p_sEmailText
	if application("TESTING") then
        oJmail.AddRecipient application("TestSendToEmailAddress")
    else
        oJmail.AddRecipient oAssociate.Email
	end if
    oJmail.From = application("RenewalEmail")
	oJmail.FromName = "ComplianceKeeper"
	oJmail.Subject = p_sSubject
	

	'Mail it
	oJmail.Send(application("sMailServerAddress"))


	dim oLog
	set oLog = new EmailLog
	oLog.ConnectionString = application("sDataSourceName")
	oLog.VocalErrors = application("bVocalErrors")
	oLog.SenderId = session("User_Id")
	oLog.CompanyId = session("UserCompanyId")
	oLog.Subject = ScrubForSql(p_sSubject)
	oLog.BodyText = ScrubForSql(p_sEmailText)
	oLog.RecipientList = oAssociate.FullName & " (" & oAssociate.Email & ")"
	oLog.SendDate = now()
	oLog.SendLog = oJmail.Log
	oLog.SaveEmailLog



	oJmail.Close
	
	'If true, no errors occurred while sending
	if oJMail.ErrorCode <> 0 then
		response.redirect("/error.asp?message=The Alert Email was not sent successfully.  " & oJMail.ErrorCode & ": " & oJMail.ErrorMessage)
	end if

end sub



sub SendCustomCCEmail(p_iUserId, p_sEmailText, p_sSubject, p_aCcList)

	dim oJmail
	dim oAssociate
	dim oSettings
	dim sSuccessURL
	dim sBody
	dim iCount
	dim sCcList
	
	'Create an instance of the jmail object and set its initial values
	set oJmail = server.CreateObject("Jmail.Message")
	oJmail.ContentType = "text/html"
	oJmail.Silent = false 'true
	oJmail.Logging = true
	
	
	'Instantiate the associate object and load the associate based on the
	'passed UserID
	set oAssociate = new Associate
	oAssociate.ConnectionString = application("sDataSourceName")
	oAssociate.TpConnectionString = application("sTpDataSourceName")
	oAssociate.VocalErrors = application("bVocalErrors")
	
	if oAssociate.LoadAssociateById(p_iUserId) = 0 then
		exit sub
	end if
	
	
	
	
	'Set the Jmail values with the information collected
	oJmail.Body = p_sEmailText
    if application("TESTING") then
        oJmail.AddRecipient application("TestSendToEmailAddress")
    else
    	oJmail.AddRecipient oAssociate.Email
	    'Add the CC recipients in the passed array.
	    if isarray(p_aCcList) then
		    for iCount = 0 to ubound(p_aCcList)
	
			    'Response.Write("<p>" & iCount & ": " & p_aCcList(iCount) & "</p>")
		
			    oJmail.AddRecipientCc(trim(p_aCcList(iCount)))
			    sCcList = sCcList & trim(p_aCcList(iCount)) & ", "
		    next
	    end if
	end if
    oJmail.From = session("UserEmail") 'application("RenewalEmail")
	oJmail.FromName = session("User_FullName") '"ComplianceKeeper"
	oJmail.Subject = p_sSubject
	
	'Response.End
	if len(sCcList) > 0 then
		sCcList = left(sCcList, len(sCcList) - 2)
	end if

	'Mail it
	oJmail.Send(application("sMailServerAddress"))


	dim oLog
	set oLog = new EmailLog
	oLog.ConnectionString = application("sDataSourceName")
	oLog.VocalErrors = application("bVocalErrors")
	oLog.SenderId = session("User_Id")
	oLog.CompanyId = session("UserCompanyId")
	oLog.Subject = ScrubForSql(p_sSubject)
	oLog.BodyText = ScrubForSql(p_sEmailText)
	oLog.RecipientList = oAssociate.FullName & " (" & oAssociate.Email & ")"
	if sCcList <> "" then
		oLog.RecipientList = oLog.RecipientList & ", " & sCcList
	end if
	oLog.SendDate = now()
	oLog.SendLog = oJmail.Log
	oLog.SaveEmailLog



	oJmail.Close
	
	'If true, no errors occurred while sending
	if oJMail.ErrorCode <> 0 then
		response.redirect("/error.asp?message=The Alert Email was not sent successfully.  " & oJMail.ErrorCode & ": " & oJMail.ErrorMessage)
	end if

end sub



'function LogEmail(p_iUserId, p_sSubject, p_sEmailText, p_sRecipientList, _
'	p_sAttachmentName, p_dDate, p_sEmailLog)
'
'	dim oConn 'DB Connection
'	dim sSql 'SQL statement
'	dim lRecordsAffected 'Number of records affected by an execute
'		
'	set oConn = Server.CreateObject("ADODB.Connection")
'
'	oConn.ConnectionString = application("sDataSourceName")
'	oConn.Open
'	
'	sSql = "INSERT INTO EmailLog (" & _
'		"SenderId, " & _
'		"RecipientList, " & _
'		"Subject, " & _
'		"Body, " & _
'		"Attachment, " & _
'		"SendDate, " & _
'		"SendLog " & _
'		") VALUES (" & _
'		"'" & p_iUserId & "', " & _
'		"'" & p_sRecipientList & "', " & _
'		"'" & p_sSubject & "', " & _
'		"'" & p_sEmailText & "', " & _
'		"'" & p_sAttachmentName & "', " & _
'		"'" & p_dDate & "', " & _
'		"'" & p_sEmailLog & "' " & _
'		")"
'	oConn.Execute sSql, lRecordsAffected
'	
'	if lRecordsAffected <> 1 then
'		LogEmail = 0
'	else
'		LogEmail = 1
'	end if
'	
'	oConn.Close 
'	set oConn = nothing
'
'end function


%>

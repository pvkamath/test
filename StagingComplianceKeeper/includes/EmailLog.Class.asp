<%
'==============================================================================
' Class: EmailLog
' Controls the creation, modification, and removal of log entries for sent
'		email messages.
' Create Date: 10/10/05
' Author(s): Mark Silvia
' ----------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- EmailId
'	- SenderId
'	- CompanyId
'	- RecipientList
'	- Subject
'	- BodyText
'	- Attachment
'	- SendDate
'	- SendLog
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- CheckDateParam
'	- QueryDb
'	- ReportError
' Methods
'	- LoadEmailLogById
'	- SaveEmailLog
'	- SearchEmailLogs
'==============================================================================

class EmailLog
	
	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'License properties
	dim g_iEmailId
	dim g_iSenderId
	dim g_iCompanyId
	dim g_sRecipientList
	dim g_sSubject
	dim g_sBodyText
	dim g_sAttachment
	dim g_dSendDate
	dim g_sSendLog
	dim g_dSearchDateFrom
	dim g_dSearchDateTo

	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Email
	public property get EmailId()
		EmailId = g_iEmailId
	end property
	
	
	'Unique UserId of the email sender.
	public property get SenderId()
		SenderId = g_iSenderId
	end property
	public property let SenderId(p_iSenderId)
		if isnumeric(p_iSenderId) then 
			g_iSenderId = cint(p_iSenderId)
		elseif p_iSenderId = "" then
			g_iSenderId = ""
		else
			ReportError("Invalid SenderId value.  Integer required.")
		end if
	end property
	
	
	'Unique Company ID which owns this email
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = cint(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	

	'Text string containing the list of recipient names and their email
	'addresses
	public property get RecipientList()
		RecipientList = g_sRecipientList
	end property
	public property let RecipientList(p_sRecipientList)
		g_sRecipientList = p_sRecipientList
	end property
	
	
	'Subject line of the email
	public property get Subject()
		Subject = g_sSubject
	end property
	public property let Subject(p_sSubject)
		g_sSubject = p_sSubject
	end property
	
	
	'Body text of the email
	public property get BodyText()
		BodyText = g_sBodyText
	end property
	public property let BodyText(p_sBodyText)
		g_sBodyText = p_sBodyText
	end property
	
	
	'Filename of the email attachment
	public property get Attachment()
		Attachment = g_sAttachment
	end property
	public property let Attachment(p_sAttachment)
		g_sAttachment = p_sAttachment
	end property
	
	
	'Date and time on which the email was sent.
	public property get SendDate()
		SendDate = g_dSendDate
	end property
	public property let SendDate(p_dSendDate)
		if isdate(p_dSendDate) then
			g_dSendDate = cdate(p_dSendDate)
		elseif p_dSendDate = "" then
			g_dSendDate = ""
		else
			ReportError("Invalid SendDate.  Date required.")
		end if
	end property
	
	
	'Log recorded from JMail.
	public property get SendLog()
		SendLog = g_sSendLog
	end property
	public property let SendLog(p_sSendLog)
		g_sSendLog = p_sSendLog
	end property


	'Allows searches based on the send date from a certain date.
	public property get SearchDateFrom()
		SearchDateFrom = g_dSearchDateFrom
	end property
	public property let SearchDateFrom(p_dSearchDateFrom)
		if isdate(p_dSearchDateFrom) then
			g_dSearchDateFrom = cdate(p_dSearchDateFrom)
		elseif p_dSearchDateFrom = "" then
			g_dSearchDateFrom = ""
		else
			ReportError("Invalid SearchDateFrom.  Date required.")
		end if
	end property
	

	'Allows searches based on the send date to a certain date.
	public property get SearchDateTo()
		SearchDateTo = g_dSearchDateTo
	end property
	public property let SearchDateTo(p_dSearchDateTo)
		if isdate(p_dSearchDateTo) then
			g_dSearchDateTo = cdate(p_dSearchDateTo)
		elseif p_dSearchDateTo = "" then
			g_dSearchDateTo = ""
		else
			ReportError("Invalid SearchDateTo.  Date required.")
		end if
	end property

	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(sSql & "<p>")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	

	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadEmailLogById
	' Description: Load an email log based on the passed email ID
	' Preconditions: ConnectionString must be set
	' Inputs: p_iEmailId - ID of the Email to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadEmailLogById(p_iEmailId)
		
		LoadEmailLogById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iEmailId, 0, "Email ID") then
			exit function
		end if 


		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM EmailLog " & _
			"WHERE EmailId = '" & p_iEmailId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the License properties
			g_iEmailId = oRs("EmailId")
			g_iSenderId = oRs("SenderId")
			g_iCompanyId = oRs("CompanyId")
			g_sRecipientList = oRs("RecipientList")
			g_sSubject = oRs("Subject")
			g_sBodyText = oRs("BodyText")
			g_sAttachment = oRs("Attachment")
			g_dSendDate = oRs("SendDate")
			g_sSendLog = oRs("SendLog")
			
			LoadEmailLogById = g_iEmailId
			
		else
			
			ReportError("Unable to load the passed Email ID.")
			exit function
		
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveEmailLog
	' Description: Saves an Email Log object's properties to the database.
	' Preconditions: ConnectionString, SenderId, RecipientList, SendDate
	' Returns: If successful, returns the saved/new ID, othewise 0.
	' -------------------------------------------------------------------------
	public function SaveEmailLog()
	
		SaveEmailLog = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iSenderId, 0, "Sender ID") or _
			not CheckStrParam(g_sRecipientList, 0, "Recipient List") or _
			not CheckDateParam(g_dSendDate, 0, "Send Date") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a EmailId property assigned already, we're working on a
		'loaded Email, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iEmailId <> "" then
			
			bIsNew = false
			sSql = "UPDATE EmailLog SET " & _
				"SenderId = '" & g_iSenderId & "', " & _
				"CompanyId = '" & g_iCompanyId & "', " & _
				"RecipientList = '" & g_sRecipientList & "', " & _
				"Subject = '" & g_sSubject & "', " & _
				"BodyText = '" & g_sBodyText & "', " & _
				"Attachment = '" & g_sAttachment & "', " & _
				"SendDate = '" & g_dSendDate & "', " & _
				"SendLog = '" & g_sSendLog & "', " & _
				"WHERE EmailId = '" & g_iEmailId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
			
				SaveEmailLog = g_iEmailId			
			
			else
			
				ReportError("Unable to save existing email log.")
				exit function
			
			end if 
				
		else 
			
			bIsNew = true
			sSql = "INSERT INTO EmailLog (" & _
				"SenderId, " & _
				"CompanyId, " & _
				"RecipientList, " & _
				"Subject, " & _
				"BodyText, " & _
				"Attachment, " & _
				"SendDate, " & _
				"SendLog " & _
				") VALUES (" & _
				"'" & g_iSenderId & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & g_sRecipientList & "', " & _
				"'" & g_sSubject & "', " & _
				"'" & g_sBodyText & "', " & _
				"'" & g_sAttachment & "', " & _
				"'" & g_dSendDate & "', " & _
				"'" & g_sSendLog & "' " & _
				")"
			'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected 
			
			'Retrieve the new License
			sSql = "SELECT EmailId FROM EmailLog " & _
				"WHERE SenderId = '" & g_iSenderId & "' " & _
				"AND SendDate = '" & g_dSendDate & "' " & _
				"ORDER BY EmailId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				
				g_iEmailId = oRs("EmailId")
				SaveEmailLog = g_iEmailId
			
			else
			
				'The record could not be retrieved.
				ReportError("Failed to save new email log.")
				exit function
			
			end if 
			
		end if 
		
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function

	
	' -------------------------------------------------------------------------
	' Name: ReleaseEmailLog
	' Desc: Clears set/loaded object properties
	' Preconditions: ConnectionString
	' -------------------------------------------------------------------------
	public sub ReleaseEmailLog()
		
		g_iEmailId = ""
		g_iSenderId = ""
		g_iCompanyId = ""
		g_sRecipientList = ""
		g_sSubject = ""
		g_sBodyText = ""
		g_sAttachment = ""
		g_dSendDate = ""
		g_sSendLog = ""
		
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: SearchEmailLogs
	' Desc: Retrieves a collection of EmailIds that match the properties of
	'		the current object. 
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchEmailLogs()
		
		set SearchEmailLogs = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
	
	
		bFirstClause = true
		
		sSql = "SELECT DISTINCT EL.EmailId, " & _
			"EL.SenderId, " & _
			"EL.SendDate " & _
			"FROM EmailLog AS EL "
	
		
		'If SenderId search
		if g_iSenderId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.SenderId = '" & g_iSenderId & "'"
		end if


		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.CompanyId = '" & g_iCompanyId & "'"
		end if
		
		
		'If RecipientList search
		if g_sRecipientList <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.RecipientList LIKE '%" & g_sRecipientList & "%'"
		end if
		
		
		'If Subject search
		if g_sSubject <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.Subject LIKE '%" & g_sSubject & "%'"
		end if
		
		
		'If SearchDateFrom search
		if g_dSearchDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.SendDate >= '" & g_dSearchDateFrom & "'"
		end if


		'If SearchDateTo search
		if g_dSearchDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "EL.SendDate <= '" & g_dSearchDateTo & "'"
		end if		
		
		sSql = sSql & " ORDER BY EL.SendDate DESC"
		
		'Response.Write(sSql & "<p>")
		
		set SearchEmailLogs = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupUserId
	' Description: Returns the full name of a user based on User ID
	' Preconditions: ConnectionString
	' Inputs: p_iUserId - User ID to look up
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function LookupUserId(p_iUserId)
	
		LookupUserId = ""
		
		'Verify preconditions
		if not CheckConnectionstring then
			exit function
		end if
		
		
		if p_iUserId = 0 then
			LookupUserId = "ComplianceKeeper.com"
			exit function
		end if
		
		
		dim sSql
		dim oRs
		
		sSql = "SELECT FirstName, MiddleName, LastName FROM Users WHERE " & _
			"UserId = '" & p_iUserId & "'" 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
				
			if oRs("MiddleName") <> "" and oRs("MiddleName") <> " " then
				LookupUserId = oRs("FirstName") & " " & oRs("MiddleName") & _
					". " & oRs("LastName")
			else
				LookupUserId = oRs("FirstName") & " " & oRs("LastName")
			end if
		
		end if 
		
		set oRs = nothing
	
	end function
	
	
end class
%>
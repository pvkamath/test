<%
'==============================================================================
' Class: Employee
' Purpose: Class used for retrieving and storing information for employees.
' -------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
'
' Employee Properties:
'	- Active
'	- Address1
'	- Address2
'	- Address3
'	- Address4
'	- BeginningEducationDate
'	- BranchId
'	- CellPhone
'	- Comments
'	- CompanyId
'	- CompanyL2Id
'	- CompanyL3Id
'	- Country
'	- DateOfBirth
'	- Department
'	- DriversLicenseNo
'	- DriversLicenseStateId
'	- Email
'	- EmployeeId
'	- Fax
'	- FingerprintDeadlineDate
'	- FirstName
'	- GenderId
'	- HireDate
'	- HomeEmail
'	- HomePhone
'	- Inactive
'	- LastName
'	- LicensePayment
'	- MiddleName
'	- NMLSNumber
'	- OfficerCustField1
'	- OfficerCustField2
'	- OfficerCustField3
'	- OfficerCustField4
'	- OfficerCustField5
'	- OfficerCustField6
'	- OfficerCustField7
'	- OfficerCustField8
'	- OfficerCustField9
'	- OfficerCustField10
'	- OfficerCustField11
'	- OfficerCustField12
'	- OfficerCustField13
'	- OfficerCustField14
'	- OfficerCustField15
'   - Verified
'	- Phone
'	- Phone2
'	- Phone3
'	- PhoneExt
'	- Phone2Ext
'	- Phone3Ext
'	- RaceId
'	- SearchCourseCompletionDateFrom
'	- SearchCourseCompletionDateTo
'	- SearchCourseCreditHours
'	- SearchCourseExpDateFrom
'	- SearchCourseExpDateTo
'	- SearchCourseName
'	- SearchCourseProviderId
'   - SearchCourseAccreditationTypeID
'	- SearchHireDateFrom
'	- SearchHireDateTo
'	- SearchLastName
'	- SearchLicenseExpDateFrom
'	- SearchLicenseExpDateTo
'	- SearchLicenseNumber
'	- SearchLicensePayment
'	- SearchLicenseStatusId
'	- SearchStateID
'	- SearchStateIdList
'	- SSN
'	- Title
'	- TerminationDate
'	- UserId
'	- UserStatus
'
' Methods:
' 	- CheckConnectionString()
'	- CheckDateParam(p_dDate, p_bAllowNull, p_sName)
'	- CheckIntParam(p_iInt, p_bAllowNull, p_sName)
'	- CheckStrParam(p_sStr, p_bAllowNull, p_sName)
'	- Class_Initialize()
'	- Class_Terminate()
'	- DeleteId(p_iUserId)
'	- QueryDb(sSQL)
'	- QueryTpDb(sSQL)
'	- ReportError(p_sError)
'	
'	- AddCourse(p_iCourseId, p_dExpDate, p_dCompletionDate)
'	- ClearCourseTriggers(p_iAssocId)
'	- CourseExistsInProfile(p_iCourseID)
'	- DeleteEmployee()
'	- GetCourse(p_iAssocId)
'	- GetCourses()
'	- GetOfficerCustField(p_iNo)
'	- GetTpCourseProgress(p_iCourseId, p_iTpUserId)
'	- GetWorkAddress()
'	- LoadEmployeeByEmail(p_sEmail, p_iCompanyId)
'	- LoadEmployeeById(p_iUserId)
'	- LoadEmployeeByNMLS(p_sEmail, p_iCompanyId)
'	- LoadEmployeeByLastNameSsn(p_sSsn, p_sLastName, p_iCompanyId)
'	- LookupBranchName(p_iBranchId)
'	- LookupBranchNumber(p_iBranchId)
'	- LookupCompanyL2Name(p_iCompanyL2Id)
'	- LookupCompanyL3Name(p_iCompanyL3Id)
'	- LookupGenderId()
'	- LookupRaceId()
'	- LookupTpUserId(p_sUserName)
'	- RemoveCourse(p_iAssocId)
'	- SaveEmployee()
'	- SearchEmployees()
'	- SearchEmployeesCoursesLicenses()
'	- SearchCourses(p_sCourseName, p_iProviderId, p_dCompletionDateFrom, p_dCompletionDateTo, p_dExpFrom, p_dExpTo, p_bCompleted, p_iAccreditationTypeID)
'	- UpdateCourse(p_iAssocId, p_iCourseId, p_dExpDate, p_dCompletionDate)
'	- UpdateTpCourse(p_iTpUserCourseId, p_dCkRenewalDate)
'	- VerifyUniqueEmail(p_sEmail)
'	- VerifyUniqueNMLS(p_sNMLSNumber)
'
'==============================================================================

class Employee

	' GLOBAL VARIABLE DECLARATIONS ============================================
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors

	dim g_bActive
	dim g_oAddress1
	dim g_oAddress2
	dim g_oAddress3
	dim g_oAddress4		
	dim g_iAppDeadline
	dim g_dBeginningEducationDate
	dim g_iBranchid
	dim g_sCellPhone
	dim g_iCompanyId
	dim g_iCompanyL2Id
	dim g_iCompanyL3Id
	dim g_iCountryId
	dim g_bCourseCompleted
	dim g_dCourseCompletionDateFrom
	dim g_dCourseCompletionDateTo
	dim g_rCourseCreditHours
	dim g_dCourseExpDateFrom
	dim g_dCourseExpDateTo
	dim g_sCourseName
	dim g_iCourseProviderId
    dim g_iCourseAccreditationTypeID
	dim g_dDateOfBirth
	dim g_sDepartment
	dim g_sDriversLicenseNo
	dim g_iDriversLicenseStateId
	dim g_sEmail
	dim g_sEmployeeId
	dim g_sFax
	dim g_dFingerprintDeadlineDate
	dim g_sFirstName
	dim g_iGenderId
	dim g_dHireDate
	dim g_dHireDateFrom
	dim g_dHireDateTo
	dim g_sHomeEmail	
	dim g_sHomePhone
	dim g_bInactive
	dim g_sLastName
	dim g_dLicenseExpDateFrom
	dim g_dLicenseExpDateTo
	dim g_sLicenseNumber
	dim g_sLicensePayment
	dim g_iLicenseStatusId
	dim g_sLicenseStatusIdList
	dim g_sManager
	dim g_sMiddleName
	dim g_sNMLSNumber
	dim g_sOfficerCustField1
	dim g_sOfficerCustField2
	dim g_sOfficerCustField3
	dim g_sOfficerCustField4
	dim g_sOfficerCustField5
	dim g_sOfficerCustField6
	dim g_sOfficerCustField7
	dim g_sOfficerCustField8
	dim g_sOfficerCustField9
	dim g_sOfficerCustField10
	dim g_sOfficerCustField11
	dim g_sOfficerCustField12
	dim g_sOfficerCustField13
	dim g_sOfficerCustField14
	dim g_sOfficerCustField15
    dim g_bVerified


	dim g_bOutOfStateOrig
	dim g_sPhone
	dim g_sPhone2
	dim g_sPhone3
	dim g_sPhoneExt
	dim g_sPhone2Ext
	dim g_sPhone3Ext
	dim g_iRaceId
	dim g_sSearchBranchIdList
	dim g_sSearchCompanyL2IdList
	dim g_sSearchCompanyL3IdList
	dim g_sSearchCourseStateIdList
	dim g_sSearchLastNameStart
	dim g_iSearchStateId
	dim g_sSearchLicStateIdList
	dim g_sSearchStateIdList
	dim g_iSearchStateIdType
	dim g_sSsn
	dim g_dTerminationDate
	dim g_sTitle
	dim g_iTpLinkCompanyId
	dim g_iUserId
	dim g_iUserStatus
    dim g_iLicensed

	' PUBLIC PROPERTIES =======================================================

	'Misc Properties
	
	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property


	'Stores/retrieves the trainingpro database connection string.
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	'Employee Properties

    'Value to check if licensed
	public property let licensed(p_iLicensed)
		g_iLicensed = p_iLicensed
	end property
	public property get licensed()
		licensed = g_iLicensed
	end property

	
	'Active boolean.  Describes whether this entry is to be used in the site.
	'public property get Active()
	'	Active = g_bActive
	'end property
	'public property let Active(p_bActive)
	'	if isnumeric(p_bActive) then
	'		g_bActive = cbool(p_bActive)
	'	else
	'		ReportError("Invalid Active value.  Boolean required.")
	'	end if 
	'end property
	

 

	'Address 1
	public property get Address1()
		set Address1 = g_oAddress1
	end property
	public property let Address1(p_oAddress1)
		set g_oAddress1 = p_oAddress1
	end property
	
	'Address 2
	public property get Address2()
		set Address2 = g_oAddress2
	end property
	public property let Address2(p_oAddress2)
		set g_oAddress2 = p_oAddress2
	end property
	
	'Address 3
	public property get Address3()
		set Address3 = g_oAddress3
	end property
	public property let Address3(p_oAddress3)
		set g_oAddress3 = p_oAddress3
	end property
	
	'Address 4
	public property get Address4()
		set Address4 = g_oAddress4
	end property
	public property let Address4(p_oAddress4)
		set g_oAddress4 = p_oAddress4
	end property	
	
	'Date the employee's education history began
	public property get BeginningEducationDate()
		BeginningEducationDate = g_dBeginningEducationDate
	end property
	public property let BeginningEducationDate(p_dBeginningEducationDate)
		if isdate(p_dBeginningEducationDate) then
			g_dBeginningEducationDate = cdate(p_dBeginningEducationDate)
		elseif p_dBeginningEducationDate = "" then
			g_dBeginningEducationDate = ""
		else
			ReportError("Invalid BeginningEducationDate value.  Date required.")
		end if
	end property
	
	'BranchId
	public property get BranchId()
		BranchId = g_iBranchId
	end property
	public property let BranchId(p_iBranchId)
		if isnumeric(p_iBranchId) then
			g_iBranchId = clng(p_iBranchId)
			g_iCompanyL2Id = ""
			g_iCompanyL3Id = ""
		elseif p_iBranchId = "" then
			g_iBranchId = ""
		else
			ReportError("Invalid BranchId value.  Integer required.")
		end if 		
	end property
	
	'Cell Phone Number
	public property get CellPhone()
		CellPhone = g_sCellPhone
	end property
	public property let CellPhone(p_sCellPhone)
		g_sCellPhone = left(trim(p_sCellPhone), 30)
	end property
	
	'City
	public property get City()
		City = g_sCity
	end property
	public property let City(p_sCity)
		g_sCity = left(trim(p_sCity), 50)
	end property
	
	'CompanyId
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = cint(p_iCompanyId)
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if 
	end property
	
	'CompanyL2Id - This, CompanyL3Id, and BranchId are all used to determine
	'which tier owns this officer.  CompanyId is always filled to distinguish 
	'the company the officer belongs to, but if the officer is assigned to one
	'of the lower tiers, one of the three properties will be assigned the value
	'and the others will be assigned NULL.
	public property get CompanyL2Id()
		CompanyL2Id = g_iCompanyL2Id
	end property
	public property let CompanyL2Id(p_iCompanyL2Id)
		if isnumeric(p_iCompanyL2Id) then
			g_iCompanyL2Id = clng(p_iCompanyL2Id)
			g_iCompanyL3Id = ""
			g_iBranchId = ""
		elseif p_iCompanyL2Id = "" then
			g_iCompanyL2Id = ""
		else
			ReportError("Invalid CompanyL2Id value.  Integer required.")
		end if
	end property

	'CompanyL3Id
	public property get CompanyL3Id()
		CompanyL3Id = g_iCompanyL3Id
	end property
	public property let CompanyL3Id(p_iCompanyL3Id)
		if isnumeric(p_iCompanyL3Id) then
			g_iCompanyL3Id = clng(p_iCompanyL3Id)
			g_iCompanyL2Id = ""
			g_iBranchId = ""
		elseif p_iCompanyL3Id = "" then
			g_iCompanyL3Id = ""
		else
			ReportError("Invalid CompanyL3Id value.  Integer required.")
		end if
	end property
	
	'Country ID Assigned to this address
	public property get CountryId
		CountryId = g_iCountryId
	end property
	public property let CountryId(p_iCountryId)
		if isnumeric(p_iCountryId) then
			g_iCountryId = p_iCountryId
		elseif p_iCountryId = "" then
			g_iCountryId = ""
		else
			ReportError("Invalid CountryId value.  Integer required.")
		end if 
	end property
	
	'Date of birth
	public property get DateOfBirth()
		DateOfBirth = g_dDateOfBirth
	end property
	public property let DateOfBirth(p_dDateOfBirth)
		if isdate(p_dDateOfBirth) then
			g_dDateOfBirth = cdate(p_dDateOfBirth)
		elseif p_dDateOfBirth = "" then
			g_dDateOfBirth = ""
		else
			ReportError("Invalid DateOfBirth value.  Date required.")
		end if
	end property
	
	'User job department
	public property get Department()
		Department = g_sDepartment
	end property
	public property let Department(p_sDepartment)
		g_sDepartment = left(p_sDepartment, 50)
	end property

	'Drivers license number
	public property get DriversLicenseNo()
		DriversLicenseNo = g_sDriversLicenseNo
	end property
	public property let DriversLicenseNo(p_sDriversLicenseNo)
		g_sDriversLicenseNo = left(trim(p_sDriversLicenseNo), 25)
	end property
	
	'Drivers license state
	public property get DriversLicenseStateId()
		DriversLicenseStateId = g_iDriversLicenseStateId
	end property
	public property let DriversLicenseStateId(p_iDriversLicenseStateId)
		if isnumeric(p_iDriversLicenseStateId) then
			g_iDriversLicenseStateId = cint(p_iDriversLicenseStateId)
		elseif p_iDriversLicenseStateId = "" then
			g_iDriversLicenseStateId = ""
		else
			ReportError("Invalid DriversLicenseStateId.  Integer required.")
		end if 
	end property
	
	'User email address
	public property get Email()
		Email = g_sEmail
	end property
	public property let Email(p_sEmail)
		g_sEmail = left(trim(p_sEmail), 50)
	end property
	
	'Employee ID field.  Not a keyfield or anything.
	public property get EmployeeId()
		EmployeeId = g_sEmployeeId
	end property
	public property let EmployeeId(p_sEmployeeId)
		g_sEmployeeId = left(p_sEmployeeId, 25)
	end property
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = left(trim(p_sFax), 30)
	end property
	
	'Fingerprint Deadline Date	
	public property get FingerprintDeadlineDate()
		FingerprintDeadlineDate = g_dFingerprintDeadlineDate
	end property
	public property let FingerprintDeadlineDate(p_dFingerprintDeadlineDate)
		if isdate(p_dFingerprintDeadlineDate) then
			g_dFingerprintDeadlineDate = cdate(p_dFingerprintDeadlineDate)
		elseif p_dFingerprintDeadlineDate = "" then
			g_dFingerprintDeadlineDate = ""
		else
			ReportError("Invalid Fingerprint Deadline Date value.  Date required.")
		end if
	end property
		
	'User first name
	public property get FirstName()
		FirstName = g_sFirstName
	end property
	public property let FirstName(p_sFirstName)
		g_sFirstName = left(trim(p_sFirstname), 50)
	end property
	
	'Combines first/middle/last name values into one returned value.
	public property get FullName()
		FullName = g_sFirstName & " "
		if trim(g_sMiddleName) <> "" then 
			FullName = FullName & g_sMiddleName & " "
		end if
		FullName = FullName & g_sLastName
	end property

	'Gender index
	public property get GenderId()
		GenderId = g_iGenderId
	end property
	public property let GenderId(p_iGenderId)
		if isnumeric(p_iGenderId) then
			g_iGenderId = cint(p_iGenderId)
		else
			ReportError("Invalid GenderId value.  Integer required.")
		end if
	end property
	
	'Date the employee was hired by the company
	public property get HireDate()
		HireDate = g_dHireDate
	end property
	public property let HireDate(p_dHireDate)
		if isdate(p_dHireDate) then
			g_dHireDate = cdate(p_dHireDate)
		elseif p_dHireDate = "" then
			g_dHireDate = ""
		else
			ReportError("Invalid HireDate value.  Date required.")
		end if
	end property
	
	'Home email address
	public property get HomeEmail()
		HomeEmail = g_sHomeEmail
	end property
	public property let HomeEmail(p_sHomeEmail)
		g_sHomeEmail = left(trim(p_sHomeEmail), 30)
	end property
	
	'Home Phone number
	public property get HomePhone()
		HomePhone = g_sHomePhone
	end property
	public property let HomePhone(p_sHomePhone)
		g_sHomePhone = left(trim(p_sHomePhone), 30)
	end property
	
	'Is the user flagged as terminated/inactive?
	public property get Inactive()
		Inactive = g_bInactive
	end property
	public property let Inactive(p_bInactive)
		if isnumeric(p_bInactive) then
			g_bInactive = cbool(p_bInactive)
		elseif p_bInactive = "" then
			g_bInactive = ""
		else
			ReportError("Invalid Inactive value.  Boolean required.")
		end if 
	end property
	

       'Verified boolean.  
	public property get Verified()
		Verified = g_bVerified
	end property
	public property let Verified(p_bVerified)
		if isnumeric(p_bVerified) then
			g_bVerified = cbool(p_bVerified)
		elseif g_bVerified = "" then
			g_bVerified = ""
		else
			ReportError("Invalid Verified value.  Boolean required.")
		end if 
	end property




	'User last name
	public property get LastName()
		LastName = g_sLastName
	end property
	public property let LastName(p_sLastName)
		g_sLastName = left(trim(p_sLastName), 50)
	end property
	
	'License Payment
	public property get LicensePayment()
		LicensePayment = g_sLicensePayment
	end property
	
	public property let LicensePayment(p_sLicensePayment)
		g_sLicensePayment = p_sLicensePayment
	end property
	
	'Is the user is flagged as a manager?
	public property get Manager()
		Manager = g_sManager
	end property
	public property let Manager(p_sManager)
		g_sManager = p_sManager
	end property
	
	'User middle name
	public property get MiddleName()
		MiddleName = g_sMiddleName
	end property
	public property let MiddleName(p_sMiddleName)
		g_sMiddleName = left(trim(p_sMiddleName), 50)
	end property

	'NMLS Number
	public property get NMLSNumber()
		NMLSNumber = g_sNMLSNumber
	end property
	public property let NMLSNumber(p_sNMLSNumber)
		g_sNMLSNumber = left(trim(p_sNMLSNumber), 25)
	end property
	
	'Numbered custom field
	public property let OfficerCustField1(p_sOfficerCustField1)
		g_sOfficerCustField1 = left(p_sOfficerCustField1, 150)
	end property
	public property get OfficerCustField1()
		OfficerCustField1 = g_sOfficerCustField1
	end property
	
	'Numbered custom field
	public property let OfficerCustField2(p_sOfficerCustField2)
		g_sOfficerCustField2 = left(p_sOfficerCustField2, 150)
	end property
	public property get OfficerCustField2()
		OfficerCustField2 = g_sOfficerCustField2
	end property
	
	'Numbered custom field
	public property let OfficerCustField3(p_sOfficerCustField3)
		g_sOfficerCustField3 = left(p_sOfficerCustField3, 150)
	end property
	public property get OfficerCustField3()
		OfficerCustField3 = g_sOfficerCustField3
	end property
	
	'Numbered custom field
	public property let OfficerCustField4(p_sOfficerCustField4)
		g_sOfficerCustField4 = left(p_sOfficerCustField4, 150)
	end property
	public property get OfficerCustField4()
		OfficerCustField4 = g_sOfficerCustField4
	end property
	
	'Numbered custom field
	public property let OfficerCustField5(p_sOfficerCustField5)
		g_sOfficerCustField5 = left(p_sOfficerCustField5, 150)
	end property
	public property get OfficerCustField5()
		OfficerCustField5 = g_sOfficerCustField5
	end property
	
	'Numbered custom field
	public property let OfficerCustField6(p_sOfficerCustField6)
		g_sOfficerCustField6 = left(p_sOfficerCustField6, 150)
	end property
	public property get OfficerCustField6()
		OfficerCustField6 = g_sOfficerCustField6
	end property
	
	'Numbered custom field
	public property let OfficerCustField7(p_sOfficerCustField7)
		g_sOfficerCustField7 = left(p_sOfficerCustField7, 150)
	end property
	public property get OfficerCustField7()
		OfficerCustField7 = g_sOfficerCustField7
	end property
	
	'Numbered custom field
	public property let OfficerCustField8(p_sOfficerCustField8)
		g_sOfficerCustField8 = left(p_sOfficerCustField8, 150)
	end property
	public property get OfficerCustField8()
		OfficerCustField8 = g_sOfficerCustField8
	end property
	
	'Numbered custom field
	public property let OfficerCustField9(p_sOfficerCustField9)
		g_sOfficerCustField9 = left(p_sOfficerCustField9, 150)
	end property
	public property get OfficerCustField9()
		OfficerCustField9 = g_sOfficerCustField9
	end property
	
	'Numbered custom field
	public property let OfficerCustField10(p_sOfficerCustField10)
		g_sOfficerCustField10 = left(p_sOfficerCustField10, 150)
	end property
	public property get OfficerCustField10()
		OfficerCustField10 = g_sOfficerCustField10
	end property
	
	'Numbered custom field
	public property let OfficerCustField11(p_sOfficerCustField11)
		g_sOfficerCustField11 = left(p_sOfficerCustField11, 150)
	end property
	public property get OfficerCustField11()
		OfficerCustField11 = g_sOfficerCustField11
	end property
	
	'Numbered custom field
	public property let OfficerCustField12(p_sOfficerCustField12)
		g_sOfficerCustField12 = left(p_sOfficerCustField12, 150)
	end property
	public property get OfficerCustField12()
		OfficerCustField12 = g_sOfficerCustField12
	end property
	
	'Numbered custom field
	public property let OfficerCustField13(p_sOfficerCustField13)
		g_sOfficerCustField13 = left(p_sOfficerCustField13, 150)
	end property
	public property get OfficerCustField13()
		OfficerCustField13 = g_sOfficerCustField13
	end property
	
	'Numbered custom field
	public property let OfficerCustField14(p_sOfficerCustField14)
		g_sOfficerCustField14 = left(p_sOfficerCustField14, 150)
	end property
	public property get OfficerCustField14()
		OfficerCustField14 = g_sOfficerCustField14
	end property
	
	'Numbered custom field
	public property let OfficerCustField15(p_sOfficerCustField15)
		g_sOfficerCustField15 = left(p_sOfficerCustField15, 150)
	end property
	public property get OfficerCustField15()
		OfficerCustField15 = g_sOfficerCustField15
	end property
	
	'Is the user flagged as Out of state origin?
	public property get OutOfStateOrig()
		OutOfStateOrig = g_bOutOfStateOrig
	end property
	public property let OutOfStateOrig(p_bOutOfStateOrig)
		if isnumeric(p_bOutOfStateOrig) then
			g_bOutOfStateOrig = cbool(p_bOutOfStateOrig)
		elseif p_bOutOfStateOrig = "" then
			g_bOutOfStateOrig = ""
		else
			ReportError("Invalid OutOfStateOrig value.  Boolean required.")
		end if 
	end property
	
	'OwnerId - returns the ID of whichever Company, L2, L3, or Branch owns the
	'officer record.  Read-only.
	public property get OwnerId()
		if g_iBranchId <> "" then
			OwnerId = g_iBranchId
		elseif g_iCompanyL3Id <> "" then
			OwnerId = g_iCompanyL3Id
		elseif g_iCompanyL2Id <> "" then
			OwnerId = g_iCompanyL2Id
		else
			OwnerId = g_iCompanyId
		end if
	end property
	
	'OwnerTypeId - Returns an integer for which type of hierarchy object owns
	'this officer record.  Read-only.
	'2 - Branch
	'3 - Company
	'4 - CompanyL2
	'5 - CompanyL3
	public property get OwnerTypeId()
		if g_iBranchId <> "" then
			OwnerTypeId = 2
		elseif g_iCompanyL3Id <> "" then
			OwnerTypeId = 5
		elseif g_iCompanyL2Id <> "" then
			OwnerTypeId = 4
		else
			OwnerTypeId = 3
		end if
	end property	
	
	'Phone number
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = left(trim(p_sPhone), 30)
	end property
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = left(trim(p_sPhoneExt), 10)
	end property
	
	'Phone number two
	public property get Phone2()
		Phone2 = g_sPhone2
	end property
	public property let Phone2(p_sPhone2)
		g_sPhone2 = left(trim(p_sPhone2), 30)
	end property
	
	'Phone number two extension
	public property get Phone2Ext()
		Phone2Ext = g_sPhone2Ext
	end property
	public property let Phone2Ext(p_sPhone2Ext)
		g_sPhone2Ext = left(trim(p_sPhone2Ext), 10)
	end property
	
	'Phone number three
	public property get Phone3()
		Phone3 = g_sPhone3
	end property
	public property let Phone3(p_sPhone3)
		g_sPhone3 = left(trim(p_sPhone3), 30)
	end property
	
	'Phone number three extension
	public property get Phone3Ext()
		Phone3Ext = g_sPhone3Ext
	end property
	public property let Phone3Ext(p_sPhone3Ext)
		g_sPhone3Ext = left(trim(p_sPhone3Ext), 10)
	end property
	
	'Race index
	public property get RaceId()
		RaceId = g_iRaceId
	end property
	public property let RaceId(p_iRaceId)
		if isnumeric(p_iRaceId) then
			g_iRaceId = cint(p_iRaceId)
		else
			ReportError("Invalid RaceId value.  Integer required.")
		end if
	end property
	
	'How are we searching for application deadlines?
	public property get SearchAppDeadline()
		SearchAppDeadline = g_iAppDeadline
	end property
	public property let SearchAppDeadline(p_iAppDeadline)
		if isnumeric(p_iAppDeadline) then
			g_iAppDeadline = clng(p_iAppDeadline)
		elseif p_iAppDeadline = "" or isnull(p_iAppDeadline) then
			g_iAppDeadline = ""
		else
			ReportError("Invalid SearchAppDeadline value.  Integer required.")
		end if 
	end property
	
	'List of Branch IDs to search within.  Used to find employees from
	'multiple branches rather than just one.
	public property get SearchBranchIdList()
		SearchBranchIdList = g_sSearchBranchIdList
	end property
	public property let SearchBranchIdList(p_sSearchBranchIdList)
		g_sSearchBranchIdList = p_sSearchBranchIdList
	end property
	
	'List of CompanyL2 IDs to search within.  Used to find employees from
	'multiple CL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property

	'List of CompanyL3 IDs to search within.  Used to find employees from
	'multiple CL3s rather than just one.
	public property get SearchCompanyL3IdList()
		SearchCompanyL3IdList = g_sSearchCompanyL3IdList
	end property
	public property let SearchCompanyL3IdList(p_sSearchCompanyL3IdList)
		g_sSearchCompanyL3IdList = p_sSearchCompanyL3IdList
	end property	
	
	'Search Course Completion status
	public property get SearchCourseCompleted()
		SearchCourseCompleted = g_bCourseCompleted
	end property
	public property let SearchCourseCompleted(p_bCourseCompleted)
		if isnumeric(p_bCourseCompleted) then
			g_bCourseCompleted = cbool(p_bCourseCompleted)
		elseif p_bCourseCompleted = "" then
			g_bCourseCompleted = ""
		else
			ReportError("Invalid SearchCourseCompleted value.  Boolean required.")
		end if 
	end property
	
	'Search Course Completion from
	public property get SearchCourseCompletionDateFrom()
		SearchCourseCompletionDateFrom = g_dCourseCompletionDateFrom
	end property
	public property let SearchCourseCompletionDateFrom(p_dCourseCompletionDateFrom)
		if isdate(p_dCourseCompletionDateFrom) then
			g_dCourseCompletionDateFrom = cdate(p_dCourseCompletionDateFrom)
		elseif isnull(p_dCourseCompletionDateFrom) or p_dCourseCompletionDateFrom = "" then
			g_dCourseCompletionDateFrom = ""
		else
			ReportError("Invalid SearchCourseCompletionDateFrom value.  " & _
				"Date required.")
		end if
	end property

	'Search Course Completion to
	public property get SearchCourseCompletionDateTo()
		SearchCourseCompletionDateTo = g_dCourseCompletionDateTo
	end property
	public property let SearchCourseCompletionDateTo(p_dCourseCompletionDateTo)
		if isdate(p_dCourseCompletionDateTo) then
			g_dCourseCompletionDateTo = cdate(p_dCourseCompletionDateTo)
		elseif isnull(p_dCourseCompletionDateTo) or p_dCourseCompletionDateTo = "" then
			g_dCourseCompletionDateTo = ""
		else
			ReportError("Invalid SearchCourseCompletionDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	'Search Course Credit Hours
	public property get SearchCourseCreditHours()
		SearchCourseCreditHours = g_rCourseCreditHours
	end property
	public property let SearchCourseCreditHours(p_rCourseCreditHours)
		if isnumeric(p_rCourseCreditHours) then
			g_rCourseCreditHours = cdbl(p_rCourseCreditHours)
		elseif p_rCourseCreditHours = "" then
			g_rCourseCreditHours = ""
		else
			ReportError("Invalid SearchCourseCreditHours value.  " & _
				"Numeric value required.")
		end if
	end property
	
	'Search Course Expiration from
	public property get SearchCourseExpDateFrom()
		SearchCourseExpDateFrom = g_dCourseExpDateFrom
	end property
	public property let SearchCourseExpDateFrom(p_dCourseExpDateFrom)
		if isdate(p_dCourseExpDateFrom) then
			g_dCourseExpDateFrom = cdate(p_dCourseExpDateFrom)
		elseif isnull(p_dCourseExpDateFrom) or p_dCourseExpDateFrom = "" then
			g_dCourseExpDateFrom = ""
		else
			ReportError("Invalid SearchCourseExpDateFrom value.  " & _
				"Date required.")
		end if
	end property

	'Search Course Expiration to
	public property get SearchCourseExpDateTo()
		SearchCourseExpDateTo = g_dCourseExpDateTo
	end property
	public property let SearchCourseExpDateTo(p_dCourseExpDateTo)
		if isdate(p_dCourseExpDateTo) then
			g_dCourseExpDateTo = cdate(p_dCourseExpDateTo)
		elseif isnull(p_dCourseExpDateTo) or p_dCourseExpDateTo = "" then
			g_dCourseExpDateTo = ""
		else
			ReportError("Invalid SearchCourseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	'Search CourseName property
	public property get SearchCourseName()
		SearchCourseName = g_sCourseName
	end property
	public property let SearchCourseName(p_sCourseName)
		g_sCourseName = p_sCourseName
	end property
	
	'Search Course Provider ID 
	public property get SearchCourseProviderId()
		SearchCourseProviderId = g_iCourseProviderId
	end property
	public property let SearchCourseProviderId(p_iCourseProviderId)
		if isnumeric(p_iCourseProviderId) then
			g_iCourseProviderId = cint(p_iCourseProviderId)
		elseif isnull(p_iCourseProviderId) or p_iCourseProviderId = "" then
			g_iCourseProviderId = ""
		else
			ReportError("Invalid SearchCourseProviderId value.  " & _
				"Integer required.")
		end if
	end property
	
    	'Search Course Accreditation Type ID 
	public property get SearchCourseAccreditationTypeId()
		SearchCourseAccreditationTypeId = g_iCourseAccreditationTypeId
	end property
	public property let SearchCourseAccreditationTypeId(p_iCourseAccreditationTypeId)
		if isnumeric(p_iCourseAccreditationTypeId) then
			g_iCourseAccreditationTypeId = cint(p_iCourseAccreditationTypeId)
		elseif isnull(p_iCourseAccreditationTypeId) or p_iCourseAccreditationTypeId = "" then
			g_iCourseAccreditationTypeId = ""
		else
			ReportError("Invalid SearchCourseAccreditationTypeId value.  " & _
				"Integer required.")
		end if
	end property

	'Search Hire Date From
	public property get SearchHireDateFrom()
		SearchHireDateFrom = g_dHireDateFrom
	end property
	public property let SearchHireDateFrom(p_dHireDateFrom)
		if isdate(p_dHireDateFrom) then
			g_dHireDateFrom = cdate(p_dHireDateFrom)
		elseif isnull(p_dHireDateFrom) or p_dHireDateFrom = "" then
			g_dHireDateFrom = ""
		else
			ReportError("Invalid SearchHireDateFrom value.  " & _
				"Date required.")
		end if
	end property	

	'Search Hire Date To
	public property get SearchHireDateTo()
		SearchHireDateTo = g_dHireDateTo
	end property
	public property let SearchHireDateTo(p_dHireDateTo)
		if isdate(p_dHireDateTo) then
			g_dHireDateTo = cdate(p_dHireDateTo)
		elseif isnull(p_dHireDateTo) or p_dHireDateTo = "" then
			g_dHireDateTo = ""
		else
			ReportError("Invalid SearchHireDateTo value.  " & _
				"Date required.")
		end if
	end property	
	
	'List of State IDs to search for courses.  Used to find employees with
	'courses from multiple states rather than just one.
	public property get SearchCourseStateIdList()
		SearchCourseStateIdList = g_sSearchCourseStateIdList
	end property
	public property let SearchCourseStateIdList(p_sSearchCourseStateIdList)
		g_sSearchCourseStateIdList = p_sSearchCourseStateIdList
	end property
	
	'Search Last Name Start property - differs from using the LastName property in
	'the search in that this one will search for officers with last names 
	'starting with this value, rather than with last names that contain this
	'value anywhere in the string.
	public property get SearchLastNameStart()
		SearchLastNameStart = g_sSearchLastNameStart
	end property
	public property let SearchLastNameStart(p_sSearchLastNameStart)
		g_sSearchLastNameStart = p_sSearchLastNameStart
	end property
	
	'Search License Expiration from
	public property get SearchLicenseExpDateFrom()
		SearchLicenseExpDateFrom = g_dLicenseExpDateFrom
	end property
	public property let SearchLicenseExpDateFrom(p_dLicenseExpDateFrom)
		if isdate(p_dLicenseExpDateFrom) then
			g_dLicenseExpDateFrom = cdate(p_dLicenseExpDateFrom)
		elseif isnull(p_dLicenseExpDateFrom) or p_dLicenseExpDateFrom = "" then
			g_dLicenseExpDateFrom = ""
		else
			ReportError("Invalid SearchLicenseExpDateFrom value.  " & _
				"Date required.")
				
		end if
	end property
	
	'Search License Expiration to
	public property get SearchLicenseExpDateTo()
		SearchLicenseExpDateTo = g_dLicenseExpDateTo
	end property
	public property let SearchLicenseExpDateTo(p_dLicenseExpDateTo)
		if isdate(p_dLicenseExpDateTo) then
			g_dLicenseExpDateTo = cdate(p_dLicenseExpDateTo)
		elseif isnull(p_dLicenseExpDateTo) or p_dLicenseExpDateTo = "" then
			g_dLicenseExpDateTo = ""
		else
			ReportError("Invalid SearchLicenseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	'Search License Number
	public property get SearchLicenseNumber()
		SearchLicenseNumber = g_sLicenseNumber
	end property
	public property let SearchLicenseNumber(p_sLicenseNumber)
		g_sLicenseNumber = p_sLicenseNumber
	end property
	
	'Search License Payment
	public property get SearchLicensePayment()
		SearchLicensePayment = g_sLicensePayment
	end property
	public property let SearchLicensePayment(p_sLicensePayment)
		g_sLicensePayment = p_sLicensePayment
	end property
	
	'Search License Status 
	public property get SearchLicenseStatusId()
		SearchLicenseStatusId = g_iLicenseStatusId
	end property
	public property let SearchLicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = cint(p_iLicenseStatusId)
		elseif isnull(p_iLicenseStatusId) or p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid SearchLicenseStatusId value.  " & _
				"Integer required.")
		end if
	end property
	
	'List of License Status IDs to search within.  Used to find licenses
	'matching any of several status types.
	public property get SearchLicenseStatusIdList()
		SearchLicenseStatusIdList = g_sLicenseStatusIdList
	end property
	public property let SearchLicenseStatusIdList(p_sLicenseStatusIdList)
		g_sLicenseStatusIdList = p_sLicenseStatusIdList
	end property
	
	'List of State IDs to search for licenses.  Used to find employees with
	'licenses from multiple states rather than just one.
	public property get SearchLicStateIdList()
		SearchLicStateIdList = g_sSearchLicStateIdList
	end property
	public property let SearchLicStateIdList(p_sSearchLicStateIdList)
		g_sSearchLicStateIdList = p_sSearchLicStateIdList
	end property
	
	'SearchStateID 
	public property get SearchStateID()
		SearchStateID = g_iSearchStateID
	end property
	public property let SearchStateID(p_iSearchStateID)
		if isnumeric(p_iSearchStateID) then
			g_iSearchStateID = cint(p_iSearchStateID)
		elseif p_iSearchStateID = "" then
			g_iSearchStateID = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property	
	
	'List of State IDs to search within.  Used to find employees from multiple
	'states rather than just one.
	public property get SearchStateIdList()
		SearchStateIdList = g_sSearchStateIdList
	end property
	public property let SearchStateIdList(p_sSearchStateIdList)
		g_sSearchStateIdList = p_sSearchStateIdList
	end property
	
	'Determines how we use the SearchStateIdList property.
	'1 - Search Licenses only
	'2 - Search Courses only
	public property get SearchStateIdType()
		SearchStateIdType = g_iSearchStateIdType
	end property
	public property let SearchStateIdType(p_iSearchStateIdType)
		if isnumeric(p_iSearchStateIdType) then
			g_iSearchStateIdType = clng(p_iSearchStateIdType)
		else
			ReportError("Invalid SearchStateIdType value.  Integer required.")
		end if
	end property
	
	'User social security number
	public property get Ssn()
		Ssn = g_sSsn
	end property
	public property let Ssn(p_sSsn)
		g_sSsn = left(trim(p_sSsn), 11)
	end property
	
	'Is there a TrainingPro company ID which we should link with this company?
	'This is used to share course data between CMS companies and a users of a 
	'specific TrainingPro company.  This value is pulled from the Companies
	'table when the user is loaded, and is thus read-only.  It can be changed
	'by updating the CMS Company record.
	public property get TpLinkCompanyId()
		TpLinkCompanyId = g_iTpLinkCompanyId
	end property
	
	'Date the employee's term w/ the company ended
	public property get TerminationDate()
		TerminationDate = g_dTerminationDate
	end property
	public property let TerminationDate(p_dTerminationDate)
		if isdate(p_dTerminationDate) then
			g_dTerminationDate = cdate(p_dTerminationDate)
		elseif p_dTerminationDate = "" then
			g_dTerminationDate = ""
		else
			ReportError("Invalid TerminationDate value.  Date required.")
		end if
	end property
	
	'User job title
	public property get Title()
		Title = g_sTitle
	end property
	public property let Title(p_sTitle)
		g_sTitle = left(trim(p_sTitle), 50)
	end property
	
	'Unique User ID
	public property get UserId()
		UserId = g_iUserId
	end property
	public property let UserId(p_iUserId)
		if isnumeric(p_iUserId) then
			g_iUserId = cint(p_iUserId)
		elseif p_iUserId = "" then
			g_iUserId = ""
		else
			ReportError("Invalid UserId value.  Integer required.")
		end if
	end property
	
	'UserStatus property.
	public property get UserStatus()
		UserStatus = g_iUserStatus
	end property
	public property let UserStatus(p_iUserStatus)
		if isnumeric(p_iUserStatus) then
			g_iUserStatus = cint(p_iUserStatus)
		else
			ReportError("Invalid UserStatus value.  Integer required.")
		end if
	end property
	
	'Zipcode
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	public property let Zipcode(p_sZipcode)
		g_sZipcode = left(trim(p_sZipcode), 10)
	end property
	
	'Optional zipcode extension
	public property get ZipcodeExt()
		ZipcodeExt = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		if len(p_sZipcodeExt) >= 4 then
			g_sZipcodeExt = left(p_sZipcodeExt, 4)
		else
			g_sZipcodeExt = ""
		end if
	end property
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif p_dDate <> "" and not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) or p_iInt = "" then
			
			if p_iInt = "" and not cbool(p_bAllowNull) then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'instantiate the Address objects
		set g_oAddress1 = new EmployeeAddress
		set g_oAddress2 = new EmployeeAddress
		set g_oAddress3 = new EmployeeAddress
		set g_oAddress4 = new EmployeeAddress		
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'destroy the Address objects
		set g_oAddress1 = nothing
		set g_oAddress2 = nothing
		set g_oAddress3 = nothing
		set g_oAddress4 = nothing
	
	end sub	
	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Delete the specified Employee entry from the relevant 
	'			   database tables.
	' Preconditions: Connectionstring
	' Inputs: p_iUserId - User ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iUserId)
		
		DeleteId = 0 
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iUserId, false, "User ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		
'		if p_iUserId >= 0 then
'
'			oConn.ConnectionString = g_sTpConnectionString
''			oConn.Open				
'
'			'Mark the user as deleted in the trainingpro database
'			sSql = "UPDATE Users SET " & _
'				   "UserStatus = '3' " & _
'				   "WHERE UserId = '" & p_iUserId & "'" 
'			oConn.Execute sSql, lRecordsAffected
'			
'			'Verify that at least one record was affected
'			if not lRecordsAffected > 0 then
'			
'				ReportError("Failure deleting the user.")
'				exit function
'			
'			end if 
'				
'		elseif p_iUserId < 0 then
		
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			'Mark the user as deleted in the cms database
			sSql = "UPDATE Employees SET " & _
				   "UserStatus = '3' " & _
				   "WHERE UserId = '" & p_iUserId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the user.")
				exit function
			
			end if 
			
'		end if 
		
		
		'We need to remove the Employee's licenses and note that they were
		'deleted via the Employee.
		oConn.Close
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE Licenses SET " & _
			"Deleted = '1', " & _
			"DeletedWithOwner = '1' " & _
			"WHERE Deleted = '0' " & _
			"AND OwnerId = '" & p_iUserId & "' " & _
			"AND OwnerTypeId = '1' "
		oConn.Execute sSql, lRecordsAffected
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function
	
	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		'Response.Write(sSql)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
		
		'Response.Write("<p>" & sSql & "<p>")
		
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function

	'This function should no longer be necessary as this class no longer
	'directly queries the TP database.  The only TP information is the course
	'info, which is taken from Views on the CK DB.
	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	'private function QueryTpDb(sSQL)
	'
	'	set QueryTpDb = Server.CreateObject("ADODB.Recordset")
	'	
	'	'Verify preconditions
	'	if not CheckConnectionString then
	'		exit function
	'	end if
	'	
	'	
	'	Dim Connect	'as connection
	'	Dim objRS 'as recordset
    '
	'	set Connect = CreateObject("ADODB.Connection")
	'	set objRS = CreateObject("ADODB.Recordset")
	'    
	'   Connect.Open g_sTpConnectionString
	'	objRs.cursorlocation = 3
	'	objRs.CursorType = 3
	'   objRS.Open sSQL, connect
	'   objRs.ActiveConnection = nothing
	'
	'	Set QueryTpDb = objRS
	'
	'	set Connect = Nothing	
	'	set objRS = Nothing
	'end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: AddCourse
	' Desc: Adds a course association with the passed information.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: CourseId, ExpDate, CompletionDate
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function AddCourse(p_iCourseId, p_dExpDate, p_dCompletionDate)
	
		AddCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckDateParam(p_dExpDate, true, "Renewal Date") or _
			not CheckDateParam(p_dCompletionDate, true, "Completion Date") _
			then
			exit function
		end if 
		
		'Verify that this is a non-TP course we're being asked to add.
		if p_iCourseId > 0 then
					
		end if 
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "INSERT INTO EmployeesCourses (" & _
			"EmployeeId, " & _
			"CourseId, " & _
			"CourseExpirationDate, " & _
			"CompletionDate, " & _
			"Completed " & _
			") VALUES (" & _
			"'" & g_iUserId & "', " & _
			"'" & p_iCourseId & "', "
		if p_dExpDate <> "" then
			sSql = sSql & "'" & p_dExpDate & "', "
		else
			sSql = sSql & "NULL, "
		end if
		if p_dCompletionDate <> "" then
			sSql = sSql & "'" & p_dCompletionDate & "', " & _
				"'1' "
		else
			sSql = sSql & "NULL, " & _
				"'0' "
		end if
		sSql = sSql & ")"
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to add the course record.")
		
		else
			
			AddCourse = 1
			
		end if
		
		oConn.Close
		set oConn = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: ClearCourseTriggers
	' Desc: Clears the flags that note whether a course association has already
	'	triggered an expiration alert email.
	' Preconditions: ConnectionString, UserId
	' Inputs: AssocId
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function ClearCourseTriggers(p_iAssocId)
	
		ClearCourseTriggers = 0
	
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Association ID") then
			exit function
		end if 
	
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "DELETE FROM EmployeesCoursesTriggers " & _
			"WHERE Id = '" & p_iAssocId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		ClearCourseTriggers = 1
			
	
		oConn.Close
		set oConn = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: CourseExistsInProfile
	' Desc: Determines whether or not the passed course is in the employee's profile
	' Preconditions: ConnectionString, EmployeeId, CompanyID
	' Inputs: CourseID
	' Returns:  integer value representing the ID of the course in the EmployeesCourses table if the course exists in the employee's profile,
	'			0 if the course does not exist in the employee's profile
	' -------------------------------------------------------------------------
	public function CourseExistsInProfile(p_iCourseID)
		dim oRs 'as object
		dim sSQL 'as string
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		'Set Default
		CourseExistsInProfile = 0
	
		sSQL = "SELECT ECs.ID FROM EmployeesCourses AS ECs " & _
			"LEFT JOIN Employees AS Emp ON (Emp.EmployeeId = ECs.EmployeeId) " & _
			"WHERE NOT ECs.Deleted = '1' " & _
			"AND ECs.CourseID = '" & p_iCourseID & "' " & _
			"AND Emp.EmployeeId = '" & g_iEmployeeId & "' " & _
			"AND (Emp.CompanyId = '" & g_iCompanyId & "' "
	
		if g_iTpLinkCompanyId <> "" then
			sSQL = sSQL & "OR Emp.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSQL = sSQL & ") "
		end if

		set oRs = QueryDB(sSQL)
		
		if not oRs.EOF then
			CourseExistsInProfile = oRs(0)
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: DeleteEmployee
	' Description: Sets the employee UserStatus to deleted.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteEmployee()

		DeleteEmployee = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iEmployeeId, false, "Employee ID") then
			exit function
		end if 

		
		dim iResult
		
		iResult = DeleteId(g_iUserId)
		
		if iResult = 0 then
		
			'Zero means failure
			ReportError("Could not delete the specified Employee.")
			DeleteEmployee = 0 
			
		else
		
			'Success
			DeleteEmployee = 1
			
		end if 

	end function
	
	' -------------------------------------------------------------------------
	' Name: GetCourse
	' Desc: Returns a recordset of a course this user has completed.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId
	' Returns: Recordset of Course association information
	' -------------------------------------------------------------------------
	public function GetCourse(p_iAssocId)
	
		set GetCourse = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Course Association ID") then
			exit function
		end if
		
		dim sSql 'SQL Statement
		

		sSql = "SELECT * FROM vAssociatesCoursesCKUserId AS ACs " & _
			"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.Id = '" & p_iAssocId & "' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if
		
		sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) " & _
			" OR (Us.BeginningEducationDate IS NULL) " & _
			" OR (ACs.PurchaseDate IS NULL)) "
		
		set GetCourse = QueryDb(sSql)
		
		'response.Write(sSql)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: GetCourses
	' Desc: Returns a recordset of the courses this user has completed.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Returns: Recordset of CourseIds
	' -------------------------------------------------------------------------
	public function GetCourses()
	
		set GetCourses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") then		
			exit function
		end if
		
		dim sSql 'SQL Statement
		
		sSql = "SELECT * FROM vAssociatesCoursesCKUserId AS ACs " & _
			"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if

		sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) " & _
			" OR (Us.BeginningEducationDate IS NULL) " & _
			" OR (ACs.PurchaseDate IS NULL)) "

		sSql = sSql & "ORDER BY ACs.CourseExpirationDate "
			
		'Response.Write("<p>" & sSql & "<p>")
				
		set GetCourses = QueryDb(sSql)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: GetOfficerCustField
	' Desc: Returns the value of the specified OfficerCustField
	' Preconditions: none
	' Inputs: p_iNo - the number of the Officer Custom Field
	' Returns: string - the value of the Officer Custom Field
	' -------------------------------------------------------------------------
	public function	GetOfficerCustField(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetOfficerCustField = g_sOfficerCustField1
			case 2
				GetOfficerCustField = g_sOfficerCustField2
			case 3
				GetOfficerCustField = g_sOfficerCustField3
			case 4
				GetOfficerCustField = g_sOfficerCustField4
			case 5
				GetOfficerCustField = g_sOfficerCustField5
			case 6
				GetOfficerCustField = g_sOfficerCustField6
			case 7
				GetOfficerCustField = g_sOfficerCustField7
			case 8
				GetOfficerCustField = g_sOfficerCustField8
			case 9
				GetOfficerCustField = g_sOfficerCustField9
			case 10
				GetOfficerCustField = g_sOfficerCustField10
			case 11
				GetOfficerCustField = g_sOfficerCustField11
			case 12
				GetOfficerCustField = g_sOfficerCustField12
			case 13
				GetOfficerCustField = g_sOfficerCustField13
			case 14
				GetOfficerCustField = g_sOfficerCustField14
			case 15
				GetOfficerCustField = g_sOfficerCustField15		
			case else
				GetOfficerCustField = ""
		end select
	end function
	
	' -------------------------------------------------------------------------
	' Name: GetTpCourseProgress
	' Desc: Returns a percentage value of the completion of a course based on
	'	the number of completed sections.
	' Preconditions: ConnectionString
	' Inputs: p_iCourseId - ID of the course to check
	'	p_iTpUserId - TrainingPro User ID to look up
	' Returns: Integer value representing the percent completion
	' -------------------------------------------------------------------------
	public function GetTpCourseProgress(p_iCourseId, p_iTpUserId)
	
		GetTpCourseProgress = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckIntParam(p_iTpUserId, false, "User ID") then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		dim iTotalLessons
		dim iCompletedLessons
		
		iTotalLessons = 0
		iCompletedLessons = 0
		
		'Get the total number of courses 
		sSql = "SELECT UL.ID, UL.Completed FROM " & application("sTpDbName") & ".dbo.UsersLessons AS UL " & _
		    "INNER JOIN " & application("sTpDbName") & ".dbo.UsersCourses AS UC ON (UC.ID = UL.Course) " & _
			"WHERE UL.UserId = '" & p_iTpUserId & "' " & _
			"AND UC.CourseID = '" & p_iCourseId & "' "
		set oRs = QueryDb(sSql)
			
		do while not oRs.EOF
				
			if oRs("Completed") then
			
				iCompletedLessons = iCompletedLessons + 1
			
			end if
			
			iTotalLessons = iTotalLessons + 1
			
			oRs.MoveNext
				
		loop
		
		if not iTotalLessons = 0 then
			GetTpCourseProgress = round(((iCompletedLessons / iTotalLessons) * 100), 0)
		else
			GetTpCourseProgress = 0
		end if
		
	end function
	
        ' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTpUserCourseID()
	' Purpose:				Returns the UserCourseID from the TP database based upon UserID and CourseID
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserID the ID of the User in the UsersCourses table
    '                       p_iCourseID the ID of the Course in the UsersCourses table
	' Outputs:				integer - the UserCourseID
	' ----------------------------------------------------------------------------------------------------------
	Public Function GetTpUserCourseID(p_iUserID, p_iCourseID)
        dim sSql
        dim oRs

        sSql = "SELECT ID FROM " & application("sTpDbName") & ".dbo.UsersCourses WHERE UserID = " & p_iUserID & " AND CourseID = " & p_iCourseID

        set oRs = QueryDb(sSql)

        if not oRs.EOF then
            GetTpUserCourseID = oRs("UserCourseID")
        else
            GetTpUserCourseID = 0
        end if
    End Function

    ' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTpCourseCompletionPercentage()
	' Purpose:				Calculates and Returns the Completion Percentage of the specified course. 
    '                       Copied from TrainingPro Report.Class.asp to more accurately calculate percentage.
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserCourseID the ID of the course in the UsersCourses table
	' Outputs:				integer - the Completion Percentage of the specified course
	' ----------------------------------------------------------------------------------------------------------
	Public Function GetCourseCompletionPercentage(p_iUserCourseID)
		dim fSectionPercentage 'as float
		dim oRS, oLessonRS, oSectionRS 'as object
		dim sSQL 'as string
		dim iCourseCompletionPercentage, iTotalLessonTimeRequired 'as integer
		dim bCompleted, bCrossCertified 'as boolean
		dim iCrossCertType 'as integer
		dim iUserLessonID, iCourseLessonID 'as integer
		dim iTimeType, iTimedMinutes 'as integer
		dim iLessonTimeRequired, iLessonTimeCredited 'as integer
		dim iNoOfLessons, iLessonIndex 'as integer
		dim aLessonTimeRequiredArray()
		dim sLessonInfo 'as string
		dim aLessonInfo 'as array
		dim fLessonWeight 'as float
		dim fLessonCompletionPercentage, fLessonWeightPercentage 'as float
		
		checkParam(p_iUserCourseID)
		
		'Initialize Calculation values
		iCourseCompletionPercentage = 0
		iTotalLessonTimeRequired = 0
		
		'Get course details
		sSQL = "SELECT Completed, CrossCertified, CrossCertType FROM " & application("sTpDbName") & ".dbo.UsersCourses WHERE ID = " & p_iUserCourseID
		set oRS = QueryDB(sSQL)
		
		if not oRS.EOF then
			bCompleted = oRS("Completed")
			bCrossCertified = oRS("CrossCertified")
			iCrossCertType = oRS("CrossCertType")
			
			'If Course is Completed, the Percentage is 100%
			if bCompleted then
				iCourseCompletionPercentage = 100
			else
				'Retrieve All Lessons
				sSQL = "SELECT UL.ID, UL.LessonID, L.TimeType, L.TimedMinutes FROM " & application("sTpDbName") & ".dbo.UsersLessons AS UL " & _
					   "INNER JOIN " & application("sTpDbName") & ".dbo.Lessons AS L ON (UL.LessonID = L.LessonID) " & _
					   "WHERE UL.Course = '" & p_iUserCourseID & "' " & _
					   "ORDER BY L.Sort "
				set oLessonRS = QueryDB(sSQL)

				'If the course has lessons, the Course Completion Percentage will have to be computed
				if not oLessonRS.EOF then
					'Number of Lessons
					iNoOfLessons = oLessonRS.RecordCount
					redim aLessonTimeRequiredArray(iNoOfLessons-1,2)
					iLessonIndex = 0

					do while not oLessonRS.EOF
						iUserLessonID = oLessonRS("ID")
						iCourseLessonID = oLessonRS("LessonID")
						iTimeType = oLessonRS("TimeType")						
						iTimedMinutes = oLessonRS("TimedMinutes")
						
						'Set Lesson Time Required, Section Time Credited (when applicable), and Lesson Time Credited
						'Calculate values based on the Lesson Time Type
						if cint(iTimeType) = 2 then 'Per Page
							'Calculate the duration by multiplying the duration by the # of sections
							sSQL = "SELECT Count(*) FROM " & application("sTpDbName") & ".dbo.LessonSections WHERE LessonID = " & iCourseLessonID
							set oSectionRS = QueryDB(sSQL)
							
							if not oSectionRS.EOF then
								'Calculate Lesson Time Required
								iLessonTimeRequired = iTimedMinutes * oSectionRS(0)
								
								'Get Lesson Time Credited
								iLessonTimeCredited = GetLessonTimeCredited(iUserLessonID,iTimeType,iTimedMinutes)
							else
								iLessonTimeRequired = 0
								iLessonTimeCredited = 0
							end if
							set oSectionRS = nothing
						else 'Total Pages or Not Timed
							'Set Lesson Time Required to duration
							iLessonTimeRequired = iTimedMinutes
							
							'Get Lesson Time Credited
							iLessonTimeCredited = GetLessonTimeCredited(iUserLessonID,iTimeType,iTimedMinutes)
						end if
						
						'Lesson Completion Percentage
						fLessonCompletionPercentage = GetLessonCompletionPercentage(iUserLessonID,iLessonTimeRequired,iLessonTimeCredited)
						
						'Store the UserLessonID and Lesson Completion Percentage into the Array
						aLessonTimeRequiredArray(iLessonIndex,0) = iUserLessonID 
						aLessonTimeRequiredArray(iLessonIndex,1) = iLessonTimeRequired
						aLessonTimeRequiredArray(iLessonIndex,2) = fLessonCompletionPercentage 'Lesson completion Percentage
						
						'Total Lesson Time Required
						iTotalLessonTimeRequired = iTotalLessonTimeRequired + iLessonTimeRequired

						oLessonRS.MoveNext
						iLessonIndex = iLessonIndex + 1
					loop

					'Loop through the Lessons to calculate Lesson Weight, Lesson Weight Completion Percentage, and total Course Completion Percentage
					iLessonIndex = 0
					do while iLessonIndex <= UBound(aLessonTimeRequiredArray,1)
						iUserLessonID = aLessonTimeRequiredArray(iLessonIndex,0) 'the ID of the Lesson record in the UsersLessons table
						iLessonTimeRequired = aLessonTimeRequiredArray(iLessonIndex,1)
						fLessonCompletionPercentage = aLessonTimeRequiredArray(iLessonIndex,2)
						
						'Lesson Weight (Lesson Time Required divided by Total Lesson Time Required)
						fLessonWeight = iLessonTimeRequired / iTotalLessonTimeRequired
						
						'Multiply the Lesson Weight * Lesson Completion Percentage
						fLessonWeightPercentage = fLessonWeight * fLessonCompletionPercentage
						
						'Sum all Lesson Weight Percentages to get CourseCompletion Percentage
						iCourseCompletionPercentage = iCourseCompletionPercentage + fLessonWeightPercentage

						iLessonIndex = iLessonIndex + 1
					loop
				end if

				set oLessonRS = nothing

			
				'Prevent an incomplete course from being marked as 100% - GLH 03.23.11
				if iCourseCompletionPercentage > 99 then
					iCourseCompletionPercentage = 99
				end if

			end if
		end if
		
		set oRS = nothing

		GetCourseCompletionPercentage = formatnumber(iCourseCompletionPercentage,0)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLessonTimeCredited()
	' Purpose:				Calculates and Returns the Lesson Time Credited of the specified lesson
    '                       Copied from TrainingPro Report.Class.asp to more accurately calculate percentage.
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserLessonID - the ID of the lesson in the UsersLessons table
	'						p_iLessonTimeType - the Time Type of the specified lesson
	'						p_iLessonTimedMinutes - the Timed Minutes (Duration) of the Lesson
	' Outputs:				integer - the Lesson Time Credited of the specified lesson
	' ----------------------------------------------------------------------------------------------------------
	Public Function GetLessonTimeCredited(p_iUserLessonID,p_iLessonTimeType,p_iLessonTimedMinutes)
		dim iLessonTimeCredited 'as integer
		dim sSQL 'as string
		dim iUserLessonTimeLogged, iUserSectionTimeLogged 'as long
		dim iUserSectionTimeCredited 'as integer
		dim oRS 'as object
		
		CheckParam(p_iUserLessonID)
		CheckParam(p_iLessonTimeType)
		CheckParam(p_iLessonTimedMinutes)
		
		iLessonTimeCredited = 0
		
		'If Lesson Time Type is Per Page, the total time for all sections must be used in the calculation.
		if cint(p_iLessonTimeType) = 2 then 'Per Page
			'Get the Time spent on each section
			sSQL = "SELECT Total FROM " & application("sTpDbName") & ".dbo.TimeCourseTotals WHERE Lesson = " & p_iUserLessonID
			set oRS = QueryDB(sSQL)

			'For each section, calculate the Section Time Credited
			if not oRS.EOF then
				do while not oRS.EOF
					'Convert Seconds to Minutes
					iUserSectionTimeLogged = int(clng(oRS(0)) / clng(60))

					'Section Time Credited
					'Use the lesser of the User Section Time Logged and the Lesson Timed Minutes/Duration 
					if iUserSectionTimeLogged > p_iLessonTimedMinutes then
						iUserSectionTimeCredited = p_iLessonTimedMinutes
					else
						iUserSectionTimeCredited = iUserSectionTimeLogged
					end if
					
					'Add to the Total Lesson Time Credited
					iLessonTimeCredited = iLessonTimeCredited + iUserSectionTimeCredited
					oRS.MoveNext
				loop
			end if
			
			set oRS = nothing
		else
			'Get The Total Time the User has logged in the Lesson
			sSQL = "SELECT SUM(Total) FROM " & application("sTpDbName") & ".dbo.TimeCourseTotals WHERE Lesson = " & p_iUserLessonID 
			set oRS = QueryDB(sSQL)
			
			'Convert Seconds to Minutes
			if not isnull(oRS(0)) then
				iUserLessonTimeLogged  = int(clng(oRS(0)) / clng(60))
			else
				iUserLessonTimeLogged = 0
			end if
			set oRS = nothing
			
			'Lesson Time Credited
			'Use the lesser of the User Lesson Time Logged and the Lesson Timed Minutes/Duration 
			if iUserLessonTimeLogged > p_iLessonTimedMinutes then
				iLessonTimeCredited = p_iLessonTimedMinutes
			else
				iLessonTimeCredited = iUserLessonTimeLogged			
			end if
		end if

		GetLessonTimeCredited = iLessonTimeCredited
	End Function
    	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLessonCompletionPercentage()
	' Purpose:				Calculates and Returns the Lesson Completion Percentage of the specified lesson
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserLessonID -  the ID of the lesson in the UsersLessons table
	'						p_iLessonTimeRequired - the lesson time required
	'						p_iLessonTimeCredited - the time credited to the user for the lesson
	' Outputs:				integer - the Completion Percentage of the specified lesson
	' ----------------------------------------------------------------------------------------------------------
	
    Public Function GetLessonCompletionPercentage(p_iUserLessonID,p_iLessonTimeRequired,p_iLessonTimeCredited)
		dim fPercentage 'as integer
		dim bLessonCompleted, bTestPassed 'as boolean
		dim bHasTest 'as boolean
		dim sSQL 'as string
		dim oRS 'as object
		
		CheckParam(p_iUserLessonID)
		CheckParam(p_iLessonTimeRequired)
		CheckParam(p_iLessonTimeCredited)

		'Determine if the Lesson was completed
		sSQL = "SELECT Completed FROM " & application("sTpDbName") & ".dbo.UsersLessons WHERE ID = " & p_iUserLessonID
		set oRS = QueryDB(sSQL)
		bLessonCompleted = oRS("Completed")
		set oRS = nothing		

		'Calculate Percentage based upon if the Lesson was Completed
		if bLessonCompleted then 
			'Determine if any Test for the Lesson was completed
			sSQL = "SELECT UTI.TestPassed FROM " & application("sTpDbName") & ".dbo.UsersTestsInfo AS UTI " & _
				   "INNER JOIN " & application("sTpDbName") & ".dbo.Tests AS T ON (T.TestID = UTI.TestID) " & _
				   "WHERE UTI.Lesson = " & p_iUserLessonID & " AND T.TestType = 1"
			set oRS = QueryDB(sSQL)
			if not oRS.EOF then
				bTestPassed = oRS("TestPassed")
				bHasTest = true
			else
				bTestPassed = false
				bHasTest = false
			end if
			set oRS = nothing		

			'If the Lesson is completed and the lesson does not have a test, the percentage is 100%.
			'If the Lesson is completed and the lesson does have a test, the percentage is 100 if the test is also complete or 99 if the test is not complete.		
			if not bHasTest then
				fPercentage = 1
			elseif bHasTest and bTestPassed then
				fPercentage = 1
			elseif bHasTest and not bTestPassed then
				fPercentage = .99
			end if
		else
			if cint(p_iLessonTimeRequired) = 0 or cint(p_iLessonTimeCredited) = 0 then
				fPercentage = 0
			else
				fPercentage = (p_iLessonTimeCredited / p_iLessonTimeRequired) * .98
			end if
		end if
		
		GetLessonCompletionPercentage = fPercentage * 100 'Change to percent
	End Function

	' -------------------------------------------------------------------------
	' Name: GetWorkAddress
	' Desc: Returns the address of the associate depending on whether they are
	'	assigned to a hierarchy office
	' Preconditions: Connectionstring, UserId
	' Returns: Recordset with address values
	' -------------------------------------------------------------------------
	public function GetWorkAddress()
		
		set GetWorkAddress = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "UserId") then
			exit function
		end if
		
		dim sSql 
		
		if g_iBranchId <> "" then
		
			sSql = "SELECT Address, Address2, City, StateId, " & _
				"Zipcode, ZipcodeExt FROM CompanyBranches " & _
				"WHERE BranchId = '" & g_iBranchId & "' "
			set GetWorkAddress = QueryDb(sSql)
											
		elseif g_iCompanyL3Id <> "" then

			sSql = "SELECT Address, Address2, City, StateId, " & _
				"Zipcode, ZipcodeExt FROM CompaniesL3 " & _
				"WHERE CompanyL3Id = '" & g_iCompanyL3Id & "' "
			set GetWorkAddress = QueryDb(sSql)
											
		elseif g_iCompanyL2Id <> "" then
		
			sSql = "SELECT Address, Address2, City, StateId, " & _
				"Zipcode, ZipcodeExt FROM CompaniesL2 " & _
				"WHERE CompanyL2Id = '" & g_iCompanyL2Id & "' "
			set GetWorkAddress = QueryDb(sSql)
						
		else
		
			sSql = "SELECT Address, Address2, City, StateId, " & _
				"Zipcode, ZipcodeExt FROM Companies " & _
				"WHERE CompanyId = '" & g_iCompanyId & "' "
			set GetWorkAddress = QueryDb(sSql)
																			
		end if			
	
	end function
	
	' -------------------------------------------------------------------------
	' Name: LoadAssociateByEmail
	' Desc: Load the associate object that matches the passed Email
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadAssociateByEmail(p_sEmail, p_iCompanyId)

		LoadAssociateByEmail = 0

		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sEmail, 0, "Email") or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if

		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT UserId FROM Associates " & _
			"WHERE Email LIKE '" & p_sEmail & "' " & _
			"AND CompanyId = '" & p_iCompanyId & "' " & _
			"AND UserStatus = '1' "
		set oRs = QueryDb(sSql)
			

		if not (oRs.BOF and oRs.EOF) then

			LoadAssociateByEmail = LoadAssociateById(oRs("UserId"))

		else		

			ReportError("Failed to locate the requested officer.")

		end if

		set oRs = nothing

	end function
	



	' -------------------------------------------------------------------------
	' Name: LoadAssociateByNMLS
	' Desc: Load the associate object that matches the passed NMLS Number
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------

    	public function LoadAssociateByNMLS(p_sNMLSNumber, p_iCompanyId)

		LoadAssociateByNMLS = 0

		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sNMLSNumber, 0, "NMLSNumber") or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if

		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT UserId FROM Associates " & _
			"WHERE NMLSNumber LIKE '" & p_sNMLSNumber & "' " & _
			"AND CompanyId = '" & p_iCompanyId & "' " 
            
            '"AND UserStatus = '1' "
		set oRs = QueryDb(sSql)
			
		if not (oRs.BOF and oRs.EOF) then
			LoadAssociateByNMLS = LoadAssociateById(oRs("UserId"))
		else		
			ReportError("Failed to locate the requested officer.")
		end if

		set oRs = nothing

	end function

	' -------------------------------------------------------------------------
	' Name: LoadAssociateById
	' Description: This method loads an associate's information into the
	'			   associate object.
	' Preconditions: ConnectionString
	' Inputs: p_iUserId - ID of the associate to load.
	' Returns: Returns the user ID if successful, otherwise 0.
	' -------------------------------------------------------------------------
	public function LoadAssociateById(p_iUserId)
	
		LoadAssociateById = 0
	
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iUserId, false, "UserID") then
			exit function
		end if
		
		dim oRs, oRs2 'Recordset
		dim sSql, sSql2 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")

		
		'Select the user from the combined TP/CMS Associates view			
		sSql = "SELECT Us.*, Cs.TpLinkCompanyId FROM Associates AS Us " & _
			"LEFT JOIN Companies AS Cs " & _
			"ON (Cs.CompanyId = Us.CompanyId) " & _
			"WHERE Us.UserId = '" & p_iUserId & "' "
		set oRs = QueryDb(sSql)
			
		
		if not (oRs.BOF and oRs.EOF) then
		
			g_iUserId = p_iUserId 'oRs("UserId")
			g_iCompanyId = oRs("CompanyId")
			g_iTpLinkCompanyId = oRs("TpLinkCompanyId")
			g_iCompanyL2Id = oRs("CompanyL2Id")
			g_iCompanyL3Id = oRs("CompanyL3Id")
			g_iBranchid = oRs("BranchId")
			g_sEmail = oRs("Email")
			g_sFirstName = oRs("FirstName")
			g_sMiddleName = trim(oRs("MiddleName"))
			g_sLastName = oRs("LastName")
			g_sSsn = oRs("Ssn")
			g_iGenderId = oRs("GenderId")
			g_iRaceId = oRs("RaceId")
			g_dDateOfBirth = oRs("DateOfBirth")
			g_sTitle = oRs("Title")
			g_sDepartment = oRs("Department")
			g_sDriversLicenseNo = oRs("DriversLicenseNo")
			g_iDriversLicenseStateId = oRs("DriversLicenseStateId")
			g_dHireDate = oRs("HireDate")
			g_dBeginningEducationDate = oRs("BeginningEducationDate")
			g_dTerminationDate = oRs("TerminationDate")
			g_dFingerprintDeadlineDate = oRs("FingerprintDeadlineDate")			
			g_sLicensePayment = oRs("LicensePayment")
			g_bInactive = oRs("Inactive")
			g_sManager = oRs("Manager")
			g_bOutOfStateOrig = oRs("OutOfStateOrig")
			g_sEmployeeId = oRs("EmployeeId")
			g_iCountryId = oRs("CountryId")
			g_sPhone = oRs("Phone")
			g_sPhone2 = oRs("Phone2")
			g_sPhone3 = oRs("Phone3")
			g_sPhoneExt = oRs("PhoneExt")
			g_sPhone2Ext = oRs("Phone2Ext")
			g_sPhone3Ext = oRs("Phone3Ext")
			g_sFax = oRs("Fax")
			g_sCellPhone = oRs("CellPhone")
			g_sHomePhone = oRs("HomePhone")
			g_sHomeEmail = oRs("HomeEmail")
			g_iUserStatus = oRs("UserStatus")
			g_sNMLSNumber = oRs("NMLSNumber")
			g_sOfficerCustField1 = oRs("OfficerCustField1")
			g_sOfficerCustField2 = oRs("OfficerCustField2")
			g_sOfficerCustField3 = oRs("OfficerCustField3")
			g_sOfficerCustField4 = oRs("OfficerCustField4")
			g_sOfficerCustField5 = oRs("OfficerCustField5")
			g_sOfficerCustField6 = oRs("OfficerCustField6")
			g_sOfficerCustField7 = oRs("OfficerCustField7")
			g_sOfficerCustField8 = oRs("OfficerCustField8")
			g_sOfficerCustField9 = oRs("OfficerCustField9")
			g_sOfficerCustField10 = oRs("OfficerCustField10")
			g_sOfficerCustField11 = oRs("OfficerCustField11")
			g_sOfficerCustField12 = oRs("OfficerCustField12")
			g_sOfficerCustField13 = oRs("OfficerCustField13")
			g_sOfficerCustField14 = oRs("OfficerCustField14")
			g_sOfficerCustField15 = oRs("OfficerCustField15")
          
            g_bVerified = oRs("Verified")							
			
			'Populate objects
			sSql2 = "SELECT * FROM AssociateAddresses " & _
					   "WHERE UserId = " & p_iUserId & " " & _
					   "ORDER BY AddressNo "
			set oRs2 = QueryDb(sSql2)
			
			if not oRs2.eof then
				do while not oRs2.eof
					select case oRs2("AddressNo")
						case 1
							g_oAddress1.AddressLine1 = oRs2("AddressLine1")
							g_oAddress1.AddressLine2 = oRs2("AddressLine2")							
							g_oAddress1.City = oRs2("City")							
							g_oAddress1.StateID = oRs2("StateID")							
							g_oAddress1.Zipcode = oRs2("Zipcode")							
							g_oAddress1.ZipcodeExt = oRs2("ZipcodeExt")				
						case 2
							g_oAddress2.AddressLine1 = oRs2("AddressLine1")
							g_oAddress2.AddressLine2 = oRs2("AddressLine2")							
							g_oAddress2.City = oRs2("City")							
							g_oAddress2.StateID = oRs2("StateID")							
							g_oAddress2.Zipcode = oRs2("Zipcode")							
							g_oAddress2.ZipcodeExt = oRs2("ZipcodeExt")		
						case 3
							g_oAddress3.AddressLine1 = oRs2("AddressLine1")
							g_oAddress3.AddressLine2 = oRs2("AddressLine2")							
							g_oAddress3.City = oRs2("City")							
							g_oAddress3.StateID = oRs2("StateID")							
							g_oAddress3.Zipcode = oRs2("Zipcode")							
							g_oAddress3.ZipcodeExt = oRs2("ZipcodeExt")		
						case 4
							g_oAddress4.AddressLine1 = oRs2("AddressLine1")
							g_oAddress4.AddressLine2 = oRs2("AddressLine2")							
							g_oAddress4.City = oRs2("City")							
							g_oAddress4.StateID = oRs2("StateID")							
							g_oAddress4.Zipcode = oRs2("Zipcode")							
							g_oAddress4.ZipcodeExt = oRs2("ZipcodeExt")																																																						
					end select
					
					oRs2.MoveNext
				loop
			end if
			
			set oRs2 = nothing
			
			LoadAssociateById = g_iUserId
		
		else

			ReportError("Unable to load officer.  Record not found.")
			exit function
		
		end if 
				
		
		set oRs = nothing
		
	end function

	' -------------------------------------------------------------------------
	' Name: LoadAssociateByLastNameSsn
	' Desc: Load the associate object that matches the passed SSN and Last
	'	name.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadAssociateByLastNameSsn(p_sSsn, p_sLastName, p_iCompanyId)

		LoadAssociateByLastNameSsn = 0

		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sSsn, 0, "SSN") or _
			not CheckStrParam(p_sLastName, 0, "Last Name") or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if

		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT UserId FROM Associates " & _
			"WHERE SSN = '" & p_sSsn & "' " & _
			"AND LastName = '" & p_sLastName & "' " & _
			"AND CompanyId = '" & p_iCompanyId & "' " & _
			"AND UserStatus = '1' "
		set oRs = QueryDb(sSql)
			

		if not (oRs.BOF and oRs.EOF) then

			LoadAssociateByLastNameSsn = LoadAssociateById(oRs("UserId"))

		else		

			ReportError("Failed to locate the requested officer.")

		end if

		set oRs = nothing

	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupBranchNumber
	' Desc: Gets the BranchNumber value of the passed BranchId
	' Preconditions: ConnectionString
	' Inputs: p_iBranchId - ID of the Branch to lookup
	' Returns: Value of the BranchNumber of the Branch 
	' -------------------------------------------------------------------------
	public function LookupBranchName(p_iBranchId)
	
		LookupBranchName = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM CompanyBranches WHERE " & _
			"BranchId = '" & p_iBranchId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupBranchName = oRs("Name")
			
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupBranchNumber
	' Desc: Gets the BranchNumber value of the passed BranchId
	' Preconditions: ConnectionString
	' Inputs: p_iBranchId - ID of the Branch to lookup
	' Returns: Value of the BranchNumber of the Branch 
	' -------------------------------------------------------------------------
	public function LookupBranchNumber(p_iBranchId)
	
		LookupBranchNumber = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM CompanyBranches WHERE " & _
			"BranchId = '" & p_iBranchId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupBranchNumber = oRs("BranchNum")
			
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupCompanyL2Name
	' Desc: Gets the name value of the passed CompanyL2Id
	' Preconditions: ConnectionString
	' Inputs: p_iCompanyL2Id - ID of the L2 to lookup
	' Returns: Value of the name of the L2
	' -------------------------------------------------------------------------
	public function LookupCompanyL2Name(p_iCompanyL2Id)
	
		LookupCompanyL2Name = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCompanyL2Id, 0, session("CompanyL2Name") & _
				" ID") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM CompaniesL2 WHERE " & _
			"CompanyL2Id = '" & p_iCompanyL2Id & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupCompanyL2Name = oRs("Name")
			
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupCompanyL3Name
	' Desc: Gets the name value of the passed CompanyL3Id
	' Preconditions: ConnectionString
	' Inputs: p_iCompanyL3Id - ID of the L3 to lookup
	' Returns: Value of the name of the L3
	' -------------------------------------------------------------------------
	public function LookupCompanyL3Name(p_iCompanyL3Id)
	
		LookupCompanyL3Name = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCompanyL3Id, 0, session("CompanyL3Name") & _
				" ID") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM CompaniesL3 WHERE " & _
			"CompanyL3Id = '" & p_iCompanyL3Id & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupCompanyL3Name = oRs("Name")
			
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupGenderId
	' Description: Returns the long form of a GenderType
	' Preconditions: ConnectionString
	' Inputs: p_iGenderId - Gender ID to look up
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function LookupGenderId()
	
		LookupGenderId = ""
		
		'Verify preconditions
		if not CheckConnectionstring then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM GenderTypes WHERE " & _
			"GenderId = '" & g_iGenderId & "'" 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
				
			LookupGenderId = oRs("GenderType")
		
		end if 
		
		set oRs = nothing
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupRaceId
	' Description: Returns the long form of a RaceType
	' Preconditions: ConnectionString
	' Inputs: p_iRaceId - Race ID to look up
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function LookupRaceId()
	
		LookupRaceId = ""
		
		'Verify preconditions
		if not CheckConnectionstring then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM RaceTypes WHERE " & _
			"RaceId = '" & g_iRaceId & "'" 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
				
			LookupRaceId = oRs("RaceType")
		
		end if 
		
		set oRs = nothing
	
	end function
	
	' -------------------------------------------------------------------------
	' Name: LookupTpUserId
	' Desc: Returns the unique user ID of a TrainingPro user matching the
	'	passed email address
	' Preconditions: Connectionstring
	' Inputs: p_sUserName
	' Returns: Integer UserID
	' -------------------------------------------------------------------------
	public function LookupTpUserId(p_sUserName)
		
		LookupTpUserId = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sUserName, false, "User Name") then
			exit function
		end if
		
		dim sSql 
		dim oRs
		
		sSql = "SELECT UserId FROM " & application("sTpDbName") & ".dbo.Users WHERE " & _
			"UserName LIKE '" & p_sUserName & "' " & _
			"AND UserStatus = '1'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupTpUserId = oRs("UserId")
			
		end if
		
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: RemoveCourse
	' Desc: Removes a course association 
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function RemoveCourse(p_iAssocId)
	
		RemoveCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Association ID") then
			exit function
		end if
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Number of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE AssociatesCourses SET " & _
			"Deleted = '1' " & _
			"WHERE Id = '" & p_iAssocId & "' " & _
			"AND UserId = '" & g_iUserId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to delete the course record.")
			
		else
		
			RemoveCourse = 1
			
		end if 
		
		oConn.Close
		set oConn = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: SaveAssociate
	' Description: Save the associate object in the database, and assign it the
	'			   resulting ID.
	' Preconditions: ConnectionString, TpConnectionString, Email
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveAssociate()
	   
       	SaveAssociate = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sEmail, 0, "Email") then
			exit function
		end if
		
		'Verify that we have a unique email address
       
        if not VerifyUniqueNMLS(g_sNMLSNumber) then
		'if not VerifyUniqueEmail(g_sEmail) then
			'ReportError("The email address provided is already in use in the system.  Saving the officer requires a unique email address.")
            ReportError("The NMLS Number provided is already in use in the system.  Saving the officer requires a unique NMLS Number.")
			exit function
		end if
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if g_iUserId = "" then 		
			'This is a new user for a CMS Company, so we'll add the user to the
			'CMS database.
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
		
			sSql = "INSERT INTO Associates (" & _
				"CompanyId, " & _
				"CompanyL2Id, " & _
				"CompanyL3Id, " & _
				"BranchId, " & _
				"Email, " & _
				"FirstName, " & _
				"MiddleName, " & _
				"LastName, " & _
				"Ssn, " & _
				"GenderId, " & _
				"RaceId, " & _
				"DateOfBirth, " & _
				"Title, " & _
				"Department, " & _
				"DriversLicenseNo, " & _
				"DriversLicenseStateId, " & _
				"HireDate, " & _
				"BeginningEducationDate, " & _
				"TerminationDate, " & _
				"FingerprintDeadlineDate, " & _				
				"LicensePayment, " & _
				"Inactive, " & _
				"Manager, " & _
				"OutOfStateOrig, " & _
				"EmployeeId, " & _
				"CountryId, " & _
				"Phone, " & _
				"Phone2, " & _
				"Phone3, " & _
				"PhoneExt, " & _
				"Phone2Ext, " & _
				"Phone3Ext, " & _
				"Fax, " & _
				"CellPhone, " & _
				"HomePhone, " & _
				"HomeEmail, " & _
				"UserStatus, " & _
				"NMLSNumber, " & _
				"OfficerCustField1, " & _
				"OfficerCustField2, " & _
				"OfficerCustField3, " & _
				"OfficerCustField4, " & _
				"OfficerCustField5, " & _
				"OfficerCustField6, " & _
				"OfficerCustField7, " & _
				"OfficerCustField8, " & _
				"OfficerCustField9, " & _
				"OfficerCustField10, " & _
				"OfficerCustField11, " & _
				"OfficerCustField12, " & _
				"OfficerCustField13, " & _
				"OfficerCustField14, " & _
				"OfficerCustField15, " & _
                "Verified " & _
				") VALUES (" & _
				"'" & g_iCompanyId & "', "
				
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "'" & g_iCompanyL2Id & "', " 
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "'" & g_iCompanyL3Id & "', " 
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_iBranchId <> "" then
				sSql = sSql & "'" & g_iBranchId & "', " 
			else
				sSql = sSql & "NULL, "
			end if
				
			sSql = sSql & "'" & g_sEmail & "', " & _
				"'" & g_sFirstName & "', " & _
				"'" & g_sMiddleName & "', " & _
				"'" & g_sLastName & "', " & _
				"'" & g_sSsn & "', " & _
				"'" & g_iGenderId & "', " & _
				"'" & g_iRaceId & "', "
			if g_dDateOfBirth <> "" then 
				sSql = sSql & "'" & g_dDateOfBirth & "', "
			else
				sSql = sSql & "NULL, "
			end if
			sSql = sSql & "'" & g_sTitle & "', " & _
				"'" & g_sDepartment & "', " & _
				"'" & g_sDriversLicenseNo & "', " & _
				"'" & g_iDriversLicenseStateId & "', "
			if g_dHireDate <> "" then
				sSql = sSql & "'" & g_dHireDate & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dBeginningEducationDate <> "" then
				sSql = sSql & "'" & g_dBeginningEducationDate & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dTerminationDate <> "" then	
				sSql = sSql & "'" & g_dTerminationDate & "', "
			else
				sSql = sSql & "NULL, "
			end if			
			
			if g_dFingerprintDeadlineDate <> "" then	
				sSql = sSql & "'" & g_dFingerprintDeadlineDate & "', "
			else
				sSql = sSql & "NULL, "
			end if						
							
			sSql = sSql & "'" & g_sLicensePayment & "', " & _
				"'" & abs(cint(g_bInactive)) & "', " & _
				"'" & g_sManager & "', " & _
				"'" & abs(cint(g_bOutOfStateOrig)) & "', " & _
				"'" & g_sEmployeeId & "', " & _
				"'" & g_iCountryId & "', " & _ 
				"'" & g_sPhone & "', " & _ 
				"'" & g_sPhone2 & "', " & _
				"'" & g_sPhone3 & "', " & _
				"'" & g_sPhoneExt & "', " & _
				"'" & g_sPhone2Ext & "', " & _
				"'" & g_sPhone3Ext & "', " & _
				"'" & g_sFax & "', " & _ 
				"'" & g_sCellPhone & "', " & _
				"'" & g_sHomePhone & "', " & _
				"'" & g_sHomeEmail & "', " & _
				"'" & g_iUserStatus & "', " & _
				"'" & g_sNMLSNumber & "', " & _
				"'" & g_sOfficerCustField1 & "', " & _
				"'" & g_sOfficerCustField2 & "', " & _
				"'" & g_sOfficerCustField3 & "', " & _
				"'" & g_sOfficerCustField4 & "', " & _
				"'" & g_sOfficerCustField5 & "', " & _
				"'" & g_sOfficerCustField6 & "', " & _
				"'" & g_sOfficerCustField7 & "', " & _
				"'" & g_sOfficerCustField8 & "', " & _
				"'" & g_sOfficerCustField9 & "', " & _
				"'" & g_sOfficerCustField10 & "', " & _
				"'" & g_sOfficerCustField11 & "', " & _
				"'" & g_sOfficerCustField12 & "', " & _
				"'" & g_sOfficerCustField13 & "', " & _
				"'" & g_sOfficerCustField14 & "', " & _
				"'" & g_sOfficerCustField15 & "', " & _	
                "'" & g_bVerified & "' " & _				
				")"
			'Response.Write(sSql)	
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to create new CMS officer.")
				exit function
			
			end if 
			
			'Attempt to retrieve the index of the newly created user.
			sSql = "SELECT UserId FROM Associates WHERE " & _
				   "NMLSNumber = '" & g_sNMLSNumber & "' "				   
			set oRs = oConn.Execute(sSql)
			
			if oRs.EOF then
			
				ReportError("Failed to create new CMS officer.")
				exit function
				
			else
				
				g_iUserId = oRs("UserId")
				
				SaveAssociate = g_iUserId
				
				'Add Associate Address records
				sSql = "INSERT INTO AssociateAddresses (UserID, AddressNo, AddressLine1, AddressLine2, City, StateID, Zipcode, ZipcodeExt) " & _
						   "VALUES (" & g_iUserID & ", 1, '" & g_oAddress1.AddressLine1 & "','" & g_oAddress1.AddressLine2 & "','" & g_oAddress1.City & "','" & g_oAddress1.StateID & "','" & g_oAddress1.Zipcode & "','" & g_oAddress1.ZipcodeExt & "')"
				oConn.execute(sSql)
				
				sSql = "INSERT INTO AssociateAddresses (UserID, AddressNo, AddressLine1, AddressLine2, City, StateID, Zipcode, ZipcodeExt) " & _
						   "VALUES (" & g_iUserID & ", 2, '" & g_oAddress2.AddressLine1 & "','" & g_oAddress2.AddressLine2 & "','" & g_oAddress2.City & "','" & g_oAddress2.StateID & "','" & g_oAddress2.Zipcode & "','" & g_oAddress2.ZipcodeExt & "')"
				oConn.execute(sSql)
				
				sSql = "INSERT INTO AssociateAddresses (UserID, AddressNo, AddressLine1, AddressLine2, City, StateID, Zipcode, ZipcodeExt) " & _
						   "VALUES (" & g_iUserID & ", 3, '" & g_oAddress3.AddressLine1 & "','" & g_oAddress3.AddressLine2 & "','" & g_oAddress3.City & "','" & g_oAddress3.StateID & "','" & g_oAddress3.Zipcode & "','" & g_oAddress3.ZipcodeExt & "')"
				oConn.execute(sSql)
				
				sSql = "INSERT INTO AssociateAddresses (UserID, AddressNo, AddressLine1, AddressLine2, City, StateID, Zipcode, ZipcodeExt) " & _
						   "VALUES (" & g_iUserID & ", 4, '" & g_oAddress4.AddressLine1 & "','" & g_oAddress4.AddressLine2 & "','" & g_oAddress4.City & "','" & g_oAddress4.StateID & "','" & g_oAddress4.Zipcode & "','" & g_oAddress4.ZipcodeExt & "')"
				oConn.execute(sSql)																																
			
			end if 
			
			
		'This is an existing record that we'll update.							
		else
		
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
	
			sSql = "UPDATE Associates SET " & _
				"CompanyId = '" & g_iCompanyId & "', "
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "CompanyL2Id = '" & g_iCompanyL2Id & "', "
			else
				sSql = sSql & "CompanyL2Id = NULL, "
			end if
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "CompanyL3Id = '" & g_iCompanyL3Id & "', "
			else
				sSql = sSql & "CompanyL3Id = NULL, "
			end if
			if g_iBranchId <> "" then
				sSql = sSql & "BranchId = '" & g_iBranchId & "', "
			else
				sSql = sSql & "BranchId = NULL, "
			end if
			sSql = sSql & "Email = '" & g_sEmail & "', " & _
				"FirstName = '" & g_sFirstName & "', " & _
				"MiddleName = '" & g_sMiddleName & "', " & _
				"LastName = '" & g_sLastName & "', " & _
				"Ssn = '" & g_sSsn & "', " & _
				"GenderId = '" & g_iGenderId & "', " & _
				"RaceId = '" & g_iRaceId & "', "
			if g_dDateofBirth <> "" then
				sSql = sSql & "DateOfBirth = '" & g_dDateOfBirth & "', "
			else
				sSql = sSql & "DateOfBirth = NULL, "
			end if 
			sSql = sSql & "Title = '" & g_sTitle & "', " & _
				"Department = '" & g_sDepartment & "', " & _
				"DriversLicenseNo = '" & g_sDriversLicenseNo & "', " & _
				"DriversLicenseStateId = '" & _
				g_iDriversLicenseStateId & "', " & _
				"EmployeeId = '" & g_sEmployeeId & "', "
			if g_dHireDate <> "" then
				sSql = sSql & "HireDate = '" & g_dHireDate & "', "
			else
				sSql = sSql & "HireDate = NULL, " 
			end if
			if g_dBeginningEducationDate <> "" then
				sSql = sSql & "BeginningEducationDate = '" & g_dBeginningEducationDate & "', "
			else
				sSql = sSql & "BeginningEducationDate = NULL, " 
			end if
			if g_dTerminationDate <> "" then	
				sSql = sSql & "TerminationDate = '" & _
					g_dTerminationDate & "', "
			else
				sSql = sSql & "TerminationDate = NULL, "
			end if
			
			if g_dFingerprintDeadlineDate <> "" then	
				sSql = sSql & "FingerprintDeadlineDate = '" & _
					g_dFingerprintDeadlineDate & "', "
			else
				sSql = sSql & "FingerprintDeadlineDate = NULL, "
			end if
			
			sSql = sSql & _
				"LicensePayment = '" & g_sLicensePayment & "', " & _
				"Inactive = '" & abs(cint(g_bInactive)) & "', " & _
				"Manager = '" & g_sManager & "', " & _
				"OutOfStateOrig = '" & abs(cint(g_bOutOfStateOrig)) & "', " & _
				"Phone = '" & g_sPhone & "', " & _
				"Phone2 = '" & g_sPhone2 & "', " & _
				"Phone3 = '" & g_sPhone3 & "', " & _
				"PhoneExt = '" & g_sPhoneExt & "', " & _
				"Phone2Ext = '" & g_sPhone2Ext & "', " & _
				"Phone3Ext = '" & g_sPhone3Ext & "', " & _
				"Fax = '" & g_sFax & "', " & _
				"Active = '" & g_bActive & "', " & _
				"UserStatus = '" & g_iUserStatus & "', " & _
				"NMLSNumber = '" & g_sNMLSNumber & "', " & _
				"OfficerCustField1 = '" & g_sOfficerCustField1 & "', " & _
				"OfficerCustField2 = '" & g_sOfficerCustField2 & "', " & _
				"OfficerCustField3 = '" & g_sOfficerCustField3 & "', " & _
				"OfficerCustField4 = '" & g_sOfficerCustField4 & "', " & _
				"OfficerCustField5 = '" & g_sOfficerCustField5 & "', " & _
				"OfficerCustField6 = '" & g_sOfficerCustField6 & "', " & _
				"OfficerCustField7 = '" & g_sOfficerCustField7 & "', " & _
				"OfficerCustField8 = '" & g_sOfficerCustField8 & "', " & _
				"OfficerCustField9 = '" & g_sOfficerCustField9 & "', " & _
				"OfficerCustField10 = '" & g_sOfficerCustField10 & "', " & _
				"OfficerCustField11 = '" & g_sOfficerCustField11 & "', " & _
				"OfficerCustField12 = '" & g_sOfficerCustField12 & "', " & _
				"OfficerCustField13 = '" & g_sOfficerCustField13 & "', " & _
				"OfficerCustField14 = '" & g_sOfficerCustField14 & "', " & _
				"OfficerCustField15 = '" & g_sOfficerCustField15 & "', " & _
                "Verified = '" & g_bVerified & "' " & _
				"WHERE UserId = '" & g_iUserId & "'"
				'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected
		
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to save existing user.")
				exit function
			
			end if 
			
			SaveAssociate = g_iUserId
		
			'Update Associate Addresses
			sSql = "UPDATE AssociateAddresses " & _
					   "SET AddressLine1 = '" & g_oAddress1.AddressLine1 & "', " & _
					   "        AddressLine2 = '" & g_oAddress1.AddressLine2 & "', " & _					   
					   "        City = '" & g_oAddress1.City & "', " & _					   
					   "        StateID = '" & g_oAddress1.StateID & "', " & _					   
					   "        Zipcode = '" & g_oAddress1.Zipcode & "', " & _					   
					   "        ZipcodeExt = '" & g_oAddress1.ZipCodeExt & "' " & _					   					   					   					   		
					   "WHERE UserID = " & g_iUserID & " AND AddressNo = 1"
			oConn.Execute(sSql)
			
			sSql = "UPDATE AssociateAddresses " & _
					   "SET AddressLine1 = '" & g_oAddress2.AddressLine1 & "', " & _
					   "        AddressLine2 = '" & g_oAddress2.AddressLine2 & "', " & _					   
					   "        City = '" & g_oAddress2.City & "', " & _					   
					   "        StateID = '" & g_oAddress2.StateID & "', " & _					   
					   "        Zipcode = '" & g_oAddress2.Zipcode & "', " & _					   
					   "        ZipcodeExt = '" & g_oAddress2.ZipCodeExt & "' " & _					   					   					   					   		
					   "WHERE UserID = " & g_iUserID & " AND AddressNo = 2"
			oConn.Execute(sSql)
			
			sSql = "UPDATE AssociateAddresses " & _
					   "SET AddressLine1 = '" & g_oAddress3.AddressLine1 & "', " & _
					   "        AddressLine2 = '" & g_oAddress3.AddressLine2 & "', " & _					   
					   "        City = '" & g_oAddress3.City & "', " & _					   
					   "        StateID = '" & g_oAddress3.StateID & "', " & _					   
					   "        Zipcode = '" & g_oAddress3.Zipcode & "', " & _					   
					   "        ZipcodeExt = '" & g_oAddress3.ZipCodeExt & "' " & _					   					   					   					   		
					   "WHERE UserID = " & g_iUserID & " AND AddressNo = 3"
			oConn.Execute(sSql)
			
			sSql = "UPDATE AssociateAddresses " & _
					   "SET AddressLine1 = '" & g_oAddress4.AddressLine1 & "', " & _
					   "        AddressLine2 = '" & g_oAddress4.AddressLine2 & "', " & _					   
					   "        City = '" & g_oAddress4.City & "', " & _					   
					   "        StateID = '" & g_oAddress4.StateID & "', " & _					   
					   "        Zipcode = '" & g_oAddress4.Zipcode & "', " & _					   
					   "        ZipcodeExt = '" & g_oAddress4.ZipCodeExt & "' " & _					   					   					   					   		
					   "WHERE UserID = " & g_iUserID & " AND AddressNo = 4"
			oConn.Execute(sSql)									
		end if
		
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: SearchAssociates
	' Description: Retrieves a collection of UserIds that match the properties
	'			   of the current object.  I.e., based on the current Name, etc
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchAssociates()

		set SearchAssociates = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim sSql2
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT DISTINCT VAs.UserId, VAs.LastName, VAs.FirstName, VAs.Inactive " & _
			"FROM Associates AS VAs " & _
			"LEFT OUTER JOIN AssociateAddresses AS AAs ON (AAs.UserID = VAs.UserID AND AAs.AddressNo = 1) " 

        'g_iLicensed = ""    - ALL
        'g_iLicensed = true  - Licensed
        'g_iLicensed = false - Not Licensed
        
        if g_iLicensed <> ""  then
            if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
            if g_iLicensed = true then
                			   
            else
               sSql = sSql & " not "
            end if 
            sSql = sSql & " exists (SELECT L.LicenseID FROM Associates as a2 INNER JOIN Licenses as L ON (a2.UserID = L.OwnerID)	Where a2.UserId = vas.UserId) "
        end if 
        

		
        'If NMLSNumber search
        if g_sNMLSNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.NMLSNumber = '" & g_sNMLSNumber & "'"
		end if 


		'If FirstName search
		if g_sFirstName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.FirstName = '" & g_sFirstName & "'"
		end if 
		
		
		'If LastName search
		if g_sLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName ='" & g_sLastName & "'" 
		end if 
		
		
		'If LastNameStart search
		if g_sSearchLastNameStart <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName LIKE '" & g_sSearchLastNameStart & "%'"
		end if
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyId = '" & g_iCompanyId & "'"
		end if 

		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL3Id = '" & iCompanyL3Id & "' "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if		

		'If CompanyL2Id search
		if g_iCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL2Id = '" & g_iCompanyL2Id & "'"
		end if
		
		
		'If CompanyL3Id search
		if g_iCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL3Id = '" & g_iCompanyL3Id & "'"
		end if

		
		'If BranchId search
		if g_iBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.BranchId = '" & g_iBranchId & "'"
		end if
		
		
		'If Email search
		if g_sEmail <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Email = '" & g_sEmail & "'"
		end if
		
		
		'If SSN search
		if g_sSsn <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Ssn = '" & g_sSsn & "'"
		end if 
		
        'If Verified search
		if g_bVerified <> "" then
        
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Verified = '" & g_bVerified & "'"
		end if 
		

		'If Hire Date From search
		if g_dHireDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.HireDate >= '" & g_dHireDateFrom & " 00:00'"
		end if 		
		
		'If Hire Date To search
		if g_dHireDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.HireDate <= '" & g_dHireDateTo & " 23:59'"
		end if 		
		
		'If UserStatus search
		if g_iUserStatus <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.UserStatus = '" & g_iUserStatus & "'"
		end if 
		
		
		'If Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
				
		
		'If StateId search
		if g_iSearchStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "AAs.StateId = '" & g_iSearchStateId & "'"
		end if
		

		sSql = sSql & " ORDER BY LastName"
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchAssociates = QueryDb(sSql)
			
	end function

	' -------------------------------------------------------------------------
	' Name: SearchAssociatesCoursesLicenses
	' Description: Retrieves a collection of UserIds that match the properties
	'			   of the current object.  I.e., based on the current Name, 
	'			   etc.  This differs from the SearchAssociates function in
	'			   that it searches by course and license information as well,
	'			   and will return the same user multiple times for each
	'			   course or license item on which they match.  It is used in
	'			   the site reports function.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchAssociatesCoursesLicenses()
		set SearchAssociatesCoursesLicenses = _
			Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = true 'false
		
		sSql = "SELECT DISTINCT VAs.UserId, VAs.LastName, VAs.FirstName " & _
			"FROM Associates AS VAs " & _
			"LEFT OUTER JOIN AssociateAddresses AS AAs ON (AAs.UserID = VAs.UserID AND AAs.AddressNo = 1) " & _
			"LEFT OUTER JOIN tmp_vAssociatesCourses AS VACs " & _
			"ON (VACs.UserId = VAs.UserID " & _
			"AND (VACs.CompanyId = '" & g_iCompanyId & "' "
			
		if session("UserTpLinkCompanyId") <> "" then
			sSql = sSql & "OR VACs.CompanyId = '" & _
				session("UserTpLinkCompanyId") & "') "
		else
			sSql = sSql & ") " 
		end if
			
		sSql = sSql & "AND ((VAs.BeginningEducationDate < VACs.PurchaseDate) " & _
			" OR (VAs.BeginningEducationDate IS NULL) " & _
			" OR (VACs.PurchaseDate IS NULL)) "
			
		sSql = sSql & "AND NOT VACs.Deleted = '1') " & _
			"LEFT OUTER JOIN vCourses AS VCs " & _
			"ON VCs.CourseId = VACs.CourseId " & _
			"LEFT OUTER JOIN Licenses AS Ls " & _
			"ON ((Ls.OwnerId = VAs.UserId) AND (Ls.OwnerTypeId = '1') AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL)) "
		'	"WHERE (VACs.Deleted = '0' OR VACs.Deleted IS NULL) " & _
		'	"AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL) " 
		
'		sSql = sSql & " WHERE ((VAs.BeginningEducationDate < VACs.PurchaseDate) " & _
'			" OR (VAs.BeginningEducationDate IS NULL) " & _
'			" OR (VAs.PurchaseDate IS NULL) "
'		bFirstClause = false
		
		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
'				if g_iSearchStateIdType = 1 then
'					sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
'				elseif g_iSearchStateIdType = 2 then
'					sSql = sSql & "VCs.StateId = '" & iStateId & "' "
'				else
'					sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' " & _
'						"OR VCs.StateId = '" & iStateId & "' "
'				end if
				sSql = sSql & "AAs.StateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 			


		'If License State Id List search
		if g_sSearchLicStateIdList <> "" then
			aStateIdList = split(g_sSearchLicStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 		
		
		
		'If Course State Id List search
		if g_sSearchCourseStateIdList <> "" then
			aStateIdList = split(g_sSearchCourseStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "VCs.StateId = '" & iStateId & "' "
			
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		

		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				'IncludeL2ChildrenInSql sSql, iCompanyL2Id, "VAs."
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL3Id = '" & iCompanyL3Id & "' "
				
				'IncludeL3ChildrenInSql sSql, iCompanyL3Id, "VAs."
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if
		
			
		'If CourseName search
		if g_sCourseName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.Name LIKE '%" & g_sCourseName & "%'"
		end if 
		
		
		'If CourseProviderId search
		if g_iCourseProviderId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.ProviderId = '" & g_iCourseProviderId & "'"
		end if
		
		'If CourseAccreditationTypeId search
		if g_iCourseAccreditationTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.AccreditationTypeId = '" & g_iCourseAccreditationTypeId & "'"
		end if

		'If CourseCompleted search
		if g_bCourseCompleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VACs.Completed = '" & abs(cint(g_bCourseCompleted))  & "' "
		end if					
		
		
		'If CourseCreditHours search
		if g_rCourseCreditHours <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.ContEdHours = '" & g_rCourseCreditHours & "'"
		end if
		
		
		'If CourseCompletionDateFrom search
		if g_dCourseCompletionDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VACs.CompletionDate >= '" & g_dCourseCompletionDateFrom & "'"
		end if 
		
		
		'If CourseCompletionDateTo search
		if g_dCourseCompletionDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VACs.CompletionDate <= '" & g_dCourseCompletionDateTo & "'"
		end if 
		
		
		'If CourseExpDateFrom search
		if g_dCourseExpDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "((VACs.CertificateExpirationDate >= '" & _
				g_dCourseExpDateFrom & "' AND VACs.UserSpecRenewalDate IS NULL) " & _
				"OR " & _
				"(VACs.UserSpecRenewalDate >= '" & g_dCourseExpDateFrom & "'))"
'			sSql = sSql & "(VACs.CertificateExpirationDate >= '" & _
'				g_dCourseExpDateFrom & "' OR VACs.UserSpecRenewalDate >= '" & _
'				g_dCourseExpDateFrom & "')"
		end if 
		
		
		'If CourseExpDateTo search
		if g_dCourseExpDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "((VACs.CertificateExpirationDate <= '" & _
				g_dCourseExpDateTo & "' AND VACs.UserSpecRenewalDate IS NULL) " & _
				"OR " & _
				"(VACs.UserSpecRenewalDate <= '" & g_dCourseExpDateTo & "'))"
'			sSql = sSql & "(VACs.CertificateExpirationDate <= '" & _
'				g_dCourseExpDateTo & "' OR VACs.UserSpecRenewalDate <= '" & _
'				g_dCourseExpDateTo & "')"
		end if
		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if

			sSql = sSql & "(Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
				
			'if expired search, check the expiration date also 
			if g_iLicenseStatusId = 6 then
				sSql = sSql & " or LicenseExpDate < getdate()"
			end if

			sSql = sSql & ")"
		end if
		
		'If LicenseStatusIdList search
		dim aStatusIdList
		dim iStatusId
		dim bFirstStatus
		if g_sLicenseStatusIdList <> "" then
			aStatusIdList = split(g_sLicenseStatusIdList, ",")
			bFirstStatus = true
			for each iStatusId in aStatusIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstStatus then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "(Ls.LicenseStatusId = '" & iStatusId & "'"
				
				'if expired search, check the expiration date also 
                if not IsNumeric(iStatusId) then
                    iStatusId = 0
                end if
				if iStatusId = 6 then
					sSql = sSql & " or LicenseExpDate < getdate()"
				end if

				sSql = sSql & ")"

				bFirstStatus = false
			
			next
			sSql = sSql & ")"
		end if 	
		
		
		'If LicenseNumber search
		if g_sLicenseNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNumber & "%'"
		end if
		
		'If LicensePayment search
		if g_sLicensePayment <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "VAs.LicensePayment LIKE '%" & g_sLicensePayment & "%'"
		end if
		
		'If SearchLicenseExpDateFrom/To search
		if g_dLicenseExpDateFrom <> "" or g_dLicenseExpDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_dLicenseExpDateTo <> "" then
				if g_iAppDeadline = "1" then 
					sSql = sSql & "((Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') " & _
						"OR (" & _
						"Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "')) "
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "3" then
					sSql = sSql & "(Ls.SubmissionDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.SubmissionDate <= '" & _
						g_dLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "4" then
					sSql = sSql & "(Ls.SubDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.SubDate <= '" & _
						g_dLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "5" then
					sSql = sSql & "(Ls.IssueDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.IssueDate <= '" & _
						g_dLicenseExpDateTo & "') "											
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dLicenseExpDateTo & "') "
				end if
			else
				if g_iAppDeadline = "1" then
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "' " & _
						"OR Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "')"					
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "3" then
					sSql = sSql & "(Ls.SubmissionDate >= '" & _
						g_dLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "4" then
					sSql = sSql & "(Ls.SubDate >= '" & _
						g_dLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "5" then
					sSql = sSql & "(Ls.IssueDate >= '" & _
						g_dLicenseExpDateFrom & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dLicenseExpDateFrom & "') "  
				end if
			end if
		elseif g_dLicenseExpDateTo <> "" then
			if g_iAppDeadline = "1" then
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "' " & _
					"OR Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "')"		
			elseif g_iAppDeadline = "2" then
				sSql = sSql & "(Ls.LicenseAppDeadline <= '" & _
					g_dLicenseExpDateTo & "') "
			else
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dLicenseExpDateTo & "') "
			end if 
		end if 

		
		
		'If SearchLicenseExpDateFrom/To search
'		if g_dLicenseExpDateFrom <> "" then
'			if bFirstClause then
'				sSql = sSql & " WHERE "
'				bFirstClause = false
'			else
'				sSql = sSql & " AND "
'			end if
'			if g_dLicenseExpDateTo <> "" then
'				sSql = sSql & "((Ls.LicenseExpDate >= '" & _
'					g_dLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseExpDate <= '" & _
'					g_dLicenseExpDateTo & "') " & _
'					"OR (" & _
'					"Ls.LicenseAppDeadline >= '" & _
'					g_dLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseAppDeadline <= '" & _
'					g_dLicenseExpDateTo & "')) "
'			else
'				sSql = sSql & "(Ls.LicenseExpDate >= '" & _
'					g_dLicenseExpDateFrom & "' " & _
'					"OR Ls.LicenseAppDeadline >= '" & _
'					g_dLicenseExpDateFrom & "')"					
'			end if
'		elseif g_dLicenseExpDateTo <> "" then
'			sSql = sSql & "(Ls.LicenseExpDate <= '" & _
'				g_dLicenseExpDateTo & "' " & _
'				"OR Ls.LicenseAppDeadline <= '" & _
'				g_dLicenseExpDateTo & "')"		
'		end if 
		
		
		'If LicenseExpDateFrom search
		'if g_dLicenseExpDateFrom <> "" then
		'	if bFirstClause then
		'		sSql = sSql & " WHERE "
		'		bFirstClause = false
		'	else
		'		sSql = sSql & " AND "
		'	end if
		'	sSql = sSql & "Ls.LicenseExpDate >= '" & g_dLicenseExpDateFrom & "'"
		'end if 
		
		
		'If LicenseExpDateTo search
		'if g_dLicenseExpDateTo <> "" then
		'	if bFirstClause then
		'		sSql = sSql & " WHERE "
		'		bFirstClause = false
		'	else
		'		sSql = sSql & " AND "
		'	end if
		'	sSql = sSql & "Ls.LicenseExpDate <= '" & g_dLicenseExpDateTo & "'"
		'end if 

		
		'If FirstName search
		if g_sFirstName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.FirstName LIKE '%" & g_sFirstName & "%'"
		end if 
		
		
		'If LastName search
		if g_sLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName LIKE '%" & g_sLastName & "%'" 
		end if 
		
		
		'If LastNameStart search
		if g_sSearchLastNameStart <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName LIKE '" & g_sSearchLastNameStart & "%'"
		end if
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyId = '" & g_iCompanyId & "'"
		end if 


		'If CompanyL2Id search
		if g_iCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL2Id = '" & g_iCompanyL2Id & "'"
		end if
		
		
		'If CompanyL3Id search
		if g_iCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL3Id = '" & g_iCompanyL3Id & "'"
		end if
				
		
		'If BranchId search
		if g_iBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.BranchId = '" & g_iBranchId & "'"
		end if
		
		
		'If Email search
		if g_sEmail <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Email = '" & g_sEmail & "'"
		end if
		
		
		'If SSN search
		if g_sSsn <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Ssn LIKE '%" & g_sSsn & "%'"
		end if 
		
		'If Hire Date From search
		if g_dHireDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.HireDate >= '" & g_dHireDateFrom & " 00:00'"
		end if 		
		
		'If Hire Date To search
		if g_dHireDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.HireDate <= '" & g_dHireDateTo & " 23:59'"
		end if 				
		
		'If UserStatus search
		if g_iUserStatus <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.UserStatus = '" & g_iUserStatus & "'"
		end if 
		
		
		'If Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
		
		
		'If StateId search
		if g_iSearchStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "AAs.StateId = '" & g_iSearchStateId & "'"
		end if
		
		sSql = sSql & " ORDER BY VAs.LastName, VAs.FirstName "
		'Response.Write("<p>" & sSql & "<p>")
		'Response.Write(vbCrLf & "<!-- " & sSql & " -->" & vbCrLf)
		
		set SearchAssociatesCoursesLicenses = QueryDb(sSql)
			
	end function

	' -------------------------------------------------------------------------
	' Name: SearchCourses
	' Input: p_sCourseName - Name text to search for
	'		p_iProviderId - Provider to search fo
	' Desc: Returns a recordset of the matching courses.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Returns: Recordset of CourseIds
	' -------------------------------------------------------------------------
	public function SearchCourses(p_sCourseName, p_iProviderId, p_dCompletionDateFrom, p_dCompletionDateTo, p_dExpFrom, _
		p_dExpTo, p_bCompleted, p_iAccreditationTypeID)
	
		set SearchCourses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iProviderId, true, "Provider ID") or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckDateParam(p_dCompletionDateFrom, true, "Completion Date (From)") or _
			not CheckDateParam(p_dCompletionDateTo, true, "Completion Date (To)") or _			
			not CheckDateParam(p_dExpFrom, true, "Expiration Date (From)") or _
			not CheckDateParam(p_dExpTo, true, "Expiration Date (To)") or _
            not CheckIntParam(p_iAccreditationTypeID, true, "Accreditation Type ID") then
			exit function
		end if
		
		dim sSql 'SQL statement
		
		sSql = "SELECT ACs.*, Us.*, Ps.Name as ProviderName, Ps.Address as ProviderAddress, Ps.Address2 as ProviderAddress2, Ps.City as ProviderCity, " & _
			"(SELECT Abbrev FROM States WHERE StateID = Ps.StateID) as ProviderState, Ps.Zipcode as ProviderZipcode, Ps.Phone as ProviderPhoneNo, Ps.ContactName as ProviderContactName, Ps.ContactTitle as ProviderContactTitle, Cs.InstructorName, Cs.InstructorEducationLevel FROM tmp_vAssociatesCourses AS ACs " & _
			"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
			"LEFT JOIN Providers AS Ps ON (Ps.ProviderID = ACs.ProviderID) " & _
			"LEFT JOIN Courses AS Cs ON (Cs.CourseID = ACs.CourseID) " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if
		
		sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) " & _
			" OR (Us.BeginningEducationDate IS NULL) " & _
			" OR (ACs.PurchaseDate IS NULL)) "
		
		if p_iProviderId <> "" then 
			sSql = sSql & "AND ACs.ProviderId = '" & p_iProviderId & "' "
		end if
		
		if p_sCourseName <> "" then
			sSql = sSql & "AND ACs.Name LIKE '%" & p_sCourseName & "%' "
		end if
		
		if p_dCompletionDateFrom <> "" then
			sSql = sSql & "AND ACs.CompletionDate >= '" & p_dCompletionDateFrom & "' "
		end if
		
		if p_dCompletionDateTo <> "" then
			sSql = sSql & "AND ACs.CompletionDate <= '" & p_dCompletionDateTo & "' "
		end if
		
		if p_dExpFrom <> "" then
			sSql = sSql & "AND ((ACs.CertificateExpirationDate >= '" & _
				p_dExpFrom & "' AND ACs.UserSpecRenewalDate IS NULL) " & _
				"OR " & _
				"(ACs.UserSpecRenewalDate >= '" & p_dExpFrom & "'))"
'			sSql = sSql & "AND (ACs.CertificateExpirationDate >= '" & _
'				p_dExpFrom & "' OR ACs.UserSpecRenewalDate >= '" & _
'				p_dExpFrom & "')"			
		end if
		
		if p_dExpTo <> "" then
			sSql = sSql & "AND ((ACs.CertificateExpirationDate <= '" & _
				p_dExpTo & "' AND ACs.UserSpecRenewalDate IS NULL) " & _
				"OR " & _
				"(ACs.UserSpecRenewalDate <= '" & p_dExpTo & "'))"
'			sSql = sSql & "AND (ACs.CertificateExpirationDate <= '" & _
'				p_dExpTo & "' OR ACs.UserSpecRenewalDate <= '" & _
'				p_dExpTo & "')"
		end if
		
		if p_bCompleted <> "" then
			sSql = sSql & "AND ACs.Completed = '" & abs(cint(p_bCompleted)) & "' "
		end if
		
		'Response.Write("<p>" & sSql & "</p>")
		
		set SearchCourses = QueryDb(sSql)
		
	end function		
	
	' -------------------------------------------------------------------------
	' Name: SetOfficerCustField
	' Desc: Returns the value of the specified OfficerCustField
	' Preconditions: none
	' Inputs: p_iNo - the number of the Officer Custom Field
	'		  p_sValue - the value to set the Officer Custom Field to
	' Returns: n/a
	' -------------------------------------------------------------------------
	public sub SetOfficerCustField(p_iNo,p_sValue)
		if not checkintparam(p_iNo, false, "Number") then
			exit sub
		end if
		
		select case p_iNo
			case 1
				g_sOfficerCustField1 = p_sValue
			case 2
				g_sOfficerCustField2 = p_sValue
			case 3
				g_sOfficerCustField3 = p_sValue
			case 4
				g_sOfficerCustField4 = p_sValue
			case 5
				g_sOfficerCustField5 = p_sValue
			case 6
				g_sOfficerCustField6 = p_sValue
			case 7
				g_sOfficerCustField7 = p_sValue
			case 8
				g_sOfficerCustField8 = p_sValue
			case 9
				g_sOfficerCustField9 = p_sValue
			case 10
				g_sOfficerCustField10 = p_sValue
			case 11
				g_sOfficerCustField11 = p_sValue
			case 12
				g_sOfficerCustField12 = p_sValue
			case 13
				g_sOfficerCustField13 = p_sValue
			case 14
				g_sOfficerCustField14 = p_sValue
			case 15
				g_sOfficerCustField15 = p_sValue
		end select
	end sub
	
	' -------------------------------------------------------------------------
	' Name: UpdateCourse
	' Desc: Updates a course association with the passed information.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId, CourseId, ExpDate, CompletionDate
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function UpdateCourse(p_iAssocId, p_iCourseId, p_dExpDate, p_dCompletionDate)
	
		UpdateCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Association ID") or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckDateParam(p_dExpDate, true, "Renewal Date") or _
			not CheckDateParam(p_dCompletionDate, true, "Completion Date") _
			then
			exit function
		end if 
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE AssociatesCourses SET " & _
			"CourseId = '" & p_iCourseId & "', "
		if p_dExpDate <> "" then
			sSql = sSql & "CourseExpirationDate = '" & p_dExpDate & "', "
		else
			sSql = sSql & "CourseExpirationDate = NULL, "
		end if 
		if p_dCompletionDate <> "" then
			sSql = sSql & "CompletionDate = '" & p_dCompletionDate & "', " & _
				"Completed = '1' " 
		else
			sSql = sSql & "CompletionDate = NULL, " & _
				"Completed = '0' "
		end if
		sSql = sSql & "WHERE Id = '" & p_iAssocId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to update the course record.")
		
		else
			
			UpdateCourse = 1
			
		end if
		
		oConn.Close
		set oConn = nothing
		
	end function
		
	' -------------------------------------------------------------------------
	' Name: UpdateTpCourse
	' Desc: Updates a TP course association with the passed information.  This
	'	action stores supplementary data on the CK database - it does not alter
	'	any information on TrainingPro.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: TpUserCourseId, CkRenewalDate
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function UpdateTpCourse(p_iTpUserCourseId, p_dCkRenewalDate)
	
		UpdateTpCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iTpUserCourseId, false, "UserCourse ID") or _
			not CheckDateParam(p_dCkRenewalDate, true, "Renewal Date") then
			exit function
		end if 
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "DELETE FROM TpUsersCoursesDates " & _
			"WHERE UserCourseId = '" & p_iTpUserCourseId & "'"
		oConn.Execute sSql, lRecordsAffected
		
		sSql = "INSERT INTO TpUsersCoursesDates (" & _
			"UserCourseId, " & _
			"CkRenewalDate " & _
			") VALUES (" & _
			"'" & p_iTpUserCourseId & "', "
		if p_dCkRenewalDate <> "" then
			sSql = sSql & "'" & p_dCkRenewalDate & "'"
		else
			sSql = sSql & "NULL"
		end if
		sSql = sSql & ")"
		oConn.Execute sSql, lRecordsAffected
			
		
'		sSql = "UPDATE TpUsersCoursesDates SET "
'		if p_dCkRenewalDate <> "" then
'			sSql = sSql & "CkRenewalDate = '" & p_dCkRenewalDate & "' "
'		else
'			sSql = sSql & "CkRenewalDate = NULL "
'		end if
'		sSql = sSql & "WHERE UserCourseId = '" & p_iTpUserCourseId & "'"
'		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to update the course record.")
		
		else
			
			UpdateTpCourse = 1
			
		end if
		
		oConn.Close
		set oConn = nothing
		
	end function

	' -------------------------------------------------------------------------
	' Name: VerifyUniqueEmail
	' Desc: Checks to see if any other user from the same company has the same
	'	email address.
	' Preconditions: ConnectionString, UserId, CompanyId
	' Inputs: p_sEmail - Email address to check for
	' Returns: True/False
	' -------------------------------------------------------------------------
	public function VerifyUniqueEmail(p_sEmail)
	
		VerifyUniqueEmail = false
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, true, "User ID") or _
			not CheckIntParam(g_iCompanyId, false, "Company ID") or _
			not CheckStrParam(p_sEmail, false, "Email") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT UserId, Email FROM Associates  WHERE UserStatus = '1' " & _
			"AND CompanyId = '" & g_iCompanyId & "' AND Email LIKE '" & p_sEmail & "' "
		
		if g_iUserId <> "" then
			sSql = sSql & "AND NOT UserId = '" & g_iUserId & "' "
		end if
		
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
			VerifyUniqueEmail = true
		end if
		
		set oRs = nothing
		
	end function
		




    ' -------------------------------------------------------------------------
    ' Name: VerifyUniqueNMLS
    ' Desc: Checks to see if any other user from the same company has the same
    '	NMLSNumber.
    ' Preconditions: ConnectionString, UserId, CompanyId
    ' Inputs: p_sEmail - Email address to check for
    ' Returns: True/False
    ' -------------------------------------------------------------------------
    public function VerifyUniqueNMLS(p_sNMLSNumber)
	
		VerifyUniqueNMLS = false

        if p_sNMLSNumber = "" then
            VerifyUniqueNMLS = true 'Allow it to be blank
            exit function
        end if

		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, true, "User ID") or _
			not CheckIntParam(g_iCompanyId, false, "Company ID") or _
			not CheckStrParam(p_sNMLSNumber, false, "NMLSNumber") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT UserId, NMLSNumber  FROM Associates  WHERE UserStatus = '1' " & _
			"AND CompanyId = '" & g_iCompanyId & "' AND NMLSNumber = '" & p_sNMLSNumber & "' "
		
		if g_iUserId <> "" then
			sSql = sSql & "AND NOT UserId = '" & g_iUserId & "' "
		end if
		
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
			VerifyUniqueNMLS = true
		end if
		
		set oRs = nothing
		
	end function
		
end class



'==============================================================================
' Class: AssociateAddress
' Purpose: Class used for retrieving and storing loan officer Address information
' -------------------
'
' Properties:
'	- AddressLine1
'	- AddressLine2
'	- City
'	- StateID
'	- Zipcode
'	- ZipcodeExt
'
'==============================================================================
class AssociateAddress

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	dim g_sAddressLine1
	dim g_sAddressLine2
	dim g_sCity
	dim g_iStateID
	dim g_sZipcode
	dim g_sZipcodeExt
	
	' PUBLIC PROPERTIES =======================================================
	
	'Address line 1
	public property get AddressLine1()
		AddressLine1 = g_sAddressLine1
	end property
	public property let AddressLine1(p_sAddressLine1)
		g_sAddressLine1 = left(trim(p_sAddressLine1), 100)
	end property
	
	'Address line 2
	public property get AddressLine2()
		AddressLine2 = g_sAddressLine2
	end property
	public property let AddressLine2(p_sAddressLine2)
		g_sAddressLine2 = left(trim(p_sAddressLine2), 100)
	end property

	'City
	public property get City()
		City = g_sCity
	end property
	public property let City(p_sCity)
		g_sCity = left(trim(p_sCity), 50)
	end property
	
	'State index
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then
			g_iStateId = cint(p_iStateId)
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property

	'Zipcode
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	public property let Zipcode(p_sZipcode)
		g_sZipcode = left(trim(p_sZipcode), 10)
	end property
	
	'Optional zipcode extension
	public property get ZipcodeExt()
		ZipcodeExt = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		if len(p_sZipcodeExt) >= 4 then
			g_sZipcodeExt = left(p_sZipcodeExt, 4)
		else
			g_sZipcodeExt = ""
		end if
	end property	
	
end class
%>
<%
'==============================================================================
' Class: Group
' Controls the creation, modification, and removal of Groups.
' Create Date: 7/08/13
' Author: David Fulford
' -------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
'	- GroupId
'	- GroupName
'	- AssociateID
'   - Deleted
'   - CompanyID
' Methods:
'	- LoadGroupById
'	- SaveGroup
'	- DeleteGroup
'	- DeleteGroupById
'	- SearchGroups
'   - GetEmployees
'   - GetLicenses
'   - AddEmployee
'==============================================================================

Class Group

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Base group properties
	dim g_iGroupId
	dim g_sGroupName
	dim g_iAssociateID
    dim g_sAssociateName
    dim g_bDeleted
	dim g_iCompanyID

    dim g_sEmployeeLastNameSearch
    dim g_iAssociateTypeIDSearch
    dim g_sLicenseNumSearch

	' PUBLIC PROPERTIES =======================================================
	
	'Stores/retrieves the database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Stores/retrieves the trainingpro database connection string
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
		
	'Retrieves the unique Id of this group.  
	public property get GroupId()
		GroupId = g_iGroupId
	end property
	public property let GroupId(p_iGroupId)
		if isnumeric(p_iGroupId) then
			g_iGroupId = cint(p_iGroupId)
		elseif p_iGroupId = "" then
			g_iGroupId = ""
		else
			ReportError("Invalid GroupId value.  Integer required.")
		end if
	end property
	
	
	'Stores/retrieves the group name
	public property get GroupName()
		GroupName = g_sGroupName
	end property
	public property let GroupName(p_sGroupName)
		g_sGroupName = left(p_sGroupName, 50)
	end property
	
	
	'The ID of the group leader
	public property let AssociateID(p_iAssociateID)
		g_iAssociateID = p_iAssociateID
	end property
	public property get AssociateID()
		AssociateID = g_iAssociateID
	end property
	
    'The Name of the group leader
    'read-only
    public property get AssociateName()
        AssociateName = g_sAssociateName
    end property

    'Whether the item has been deleted or not
    public property let Deleted(p_bDeleted)
        g_bDeleted = p_bDeleted
    end property
    public property get Deleted()
        Deleted = g_bDeleted
    end property

	'The ID of the associated company
	public property let CompanyID(p_iCompanyID)
		g_iCompanyID = p_iCompanyID
	end property
	public property get CompanyID()
		CompanyID = g_iCompanyID
	end property

	'The ID of the associated company
	public property let EmployeeLastNameSearch(p_sEmployeeLastNameSearch)
		g_sEmployeeLastNameSearch = p_sEmployeeLastNameSearch
	end property
	public property get EmployeeLastNameSearch()
		EmployeeLastNameSearch = g_sEmployeeLastNameSearch
	end property

	'The ID of the associated company
	public property let AssociateTypeIDSearch(p_iAssociateTypeIDSearch)
		g_iAssociateTypeIDSearch = p_iAssociateTypeIDSearch
	end property
	public property get AssociateTypeIDSearch()
		AssociateTypeIDSearch = g_iAssociateTypeIDSearch
	end property

	'The ID of the associated company
	public property let LicenseNumSearch(p_sLicenseNumSearch)
		g_sLicenseNumSearch = p_sLicenseNumSearch
	end property
	public property get LicenseNumSearch()
		LicenseNumSearch = g_sLicenseNumSearch
	end property

	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'nothing

	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'nothing
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		CheckConnectionString = false
	
		if isnull(g_sConnectionString) or _
			g_sConnectionString = "" then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) or _
			g_sTpConnectionString = "" then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Mark the specified Group entry as deleted
	' Preconditions: Connectionstring
	' Inputs: p_iGroupId - Group ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iGroupId)
	
		DeleteId = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iGroupId, false, "Group ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iGroupId > 0 then
			
			oConn.ConnectionString = g_sTpConnectionString
			oConn.Open
			
			'Mark the group as deleted
			sSql = "UPDATE Groups SET " & _
				"Deleted = '1' " & _
				"WHERE GroupId = '" & p_iGroupId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the group.")
				exit function
				
			end if
			
		end if
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadGroupById
	' Description: Loads a group's information into the group object
	' Preconditions: ConnectionString
	' Inputs: p_iGroupId - ID of the group to load
	' Returns: Returns the Group ID if successful, otherwise 0
	' -------------------------------------------------------------------------
	public function LoadGroupById(p_iGroupId)
		
		LoadGroupById = 0
		
		'Verify preconditions/inputs
		if not CheckConnectionString then 
			exit function
		elseif not CheckIntParam(p_iGroupId, false, "Group ID") then
			exit function
		end if
		
		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected
		
		'set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if p_iGroupId > 0 then
            sSql = "SELECT Groups.*, Associates.LastName, Associates.FirstName FROM Groups " & _
                   "LEFT JOIN Associates ON Groups.AssociateId = Associates.UserId " & _
                   "WHERE Groups.GroupId = '" & p_iGroupId & "'"
            set oRs = QueryDb(sSql)
        end if
		
				
		if not (oRs.BOF and oRs.EOF) then
			
			g_iGroupId = oRs("GroupId")
			g_sGroupName = oRs("GroupName")
			g_iAssociateId = oRs("AssociateId")
            if g_iAssociateId <> "" then
                g_sAssociateName = oRs("LastName") & ", " & oRs("FirstName")
            end if
			g_bDeleted = oRs("Deleted")
            g_iCompanyID = oRs("CompanyID")

			LoadGroupById = g_iGroupId

		else
		
			ReportError("Unable to load Group.  Group ID not found.")
			exit function
			
		end if 
		
		
		'set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveGroup
	' Desc: Save the group object in the database, and assign it the resulting
	'	ID. 
	' Preconditions: Connectionstring
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveGroup()
	
		SaveGroup = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sGroupName, false, "Group Name") or _
            not CheckIntParam(g_iCompanyID, false, "Company ID") then
			exit function
		end if 
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if g_iGroupId = "" then
		
			'This is a new group being added to CMS, so we'll INSERT a new
			'record
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "INSERT INTO Groups (" & _
				"GroupName, " & _
				"AssociateID, " & _
				"Deleted, " & _
                "CompanyID " & _
				") VALUES (" & _
				"'" & g_sGroupName & "', "

                if g_iAssociateID <> "" then
                    sSql = sSql & "'" & g_iAssociateID & "', "
                else
                    sSql = sSql & "NULL, "
                end if

				sSql = sSql & "'" & g_bDeleted & "', " & _
                "'" & g_iCompanyID & "' " & _
				")"

			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to create new Group.")
				exit function
			
			end if
			
			'Attempt to retrieve the index of the newly created group.
			sSql = "SELECT MAX(GroupId) AS GroupID FROM Groups " & _
				"WHERE GroupName LIKE '" & g_sGroupName & "' "
			set oRs = oConn.Execute(sSql)
			
			if oRs.EOF then 
			
				ReportError("Failed to create new Group.")
				exit function
				
			else
			
				g_iGroupId = oRs("GroupId")
				SaveGroup = g_iGroupId
				
			end if
			
		else
		
			'This is an update to an existing group.
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			sSql = "UPDATE Groups SET " & _
				"GroupName = '" & g_sGroupName & "', "
                
                if g_iAssociateID <> "" then
                    sSql = sSql & "AssociateID = '" & g_iAssociateID & "', "
                else
                    sSql = sSql & "AssociateID = NULL, "
                end if

				sSql = sSql & "Deleted = '" & g_bDeleted & "', " & _
                "CompanyID = '" & g_iCompanyID & "' " & _
				"WHERE GroupId = '" & g_iGroupId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected = 1 then
				
				ReportError("Failed to save group information.")
				exit function
				
			end if
			
			SaveGroup = g_iGroupId
			
		end if 
		
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: ReleaseGroup
	' Desc: Clears set/loaded Group properties
	' Preconditions: ConnectionString
	' -------------------------------------------------------------------------
	public sub ReleaseGroup()
	
		g_iGroupId = ""
		g_sGroupName = ""
		g_iAssociateID = ""
		g_bDeleted = ""
        g_iCompanyID = ""
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: SearchGroups
	' Desc: Retrieves a collection of GroupIds that match the properties of 
	'	the current object.
	' Preconditions: ConnectionString
	' Returns: Returns the resulting recordset
	' -------------------------------------------------------------------------
	public function SearchGroups()
		
		set SearchGroups = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		dim bFirstClause 'Used to direct SQL phrasing
		dim sSql 'SQL Statement
		dim oRs 'Recordset object

		bFirstClause = true
		
		sSql = "SELECT Groups.*, Associates.LastName, Associates.FirstName " & _
			"FROM Groups " & _
            "LEFT JOIN Associates ON Groups.AssociateID = Associates.UserID "

	
		'If Group Name search
		if g_sGroupName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Groups.GroupName LIKE '%" & g_sGroupName & "%'"
		end if 
		
		
		'If Group AssociateID search
		if g_iAssociateID <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Groups.AssociateID = '" & g_iAssociateID & "'"
		end if 
		
		
		'If Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Groups.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if 

        'If Company search
        if g_iCompanyID <> "" then
        	if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Groups.CompanyID = '" & g_iCompanyID & "'"
		end if 

		sSql = sSql & " ORDER BY GroupName"

		set SearchGroups = QueryDb(sSql)

	end function

    ' -------------------------------------------------------------------------
	' Name: GetEmployees
	' Desc: Returns a recordset of the employees assigned to this group.
	' Preconditions: ConnectionString, GroupId
	' Returns: Recordset of EmployeeIds
	' -------------------------------------------------------------------------
	public function GetEmployees()
	
		set GetEmployees = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iGroupId, false, "Group ID") then		
			exit function
		end if
		
		dim sSql 'SQL Statement
		sSql = "SELECT GroupsEmployees.*, Associates.*, AssociateTypeName FROM GroupsEmployees " & _
			"LEFT JOIN Associates ON GroupsEmployees.EmployeeID = Associates.UserID " & _
            "LEFT JOIN AssociateType ON Associates.AssociateTypeID = AssociateType.AssociateTypeID " & _
			"WHERE NOT GroupsEmployees.Deleted = '1' " & _
			"AND GroupsEmployees.GroupId = '" & g_iGroupId & "' " & _	
            "AND Associates.CompanyId = '" & g_iCompanyId & "' "
		
        if g_sEmployeeLastNameSearch <> "" then
            sSql = sSql & "AND Associates.LastName LIKE '%" & g_sEmployeeLastNameSearch & "%'"
        end if
        
        if g_iAssociateTypeIDSearch <> "" then
            sSql = sSql & "AND Associates.AssociateTypeID = '" & g_iAssociateTypeIDSearch & "'"
        end if

        sSql = sSql & "ORDER BY Associates.LastName, Associates.FirstName "
				
		set GetEmployees = QueryDb(sSql)
		
	end function

    ' -------------------------------------------------------------------------
	' Name: GetLicenses
	' Desc: Returns a recordset of the licenses assigned to this group.
	' Preconditions: ConnectionString, GroupId
	' Returns: Recordset of CourseIds
	' -------------------------------------------------------------------------
	public function GetLicenses()
	
		set GetLicenses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iGroupId, false, "Group ID") then		
			exit function
		end if
		
		dim sSql 'SQL Statement
		
		sSql = "SELECT * FROM GroupsLicenses " & _
			"LEFT JOIN Licenses ON (GroupsLicenses.LicenseID = Licenses.LicenseID) " & _
			"WHERE NOT GroupsLicenses.Deleted = '1' " & _
			"AND GroupsLicenses.GroupId = '" & g_iGroupId & "' "
        
        if g_sLicenseNumSearch <> "" then
            sSql = sSql & "AND LicenseNum LIKE '%" & g_sLicenseNumSearch & "%'"
        end if

		sSql = sSql & "ORDER BY LicenseStateId, LicenseNum, LicenseExpDate "
				
		set GetLicenses = QueryDb(sSql)
		
	end function
	
    ' -------------------------------------------------------------------------
	' Name: AddEmployee
	' Desc: Adds an employee to the specified group
	' Preconditions: ConnectionString, GroupId, AssociateID
	' Returns: Id of the newly created GroupsEmployees record
	' -------------------------------------------------------------------------
	public function AddEmployee(p_iAssociateID)
	
		set AddEmployee = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iGroupId, false, "Group ID") or _
            not CheckIntParam(p_iAssociateID, false, "Employee ID") then		
			exit function
		end if

        dim oConn		
        set oConn = Server.CreateObject("ADODB.Connection")

        dim lRecordsAffected

		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		dim sSql 'SQL Statement
		
        dim oRs
        
        sSql = "SELECT COUNT(*) AS count FROM GroupsEmployees WHERE GroupID = '" & g_iGroupID & "'" & _
               "AND EmployeeID = '" & p_iAssociateID & "' AND Deleted = 0"

        set oRs = oConn.Execute(sSql)

        if oRs("count") > 0 then
            ReportError("Unable to assign. Employee is already assigned to this group.")
            AddEmployee = -1
            exit function
        end if

        set oRs = nothing

		sSql = "INSERT INTO GroupsEmployees (GroupID, EmployeeID, Deleted) VALUES (" & _
			g_iGroupID & ", " & p_iAssociateID & ", 0) " 
		
		oConn.Execute sSql, lRecordsAffected
			
		if not lRecordsAffected > 0 then
			
			ReportError("Failed to create new Group Employee.")
			exit function
			
		end if
		
        ssql = "SELECT MAX(Id) FROM GroupsEmployees"

        set oRs = oConn.Execute(sSql)
			
		if oRs.EOF then 
			ReportError("Failed to create new Employee.")
			exit function
		else
			AddEmployee = g_iGroupId
		end if
	end function

    ' -------------------------------------------------------------------------
	' Name: DeleteEmployee
	' Desc: Deleted an employee from the specified group
	' Preconditions: ConnectionString, GroupsEmployeesId
	' Returns: If successful 1, otherwise 0
	' -------------------------------------------------------------------------
	public function DeleteEmployee(p_iGroupsEmployeesId)
	
		DeleteEmployee = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iGroupsEmployeesId, false, "Group Employee ID") then		
			exit function
		end if

        dim oConn		
        set oConn = Server.CreateObject("ADODB.Connection")

        dim lRecordsAffected

		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		dim sSql 'SQL Statement
		
		sSql = "UPDATE GroupsEmployees SET " & _
			"Deleted = '1' " & _
			"WHERE Id = '" & p_iGroupsEmployeesId & "'"
		oConn.Execute sSql, lRecordsAffected
			
		'Verify that at least one record was affected
		if not lRecordsAffected > 0 then
				
			ReportError("Failure deleting the Employee.")
			exit function
				
		end if

        DeleteEmployee = 1
	end function

    ' -------------------------------------------------------------------------
	' Name: AddLicense
	' Desc: Adds a license to the specified group
	' Preconditions: ConnectionString, GroupId, LicenseID
	' Returns: Id of the newly created GroupsLicenses record
	' -------------------------------------------------------------------------
	public function AddLicense(p_iLicenseID)
	
		set AddLicense = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iGroupId, false, "Group ID") or _
            not CheckIntParam(p_iLicenseID, false, "License ID") then		
			exit function
		end if

        dim oConn		
        set oConn = Server.CreateObject("ADODB.Connection")

        dim lRecordsAffected

		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		dim sSql 'SQL Statement
		
		sSql = "INSERT INTO GroupsLicenses (GroupID, LicenseID, Deleted) VALUES (" & _
			g_iGroupID & ", " & p_iLicenseID & ", 0) " 
		
		oConn.Execute sSql, lRecordsAffected
			
		if not lRecordsAffected > 0 then
			
			ReportError("Failed to create new Group License.")
			exit function
			
		end if
		
        ssql = "SELECT MAX(Id) FROM GroupsLicenses"

        dim oRs

        set oRs = oConn.Execute(sSql)
			
		if oRs.EOF then 
			ReportError("Failed to create new License.")
			exit function
		else
			AddLicense = g_iGroupId
		end if
	end function

    ' -------------------------------------------------------------------------
	' Name: DeleteLicense
	' Desc: Deletes a license from the specified group
	' Preconditions: ConnectionString, GroupsLicensesId
	' Returns: If successful 1, otherwise 0
	' -------------------------------------------------------------------------
	public function DeleteLicense(p_iGroupsLicensesId)
	
		DeleteLicense = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iGroupsLicensesId, false, "Group License ID") then		
			exit function
		end if

        dim oConn		
        set oConn = Server.CreateObject("ADODB.Connection")

        dim lRecordsAffected

		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		dim sSql 'SQL Statement
		
		sSql = "UPDATE GroupsLicenses SET " & _
			"Deleted = '1' " & _
			"WHERE Id = '" & p_iGroupsLicensesId & "'"
		oConn.Execute sSql, lRecordsAffected
			
		'Verify that at least one record was affected
		if not lRecordsAffected > 0 then
				
			ReportError("Failure deleting the license.")
			exit function
				
		end if

        DeleteLicense = 1
	end function
end class
%>
		
		

<%
' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True			' Template Constant
	const SHOW_MENUS = True					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	const SECONDARY_NAV_FILE_NAME = "admin"		' Maryland State Fair Template Constant
	const FAIR_SECTION = "admin"				' Maryland State Fair Template Constant
	const MENU_NUMBER = 1
	dim iPage
	dim sPgeTitle							' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Officer Admin"

server.ScriptTimeout = 6000


'Verify that the user has logged in.
CheckIsLoggedIn()
afxsecCheckDirectoryAccess()
afxsecCheckPageAccess()


dim bIsBranchAdmin
bIsBranchAdmin = CheckIsBranchAdmin


'Configure the administration submenu options
bAdminSubmenu = true

dim pageHeaderText

if Request("URL") = "/branches/addBranchesproc.asp" then
    sAdminSubmenuType = "BRANCHES"
    pageHeaderText = "Branches"    
else
    sAdminSubmenuType = "OFFICERS"
    pageHeaderText = "Loan Officers"
end if
	

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
//--></script>

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------


'Course information variables
dim sFirstName
dim sMiddleName
dim sLastName
dim sSsn
dim dDateOfBirth
dim sNMLSNumber
dim iDriversLicenseStateId
dim sDriversLicenseNo
dim iBranchId
dim iCompanyL2Id
dim iCompanyL3Id
dim dHireDate
dim dTerminationDate
dim sManager
dim sEmployeeId
dim sTitle
dim sDepartment
dim sEmail
dim sPhone
dim sPhoneExt
dim sPhone2
dim	sPhone2Ext
dim sPhone3
dim sPhone3Ext
dim sFax
dim sAddress
dim sAddress2
dim sCity
dim iStateId
dim sZipcode
dim sZipcodeExt
dim sCellPhone
dim sHomePhone
dim sHomeEmail
dim sBranchNumber
dim sLegalName
dim iIncorpStateId
dim dIncorpDate
dim iOrgTypeId
dim sEin
dim sEntityLicNum
'dim sFhaLicenseNum
dim sFhaBranchId
dim sVaBranchId

dim aOfficerCustField(14)
dim iCurField 

dim	sMailingAddress
dim	sMailingAddress2
dim	sMailingCity
dim	iMailingStateId
dim	sMailingZipcode
dim	sMailingZipcodeExt
dim	sContactName
dim	sContactPhone
dim	sContactPhoneExt
dim	sContactEmail
dim	sContactFax



dim sLicenseNum
dim iLicenseStateId
dim iLicenseTypeId
dim sLicenseType
dim iLicenseStatusId
dim dLicenseExpDate
dim dLicenseAppDeadline
dim iLicenseOwnerTypeId
dim iLicenseOwnerId
dim sLicenseOwnerName
dim sLicenseNotes

dim iProviderId
dim sCourseName
dim rCourseHours
dim dCompletionDate
dim dExpDate
dim i
dim iAssocId
dim iSuccess
dim sAction

'We need to find and parse the binary information uploaded
dim iBinFormData
dim sFormData
dim iCount


'Used to seach the temp table
dim sSQL2 
dim hasActive
dim onActive  
dim foundActive
dim NoUpdate

iBinFormData = Request.BinaryRead(Request.TotalBytes)
	
for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next

	
dim sFormArray
sFormArray = split(sFormData, "-----------------------------")
	
dim sName
dim sValue
dim iLocStart
dim iLocEnd
	
dim sForm()
	
dim sCSVLines
dim sCSVSingleLine
dim sCSVData
dim iLineCount
dim iCountCols

for iCount = 1 to lenb(iBinFormData)
	
	sFormData = sFormData & chr(ascb(midb(iBinFormData, iCount, 1)))
		
next
	
dim oRegExp
dim sRegExpMatches
dim oMatch
set oRegExp = new RegExp
oRegExp.Pattern = ",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))"

'Response.Write("<p>" & oRegExp.Pattern & "<p>")

oRegExp.IgnoreCase = true
oRegExp.Global = true


'dim iCountCols
dim iCharCount
dim sCurrentField
dim sCurrentChar
dim bInQuotes

iCountCols = 0
iCharCount = 0

'Loop through the array of form values to parse out the data.
for iCount = 0 to ubound(sFormArray)
	
	'Response.Write(sFormArray(iCount) & "<p><br>")
	if instr(1, sFormArray(iCount), "name=") > 0 then
		'Response.Write(mid(sFormArray(iCount), instr(1, sFormArray(iCount), "name=")) & "<br>")
		iLocStart = instr(1, sFormArray(iCount), "name=""") + 6
		iLocEnd = instr(iLocStart, sFormArray(iCount), """")
		sName = mid(sFormArray(iCount), iLocStart, iLocEnd - iLocStart)
			
		'Find the CSV file within the data
		if sName = "AddUserCSV" then 
			
			'+28 accounts for mime type text and two CR/LF pairs.
			sValue = mid(sFormArray(iCount), instr(1, sFormArray(iCount), "Content-Type: ") + 14)
			
            'response.write("X" & sValue & "X, " & instr(1, sValue, vbCrLf) & "<br>")

			sValue = mid(sValue, instr(1, sValue, vbCrLf) + 4)
			sValue = left(sValue, len(sValue) - 2)
			
            'Response.Write("'" & sName & "' : '" & sValue & "'<p>")
				
			sCSVLines = split(sValue, vbCrLf)
				
			redim sCSVData(43, ubound(sCSVLines))
				
			for iLineCount = 0 to ubound(sCSVLines)

				if len(sCsvLines(iLineCount)) > 5 and mid(sCsvLines(iLineCount), 1, 3) <> ",,," and mid(sCsvLines(iLineCount), 1, 10) <> "First Name" and mid(sCsvLines(iLineCount), 1, 24) <> "License Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Course Identifier Field" and mid(sCsvLines(iLineCount), 1, 24) <> "Company Identifier Field" and mid(sCsvLines(iLineCount), 1, 25) <> "Division Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Region Identifier Field" and mid(sCsvLines(iLineCount), 1, 23) <> "Branch Identifier Field" then
			
					'Parse through the line and split at commas or quotation marks.
					for iCharCount = 1 to len(sCSVLines(iLineCount))
				
						sCurrentChar = mid(sCsvLines(iLineCount), iCharCount, 1)
						if sCurrentChar = """" then
							if bInQuotes then
								bInQuotes = false
							else
								bInQuotes = true
							end if
						elseif (sCurrentChar = "," and not bInQuotes) or sCurrentChar = chr(10) then
							'We're done with a field.  Wrap it up.
							
							'Response.Write(sCurrentField & "(" & iCountCols & ")" & "|")
							
							sCSVData(iCountCols, iLineCount) = sCurrentField
							
							iCountCols = iCountCols + 1	
							sCurrentField = ""	
						else
							sCurrentField = sCurrentField & mid(sCsvLines(iLineCount), iCharCount, 1)
						end if					
					
					next
					
					'Wrap up the leftovers...
					'Response.Write(sCurrentField & "X|X")
					sCsvData(iCountCols, iLineCount) = sCurrentField
					iCountCols = 0
					sCurrentField = ""
				
				else				
					'Response.Write("Making Blank Line...<br>")
					
					'We'll get here if we get some line in our CSV that's not in
					'the correct format.  Rather than bother with removing just
					'that line from the array, we'll just make a blank entry that
					'we can ignore later.
					sCsvData(0, iLineCount) = ""
					sCsvData(1, iLineCount) = ""
					sCsvData(2, iLineCount) = ""
					sCsvData(3, iLineCount) = ""
					sCsvData(4, iLineCount) = ""
					sCsvData(5, iLineCount) = ""
					sCsvData(6, iLineCount) = ""
					sCsvData(7, iLineCount) = ""
					sCsvData(8, iLineCount) = ""
					sCsvData(9, iLineCount) = ""
					sCsvData(10, iLineCount) = ""
					sCsvData(11, iLineCount) = ""
					sCsvData(12, iLineCount) = ""
					sCsvData(13, iLineCount) = ""
					sCsvData(14, iLineCount) = ""
					sCsvData(15, iLineCount) = ""
					sCsvData(16, iLineCount) = ""
					sCsvData(17, iLineCount) = ""
					sCsvData(18, iLineCount) = ""
					sCsvData(19, iLineCount) = ""
					sCsvData(20, iLineCount) = ""
					sCsvData(21, iLineCount) = ""
					sCsvData(22, iLineCount) = ""
					sCsvData(23, iLineCount) = ""
					sCsvData(24, iLineCount) = ""
					sCsvData(25, iLineCount) = ""
					sCsvData(26, iLineCount) = ""
					sCsvData(27, iLineCount) = ""
					sCsvData(28, iLineCount) = ""
					sCsvData(29, iLineCount) = ""
					sCsvData(30, iLineCount) = ""
					sCsvData(31, iLineCount) = ""
					sCsvData(32, iLineCount) = ""
					sCsvData(33, iLineCount) = ""
					sCsvData(34, iLineCount) = ""
					sCsvData(35, iLineCount) = ""
					sCsvData(36, iLineCount) = ""
					sCsvData(37, iLineCount) = ""
					sCsvData(38, iLineCount) = ""
					sCsvData(39, iLineCount) = ""
					sCsvData(40, iLineCount) = ""
					sCsvData(41, iLineCount) = ""
					sCsvData(42, iLineCount) = ""
					sCsvData(43, iLineCount) = ""
				end if
				
				'Response.Write("<P>")
				
			next 
			'Response.end
			
		else

			sValue = trim(mid(sFormArray(iCount), iLocEnd + 1))
			sValue = mid(left(sValue, len(sValue) - 2), 5)
			
            'Response.Write("'" & sName & "' : '" & sValue & "' : " & len(sValue) & "<p>")
			
            redim preserve sForm(2, iCount + 1)
			sForm(0, iCount) = sName
			sForm(1, iCount) = sValue
			
		end if
						
	end if
				
next


dim iCompanyId
iCompanyId = session("UserCompanyId") 'ScrubForSQL(request("CompanyId"))

dim oCompany
dim saRS
dim sMode
dim iBusinessType

set oCompany = new Company
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")
oCompany.VocalErrors = false 'application("bVocalErrors")

if oCompany.LoadCompanyById(iCompanyId) = 0 then
	
	'The load was unsuccessful.  End.
	Response.write("Failed to load the passed Company ID.")
	Response.end
		
end if 


%>
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
					
			<!-- Main table -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
                                                                                    
					<td class="sectiontitle"><img src="/media/images/navIcon-officers.gif" width="17" alt="Officers" align="absmiddle" vspace="10"> <%= pageHeaderText %> </td>
				</tr>	
				<tr>
					<td class="bckWhiteBottomBorder">&nbsp;</td>
				</tr>
				<tr>
					<td class="bckRight">
						
						<table cellpadding="10" cellspacing="0" border="0" width="100%">
							<tr>
								<td>
						
						
<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle">Import </span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
<%

'Initialize the associate object.
dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.TpConnectionString = application("sTpDataSourceName")
oAssociate.VocalErrors = false 'application("bVocalErrors")
oAssociate.CompanyId = session("UserCompanyId")

'Initialize the license object.
dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")

'Initialize the course object
dim oCourse
set oCourse = new Course
oCourse.ConnectionString = application("sDataSourceName")
oCourse.TpConnectionString = application("sTpDataSourceName")
oCourse.VocalErrors = application("bVocalErrors")

'Initialize the CompanyL2 object
dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = false 'application("bVocalErrors")
oCompanyL2.CompanyId = session("UserCompanyId")

'Initialize the CompanyL3 object
dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = false 'application("bVocalErrors")
oCompanyL3.CompanyId = session("UserCompanyId")

'Initialize the Branch object
dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.TpConnectionString = application("sTpDataSourceName")
oBranch.VocalErrors = false 'application("bVocalErrors")
oBranch.CompanyId = session("UserCompanyId")


dim oRs 'Recordset object used for search results.
dim oRsActive 'Recordset object used for search results.


'Declare some variable to track where we are in the loop
dim bErrors 
dim bErrorsInList
dim bBlankSkip

dim bUpdate 'Determines whether this is a new record or an update

response.write("<ul>")

'Variable to hold the type of records we're parsing now, 1 for officers, 
'2 for licenses, 3 for courses.
dim iRecordType 
iRecordType = 1

dim bSkipLine

' *********** NOTE: Build queryable temp table
' From the time we create the table, until we are done reading it, 
' we must reuse the same database session to ensure the temp table is local to the user,
' and that it gets destroyed when the connection is closed (the destruction is automatic,
' though we will clean up ourselves as well as it is good form)    
dim sCreateTable ' as string

sCreateTable = "CREATE TABLE [#tmpLicense](	[LicenseId] [int] IDENTITY(1,1) NOT NULL, " & _
"   [Email] [varchar](100) NULL, " & _
"   [LicenseNum] [varchar](100) NULL, " & _
"	[LicenseStateID] [int] NULL, " & _
"	[LicenseType] [varchar] (100) NULL, " & _
"	[LicenseStatusID] [int] NULL, " & _
"	[LicenseExpDate] [datetime] NULL, " & _
"	 CONSTRAINT [PK_Licenses] PRIMARY KEY CLUSTERED  " & _
"	( " & _
"		[LicenseId] ASC " & _
"	) " & _
") " 


dim licenseCnn
dim licenseRs
dim licenseCmd

set licenseCnn = CreateObject("ADODB.Connection")
licenseCnn.Open  =  application("sDataSourceName")

set licenseCmd = CreateObject("ADODB.Command")
licenseCmd.ActiveConnection = licenseCnn
licenseCmd.CommandText = sCreateTable
licenseCmd.Execute
' Temp officer license table created

'Now loop throught the array to populate the officer license temp table
dim sSQL 
for iCount = 0 to ubound(sCSVData, 2)
    if ucase(sCsvData(0, iCount)) = "*LICENSE" then  
        sEmail = ScrubForSql(sCsvData(1, iCount))
		sLicenseNum = ScrubForSql(sCsvData(2, iCount))
		iLicenseStateId = GetStateIdByAbbrev(ScrubForSql(sCsvData(3, iCount)))
		sLicenseType = ScrubForSql(sCsvData(4, iCount))
        if iLicenseStateId = "" then 
            iLicenseStateId = GetStateIdByAbbrev(MID(slicenseType,1,2))            
        end if 
		iLicenseStatusId = oLicense.LookupLicenseStatusIdByName(ScrubForSql(sCsvData(5, iCount)))
		dLicenseExpDate = ScrubForSql(sCsvData(6, iCount))
			
        sSQL ="INSERT INTO #tmpLicense (Email,LicenseNum,LicenseStateID, LicenseType, LicenseStatusID, LicenseExpDate) " _
        & "VALUES ('" & sEmail & "','" & sLicenseNum & "','" & iLicenseStateId & "','" & sLicenseType & "','" & iLicenseStatusId & "','" & dLicenseExpDate & "')"  

        set licenseCmd = CreateObject("ADODB.Command")
        licenseCmd.ActiveConnection = licenseCnn
        licenseCmd.CommandText = sSQL
        licenseCmd.Execute		
    end if
next

 
' *********** NOTE: Officer License Temporary Table is now populated
' Now run original loop to process records.
' We will use the officer license temporary table later

'Loop through each of the records in our array.
for iCount = 0 to ubound(sCSVData, 2)

	'If we come across a marker to designate a change in the record type,
	'change the value accordingly.
	if ucase(sCsvData(0, iCount)) = "*LICENSE" then
		iRecordType = 2
	elseif ucase(sCsvData(0, iCount)) = "*COURSE" then
		iRecordType = 3
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyName")) then
		iRecordType = 4
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyL2Name")) then
		iRecordType = 5
	elseif ucase(sCsvData(0, iCount)) = "*" & ucase(session("CompanyL3Name")) then
		iRecordType = 6
	elseif ucase(sCsvData(0, iCount)) = "*BRANCH" then
		iRecordType = 7
	else
		iRecordType = 1
	end if 	

%>
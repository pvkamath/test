<%
'==============================================================================
' Class: License
' Controls the creation, modification, and removal of Licenses for Loan
'		Officers, Brokers, and Branches.  
' Create Date: 2/15/05
' Author(s): Mark Silvia
' ----------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- LicenseId
'	- LicenseNum
'	- LicenseExpDate
'	- LicenseAppDeadline
'	- LicenseStatusId
'	- LicenseTypeId
'	- LicenseType
'	- LicenseStateId
'	- LegalName
'	- SubmissionDate
'	- ReceivedDate
'	- OwnerId
'	- OwnerTypeId
'	- OwnerCompanyId
'	- OwnerBranchId
'	- Triggered30
'	- Triggered60
'	- Triggered90
'	- Deleted
'	- DeletedWithUser
'	- SearchLicenseExpDateFrom
'	- SearchLicenseExpDateTo
'	- SearchLicensingTypeId
'	- SearchBranchIdList
'	- SearchStateIdList
'	- SearchStatusIdList
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
' Methods
'	- LoadLicenseById
'	- SaveLicense
'	- DeleteLicense
'	- SearchLicenses
'	- LookupState
'	- LookupLicenseStatus
'	- LookupLicenseType
'	- LookupLicenseStatusIdByName
'	- LookupLicenseTypeIdByName
'	- Safe
'==============================================================================

class License
	
	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'License properties
	dim g_iLicenseId
	dim g_sLicenseNum
	dim g_dLicenseExpDate
	dim g_dLicenseAppDeadline
	dim g_iLicenseStatusId
	dim g_iLicenseTypeId
	dim g_sLicenseType
	dim g_iLicenseStateId
	dim g_sLegalName
	dim g_dSubmissionDate
	dim g_dReceivedDate
	dim g_iOwnerId
	dim g_iOwnerTypeId
	dim g_iOwnerCompanyId
	dim g_iOwnerBranchId
	dim g_bTriggered30
	dim g_bTriggered60
	dim g_bTriggered90
	dim g_dSubDate
	dim g_dCancellationDate
	dim g_dIssueDate
	dim g_sNotes
	dim g_bDeleted
	dim g_bDeletedWithOwner
	
	'Search info
	dim g_dSearchLicenseExpDateFrom
	dim g_dSearchLicenseExpDateTo
	dim g_iAppDeadline
	dim g_sSearchOwnerTypeIdList
	dim g_sSearchText
	dim g_sSearchLastName
	dim g_iSearchBranchId
	dim g_iSearchCompanyL2Id
	dim g_iSearchCompanyL3Id
	dim g_sSearchBranchIdList
	dim g_sSearchCompanyL2IdList
	dim g_sSearchCompanyL3IdList
	dim g_sSearchStateIdList
	dim g_iSearchOwnerInactive
	dim g_sSearchStatusIdList
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Date the license was cancelled
	public property get CancellationDate()
		CancellationDate = g_dCancellationDate
	end property
	public property let CancellationDate(p_dCancellationDate)
		if isdate(p_dCancellationDate) then
			g_dCancellationDate = p_dCancellationDate
		elseif p_dCancellationDate = "" then
			g_dCancellationDate = ""
		else
			ReportError("Invalid CancellationDate value.  Date required.")
		end if
	end property	
	
	'Unique ID for this License
	public property get LicenseId()
		LicenseId = g_iLicenseId
	end property
	
	
	'License Number 
	public property get LicenseNum()
		LicenseNum = g_sLicenseNum
	end property
	public property let LicenseNum(p_sLicenseNum)
		g_sLicenseNum = left(p_sLicenseNum, 50)
	end property
	
	
	'License expiration date
	public property get LicenseExpDate()
		LicenseExpDate = g_dLicenseExpDate
	end property
	public property let LicenseExpDate(p_dLicenseExpDate)
		if isdate(p_dLicenseExpDate) then
			g_dLicenseExpDate = p_dLicenseExpDate
		elseif p_dLicenseExpDate = "" then
			g_dLicenseExpDate = ""
		else
			ReportError("Invalid LicenseExpDate value.  Date required.")
		end if
	end property
	
	
	'License renewal application deadline.  This is optional, but when 
	'specified will take priority over the expiration date.  I.e., the exp
	'date is not the most important if there is a specified app deadline for
	'renewal before the actual expiration.
	public property get LicenseAppDeadline()
		LicenseAppDeadline = g_dLicenseAppDeadline
	end property
	public property let LicenseAppDeadline(p_dLicenseAppDeadline)
		if isdate(p_dLicenseAppDeadline) then
			g_dLicenseAppDeadline = cdate(p_dLicenseAppDeadline)
		elseif p_dLicenseAppDeadline = "" then
			g_dLicenseAppDeadline = p_dLicenseAppDeadline
		else
			ReportError("Invalid LicenseAppDeadline value.  Date required.")
		end if
	end property
	
	
	'Current license status
	public property get LicenseStatusId()
	
		'Mark sure the license is not passed its expiration date, in which case
		'we need to change the license status.
		if g_dLicenseExpDate <> "" and g_dLicenseExpDate < date() then
			g_iLicenseStatusId = 6
		end if 
	
		LicenseStatusId = g_iLicenseStatusId
	end property
	public property let LicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = abs(cint(p_iLicenseStatusId))
		elseif p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid LicenseStatusId value.  Integer required.")
		end if 
	end property
	
	
	'Current license type - states will have varying types of licenses
	'that may be assigned to officers/brokers/branches. 
	'This property is no longer used by the site.
	public property get LicenseTypeId()
		LicenseTypeId = g_iLicenseTypeId
	end property
	public property let LicenseTypeId(p_iLicenseTypeId)
		if isnumeric(p_iLicenseTypeId) then
			g_iLicenseTypeId = cint(p_iLicenseTypeId)
		else
			ReportError("Invalid LicenseTypeId value.  Integer required.")
		end if 
	end property
	
	
	'Description of the license type.
	public property get LicenseType()
		LicenseType = g_sLicenseType
	end property
	public property let LicenseType(p_sLicenseType)
		if len(p_sLicenseType) > 0 then
			g_sLicenseType = left(p_sLicenseType, 50)
		else
			g_sLicenseType = ""
		end if
	end property
	
	
	'State which assigned this license to its owner.
	public property get LicenseStateId()
		LicenseStateId = g_iLicenseStateId
	end property
	public property let LicenseStateId(p_iLicenseStateId)
		if isnumeric(p_iLicenseStateId) then
			g_iLicenseStateId = cint(p_iLicenseStateId)
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if 
	end property
	
	
	'Description of the license type.
	public property get LegalName()
		LegalName = g_sLegalName
	end property
	public property let LegalName(p_sLegalName)
		if len(p_sLegalName) > 0 then
			g_sLegalName = left(p_sLegalName, 50)
		else
			g_sLegalName = ""
		end if
	end property
	
	
	'Date the license renewal application was submitted to the state
	public property get SubmissionDate()
		SubmissionDate = g_dSubmissionDate
	end property
	public property let SubmissionDate(p_dSubmissionDate)
		if isdate(p_dSubmissionDate) then
			g_dSubmissionDate = p_dSubmissionDate
		elseif p_dSubmissionDate = "" then
			g_dSubmissionDate = ""
		else
			ReportError("Invalid SubmissionDate value.  Date required.")
		end if
	end property	
	
	
	'Date the license renewal application was received back from the state
	public property get ReceivedDate()
		ReceivedDate = g_dReceivedDate
	end property
	public property let ReceivedDate(p_dReceivedDate)
		if isdate(p_dReceivedDate) then
			g_dReceivedDate = p_dReceivedDate
		elseif p_dReceivedDate = "" then
			g_dReceivedDate = ""
		else
			ReportError("Invalid ReceivedDate value.  Date required.")
		end if
	end property
	
	
	'Date the license was submitted
	public property get SubDate()
		SubDate = g_dSubDate
	end property
	public property let SubDate(p_dSubDate)
		if isdate(p_dSubDate) then
			g_dSubDate = p_dSubDate
		elseif p_dSubDate = "" then
			g_dSubDate = ""
		else
			ReportError("Invalid SubDate value.  Date required.")
		end if
	end property	


	'Date the license was issued
	public property get IssueDate()
		IssueDate = g_dIssueDate
	end property
	public property let IssueDate(p_dIssueDate)
		if isdate(p_dIssueDate) then
			g_dIssueDate = p_dIssueDate
		elseif p_dIssueDate = "" then
			g_dIssueDate = ""
		else
			ReportError("Invalid IssueDate value.  Date required.")
		end if
	end property	
	
	
	'License Notes
	public property get Notes()
		Notes = g_sNotes
	end property
	public property let Notes(p_sNotes)
		g_sNotes = p_sNotes
	end property
	
	
	'Owner ID - which broker/branch/officer is assigned this license?
	public property get OwnerId()
		OwnerId = g_iOwnerId
	end property
	public property let OwnerId(p_iOwnerId)
		if isnumeric(p_iOwnerId) then
			g_iOwnerId = clng(p_iOwnerId)
		else
			ReportError("Invalid OwnerId value.  Integer required.")
		end if
	end property
	
	
	'Owner type - is the owner ID a reference to a broker or officer/etc?
	'OwnerTypeId = 1 - Loan Officer
	'OwnerTypeId = 2 - Branch
	'OwnerTypeId = 3 - Company
	'OwnerTypeId = 4 - CompanyL2
	'OwnerTypeId = 5 - CompanyL3
	public property get OwnerTypeId()
		OwnerTypeId = g_iOwnerTypeId
	end property
	public property let OwnerTypeId(p_iOwnerTypeId)
		if isnumeric(p_iOwnerTypeId) then
			g_iOwnerTypeId = cint(p_iOwnerTypeId)
		else
			ReportError("Invalid OwnerTypeId value.  Integer required.")
		end if
	end property
	
	
	'Owner Company - which company owns this license?
	public property get OwnerCompanyId()
		OwnerCompanyId = g_iOwnerCompanyId
	end property
	public property let OwnerCompanyId(p_iOwnerCompanyId)
		if isnumeric(p_iOwnerCompanyId) then
			g_iOwnerCompanyId = cint(p_iOwnerCompanyId)
		elseif p_iOwnerCompanyId = "" then
			g_iOwnerCompanyId = ""
		else
			ReportError("Invalid OwnerCompanyId value.  Integer required.")
		end if
	end property
	
	
	'Owner Branch - which branch owns this license?
	public property get OwnerBranchId()
		OwnerBranchId = g_iOwnerBranchId
	end property
	public property let OwnerBranchId(p_iBranchId)
		if isnumeric(p_iBranchId) then
			g_iOwnerBranchId = cint(p_iBranchId)
		elseif p_iBranchId = "" then
			g_iOwnerBranchId = ""
		else
			ReportError("Invalid OwnerBranchId value.  Integer required.")
		end if
	end property
	
	
	'Have we triggered the 30 day alert yet?
	public property get Triggered30()
		Triggered30 = g_bTriggered30
	end property
	public property let Triggered30(p_bTriggered30)
		if isnumeric(p_bTriggered30) then
			g_bTriggered30 = cbool(p_bTriggered30)
		elseif p_bTriggered30 = "" then
			g_bTriggered30 = ""
		else
			ReportError("Invalid Triggered30 value.  Boolean required.")
		end if 	
	end property


	'Have we triggered the 60 day alert yet?
	public property get Triggered60()
		Triggered60 = g_bTriggered60
	end property
	public property let Triggered60(p_bTriggered60)
		if isnumeric(p_bTriggered60) then
			g_bTriggered60 = cbool(p_bTriggered60)
		elseif p_bTriggered60 = "" then
			g_bTriggered60 = ""
		else
			ReportError("Invalid Triggered60 value.  Boolean required.")
		end if 	
	end property
	
	
	'Have we triggered the 90 day alert yet?
	public property get Triggered90()
		Triggered90 = g_bTriggered90
	end property
	public property let Triggered90(p_bTriggered90)
		if isnumeric(p_bTriggered90) then
			g_bTriggered90 = cbool(p_bTriggered90)
		elseif p_bTriggered90 = "" then
			g_bTriggered90 = ""
		else
			ReportError("Invalid Triggered90 value.  Boolean required.")
		end if 	
	end property
	
	
	'Has this license been marked as deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	
	
	'Was this license marked as deleted during an owner delete?  This happens
	'because we don't want to leave active licenses in the system that don't
	'belong to an existing owner.
	public property get DeletedWithOwner()
		DeletedWithOwner = g_bDeletedWithOwner
	end property
	
	
	'Search method minimum License expiration date
	public property get SearchLicenseExpDateFrom()
		SearchLicenseExpDateFrom = g_dSearchLicenseExpDateFrom
	end property
	public property let SearchLicenseExpDateFrom(p_dSearchLicenseExpDateFrom)
		if isdate(p_dSearchLicenseExpDateFrom) then
			g_dSearchLicenseExpDateFrom = p_dSearchLicenseExpDateFrom
		elseif p_dSearchLicenseExpDateFrom = "" then
			g_dSearchLicenseExpDateFrom = ""
		else
			ReportError("Invalid SearchLicenseExpDateFrom value.  " & _
				"Date required.")
		end if
	end property
	
	
	'Search method maximum License expiration date
	public property get SearchLicenseExpDateTo()
		SearchLicenseExpDateTo = g_dSearchLicenseExpDateTo
	end property
	public property let SearchLicenseExpDateTo(p_dSearchLicenseExpDateTo)
		if isdate(p_dSearchLicenseExpDateTo) then
			g_dSearchLicenseExpDateTo = p_dSearchLicenseExpDateTo
		elseif p_dSearchLicenseExpDateTo = "" then
			g_dSearchLicenseExpDateTo = ""
		else
			ReportError("Invalid SearchLicenseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	
	'How are we searching for application deadlines?
	public property get SearchAppDeadline()
		SearchAppDeadline = g_iAppDeadline
	end property
	public property let SearchAppDeadline(p_iAppDeadline)
		if isnumeric(p_iAppDeadline) then
			g_iAppDeadline = clng(p_iAppDeadline)
		elseif p_iAppDeadline = "" or isnull(p_iAppDeadline) then
			g_iAppDeadline = ""
		else
			ReportError("Invalid SearchAppDeadline value.  Integer required. x" & p_iAppDeadline & "x")
		end if 
	end property
	
	
	'Search method value to determine which License Owner types to search for
	'public property get SearchOwnerTypesId()
	'	SearchOwnerTypesId = g_iSearchOwnerTypesId
	'end property
	'public property let SearchOwnerTypesId(p_iSearchOwnerTypesId)
	'	if isnumeric(p_iSearchOwnerTypesId) then
	'		g_iSearchOwnerTypesId = cint(p_iSearchOwnerTypesId)
	'	else
	'		ReportError("Invalid SearchOwnerTypesId value.  Integer required.")
	'	end if
	'end property


	'Search method value to determine which License Owner types to search for
	public property get SearchOwnerTypeIdList()
		SearchOwnerTypeIdList = g_sSearchOwnerTypeIdList
	end property
	public property let SearchOwnerTypeIdList(p_sSearchOwnerTypeIdList)
		g_sSearchOwnerTypeIdList = p_sSearchOwnerTypeIdList
	end property
	
	
	'Search method value to match against various returned fields
	public property get SearchText()
		SearchText = g_sSearchText
	end property
	public property let SearchText(p_sSearchText)
		g_sSearchText = left(p_sSearchText, 20)
	end property
	
	
	'Search method value to match against an associate owner's last name
	public property get SearchLastName()
		SearchLastName = g_sSearchLastName
	end property
	public property let SearchLastName(p_sSearchLastName)
		if p_sSearchLastName <> "" then
			g_sSearchLastName = left(p_sSearchLastName, 50)
		else
			g_sSearchLastName = ""
		end if
	end property


	'Search method value to determine which CompanyL2s to search for
	public property get SearchCompanyL2Id()
		SearchCompanyL2Id = g_iSearchCompanyL2Id
	end property
	public property let SearchCompanyL2Id(p_iSearchCompanyL2Id)
		if isnumeric(p_iSearchCompanyL2Id) then
			g_iSearchCompanyL2Id = cint(p_iSearchCompanyL2Id)
		elseif p_iSearchCompanyL2Id = "" then
			g_iSearchCompanyL2Id = ""
		else
			ReportError("Invalid SearchCompanyL2Id value.  Integer required.")
		end if
	end property


	'Search method value to determine which CompanyL3s to search for
	public property get SearchCompanyL3Id()
		SearchCompanyL3Id = g_iSearchCompanyL3Id
	end property
	public property let SearchCompanyL3Id(p_iSearchCompanyL3Id)
		if isnumeric(p_iSearchCompanyL3Id) then
			g_iSearchCompanyL3Id = cint(p_iSearchCompanyL3Id)
		elseif p_iSearchCompanyL3Id = "" then
			g_iSearchCompanyL3Id = ""
		else
			ReportError("Invalid SearchCompanyL3Id value.  Integer required.")
		end if
	end property


	'Search method value to determine which branches to search for
	public property get SearchBranchId()
		SearchBranchId = g_iSearchBranchId
	end property
	public property let SearchBranchId(p_iSearchBranchId)
		if isnumeric(p_iSearchBranchId) then
			g_iSearchBranchId = cint(p_iSearchBranchId)
		elseif p_iSearchBranchId = "" then
			g_iSearchBranchId = ""
		else
			ReportError("Invalid SearchBranchId value.  Integer required.")
		end if
	end property
	

	'List of CompanyL2 IDs to search within.  Used to find associates from
	'multiple companyL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property


	'List of CompanyL3 IDs to search within.  Used to find associates from
	'multiple companyL3s rather than just one.
	public property get SearchCompanyL3IdList()
		SearchCompanyL3IdList = g_sSearchCompanyL3IdList
	end property
	public property let SearchCompanyL3IdList(p_sSearchCompanyL3IdList)
		g_sSearchCompanyL3IdList = p_sSearchCompanyL3IdList
	end property
	

	'List of Branch IDs to search within.  Used to find associates from
	'multiple branches rather than just one.
	public property get SearchBranchIdList()
		SearchBranchIdList = g_sSearchBranchIdList
	end property
	public property let SearchBranchIdList(p_sSearchBranchIdList)
		g_sSearchBranchIdList = p_sSearchBranchIdList
	end property


	'List of State IDs to search within.  Used to find associates from multiple
	'states rather than just one.
	public property get SearchStateIdList()
		SearchStateIdList = g_sSearchStateIdList
	end property
	public property let SearchStateIdList(p_sSearchStateIdList)
		g_sSearchStateIdList = p_sSearchStateIdList
	end property


	'List of License Status IDs to search within.  Used to find licenses
	'matching any of several status types.
	public property get SearchStatusIdList()
		SearchStatusIdList = g_sSearchStatusIdList
	end property
	public property let SearchStatusIdList(p_sSearchStatusIdList)
		g_sSearchStatusIdList = p_sSearchStatusIdList
	end property
	
	
	'Search method value to determine which whether to include licenses
	'belonging to inactive owners.
	public property get SearchOwnerInactive()
		SearchOwnerInactive = g_iSearchOwnerInactive
	end property
	public property let SearchOwnerInactive(p_iSearchOwnerInactive)
		if isnumeric(p_iSearchOwnerInactive) then
			g_iSearchOwnerInactive = cint(p_iSearchOwnerInactive)
		elseif p_iSearchOwnerInactive = "" then
			g_iSearchOwnerInactive = ""
		else
			ReportError("Invalid SearchOwnerInactive value.  Integer " & _
				"required.")
		end if
	end property

	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blankx for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			if not p_bAllowNull then
				ReportError(p_sName & " must be a valid date for this operation.")
				CheckDateParam = false
			end if
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(sSql & "<p>")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
		
		'Response.Write("<p>" & sSql & "<p>")
		
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	

	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Delete the specified object from the site by marking it as
	'	being deleted.
	' Preconditions: ConnectionString
	' Inputs: p_iLicenseId - License ID to delete
	' Returns: If successful, returns 1.  Otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iLicenseId)
	
		DeleteId = 0
		
		'Verify Preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iLicenseId, 0, "License ID") then
			exit function
		end if 
		
		
		dim oConn 'DB connection
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by last transaction
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'Well mark this ID as deleted. 
		sSql = "UPDATE Licenses SET Deleted = '1' " & _
			"WHERE LicenseId = '" & p_iLicenseId &  "'"
		oConn.Execute sSql, lRecordsAffected
		
		
		'Verify that at least one record was affected
		if not lRecordsAffected > 0 then
			
			ReportError("Failure deleting the License record.")
			exit function
			
		end if 
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadLicenseById
	' Description: Load a company based on the passed LicenseId
	' Preconditions: ConnectionString must be set
	' Inputs: p_iLicenseId - ID of the License to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadLicenseById(p_iLicenseId)
		
		LoadLicenseById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iLicenseId, 0, "License ID") then
			exit function
		end if 


		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM Licenses " & _
			"WHERE LicenseId = '" & p_iLicenseId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the License properties
			g_iLicenseId = oRs("LicenseId")
			g_sLicenseNum = oRs("LicenseNum")
			g_dLicenseExpDate = oRs("LicenseExpDate")
			g_dLicenseAppDeadline = oRs("LicenseAppDeadline")
			g_iLicenseStatusId = oRs("LicenseStatusId")
			g_iLicenseTypeId = oRs("LicenseTypeId")
			g_sLicenseType = oRs("LicenseType")
			g_iLicenseStateId = oRs("LicenseStateId")
			g_sLegalName = oRs("LegalName")
			g_dSubmissionDate = oRs("SubmissionDate")
			g_dReceivedDate = oRs("ReceivedDate")
			g_iOwnerId = oRs("OwnerId")
			g_iOwnerTypeId = oRs("OwnerTypeId")
			g_iOwnerCompanyId = oRs("OwnerCompanyId")
			g_iOwnerBranchId = oRs("OwnerBranchId")
			g_bTriggered30 = oRs("Triggered30")
			g_bTriggered60 = oRs("Triggered60")
			g_bTriggered90 = oRs("Triggered90")
			g_bDeleted = oRs("Deleted")
			g_bDeletedWithOwner = oRs("DeletedWithOwner")
			g_dIssueDate = oRs("IssueDate")
			g_dSubDate = oRs("SubDate")
			g_dCancellationDate = oRs("CancellationDate")
			g_sNotes = oRs("Notes")
			
			LoadLicenseById = g_iLicenseId
			
		else
			
			ReportError("Unable to load the passed License ID.")
			exit function
		
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveLicense
	' Description: Saves a License object's properties to the database.
	' Preconditions: ConnectionString, LicenseExpDate, LicenseStatusId, 
	'	LicenseNum, OwnerID, OwnerTypeId
	' Returns: If successful, returns the saved/new ID, othewise 0.
	' -------------------------------------------------------------------------
	public function SaveLicense()
	
		SaveLicense = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sLicenseNum, 0, "License Number") or _
			not CheckDateParam(g_dLicenseExpDate, true, "Expiration Date") or _
			not CheckIntParam(g_iLicenseStatusId, 0, "License Status ID") or _
			not CheckIntParam(g_iLicenseStateId, 0, "License State ID") or _
			not CheckIntParam(g_iOwnerId, 0, "Owner ID") or _
			not CheckIntParam(g_iOwnerTypeId, 0, "Owner Type ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a LicenseId property assigned already, we're working on a
		'loaded License, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iLicenseId <> "" then
			
			bIsNew = false
			sSql = "UPDATE Licenses SET " & _
				"LicenseNum = '" & g_sLicenseNum & "', "				
			if g_dLicenseExpDate <> "" then
				sSql = sSql & "LicenseExpDate = '" & g_dLicenseExpDate & "', "
			else
				sSql = sSql & "LicenseExpDate = NULL, "
			end if
			if g_dLicenseAppDeadline <> "" then
				sSql = sSql & "LicenseAppDeadline = '" & _
					g_dLicenseAppDeadline & "', "
			else
				sSql = sSql & "LicenseAppDeadline = NULL, "
			end if 
			sSql = sSql & "LicenseStatusId = '" & g_iLicenseStatusId & "', " & _
				"LicenseTypeId = '" & g_iLicenseTypeId & "', " & _
				"LicenseType = '" & g_sLicenseType & "', " & _
				"LicenseStateId = '" & g_iLicenseStateId & "', " & _
				"LegalName = '" & g_sLegalName & "', "
				
			if g_dSubmissionDate <> "" then
				sSql = sSql & "SubmissionDate = '" & g_dSubmissionDate & "', "
			else
				sSql = sSql & "SubmissionDate = NULL, "
			end if
			
			if g_dReceivedDate <> "" then
				sSql = sSql & "ReceivedDate = '" & g_dReceivedDate & "', "
			else
				sSql = sSql & "ReceivedDate = NULL, "
			end if			
			
			if g_dIssueDate <> "" then
				sSql = sSql & "IssueDate = '" & g_dIssueDate & "', " 
			else
				sSql = sSql & "IssueDate = NULL, "
			end if
			
			if g_dSubDate <> "" then
				sSql = sSql & "SubDate = '" & g_dSubDate & "', " 
			else
				sSql = sSql & "SubDate = NULL, "
			end if
				
			if g_dCancellationDate <> "" then
				sSql = sSql & "CancellationDate = '" & g_dCancellationDate & "', " 
			else
				sSql = sSql & "CancellationDate = NULL, "
			end if				
				
			sSql = sSql & "OwnerId = '" & g_iOwnerId & "', " & _
				"OwnerTypeId = '" & g_iOwnerTypeId & "', " & _
				"OwnerCompanyId = '" & g_iOwnerCompanyId & "', " & _
				"OwnerBranchId = '" & g_iOwnerBranchId & "', " & _
				"Triggered30 = '" & abs(cint(g_bTriggered30)) & "', " & _
				"Triggered60 = '" & abs(cint(g_bTriggered60)) & "', " & _
				"Triggered90 = '" & abs(cint(g_bTriggered90)) & "', " & _
				"Notes = '" & Safe(g_sNotes) & "' " & _
				"WHERE LicenseId = '" & g_iLicenseId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
			
				SaveLicense = g_iLicenseId			
			
			else
			
				ReportError("Unable to save existing License.")
				exit function
			
			end if 
				
		else 
			
			bIsNew = true
			sSql = "INSERT INTO Licenses (" & _
				"LicenseNum, " & _
				"LicenseExpDate, " & _
				"LicenseAppDeadline, " & _				
				"LicenseStatusId, " & _
				"LicenseTypeId, " & _
				"LicenseType, " & _
				"LicenseStateId, " & _
				"LegalName, " & _
				"SubmissionDate, " & _
				"ReceivedDate, " & _
				"IssueDate, " & _
				"SubDate, " & _
				"CancellationDate, " & _
				"Notes, " & _
				"OwnerId, " & _
				"OwnerTypeId, " & _
				"OwnerCompanyId, " & _
				"OwnerBranchId, " & _
				"Triggered30, " & _
				"Triggered60, " & _
				"Triggered90 " & _
				") VALUES (" & _
				"'" & g_sLicenseNum & "', "
			
			if g_dLicenseExpDate <> "" then
				sSql = sSql & "'" & g_dLicenseExpDate & "', "
			else
				sSql = sSql & "NULL, "
			end if
				
			if g_dLicenseAppDeadline <> "" then
				sSql = sSql & "'" & g_dLicenseAppDeadline & "', " 
			else
				sSql = sSql & "NULL, " 
			end if
				
			sSql = sSql & "'" & g_iLicenseStatusId & "', " & _
				"'" & g_iLicenseTypeId & "', " & _
				"'" & g_sLicenseType & "', " & _
				"'" & g_iLicenseStateId & "', " & _
				"'" & g_sLegalName & "', "
				
			if g_dSubmissionDate <> "" then
				sSql = sSql & "'" & g_dSubmissionDate & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dReceivedDate <> "" then
				sSql = sSql & "'" & g_dReceivedDate & "', "
			else
				sSql = sSql  & "NULL, "
			end if
			if g_dIssueDate <> "" then
				sSql = sSql & "'" & g_dIssueDate & "', " 
			else
				sSql = sSql & "NULL, "
			end if
			if g_dSubDate <> "" then
				sSql = sSql & "'" & g_dSubDate & "', " 
			else
				sSql = sSql & "NULL, "
			end if
				
			if g_dCancellationDate <> "" then
				sSql = sSql & "'" & g_dCancellationDate & "', " 
			else
				sSql = sSql & "NULL, "
			end if
				
			sSql = sSql & "'" & g_sNotes & "', " & _
				"'" & g_iOwnerId & "', " & _
				"'" & g_iOwnerTypeId & "', " & _
				"'" & g_iOwnerCompanyId & "', " & _
				"'" & g_iOwnerBranchId & "', " & _
				"'" & abs(cint(g_bTriggered30)) & "', " & _
				"'" & abs(cint(g_bTriggered60)) & "', " & _
				"'" & abs(cint(g_bTriggered90)) & "' " & _
				")"
			'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected 
			
			'Retrieve the new License
			sSql = "SELECT LicenseId FROM Licenses " & _
				"WHERE LicenseNum = '" & g_sLicenseNum & "'" & _
				"ORDER BY LicenseId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				
				g_iLicenseId = oRs("LicenseId")
				SaveLicense = g_iLicenseId
			
			else
			
				'The record could not be retrieved.
				ReportError("Failed to save new license.")
				exit function
			
			end if 
			
		end if 
		
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			
			
	' -------------------------------------------------------------------------
	' Name: DeleteLicense
	' Desc: Delete the loaded License.
	' Preconditions: ConnectionString
	' Returns: If successful, 1, othewise 0.
	' -------------------------------------------------------------------------
	public function DeleteLicense()
	
		DeleteLicense = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
	
		
		if DeleteId(g_iLicenseId) = 0 then
		
			'Failure
			ReportError("Failed to delete the License.")
			DeleteLicense = 0
		
		else
		
			'Success
			DeleteLicense = 1
			g_bDeleted = true
		
		end if 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: ReleaseLicense
	' Desc: Clears set/loaded License properties
	' Preconditions: ConnectionString
	' -------------------------------------------------------------------------
	public sub ReleaseLicense()
		
		g_iLicenseId = ""
		g_sLicenseNum = ""
		g_dLicenseExpDate = ""
		g_dLicenseAppDeadline = ""
		g_iLicenseTypeId = ""
		g_sLicenseType = ""
		g_iLicenseStatusId = ""
		g_sLegalName = ""
		g_dSubmissionDate = ""
		g_dReceivedDate = ""
		g_iOwnerId = ""
		g_iOwnerTypeId = ""
		g_iOwnerCompanyId = ""
		g_iOwnerBranchId = ""
		g_bTriggered30 = ""
		g_bTriggered60 = ""
		g_bTriggered90 = ""
		g_dSearchLicenseExpDateFrom = ""
		g_dSearchLicenseExpDateTo = ""
		g_sSearchBranchIdList = ""
		g_bDeleted = ""
		g_bDeletedWithOwner = ""
		
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: SearchLicenses
	' Desc: Retrieves a collection of LicenseIds that match the properties of
	'		the current object.  I.e., based on the current LicenseNum, etc.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchLicenses()
		
		set SearchLicenses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
		dim oSsnRs
		dim bFirstId 'Used to correct SQL phrasing in ID loop
		dim iStateLicensingTypeId
		
		
		'Select the state licensing type from the preferences table
		'based on the current StateID session variable.  This determines
		'which OwnerTypeIds we search for.
		sSql = "SELECT LicensingTypeId FROM StatePreferences " & _
			"WHERE StateId = '" & session("StateId") & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			iStateLicensingTypeId = oRs("LicensingTypeId")
		end if 
		
		
		bFirstClause = false
		
		sSql = "SELECT DISTINCT Ls.LicenseId, Ls.LicenseExpDate, " & _
			"Ls.LicenseAppDeadline, " & _
			"Ls.LicenseStateId, " & _
			"Ls.LicenseNum, " & _
			"Ls.OwnerTypeId, " & _
			"Ls.OwnerId, " & _
			"Ls.IssueDate, " & _
			"ACs.LastName, " & _
			"Bs.Name AS BranchName " & _
			" FROM Licenses AS Ls " & _
			"LEFT JOIN Associates AS ACs " & _
			"ON (ACs.UserId = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 1) " & _
			"LEFT JOIN CompanyBranches AS Bs " & _
			"ON (Bs.BranchId = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 2) " & _
			"LEFT JOIN CompaniesL2 AS L2s " & _
			"ON (L2s.CompanyL2Id = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 4) " & _
			"LEFT JOIN CompaniesL3 AS L3s " & _
			"ON (L3s.CompanyL3Id = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 5) " & _
			"WHERE (NOT (Ls.Deleted = '1')) "

		
		'If we have a State Licensing Type
		if iStateLicensingTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			if iStateLicensingTypeId <= 3 then
				sSql = sSql & "Ls.OwnerTypeId = '" & _
					iStateLicensingTypeId & "'"
			elseif iStateLicensingTypeId = 4 then
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR Ls.OwnerTypeId = '2')"
			elseif iStateLicensingTypeId = 5 then
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR Ls.OwnerTypeId = '3')"
			elseif iStateLicensingTypeId = 6 then
				sSql = sSql & "(Ls.OwnerTypeId = '2' OR Ls.OwnerTypeId = '3')"
			else
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR " & _
					"Ls.OwnerTypeId = '2' OR " & _
					"Ls.OwnerTypeId = '3')"
			end if 		
		end if 
		
		
		'If LicenseNum search
		if g_sLicenseNum <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNum & "%'"
		end if
		
		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
		end if
		
		
		'If LicenseTypeId search
		if g_iLicenseTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseTypeId = '" & g_iLicenseTypeId & "'"
		end if
		
		
		'If license type search
		if g_sLicenseType <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseType LIKE '%" & g_sLicenseType & "%'"					
		end if 
		
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if			
			sSql = sSql & "Ls.OwnerId = '" & g_iOwnerId & "' "
		end if
		
		
		'If OwnerTypeId search
		if g_iOwnerTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerTypeId = '" & g_iOwnerTypeId & "'"
		end if
		
		
		'If Owner Type ID List search
		dim aOwnerTypeIdList
		dim iOwnerTypeId
		dim bFirstOwnerType
		if g_sSearchOwnerTypeIdList <> "" then
			aOwnerTypeIdList = split(g_sSearchOwnerTypeIdList, ",")
			bFirstOwnerType = true
			for each iOwnerTypeId in aOwnerTypeIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstOwnerType then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.OwnerTypeId = '" & iOwnerTypeId & "' "
				
				bFirstOwnerType = false
				
			next 
			sSql = sSql & ")"
		end if
		
		
		if g_iOwnerCompanyId = "" then 
			g_iOwnerCompanyId = session("UserCompanyId")
		end if 
		'If OwnerCompanyId search
		if g_iOwnerCompanyId <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerCompanyId = '" & g_iOwnerCompanyId & "'" 
		end if 
		
		
		'If Owner Inactive search
		if g_iSearchOwnerInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "(ACs.Inactive = '" & g_iSearchOwnerInactive & "' " & _
				"OR Bs.Inactive = '" & g_iSearchOwnerInactive & "' " & _
				"OR L2s.Inactive = '" & g_iSearchOwnerInactive & "' " & _
				"OR L3s.Inactive = '" & g_iSearchOwnerInactive & "' " & _
				"OR Ls.OwnerTypeId = '3') "
		end if
		
		
		'If Triggered30 search
		if g_bTriggered30 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered30 = '" & _
				abs(cint(g_bTriggered30)) & "' "
		end if
		
		
		'If Triggered60 search
		if g_bTriggered60 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered60 = '" & _
				abs(cint(g_bTriggered60)) & "' "
		end if
		
		
		'If Triggered90 search
		if g_bTriggered90 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered90 = '" & _
				abs(cint(g_bTriggered90)) & "' "
		end if
		
		
		'If CompanyL2Id search
		if g_iSearchCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.CompanyL2Id = '" & g_iSearchCompanyL2Id & _
				"' OR Bs.CompanyL2Id = '" & g_iSearchCompanyL2Id & "'"
		end if		

		'If CompanyL3Id search
		if g_iSearchCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.CompanyL3Id = '" & g_iSearchCompanyL3Id & _
				"' OR Bs.CompanyL3Id = '" & g_iSearchCompanyL3Id & "'"
		end if		
		
		'If BranchId search
		if g_iSearchBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.BranchId = '" & g_iSearchBranchId & "' " & _
				"OR Bs.BranchId = '" & g_iSearchBranchId & "'"
		end if			
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true

		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.CompanyL2Id = '" & iCompanyL2Id & "' " & _
					"OR L2s.CompanyL2Id = '" & iCompanyL2Id & "') "
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.CompanyL3Id = '" & iCompanyL3Id & "' " & _
					"OR L3s.CompanyL3Id = '" & iCompanyL3Id & "') "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.BranchId = '" & iBranchId & "' " & _
					"OR Bs.BranchId = '" & iBranchId & "') "
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if
		
		
		'If OwnerBranchId search
		if g_iOwnerBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerBranchId = '" & g_iOwnerBranchId & "'"
		end if
		
		
		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 		


		'If Status Id List search
		dim aStatusIdList
		dim iStatusId
		dim bFirstStatus
		if g_sSearchStatusIdList <> "" then
			aStatusIdList = split(g_sSearchStatusIdList, ",")
			bFirstStatus = true
			for each iStatusId in aStatusIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstStatus then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStatusId = '" & iStatusId & "' "
				
				bFirstStatus = false
			
			next
			sSql = sSql & ")"
		end if 		
		
		
		'If LicenseStateId search
		if g_iLicenseStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStateId = '" & g_iLicenseStateId & "'"
		end if
		
		
		'If SearchLicenseExpDateFrom/To search
		if g_dSearchLicenseExpDateFrom <> "" or g_dSearchLicenseExpDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_dSearchLicenseExpDateTo <> "" then
				if g_iAppDeadline = "1" then 
					sSql = sSql & "((Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dSearchLicenseExpDateTo & "') " & _
						"OR (" & _
						"Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dSearchLicenseExpDateTo & "')) "
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "3" then
					sSql = sSql & "(Ls.SubmissionDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.SubmissionDate <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "4" then
					sSql = sSql & "(Ls.SubDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.SubDate <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				elseif g_iAppDeadline = "5" then
					sSql = sSql & "(Ls.IssueDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.IssueDate <= '" & _
						g_dSearchLicenseExpDateTo & "') "		
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				end if
			else
				if g_iAppDeadline = "1" then
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"OR Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "')"					
				elseif g_iAppDeadline = "2" then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "3" then
					sSql = sSql & "(Ls.SubmissionDate >= '" & _
						g_dSearchLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "4" then
					sSql = sSql & "(Ls.SubDate >= '" & _
						g_dSearchLicenseExpDateFrom & "') "
				elseif g_iAppDeadline = "5" then
					sSql = sSql & "(Ls.IssueDate >= '" & _
						g_dSearchLicenseExpDateFrom & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "') "  
				end if
			end if
		elseif g_dSearchLicenseExpDateTo <> "" then
			if g_iAppDeadline = "1" then
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dSearchLicenseExpDateTo & "' " & _
					"OR Ls.LicenseAppDeadline <= '" & _
					g_dSearchLicenseExpDateTo & "')"		
			elseif g_iAppDeadline = "2" then
				sSql = sSql & "(Ls.LicenseAppDeadline <= '" & _
					g_dSearchLicenseExpDateTo & "') "
			else
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dSearchLicenseExpDateTo & "') "
			end if 
		end if 
		
		
		'If SearchLicenseExpDateFrom/To search
'		if g_dSearchLicenseExpDateFrom <> "" then
'			if bFirstClause then
'				sSql = sSql & " WHERE "
'				bFirstClause = false
'			else
'				sSql = sSql & " AND "
'			end if
'			if g_dSearchLicenseExpDateTo <> "" then
'				sSql = sSql & "((Ls.LicenseExpDate >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseExpDate <= '" & _
'					g_dSearchLicenseExpDateTo & "') " & _
'					"OR (" & _
'					"Ls.LicenseAppDeadline >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseAppDeadline <= '" & _
'					g_dSearchLicenseExpDateTo & "')) "
'			else
'				sSql = sSql & "(Ls.LicenseExpDate >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"OR Ls.LicenseAppDeadline >= '" & _
'					g_dSearchLicenseExpDateFrom & "')"					
'			end if
'		elseif g_dSearchLicenseExpDateTo <> "" then
'			sSql = sSql & "(Ls.LicenseExpDate <= '" & _
'				g_dSearchLicenseExpDateTo & "' " & _
'				"OR Ls.LicenseAppDeadline <= '" & _
'				g_dSearchLicenseExpDateTo & "')"		
'		end if 

		
		'If SearchLastName search
		if g_sSearchLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.LastName LIKE '%" & g_sSearchLastName & "%'"
		end if			
		
		
		sSql = sSql & " ORDER BY Ls.OwnerTypeId DESC, ACs.LastName, Bs.BranchName, Ls.LicenseExpDate ASC"
		
		Response.Write("<p>" & sSql & "<p>")
		'Response.Write(vbcrlf & "<!-- " & sSql & "-->" & vbCrLf)
		
		set SearchLicenses = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name: SearchLicensesByState
	' Desc: Retrieves a collection of LicenseIds that match the properties of
	'		the current object.  I.e., based on the current LicenseNum, etc.,
	'		grouping the totals by state.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchLicensesByState()
		
		set SearchLicensesByState = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
		dim oSsnRs
		dim bFirstId 'Used to correct SQL phrasing in ID loop
		dim iStateLicensingTypeId
		
		
		'Select the state licensing type from the preferences table
		'based on the current StateID session variable.  This determines
		'which OwnerTypeIds we search for.
		sSql = "SELECT LicensingTypeId FROM StatePreferences " & _
			"WHERE StateId = '" & session("StateId") & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			iStateLicensingTypeId = oRs("LicensingTypeId")
		end if 
		
		
		bFirstClause = false
		
		sSql = "SELECT COUNT(Ls.LicenseId) AS LicenseCount, " & _
		 	"Ls.LicenseStateId " & _
			"FROM Licenses AS Ls " & _
			"LEFT JOIN Associates AS ACs " & _
			"ON (ACs.UserId = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 1) " & _
			"LEFT JOIN CompanyBranches AS Bs " & _
			"ON (Bs.BranchId = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 2) " & _
			"LEFT JOIN CompaniesL2 AS L2s " & _
			"ON (L2s.CompanyL2Id = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 4) " & _
			"LEFT JOIN CompaniesL3 AS L3s " & _
			"ON (L3s.CompanyL3Id = Ls.OwnerId " & _
			"AND Ls.OwnerTypeId = 5) " & _
			"WHERE (NOT (Ls.Deleted = '1')) "

		
		'If we have a State Licensing Type
		if iStateLicensingTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			if iStateLicensingTypeId <= 3 then
				sSql = sSql & "Ls.OwnerTypeId = '" & _
					iStateLicensingTypeId & "'"
			elseif iStateLicensingTypeId = 4 then
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR Ls.OwnerTypeId = '2')"
			elseif iStateLicensingTypeId = 5 then
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR Ls.OwnerTypeId = '3')"
			elseif iStateLicensingTypeId = 6 then
				sSql = sSql & "(Ls.OwnerTypeId = '2' OR Ls.OwnerTypeId = '3')"
			else
				sSql = sSql & "(Ls.OwnerTypeId = '1' OR " & _
					"Ls.OwnerTypeId = '2' OR " & _
					"Ls.OwnerTypeId = '3')"
			end if 		
		end if 
		
		
		'If LicenseNum search
		if g_sLicenseNum <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNum & "%'"
		end if
		
		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
		end if
		
		
		'If LicenseTypeId search
		if g_iLicenseTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseTypeId = '" & g_iLicenseTypeId & "'"
		end if
		
		
		'If license type search
		if g_sLicenseType <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseType LIKE '%" & g_sLicenseType & "%'"					
		end if 
		
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if			
			sSql = sSql & "Ls.OwnerId = '" & g_iOwnerId & "' "
		end if
		
		
		'If OwnerTypeId search
		if g_iOwnerTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerTypeId = '" & g_iOwnerTypeId & "'"
		end if
		
		
		'If Owner Type ID List search
		dim aOwnerTypeIdList
		dim iOwnerTypeId
		dim bFirstOwnerType
		if g_sSearchOwnerTypeIdList <> "" then
			aOwnerTypeIdList = split(g_sSearchOwnerTypeIdList, ",")
			bFirstOwnerType = true
			for each iOwnerTypeId in aOwnerTypeIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstOwnerType then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.OwnerTypeId = '" & iOwnerTypeId & "' "
				
				bFirstOwnerType = false
				
			next 
			sSql = sSql & ")"
		end if
		
		
		if g_iOwnerCompanyId = "" then 
			g_iOwnerCompanyId = session("UserCompanyId")
		end if 
		'If OwnerCompanyId search
		if g_iOwnerCompanyId <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerCompanyId = '" & g_iOwnerCompanyId & "'" 
		end if 
		
		
		'If Triggered30 search
		if g_bTriggered30 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered30 = '" & _
				abs(cint(g_bTriggered30)) & "' "
		end if
		
		
		'If Triggered60 search
		if g_bTriggered60 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered60 = '" & _
				abs(cint(g_bTriggered60)) & "' "
		end if
		
		
		'If Triggered90 search
		if g_bTriggered90 <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.Triggered90 = '" & _
				abs(cint(g_bTriggered90)) & "' "
		end if
		
		
		'If CompanyL2Id search
		if g_iSearchCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.CompanyL2Id = '" & g_iSearchCompanyL2Id & _
				"' OR Bs.CompanyL2Id = '" & g_iSearchCompanyL2Id & "'"
		end if		

		'If CompanyL3Id search
		if g_iSearchCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.CompanyL3Id = '" & g_iSearchCompanyL3Id & _
				"' OR Bs.CompanyL3Id = '" & g_iSearchCompanyL3Id & "'"
		end if		
		
		'If BranchId search
		if g_iSearchBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.BranchId = '" & g_iSearchBranchId & "' " & _
				"OR Bs.BranchId = '" & g_iSearchBranchId & "'"
		end if			
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true

		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.CompanyL2Id = '" & iCompanyL2Id & "' " & _
					"OR L2s.CompanyL2Id = '" & iCompanyL2Id & "') "
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.CompanyL3Id = '" & iCompanyL3Id & "' " & _
					"OR L3s.CompanyL3Id = '" & iCompanyL3Id & "') "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "(ACs.BranchId = '" & iBranchId & "' " & _
					"OR Bs.BranchId = '" & iBranchId & "') "
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if
		
		
		'If OwnerBranchId search
		if g_iOwnerBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.OwnerBranchId = '" & g_iOwnerBranchId & "'"
		end if
		
		
		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 		
		
		
		'If LicenseStateId search
		if g_iLicenseStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStateId = '" & g_iLicenseStateId & "'"
		end if
		

		'If SearchLicenseExpDateFrom/To search
		if g_dSearchLicenseExpDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_dSearchLicenseExpDateTo <> "" then
				if g_iAppDeadline = 1 then 
					sSql = sSql & "((Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dSearchLicenseExpDateTo & "') " & _
						"OR (" & _
						"Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dSearchLicenseExpDateTo & "')) "
				elseif g_iAppDeadline = 2 then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseAppDeadline <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"AND Ls.LicenseExpDate <= '" & _
						g_dSearchLicenseExpDateTo & "') "
				end if
			else
				if g_iAppDeadline = 1 then
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "' " & _
						"OR Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "')"					
				elseif g_iAppDeadline = 2 then
					sSql = sSql & "(Ls.LicenseAppDeadline >= '" & _
						g_dSearchLicenseExpDateFrom & "') "
				else
					sSql = sSql & "(Ls.LicenseExpDate >= '" & _
						g_dSearchLicenseExpDateFrom & "') "  
				end if
			end if
		elseif g_dSearchLicenseExpDateTo <> "" then
			if g_iAppDeadline = 1 then
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dSearchLicenseExpDateTo & "' " & _
					"OR Ls.LicenseAppDeadline <= '" & _
					g_dSearchLicenseExpDateTo & "')"		
			elseif g_iAppDeadline = 2 then
				sSql = sSql & "(Ls.LicenseAppDeadline <= '" & _
					g_dSearchLicenseExpDateTo & "') "
			else
				sSql = sSql & "(Ls.LicenseExpDate <= '" & _
					g_dSearchLicenseExpDateTo & "') "
			end if 
		end if 

		
		'If SearchLicenseExpDateFrom/To search
'		if g_dSearchLicenseExpDateFrom <> "" then
'			if bFirstClause then
'				sSql = sSql & " WHERE "
'				bFirstClause = false
'			else
'				sSql = sSql & " AND "
'			end if
'			if g_dSearchLicenseExpDateTo <> "" then
'				sSql = sSql & "((Ls.LicenseExpDate >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseExpDate <= '" & _
'					g_dSearchLicenseExpDateTo & "') " & _
'					"OR (" & _
'					"Ls.LicenseAppDeadline >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"AND Ls.LicenseAppDeadline <= '" & _
'					g_dSearchLicenseExpDateTo & "')) "
'			else
'				sSql = sSql & "(Ls.LicenseExpDate >= '" & _
'					g_dSearchLicenseExpDateFrom & "' " & _
'					"OR Ls.LicenseAppDeadline >= '" & _
'					g_dSearchLicenseExpDateFrom & "')"					
'			end if
'		elseif g_dSearchLicenseExpDateTo <> "" then
'			sSql = sSql & "(Ls.LicenseExpDate <= '" & _
'				g_dSearchLicenseExpDateTo & "' " & _
'				"OR Ls.LicenseAppDeadline <= '" & _
'				g_dSearchLicenseExpDateTo & "')"		
'		end if 
		
		
		'If SearchLastName search
		if g_sSearchLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ACs.LastName LIKE '%" & g_sSearchLastName & "%'"
		end if			
		
		
		sSql = sSql & " GROUP BY Ls.LicenseStateId"
		
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchLicensesByState = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		elseif not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupState = oRs("State")
		end if 
		
		set oRs = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name:				LookupLicenseStatus
	' Description:		Gets the name of the LicenseStatusId from the passed
	'					LicenseStatusId
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iLicenseStatusId - License Status to look up
	' Outputs:			None.
	' Returns:			String w/ License Status Name, or empty
	' -------------------------------------------------------------------------
	public function LookupLicenseStatus(p_iLicenseStatusId)
	
		LookupLicenseStatus = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM LicenseStatusTypes " & _
		       "WHERE LicenseStatusId = '" & p_iLicenseStatusId & "'"
		set oRs = QueryDb(sSql)
		
		if oRs.EOF then
		
			LookupLicenseStatus = ""
			
		else
			
			LookupLicenseStatus = oRs("LicenseStatus")
			
		end if 
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: LookupLicenseType
	' Desc: Gets the name of the license type ID for the passed LicenseTypeId
	' Preconditions: ConnectionString
	' Inputs: p_iLicenseTypeId - License Type to look up
	' Returns: String with Type name, or empty.
	' -------------------------------------------------------------------------
	public function LookupLicenseType(p_iLicenseTypeId)
		
		LookupLicenseType = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL query
		dim oRs 'ADODB recordset
		
		sSql = "SELECT * FROM LicenseTypes " & _
			"WHERE LicenseTypeId = '" & p_iLicenseTypeId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupLicenseType = oRs("LicenseType")
			
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupLicenseStatusIdByName
	' Desc: Gets the ID of the license status based on the passed license
	'	status name value.
	' Preconditions: ConnectionString
	' Inputs: p_sStatus
	' Returns: Integer License Status Id, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupLicenseStatusIdByName(p_sStatus)
		
		LookupLicenseStatusIdByName = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT LicenseStatusId FROM LicenseStatusTypes " & _
			"WHERE LicenseStatus LIKE '" & p_sStatus & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupLicenseStatusIdByName = oRs("LicenseStatusId")
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupLicenseTypeIdByName
	' Desc: Gets the ID of the license status based on the passed license
	'	type name value.
	' Preconditions: ConnectionString
	' Inputs: p_sType
	' Returns: Integer License Type Id, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupLicenseTypeIdByName(p_sType)
	
		LookupLicenseTypeIdByName = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT LicenseTypeId FROM LicenseTypes " & _
			"WHERE LicenseType LIKE '" & p_sType & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupLicenseTypeIdByName = oRs("LicenseTypeId")
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupAssociateNameById
	' Desc: Gets the name of an associate based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupAssociateNameById(p_iId)
	
		LookupAssociateNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT FirstName, LastName FROM Associates " & _
			"WHERE UserId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupAssociateNameById = oRs("FirstName") & " " & oRs("LastName")
			
		end if
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: LookupCompanyNameById
	' Desc: Gets the name of a company based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupCompanyNameById(p_iId)
	
		LookupCompanyNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM Companies " & _
			"WHERE CompanyId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupCompanyNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	


	' -------------------------------------------------------------------------
	' Name: LookupCompanyL2NameById
	' Desc: Gets the name of a company L2 based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupCompanyL2NameById(p_iId)
	
		LookupCompanyL2NameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM CompaniesL2 " & _
			"WHERE CompanyL2Id = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupCompanyL2NameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	


	' -------------------------------------------------------------------------
	' Name: LookupCompanyL3NameById
	' Desc: Gets the name of a company L3 based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupCompanyL3NameById(p_iId)
	
		LookupCompanyL3NameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM CompaniesL3 " & _
			"WHERE CompanyL3Id = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupCompanyL3NameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	


	' -------------------------------------------------------------------------
	' Name: LookupBranchNameById
	' Desc: Gets the name of a branch based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupBranchNameById(p_iId)
	
		LookupBranchNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM CompanyBranches " & _
			"WHERE BranchId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupBranchNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	
	
	
	' -------------------------------------------------------------------------
	' Name: HasDocuments
	' Desc: Determines whether the passed license ID has attachments
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: true if attachments are present, false if not
	' -------------------------------------------------------------------------
	public function HasDocuments(p_iId)
	
		HasDocuments = false
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object

		sSql = "SELECT Ds.DocId FROM AssociateDocs AS Ds " & _
			"WHERE NOT Ds.Deleted = '1' " & _
			"AND Ds.OwnerTypeId = '7' " & _
			"AND Ds.OwnerId = '" & p_iId & "' "
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			HasDocuments = true
			
		end if
		
		set oRs = nothing
		
	end function	
	
	' -------------------------------------------------------------------------
	' Name:	Safe
	' Description: Escapes/scrubs characters to make content safe for
	'	submission into the database.
	' Inputs: p_sText - String to make database-safe.
	' Returns: Edited string
	' -------------------------------------------------------------------------
	public function Safe(p_sText)
	
		if p_sText <> "" then
	
			p_sText = replace(p_sText, "''", "'")
			p_sText = replace(p_sText, "'", "''")
		
			Safe = p_sText
			
		end if
	
	end function	
	
end class
%>
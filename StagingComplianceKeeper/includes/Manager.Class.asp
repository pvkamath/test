<%
'==============================================================================
' Class: AssociateDoc
' Controls the creation, modification, and removal of managers that may be
'	listed under branches/l2s/l3s.
' Create Date: 8/3/06
' Author: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- ManagerId
'	- FullName
'	- CompanyId
'	- OwnerId
'	- OwnerTypeId
'	- Deleted
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
'
' Methods
'	- LoadManagerById
'	- SaveManager
'	- SearchManagers
'==============================================================================

class Manager

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Document properties
	dim g_iManagerId
	dim g_sFullName
	dim g_iCompanyId
	dim g_iOwnerId
	dim g_iOwnerTypeId
	dim g_bDeleted
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Manager
	public property get ManagerId()
		ManagerId = g_iManagerId
	end property
	
	
	'Manager's full name
	public property get FullName()
		FullName = g_sFullName
	end property
	public property let FullName(p_sFullName)
		g_sFullName = left(p_sFullName, 50)
	end property
	
	
	'CompanyId - Unique ID of the company to which this manager belongs.  This
	'would be redundant with the OwnerId/OwnerTypeId combo, except that we 
	'need it to distinguish which company owns a state doc.  The state IDs are
	'shared between companies.
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property 
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'OwnerId - Unique ID of the record this doc belongs to
	public property get OwnerId()
		OwnerId = g_iOwnerId
	end property
	public property let OwnerId(p_iOwnerId)
		if isnumeric(p_iOwnerId) then
			g_iOwnerId = clng(p_iOwnerId)
		elseif p_iOwnerId = "" then
			g_iOwnerId = "" 
		else
			ReportError("Invalid OwnerId value.  Integer required.")
		end if
	end property
	
	
	'Owner Type - Which type of record owns this doc?
	'1 - Branch
	'2 - CompanyL2
	'3 - CompanyL3
	public property get OwnerTypeId()
		OwnerTypeId = g_iOwnerTypeId
	end property
	public property let OwnerTypeId(p_iOwnerTypeId)
		if isnumeric(p_iOwnerTypeId) then
			g_iOwnerTypeId = clng(p_iOwnerTypeId)
		elseif p_iOwnerTypeId = "" then
			g_iOwnerTypeId = "" 
		else
			ReportError("Invalid OwnerTypeId value.  Integer required.")
		end if
	end property
	
	
	'Deleted - Has this record been marked as deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = ""
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
		
		g_bDeleted = 0
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(sSql & "<p>")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadManagerById
	' Desc: Load the object based on the passed Id
	' Preconditions: ConnectionString
	' Inputs: p_iManagerId - ID of the manager to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadManagerById(p_iManagerId)
		
		LoadManagerById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iManagerId, 0, "Manager ID") then
			exit function
		end if 
		
		
		dim oRs 'RecordSet
		dim sSql 'SQL Statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM Managers " & _
			"WHERE ManagerId = '" & p_iManagerId & "'" 
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the Managerument properties
			g_iManagerId = oRs("ManagerId")
			g_sFullName = oRs("FullName")
			g_iCompanyId = oRs("CompanyId")
			g_iOwnerId = oRs("OwnerId")
			g_iOwnerTypeId = oRs("OwnerTypeId")
			g_bDeleted = oRs("Deleted")
			
			LoadManagerById = g_iManagerId
		
		else
			
			ReportError("Unable to load the passed Manager ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
				
				
	' -------------------------------------------------------------------------
	' Name: SaveManager
	' Desc: Saves a Manager object's properties to the database.
	' Preconditions: ConnectionString, OwnerId, OwnerTypeId, FullName
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveManager()
		
		SaveManager = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sFullName, 0, "Full Name") or _
			not CheckIntParam(g_iOwnerId, 0, "Owner ID") or _
			not CheckIntParam(g_iOwnerTypeId, 0, "Owner Type ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a ManagerId property assigned already, we're working on a
		'loaded Manager, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iManagerId <> "" then
		
			bIsNew = false
			sSql = "UPDATE Managers SET " & _
				"FullName = '" & safe(g_sFullName) & "', " & _ 
				"CompanyId = '" & g_iCompanyId & "', " & _
				"OwnerId = '" & g_iOwnerId & "', " & _
				"OwnerTypeId = '" & g_iOwnerTypeId & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE ManagerId = '" & g_iManagerId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				
				SaveManager = g_iManagerId
				
			else
				
				ReportError("Unable to save existing Manager.")
				exit function
				
			end if
			
		else
		
			bIsNew = true
			sSql = "INSERT INTO Managers (" & _
				"FullName, " & _
				"CompanyId, " & _
				"OwnerId, " & _
				"OwnerTypeId, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & safe(g_sFullName) & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & g_iOwnerId & "', " & _
				"'" & g_iOwnerTypeId & "', " & _
				"'" & g_bDeleted & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new Manager
			sSql = "SELECT ManagerId FROM Managers " & _
				"WHERE OwnerId = '" & g_iOwnerId & "' " & _
				"AND FullName = '" & g_sFullName & "' " & _
				"ORDER BY ManagerId DESC" 
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				g_iManagerId  = oRs("ManagerId")
				SaveManager = g_iManagerId 
				
			else
			
				'The record could not be retrieved
				ReportError("Failed to save new Manager.")
				exit function
				
			end if 
			
		end if 
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			

	' -------------------------------------------------------------------------
	' Name: SearchManagers
	' Desc: Retrieves a collection of ManagerIds that match the properties of
	'		the current object instance.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchManagers()
		
		set SearchManagers = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql 'SQL statement
		dim oRs 'Recordset object

		
		bFirstClause = true
		
		sSql = "SELECT Ms.ManagerId, " & _
			"Ms.FullName " & _
			"FROM Managers AS Ms "
			'"WHERE (NOT Ms.Deleted = '1') "
			
		'If FullName search
		if g_sFullName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ms.FullName LIKE '%" & g_sFullName & "%'"
		end if 
		
		
		'If Deleted search
		if g_bDeleted <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ms.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ms.CompanyId = '" & g_iCompanyId & "'"
		end if
		
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ms.OwnerId = '" & g_iOwnerId & "'"
		end if
		
		
		'If OwnerType search
		if g_iOwnerTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ms.OwnerTypeId = '" & g_iOwnerTypeId & "'"
		end if
		
		
		sSql = sSql & " ORDER BY Ms.FullName"
		
		'Response.Write("<P>" & sSql & "<P>")
		
		set SearchManagers = QueryDb(sSql) 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupCompanyNameById
	' Desc: Gets the name of a company based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupCompanyNameById(p_iId)
	
		LookupCompanyNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM Companies " & _
			"WHERE CompanyId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupCompanyNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	


	' -------------------------------------------------------------------------
	' Name: LookupBranchNameById
	' Desc: Gets the name of a branch based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupBranchNameById(p_iId)
	
		LookupBranchNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM CompanyBranches " & _
			"WHERE BranchId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupBranchNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	'					p_bAbbrev - Return two-letter abbreviation
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId, p_bAbbrev)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		elseif not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			if p_bAbbrev then
				LookupState = oRs("Abbrev")
			else
				LookupState = oRs("State")
			end if
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:	Safe
	' Description: Escapes/scrubs characters to make content safe for
	'	submission into the database.
	' Inputs: p_sText - String to make database-safe.
	' Returns: Edited string
	' -------------------------------------------------------------------------
	public function Safe(p_sText)
	
		if p_sText <> "" then
	
			p_sText = replace(p_sText, "''", "'")
			p_sText = replace(p_sText, "'", "''")
		
			Safe = p_sText
			
		end if
	
	end function
	
		
end class
%>
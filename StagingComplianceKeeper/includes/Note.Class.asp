<%
'==============================================================================
' Class: Note
' Controls the creation, modification, and removal of Notes/Comments for Loan
'		Officers, Brokers, and Branches.  
' Create Date: 4/15/05
' Author(s): Mark Silvia
' ----------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- NoteId
'	- NoteText
'	- NoteDate
'	- CreatorId
'	- OwnerId
'	- OwnerTypeId
'	- Deleted
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
' Methods
'	- LoadNoteById
'	- SaveNote
'	- DeleteNote
'	- SearchNotes
'	- LookupUserName
'==============================================================================

class Note
	
	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Note properties
	dim g_iNoteId
	dim g_sNoteText
	dim g_dNoteDate
	dim g_iUserId
	dim g_iOwnerId
	dim g_iOwnerTypeId
	dim g_bDeleted
	
	'Search info
	dim g_dSearchLicenseExpDateFrom
	dim g_dSearchLicenseExpDateTo
	dim g_iSearchOwnerTypesId
	dim g_sSearchText
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Note
	public property get NoteId()
		NoteId = g_iNoteId
	end property
	
	
	'NoteText - stores the actual body text of the note itself.
	public property get NoteText()
		NoteText = g_sNoteText
	end property
	public property let NoteText(p_sNoteText)
		g_sNoteText = p_sNoteText
	end property


	'NoteDate - the date on which the note was posted.
	public property get NoteDate()
		NoteDate = g_dNoteDate
	end property
	public property let NoteDate(p_dNoteDate)
		if isdate(p_dNoteDate) then	
			g_dNoteDate = cdate(p_dNoteDate)
		elseif p_dNoteDate = "" then
			g_dNoteDate = ""
		else
			ReportError("Invalid NoteDate value.  Date required.")
		end if
	end property


	'UserId - unique ID of the user who created this note.
	public property get UserId()
		UserId = g_iUserId
	end property
	public property let UserId(p_iUserId)
		if isnumeric(p_iUserId) then
			g_iUserId = cint(p_iUserId)
		else
			ReportError("Invalid UserId value.  Integer required.")
		end if
	end property
	
	
	'Owner ID - which broker/branch/officer is assigned this license?
	public property get OwnerId()
		OwnerId = g_iOwnerId
	end property
	public property let OwnerId(p_iOwnerId)
		if isnumeric(p_iOwnerId) then
			g_iOwnerId = clng(p_iOwnerId)
		else
			ReportError("Invalid OwnerId value.  Integer required.")
		end if
	end property
	
	
	'Owner type - is the owner ID a reference to a broker or officer/etc?
	'OwnerTypeId = 1 - Loan Officer
	'OwnerTypeId = 2 - Branch
	'OwnerTypeId = 3 - Company
	public property get OwnerTypeId()
		OwnerTypeId = g_iOwnerTypeId
	end property
	public property let OwnerTypeId(p_iOwnerTypeId)
		if isnumeric(p_iOwnerTypeId) then
			g_iOwnerTypeId = cint(p_iOwnerTypeId)
		else
			ReportError("Invalid OwnerTypeId value.  Integer required.")
		end if
	end property
	
	
	'Has this note been marked as deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	

	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Delete the specified object from the site by marking it as
	'	being deleted.
	' Preconditions: ConnectionString
	' Inputs: p_iNoteId - Note ID to delete
	' Returns: If successful, returns 1.  Otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iNoteId)
	
		DeleteId = 0
		
		'Verify Preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iNoteId, 0, "Note ID") then
			exit function
		end if 
		
		
		dim oConn 'DB connection
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by last transaction
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'Well mark this ID as deleted. 
		sSql = "UPDATE Notes SET Deleted = '1' " & _
			"WHERE NoteId = '" & p_iNoteId &  "'"
		oConn.Execute sSql, lRecordsAffected
		
		
		'Verify that at least one record was affected
		if not lRecordsAffected > 0 then
			
			ReportError("Failure deleting the Notes record.")
			exit function
			
		end if 
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadNoteById
	' Description: Load a note based on the passed NoteId
	' Preconditions: ConnectionString must be set
	' Inputs: p_iNoteId - ID of the Note to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadNoteById(p_iNoteId)
		
		LoadNoteById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iNoteId, 0, "Note ID") then
			exit function
		end if 


		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM Notes " & _
			"WHERE NoteId = '" & p_iNoteId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the License properties
			g_iNoteId = oRs("NoteId")
			g_sNoteText = oRs("NoteText")
			g_dNoteDate = oRs("NoteDate")
			g_iUserId = oRs("UserId")
			g_iOwnerId = oRs("OwnerId")
			g_iOwnerTypeId = oRs("OwnerTypeId")
			g_bDeleted = oRs("Deleted")
			
			LoadNoteById = g_iNoteId
			
		else
			
			ReportError("Unable to load the passed Note ID.")
			exit function
		
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveNote
	' Description: Saves a Note object's properties to the database.
	' Preconditions: ConnectionString
	' Returns: If successful, returns the saved/new ID, othewise 0.
	' -------------------------------------------------------------------------
	public function SaveNote()
	
		SaveNote = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, 0, "User ID") or _
			not CheckIntParam(g_iOwnerId, 0, "Owner ID") or _
			not CheckIntParam(g_iOwnerTypeId, 0, "Owner Type ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a NoteId property assigned already, we're working on a
		'loaded Note, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iNoteId <> "" then
			
			bIsNew = false
			sSql = "UPDATE Notes SET " & _
				"NoteText = '" & g_sNoteText & "', " & _
				"NoteDate = '" & g_dNoteDate & "', " & _
				"UserId = '" & g_iUserId & "', " & _
				"OwnerId = '" & g_iOwnerId & "', " & _
				"OwnerTypeId = '" & g_iOwnerTypeId & "' " & _
				"WHERE NoteId = '" & g_iNoteId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
			
				SaveNote = g_iNoteId			
			
			else
			
				ReportError("Unable to save existing Note.")
				exit function
			
			end if 
				
		else 
			
			bIsNew = true
			sSql = "INSERT INTO Notes (" & _
				"NoteText, " & _
				"NoteDate, " & _
				"UserId, " &  _
				"OwnerId, " & _
				"OwnerTypeId " & _
				") VALUES (" & _
				"'" & g_sNoteText & "', " & _
				"'" & g_dNoteDate & "', " & _
				"'" & g_iUserId & "', " & _
				"'" & g_iOwnerId & "', " & _
				"'" & g_iOwnerTypeId & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected 
			
			'Retrieve the new License
			sSql = "SELECT NoteId FROM Notes " & _
				"WHERE UserId = '" & g_iUserId & "'" & _
				"ORDER BY NoteId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				
				g_iNoteId = oRs("NoteId")
				SaveNote = g_iNoteId
			
			else
			
				'The record could not be retrieved.
				ReportError("Failed to save new Note.")
				exit function
			
			end if 
			
		end if 
		
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			
			
	' -------------------------------------------------------------------------
	' Name: DeleteNote
	' Desc: Delete the loaded Note.
	' Preconditions: ConnectionString
	' Returns: If successful, 1, othewise 0.
	' -------------------------------------------------------------------------
	public function DeleteNote()
	
		DeleteNote = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
	
		
		if DeleteId(g_iNoteId) = 0 then
		
			'Failure
			ReportError("Failed to delete the Note.")
			DeleteNote = 0
		
		else
		
			'Success
			DeleteNote = 1
			g_bDeleted = true
		
		end if 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: ReleaseLicense
	' Desc: Clears set/loaded License properties
	' Preconditions: ConnectionString
	' -------------------------------------------------------------------------
	public sub ReleaseNote()
		
		g_iNoteId = ""
		g_sNoteText = ""
		g_iUserId = ""
		g_iOwnerId = ""
		g_iOwnerTypeId = ""
		
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name: SearchNotes
	' Desc: Retrieves a collection of NotesIds that match the properties of
	'		the current object.  I.e., based on the current NoteText, etc.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchNotes()
		
		set SearchNotes = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
		dim oSsnRs

		
		bFirstClause = false
		
		sSql = "SELECT DISTINCT(Ns.NoteId), " & _
			"Ns.NoteDate " & _
			"FROM Notes AS Ns " & _
			"WHERE (NOT Ns.Deleted = '1') "
			
		
		'If note text search
		if g_sNoteText <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ns.NoteText LIKE '%" & g_sNoteText & "%'"
		end if
		
		
		'If we have a user ID
		if g_iUserId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ns.UserId = '" & g_iUserId & "'"
		end if
		
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ns.OwnerId = '" & g_iOwnerId & "'"
		end if
		
		
		'If OwnerTypeId search
		if g_iOwnerTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ns.OwnerTypeId = '" & g_iOwnerTypeId & "'"
		end if
		
		
		sSql = sSql & " ORDER BY Ns.NoteDate DESC"
		
		'Response.Write(sSql & "<p>")
		
		set SearchNotes = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupUserName
	' Desc: Gets the first and last name of the specified user.
	' Preconditions: ConnectionString
	' Inputs: p_iUserId - User ID to look up
	' Returns: String with User Name, or empty
	' -------------------------------------------------------------------------
	public function LookupUserName(p_iUserId)
	
		LookupUserName = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iUserId, 0, "User ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset
		
		sSql = "SELECT FirstName, LastName FROM Users " & _
			"WHERE UserId = '" & g_iUserId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupUserName = oRs("FirstName") & " " & oRs("LastName")
		end if 
		
		set oRs = nothing
		
	end function 
		

end class
%>
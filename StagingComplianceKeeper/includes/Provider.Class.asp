<%
'==============================================================================
' Class: Provider
' Controls the creation, modification, and removal of education providers.
' Create Date: 12/13/04
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- ConnectionString
'	- VocalErrors
'	- ProviderId
'	- Name
'	- OwnerCompanyId
'	- Status
'	- Address
'	- Address2
'	- City
'	- StateId
'	- Zipcode
'	- Website
'	- Phone
'	- PhoneExt
'	- Fax
'	- ContactName
'	- ContactTitle
' Methods:
'	- LoadProviderById
'	- SaveProvider
'	- DeleteProvider
'	- DeleteProviderById
'	- SearchProviders
'	- LookupState
'==============================================================================

class Provider

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Base provider properties
	dim g_iProviderId
	dim g_sName
	dim g_iOwnerCompanyId
	dim g_iStatus

	'Location info
	dim g_sAddress
	dim g_sAddress2
	dim g_sCity
	dim g_iStateId
	dim g_sZipcode
	dim g_sWebsite
	dim g_sPhone
	dim g_sPhoneExt
	dim g_sFax
	dim g_sContactName
	dim g_sContactTitle


	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	
	'Retrieves the unique ID of this Provider.  This is an identity in the DB, 
	'so it cannot be set.
	public property get ProviderId()
		ProviderId = g_iProviderId
	end property
	

	'Stores/retrieves the Provider name.
	public property let Name(p_sName)
		g_sName = p_sName
	end property
	public property get Name()
		Name = g_sName
	end property
	
	
	'Company for which this provider exists.  Value of zero makes the provider
	'visible to all companies.
	public property get OwnerCompanyId()
		OwnerCompanyId = g_iOwnerCompanyId
	end property
	public property let OwnerCompanyId(p_iOwnerCompanyId)
		if isnumeric(p_iOwnerCompanyId) then
			g_iOwnerCompanyId = cint(p_iOwnerCompanyId)
		else
			ReportError("Invalid OwnerCompanyId value.  Integer required.")
		end if 
	end property
	
	
	'Whether or not the provider is currently enabled and active.  Disabled
	'companies do not appear on the site.
	public property get Status()
		Status = g_iStatus
	end property
	public property let Status(p_iStatus)
		if isnumeric(p_iStatus) then
			g_iStatus = abs(cint(p_iStatus))
		else
			ReportError("Invalid Status value.  Positive integer required.")
		end if 	
	end property


	'Stores/retrieves the provider address
	public property let Address(p_sAddress)
		g_sAddress = p_sAddress
	end property
	public property get Address()
		Address = g_sAddress
	end property
	

	'Address line two
	public property get Address2()
		Address2 = g_sAddress2
	end property
	public property let Address2(p_sAddress2)
		g_sAddress2 = p_sAddress2
	end property
	
	
	'Stores/retrieves the provider city
	public property let City(p_sCity)
		g_sCity = p_sCity
	end property
	public property get City()
		City = g_sCity
	end property
	
	
	'Stores/retrieves the provider state ID
	public property let StateId(p_iStateId)
		g_iStateId = p_iStateId
	end property
	public property get StateId()
		StateId = g_iStateId
	end property
	
	
	'Stores/retrieves the provider zip.
	public property let Zipcode(p_sZipcode)
		g_sZipcode = p_sZipcode
	end property
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	
	
	'Website URL
	public property get Website()
		Website = g_sWebsite
	end property
	public property let Website(p_sWebsite)
		g_sWebsite = p_sWebsite
	end property
	
	
	'Phone number as a string
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = p_sPhone
	end property
	
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = p_sPhoneExt
	end property
	
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = p_sFax
	end property

	'Contact Name
	public property get ContactName()
		ContactName = g_sContactName
	end property
	public property let ContactName(p_sContactName)
		g_sContactName = p_sContactName
	end property
		
	'Contact Title
	public property get ContactTitle()
		ContactTitle = g_sContactTitle
	end property
	public property let ContactTitle(p_sContactTitle)
		g_sContactTitle = p_sContactTitle
	end property
		
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'nothing
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'nothing
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	' -------------------------------------------------------------------------
	' Name:				DeleteId
	' Description:		Delete the specified Provider object from the relevant 
	'					database tables
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iProviderId - Provider ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iProviderId)
	
		DeleteId = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyId, 1, "Company ID") then
			exit function		
		end if
		
	
		dim oConn 'DB connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")

		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		
		'We'll clean out our records from the database tables.
		sSql = "DELETE FROM Providers " & _
			   "WHERE ProviderId = '" & p_iProviderId & "'"
		oConn.Execute sSql, lRecordsAffected
		
		
		'Verify that at least one record was affected.
		if not lRecordsAffected > 0 then
		
			ReportError("Failure deleting the provider record.")
			DeleteId = 0
			exit function		
		
		end if 
		

		DeleteId = 1
	
	end function


	
	' PUBLIC METHODS ==========================================================
	' -------------------------------------------------------------------------
	' Name:				LoadProviderById
	' Description:		This method attempts to load the properties of a stored
	'					Provider based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iProviderId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadProviderById(p_iProviderId)
	
		LoadProviderById = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iProviderId, 1, "Provider ID") then
			exit function		
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")

		sSql = "SELECT * FROM Providers " & _
			   "WHERE ProviderId = '" & p_iProviderId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so this is a valid ID.  We'll fill in
			'the relevant object properties.
			g_iProviderId = oRs("ProviderId")
			g_sName = oRs("Name")
			g_iOwnerCompanyId = oRs("OwnerCompanyId")
			g_iStatus = oRs("Status")
			
			g_sAddress = oRs("Address")
			g_sAddress2 = oRs("Address2")
			g_sCity = oRs("City")
			g_iStateId = oRs("StateID")
			g_sZipcode = oRs("Zipcode")
			g_sWebsite = oRs("Website")
			g_sPhone = oRs("Phone")
			g_sPhoneExt = oRs("PhoneExt")
			g_sFax = oRs("Fax")
			g_sContactName = oRs("ContactName")
			g_sContactTitle = oRs("ContactTitle")
			LoadProviderById = g_iProviderId
			
		else
		
			'If no recordset was returned, this ID doesn't exist.  We return
			'zero and quit.
			LoadProviderById = 0
			set oRs = nothing
			ReportError("Unable to load the passed provider ID.")
			exit function
	
		end if

			
		set oRs = nothing
		
	end function
	
	
	
	' -------------------------------------------------------------------------
	' Name:				SaveProvider
	' Description:		Save the Provider object in the database, and assign it
	'					the resulting Provider ID.
	' Pre-conditions:	ConnectionString, Name, OwnerStateId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns the new ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveProvider()
	
		SaveProvider = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckStrParam(g_sName, 0, "Provider Name") then
			exit function
		elseif not CheckIntParam(g_iOwnerCompanyId, 0, "Owner Company ID") then
			exit function				
		end if


		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		'If we have a ProviderId property assigned already, we're working on a
		'loaded Provider, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iProviderId <> "" then
		
			'Now we can proceed with building our SQL statement.
			bIsNew = false
			sSql = "UPDATE Providers SET " & _
				   "Name = '" & g_sName & "', " & _
				   "OwnerCompanyId = '" & g_iOwnerCompanyId & "', " & _
				   "Status = '" & abs(cint(g_iStatus)) & "', " & _
				   "Address = '" & g_sAddress & "', " & _
				   "Address2 = '" & g_sAddress2 & "', " & _
				   "City = '" & g_sCity & "', " & _
				   "StateId = '" & g_iStateId & "', " & _
				   "Zipcode = '" & g_sZipcode & "', " & _
				   "Website = '" & g_sWebsite & "', " & _
				   "Phone = '" & g_sPhone & "', " & _
				   "PhoneExt = '" & g_sPhoneExt & "', " & _
				   "Fax = '" & g_sFax & "', " & _
				   "ContactName = '" & g_sContactName & "', " & _
				   "ContactTitle = '" & g_sContactTitle & "' "

			sSql = sSql & "WHERE ProviderId = '" & g_iProviderId & "'"
			'Response.Write(sSql & "<p>")			
			oConn.Execute(sSQL)
			
			SaveProvider = g_iProviderId

		else

			'Build an SQL statement for a new Company.
			bIsNew = true
			sSql = "INSERT INTO Providers (" & _
				   "Name, " & _
				   "OwnerCompanyId, " & _
				   "Status, " & _
				   "Address, " & _
				   "Address2, " & _
				   "City, " & _
				   "StateId, " & _
				   "Zipcode, " & _
				   "Website, " & _
				   "Phone, " & _
				   "PhoneExt, " & _
				   "Fax, " & _ 
				   "ContactName, " & _
				   "ContactTitle " & _
				   ") VALUES (" & _
				   "'" & g_sName & "', " & _
				   "'" & g_iOwnerCompanyId & "', " & _
				   "'" & g_iStatus & "', " & _
				   "'" & g_sAddress & "', " & _
				   "'" & g_sAddress2 & "', " & _
				   "'" & g_sCity & "', " & _
				   "'" & g_iStateId & "', " & _
				   "'" & g_sZipcode & "', " & _
				   "'" & g_sWebsite & "', " & _
				   "'" & g_sPhone & "', " & _
				   "'" & g_sPhoneExt & "', " & _
				   "'" & g_sFax & "', " & _
				   "'" & g_sContactName & "', " & _
				   "'" & g_sContactTitle & "'" & _
				   ")"
			set oRs = oConn.Execute(sSQL)
				   
			'Now we'll try to pull out the new Company we just created.
			sSql = "SELECT ProviderId FROM Providers " & _
			       "WHERE Name = '" & g_sName & "' " & _
			       "AND OwnerCompanyId = '" & g_iOwnerCompanyId & "' " & _
			       "ORDER BY ProviderId DESC"
			
			set oRs = oConn.Execute(sSQL)
						
			if not (oRs.EOF and oRs.BOF) then
			
				g_iProviderId = oRs("ProviderId")
				SaveProvider = g_iProviderId
			
			else
			
				'Our record was not created, or for some other reason we
				'couldn't pull the record back out.
				ReportError("Provider creation failed.")
				SaveProvider = 0
				exit function
			
			end if 
		
		end if

			   
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name:				DeleteProvider
	' Description:		Delete the loaded Provider object from the relevant
	'                   database tables
	' Pre-conditions:	ConnectionString, ProviderId
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteProvider()
	
		DeleteProvider = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iProviderId, 0, "Provider ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(g_iProviderId)
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete this provider.")
			DeleteProvider = 0
		
		else
		
			'Hooray
			DeleteProvider = 1
		
		end if 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				DeleteProviderById
	' Description:		Delete the specified provider object from the relevant
	'					database tables
	' Preconditions:	ConnectionString
	' Inputs:			p_iProviderId - Provider ID to delete
	' Outputs:			None.
	' Returns:			If successful, returns 1.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteProviderById(p_iProviderId)
	
		DeleteProviderById = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iProviderId, 0, "Provider ID") then
			exit function		
		end if
		
	
		dim iResult
		
		iResult = DeleteId(p_iProviderId)
		
		if iResult = 0 then
		
			'Zero means failure.
			ReportError("Could not delete the specified provider.")
			DeleteProviderById = 0
		
		else
		
			'Hooray
			DeleteProviderById = 1
		
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:				SearchProviders
	' Description:		Retrieves a collection of ProviderIds that match the 
	'                   properties of the current object.  I.e., based on the
	'                   current Name, City, etc.
	' Pre-conditions:	ConnectionString
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchProviders()
	
		set SearchProviders = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		

		dim bFirstClause 'Used direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT DISTINCT(Ps.ProviderID), Ps.Name FROM Providers AS Ps"

		
		'if Name search
		if g_sName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ps.Name LIKE '%" & g_sName & "%'"
		end if
		
		'if Address search
		if g_sAddress <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ps.Address LIKE '%" & g_sAddress & "%'" 
		end if
		
		'if City search
		if g_sCity <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ps.City LIKE '%" & g_sCity & "%'" 
		end if
		
		'if StateID search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ps.StateID = '" & g_iStateId & "'" 
		end if

		'if Zipcode search
		if g_sZipcode <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ps.Zipcode = '" & g_sZipcode & "'" 
		end if

		'if Active search
		if g_iStatus <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			if g_iStatus = 1 then
				sSql = sSql & "Ps.Status = '" & g_iStatus & "'" 
			else
				sSql = sSql & "Ps.Status = '2'"
			end if 
		end if
		
		'if OwnerCompanyId search
		if g_iOwnerCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "Ps.OwnerCompanyId = '" & g_iOwnerCompanyId & "'"
		end if
		
		'Do not retrieve deleted providers.
		if bFirstClause then
			sSql = sSql & " WHERE "
			bFirstClause = false
		else
			sSql = sSql & " AND "
		end if 
		sSql = sSql & "NOT Ps.Status = '3'"
		
		
		sSQL = sSQL & " ORDER BY Ps.Name"

		'response.write("<p>" & sSql & "<p>")
		
		set SearchProviders = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		LookupState = oRs("State")
		
		set oRs = nothing
		
	end function
		

end class
%>
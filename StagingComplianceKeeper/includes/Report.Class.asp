<%

'==============================================================================
' Class: Report
' Controls the creation and display of Reports.
' Create Date: 01/17/05
' Current Version: 1.0
' Author(s): Mark Silvia
' --------------------
' Properties:
'	- ConnectionString
' Methods:
'	- Class_Initialize()
'	- Class_Terminate()
'	- CheckConnectionString()
'	- CheckIntParam()
'	- CheckStrParam()
'	- QueryDb(sSql)
'	- QueryTpDb(sSql)
'	- GetBranchReport()
'	- WriteCSVBranchReport()
'==============================================================================

Class Report

	' GLOBAL VARIABLE DECLARATIONS ============================================

	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Base company properties
	dim g_oRs 'Main report recordset
	
	dim g_iReportNumber
	dim g_sReportPath
	dim g_iStateId
	dim g_iCompanyId
	dim g_iBranchId
	dim g_iUserId
	dim g_iReportDays
	
	'Output setting properties
	dim g_bOutputFirstName
	dim g_bOutputMiddleName
	dim g_bOutputLastName
	dim g_bOutputSsn
	dim g_bOutputAddress
	dim g_bOutputBranch
	dim g_bOutputCompany
	dim g_bOutputEmail
	dim g_bOutputTitle
	dim g_bOutputDriversLic
	dim g_bOutputPhone
	dim g_bOutputCourses

	

	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
		g_oCompany.ConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	
	
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property 
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Display/hide error messages.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	'Report number is the ID of the report, used in the file name.  Read only.
	public property get ReportNumber()
		ReportNumber = g_iReportNumber
	end property
	
	
	'Location to save the report CSV file.
	public property get ReportPath()
		ReportPath = g_sReportPath
	end property
	public property let ReportPath(p_sReportPath)
		g_sReportPath = p_sReportPath
	end property
	
	
	'Certifications Issued search type.
	public property get CertsIssued()
		CertsIssued = g_iCertsIssued
	end property
	public property let CertsIssued(p_iCertsIssued)
		g_iCertsIssued = p_iCertsIssued
	end property
	
	
	'State ID
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		g_iStateId = p_iStateId
	end property
	
	
	'Course ID
	public property get CourseId()
		CourseId = g_iCourseId
	end property
	public property let CourseId(p_iCourseId)
		g_iCourseId = p_iCourseId
	end property
	
	'Course Type
	public property get CourseType()
		CourseType = g_iCourseType
	end property
	public property let CourseType(p_iCourseType)
		g_iCourseType = p_iCourseType
	end property
	
	'Branch ID
	public property get BranchId()
		BranchId = g_iBranchId
	end property
	public property let BranchId(p_iBranchId)
		g_iBranchId = p_iBranchId
	end property
	
	'User ID
	public property get UserId()
		UserId = g_iUserId
	end property
	public property let UserId(p_iUserId)
		g_iUserId = p_iUserId
	end property
	
	'Report term in days
	public property get ReportDays()
		ReportDays = g_iReportDays
	end property
	public property let ReportDays(p_iReportDays)
		if isnumeric(p_iReportDays) then
			g_iReportDays = abs(cint(p_iReportDays))
		else
			ReportError("Invalid ReportDays value.  Integer required.")
		end if 
	end property
	
	
	'Display setting properties
	'Output setting properties
	public property get OutputFirstName()
		OutputFirstName = g_bOutputFirstName
	end property
	public property let OutputFirstName(p_bOutputFirstName)
		if isnumeric(p_bOutputFirstName) then
			g_bOutputFirstName = cbool(p_bOutputFirstName)
		else
			ReportError("Invalid OutputFirstName.  Boolean required.")
		end if 
	end property
	
	
	public property get OutputMiddleName()
		OutputMiddleName = g_bOutputMiddleName
	end property
	public property let OutputMiddleName(p_bMiddleName)
		if isnumeric(p_bOutputMiddleName) then
			g_bOutputMiddleName = cbool(p_bOutputMiddleName)
		else
			ReportError("Invalid OutputMiddleName value.  Boolean required.")
		end if 
	end if 
	
	
	public property get OutputLastName()
		OutputLastName = g_bOutputLastName
	end property
	public property let OutputLastName(p_bOutputLastName)
		if isnumeric(p_bOutputLastName) then
			g_bOutputLastName = cbool(p_bOutputLastName)
		else
			ReportError("Invalid OutputLastName value.  Boolean required.")
		end if 
	end property


	public property get OutputSsn()
		OutputSsn = g_bOutputSsn
	end property
	public property Let OutputSsn(p_bOutputSsn)
		if isnumeric(p_bOutputSsn) then
			g_bOutputSsn = cbool(p_bOutputSsn)
		else
			ReportError("Invalid OutputSsn value.  Boolean required.")
		end if
	end property
	
	
	public property get OutputAddress()
		OutputAddress = g_bOutputAddress
	end property
	public property let OutputAddress(p_bOutputAddress)
		if isnumeric(p_bOutputAddress) then
			g_bOutputAddress = cbool(p_bOutputAddress)
		else
			ReportError("Invalid OutputAddress value.  Boolean required.")
		end if 
	end property
	
	
	public property get OutputBranch()
		OutputBranch = g_bOutputBranch
	end property
	public property let OutputBranch(p_bOutputBranch)
		if isnumeric(p_bOutputBranch) then
			g_bOutputBranch = cbool(p_bOutputBranch)
		else
			ReportError("Invalid OutputBranch value.  Boolean required.")
		end if		
	end property


	public property get OutputCompany()
		OutputCompany = g_bOutputCompany
	end property
	public property let OutputCompany(p_bOutputCompany)
		if isnumeric(p_bOutputCompany) then 
			g_bOutputCompany = cbool(p_bOutputCompany)
		else
			ReportError("Invalid OuputCompany value.  Boolean required.")
		end if 
	end property
	
	
	public property get OutputEmail()
		OutputEmail = g_bOutputEmail
	end property
	public property let OutputEmail(p_bOutputEmail)
		if isnumeric(p_bOutputEmail) then
			g_bOutputEmail = cbool(p_bOutputEmail)
		else
			ReportError("Invalid OutputEmail value.  Boolean required.")
		end if 
	end property
	
	
	public property get OutputDriversLic()
		OutputDriversLic = g_bOutputDriversLic
	end property
	public property let OutputDriversLic(p_bOutputDriversLic)
		if isnumeric(p_bOutputDriversLic) then
			g_bOutputDriversLic = cbool(p_bOutputDriversLic)
		else
			ReportError("Invalid OutputDriversLic value.  Boolean required.")
		end if 
	end property
	
	
	public property get OutputCourses()
		OutputCourses = g_bOutputCourses
	end property
	public property let OutputCourses(p_bOutputCourses)
		if isnumeric(p_bOutputCourses) then
			g_bOutputCourses = cbool(p_bOutputCourses)
		else
			ReportError("Invalid OutputCourses value.  Boolean required.")
		end if			
	end property



	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub

	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	

	
	
	' PUBLIC METHODS ==========================================================
	' -------------------------------------------------------------------------
	' Name:				GetBranchReport
	' Description:		This method creates a report recordset based on the
	'					assigned object properties.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the generated recordset.
	' -------------------------------------------------------------------------
	public function GetBranchReport()

		set GetBranchReport = server.CreateObject("ADODB.Recordset")

		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if


		'Determine which DB to use
		dim bUseTpDb
		if g_iBranchId <> "" then
			if g_iBranchId >= 0 then
				bUseTpDb = true
			elseif g_iBranchId < 0 then
				bUseTpDb = false
			end if
		else
			if g_iCompanyId >= 0 then
				bUseTpDb = true
			elseif g_iCompanyId < 0 then
				bUseTpDb = false
			end if
		end if


		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim bFirstClause

		bFirstClause = true

		set oRs = Server.CreateObject("ADODB.RecordSet")

		if bUseTpDb then
			sSql = "SELECT *, " & _
				"Cs.Name AS CompanyName, " & _
				"CBs.Name AS BranchName, " & _
				"FROM Associates AS Us " & _
				"LEFT JOIN Companies AS Cs " & _
				"ON (Cs.CompanyId = Us.CompanyId) " & _
				"LEFT JOIN CompanyBranches AS CBs " & _
				"ON (CBs.BranchId = Us.BranchId) "
		else
			sSql = "SELECT *, " & _
				"Cs.Name AS CompanyName, " & _
				"CBs.Name AS BranchName, " & _
				"FROM Users AS Us " & _
				"LEFT JOIN UsersInfo AS UI " & _
				"ON (UI.UserId = Us.UserId) " & _
				"LEFT JOIN Companies AS Cs " & _
				"ON (Cs.CompanyId = As.CompanyId) " & _
				"LEFT JOIN CompanyBranches AS CBs " & _
				"ON (CBs.BranchId = As.BranchId) " 
		end if 
		
			  
		'if Company search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Us.CompanyID = '" & g_iCompanyId & "'"
		end if
			   
		'if Branch search
		if g_iBranchId <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Us.BranchID = '" & g_iBranchId & "'"
		end if
		
		'if State search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StateID = '" & g_iStateId & "'"
		end if	

		if bFirstClause then
			sSql = sSql & " WHERE "
			bFirstClause = false
		else
			sSql = sSql & " AND "
		end if
		sSQL = sSql & " Us.UserStatus <> 3 "

		sSql = sSql & " ORDER BY Us.LastName"

		set GetReport = QueryDb(sSql)

	end function


	' -------------------------------------------------------------------------
	' Name:				GetExpirationReport
	' Description:		This method creates a report recordset based on the
	'					assigned object properties.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			None.
	' Outputs:			None.
	' Returns:			Returns the generated recordset.
	' -------------------------------------------------------------------------
	public function GetExpirationReport()

		set GetExpirationReport = server.CreateObject("ADODB.Recordset")

		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if


		'Determine which DB to use
		dim bUseTpDb
		if g_iBranchId <> "" then
			if g_iBranchId >= 0 then
				bUseTpDb = true
			elseif g_iBranchId < 0 then
				bUseTpDb = false
			end if
		else
			if g_iCompanyId >= 0 then
				bUseTpDb = true
			elseif g_iCompanyId < 0 then
				bUseTpDb = false
			end if
		end if


		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim bFirstClause

		bFirstClause = true

		set oRs = Server.CreateObject("ADODB.RecordSet")

		if bUseTpDb then
			sSql = "SELECT *, " & _
				"Cs.Name AS CompanyName, " & _
				"CBs.Name AS BranchName, " & _
				"FROM AssociateLicenses AS ALs " & _
				"LEFT JOIN Associates AS Us " & _
				"LEFT JOIN Companies AS Cs " & _
				"ON (Cs.CompanyId = Us.CompanyId) " & _
				"LEFT JOIN CompanyBranches AS CBs " & _
				"ON (CBs.BranchId = Us.BranchId) "
		else
			sSql = "SELECT *, " & _
				"Cs.Name AS CompanyName, " & _
				"CBs.Name AS BranchName, " & _
				"FROM AssociateLicenses AS ALs " & _
				"LEFT JOIN Users AS Us " & _
				"LEFT JOIN UsersInfo AS UI " & _
				"ON (UI.UserId = Us.UserId) " & _
				"LEFT JOIN Companies AS Cs " & _
				"ON (Cs.CompanyId = As.CompanyId) " & _
				"LEFT JOIN CompanyBranches AS CBs " & _
				"ON (CBs.BranchId = As.BranchId) "
		end if


		'if Company search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Us.CompanyID = '" & g_iCompanyId & "'"
		end if

		'if Branch search
		if g_iBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Us.BranchID = '" & g_iBranchId & "'"
		end if
		
		'if LoanOfficer Search 
		if g_iUserId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "ALs.UserId = '" & g_iUserId & "'"
		end if 
		
		'if ReportDays search
		if g_iReportDays <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "ALs.LicenseExpDate <= '" & _
				Now + g_iReportDays & "' "
		end if 
		

		'if State search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Cs.StateID = '" & g_iStateId & "'"
		end if

		if bFirstClause then
			sSql = sSql & " WHERE "
			bFirstClause = false
		else
			sSql = sSql & " AND "
		end if
		sSQL = sSql & " Us.UserStatus <> 3 "

		sSql = sSql & " ORDER BY Us.LastName"

		set GetReport = QueryDb(sSql)

	end function


	' -------------------------------------------------------------------------
	' Name: WriteCSVExpirationReport
	' Description: This method creates a report in CSV format and writes it out
	'			   to a file.
	' Preconditions: ConnectionString, ReportPath
	' Returns: Report filename if successful, empty string if failed.
	' -------------------------------------------------------------------------
	public function WriteCSVExpirationReport()
		
		WriteCSVExpirationReport = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		if not CheckStrParam(g_sReportPath, false, "Report Path")
			exit function
		end if 
		

		dim sTempDateText
		dim bFirstField
				
		'Get the report recordset.
		dim oRs
		set oRs = GetExpirationReport()
		
		'ReportNumber is made up of the date and the branch ID if present
		g_iReportNumber = Year(Now) & Month(Now) & Day(Now) & Hour(Now) & _
			Minute(Now) & Second(Now) & g_iBranchId
			
		'Create the file path from the ReportNumber and the ReportPath
		dim sFullFilePath
		sFullFilePath = g_sReportPath & "report_" & g_iReportNumber & ".csv"
		sFileName = "report_" & g_iReportNumber & ".csv"
		
		
		'Declare the FileSystem and File objects
		dim oFs, oFile
		set oFs = CreateObject("Scripting.FileSystemObject")
		set oFile = oFs.OpenTextFile(sFullFilePath, 2, true)
		
		'Write the file header
		oFile.WriteLine("Loan Officer Licensing " & g_iReportDays & " Day Expiration Report")
		oFile.WriteLine("Report Generated: " & Now())
		
		
		oFile.WriteLine("")
		
		dim sLine
		sLine = ""
		
		bFirstField = true

		if g_bOutputCompany <> "" then
			if not bOutputCompany then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Company"
		end if
		
		if g_bOutputBranch <> "" then
			if not bOutputBranch then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Branch"
		end if
		
		if g_bOutputLastName <> "" then
			if not bOutputLastName then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Last Name"
		end if
		
		if g_bOutputFirstName <> "" then
			if not bFirstField then
				sLine = sLine & ","
			else
				bFirstField = true
			end if		
			sLine = sLine & "First Name"
		end if
		
		if g_bOutputTitle <> "" then
			if not bOutputTitle then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Title"
		end if
	
		if g_bOutputSsn <> "" then
			if not bFirstField then
				sLine = sLine & ","
			else
				bFirstField = true
			end if		
			sLine = sLine & "SSN"
		end if
		
		if g_bOutputAddress <> "" then
			if not bOutputSsn then
				sLine = sLine & ","
			else
				bFirstField = true
			end if 
			sLine = sLine & "Address"
		end if
		
		if g_bOutputEmail <> "" then
			if not bOutputEmail then
				sLine = sLine & ","
			else
				bFirstField = true
			end if 
			sLine = sLine & "Email"
		end if
		
		if g_bOutputPhone <> "" then
			if not bOutputPhone then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Phone"
		end if
		
		oFile.WriteLine(sLine)

		
		'Loop through the recordset and write each record.
		if not oRs.EOF then
			do while not oRs.EOF

				bFirstField = true
				sLine = ""
				
				if g_bOutputCompany <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if 
					sLine = sLine & """" & oRs("CompanyName") & """"
				end if
				
				if g_bOutputBranch <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("BranchName") & """"
				end if
					
				if g_bOutputLastName <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("LastName") & """"
				end if
				
				if g_bOutputFirstName <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
								
					sLine = sLine & """" & oRs("FirstName")
					
					if g_bOutputMiddleName then				
						if trim(oRs("MiddleName")) <> "" and not isnull(oRs("MiddleName")) then
							sLine = sLine & " " & oRs("MiddleName") & "."
						end if
					end if 
					
					sLine = sLine & """"
				end if
				
				if g_bOutputTitle <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if 
					sLine = sLine & """" & oRs("Title") & """"
				end if
				
				if g_bOutputSsn <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("SSN") & """"
				end if
				
				if g_bOutputAddress <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Address") & " " & _
						oRs("Address2") & " " & oRs("City") & " " & _
						LookupStateId(oRs("StateId")) & " " & oRs("Zipcode") & """"
				end if
				
				if g_bOutputEmail <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Email") & """"
				end if 
				
				if g_bOutputPhone <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Phone")
					if oRs("PhoneExt") <> "" then
						sLine = sLine & " ext." & oRs("PhoneExt")
					end if
					sLine = sLine & """"
				end if 
				
				oFile.WriteLine(sLine)
				
				oRs.MoveNext
			loop
		else
			oFile.WriteLine("No Results Found")
		end if 

	end function


	' -------------------------------------------------------------------------
	' Name: WriteCSVBranchReport
	' Description: This method creates a report in CSV format and writes it out
	'			   to a file.
	' Preconditions: ConnectionString, ReportPath
	' Returns: Report filename if successful, empty string if failed.
	' -------------------------------------------------------------------------
	public function WriteCSVBranchReport()
		
		WriteCSVBranchReport = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		if not CheckStrParam(g_sReportPath, false, "Report Path")
			exit function
		end if 
		

		dim sTempDateText
		dim bFirstField
				
		'Get the report recordset.
		dim oRs
		set oRs = GetBranchReport()
		
		'ReportNumber is made up of the date and the branch ID if present
		g_iReportNumber = Year(Now) & Month(Now) & Day(Now) & Hour(Now) & _
			Minute(Now) & Second(Now) & g_iBranchId
			
		'Create the file path from the ReportNumber and the ReportPath
		dim sFullFilePath
		sFullFilePath = g_sReportPath & "report_" & g_iReportNumber & ".csv"
		sFileName = "report_" & g_iReportNumber & ".csv"
		
		
		'Declare the FileSystem and File objects
		dim oFs, oFile
		set oFs = CreateObject("Scripting.FileSystemObject")
		set oFile = oFs.OpenTextFile(sFullFilePath, 2, true)
		
		'Write the file header
		oFile.WriteLine("Branch Loan Officer Report")
		oFile.WriteLine("Report Generated: " & Now())
		
		
		oFile.WriteLine("")
		
		dim sLine
		sLine = ""
		
		bFirstField = true

		if g_bOutputCompany <> "" then
			if not bOutputCompany then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Company"
		end if
		
		if g_bOutputBranch <> "" then
			if not bOutputBranch then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Branch"
		end if
		
		if g_bOutputLastName <> "" then
			if not bOutputLastName then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Last Name"
		end if
		
		if g_bOutputFirstName <> "" then
			if not bFirstField then
				sLine = sLine & ","
			else
				bFirstField = true
			end if		
			sLine = sLine & "First Name"
		end if
		
		'if g_bOutputMiddleName <> "" then
		'	if not bFirstField then
		'		sLine = sLine & ","
		'	else
		'		bFirstField = true
		'	end if 
		'	sLine = sLine & "Initial"
		'end if 
		
		if g_bOutputTitle <> "" then
			if not bOutputTitle then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Title"
		end if
	
		if g_bOutputSsn <> "" then
			if not bFirstField then
				sLine = sLine & ","
			else
				bFirstField = true
			end if		
			sLine = sLine & "SSN"
		end if
		
		if g_bOutputAddress <> "" then
			if not bOutputSsn then
				sLine = sLine & ","
			else
				bFirstField = true
			end if 
			sLine = sLine & "Address"
		end if
		
		if g_bOutputEmail <> "" then
			if not bOutputEmail then
				sLine = sLine & ","
			else
				bFirstField = true
			end if 
			sLine = sLine & "Email"
		end if
		
		if g_bOutputPhone <> "" then
			if not bOutputPhone then
				sLine = sLine & ","
			else
				bFirstField = true
			end if
			sLine = sLine & "Phone"
		end if
		
		oFile.WriteLine(sLine)		

		
		'Loop through the recordset and write each record.
		if not oRs.EOF then
			do while not oRs.EOF

				bFirstField = true
				sLine = ""
				
				if g_bOutputCompany <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if 
					sLine = sLine & """" & oRs("CompanyName") & """"
				end if
				
				if g_bOutputBranch <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("BranchName") & """"
				end if
					
				if g_bOutputLastName <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("LastName") & """"
				end if
				
				if g_bOutputFirstName <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
								
					sLine = sLine & """" & oRs("FirstName")
					
					if g_bOutputMiddleName then				
						if trim(oRs("MiddleName")) <> "" and not isnull(oRs("MiddleName")) then
							sLine = sLine & " " & oRs("MiddleName") & "."
						end if
					end if 
					
					sLine = sLine & """"
				end if
				
				if g_bOutputTitle <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if 
					sLine = sLine & """" & oRs("Title") & """"
				end if
				
				if g_bOutputSsn <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("SSN") & """"
				end if
				
				if g_bOutputAddress <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Address") & " " & _
						oRs("Address2") & " " & oRs("City") & " " & _
						LookupStateId(oRs("StateId")) & " " & oRs("Zipcode") & """"
				end if
				
				if g_bOutputEmail <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Email") & """"
				end if 
				
				if g_bOutputPhone <> "" then
					if not bFirstField then
						sLine = sLine & ","
					else
						bFirstField = false
					end if
					sLine = sLine & """" & oRs("Phone")
					if oRs("PhoneExt") <> "" then
						sLine = sLine & " ext." & oRs("PhoneExt")
					end if
					sLine = sLine & """"
				end if 
				
				oFile.WriteLine(sLine)
				
				oRs.MoveNext
			loop
		else
			oFile.WriteLine("No Results Found")
		end if 

	end function
	
end class

%>
<%
'==============================================================================
' Class: SavedReport
' Controls the creation, modification, and removal of saved report
'	configurations
' Created: 4/10/06
' Authors: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- ReportId
'	- Name
'	- Deleted
'	- CompanyId
'	- LastName
'	- Ssn
'	- StateId
'	- BranchId
'	- Course
'	- ProviderId
'   - AccreditationTypeID
'	- CreditHours
'	- CourseCompletionDateTo
'	- CourseCompletionDateFrom
'	- CourseExpTo
'	- CourseExpFrom
'	- HireDateFrom
'	- HireDateTo
'	- LicenseNumber
'	- LicensePayment
'	- LicExpTo
'	- LicExpFrom
'	- AppDeadline
'	- LicenseStatusId
'	- ShowOfficers
'	- ShowCompanies
'	- ShowBranches
'	- ShowCompanyL2s
'	- ShowCompanyL3s
'	- ShowCourses
'	- ShowLicenses
'	- ShowInactive
'	- ShowBranchAdmins
'	- ShowLicIssued
'	- ShowAdmins
'	- ShowLicBranches
'	- ShowLicIssuedBranches
'	- ShowLicCompanyL3s
'	- ShowLicIssuedCompanyL3s
'	- ShowLicCompanyL2s
'	- ShowLicIssuedCompanyL2s
'	- ShowFirstName
'	- ShowSsn
'	- ShowNMLSNumber
'	- ShowEmployeeId
'	- ShowTitle
'	- ShowDepartment
'	- ShowPhone
'	- ShowEmail
'	- ShowWorkAddress
'	- ShowWorkCity
'	- ShowWorkState
'	- ShowWorkZip
' 	- ShowAddress
'	- ShowCity
'	- ShowState
'	- ShowZip
' 	- ShowAddress2Address
'	- ShowAddress2City
'	- ShowAddress2State
'	- ShowAddress2Zip
' 	- ShowAddress3Address
'	- ShowAddress3City
'	- ShowAddress3State
'	- ShowAddress3Zip
' 	- ShowAddress4Address
'	- ShowAddress4City
'	- ShowAddress4State
'	- ShowAddress4Zip
'   - ShowCompanyNotes
'   - ShowDivisionNotes
'   - ShowRegionNotes
'   - ShowBranchNotes
'	- ShowOfficerNotes
'   - ShowFingerprintDeadline
'	- ShowProviderName
'	- ShowProviderAddress
'	- ShowProviderPhoneNo
'	- ShowProviderContactName
'	- ShowProviderContactTitle
'	- ShowInstructorName
'	- ShowInstructorEducationLevel
'   - ShowBranchCustom1
'   - ShowBranchCustom2
'   - ShowBranchCustom3
'   - ShowBranchCustom4
'   - ShowBranchCustom5
'	- ShowOfficerCustom1
'	- ShowOfficerCustom2
'	- ShowOfficerCustom3
'	- ShowOfficerCustom4
'	- ShowOfficerCustom5
'	- ShowOfficerCustom6
'	- ShowOfficerCustom7
'	- ShowOfficerCustom8
'	- ShowOfficerCustom9
'	- ShowOfficerCustom10
'	- ShowOfficerCustom11
'	- ShowOfficerCustom12
'	- ShowOfficerCustom13
'	- ShowOfficerCustom14
'	- ShowOfficerCustom15
'   - ShowAgencyType
'   - ShowAgencyWebsite
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- CheckDateParam
'	- QueryDb
'	- ReportError
' Public Methods
'	- LoadReportById
'	- SaveReport
'	- SearchReports
'   - GetShowBranchCustom(p_iNo)
'   - SetShowBranchCustom(p_iNo,p_sValue)
'	- GetShowOfficerCustom(p_iNo)
'	- SetShowOfficerCustom(p_iNo,p_sValue)
'==============================================================================

class SavedReport

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Settings properties
	dim g_iReportId
	dim g_sName
	dim g_iCompanyId
	dim g_bDeleted
	dim g_sLastName
	dim g_sSsn
	dim g_iStateId
	dim g_iBranchId
	dim g_sStateIdList
	dim g_sLicStateIdList
	dim g_sCourseStateIdList
	dim g_sStructureIdList
	dim g_sCourse
	dim g_iProviderId
    dim g_iAccreditationTypeID
	dim g_rCreditHours
	dim g_dCourseCompletionDateTo
	dim g_dCourseCompletionDateFrom	
	dim g_dCourseExpTo
	dim g_dCourseExpFrom
	dim g_dHireDateFrom
	dim g_dHireDateTo
	dim g_sLicenseNumber
	dim g_sLicensePayment
	dim g_dLicExpTo
	dim g_dLicExpFrom
	dim g_iAppDeadline
	dim g_iLicenseStatusId
	dim g_sLicenseStatusIdList
	dim g_bShowOfficers
	dim g_bShowCompanies
	dim g_bShowBranches
	dim g_bShowCompanyL2s
	dim g_bShowCompanyL3s
	dim g_bShowCourses
	dim g_bShowLicenses
	dim g_bShowLicIssued
	dim g_bShowAdmins
	dim g_bShowLicBranches
	dim g_bShowLicIssuedBranches
	dim g_bShowLicCompanyL3s
	dim g_bShowLicIssuedCompanyL3s
	dim g_bShowLicCompanyL2s
	dim g_bShowLicIssuedCompanyL2s
	dim g_bShowFirstName
	dim g_bShowSsn
	dim g_bShowNMLSNumber
	dim g_bShowEmployeeId
	dim g_bShowTitle
	dim g_bShowDepartment
	dim g_bShowPhone
	dim g_bShowEmail
	dim g_bShowWorkAddress
	dim g_bShowWorkCity
	dim g_bShowWorkState
	dim g_bShowWorkZip
	dim g_bShowAddress
	dim g_bShowCity
	dim g_bShowState
	dim g_bShowZip
	dim g_bShowAddress2Address
	dim g_bShowAddress2City
	dim g_bShowAddress2State
	dim g_bShowAddress2Zip
	dim g_bShowAddress3Address
	dim g_bShowAddress3City
	dim g_bShowAddress3State
	dim g_bShowAddress3Zip
	dim g_bShowAddress4Address
	dim g_bShowAddress4City
	dim g_bShowAddress4State
	dim g_bShowAddress4Zip			
	dim g_bShowCompanyNotes
    dim g_bShowDivisionNotes
    dim g_bShowRegionNotes
    dim g_bShowBranchNotes
    dim g_bShowOfficerNotes
	dim g_bShowProviderName
	dim g_bShowProviderAddress
	dim g_bShowProviderPhoneNo
	dim g_bShowProviderContactName
	dim g_bShowProviderContactTitle
	dim g_bShowInstructorName
	dim g_bShowInstructorEducationLevel	
    dim g_bShowBranchCustom1
    dim g_bShowBranchCustom2
    dim g_bShowBranchCustom3
    dim g_bShowBranchCustom4
    dim g_bShowBranchCustom5
	dim g_bShowOfficerCustom1
	dim g_bShowOfficerCustom2
	dim g_bShowOfficerCustom3
	dim g_bShowOfficerCustom4
	dim g_bShowOfficerCustom5
	dim g_bShowOfficerCustom6
	dim g_bShowOfficerCustom7
	dim g_bShowOfficerCustom8
	dim g_bShowOfficerCustom9
	dim g_bShowOfficerCustom10
	dim g_bShowOfficerCustom11
	dim g_bShowOfficerCustom12
	dim g_bShowOfficerCustom13
	dim g_bShowOfficerCustom14
	dim g_bShowOfficerCustom15
	dim g_bShowOfficer
    dim g_bShowFingerprintDeadline
    dim g_bShowAgencyType
    dim g_bShowAgencyWebsite
	'dim g_bShowInactive
	dim g_iShowInactive
	dim g_iShowCompleted
	'dim g_iShowBranchAdmins
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property


	'Unique ID for this saved Report
	public property get ReportId()
		ReportId = g_iReportId
	end property
	
	
	'Name for this saved Report
	public property get Name()
		Name = g_sName
	end property
	public property let Name(p_sName)
		if p_sName = "" then 
			g_sName = "" 
		else
			g_sName = left(p_sName, 50)
		end if
	end property
	
	
	'ID of the Company that owns this saved report
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Boolean required.")
		end if
	end property
	
	
	'Has this saved record been deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = false
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	'Last name search
	public property get LastName()
		LastName = g_sLastName
	end property
	public property let LastName(p_sLastName)
		if p_sLastName = "" then 
			g_sLastName = "" 
		else
			g_sLastName = left(p_sLastName, 50)
		end if
	end property
	
	
	'SSN search
	public property get Ssn()
		Ssn = g_sSsn
	end property
	public property let Ssn(p_sSsn)
		if p_sSsn = "" then
			g_sSsn = ""
		else 
			g_sSsn = left(p_sSsn, 11)
		end if 
	end property
	
	
	'State ID search
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then	
			g_iStateId = clng(p_iStateId)
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Branch ID search
	public property get BranchId()
		BranchId = g_iBranchId
	end property 
	public property let BranchId(p_iBranchId)
		if isnumeric(p_iBranchId) then
			g_iBranchId = clng(p_iBranchId)
		elseif p_iBranchId = "" then
			g_iBranchId = ""
		else
			ReportError("Invalid BranchId value.  Integer required.")
		end if
	end property
	
	
	'State ID List search
	public property get StateIdList()
		StateIdList = g_sStateIdList
	end property
	public property let StateIdList(p_sStateIdList)
		g_sStateIdList = p_sStateIdList
	end property


	'License State ID List search
	public property get LicStateIdList()
		LicStateIdList = g_sLicStateIdList
	end property
	public property let LicStateIdList(p_sLicStateIdList)
		g_sLicStateIdList = p_sLicStateIdList
	end property


	'Course State ID List search
	public property get CourseStateIdList()
		CourseStateIdList = g_sCourseStateIdList
	end property
	public property let CourseStateIdList(p_sCourseStateIdList)
		g_sCourseStateIdList = p_sCourseStateIdList
	end property


	'Structure ID List search
	public property get StructureIdList()
		StructureIdList = g_sStructureIdList
	end property
	public property let StructureIdList(p_sStructureIdList)
		g_sStructureIdList = p_sStructureIdList
	end property
	
	
	'Course name search
	public property get Course()
		Course = g_sCourse
	end property
	public property let Course(p_sCourse)
		if p_sCourse = "" then
			g_sCourse = "" 
		else
			g_sCourse = left(p_sCourse, 50)
		end if
	end property
	
	
	'Provider ID search
	public property get ProviderId()
		ProviderId = g_iProviderId
	end property
	public property let ProviderId(p_iProviderId)
		if isnumeric(p_iProviderId) then
			g_iProviderId = clng(p_iProviderId)
		elseif p_iProviderId = "" then
			g_iProviderId = ""
		else
			ReportError("Invalid ProviderId value.  Integer required.")
		end if
	end property
	
	'Accreditation Type ID search
	public property get AccreditationTypeId()
		AccreditationTypeId = g_iAccreditationTypeId
	end property
	public property let AccreditationTypeId(p_iAccreditationTypeId)
		if isnumeric(p_iAccreditationTypeId) then
			g_iAccreditationTypeId = clng(p_iAccreditationTypeId)
		elseif p_iAccreditationTypeId = "" then
			g_iAccreditationTypeId = ""
		else
			ReportError("Invalid AccreditationTypeId value.  Integer required.")
		end if
	end property
	
	'Credit Hours search
	public property get CreditHours()
		CreditHours = g_rCreditHours
	end property
	public property let CreditHours(p_rCreditHours)
		if isnumeric(p_rCreditHours) then
			g_rCreditHours = cdbl(p_rCreditHours)
		elseif p_rCreditHours = "" then
			g_rCreditHours = ""
		else
			ReportError("Invalid CreditHours value.  Numeric value required.")
		end if
	end property		
	
	
	'Course CompletionDate To search
	public property get CourseCompletionDateTo()
		CourseCompletionDateTo = g_dCourseCompletionDateTo
	end property
	public property let CourseCompletionDateTo(p_dCourseCompletionDateTo)
		if isdate(p_dCourseCompletionDateTo) then
			g_dCourseCompletionDateTo = cdate(p_dCourseCompletionDateTo)
		elseif p_dCourseCompletionDateTo = "" then
			g_dCourseCompletionDateTo = ""
		else
			ReportError("Invalid CourseCompletionDateTo value.  Date required.")
		end if
	end property


	'Course CompletionDate From search
	public property get CourseCompletionDateFrom()
		CourseCompletionDateFrom = g_dCourseCompletionDateFrom
	end property
	public property let CourseCompletionDateFrom(p_dCourseCompletionDateFrom)
		if isdate(p_dCourseCompletionDateFrom) then
			g_dCourseCompletionDateFrom = cdate(p_dCourseCompletionDateFrom)
		elseif p_dCourseCompletionDateFrom = "" then
			g_dCourseCompletionDateFrom = ""
		else
			ReportError("Invalid CourseCompletionDateFrom value.  Date required.")
		end if
	end property		
	
	
	'Course Expiration To search
	public property get CourseExpTo()
		CourseExpTo = g_dCourseExpTo
	end property
	public property let CourseExpTo(p_dCourseExpTo)
		if isdate(p_dCourseExpTo) then
			g_dCourseExpTo = cdate(p_dCourseExpTo)
		elseif p_dCourseExpTo = "" then
			g_dCourseExpTo = ""
		else
			ReportError("Invalid CourseExpTo value.  Date required.")
		end if
	end property


	'Course Expiration From search
	public property get CourseExpFrom()
		CourseExpFrom = g_dCourseExpFrom
	end property
	public property let CourseExpFrom(p_dCourseExpFrom)
		if isdate(p_dCourseExpFrom) then
			g_dCourseExpFrom = cdate(p_dCourseExpFrom)
		elseif p_dCourseExpFrom = "" then
			g_dCourseExpFrom = ""
		else
			ReportError("Invalid CourseExpFrom value.  Date required.")
		end if
	end property			
	
	'Hire Date From search
	public property get HireDateFrom()
		HireDateFrom = g_dHireDateFrom
	end property
	public property let HireDateFrom(p_dHireDateFrom)
		if isdate(p_dHireDateFrom) then
			g_dHireDateFrom = cdate(p_dHireDateFrom)
		elseif p_dHireDateFrom = "" then
			g_dHireDateFrom = ""
		else
			ReportError("Invalid HireDateFrom value.  Date required.")
		end if
	end property			
	
	'Hire Date To search
	public property get HireDateTo()
		HireDateTo = g_dHireDateTo
	end property
	public property let HireDateTo(p_dHireDateTo)
		if isdate(p_dHireDateTo) then
			g_dHireDateTo = cdate(p_dHireDateTo)
		elseif p_dHireDateTo = "" then
			g_dHireDateTo = ""
		else
			ReportError("Invalid HireDateTo value.  Date required.")
		end if
	end property				
	
	'License Number search
	public property get LicenseNumber()
		LicenseNumber = g_sLicenseNumber
	end property
	public property let LicenseNumber(p_sLicenseNumber)
		if p_sLicenseNumber = "" then 
			g_sLicenseNumber = "" 
		else
			g_sLicenseNumber = left(p_sLicenseNumber, 50)
		end if
	end property
	
	'License Payment search
	public property get LicensePayment()
		LicensePayment = g_sLicensePayment
	end property
	public property let LicensePayment(p_sLicensePayment)
		if p_sLicensePayment = "" then 
			g_sLicensePayment = "" 
		else
			g_sLicensePayment = left(p_sLicensePayment, 50)
		end if
	end property
	
	
	'License Expiration To search
	public property get LicExpTo()
		LicExpTo = g_dLicExpTo
	end property
	public property let LicExpTo(p_dLicExpTo)
		if isdate(p_dLicExpTo) then
			g_dLicExpTo = cdate(p_dLicExpTo)
		elseif p_dLicExpTo = "" then
			g_dLicExpTo = ""
		else
			ReportError("Invalid LicExpTo value.  Date required.")
		end if
	end property


	'License Expiration From search
	public property get LicExpFrom()
		LicExpFrom = g_dLicExpFrom
	end property
	public property let LicExpFrom(p_dLicExpFrom)
		if isdate(p_dLicExpFrom) then
			g_dLicExpFrom = cdate(p_dLicExpFrom)
		elseif p_dLicExpFrom = "" then
			g_dLicExpFrom = ""
		else
			ReportError("Invalid LicExpFrom value.  Date required.")
		end if
	end property	
	
	
	'How are we searching for application deadlines?
	public property get AppDeadline()
		AppDeadline = g_iAppDeadline
	end property
	public property let AppDeadline(p_iAppDeadline)
		if isnumeric(p_iAppDeadline) then
			g_iAppDeadline = clng(p_iAppDeadline)
		elseif p_iAppDeadline = "" then
			g_iAppDeadline = ""
		else
			ReportError("Invalid AppDeadline value.  Integer required.")
		end if 
	end property
	
	
	'License Status ID search
	public property get LicenseStatusId()
		LicenseStatusId = g_iLicenseStatusId
	end property
	public property let LicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = clng(p_iLicenseStatusId)
		elseif p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid LicenseStatusId value.  Integer required.")
		end if
	end property
	
	
	'List of License Status IDs to search within.  Used to find licenses
	'matching any of several status types.
	public property get LicenseStatusIdList()
		LicenseStatusIdList = g_sLicenseStatusIdList
	end property
	public property let LicenseStatusIdList(p_sLicenseStatusIdList)
		g_sLicenseStatusIdList = p_sLicenseStatusIdList
	end property
		
	
	'Do we show officers in the search?
	public property get ShowOfficers()
		ShowOfficers = g_bShowOfficers
	end property
	public property let ShowOfficers(p_bShowOfficers)
		if isnumeric(p_bShowOfficers) then
			g_bShowOfficers = cbool(p_bShowOfficers)
		elseif p_bShowOfficers = "" then
			g_bShowOfficers = false
		else
			ReportError("Invalid ShowOfficers value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companies in the search?
	public property get ShowCompanies()
		ShowCompanies = g_bShowCompanies
	end property
	public property let ShowCompanies(p_bShowCompanies)
		if isnumeric(p_bShowCompanies) then
			g_bShowCompanies = cbool(p_bShowCompanies)
		elseif p_bShowCompanies = "" then
			g_bShowCompanies = false
		else
			ReportError("Invalid ShowCompanies value.  Boolean required.")
		end if
	end property
	
	
	'Do we show branches in the search?
	public property get ShowBranches()
		ShowBranches = g_bShowBranches
	end property
	public property let ShowBranches(p_bShowBranches)
		if isnumeric(p_bShowBranches) then
			g_bShowBranches = cbool(p_bShowBranches)
		elseif p_bShowBranches = "" then
			g_bShowBranches = false
		else
			ReportError("Invalid ShowBranches value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companyl2s in the search?
	public property get ShowCompanyL2s()
		ShowCompanyL2s = g_bShowCompanyL2s
	end property
	public property let ShowCompanyL2s(p_bShowCompanyL2s)
		if isnumeric(p_bShowCompanyL2s) then
			g_bShowCompanyL2s = cbool(p_bShowCompanyL2s)
		elseif p_bShowCompanyL2s = "" then
			g_bShowCompanyL2s = false
		else
			ReportError("Invalid ShowCompanyL2s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companyl3s in the search?
	public property get ShowCompanyL3s()
		ShowCompanyL3s = g_bShowCompanyL3s
	end property
	public property let ShowCompanyL3s(p_bShowCompanyL3s)
		if isnumeric(p_bShowCompanyL3s) then
			g_bShowCompanyL3s = cbool(p_bShowCompanyL3s)
		elseif p_bShowCompanyL3s = "" then
			g_bShowCompanyL3s = false
		else
			ReportError("Invalid ShowCompanyL3s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show courses in the search?
	public property get ShowCourses()
		ShowCourses = g_bShowCourses
	end property
	public property let ShowCourses(p_bShowCourses)
		if isnumeric(p_bShowCourses) then
			g_bShowCourses = cbool(p_bShowCourses)
		elseif p_bShowCourses = "" then
			g_bShowCourses = false
		else
			ReportError("Invalid ShowCourses value.  Boolean required.")
		end if
	end property
	
	
	'Do we show licenses in the search?
	public property get ShowLicenses()
		ShowLicenses = g_bShowLicenses
	end property
	public property let ShowLicenses(p_bShowLicenses)
		if isnumeric(p_bShowLicenses) then
			g_bShowLicenses = cbool(p_bShowLicenses)
		elseif p_bShowLicenses = "" then
			g_bShowLicenses = false
		else
			ReportError("Invalid ShowLicenses value.  Boolean required.")
		end if
	end property
	
	
	'Do we show LicIssued in the search?
	public property get ShowLicIssued()
		ShowLicIssued = g_bShowLicIssued
	end property
	public property let ShowLicIssued(p_bShowLicIssued)
		if isnumeric(p_bShowLicIssued) then
			g_bShowLicIssued = cbool(p_bShowLicIssued)
		elseif p_bShowLicIssued = "" then
			g_bShowLicIssued = false
		else
			ReportError("Invalid ShowLicIssued value.  Boolean required.")
		end if
	end property
	
	
	'Do we show only inactive officers in the search?
	public property get ShowInactive()
		ShowInactive = g_iShowInactive
	end property
	public property let ShowInactive(p_iShowInactive)
		if isnumeric(p_iShowInactive) then
			g_iShowInactive = cint(p_iShowInactive)
		elseif p_iShowInactive = "" then
			g_iShowInactive = ""
		else
			ReportError("Invalid ShowInactive value.  Integer required.")
		end if
	end property
	
	
	'Do we show only Completed courses in the search?
	public property get ShowCompleted()
		ShowCompleted = g_iShowCompleted
	end property
	public property let ShowCompleted(p_iShowCompleted)
		if isnumeric(p_iShowCompleted) then
			g_iShowCompleted = cint(p_iShowCompleted)
		elseif p_iShowCompleted = "" then
			g_iShowCompleted = ""
		else
			ReportError("Invalid ShowCompleted value.  Integer required.")
		end if
	end property
		
	
	'Do we show Admins in the search?
	public property get ShowAdmins()
		ShowAdmins = g_bShowAdmins
	end property
	public property let ShowAdmins(p_bShowAdmins)
		if isnumeric(p_bShowAdmins) then
			g_bShowAdmins = cbool(p_bShowAdmins)
		elseif p_bShowAdmins = "" then
			g_bShowAdmins = false
		else
			ReportError("Invalid ShowAdmins value.  Boolean required.")
		end if
	end property
	
	
	'Do we show branch licenses in the search?
	public property get ShowLicBranches()
		ShowLicBranches = g_bShowLicBranches
	end property
	public property let ShowLicBranches(p_bShowLicBranches)
		if isnumeric(p_bShowLicBranches) then
			g_bShowLicBranches = cbool(p_bShowLicBranches)
		elseif p_bShowLicBranches = "" then
			g_bShowLicBranches = false
		else
			ReportError("Invalid ShowLicBranches value.  Boolean required.")
		end if
	end property
	
	
	'Do we show branch license issue dates in the search?
	public property get ShowLicIssuedBranches()
		ShowLicIssuedBranches = g_bShowLicIssuedBranches
	end property
	public property let ShowLicIssuedBranches(p_bShowLicIssuedBranches)
		if isnumeric(p_bShowLicIssuedBranches) then
			g_bShowLicIssuedBranches = cbool(p_bShowLicIssuedBranches)
		elseif p_bShowLicIssuedBranches = "" then
			g_bShowLicIssuedBranches = false
		else
			ReportError("Invalid ShowLicIssuedBranches value.  Boolean required.")
		end if
	end property
	

	'Do we show l3 licenses in the search?
	public property get ShowLicCompanyL3s()
		ShowLicCompanyL3s = g_bShowLicCompanyL3s
	end property
	public property let ShowLicCompanyL3s(p_bShowLicCompanyL3s)
		if isnumeric(p_bShowLicCompanyL3s) then
			g_bShowLicCompanyL3s = cbool(p_bShowLicCompanyL3s)
		elseif p_bShowLicCompanyL3s = "" then
			g_bShowLicCompanyL3s = false
		else
			ReportError("Invalid ShowLicCompanyL3s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show l3 license issue dates in the search?
	public property get ShowLicIssuedCompanyL3s()
		ShowLicIssuedCompanyL3s = g_bShowLicIssuedCompanyL3s
	end property
	public property let ShowLicIssuedCompanyL3s(p_bShowLicIssuedCompanyL3s)
		if isnumeric(p_bShowLicIssuedCompanyL3s) then
			g_bShowLicIssuedCompanyL3s = cbool(p_bShowLicIssuedCompanyL3s)
		elseif p_bShowLicIssuedCompanyL3s = "" then
			g_bShowLicIssuedCompanyL3s = false
		else
			ReportError("Invalid ShowLicIssuedCompanyL3s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show l2 licenses in the search?
	public property get ShowLicCompanyL2s()
		ShowLicCompanyL2s = g_bShowLicCompanyL2s
	end property
	public property let ShowLicCompanyL2s(p_bShowLicCompanyL2s)
		if isnumeric(p_bShowLicCompanyL2s) then
			g_bShowLicCompanyL2s = cbool(p_bShowLicCompanyL2s)
		elseif p_bShowLicCompanyL2s = "" then
			g_bShowLicCompanyL2s = false
		else
			ReportError("Invalid ShowLicCompanyL2s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show l2 license issue dates in the search?
	public property get ShowLicIssuedCompanyL2s()
		ShowLicIssuedCompanyL2s = g_bShowLicIssuedCompanyL2s
	end property
	public property let ShowLicIssuedCompanyL2s(p_bShowLicIssuedCompanyL2s)
		if isnumeric(p_bShowLicIssuedCompanyL2s) then
			g_bShowLicIssuedCompanyL2s = cbool(p_bShowLicIssuedCompanyL2s)
		elseif p_bShowLicIssuedCompanyL2s = "" then
			g_bShowLicIssuedCompanyL2s = false
		else
			ReportError("Invalid ShowLicIssuedCompanyL2s value.  Boolean required.")
		end if
	end property	
	
	
	'Do we show FirstName in the search?
	public property get ShowFirstName()
		ShowFirstName = g_bShowFirstName
	end property
	public property let ShowFirstName(p_bShowFirstName)
		if isnumeric(p_bShowFirstName) then
			g_bShowFirstName = cbool(p_bShowFirstName)
		elseif p_bShowFirstName = "" then
			g_bShowFirstName = false
		else
			ReportError("Invalid ShowFirstName value.  Boolean required.")
		end if
	end property
	
	
	'Do we show SSNs in the search?
	public property get ShowSsn()
		ShowSsn = g_bShowSsn
	end property
	public property let ShowSsn(p_bShowSsn)
		if isnumeric(p_bShowSsn) then
			g_bShowSsn = cbool(p_bShowSsn)
		elseif p_bShowSsn = "" then
			g_bShowSsn = false
		else
			ReportError("Invalid ShowSsn value.  Boolean required.")
		end if
	end property

	'Do we show NMLSNumber in the search?
	public property get ShowNMLSNumber()
		ShowNMLSNumber = g_bShowNMLSNumber
	end property
	public property let ShowNMLSNumber(p_bShowNMLSNumber)
		if isnumeric(p_bShowNMLSNumber) then
			g_bShowNMLSNumber = cbool(p_bShowNMLSNumber)
		elseif p_bShowNMLSNumber = "" then
			g_bShowNMLSNumber = false
		else
			ReportError("Invalid ShowNMLSNumber value.  Boolean required.")
		end if
	end property
	
	'Do we show EmployeeId in the search?
	public property get ShowEmployeeId()
		ShowEmployeeId = g_bShowEmployeeId
	end property
	public property let ShowEmployeeId(p_bShowEmployeeId)
		if isnumeric(p_bShowEmployeeId) then
			g_bShowEmployeeId = cbool(p_bShowEmployeeId)
		elseif p_bShowEmployeeId = "" then
			g_bShowEmployeeId = false
		else
			ReportError("Invalid ShowEmployeeId value.  Boolean required.")
		end if
	end property

	'Do we show Title in the search?
	public property get ShowTitle()
		ShowTitle = g_bShowTitle
	end property
	public property let ShowTitle(p_bShowTitle)
		if isnumeric(p_bShowTitle) then
			g_bShowTitle = cbool(p_bShowTitle)
		elseif p_bShowTitle = "" then
			g_bShowTitle = false
		else
			ReportError("Invalid ShowTitle value.  Boolean required.")
		end if
	end property	
	
	'Do we show Department in the search?
	public property get ShowDepartment()
		ShowDepartment = g_bShowDepartment
	end property
	public property let ShowDepartment(p_bShowDepartment)
		if isnumeric(p_bShowDepartment) then
			g_bShowDepartment = cbool(p_bShowDepartment)
		elseif p_bShowDepartment = "" then
			g_bShowDepartment = false
		else
			ReportError("Invalid ShowDepartment value.  Boolean required.")
		end if
	end property	
	
	'Do we show Phone numbers in the search?
	public property get ShowPhone()
		ShowPhone = g_bShowPhone
	end property
	public property let ShowPhone(p_bShowPhone)
		if isnumeric(p_bShowPhone) then
			g_bShowPhone = cbool(p_bShowPhone)
		elseif p_bShowPhone = "" then
			g_bShowPhone = false
		else
			ReportError("Invalid ShowPhone value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Email in the search?
	public property get ShowEmail()
		ShowEmail = g_bShowEmail
	end property
	public property let ShowEmail(p_bShowEmail)
		if isnumeric(p_bShowEmail) then
			g_bShowEmail = cbool(p_bShowEmail)
		elseif p_bShowEmail = "" then
			g_bShowEmail = false
		else
			ReportError("Invalid ShowEmail value.  Boolean required.")
		end if
	end property
	
	
	'Do we show WorkAddress in the search?
	public property get ShowWorkAddress()
		ShowWorkAddress = g_bShowWorkAddress
	end property
	public property let ShowWorkAddress(p_bShowWorkAddress)
		if isnumeric(p_bShowWorkAddress) then
			g_bShowWorkAddress = cbool(p_bShowWorkAddress)
		elseif p_bShowWorkAddress = "" then
			g_bShowWorkAddress = false
		else
			ReportError("Invalid ShowWorkAddress value.  Boolean required.")
		end if
	end property
	
	
	'Do we show WorkCity in the search?
	public property get ShowWorkCity()
		ShowWorkCity = g_bShowWorkCity
	end property
	public property let ShowWorkCity(p_bShowWorkCity)
		if isnumeric(p_bShowWorkCity) then
			g_bShowWorkCity = cbool(p_bShowWorkCity)
		elseif p_bShowWorkCity = "" then
			g_bShowWorkCity = false
		else
			ReportError("Invalid ShowWorkCity value.  Boolean required.")
		end if
	end property
	
	
	'Do we show WorkState in the search?
	public property get ShowWorkState()
		ShowWorkState = g_bShowWorkState
	end property
	public property let ShowWorkState(p_bShowWorkState)
		if isnumeric(p_bShowWorkState) then
			g_bShowWorkState = cbool(p_bShowWorkState)
		elseif p_bShowWorkState = "" then
			g_bShowWorkState = false
		else
			ReportError("Invalid ShowWorkState value.  Boolean required.")
		end if
	end property
	
	
	'Do we show WorkZip in the search?
	public property get ShowWorkZip()
		ShowWorkZip = g_bShowWorkZip
	end property
	public property let ShowWorkZip(p_bShowWorkZip)
		if isnumeric(p_bShowWorkZip) then
			g_bShowWorkZip = cbool(p_bShowWorkZip)
		elseif p_bShowWorkZip = "" then
			g_bShowWorkZip = false
		else
			ReportError("Invalid ShowWorkZip value.  Boolean required.")
		end if
	end property


	'Do we show Address in the search?
	public property get ShowAddress()
		ShowAddress = g_bShowAddress
	end property
	public property let ShowAddress(p_bShowAddress)
		if isnumeric(p_bShowAddress) then
			g_bShowAddress = cbool(p_bShowAddress)
		elseif p_bShowAddress = "" then
			g_bShowAddress = false
		else
			ReportError("Invalid ShowAddress value.  Boolean required.")
		end if
	end property
	
	
	'Do we show City in the search?
	public property get ShowCity()
		ShowCity = g_bShowCity
	end property
	public property let ShowCity(p_bShowCity)
		if isnumeric(p_bShowCity) then
			g_bShowCity = cbool(p_bShowCity)
		elseif p_bShowCity = "" then
			g_bShowCity = false
		else
			ReportError("Invalid ShowCity value.  Boolean required.")
		end if
	end property
	
	
	'Do we show State in the search?
	public property get ShowState()
		ShowState = g_bShowState
	end property
	public property let ShowState(p_bShowState)
		if isnumeric(p_bShowState) then
			g_bShowState = cbool(p_bShowState)
		elseif p_bShowState = "" then
			g_bShowState = false
		else
			ReportError("Invalid ShowState value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Zip in the search?
	public property get ShowZip()
		ShowZip = g_bShowZip
	end property
	public property let ShowZip(p_bShowZip)
		if isnumeric(p_bShowZip) then
			g_bShowZip = cbool(p_bShowZip)
		elseif p_bShowZip = "" or isnull(p_bShowZip) then
			g_bShowZip = false
		else
			ReportError("Invalid ShowZip value.  Boolean required.")
		end if
	end property

	
	'Do we show Address in the search?
	public property get ShowAddress2Address()
		ShowAddress2Address = g_bShowAddress2Address
	end property
	public property let ShowAddress2Address(p_bShowAddress2Address)
		if isnumeric(p_bShowAddress2Address) then
			g_bShowAddress2Address = cbool(p_bShowAddress2Address)
		elseif p_bShowAddress2Address = "" then
			g_bShowAddress2Address = false
		else
			ReportError("Invalid ShowAddress2Address value.  Boolean required.")
		end if
	end property
	
	
	'Do we show City in the search?
	public property get ShowAddress2City()
		ShowAddress2City = g_bShowAddress2City
	end property
	public property let ShowAddress2City(p_bShowAddress2City)
		if isnumeric(p_bShowAddress2City) then
			g_bShowAddress2City = cbool(p_bShowAddress2City)
		elseif p_bShowAddress2City = "" then
			g_bShowAddress2City = false
		else
			ReportError("Invalid ShowAddress2City value.  Boolean required.")
		end if
	end property
	
	
	'Do we show State in the search?
	public property get ShowAddress2State()
		ShowAddress2State = g_bShowAddress2State
	end property
	public property let ShowAddress2State(p_bShowAddress2State)
		if isnumeric(p_bShowAddress2State) then
			g_bShowAddress2State = cbool(p_bShowAddress2State)
		elseif p_bShowAddress2State = "" then
			g_bShowAddress2State = false
		else
			ReportError("Invalid ShowAddress2State value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Zip in the search?
	public property get ShowAddress2Zip()
		ShowAddress2Zip = g_bShowAddress2Zip
	end property
	public property let ShowAddress2Zip(p_bShowAddress2Zip)
		if isnumeric(p_bShowAddress2Zip) then
			g_bShowAddress2Zip = cbool(p_bShowAddress2Zip)
		elseif p_bShowAddress2Zip = "" or isnull(p_bShowAddress2Zip) then
			g_bShowAddress2Zip = false
		else
			ReportError("Invalid ShowAddress2Zip value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Address in the search?
	public property get ShowAddress3Address()
		ShowAddress3Address = g_bShowAddress3Address
	end property
	public property let ShowAddress3Address(p_bShowAddress3Address)
		if isnumeric(p_bShowAddress3Address) then
			g_bShowAddress3Address = cbool(p_bShowAddress3Address)
		elseif p_bShowAddress3Address = "" then
			g_bShowAddress3Address = false
		else
			ReportError("Invalid ShowAddress3Address value.  Boolean required.")
		end if
	end property
	
	
	'Do we show City in the search?
	public property get ShowAddress3City()
		ShowAddress3City = g_bShowAddress3City
	end property
	public property let ShowAddress3City(p_bShowAddress3City)
		if isnumeric(p_bShowAddress3City) then
			g_bShowAddress3City = cbool(p_bShowAddress3City)
		elseif p_bShowAddress3City = "" then
			g_bShowAddress3City = false
		else
			ReportError("Invalid ShowAddress3City value.  Boolean required.")
		end if
	end property
	
	
	'Do we show State in the search?
	public property get ShowAddress3State()
		ShowAddress3State = g_bShowAddress3State
	end property
	public property let ShowAddress3State(p_bShowAddress3State)
		if isnumeric(p_bShowAddress3State) then
			g_bShowAddress3State = cbool(p_bShowAddress3State)
		elseif p_bShowAddress3State = "" then
			g_bShowAddress3State = false
		else
			ReportError("Invalid ShowAddress3State value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Zip in the search?
	public property get ShowAddress3Zip()
		ShowAddress3Zip = g_bShowAddress3Zip
	end property
	public property let ShowAddress3Zip(p_bShowAddress3Zip)
		if isnumeric(p_bShowAddress3Zip) then
			g_bShowAddress3Zip = cbool(p_bShowAddress3Zip)
		elseif p_bShowAddress3Zip = "" or isnull(p_bShowAddress3Zip) then
			g_bShowAddress3Zip = false
		else
			ReportError("Invalid ShowAddress3Zip value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Address in the search?
	public property get ShowAddress4Address()
		ShowAddress4Address = g_bShowAddress4Address
	end property
	public property let ShowAddress4Address(p_bShowAddress4Address)
		if isnumeric(p_bShowAddress4Address) then
			g_bShowAddress4Address = cbool(p_bShowAddress4Address)
		elseif p_bShowAddress4Address = "" then
			g_bShowAddress4Address = false
		else
			ReportError("Invalid ShowAddress4Address value.  Boolean required.")
		end if
	end property
	
	
	'Do we show City in the search?
	public property get ShowAddress4City()
		ShowAddress4City = g_bShowAddress4City
	end property
	public property let ShowAddress4City(p_bShowAddress4City)
		if isnumeric(p_bShowAddress4City) then
			g_bShowAddress4City = cbool(p_bShowAddress4City)
		elseif p_bShowAddress4City = "" then
			g_bShowAddress4City = false
		else
			ReportError("Invalid ShowAddress4City value.  Boolean required.")
		end if
	end property
	
	
	'Do we show State in the search?
	public property get ShowAddress4State()
		ShowAddress4State = g_bShowAddress4State
	end property
	public property let ShowAddress4State(p_bShowAddress4State)
		if isnumeric(p_bShowAddress4State) then
			g_bShowAddress4State = cbool(p_bShowAddress4State)
		elseif p_bShowAddress4State = "" then
			g_bShowAddress4State = false
		else
			ReportError("Invalid ShowAddress4State value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Zip in the search?
	public property get ShowAddress4Zip()
		ShowAddress4Zip = g_bShowAddress4Zip
	end property
	public property let ShowAddress4Zip(p_bShowAddress4Zip)
		if isnumeric(p_bShowAddress4Zip) then
			g_bShowAddress4Zip = cbool(p_bShowAddress4Zip)
		elseif p_bShowAddress4Zip = "" or isnull(p_bShowAddress4Zip) then
			g_bShowAddress4Zip = false
		else
			ReportError("Invalid ShowAddress4Zip value.  Boolean required.")
		end if
	end property
	
	
	'Do we show Company Notes in the search?
	public property get ShowCompanyNotes()
		ShowCompanyNotes = g_bShowCompanyNotes
	end property
	public property let ShowCompanyNotes(p_bShowCompanyNotes)
		if isnumeric(p_bShowCompanyNotes) then
			g_bShowCompanyNotes = cbool(p_bShowCompanyNotes)
		elseif p_bShowCompanyNotes = "" or isnull(p_bShowCompanyNotes) then
			g_bShowCompanyNotes = false
		else
			ReportError("Invalid ShowCompanyNotes value.  Boolean required.")
		end if
	end property	

	'Do we show Division Notes in the search?
	public property get ShowDivisionNotes()
		ShowDivisionNotes = g_bShowDivisionNotes
	end property
	public property let ShowDivisionNotes(p_bShowDivisionNotes)
		if isnumeric(p_bShowDivisionNotes) then
			g_bShowDivisionNotes = cbool(p_bShowDivisionNotes)
		elseif p_bShowDivisionNotes = "" or isnull(p_bShowDivisionNotes) then
			g_bShowDivisionNotes = false
		else
			ReportError("Invalid ShowDivisionNotes value.  Boolean required.")
		end if
	end property	

	'Do we show Region Notes in the search?
	public property get ShowRegionNotes()
		ShowRegionNotes = g_bShowRegionNotes
	end property
	public property let ShowRegionNotes(p_bShowRegionNotes)
		if isnumeric(p_bShowRegionNotes) then
			g_bShowRegionNotes = cbool(p_bShowRegionNotes)
		elseif p_bShowRegionNotes = "" or isnull(p_bShowRegionNotes) then
			g_bShowRegionNotes = false
		else
			ReportError("Invalid ShowRegionNotes value.  Boolean required.")
		end if
	end property	

	'Do we show Branch Notes in the search?
	public property get ShowBranchNotes()
		ShowBranchNotes = g_bShowBranchNotes
	end property
	public property let ShowBranchNotes(p_bShowBranchNotes)
		if isnumeric(p_bShowBranchNotes) then
			g_bShowBranchNotes = cbool(p_bShowBranchNotes)
		elseif p_bShowBranchNotes = "" or isnull(p_bShowBranchNotes) then
			g_bShowBranchNotes = false
		else
			ReportError("Invalid ShowBranchNotes value.  Boolean required.")
		end if
	end property	

	'Do we show Officer Notes in the search?
	public property get ShowOfficerNotes()
		ShowOfficerNotes = g_bShowOfficerNotes
	end property
	public property let ShowOfficerNotes(p_bShowOfficerNotes)
		if isnumeric(p_bShowOfficerNotes) then
			g_bShowOfficerNotes = cbool(p_bShowOfficerNotes)
		elseif p_bShowOfficerNotes = "" or isnull(p_bShowOfficerNotes) then
			g_bShowOfficerNotes = false
		else
			ReportError("Invalid ShowOfficerNotes value.  Boolean required.")
		end if
	end property	
	
	'Do we show Officer Notes in the search?
	public property get ShowFingerprintDeadline()
		ShowFingerprintDeadline = g_bShowFingerprintDeadline
	end property
	public property let ShowFingerprintDeadline(p_bShowFingerprintDeadline)
		if isnumeric(p_bShowFingerprintDeadline) then
			g_bShowFingerprintDeadline = cbool(p_bShowFingerprintDeadline)
		elseif p_bShowFingerprintDeadline = "" or isnull(p_bShowFingerprintDeadline) then
			g_bShowFingerprintDeadline = false
		else
			ReportError("Invalid ShowFingerprintDeadline value.  Boolean required.")
		end if
	end property	

	public property get ShowAgencyType()
		ShowAgencyType = g_bShowAgencyType
	end property
	public property let ShowAgencyType(p_bShowAgencyType)
		if isnumeric(p_bShowAgencyType) then
			g_bShowAgencyType = cbool(p_bShowAgencyType)
		elseif p_bShowAgencyType = "" or isnull(p_bShowAgencyType) then
			g_bShowAgencyType = false
		else
			ReportError("Invalid ShowAgencyType value.  Boolean required.")
		end if
	end property	

	public property get ShowAgencyWebsite()
		ShowAgencyWebsite = g_bShowAgencyWebsite
	end property
	public property let ShowAgencyWebsite(p_bShowAgencyWebsite)
		if isnumeric(p_bShowAgencyWebsite) then
			g_bShowAgencyWebsite = cbool(p_bShowAgencyWebsite)
		elseif p_bShowAgencyWebsite = "" or isnull(p_bShowAgencyWebsite) then
			g_bShowAgencyWebsite = false
		else
			ReportError("Invalid ShowAgencyWebsite value.  Boolean required.")
		end if
	end property	

	'Do we show ProviderName in the search?
	public property get ShowProviderName()
		ShowProviderName = g_bShowProviderName
	end property
	public property let ShowProviderName(p_bShowProviderName)
		if isnumeric(p_bShowProviderName) then
			g_bShowProviderName = cbool(p_bShowProviderName)
		elseif p_bShowProviderName = "" then
			g_bShowProviderName = false
		else
			ReportError("Invalid ShowProviderName value.  Boolean required.")
		end if
	end property
	
	'Do we show ProviderAddress in the search?
	public property get ShowProviderAddress()
		ShowProviderAddress = g_bShowProviderAddress
	end property
	public property let ShowProviderAddress(p_bShowProviderAddress)
		if isnumeric(p_bShowProviderAddress) then
			g_bShowProviderAddress = cbool(p_bShowProviderAddress)
		elseif p_bShowProviderAddress = "" then
			g_bShowProviderAddress = false
		else
			ReportError("Invalid ShowProviderAddress value.  Boolean required.")
		end if
	end property
	
	'Do we show ProviderPhoneNo in the search?
	public property get ShowProviderPhoneNo()
		ShowProviderPhoneNo = g_bShowProviderPhoneNo
	end property
	public property let ShowProviderPhoneNo(p_bShowProviderPhoneNo)
		if isnumeric(p_bShowProviderPhoneNo) then
			g_bShowProviderPhoneNo = cbool(p_bShowProviderPhoneNo)
		elseif p_bShowProviderPhoneNo = "" then
			g_bShowProviderPhoneNo = false
		else
			ReportError("Invalid ShowProviderPhoneNo value.  Boolean required.")
		end if
	end property
	
	'Do we show ProviderContactName in the search?
	public property get ShowProviderContactName()
		ShowProviderContactName = g_bShowProviderContactName
	end property
	public property let ShowProviderContactName(p_bShowProviderContactName)
		if isnumeric(p_bShowProviderContactName) then
			g_bShowProviderContactName = cbool(p_bShowProviderContactName)
		elseif p_bShowProviderContactName = "" then
			g_bShowProviderContactName = false
		else
			ReportError("Invalid ShowProviderContactName value.  Boolean required.")
		end if
	end property
	
	'Do we show ProviderContactTitle in the search?
	public property get ShowProviderContactTitle()
		ShowProviderContactTitle = g_bShowProviderContactTitle
	end property
	public property let ShowProviderContactTitle(p_bShowProviderContactTitle)
		if isnumeric(p_bShowProviderContactTitle) then
			g_bShowProviderContactTitle = cbool(p_bShowProviderContactTitle)
		elseif p_bShowProviderContactTitle = "" then
			g_bShowProviderContactTitle = false
		else
			ReportError("Invalid ShowProviderContactTitle value.  Boolean required.")
		end if
	end property

	'Do we show InstructorName in the search?
	public property get ShowInstructorName()
		ShowInstructorName = g_bShowInstructorName
	end property
	public property let ShowInstructorName(p_bShowInstructorName)
		if isnumeric(p_bShowInstructorName) then
			g_bShowInstructorName = cbool(p_bShowInstructorName)
		elseif p_bShowInstructorName = "" then
			g_bShowInstructorName = false
		else
			ReportError("Invalid ShowInstructorName value.  Boolean required.")
		end if
	end property

	'Do we show InstructorEducationLevel in the search?
	public property get ShowInstructorEducationLevel()
		ShowInstructorEducationLevel = g_bShowInstructorEducationLevel
	end property
	public property let ShowInstructorEducationLevel(p_bShowInstructorEducationLevel)
		if isnumeric(p_bShowInstructorEducationLevel) then
			g_bShowInstructorEducationLevel = cbool(p_bShowInstructorEducationLevel)
		elseif p_bShowInstructorEducationLevel = "" then
			g_bShowInstructorEducationLevel = false
		else
			ReportError("Invalid ShowInstructorEducationLevel value.  Boolean required.")
		end if
	end property
	
	'Do we show Branch Custom Field 1 in the search?
	public property get ShowBranchCustom1()
		ShowBranchCustom1 = g_bShowBranchCustom1
	end property
	public property let ShowBranchCustom1(p_bShowBranchCustom1)
		if isnumeric(p_bShowBranchCustom1) then
			g_bShowBranchCustom1 = cbool(p_bShowBranchCustom1)
		elseif p_bShowBranchCustom1 = "" or isnull(p_bShowBranchCustom1) then
			g_bShowBranchCustom1 = false
		else
			ReportError("Invalid ShowBranchCustom1 value.  Boolean required.")
		end if
	end property

	'Do we show Branch Custom Field 2 in the search?
	public property get ShowBranchCustom2()
		ShowBranchCustom2 = g_bShowBranchCustom2
	end property
	public property let ShowBranchCustom2(p_bShowBranchCustom2)
		if isnumeric(p_bShowBranchCustom2) then
			g_bShowBranchCustom2 = cbool(p_bShowBranchCustom2)
		elseif p_bShowBranchCustom2 = "" or isnull(p_bShowBranchCustom2) then
			g_bShowBranchCustom2 = false
		else
			ReportError("Invalid ShowBranchCustom2 value.  Boolean required.")
		end if
	end property

	'Do we show Branch Custom Field 3 in the search?
	public property get ShowBranchCustom3()
		ShowBranchCustom3 = g_bShowBranchCustom3
	end property
	public property let ShowBranchCustom3(p_bShowBranchCustom3)
		if isnumeric(p_bShowBranchCustom3) then
			g_bShowBranchCustom3 = cbool(p_bShowBranchCustom3)
		elseif p_bShowBranchCustom3 = "" or isnull(p_bShowBranchCustom3) then
			g_bShowBranchCustom3 = false
		else
			ReportError("Invalid ShowBranchCustom3 value.  Boolean required.")
		end if
	end property

	'Do we show Branch Custom Field 4 in the search?
	public property get ShowBranchCustom4()
		ShowBranchCustom4 = g_bShowBranchCustom4
	end property
	public property let ShowBranchCustom4(p_bShowBranchCustom4)
		if isnumeric(p_bShowBranchCustom4) then
			g_bShowBranchCustom4 = cbool(p_bShowBranchCustom4)
		elseif p_bShowBranchCustom4 = "" or isnull(p_bShowBranchCustom4) then
			g_bShowBranchCustom4 = false
		else
			ReportError("Invalid ShowBranchCustom4 value.  Boolean required.")
		end if
	end property

	'Do we show Branch Custom Field 5 in the search?
	public property get ShowBranchCustom5()
		ShowBranchCustom5 = g_bShowBranchCustom5
	end property
	public property let ShowBranchCustom5(p_bShowBranchCustom5)
		if isnumeric(p_bShowBranchCustom5) then
			g_bShowBranchCustom5 = cbool(p_bShowBranchCustom5)
		elseif p_bShowBranchCustom5 = "" or isnull(p_bShowBranchCustom5) then
			g_bShowBranchCustom5 = false
		else
			ReportError("Invalid ShowBranchCustom5 value.  Boolean required.")
		end if
	end property

	'Do we show Officer Custom Field 1 in the search?
	public property get ShowOfficerCustom1()
		ShowOfficerCustom1 = g_bShowOfficerCustom1
	end property
	public property let ShowOfficerCustom1(p_bShowOfficerCustom1)
		if isnumeric(p_bShowOfficerCustom1) then
			g_bShowOfficerCustom1 = cbool(p_bShowOfficerCustom1)
		elseif p_bShowOfficerCustom1 = "" or isnull(p_bShowOfficerCustom1) then
			g_bShowOfficerCustom1 = false
		else
			ReportError("Invalid ShowOfficerCustom1 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 2 in the search?
	public property get ShowOfficerCustom2()
		ShowOfficerCustom2 = g_bShowOfficerCustom2
	end property
	public property let ShowOfficerCustom2(p_bShowOfficerCustom2)
		if isnumeric(p_bShowOfficerCustom2) then
			g_bShowOfficerCustom2 = cbool(p_bShowOfficerCustom2)
		elseif p_bShowOfficerCustom2 = "" or isnull(p_bShowOfficerCustom2) then
			g_bShowOfficerCustom2 = false
		else
			ReportError("Invalid ShowOfficerCustom2 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 3 in the search?
	public property get ShowOfficerCustom3()
		ShowOfficerCustom3 = g_bShowOfficerCustom3
	end property
	public property let ShowOfficerCustom3(p_bShowOfficerCustom3)
		if isnumeric(p_bShowOfficerCustom3) then
			g_bShowOfficerCustom3 = cbool(p_bShowOfficerCustom3)
		elseif p_bShowOfficerCustom3 = "" or isnull(p_bShowOfficerCustom3) then
			g_bShowOfficerCustom3 = false
		else
			ReportError("Invalid ShowOfficerCustom3 value.  Boolean required.")
		end if
	end property	
	
	'Do we show Officer Custom Field 4 in the search?
	public property get ShowOfficerCustom4()
		ShowOfficerCustom4 = g_bShowOfficerCustom4
	end property
	public property let ShowOfficerCustom4(p_bShowOfficerCustom4)
		if isnumeric(p_bShowOfficerCustom4) then
			g_bShowOfficerCustom4 = cbool(p_bShowOfficerCustom4)
		elseif p_bShowOfficerCustom4 = "" or isnull(p_bShowOfficerCustom4) then
			g_bShowOfficerCustom4 = false
		else
			ReportError("Invalid ShowOfficerCustom4 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 5 in the search?
	public property get ShowOfficerCustom5()
		ShowOfficerCustom5 = g_bShowOfficerCustom5
	end property
	public property let ShowOfficerCustom5(p_bShowOfficerCustom5)
		if isnumeric(p_bShowOfficerCustom5) then
			g_bShowOfficerCustom5 = cbool(p_bShowOfficerCustom5)
		elseif p_bShowOfficerCustom5 = "" or isnull(p_bShowOfficerCustom5) then
			g_bShowOfficerCustom5 = false
		else
			ReportError("Invalid ShowOfficerCustom5 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 6 in the search?
	public property get ShowOfficerCustom6()
		ShowOfficerCustom6 = g_bShowOfficerCustom6
	end property
	public property let ShowOfficerCustom6(p_bShowOfficerCustom6)
		if isnumeric(p_bShowOfficerCustom6) then
			g_bShowOfficerCustom6 = cbool(p_bShowOfficerCustom6)
		elseif p_bShowOfficerCustom6 = "" or isnull(p_bShowOfficerCustom6) then
			g_bShowOfficerCustom6 = false
		else
			ReportError("Invalid ShowOfficerCustom6 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 7 in the search?
	public property get ShowOfficerCustom7()
		ShowOfficerCustom7 = g_bShowOfficerCustom7
	end property
	public property let ShowOfficerCustom7(p_bShowOfficerCustom7)
		if isnumeric(p_bShowOfficerCustom7) then
			g_bShowOfficerCustom7 = cbool(p_bShowOfficerCustom7)
		elseif p_bShowOfficerCustom7 = "" or isnull(p_bShowOfficerCustom7) then
			g_bShowOfficerCustom7 = false
		else
			ReportError("Invalid ShowOfficerCustom7 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 8 in the search?
	public property get ShowOfficerCustom8()
		ShowOfficerCustom8 = g_bShowOfficerCustom8
	end property
	public property let ShowOfficerCustom8(p_bShowOfficerCustom8)
		if isnumeric(p_bShowOfficerCustom8) then
			g_bShowOfficerCustom8 = cbool(p_bShowOfficerCustom8)
		elseif p_bShowOfficerCustom8 = "" or isnull(p_bShowOfficerCustom8) then
			g_bShowOfficerCustom8 = false
		else
			ReportError("Invalid ShowOfficerCustom8 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 9 in the search?
	public property get ShowOfficerCustom9()
		ShowOfficerCustom9 = g_bShowOfficerCustom9
	end property
	public property let ShowOfficerCustom9(p_bShowOfficerCustom9)
		if isnumeric(p_bShowOfficerCustom9) then
			g_bShowOfficerCustom9 = cbool(p_bShowOfficerCustom9)
		elseif p_bShowOfficerCustom9 = "" or isnull(p_bShowOfficerCustom9) then
			g_bShowOfficerCustom9 = false
		else
			ReportError("Invalid ShowOfficerCustom9 value.  Boolean required.")
		end if
	end property	
	
	'Do we show Officer Custom Field 10 in the search?
	public property get ShowOfficerCustom10()
		ShowOfficerCustom10 = g_bShowOfficerCustom10
	end property
	public property let ShowOfficerCustom10(p_bShowOfficerCustom10)
		if isnumeric(p_bShowOfficerCustom10) then
			g_bShowOfficerCustom10 = cbool(p_bShowOfficerCustom10)
		elseif p_bShowOfficerCustom10 = "" or isnull(p_bShowOfficerCustom10) then
			g_bShowOfficerCustom10 = false
		else
			ReportError("Invalid ShowOfficerCustom10 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 11 in the search?
	public property get ShowOfficerCustom11()
		ShowOfficerCustom11 = g_bShowOfficerCustom11
	end property
	public property let ShowOfficerCustom11(p_bShowOfficerCustom11)
		if isnumeric(p_bShowOfficerCustom11) then
			g_bShowOfficerCustom11 = cbool(p_bShowOfficerCustom11)
		elseif p_bShowOfficerCustom11 = "" or isnull(p_bShowOfficerCustom11) then
			g_bShowOfficerCustom11 = false
		else
			ReportError("Invalid ShowOfficerCustom11 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 12 in the search?
	public property get ShowOfficerCustom12()
		ShowOfficerCustom12 = g_bShowOfficerCustom12
	end property
	public property let ShowOfficerCustom12(p_bShowOfficerCustom12)
		if isnumeric(p_bShowOfficerCustom12) then
			g_bShowOfficerCustom12 = cbool(p_bShowOfficerCustom12)
		elseif p_bShowOfficerCustom12 = "" or isnull(p_bShowOfficerCustom12) then
			g_bShowOfficerCustom12 = false
		else
			ReportError("Invalid ShowOfficerCustom12 value.  Boolean required.")
		end if
	end property

	'Do we show Officer Custom Field 13 in the search?
	public property get ShowOfficerCustom13()
		ShowOfficerCustom13 = g_bShowOfficerCustom13
	end property
	public property let ShowOfficerCustom13(p_bShowOfficerCustom13)
		if isnumeric(p_bShowOfficerCustom13) then
			g_bShowOfficerCustom13 = cbool(p_bShowOfficerCustom13)
		elseif p_bShowOfficerCustom13 = "" or isnull(p_bShowOfficerCustom13) then
			g_bShowOfficerCustom13 = false
		else
			ReportError("Invalid ShowOfficerCustom13 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 14 in the search?
	public property get ShowOfficerCustom14()
		ShowOfficerCustom14 = g_bShowOfficerCustom14
	end property
	public property let ShowOfficerCustom14(p_bShowOfficerCustom14)
		if isnumeric(p_bShowOfficerCustom14) then
			g_bShowOfficerCustom14 = cbool(p_bShowOfficerCustom14)
		elseif p_bShowOfficerCustom14 = "" or isnull(p_bShowOfficerCustom14) then
			g_bShowOfficerCustom14 = false
		else
			ReportError("Invalid ShowOfficerCustom14 value.  Boolean required.")
		end if
	end property
	
	'Do we show Officer Custom Field 15 in the search?
	public property get ShowOfficerCustom15()
		ShowOfficerCustom15 = g_bShowOfficerCustom15
	end property
	public property let ShowOfficerCustom15(p_bShowOfficerCustom15)
		if isnumeric(p_bShowOfficerCustom15) then
			g_bShowOfficerCustom15 = cbool(p_bShowOfficerCustom15)
		elseif p_bShowOfficerCustom15 = "" or isnull(p_bShowOfficerCustom15) then
			g_bShowOfficerCustom15 = false
		else
			ReportError("Invalid ShowOfficerCustom15 value.  Boolean required.")
		end if
	end property
	
	' PRIVATE METHODS =========================================================

	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	



	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadReportById
	' Desc: Load a report object based on the passed ReportId
	' Preconditions: ConnectionString
	' Inputs: p_iReportId - ID of report to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadReportById(p_iReportId)
	
		LoadReportById = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iReportId, 0, "Report ID") then
			exit function
		end if
		
		if p_iReportId = "-1" then
			'Load a custom report for 0-30 day license expirations
			g_iReportId = "-1"
			g_sName = "License Expirations in 30 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = date
			g_dLicExpTo = date + 30
			g_iAppDeadline = 1
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "1,2,4,5,10,11,12,18"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false			
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false		
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false							
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false
            g_bShowBranchCustom2 = false
            g_bShowBranchCustom3 = false
            g_bShowBranchCustom4 = false
            g_bShowBranchCustom5 = false
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""
			g_sLicenseNumber = ""
			g_sLicensePayment = ""
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-2" then
			'Load a custom report for 30-60 day license expirations
			g_iReportId = "-2"
			g_sName = "License Expirations in 30-60 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = date + 30
			g_dLicExpTo = date + 60
			g_iAppDeadline = 1
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "1,2,4,5,10,11,12,18"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNOtes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-3" then
			'Load a custom report for 60-90 day license expirations
			g_iReportId = "-3"
			g_sName = "License Expirations in 60-90 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = date + 60
			g_dLicExpTo = date + 90
			g_iAppDeadline = 1
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "1,2,4,5,10,11,12,18"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-4" then
			'Load a custom report for 0-30 day course expirations
			g_iReportId = "-4"
			g_sName = "Course Expirations in 30 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dCourseExpFrom = date
			g_dCourseExpTo = date + 30
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_bShowOfficers = true
			g_bShowCompanies = false
			g_bShowBranches = false
			g_bShowCompanyL2s = false
			g_bShowCompanyL3s = false
			g_bShowCourses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = false
			g_bShowLicCompanyL3s = false
			g_bShowLicCompanyL2s = false
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = ""
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""						
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_dLicExpTo = ""
			g_dLicExpFrom = ""
			g_iAppDeadline = ""
			g_bShowLicenses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-5" then
			'Load a custom report for 30-60 day course expirations
			g_iReportId = "-5"
			g_sName = "Course Expirations in 30-60 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dCourseExpFrom = date +30
			g_dCourseExpTo = date + 60
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_bShowOfficers = true
			g_bShowCompanies = false
			g_bShowBranches = false
			g_bShowCompanyL2s = false
			g_bShowCompanyL3s = false
			g_bShowCourses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = false
			g_bShowLicCompanyL3s = false
			g_bShowLicCompanyL2s = false
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = ""
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""						
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_dLicExpTo = ""
			g_dLicExpFrom = ""
			g_iAppDeadline = ""
			g_bShowLicenses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-6" then
			'Load a custom report for 60-90 day course expirations
			g_iReportId = "-6"
			g_sName = "Course Expirations in 60-90 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dCourseExpFrom = date + 60
			g_dCourseExpTo = date + 90
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_bShowOfficers = true
			g_bShowCompanies = false
			g_bShowBranches = false
			g_bShowCompanyL2s = false
			g_bShowCompanyL3s = false
			g_bShowCourses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = false
			g_bShowLicCompanyL3s = false
			g_bShowLicCompanyL2s = false
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = ""
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID  = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_dLicExpTo = ""
			g_dLicExpFrom = ""
			g_iAppDeadline = ""
			g_bShowLicenses = false
			LoadReportById = g_iReportId
			exit function

		elseif p_iReportId = "-7" then
			'Load a custom report for pending license renewals
			g_iReportId = "-7"
			g_sName = "Pending Licenses"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = ""
			g_dLicExpTo = ""
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "10,22,23,24,25"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""						
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_iAppDeadline = ""
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-8" then
			'Load a custom report for active licenses
			g_iReportId = "-8"
			g_sName = "Active Licenses"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = date()
			g_dLicExpTo = ""
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "1,2,4,5,10,11,12,18"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_iAppDeadline = ""
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-9" then
			'Load a custom report for -60-0 day license expirations
			g_iReportId = "-1"
			g_sName = "Licenses Expired in Last 60 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dLicExpFrom = date - 60
			g_dLicExpTo = date
			g_iAppDeadline = 1
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = "" '"1,2,4,5,10,11,12,18"
			g_bShowOfficers = true
			g_bShowCompanies = true
			g_bShowBranches = true
			g_bShowCompanyL2s = true
			g_bShowCompanyL3s = true
			g_bShowLicenses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = true
			g_bShowLicCompanyL3s = true
			g_bShowLicCompanyL2s = true
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_dCourseExpFrom = ""
			g_dCourseExpTo = ""
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_bShowCourses = false
			LoadReportById = g_iReportId
			exit function
			
		elseif p_iReportId = "-10" then
			'Load a custom report for -60-0 day course expirations
			g_iReportId = "-6"
			g_sName = "Courses Expired in the Last 60 Days"
			g_iCompanyId = session("UserCompanyId")
			g_bDeleted = false
			g_dCourseExpFrom = date - 60
			g_dCourseExpTo = date
			g_dHireDateFrom = ""
			g_dHireDateTo = ""			
			g_bShowOfficers = true
			g_bShowCompanies = false
			g_bShowBranches = false
			g_bShowCompanyL2s = false
			g_bShowCompanyL3s = false
			g_bShowCourses = true
			g_iShowInactive = 0
			g_iShowCompleted = 0
			g_bShowAdmins = false
			g_bShowLicBranches = false
			g_bShowLicCompanyL3s = false
			g_bShowLicCompanyL2s = false
			g_bShowLicIssued = false
			g_bShowLicIssuedBranches = false
			g_bShowLicIssuedCompanyL3s = false
			g_bShowLicIssuedCompanyL2s = false
			g_bShowFirstName = true
			g_bShowSsn = false
			g_bShowEmail = false
			g_bShowNMLSNumber = false
			g_bShowEmployeeId = false
			g_bShowEmail = false
			g_bShowTitle = false
			g_bShowDepartment = false						
			g_bShowPhone = false
			g_bShowWorkAddress = false
			g_bShowWorkCity = false
			g_bShowWorkState = false
			g_bShowWorkZip = false
			g_bShowAddress = false
			g_bShowCity = false
			g_bShowState = false
			g_bShowZip = false
			g_bShowAddress2Address = false
			g_bShowAddress2City = false
			g_bShowAddress2State = false
			g_bShowAddress2Zip = false
			g_bShowAddress3Address = false
			g_bShowAddress3City = false
			g_bShowAddress3State = false
			g_bShowAddress3Zip = false
			g_bShowAddress4Address = false
			g_bShowAddress4City = false
			g_bShowAddress4State = false
			g_bShowAddress4Zip = false					
            g_bShowCompanyNotes = false
            g_bShowDivisionNotes = false
            g_bShowRegionNotes = false
            g_bShowBranchNotes = false
			g_bShowOfficerNotes = false
            g_bShowFingerprintDeadline = false
            g_bShowAgencyType = false
            g_bShowAgencyWebsite = false
			g_bShowProviderName = false
			g_bShowProviderAddress = false
			g_bShowProviderPhoneNo = false
			g_bShowProviderContactName = false
			g_bShowProviderContactTitle = false
			g_bShowInstructorName = false
			g_bShowInstructorEducationLevel = false
            g_bShowBranchCustom1 = false	
            g_bShowBranchCustom2 = false	
            g_bShowBranchCustom3 = false	
            g_bShowBranchCustom4 = false	
            g_bShowBranchCustom5 = false	
			g_bShowOfficerCustom1 = false	
			g_bShowOfficerCustom2 = false
			g_bShowOfficerCustom3 = false	
			g_bShowOfficerCustom4 = false
			g_bShowOfficerCustom5 = false	
			g_bShowOfficerCustom6 = false
			g_bShowOfficerCustom7 = false	
			g_bShowOfficerCustom8 = false
			g_bShowOfficerCustom9 = false
			g_bShowOfficerCustom10 = false	
			g_bShowOfficerCustom11 = false
			g_bShowOfficerCustom12 = false	
			g_bShowOfficerCustom13 = false
			g_bShowOfficerCustom14 = false	
			g_bShowOfficerCustom15 = false
			
			g_iLicenseStatusId = ""
			g_sLicenseStatusIdList = ""
			'g_sName = ""
			g_sLastName = ""
			g_sSsn = ""
			g_iStateId = ""
			g_iBranchId = ""
			g_sStateIdList = ""
			g_sLicStateIdList = ""
			g_sCourseStateIdList = ""
			g_sStructureIdList = ""
			g_sCourse = ""
			g_iProviderId = ""
            g_iAccreditationTypeID = ""
			g_rCreditHours = ""
			g_dCourseCompletionDateFrom = ""
			g_dCourseCompletionDateTo = ""			
			g_sLicenseNumber = ""
			g_sLicensePayment = ""			
			g_dLicExpTo = ""
			g_dLicExpFrom = ""
			g_iAppDeadline = ""
			g_bShowLicenses = false
			LoadReportById = g_iReportId
			exit function
			
		end if
		
		
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM SavedReports " & _
			"WHERE ReportId = '" & p_iReportId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the properties
			g_iReportId = oRs("ReportId")
			g_sName = oRs("Name")
			g_iCompanyId = oRs("CompanyId")
			g_bDeleted = oRs("Deleted")
			g_sLastName = oRs("LastName")
			g_sSsn = oRs("Ssn")
			g_iStateId = oRs("StateId")
			g_iBranchId = oRs("BranchId")
			g_sStateIdList = oRs("StateIdList")
			g_sLicStateIdList = oRs("LicStateIdList")
			g_sCourseStateIdList = oRs("CourseStateIdList")
			g_sStructureIdList = oRs("StructureIdList")
			g_sCourse = oRs("Course")
			g_iProviderId = oRs("ProviderId")
            g_iAccreditationTypeID = oRs("AccreditationTypeID")
			g_rCreditHours = oRs("CreditHours")
			g_dCourseCompletionDateFrom = oRs("CourseCompletionDateFrom")
			g_dCourseCompletionDateTo = oRs("CourseCompletionDateTo")		
			g_dCourseExpFrom = oRs("CourseExpFrom")
			g_dCourseExpTo = oRs("CourseExpTo")
			g_dHireDateFrom = oRs("HireDateFrom")
			g_dHireDateTo = oRs("HireDateTo")
			g_sLicenseNumber = oRs("LicenseNumber")
			g_sLicensePayment = oRs("LicensePayment")
			g_dLicExpTo = oRs("LicExpTo")
			g_dLicExpFrom = oRs("LicExpFrom")
			g_iAppDeadline = oRs("AppDeadline")
			g_iLicenseStatusId = oRs("LicenseStatusId")
			g_sLicenseStatusIdList = oRs("LicenseStatusIdList")
			g_bShowOfficers = oRs("ShowOfficers")
			g_bShowCompanies = oRs("ShowCompanies")
			g_bShowBranches = oRs("ShowBranches")
			g_bShowCompanyL2s = oRs("ShowCompanyL2s")
			g_bShowCompanyL3s = oRs("ShowCompanyL3s")
			g_bShowCourses = oRs("ShowCourses")
			g_bShowLicenses = oRs("ShowLicenses")
			g_bShowLicIssued = oRs("ShowLicIssued")
            g_iShowInactive = oRs("ShowInactive")
			g_iShowCompleted = oRs("ShowCompleted")
			g_bShowAdmins = oRs("ShowAdmins")

			g_bShowLicBranches = oRs("ShowLicBranches")
			g_bShowLicCompanyL3s = oRs("ShowLicCompanyL3s")
			g_bShowLicCompanyL2s = oRs("ShowLicCompanyL2s")
			g_bShowLicIssuedBranches = oRs("ShowLicIssuedBranches")
			g_bShowLicIssuedCompanyL3s = oRs("ShowLicIssuedCompanyL3s")
			g_bShowLicIssuedCompanyL2s = oRs("ShowLicIssuedCompanyL2s")
			g_bShowFirstName = oRs("ShowFirstName")
			g_bShowSsn = oRs("ShowSsn")
			g_bShowEmail = oRs("ShowEmail")
			g_bShowNMLSNumber = oRs("ShowNMLSNumber")
			g_bShowEmployeeId = oRs("ShowEmployeeId")
			g_bShowEmail = oRs("ShowEmail")
			g_bShowTitle = oRs("ShowTitle")
			g_bShowDepartment = oRs("ShowDepartment")	
			g_bShowPhone = oRs("ShowPhone")
			g_bShowWorkAddress = oRs("ShowWorkAddress")
			g_bShowWorkCity = oRs("ShowWorkCity")
			g_bShowWorkState = oRs("ShowWorkState")
			g_bShowWorkZip = oRs("ShowWorkZip")
			g_bShowAddress = oRs("ShowAddress")
			g_bShowCity = oRs("ShowCity")
			g_bShowState = oRs("ShowState")
			g_bShowZip = oRs("ShowZip")
			g_bShowAddress2Address = oRs("ShowAddress2Address")
			g_bShowAddress2City = oRs("ShowAddress2City")
			g_bShowAddress2State = oRs("ShowAddress2State")
			g_bShowAddress2Zip = oRs("ShowAddress2Zip")
			g_bShowAddress3Address = oRs("ShowAddress3Address")
			g_bShowAddress3City = oRs("ShowAddress3City")
			g_bShowAddress3State = oRs("ShowAddress3State")
			g_bShowAddress3Zip = oRs("ShowAddress3Zip")
			g_bShowAddress4Address = oRs("ShowAddress4Address")
			g_bShowAddress4City = oRs("ShowAddress4City")
			g_bShowAddress4State = oRs("ShowAddress4State")
			g_bShowAddress4Zip = oRs("ShowAddress4Zip")									
            g_bShowCompanyNotes = oRs("ShowCompanyNotes")
            g_bShowDivisionNotes = oRs("ShowDivisionNotes")
            g_bShowRegionNotes = oRs("ShowRegionNotes")
            g_bShowBranchNotes = oRs("ShowBranchNotes")
			g_bShowOfficerNotes = oRs("ShowOfficerNotes")
            g_bShowFingerprintDeadline = oRs("ShowFingerprintDeadline")
            g_bShowAgencyType = oRs("ShowAgencyType")
            g_bShowAgencyWebsite = oRs("ShowAgencyWebsite")
			g_bShowProviderName = oRs("ShowProviderName")
			g_bShowProviderAddress = oRs("ShowProviderAddress")
			g_bShowProviderPhoneNo = oRs("ShowProviderPhoneNo")
			g_bShowProviderContactName = oRs("ShowProviderContactName")
			g_bShowProviderContactTitle = oRs("ShowProviderContactTitle")
			g_bShowInstructorName = oRs("ShowInstructorName")
			g_bShowInstructorEducationLevel = oRs("ShowInstructorEducationLevel")
			g_bShowBranchCustom1 = oRs("ShowBranchCustom1")
            g_bShowBranchCustom2 = oRs("ShowBranchCustom2")
            g_bShowBranchCustom3 = oRs("ShowBranchCustom3")
            g_bShowBranchCustom4 = oRs("ShowBranchCustom4")
            g_bShowBranchCustom5 = oRs("ShowBranchCustom5")
			g_bShowOfficerCustom1 = oRs("ShowOfficerCustom1")
			g_bShowOfficerCustom2 = oRs("ShowOfficerCustom2")
			g_bShowOfficerCustom3 = oRs("ShowOfficerCustom3")	
			g_bShowOfficerCustom4 = oRs("ShowOfficerCustom4")
			g_bShowOfficerCustom5 = oRs("ShowOfficerCustom5")	
			g_bShowOfficerCustom6 = oRs("ShowOfficerCustom6")
			g_bShowOfficerCustom7 = oRs("ShowOfficerCustom7")	
			g_bShowOfficerCustom8 = oRs("ShowOfficerCustom8")
			g_bShowOfficerCustom9 = oRs("ShowOfficerCustom9")
			g_bShowOfficerCustom10 = oRs("ShowOfficerCustom10")	
			g_bShowOfficerCustom11 = oRs("ShowOfficerCustom11")
			g_bShowOfficerCustom12 = oRs("ShowOfficerCustom12")	
			g_bShowOfficerCustom13 = oRs("ShowOfficerCustom13")
			g_bShowOfficerCustom14 = oRs("ShowOfficerCustom14")	
			g_bShowOfficerCustom15 = oRs("ShowOfficerCustom15")
			
			LoadReportById = g_iReportId
			
		else
			
			ReportError("Unable to load the passed Settings ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveReport
	' Desc: Save the object's properties to the database.
	' Preconditions: ConnectionString, g_iCompanyId
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveReport()
		
		SaveReport = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iCompanyId, 0, "Company ID") then
			exit function
		end if
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		'Find out if a Report with this name already exists
		sSql = "SELECT ReportId FROM SavedReports " & _
			"WHERE Name LIKE '" & safe(g_sName) & "' " & _
			"AND CompanyId = '" & g_iCompanyId & "'"
		set oRs = oConn.Execute(sSql)
		if not (oRs.EOF and oRs.BOF) then
			g_iReportId = oRs("ReportId")
		end if 
		
				
		'If we have a ReportId property assigned already, we're working with a
		'loaded Report, so we'll UPDATE.  Otherwise, we INSERT.
		if g_iReportId <> "" then
			
			sSql = "UPDATE SavedReports SET " & _
				"Name = '" & g_sName & "', " & _
				"CompanyId = '" & g_iCompanyId & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "', " & _
				"LastName = '" & g_sLastName & "', " & _
				"Ssn = '" & g_sSsn & "', " & _
				"StateId = '" & g_iStateId & "', " & _
				"BranchId = '" & g_iBranchId & "', " & _
				"StateIdList = '" & g_sStateIdList & "', " & _
				"LicStateIdList = '" & g_sLicStateIdList & "', " & _
				"CourseStateIdList = '" & g_sCourseStateIdList & "', " & _
				"StructureIdList = '" & g_sStructureIdList & "', " & _
				"AppDeadline = '" & g_iAppDeadline & "', " & _
				"Course = '" & g_sCourse & "', "
			if g_iProviderId <> "" then
				sSql = sSql & "ProviderId = '" & g_iProviderId & "', "
			else
				sSql = sSql & "ProviderId = NULL, "
			end if
            if g_iAccreditationTypeID <> "" then
                sSql = sSql & "AccreditationTypeID = '" & g_iAccreditationTypeID & "', "
            else
                sSql = sSql & "AccreditationTypeID = NULL, "
            end if
            sSql = sSql & "CreditHours = '" & g_rCreditHours & "', " & _
				"LicenseNumber = '" & g_sLicenseNumber & "', "
			if g_dCourseCompletionDateTo <> "" then
				sSql = sSql & "CourseCompletionDateTo = '" & g_dCourseCompletionDateTo & "', "
			else
				sSql = sSql & "CourseCompletionDateTo = NULL, "
			end if
			if g_dCourseCompletionDateFrom <> "" then	
				sSql = sSql & "CourseCompletionDateFrom = '" & g_dCourseCompletionDateFrom & "', "
			else
				sSql = sSql & "CourseCompletionDateFrom = NULL, "
			end if
			if g_dCourseExpTo <> "" then
				sSql = sSql & "CourseExpTo = '" & g_dCourseExpTo & "', "
			else
				sSql = sSql & "CourseExpTo = NULL, "
			end if
			if g_dCourseExpFrom <> "" then	
				sSql = sSql & "CourseExpFrom = '" & g_dCourseExpFrom & "', "
			else
				sSql = sSql & "CourseExpFrom = NULL, "
			end if
			if g_dHireDateFrom <> "" then	
				sSql = sSql & "HireDateFrom = '" & g_dHireDateFrom & "', "
			else
				sSql = sSql & "HireDateFrom = NULL, "
			end if			
			if g_dHireDateTo <> "" then	
				sSql = sSql & "HireDateTo = '" & g_dHireDateTo & "', "
			else
				sSql = sSql & "HireDateTo = NULL, "
			end if						
			if g_dLicExpTo <> "" then
				sSql = sSql & "LicExpTo = '" & g_dLicExpTo & "', "
			else
				sSql = sSql & "LicExpTo = NULL, "
			end if
			if g_dLicExpFrom <> "" then	
				sSql = sSql & "LicExpFrom = '" & g_dLicExpFrom & "', "
			else
				sSql = sSql & "LicexpFrom = NULL, "
			end if
			if g_iLicenseStatusId <> "" then 
				sSql = sSql & "LicenseStatusId = '" & g_iLicenseStatusId & _
					"', "
			else 
				sSql = sSql & "LicenseStatusId = NULL, "
			end if
			sSql = sSql & _
				"LicensePayment = '" & g_sLicensePayment & "', " & _
				"LicenseStatusIdList = '" & g_sLicenseStatusIdList & "', " & _
				"ShowOfficers = '" & abs(cint(g_bShowOfficers)) & "', " & _
				"ShowCompanies = '" & abs(cint(g_bShowCompanies)) & "', " & _
				"ShowBranches = '" & abs(cint(g_bShowBranches)) & "', " & _
				"ShowCompanyL2s = '" & abs(cint(g_bShowCompanyL2s)) & "', " & _
				"ShowCompanyL3s = '" & abs(cint(g_bShowCompanyL3s)) & "', " & _
				"ShowCourses = '" & abs(cint(g_bShowCourses)) & "', " & _
				"ShowLicenses = '" & abs(cint(g_bShowLicenses)) & "', " & _
				"ShowLicIssued = '" & abs(cint(g_bShowLicIssued)) & "', "
            if g_iShowInactive <> "" then
				ssql = ssql & "ShowInactive = '" & g_iShowInactive & "', " 
            else
                ssql = ssql & "ShowInactive = NULL, "
            end if
            if g_iShowCompleted <> "" then
				ssql = ssql & "ShowCompleted = '" & g_iShowCompleted & "', "
            else
                ssql = ssql & "ShowCompleted = NULL, " 
            end if
	        ssql = ssql & "ShowAdmins = '" & abs(cint(g_bShowAdmins)) & "', " & _
				"ShowLicBranches = '" & abs(cint(g_bShowLicBranches)) & "', " & _
				"ShowLicIssuedBranches = '" & abs(cint(g_bShowLicIssuedBranches)) & "', " & _
				"ShowLicCompanyL2s = '" & abs(cint(g_bShowLicCompanyL2s)) & "', " & _
				"ShowLicIssuedCompanyL2s = '" & abs(cint(g_bShowLicIssuedCompanyL2s)) & "', " & _
				"ShowLicCompanyL3s = '" & abs(cint(g_bShowLicCompanyL3s)) & "', " & _
				"ShowLicIssuedCompanyL3s = '" & abs(cint(g_bShowLicIssuedCompanyL3s)) & "', " & _
				"ShowFirstName = '" & abs(cint(g_bShowFirstName)) & "', " & _
				"ShowSsn = '" & abs(cint(g_bShowSsn)) & "', " & _
				"ShowNMLSNumber = '" & abs(cint(g_bShowNMLSNumber)) & "', " & _
				"ShowEmployeeId = '" & abs(cint(g_bShowEmployeeId)) & "', " & _
				"ShowTitle = '" & abs(cint(g_bShowTitle)) & "', " & _
				"ShowDepartment = '" & abs(cint(g_bShowDepartment)) & "', " & _
				"ShowPhone = '" & abs(cint(g_bShowPhone)) & "', " & _
				"ShowEmail = '" & abs(cint(g_bShowEmail)) & "', " & _
				"ShowWorkAddress = '" & abs(cint(g_bShowWorkAddress)) & "', " & _
				"ShowWorkCity = '" & abs(cint(g_bShowWorkCity)) & "', " & _
				"ShowWorkState = '" & abs(cint(g_bShowWorkState)) & "', " & _
				"ShowWorkZip = '" & abs(cint(g_bShowWorkZip)) & "', " & _
				"ShowAddress = '" & abs(cint(g_bShowAddress)) & "', " & _
				"ShowCity = '" & abs(cint(g_bShowCity)) & "', " & _
				"ShowState = '" & abs(cint(g_bShowState)) & "', " & _
				"ShowZip = '" & abs(cint(g_bShowZip)) & "', " & _
				"ShowAddress2Address = '" & abs(cint(g_bShowAddress2Address)) & "', " & _
				"ShowAddress2City = '" & abs(cint(g_bShowAddress2City)) & "', " & _
				"ShowAddress2State = '" & abs(cint(g_bShowAddress2State)) & "', " & _
				"ShowAddress2Zip = '" & abs(cint(g_bShowAddress2Zip)) & "', " & _
				"ShowAddress3Address = '" & abs(cint(g_bShowAddress3Address)) & "', " & _
				"ShowAddress3City = '" & abs(cint(g_bShowAddress3City)) & "', " & _
				"ShowAddress3State = '" & abs(cint(g_bShowAddress3State)) & "', " & _
				"ShowAddress3Zip = '" & abs(cint(g_bShowAddress3Zip)) & "', " & _
				"ShowAddress4Address = '" & abs(cint(g_bShowAddress4Address)) & "', " & _
				"ShowAddress4City = '" & abs(cint(g_bShowAddress4City)) & "', " & _
				"ShowAddress4State = '" & abs(cint(g_bShowAddress4State)) & "', " & _
				"ShowAddress4Zip = '" & abs(cint(g_bShowAddress4Zip)) & "', " & _												
                "ShowCompanyNotes = '" & abs(cint(g_bShowCompanyNotes)) & "', " & _
                "ShowDivisionNotes = '" & abs(cint(g_bShowDivisionNotes)) & "', " & _
                "ShowRegionNotes = '" & abs(cint(g_bShowRegionNotes)) & "', " & _
                "ShowBranchNotes = '" & abs(cint(g_bShowBranchNotes)) & "', " & _
				"ShowOfficerNotes = '" & abs(cint(g_bShowOfficerNotes)) & "', " & _
                "ShowFingerprintDeadline = '" & abs(cint(g_bShowFingerprintDeadline)) & "', " & _
                "ShowAgencyType = '" & abs(cint(g_bShowAgencyType)) & "', " & _
                "ShowAgencyWebsite = '" & abs(cint(g_bShowAgencyWebsite)) & "', " & _
				"ShowProviderName = '" & abs(cint(g_bShowProviderName)) & "', " & _
				"ShowProviderAddress = '" & abs(cint(g_bShowProviderAddress)) & "', " & _
				"ShowProviderPhoneNo = '" & abs(cint(g_bShowProviderPhoneNo)) & "', " & _
				"ShowProviderContactName = '" & abs(cint(g_bShowProviderContactName)) & "', " & _
				"ShowProviderContactTitle = '" & abs(cint(g_bShowProviderContactTitle)) & "', " & _
				"ShowInstructorName = '" & abs(cint(g_bShowInstructorName)) & "', " & _
				"ShowInstructorEducationLevel = '" & abs(cint(g_bShowInstructorEducationLevel)) & "', " & _
				"ShowBranchCustom1 = '" & abs(cint(g_bShowBranchCustom1)) & "', " & _
                "ShowBranchCustom2 = '" & abs(cint(g_bShowBranchCustom2)) & "', " & _
                "ShowBranchCustom3 = '" & abs(cint(g_bShowBranchCustom3)) & "', " & _
                "ShowBranchCustom4 = '" & abs(cint(g_bShowBranchCustom4)) & "', " & _
                "ShowBranchCustom5 = '" & abs(cint(g_bShowBranchCustom5)) & "', " & _
				"ShowOfficerCustom1 = '" & abs(cint(g_bShowOfficerCustom1)) & "', " & _
				"ShowOfficerCustom2 = '" & abs(cint(g_bShowOfficerCustom2)) & "', " & _
				"ShowOfficerCustom3 = '" & abs(cint(g_bShowOfficerCustom3)) & "', " & _
				"ShowOfficerCustom4 = '" & abs(cint(g_bShowOfficerCustom4)) & "', " & _
				"ShowOfficerCustom5 = '" & abs(cint(g_bShowOfficerCustom5)) & "', " & _
				"ShowOfficerCustom6 = '" & abs(cint(g_bShowOfficerCustom6)) & "', " & _
				"ShowOfficerCustom7 = '" & abs(cint(g_bShowOfficerCustom7)) & "', " & _
				"ShowOfficerCustom8 = '" & abs(cint(g_bShowOfficerCustom8)) & "', " & _
				"ShowOfficerCustom9 = '" & abs(cint(g_bShowOfficerCustom9)) & "', " & _
				"ShowOfficerCustom10 = '" & abs(cint(g_bShowOfficerCustom10)) & "', " & _
				"ShowOfficerCustom11 = '" & abs(cint(g_bShowOfficerCustom11)) & "', " & _
				"ShowOfficerCustom12 = '" & abs(cint(g_bShowOfficerCustom12)) & "', " & _
				"ShowOfficerCustom13 = '" & abs(cint(g_bShowOfficerCustom13)) & "', " & _
				"ShowOfficerCustom14 = '" & abs(cint(g_bShowOfficerCustom14)) & "', " & _
				"ShowOfficerCustom15 = '" & abs(cint(g_bShowOfficerCustom15)) & "' " & _
				"WHERE ReportId = '" & g_iReportId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				SaveReport = g_iReportId
			else
				ReportError("Unable to save existing Report.")
				exit function
			end if
			
		else
		
			sSql = "INSERT INTO SavedReports (" & _
				"Name, " & _
				"CompanyId, " & _
				"Deleted, " & _
				"LastName, " & _
				"Ssn, " & _
				"StateId, " & _
				"BranchId, " & _
				"StateIdList, " & _
				"LicStateIdList, " & _
				"CourseStateIdList, " & _
				"StructureIdList, " & _
				"AppDeadline, " & _
				"Course, " & _
				"ProviderId, " & _
                "AccreditationTypeID, " & _
				"CreditHours, " & _
				"LicenseNumber, " & _
				"CourseCompletionDateTo, " & _
				"CourseCompletionDateFrom, " & _
				"CourseExpTo, " & _
				"CourseExpFrom, " & _
				"HireDateFrom, " & _
				"HireDateTo, " & _				
				"LicExpTo, " & _
				"LicExpFrom, " & _
				"LicenseStatusId, " & _
				"LicensePayment, " & _
				"LicenseStatusIdList, " & _
				"ShowOfficers, " & _
				"ShowCompanies, " & _
				"ShowBranches, " & _
				"ShowCompanyL2s, " & _
				"ShowCompanyL3s, " & _
				"ShowCourses, " & _
				"ShowLicenses, " & _
				"ShowLicIssued, " & _
				"ShowInactive, " & _
				"ShowCompleted, " & _
				"ShowAdmins, " & _
				"ShowLicBranches, " & _
				"ShowLicIssuedBranches, " & _
				"ShowLicCompanyL2s, " & _
				"ShowLicIssuedCompanyL2s, " & _
				"ShowLicCompanyL3s, " & _
				"ShowLicIssuedCompanyL3s, " & _
				"ShowFirstName, " & _
				"ShowSsn, " & _
				"ShowNMLSNumber, " & _
				"ShowEmployeeId, " & _
				"ShowTitle, " & _
				"ShowDepartment, " & _
				"ShowPhone, " & _
				"ShowEmail, " & _
				"ShowWorkAddress, " & _
				"ShowWorkCity, " & _
				"ShowWorkState, " & _
				"ShowWorkZip, " & _
				"ShowAddress, " & _
				"ShowCity, " & _
				"ShowState, " & _
				"ShowZip, " & _
				"ShowAddress2Address, " & _
				"ShowAddress2City, " & _
				"ShowAddress2State, " & _
				"ShowAddress2Zip, " & _
				"ShowAddress3Address, " & _
				"ShowAddress3City, " & _
				"ShowAddress3State, " & _
				"ShowAddress3Zip, " & _
				"ShowAddress4Address, " & _
				"ShowAddress4City, " & _
				"ShowAddress4State, " & _
				"ShowAddress4Zip, " & _												
                "ShowCompanyNotes, " & _
                "ShowDivisionNotes, " & _
                "ShowRegionNotes, " & _
                "ShowBranchNotes, " & _
				"ShowOfficerNotes, " & _
                "ShowFingerprintDeadline, " & _
                "ShowAgencyType, " & _
                "ShowAgencyWebsite, " & _
				"ShowProviderName, " & _
				"ShowProviderAddress, " & _				
				"ShowProviderPhoneNo, " & _
				"ShowProviderContactName, " & _
				"ShowProviderContactTitle, " & _
				"ShowInstructorName, " & _
				"ShowInstructorEducationLevel, " & _
				"ShowBranchCustom1, " & _
                "ShowBranchCustom2, " & _
                "ShowBranchCustom3, " & _
                "ShowBranchCustom4, " & _
                "ShowBranchCustom5, " & _
				"ShowOfficerCustom1, " & _
				"ShowOfficerCustom2, " & _
				"ShowOfficerCustom3, " & _
				"ShowOfficerCustom4, " & _
				"ShowOfficerCustom5, " & _
				"ShowOfficerCustom6, " & _
				"ShowOfficerCustom7, " & _
				"ShowOfficerCustom8, " & _
				"ShowOfficerCustom9, " & _
				"ShowOfficerCustom10, " & _
				"ShowOfficerCustom11, " & _
				"ShowOfficerCustom12, " & _
				"ShowOfficerCustom13, " & _
				"ShowOfficerCustom14, " & _
				"ShowOfficerCustom15" & _
				") VALUES (" & _
				"'" & g_sName & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & abs(cint(g_bDeleted)) & "', " & _
				"'" & g_sLastName & "', " & _
				"'" & g_sSsn & "', " & _
				"'" & g_iStateId & "', " & _
				"'" & g_iBranchId & "', " & _
				"'" & g_sStateIdList & "', " & _
				"'" & g_sLicStateIdList & "', " & _
				"'" & g_sCourseStateIdList & "', " & _
				"'" & g_sStructureIdList & "', " & _
				"'" & g_iAppDeadline & "', " & _
				"'" & g_sCourse & "', "
			if g_iProviderId <> "" then
				sSql = sSql & "'" & g_iProviderId & "', "
			else
				sSql = sSql & "NULL, "
			end if
            if g_iAccreditationTypeID <> "" then
                sSql = sSql & "'" & g_iAccreditationTypeID & "', "
            else
                sSql = sSql & "NULL, "
            end if
			sSql = sSql & "'" & g_rCreditHours & "', " & _
				"'" & g_sLicenseNumber & "', "
			if g_dCourseCompletionDateTo <> "" then
				sSql = sSql & "'" & g_dCourseCompletionDateTo & "', "
			else
				sSql = sSql & "NULL, "
			end if
			if g_dCourseCompletionDateFrom <> "" then	
				sSql = sSql & "'" & g_dCourseCompletionDateFrom & "', "
			else
				sSql = sSql & "NULL, "
			end if
			if g_dCourseExpTo <> "" then
				sSql = sSql & "'" & g_dCourseExpTo & "', "
			else
				sSql = sSql & "NULL, "
			end if
			if g_dCourseExpFrom <> "" then	
				sSql = sSql & "'" & g_dCourseExpFrom & "', "
			else
				sSql = sSql & "NULL, "
			end if
			if g_dHireDateFrom <> "" then	
				sSql = sSql & "'" & g_dHireDateFrom & "', "
			else
				sSql = sSql & "NULL, "
			end if						
			if g_dHireDateTo <> "" then	
				sSql = sSql & "'" & g_dHireDateTo & "', "
			else
				sSql = sSql & "NULL, "
			end if		
			if g_dLicExpTo <> "" then
				sSql = sSql & "'" & g_dLicExpTo & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dLicExpFrom <> "" then 
				sSql = sSql & "'" & g_dLicExpFrom & "', "
			else
				sSql = sSql & "NULL, "
			end if
			if g_iLicenseStatusId <> "" then
				sSql = sSql & "'" & g_iLicenseStatusId & "', "
			else
				sSql = sSql & "NULL, "
			end if
			sSql = sSql & _
				"'" & g_sLicensePayment & "', " & _
				"'" & g_sLicenseStatusIdList & "', " & _
				"'" & abs(cint(g_bShowOfficers)) & "', " & _
				"'" & abs(cint(g_bShowCompanies)) & "', " & _
				"'" & abs(cint(g_bShowBranches)) & "', " & _
				"'" & abs(cint(g_bShowCompanyL2s)) & "', " & _
				"'" & abs(cint(g_bShowCompanyL3s)) & "', " & _
				"'" & abs(cint(g_bShowCourses)) & "', " & _
				"'" & abs(cint(g_bShowLicenses)) & "', " & _
				"'" & abs(cint(g_bShowLicIssued)) & "', " 
            if g_iShowInactive <> "" then
				sSql = sSql &  "'" & g_iShowInactive & "', " 
            else
                sSql = sSql & "NULL, "
            end if
            if g_iShowCompleted <> "" then
				sSql = sSql & "'" & g_iShowCompleted & "', "
            else
                sSql = sSql & "NULL, " 
            end if
				sSql = sSql & "'" & abs(cint(g_bShowAdmins)) & "', " & _
				"'" & abs(cint(g_bShowLicBranches)) & "', " & _
				"'" & abs(cint(g_bShowLicIssuedBranches)) & "', " & _
				"'" & abs(cint(g_bShowLicCompanyL2s)) & "', " & _
				"'" & abs(cint(g_bShowLicIssuedCompanyL2s)) & "', " & _
				"'" & abs(cint(g_bShowLicCompanyL3s)) & "', " & _
				"'" & abs(cint(g_bShowLicIssuedCompanyL3s)) & "', " & _
				"'" & abs(cint(g_bShowFirstName)) & "', " & _
				"'" & abs(cint(g_bShowSsn)) & "', " & _
				"'" & abs(cint(g_bShowNMLSNumber)) & "', " & _
				"'" & abs(cint(g_bShowEmployeeId)) & "', " & _
				"'" & abs(cint(g_bShowTitle)) & "', " & _
				"'" & abs(cint(g_bShowDepartment)) & "', " & _								
				"'" & abs(cint(g_bShowPhone)) & "', " & _
				"'" & abs(cint(g_bShowEmail)) & "', " & _
				"'" & abs(cint(g_bShowWorkAddress)) & "', " & _
				"'" & abs(cint(g_bShowWorkCity)) & "', " & _
				"'" & abs(cint(g_bShowWorkState)) & "', " & _
				"'" & abs(cint(g_bShowWorkZip)) & "', " & _
				"'" & abs(cint(g_bShowAddress)) & "', " & _
				"'" & abs(cint(g_bShowCity)) & "', " & _
				"'" & abs(cint(g_bShowState)) & "', " & _
				"'" & abs(cint(g_bShowZip)) & "', " & _		
				"'" & abs(cint(g_bShowAddress2Address)) & "', " & _
				"'" & abs(cint(g_bShowAddress2City)) & "', " & _
				"'" & abs(cint(g_bShowAddress2State)) & "', " & _
				"'" & abs(cint(g_bShowAddress2Zip)) & "', " & _		
				"'" & abs(cint(g_bShowAddress3Address)) & "', " & _
				"'" & abs(cint(g_bShowAddress3City)) & "', " & _
				"'" & abs(cint(g_bShowAddress3State)) & "', " & _
				"'" & abs(cint(g_bShowAddress3Zip)) & "', " & _		
				"'" & abs(cint(g_bShowAddress4Address)) & "', " & _
				"'" & abs(cint(g_bShowAddress4City)) & "', " & _
				"'" & abs(cint(g_bShowAddress4State)) & "', " & _
				"'" & abs(cint(g_bShowAddress4Zip)) & "', " & _																
                "'" & abs(cint(g_bShowCompanyNotes)) & "', " & _
                "'" & abs(cint(g_bShowDivisionNotes)) & "', " & _
                "'" & abs(cint(g_bShowRegionNotes)) & "', " & _
                "'" & abs(cint(g_bShowBranchNotes)) & "', " & _
				"'" & abs(cint(g_bShowOfficerNotes)) & "', " & _				
                "'" & abs(cint(g_bShowFingerprintDeadline)) & "', " & _
                "'" & abs(cint(g_bShowAgencyType)) & "', " & _
                "'" & abs(cint(g_bShowAgencyWebsite)) & "', " & _
				"'" & abs(cint(g_bShowProviderName)) & "', " & _
				"'" & abs(cint(g_bShowProviderAddress)) & "', " & _				
				"'" & abs(cint(g_bShowProviderPhoneNo)) & "', " & _
				"'" & abs(cint(g_bShowProviderContactName)) & "', " & _
				"'" & abs(cint(g_bShowProviderContactTitle)) & "', " & _
				"'" & abs(cint(g_bShowInstructorName)) & "', " & _
				"'" & abs(cint(g_bShowInstructorEducationLevel)) & "', " & _
				"'" & abs(cint(g_bShowBranchCustom1)) & "', " & _
                "'" & abs(cint(g_bShowBranchCustom2)) & "', " & _
                "'" & abs(cint(g_bShowBranchCustom3)) & "', " & _
                "'" & abs(cint(g_bShowBranchCustom4)) & "', " & _
                "'" & abs(cint(g_bShowBranchCustom5)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom1)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom2)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom3)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom4)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom5)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom6)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom7)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom8)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom9)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom10)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom11)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom12)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom13)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom14)) & "', " & _
				"'" & abs(cint(g_bShowOfficerCustom15)) & "'" & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new Report
			sSql = "SELECT ReportId FROM SavedReports " & _
				"WHERE Name = '" & g_sName & "'" & _
				"ORDER BY ReportId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				g_iReportId = oRs("ReportId")
				SaveReport = g_iReportId
			else
				'Record could not be retrieved
				ReportError("Failed to save new Report.")
				exit function
			end if
		
		end if
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchReports
	' Desc: Retrieves a collection of ReportIds that match the properties of 
	'	the current object.
	' Preconditions: ConnectionString
	' Returns: Recordset of the results
	' -------------------------------------------------------------------------
	public function SearchReports()
	
		set SearchReports = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		dim bFirstClause 'Used to direct SQL phrasing
		dim sSql
		
		sSql = "SELECT SRs.ReportId, SRs.Name " & _
			"FROM SavedReports AS SRs "
		
			
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyId = '" & g_iCompanyId & "'"
		end if 	
		
		
		'If Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "SRs.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if 	
			
		
		sSql = sSql & " ORDER BY SRs.Name"
		
		set SearchReports = QueryDb(sSql)
		
	end function
	
	' -------------------------------------------------------------------------
	' Name: GetShowBranchCustom
	' Desc: Returns the value of the specified ShowBranchCustom field
	' Preconditions: none
	' Inputs: p_iNo - the number of the Show Branch Custom Field
	' Returns: string - the value of the Show Branch Custom Field
	' -------------------------------------------------------------------------
	public function	GetShowBranchCustom(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetShowBranchCustom = g_bShowBranchCustom1
			case 2
				GetShowBranchCustom = g_bShowBranchCustom2
			case 3
				GetShowBranchCustom = g_bShowBranchCustom3
			case 4
				GetShowBranchCustom = g_bShowBranchCustom4
			case 5
				GetShowBranchCustom = g_bShowBranchCustom5
			case else
				GetShowOfficerCustom = ""
		end select
	end function
	
	' -------------------------------------------------------------------------
	' Name: SetShowBranchCustom
	' Desc: Returns the value of the specified ShowBranchCustom field
	' Preconditions: none
	' Inputs: p_iNo - the number of the Show Branch Custom Field
	'		  p_sValue - the value to set the Show Branch Custom Field to
	' Returns: n/a
	' -------------------------------------------------------------------------
	public sub SetShowBranchCustom(p_iNo,p_sValue)
		if not checkintparam(p_iNo, false, "Number") then
			exit sub
		end if
		
		select case p_iNo
			case 1
				g_bShowBranchCustom1 = p_sValue
			case 2
				g_bShowBranchCustom2 = p_sValue
			case 3
				g_bShowBranchCustom3 = p_sValue
			case 4
				g_bShowBranchCustom4 = p_sValue
			case 5
				g_bShowBranchCustom5 = p_sValue
		end select
	end sub

	' -------------------------------------------------------------------------
	' Name: GetShowOfficerCustom
	' Desc: Returns the value of the specified ShowOfficerCustom field
	' Preconditions: none
	' Inputs: p_iNo - the number of the Show Officer Custom Field
	' Returns: string - the value of the Show Officer Custom Field
	' -------------------------------------------------------------------------
	public function	GetShowOfficerCustom(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetShowOfficerCustom = g_bShowOfficerCustom1
			case 2
				GetShowOfficerCustom = g_bShowOfficerCustom2
			case 3
				GetShowOfficerCustom = g_bShowOfficerCustom3
			case 4
				GetShowOfficerCustom = g_bShowOfficerCustom4
			case 5
				GetShowOfficerCustom = g_bShowOfficerCustom5
			case 6
				GetShowOfficerCustom = g_bShowOfficerCustom6
			case 7
				GetShowOfficerCustom = g_bShowOfficerCustom7
			case 8
				GetShowOfficerCustom = g_bShowOfficerCustom8
			case 9
				GetShowOfficerCustom = g_bShowOfficerCustom9
			case 10
				GetShowOfficerCustom = g_bShowOfficerCustom10
			case 11
				GetShowOfficerCustom = g_bShowOfficerCustom11
			case 12
				GetShowOfficerCustom = g_bShowOfficerCustom12
			case 13
				GetShowOfficerCustom = g_bShowOfficerCustom13
			case 14
				GetShowOfficerCustom = g_bShowOfficerCustom14
			case 15
				GetShowOfficerCustom = g_bShowOfficerCustom15		
			case else
				GetShowOfficerCustom = ""
		end select
	end function
	
	' -------------------------------------------------------------------------
	' Name: SetShowOfficerCustom
	' Desc: Returns the value of the specified ShowOfficerCustom field
	' Preconditions: none
	' Inputs: p_iNo - the number of the Show Officer Custom Field
	'		  p_sValue - the value to set the Show Officer Custom Field to
	' Returns: n/a
	' -------------------------------------------------------------------------
	public sub SetShowOfficerCustom(p_iNo,p_sValue)
		if not checkintparam(p_iNo, false, "Number") then
			exit sub
		end if
		
		select case p_iNo
			case 1
				g_bShowOfficerCustom1 = p_sValue
			case 2
				g_bShowOfficerCustom2 = p_sValue
			case 3
				g_bShowOfficerCustom3 = p_sValue
			case 4
				g_bShowOfficerCustom4 = p_sValue
			case 5
				g_bShowOfficerCustom5 = p_sValue
			case 6
				g_bShowOfficerCustom6 = p_sValue
			case 7
				g_bShowOfficerCustom7 = p_sValue
			case 8
				g_bShowOfficerCustom8 = p_sValue
			case 9
				g_bShowOfficerCustom9 = p_sValue
			case 10
				g_bShowOfficerCustom10 = p_sValue
			case 11
				g_bShowOfficerCustom11 = p_sValue
			case 12
				g_bShowOfficerCustom12 = p_sValue
			case 13
				g_bShowOfficerCustom13 = p_sValue
			case 14
				g_bShowOfficerCustom14 = p_sValue
			case 15
				g_bShowOfficerCustom15 = p_sValue
		end select
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	Safe
	' Description: Escapes/scrubs characters to make content safe for
	'	submission into the database.
	' Inputs: p_sText - String to make database-safe.
	' Returns: Edited string
	' -------------------------------------------------------------------------
	public function Safe(p_sText)
	
		if p_sText <> "" then
	
			p_sText = replace(p_sText, "''", "'")
			p_sText = replace(p_sText, "'", "''")
		
			Safe = p_sText
			
		end if
	
	end function
		
	
end class
%>

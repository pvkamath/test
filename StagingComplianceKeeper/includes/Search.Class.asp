<%
'===============================================================================================================
' Class: Search
' Description: Class used for searching the information
' Create Date: 7/23/2004
' Current Version: 1.0
' ----------------------
' Properties:
'	- ConnectionString
' Methods:
'	- CheckConnectionString()
'	- CheckParam(p_sParam)
'	- AddCourseType(p_iCourseType)
'	- QueryDB(sSQL)
'	- SearchCourses()
'	- SearchUsers()
'
'===============================================================================================================
Class Search
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString 'as string
	dim iCourseID 'as integer
	dim sCourseName 'as string
	dim iCourseType 'as integer
	dim iCourseProgram 'as integer
	dim iNoOfCourseTypes 'as integer
	dim iState 'as integer
	dim bCurrentCourses 'as boolean
	dim sEmail 'as string
	dim sLastName 'as string
	dim iProviderID 'as integer
	dim iCompanyId 
	dim iBranchID 'as integer
	dim iUserStatus 'as integer
	dim aCourseTypes() 'as array
	
	' PRIVATE METHODS ===========================================================================================
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				CheckConnectionString
	' Description:		Check to make sure the connectionstring has been set
	' Inputs:			none
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------	
	Private Sub CheckConnectionString()
		if isNull(g_sConnectionString) then
			response.write "You must specify the value of the ConnectionString Property value before proceeding."
		end if
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				CheckParam
	' Description:		Checks to make sure the passed in value is not null
	' Inputs:			p_sParam - the required parameter
	' Output:			none
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Sub CheckParam(p_sParam)
		if ((isNull(p_sParam)) or (trim(p_sParam) = "")) then
			response.write "Processing Error!!!<br>You must specify the value before proceeding."
			response.end
		end if
	End Sub
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:				QueryDB
	' Description:		executes the query in the database
	' Inputs:			sSQL - the query to execute
	' Output:			recordset - a recordset of the executed query is returned
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Function QueryDB(sSQL)
		CheckParam(sSQL)
		CheckConnectionString	
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		'    objRS.ActiveConnection = Connect
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function

	' PUBLIC METHODS ===========================================================================================
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddCourseType
	' Purpose:				adds a course type to the course
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iCourseType - the course type that will be added
	' Outputs:				n/a
	' ----------------------------------------------------------------------------------------------------------	
	Sub AddCourseType(p_iCourseType)	
		if trim(iNoOfCourseTypes) = "" then
			iNoOfCourseTypes = 0
		else
			iNoOfCourseTypes = iNoOfCourseTypes  + 1
		end if
		
		redim preserve aCourseTypes(iNoOfCourseTypes)
		
		aCourseTypes(iNoOfCourseTypes) = p_iCourseType
	End Sub
		
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SearchCourses()
	' Description:			Searches the Course Information
	' Required Properties:  ConnectionString
	' Optional Properties:
	' Parameters:			n/a
	' Outputs:				recordset of the records that matched the search criteria
	' ----------------------------------------------------------------------------------------------------------
	Function SearchCourses()
		dim bFirstClause 'as boolean
		dim sSQL 'as strig
		dim iCurrentCourseType 'as integer
		
		bFirstClause = true
	
		sSQL = "SELECT Distinct(C.CourseID), C.*, S.State, P.Name as ProgramName FROM Courses AS C " & _
			   "INNER JOIN CoursesTypesX AS CTX ON (C.CourseID = CTX.CourseID) " & _
			   "LEFT OUTER JOIN States AS S ON (S.StateID = C.StateID) " & _
			   "INNER JOIN Programs AS P ON (P.ProgramID = C.ProgramID) "

		'if only Current Courses search
		if bCurrentCourses then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if
			sSQL = sSQL & " C.ExpirationDate >= '" & date() & " 00:00' "		
		end if
			   
		'if course ID search
		if iCourseID <>  "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if
			sSQL = sSQL & "C.CourseID = '" & iCourseID & "'"
		end if
			   
		'if course Name search
		if sCourseName <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if
			sSQL = sSQL & "C.Name like '%" & sCourseName & "%'"
		end if
		
		'if course type search
		for each iCurrentCourseType in aCourseTypes
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if

			sSQL = sSQL & " CTX.TypeID = '" & iCurrentCourseType & "' "
		next		
		
		'if course Program search
		if iCourseProgram <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if
			sSQL = sSQL & "C.ProgramID = '" & iCourseProgram & "'"
		end if
		
		'if course State search
		if iState <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if
			sSQL = sSQL & "(C.StateID = '" & iState & "' OR C.StateID = 0)"  'courses that are for all states are set to 0
		end if

		sSQL = sSQL & "ORDER BY S.State"

		set SearchCourses = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SearchUsers()
	' Description:			Searches the User Information
	' Required Properties:  ConnectionString
	' Optional Properties:	sEmail, sLastName, iProviderID, iBranchID, iUserStatus
	' Parameters:			n/a
	' Outputs:				recordset of the records that matched the search criteria
	' ----------------------------------------------------------------------------------------------------------
	Function SearchUsers()
		dim sSQL
		
		sSQL = "SELECT DISTINCT U.*, UST.Type, UST.Display, SGs.GroupName FROM Users AS U " & _
			   "INNER JOIN UserStatusTypes AS UST ON (UST.StatusID = U.UserStatus) " & _
			   "LEFT JOIN UsersBranches AS UBs ON (UBs.UserId = U.UserId) " & _
			   "LEFT JOIN afxSecurityGroups AS SGs ON (U.GroupId = SGs.GroupId) " & _
			   "WHERE U.UserStatus <> 3 "
			   
		'Add values to search if not empty
		if sEmail <> "" then
			sSQL = sSQL & "AND U.Email like '%" & sEmail & "%'"
		end if
	
		if sLastName <> "" then
			sSQL = sSQL & "AND U.LastName like '%" & sLastName & "%'"
		end if
	
		if iProviderID <> "" then
			sSQL = sSQL & "AND U.ProviderID = '" & iProviderID & "'"
		end if
		
	
		if iCompanyID <> "" then
			sSQL = sSQL & "AND U.CompanyID = '" & iCompanyID & "'"
		end if
	
		if iBranchID <> "" then
			sSQL = sSQL & "AND UBs.BranchID = '" & iBranchID & "'"
		end if

		if iUserStatus <> "" then
			sSQL = sSQL & "AND U.UserStatus = '" & iUserStatus & "'"
		end if
		
		sSQL = sSQL & "ORDER BY U.LastName"
		
		'Response.Write(sSql)
		
		set SearchUsers = QueryDB(sSQL)
	End Function
	
	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined
	Public Property Let BranchID(p_iBranchID)
		iBranchID = p_iBranchID
	End Property

	Public Property Get BranchID()
		BranchID = iBranchID
	End Property
		
	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	Public Property Let ProviderID(p_iProviderID)
		iProviderID = p_iProviderID
	End Property

	Public Property Get ProviderID()
		ProviderID = iProviderID
	End Property
	
	public property let CompanyId(p_iCompanyId)
		iCompanyId = p_iCompanyId
	end property
	
	public property get CompanyId()
		CompanyId = iCompanyId
	end property
	
	Public Property Let CourseID(p_iCourseID)
		iCourseID = p_iCourseID
	End Property
	
	Public Property Get CourseID()
		CourseID = iCourseID
	End Property
	
	Public Property Let CourseName(p_sCourseName)
		sCourseName = p_sCourseName
	End Property

	Public Property Get CourseName()
		CourseName = sCourseName
	End Property
	
	Public Property Let CourseType(p_iCourseType)
		iCourseType = p_iCourseType
	End Property

	Public Property Get CourseType()
		CourseType = iCourseType
	End Property
	
	Public Property Let CourseProgram(p_iCourseProgram)
		iCourseProgram = p_iCourseProgram
	End Property

	Public Property Get CourseProgram()
		CourseProgram = iCourseProgram
	End Property

	Public Property Let CurrentCourses(p_bCurrentCourses)
		bCurrentCourses = p_bCurrentCourses
	End Property

	Public Property Get CurrentCourses()
		CurrentCourses = bCurrentCourses
	End Property
	
	Public Property Let Email(p_sEmail)
		sEmail = p_sEmail
	End Property

	Public Property Get Email()
		Email = sEmail
	End Property

	Public Property Let LastName(p_sLastName)
		sLastName = p_sLastName
	End Property

	Public Property Get LastName()
		LastName = sLastName
	End Property
	
	Public Property Let State(p_iState)
		iState = p_iState
	End Property

	Public Property Get State()
		State = iState
	End Property

	Public Property Let UserStatus(p_iUserStatus)
		iUserStatus = p_iUserStatus
	End Property

	Public Property Get UserStatus()
		UserStatus = iUserStatus
	End Property
end class
%>

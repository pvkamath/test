<%
'==============================================================================
' Class: Settings
' Controls the creation, modification, and removal of site configuration 
'		settings for Companies
' Create Date: 4/15/05
' Author(s): Mark Silvia
' ----------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- SettingsId
'	- Active
'	- AllowOfficerNoteEditing
'	- CompanyName
'	- CompanyL2Name
'	- CompanyL3Name
'	- CustField1Name
'	- CustField2Name
'	- CustField3Name
'	- CustField4Name
'	- CustField5Name
'	- OfficerCustField1Name
'	- OfficerCustField2Name
'	- OfficerCustField3Name
'	- OfficerCustField4Name
'	- OfficerCustField5Name
'	- OfficerCustField6Name
'	- OfficerCustField7Name
'	- OfficerCustField8Name
'	- OfficerCustField9Name
'	- OfficerCustField10Name
'	- OfficerCustField11Name
'	- OfficerCustField12Name
'	- OfficerCustField13Name
'	- OfficerCustField14Name
'	- OfficerCustField15Name
'	- Send30DayEmail
'	- Send30DayEmailDaily
'	- Send60DayEmail
'	- Send90DayEmail
'	- SendCourseEmail
'	- Send30DayCourseEmail
'	- Send30DayCourseEmailDaily
'	- Send60DayCourseEmail
'	- Send90DayCourseEmail
'	- SendCoAdminFingerprintDeadlineEmail
'	- SendL2AdminFingerprintDeadlineEmail
'	- SendL3AdminFingerprintDeadlineEmail
'	- SendBrAdminFingerprintDeadlineEmail
'	- SendLoFingerprintDeadlineEmail
'	- SendLO30DayEmail
'	- SendLO60DayEmail
'	- SendLO90DayEmail
'	- UseRenewalAppDeadlineLicExpEmails
'	- Email30DayText
'	- Email60DayText
'	- Email90DayText
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
' Methods
'	- LoadSettingsById
'	- LoadSettingsByCompanyId
'	- SaveSettings
'	- DeleteSettings
'	- GetOfficerCustFieldName(p_iNo)
'	- SetOfficerCustFieldName(p_iNo,p_sValue)
'==============================================================================

class Settings
	
	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Settings properties
	dim g_iSettingsId
	dim g_iCompanyId
	dim g_bActive
	dim g_bAllowOfficerNoteEditing
	dim g_sCompanyName
	dim g_sCompanyL2Name
	dim g_sCompanyL3Name
	dim g_sCustField1Name
	dim g_sCustField2Name
	dim g_sCustField3Name
	dim g_sCustField4Name
	dim g_sCustField5Name
	dim g_sOfficerCustField1Name
	dim g_sOfficerCustField2Name
	dim g_sOfficerCustField3Name
	dim g_sOfficerCustField4Name
	dim g_sOfficerCustField5Name
	dim g_sOfficerCustField6Name
	dim g_sOfficerCustField7Name
	dim g_sOfficerCustField8Name
	dim g_sOfficerCustField9Name
	dim g_sOfficerCustField10Name
	dim g_sOfficerCustField11Name
	dim g_sOfficerCustField12Name
	dim g_sOfficerCustField13Name
	dim g_sOfficerCustField14Name
	dim g_sOfficerCustField15Name
	dim g_bSend30DayEmail
	dim g_bSend30DayEmailDaily
	dim g_bSend60DayEmail
	dim g_bSend90DayEmail
	dim g_bSendCourseEmail
	dim g_bSend30DayCourseEmail
	dim g_bSend30DayCourseEmailDaily
	dim g_bSend60DayCourseEmail
	dim g_bSend90DayCourseEmail
	dim g_bCcBrAdmin30Day
	dim g_bCcCoAdmin30Day
	dim g_bCcL2Admin30Day
	dim g_bCcL3Admin30Day
	dim g_bCcBrAdmin60Day
	dim g_bCcCoAdmin60Day
	dim g_bCcL2Admin60Day
	dim g_bCcL3Admin60Day
	dim g_bCcBrAdmin90Day
	dim g_bCcCoAdmin90Day
	dim g_bCcL2Admin90Day
	dim g_bCcL3Admin90Day

	dim g_bCcBrAdmin30DayCourse
	dim g_bCcCoAdmin30DayCourse
	dim g_bCcL2Admin30DayCourse
	dim g_bCcL3Admin30DayCourse
	dim g_bCcBrAdmin60DayCourse
	dim g_bCcCoAdmin60DayCourse
	dim g_bCcL2Admin60DayCourse
	dim g_bCcL3Admin60DayCourse
	dim g_bCcBrAdmin90DayCourse
	dim g_bCcCoAdmin90DayCourse
	dim g_bCcL2Admin90DayCourse
	dim g_bCcL3Admin90DayCourse

	dim g_bSendCoAdminCourseEmail
	dim g_bSendBrAdminCourseEmail
	dim g_bSendL2AdminCourseEmail
	dim g_bSendL3AdminCourseEmail
	dim g_bSendLoCourseEmail
	dim g_bSendCoAdminEventEmail
	dim g_bSendBrAdminEventEmail
	dim g_bSendL2AdminEventEmail
	dim g_bSendL3AdminEventEmail
	dim g_bSendLoEventEmail
	dim g_bSendCoAdminFingerprintDeadlineEmail
	dim g_bSendL2AdminFingerprintDeadlineEmail
	dim g_bSendL3AdminFingerprintDeadlineEmail
	dim g_bSendBrAdminFingerprintDeadlineEmail
	dim g_bSendLoFingerprintDeadlineEmail	
	dim g_bSendLO30DayEmail
	dim g_bSendLO60DayEmail
	dim g_bSendLO90DayEmail			
	dim g_bUseRenewalAppDeadlineLicExpEmails	
	dim g_sEmail30DayText
	dim g_sEmail60DayText
	dim g_sEmail90DayText
	dim g_sCourseEmailText
	dim g_sCourseEmail30DayText
	dim g_sCourseEmail60DayText
	dim g_sCourseEmail90DayText
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Settings group
	public property get SettingsId()
		SettingsId = g_iSettingsId
	end property
	
	
	'Company ID that these settings are assigned to.
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Boolean required.")
		end if
	end property
	

	'Active flag, determines whether to actually apply the settings.
	public property get Active()
		Active = g_bActive
	end property
	public property let Active(p_bActive)
		if isnumeric(p_bActive) then
			g_bActive = cbool(p_bActive)
		elseif p_bActive = "" then
			g_bActive = ""
		else
			ReportError("Invalid Active value.  Boolean required.")
		end if
	end property
	 
	'AllowOfficerNoteEditing flag, determines whether to actually apply the settings.
	public property get AllowOfficerNoteEditing()
		AllowOfficerNoteEditing = g_bAllowOfficerNoteEditing
	end property
	public property let AllowOfficerNoteEditing(p_bAllowOfficerNoteEditing)
		if isnumeric(p_bAllowOfficerNoteEditing) then
			g_bAllowOfficerNoteEditing = cbool(p_bAllowOfficerNoteEditing)
		else
			ReportError("Invalid AllowOfficerNoteEditing value.  Boolean required.")
		end if
	end property
	
	'Name to give "Company" when it is displayed on the site frontend.
	public property get CompanyName()
		CompanyName = g_sCompanyName
	end property
	public property let CompanyName(p_sCompanyName)
		g_sCompanyName = p_sCompanyName
	end property
	
	
	'Name to give second-tier companies when they are displayed on the site
	'frontend.
	public property get CompanyL2Name()
		CompanyL2Name = g_sCompanyL2Name
	end property
	public property let CompanyL2Name(p_sCompanyL2Name)
		g_sCompanyL2Name = p_sCompanyL2Name
	end property


	'Name to give third-tier companies when they are displayed on the site
	'frontend.
	public property get CompanyL3Name()
		CompanyL3Name = g_sCompanyL3Name
	end property
	public property let CompanyL3Name(p_sCompanyL3Name)
		g_sCompanyL3Name = p_sCompanyL3Name
	end property
	
	
	'Name to give to the custom field available in branch/region/division
	'profiles.
	public property get CustField1Name()
		CustField1Name = g_sCustField1Name
	end property
	public property let CustField1Name(p_sCustField1Name)
		g_sCustField1Name = p_sCustField1Name
	end property
	

	'Name to give to the custom field available in branch/region/division
	'profiles.
	public property get CustField2Name()
		CustField2Name = g_sCustField2Name
	end property
	public property let CustField2Name(p_sCustField2Name)
		g_sCustField2Name = p_sCustField2Name
	end property
	
	
	'Name to give to the custom field available in branch/region/division
	'profiles.
	public property get CustField3Name()
		CustField3Name = g_sCustField3Name
	end property
	public property let CustField3Name(p_sCustField3Name)
		g_sCustField3Name = p_sCustField3Name
	end property
	
	
	'Name to give to the custom field available in branch/region/division
	'profiles.
	public property get CustField4Name()
		CustField4Name = g_sCustField4Name
	end property
	public property let CustField4Name(p_sCustField4Name)
		g_sCustField4Name = p_sCustField4Name
	end property
	
	
	'Name to give to the custom field available in branch/region/division
	'profiles.
	public property get CustField5Name()
		CustField5Name = g_sCustField5Name
	end property
	public property let CustField5Name(p_sCustField5Name)
		g_sCustField5Name = p_sCustField5Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField1Name()
		OfficerCustField1Name = g_sOfficerCustField1Name
	end property
	public property let OfficerCustField1Name(p_sOfficerCustField1Name)
		g_sOfficerCustField1Name = p_sOfficerCustField1Name
	end property
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField2Name()
		OfficerCustField2Name = g_sOfficerCustField2Name
	end property
	public property let OfficerCustField2Name(p_sOfficerCustField2Name)
		g_sOfficerCustField2Name = p_sOfficerCustField2Name
	end property		


	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField3Name()
		OfficerCustField3Name = g_sOfficerCustField3Name
	end property
	public property let OfficerCustField3Name(p_sOfficerCustField3Name)
		g_sOfficerCustField3Name = p_sOfficerCustField3Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField4Name()
		OfficerCustField4Name = g_sOfficerCustField4Name
	end property
	public property let OfficerCustField4Name(p_sOfficerCustField4Name)
		g_sOfficerCustField4Name = p_sOfficerCustField4Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField5Name()
		OfficerCustField5Name = g_sOfficerCustField5Name
	end property
	public property let OfficerCustField5Name(p_sOfficerCustField5Name)
		g_sOfficerCustField5Name = p_sOfficerCustField5Name
	end property	

	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField6Name()
		OfficerCustField6Name = g_sOfficerCustField6Name
	end property
	public property let OfficerCustField6Name(p_sOfficerCustField6Name)
		g_sOfficerCustField6Name = p_sOfficerCustField6Name
	end property
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField7Name()
		OfficerCustField7Name = g_sOfficerCustField7Name
	end property
	public property let OfficerCustField7Name(p_sOfficerCustField7Name)
		g_sOfficerCustField7Name = p_sOfficerCustField7Name
	end property		


	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField8Name()
		OfficerCustField8Name = g_sOfficerCustField8Name
	end property
	public property let OfficerCustField8Name(p_sOfficerCustField8Name)
		g_sOfficerCustField8Name = p_sOfficerCustField8Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField9Name()
		OfficerCustField9Name = g_sOfficerCustField9Name
	end property
	public property let OfficerCustField9Name(p_sOfficerCustField9Name)
		g_sOfficerCustField9Name = p_sOfficerCustField9Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField10Name()
		OfficerCustField10Name = g_sOfficerCustField10Name
	end property
	public property let OfficerCustField10Name(p_sOfficerCustField10Name)
		g_sOfficerCustField10Name = p_sOfficerCustField10Name
	end property	

	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField11Name()
		OfficerCustField11Name = g_sOfficerCustField11Name
	end property
	public property let OfficerCustField11Name(p_sOfficerCustField11Name)
		g_sOfficerCustField11Name = p_sOfficerCustField11Name
	end property
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField12Name()
		OfficerCustField12Name = g_sOfficerCustField12Name
	end property
	public property let OfficerCustField12Name(p_sOfficerCustField12Name)
		g_sOfficerCustField12Name = p_sOfficerCustField12Name
	end property		


	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField13Name()
		OfficerCustField13Name = g_sOfficerCustField13Name
	end property
	public property let OfficerCustField13Name(p_sOfficerCustField13Name)
		g_sOfficerCustField13Name = p_sOfficerCustField13Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField14Name()
		OfficerCustField14Name = g_sOfficerCustField14Name
	end property
	public property let OfficerCustField14Name(p_sOfficerCustField14Name)
		g_sOfficerCustField14Name = p_sOfficerCustField14Name
	end property	
	
	
	'Name to give to the custom field available in loan officer profiles.
	public property get OfficerCustField15Name()
		OfficerCustField15Name = g_sOfficerCustField15Name
	end property
	public property let OfficerCustField15Name(p_sOfficerCustField15Name)
		g_sOfficerCustField15Name = p_sOfficerCustField15Name
	end property	


	'Do we send a renewal reminder email to the loan officer 30 days before
	'license expiration?
	public property get Send30DayEmail()
		Send30DayEmail = g_bSend30DayEmail
	end property
	public property let Send30DayEmail(p_bSend30DayEmail)
		if isnumeric(p_bSend30DayEmail) then
			g_bSend30DayEmail = cbool(p_bSend30DayEmail)
		elseif p_bSend30DayEmail = "" then
			g_bSend30DayEmail = ""
		else
			ReportError("Invalid Send30DayEmail value.  Boolean required.")
		end if
	end property	

	'Do we send a 30 day renewal reminder email to the loan officer daily until
	'license expiration?
	public property get Send30DayEmailDaily()
		Send30DayEmailDaily = g_bSend30DayEmailDaily
	end property
	public property let Send30DayEmailDaily(p_bSend30DayEmailDaily)
		if isnumeric(p_bSend30DayEmailDaily) then
			g_bSend30DayEmailDaily = cbool(p_bSend30DayEmailDaily)
		elseif p_bSend30DayEmailDaily = "" then
			g_bSend30DayEmailDaily = ""
		else
			ReportError("Invalid Send30DayEmailDaily value.  Boolean required.")
		end if
	end property	
	

	'Do we send a renewal reminder email to the loan officer 60 days before
	'license expiration?
	public property get Send60DayEmail()
		Send60DayEmail = g_bSend60DayEmail
	end property
	public property let Send60DayEmail(p_bSend60DayEmail)
		if isnumeric(p_bSend60DayEmail) then
			g_bSend60DayEmail = cbool(p_bSend60DayEmail)
		elseif p_bSend60DayEmail = "" then
			g_bSend60DayEmail = ""
		else
			ReportError("Invalid Send60DayEmail value.  Boolean required.")
		end if
	end property	


	'Do we send a renewal reminder email to the loan officer 90 days before
	'license expiration?
	public property get Send90DayEmail()
		Send90DayEmail = g_bSend90DayEmail
	end property
	public property let Send90DayEmail(p_bSend90DayEmail)
		if isnumeric(p_bSend90DayEmail) then
			g_bSend90DayEmail = cbool(p_bSend90DayEmail)
		elseif p_bSend90DayEmail = "" then
			g_bSend90DayEmail = ""
		else
			ReportError("Invalid Send90DayEmail value.  Boolean required.")
		end if
	end property


	'Do we send a course completion notice email at all?
	public property get SendCourseEmail()
		SendCourseEmail = g_bSendCourseEmail
	end property
	public property let SendCourseEmail(p_bSendCourseEmail)
		if isnumeric(p_bSendCourseEmail) then
			g_bSendCourseEmail = cbool(p_bSendCourseEmail)
		elseif p_bSendCourseEmail = "" then
			g_bSendCourseEmail = ""
		else
			ReportError("Invalid SendCourseEmail value.  Boolean required.")
		end if
	end property
	
	
	'Do we send a renewal reminder email to the loan officer 30 days before
	'course expiration?
	public property get Send30DayCourseEmail()
		Send30DayCourseEmail = g_bSend30DayCourseEmail
	end property
	public property let Send30DayCourseEmail(p_bSend30DayCourseEmail)
		if isnumeric(p_bSend30DayCourseEmail) then
			g_bSend30DayCourseEmail = cbool(p_bSend30DayCourseEmail)
		elseif p_bSend30DayCourseEmail = "" then
			g_bSend30DayCourseEmail = ""
		else
			ReportError("Invalid Send30DayCourseEmail value.  Boolean required.")
		end if
	end property	

	'Do we send a 30 day course renewal reminder email to the loan officer daily until
	'license expiration?
	public property get Send30DayCourseEmailDaily()
		Send30DayCourseEmailDaily = g_bSend30DayCourseEmailDaily
	end property
	public property let Send30DayCourseEmailDaily(p_bSend30DayCourseEmailDaily)
		if isnumeric(p_bSend30DayCourseEmailDaily) then
			g_bSend30DayCourseEmailDaily = cbool(p_bSend30DayCourseEmailDaily)
		elseif p_bSend30DayCourseEmailDaily = "" then
			g_bSend30DayCourseEmailDaily = ""
		else
			ReportError("Invalid Send30DayCourseEmailDaily value.  Boolean required.")
		end if
	end property	


	'Do we send a renewal reminder email to the loan officer 60 days before
	'course expiration?
	public property get Send60DayCourseEmail()
		Send60DayCourseEmail = g_bSend60DayCourseEmail
	end property
	public property let Send60DayCourseEmail(p_bSend60DayCourseEmail)
		if isnumeric(p_bSend60DayCourseEmail) then
			g_bSend60DayCourseEmail = cbool(p_bSend60DayCourseEmail)
		elseif p_bSend60DayCourseEmail = "" then
			g_bSend60DayCourseEmail = ""
		else
			ReportError("Invalid Send60DayCourseEmail value.  Boolean required.")
		end if
	end property	
	
	
	'Do we send a renewal reminder email to the loan officer 90 days before
	'course expiration?
	public property get Send90DayCourseEmail()
		Send90DayCourseEmail = g_bSend90DayCourseEmail
	end property
	public property let Send90DayCourseEmail(p_bSend90DayCourseEmail)
		if isnumeric(p_bSend90DayCourseEmail) then
			g_bSend90DayCourseEmail = cbool(p_bSend90DayCourseEmail)
		elseif p_bSend90DayCourseEmail = "" then
			g_bSend90DayCourseEmail = ""
		else
			ReportError("Invalid Send90DayCourseEmail value.  Boolean required.")
		end if
	end property	
	
	
	'Do we CC the 30 day alert to branch admins?
	public property get CcBrAdmin30Day()
		CcBrAdmin30Day = g_bCcBrAdmin30Day
	end property
	public property let CcBrAdmin30Day(p_bCcBrAdmin30Day)
		if isnumeric(p_bCcBrAdmin30Day) then
			g_bCcBrAdmin30Day = cbool(p_bCcBrAdmin30Day)
		elseif p_bCcBrAdmin30Day = "" then
			g_bCcBrAdmin30Day = ""
		else
			ReportError("Invalid CcBrAdmin30Day value.  Boolean required.")
		end if
	end property


	'Do we CC the 30 day alert to company admins?
	public property get CcCoAdmin30Day()
		CcCoAdmin30Day = g_bCcCoAdmin30Day
	end property
	public property let CcCoAdmin30Day(p_bCcCoAdmin30Day)
		if isnumeric(p_bCcCoAdmin30Day) then
			g_bCcCoAdmin30Day = cbool(p_bCcCoAdmin30Day)
		elseif p_bCcCoAdmin30Day = "" then
			g_bCcCoAdmin30Day = ""
		else
			ReportError("Invalid CcCoAdmin30Day value.  Boolean required.")
		end if
	end property	


	'Do we CC the 30 day alert to L2 admins?
	public property get CcL2Admin30Day()
		CcL2Admin30Day = g_bCcL2Admin30Day
	end property
	public property let CcL2Admin30Day(p_bCcL2Admin30Day)
		if isnumeric(p_bCcL2Admin30Day) then
			g_bCcL2Admin30Day = cbool(p_bCcL2Admin30Day)
		elseif p_bCcL2Admin30Day = "" then
			g_bCcL2Admin30Day = ""
		else
			ReportError("Invalid CcL2Admin30Day value.  Boolean required.")
		end if
	end property	


	'Do we CC the 30 day alert to L3 admins?
	public property get CcL3Admin30Day()
		CcL3Admin30Day = g_bCcL3Admin30Day
	end property
	public property let CcL3Admin30Day(p_bCcL3Admin30Day)
		if isnumeric(p_bCcL3Admin30Day) then
			g_bCcL3Admin30Day = cbool(p_bCcL3Admin30Day)
		elseif p_bCcL3Admin30Day = "" then
			g_bCcL3Admin30Day = ""
		else
			ReportError("Invalid CcL3Admin30Day value.  Boolean required.")
		end if
	end property	


	'Do we CC the 60 day alert to branch admins?
	public property get CcBrAdmin60Day()
		CcBrAdmin60Day = g_bCcBrAdmin60Day
	end property
	public property let CcBrAdmin60Day(p_bCcBrAdmin60Day)
		if isnumeric(p_bCcBrAdmin60Day) then
			g_bCcBrAdmin60Day = cbool(p_bCcBrAdmin60Day)
		elseif p_bCcBrAdmin60Day = "" then
			g_bCcBrAdmin60Day = ""
		else
			ReportError("Invalid CcBrAdmin60Day value.  Boolean required.")
		end if
	end property


	'Do we CC the 60 day alert to company admins?
	public property get CcCoAdmin60Day()
		CcCoAdmin60Day = g_bCcCoAdmin60Day
	end property
	public property let CcCoAdmin60Day(p_bCcCoAdmin60Day)
		if isnumeric(p_bCcCoAdmin60Day) then
			g_bCcCoAdmin60Day = cbool(p_bCcCoAdmin60Day)
		elseif p_bCcCoAdmin60Day = "" then
			g_bCcCoAdmin60Day = ""
		else
			ReportError("Invalid CcCoAdmin60Day value.  Boolean required.")
		end if
	end property
	
	
	'Do we CC the 60 day alert to L2 admins?
	public property get CcL2Admin60Day()
		CcL2Admin60Day = g_bCcL2Admin60Day
	end property
	public property let CcL2Admin60Day(p_bCcL2Admin60Day)
		if isnumeric(p_bCcL2Admin60Day) then
			g_bCcL2Admin60Day = cbool(p_bCcL2Admin60Day)
		elseif p_bCcL2Admin60Day = "" then
			g_bCcL2Admin60Day = ""
		else
			ReportError("Invalid CcL2Admin60Day value.  Boolean required.")
		end if
	end property	


	'Do we CC the 60 day alert to L3 admins?
	public property get CcL3Admin60Day()
		CcL3Admin60Day = g_bCcL3Admin60Day
	end property
	public property let CcL3Admin60Day(p_bCcL3Admin60Day)
		if isnumeric(p_bCcL3Admin60Day) then
			g_bCcL3Admin60Day = cbool(p_bCcL3Admin60Day)
		elseif p_bCcL3Admin60Day = "" then
			g_bCcL3Admin60Day = ""
		else
			ReportError("Invalid CcL3Admin60Day value.  Boolean required.")
		end if
	end property
	
	
	'Do we CC the 90 day alert to branch admins?
	public property get CcBrAdmin90Day()
		CcBrAdmin90Day = g_bCcBrAdmin90Day
	end property
	public property let CcBrAdmin90Day(p_bCcBrAdmin90Day)
		if isnumeric(p_bCcBrAdmin90Day) then
			g_bCcBrAdmin90Day = cbool(p_bCcBrAdmin90Day)
		elseif p_bCcBrAdmin90Day = "" then
			g_bCcBrAdmin90Day = ""
		else
			ReportError("Invalid CcBrAdmin90Day value.  Boolean required.")
		end if
	end property


	'Do we CC the 90 day alert to company admins?
	public property get CcCoAdmin90Day()
		CcCoAdmin90Day = g_bCcCoAdmin90Day
	end property
	public property let CcCoAdmin90Day(p_bCcCoAdmin90Day)
		if isnumeric(p_bCcCoAdmin90Day) then
			g_bCcCoAdmin90Day = cbool(p_bCcCoAdmin90Day)
		elseif p_bCcCoAdmin90Day = "" then
			g_bCcCoAdmin90Day = ""
		else
			ReportError("Invalid CcCoAdmin90Day value.  Boolean required.")
		end if
	end property
	
	
	'Do we CC the 90 day alert to L2 admins?
	public property get CcL2Admin90Day()
		CcL2Admin90Day = g_bCcL2Admin90Day
	end property
	public property let CcL2Admin90Day(p_bCcL2Admin90Day)
		if isnumeric(p_bCcL2Admin90Day) then
			g_bCcL2Admin90Day = cbool(p_bCcL2Admin90Day)
		elseif p_bCcL2Admin90Day = "" then
			g_bCcL2Admin90Day = ""
		else
			ReportError("Invalid CcL2Admin90Day value.  Boolean required.")
		end if
	end property	


	'Do we CC the 90 day alert to L3 admins?
	public property get CcL3Admin90Day()
		CcL3Admin90Day = g_bCcL3Admin90Day
	end property
	public property let CcL3Admin90Day(p_bCcL3Admin90Day)
		if isnumeric(p_bCcL3Admin90Day) then
			g_bCcL3Admin90Day = cbool(p_bCcL3Admin90Day)
		elseif p_bCcL3Admin90Day = "" then
			g_bCcL3Admin90Day = ""
		else
			ReportError("Invalid CcL3Admin90Day value.  Boolean required.")
		end if
	end property
	
	
	'Do we CC the 30 day course alert to branch admins?
	public property get CcBrAdmin30DayCourse()
		CcBrAdmin30DayCourse = g_bCcBrAdmin30DayCourse
	end property
	public property let CcBrAdmin30DayCourse(p_bCcBrAdmin30DayCourse)
		if isnumeric(p_bCcBrAdmin30DayCourse) then
			g_bCcBrAdmin30DayCourse = cbool(p_bCcBrAdmin30DayCourse)
		elseif p_bCcBrAdmin30DayCourse = "" then
			g_bCcBrAdmin30DayCourse = ""
		else
			ReportError("Invalid CcBrAdmin30DayCourse value.  Boolean required.")
		end if
	end property


	'Do we CC the 30 day alert to company admins?
	public property get CcCoAdmin30DayCourse()
		CcCoAdmin30DayCourse = g_bCcCoAdmin30DayCourse
	end property
	public property let CcCoAdmin30DayCourse(p_bCcCoAdmin30DayCourse)
		if isnumeric(p_bCcCoAdmin30DayCourse) then
			g_bCcCoAdmin30DayCourse = cbool(p_bCcCoAdmin30DayCourse)
		elseif p_bCcCoAdmin30DayCourse = "" then
			g_bCcCoAdmin30DayCourse = ""
		else
			ReportError("Invalid CcCoAdmin30DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 30 day alert to L2 admins?
	public property get CcL2Admin30DayCourse()
		CcL2Admin30DayCourse = g_bCcL2Admin30DayCourse
	end property
	public property let CcL2Admin30DayCourse(p_bCcL2Admin30DayCourse)
		if isnumeric(p_bCcL2Admin30DayCourse) then
			g_bCcL2Admin30DayCourse = cbool(p_bCcL2Admin30DayCourse)
		elseif p_bCcL2Admin30DayCourse = "" then
			g_bCcL2Admin30DayCourse = ""
		else
			ReportError("Invalid CcL2Admin30DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 30 day alert to L3 admins?
	public property get CcL3Admin30DayCourse()
		CcL3Admin30DayCourse = g_bCcL3Admin30DayCourse
	end property
	public property let CcL3Admin30DayCourse(p_bCcL3Admin30DayCourse)
		if isnumeric(p_bCcL3Admin30DayCourse) then
			g_bCcL3Admin30DayCourse = cbool(p_bCcL3Admin30DayCourse)
		elseif p_bCcL3Admin30DayCourse = "" then
			g_bCcL3Admin30DayCourse = ""
		else
			ReportError("Invalid CcL3Admin30DayCourse value.  Boolean required.")
		end if
	end property	
	
	
	'Do we CC the 60 day course alert to branch admins?
	public property get CcBrAdmin60DayCourse()
		CcBrAdmin60DayCourse = g_bCcBrAdmin60DayCourse
	end property
	public property let CcBrAdmin60DayCourse(p_bCcBrAdmin60DayCourse)
		if isnumeric(p_bCcBrAdmin60DayCourse) then
			g_bCcBrAdmin60DayCourse = cbool(p_bCcBrAdmin60DayCourse)
		elseif p_bCcBrAdmin60DayCourse = "" then
			g_bCcBrAdmin60DayCourse = ""
		else
			ReportError("Invalid CcBrAdmin60DayCourse value.  Boolean required.")
		end if
	end property


	'Do we CC the 60 day alert to company admins?
	public property get CcCoAdmin60DayCourse()
		CcCoAdmin60DayCourse = g_bCcCoAdmin60DayCourse
	end property
	public property let CcCoAdmin60DayCourse(p_bCcCoAdmin60DayCourse)
		if isnumeric(p_bCcCoAdmin60DayCourse) then
			g_bCcCoAdmin60DayCourse = cbool(p_bCcCoAdmin60DayCourse)
		elseif p_bCcCoAdmin60DayCourse = "" then
			g_bCcCoAdmin60DayCourse = ""
		else
			ReportError("Invalid CcCoAdmin60DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 60 day alert to L2 admins?
	public property get CcL2Admin60DayCourse()
		CcL2Admin60DayCourse = g_bCcL2Admin60DayCourse
	end property
	public property let CcL2Admin60DayCourse(p_bCcL2Admin60DayCourse)
		if isnumeric(p_bCcL2Admin60DayCourse) then
			g_bCcL2Admin60DayCourse = cbool(p_bCcL2Admin60DayCourse)
		elseif p_bCcL2Admin60DayCourse = "" then
			g_bCcL2Admin60DayCourse = ""
		else
			ReportError("Invalid CcL2Admin60DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 60 day alert to L3 admins?
	public property get CcL3Admin60DayCourse()
		CcL3Admin60DayCourse = g_bCcL3Admin60DayCourse
	end property
	public property let CcL3Admin60DayCourse(p_bCcL3Admin60DayCourse)
		if isnumeric(p_bCcL3Admin60DayCourse) then
			g_bCcL3Admin60DayCourse = cbool(p_bCcL3Admin60DayCourse)
		elseif p_bCcL3Admin60DayCourse = "" then
			g_bCcL3Admin60DayCourse = ""
		else
			ReportError("Invalid CcL3Admin60DayCourse value.  Boolean required.")
		end if
	end property	
	
	
	'Do we CC the 90 day course alert to branch admins?
	public property get CcBrAdmin90DayCourse()
		CcBrAdmin90DayCourse = g_bCcBrAdmin90DayCourse
	end property
	public property let CcBrAdmin90DayCourse(p_bCcBrAdmin90DayCourse)
		if isnumeric(p_bCcBrAdmin90DayCourse) then
			g_bCcBrAdmin90DayCourse = cbool(p_bCcBrAdmin90DayCourse)
		elseif p_bCcBrAdmin90DayCourse = "" then
			g_bCcBrAdmin90DayCourse = ""
		else
			ReportError("Invalid CcBrAdmin90DayCourse value.  Boolean required.")
		end if
	end property


	'Do we CC the 90 day alert to company admins?
	public property get CcCoAdmin90DayCourse()
		CcCoAdmin90DayCourse = g_bCcCoAdmin90DayCourse
	end property
	public property let CcCoAdmin90DayCourse(p_bCcCoAdmin90DayCourse)
		if isnumeric(p_bCcCoAdmin90DayCourse) then
			g_bCcCoAdmin90DayCourse = cbool(p_bCcCoAdmin90DayCourse)
		elseif p_bCcCoAdmin90DayCourse = "" then
			g_bCcCoAdmin90DayCourse = ""
		else
			ReportError("Invalid CcCoAdmin90DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 90 day alert to L2 admins?
	public property get CcL2Admin90DayCourse()
		CcL2Admin90DayCourse = g_bCcL2Admin90DayCourse
	end property
	public property let CcL2Admin90DayCourse(p_bCcL2Admin90DayCourse)
		if isnumeric(p_bCcL2Admin90DayCourse) then
			g_bCcL2Admin90DayCourse = cbool(p_bCcL2Admin90DayCourse)
		elseif p_bCcL2Admin90DayCourse = "" then
			g_bCcL2Admin90DayCourse = ""
		else
			ReportError("Invalid CcL2Admin90DayCourse value.  Boolean required.")
		end if
	end property	


	'Do we CC the 90 day alert to L3 admins?
	public property get CcL3Admin90DayCourse()
		CcL3Admin90DayCourse = g_bCcL3Admin90DayCourse
	end property
	public property let CcL3Admin90DayCourse(p_bCcL3Admin90DayCourse)
		if isnumeric(p_bCcL3Admin90DayCourse) then
			g_bCcL3Admin90DayCourse = cbool(p_bCcL3Admin90DayCourse)
		elseif p_bCcL3Admin90DayCourse = "" then
			g_bCcL3Admin90DayCourse = ""
		else
			ReportError("Invalid CcL3Admin90DayCourse value.  Boolean required.")
		end if
	end property	
	
	
	'Do we send a course completion notice to the company admins?
	public property get SendCoAdminCourseEmail()
		SendCoAdminCourseEmail = g_bSendCoAdminCourseEmail
	end property
	public property let SendCoAdminCourseEmail(p_bSendCoAdminCourseEmail)
		if isnumeric(p_bSendCoAdminCourseEmail) then
			g_bSendCoAdminCourseEmail = cbool(p_bSendCoAdminCourseEmail)
		elseif p_bSendCoAdminCourseEmail = "" then
			g_bSendCoAdminCourseEmail = ""
		else
			ReportError("Invalid SendCoAdminCourseEmail value.  Boolean required.")
		end if
	end property


	'Do we send a course completion notice to the branch admins?
	public property get SendBrAdminCourseEmail()
		SendBrAdminCourseEmail = g_bSendBrAdminCourseEmail
	end property
	public property let SendBrAdminCourseEmail(p_bSendBrAdminCourseEmail)
		if isnumeric(p_bSendBrAdminCourseEmail) then
			g_bSendBrAdminCourseEmail = cbool(p_bSendBrAdminCourseEmail)
		elseif p_bSendBrAdminCourseEmail = "" then
			g_bSendBrAdminCourseEmail = ""
		else
			ReportError("Invalid SendBrAdminCourseEmail value.  Boolean required.")
		end if
	end property


	'Do we send a course completion notice to the L2 admins?
	public property get SendL2AdminCourseEmail()
		SendL2AdminCourseEmail = g_bSendL2AdminCourseEmail
	end property
	public property let SendL2AdminCourseEmail(p_bSendL2AdminCourseEmail)
		if isnumeric(p_bSendL2AdminCourseEmail) then
			g_bSendL2AdminCourseEmail = cbool(p_bSendL2AdminCourseEmail)
		elseif p_bSendL2AdminCourseEmail = "" then
			g_bSendL2AdminCourseEmail = ""
		else
			ReportError("Invalid SendL2AdminCourseEmail value.  Boolean required.")
		end if
	end property
	
	
	'Do we send a course completion notice to the L3 admins?
	public property get SendL3AdminCourseEmail()
		SendL3AdminCourseEmail = g_bSendL3AdminCourseEmail
	end property
	public property let SendL3AdminCourseEmail(p_bSendL3AdminCourseEmail)
		if isnumeric(p_bSendL3AdminCourseEmail) then
			g_bSendL3AdminCourseEmail = cbool(p_bSendL3AdminCourseEmail)
		elseif p_bSendL3AdminCourseEmail = "" then
			g_bSendL3AdminCourseEmail = ""
		else
			ReportError("Invalid SendL3AdminCourseEmail value.  Boolean required.")
		end if
	end property


	'Do we send a course completion notice to the loan officers?
	public property get SendLoCourseEmail()
		SendLoCourseEmail = g_bSendLoCourseEmail
	end property
	public property let SendLoCourseEmail(p_bSendLoCourseEmail)
		if isnumeric(p_bSendLoCourseEmail) then
			g_bSendLoCourseEmail = cbool(p_bSendLoCourseEmail)
		elseif p_bSendLoCourseEmail = "" then
			g_bSendLoCourseEmail = ""
		else
			ReportError("Invalid SendLoCourseEmail value.  Boolean required.")
		end if
	end property
	

	'Do we send a Event notice to the company admins?
	public property get SendCoAdminEventEmail()
		SendCoAdminEventEmail = g_bSendCoAdminEventEmail
	end property
	public property let SendCoAdminEventEmail(p_bSendCoAdminEventEmail)
		if isnumeric(p_bSendCoAdminEventEmail) then
			g_bSendCoAdminEventEmail = cbool(p_bSendCoAdminEventEmail)
		elseif p_bSendCoAdminEventEmail = "" then
			g_bSendCoAdminEventEmail = ""
		else
			ReportError("Invalid SendCoAdminEventEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Event notice to the branch admins?
	public property get SendBrAdminEventEmail()
		SendBrAdminEventEmail = g_bSendBrAdminEventEmail
	end property
	public property let SendBrAdminEventEmail(p_bSendBrAdminEventEmail)
		if isnumeric(p_bSendBrAdminEventEmail) then
			g_bSendBrAdminEventEmail = cbool(p_bSendBrAdminEventEmail)
		elseif p_bSendBrAdminEventEmail = "" then
			g_bSendBrAdminEventEmail = ""
		else
			ReportError("Invalid SendBrAdminEventEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Event notice to the L2 admins?
	public property get SendL2AdminEventEmail()
		SendL2AdminEventEmail = g_bSendL2AdminEventEmail
	end property
	public property let SendL2AdminEventEmail(p_bSendL2AdminEventEmail)
		if isnumeric(p_bSendL2AdminEventEmail) then
			g_bSendL2AdminEventEmail = cbool(p_bSendL2AdminEventEmail)
		elseif p_bSendL2AdminEventEmail = "" then
			g_bSendL2AdminEventEmail = ""
		else
			ReportError("Invalid SendL2AdminEventEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Event notice to the L3 admins?
	public property get SendL3AdminEventEmail()
		SendL3AdminEventEmail = g_bSendL3AdminEventEmail
	end property
	public property let SendL3AdminEventEmail(p_bSendL3AdminEventEmail)
		if isnumeric(p_bSendL3AdminEventEmail) then
			g_bSendL3AdminEventEmail = cbool(p_bSendL3AdminEventEmail)
		elseif p_bSendL3AdminEventEmail = "" then
			g_bSendL3AdminEventEmail = ""
		else
			ReportError("Invalid SendL3AdminEventEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Event notice to the loan officers?
	public property get SendLoEventEmail()
		SendLoEventEmail = g_bSendLoEventEmail
	end property
	public property let SendLoEventEmail(p_bSendLoEventEmail)
		if isnumeric(p_bSendLoEventEmail) then
			g_bSendLoEventEmail = cbool(p_bSendLoEventEmail)
		elseif p_bSendLoEventEmail = "" then
			g_bSendLoEventEmail = ""
		else
			ReportError("Invalid SendLoEventEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Fingerprint Deadline notice to the branch admins?
	public property get SendBrAdminFingerprintDeadlineEmail()
		SendBrAdminFingerprintDeadlineEmail = g_bSendBrAdminFingerprintDeadlineEmail
	end property
	public property let SendBrAdminFingerprintDeadlineEmail(p_bSendBrAdminFingerprintDeadlineEmail)
		if isnumeric(p_bSendBrAdminFingerprintDeadlineEmail) then
			g_bSendBrAdminFingerprintDeadlineEmail = cbool(p_bSendBrAdminFingerprintDeadlineEmail)
		elseif p_bSendBrAdminFingerprintDeadlineEmail = "" then
			g_bSendBrAdminFingerprintDeadlineEmail = ""
		else
			ReportError("Invalid SendBrAdminFingerprintDeadlineEmail value.  Boolean required.")
		end if
	end property	
	
	
	'Do we send a Fingerprint Deadline notice to the company admins?
	public property get SendCoAdminFingerprintDeadlineEmail()
		SendCoAdminFingerprintDeadlineEmail = g_bSendCoAdminFingerprintDeadlineEmail
	end property
	public property let SendCoAdminFingerprintDeadlineEmail(p_bSendCoAdminFingerprintDeadlineEmail)
		if isnumeric(p_bSendCoAdminFingerprintDeadlineEmail) then
			g_bSendCoAdminFingerprintDeadlineEmail = cbool(p_bSendCoAdminFingerprintDeadlineEmail)
		elseif p_bSendCoAdminFingerprintDeadlineEmail = "" then
			g_bSendCoAdminFingerprintDeadlineEmail = ""
		else
			ReportError("Invalid SendCoAdminFingerprintDeadlineEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Fingerprint Deadline notice to the L2 admins?
	public property get SendL2AdminFingerprintDeadlineEmail()
		SendL2AdminFingerprintDeadlineEmail = g_bSendL2AdminFingerprintDeadlineEmail
	end property
	public property let SendL2AdminFingerprintDeadlineEmail(p_bSendL2AdminFingerprintDeadlineEmail)
		if isnumeric(p_bSendL2AdminFingerprintDeadlineEmail) then
			g_bSendL2AdminFingerprintDeadlineEmail = cbool(p_bSendL2AdminFingerprintDeadlineEmail)
		elseif p_bSendL2AdminFingerprintDeadlineEmail = "" then
			g_bSendL2AdminFingerprintDeadlineEmail = ""
		else
			ReportError("Invalid SendL2AdminFingerprintDeadlineEmail value.  Boolean required.")
		end if
	end property


	'Do we send a Fingerprint Deadline notice to the L3 admins?
	public property get SendL3AdminFingerprintDeadlineEmail()
		SendL3AdminFingerprintDeadlineEmail = g_bSendL3AdminFingerprintDeadlineEmail
	end property
	public property let SendL3AdminFingerprintDeadlineEmail(p_bSendL3AdminFingerprintDeadlineEmail)
		if isnumeric(p_bSendL3AdminFingerprintDeadlineEmail) then
			g_bSendL3AdminFingerprintDeadlineEmail = cbool(p_bSendL3AdminFingerprintDeadlineEmail)
		elseif p_bSendL3AdminFingerprintDeadlineEmail = "" then
			g_bSendL3AdminFingerprintDeadlineEmail = ""
		else
			ReportError("Invalid SendL3AdminFingerprintDeadlineEmail value.  Boolean required.")
		end if
	end property	

	'Do we send a Fingerprint Deadline notice to the loan officers?
	public property get SendLoFingerprintDeadlineEmail()
		SendLoFingerprintDeadlineEmail = g_bSendLoFingerprintDeadlineEmail
	end property
	public property let SendLoFingerprintDeadlineEmail(p_bSendLoFingerprintDeadlineEmail)
		if isnumeric(p_bSendLoFingerprintDeadlineEmail) then
			g_bSendLoFingerprintDeadlineEmail = cbool(p_bSendLoFingerprintDeadlineEmail)
		elseif p_bSendLoFingerprintDeadlineEmail = "" then
			g_bSendLoFingerprintDeadlineEmail = ""
		else
			ReportError("Invalid SendLoFingerprintDeadlineEmail value.  Boolean required.")
		end if
	end property		
	
	'Do we send a 30 Day Email to the Loan Officer
	public property get SendLO30DayEmail()
		SendLO30DayEmail = g_bSendLO30DayEmail
	end property
	public property let SendLO30DayEmail(p_bSendLO30DayEmail)
		if isnumeric(p_bSendLO30DayEmail) then
			g_bSendLO30DayEmail = cbool(p_bSendLO30DayEmail)
		elseif p_bSendLO30DayEmail = "" then
			g_bSendLO30DayEmail = ""
		else
			ReportError("Invalid SendLO30DayEmail value.  Boolean required.")
		end if
	end property	
	
	'Do we send a 60 Day Email to the Loan Officer
	public property get SendLO60DayEmail()
		SendLO60DayEmail = g_bSendLO60DayEmail
	end property
	public property let SendLO60DayEmail(p_bSendLO60DayEmail)
		if isnumeric(p_bSendLO60DayEmail) then
			g_bSendLO60DayEmail = cbool(p_bSendLO60DayEmail)
		elseif p_bSendLO60DayEmail = "" then
			g_bSendLO60DayEmail = ""
		else
			ReportError("Invalid SendLO60DayEmail value.  Boolean required.")
		end if
	end property	
	
	'Do we send a 90 Day Email to the Loan Officer
	public property get SendLO90DayEmail()
		SendLO90DayEmail = g_bSendLO90DayEmail
	end property
	public property let SendLO90DayEmail(p_bSendLO90DayEmail)
		if isnumeric(p_bSendLO90DayEmail) then
			g_bSendLO90DayEmail = cbool(p_bSendLO90DayEmail)
		elseif p_bSendLO90DayEmail = "" then
			g_bSendLO90DayEmail = ""
		else
			ReportError("Invalid SendLO90DayEmail value.  Boolean required.")
		end if
	end property		
	
	'Do we use the Renewal App deadline instead of the License Expiration Date for the License exp emails
	public property get UseRenewalAppDeadlineLicExpEmails()
		UseRenewalAppDeadlineLicExpEmails = g_bUseRenewalAppDeadlineLicExpEmails
	end property
	public property let UseRenewalAppDeadlineLicExpEmails(p_bUseRenewalAppDeadlineLicExpEmails)
		if isnumeric(p_bUseRenewalAppDeadlineLicExpEmails) then
			g_bUseRenewalAppDeadlineLicExpEmails = cbool(p_bUseRenewalAppDeadlineLicExpEmails)
		elseif p_bUseRenewalAppDeadlineLicExpEmails = "" then
			g_bUseRenewalAppDeadlineLicExpEmails = ""
		else
			ReportError("Invalid UseRenewalAppDeadlineLicExpEmails value.  Boolean required.")
		end if
	end property	
	
	'This is the body text of the email we send 30 days before the license 
	'expires.
	public property get Email30DayText()
		Email30DayText = g_sEmail30DayText
	end property
	public property let Email30DayText(p_sEmail30DayText)
		g_sEmail30DayText = p_sEmail30DayText
	end property


	'This is the body text of the email we send 60 days before the license 
	'expires.
	public property get Email60DayText()
		Email60DayText = g_sEmail60DayText
	end property
	public property let Email60DayText(p_sEmail60DayText)
		g_sEmail60DayText = p_sEmail60DayText
	end property
	
	
	'This is the body text of the email we send 90 days before the license 
	'expires.
	public property get Email90DayText()
		Email90DayText = g_sEmail90DayText
	end property
	public property let Email90DayText(p_sEmail90DayText)
		g_sEmail90DayText = p_sEmail90DayText
	end property
	
	
	'This is the body text of the email we send when an officer is assigned
	'a new completed course.
	public property get CourseEmailText()
		CourseEmailText = g_sCourseEmailText
	end property
	public property let CourseEmailText(p_sCourseEmailText)
		g_sCourseEmailText = p_sCourseEmailText
	end property


	'This is the body text of the email we send 30 days before the course 
	'expires.
	public property get CourseEmail30DayText()
		CourseEmail30DayText = g_sCourseEmail30DayText
	end property
	public property let CourseEmail30DayText(p_sCourseEmail30DayText)
		g_sCourseEmail30DayText = p_sCourseEmail30DayText
	end property
	
	
	'This is the body text of the email we send 60 days before the course 
	'expires.
	public property get CourseEmail60DayText()
		CourseEmail60DayText = g_sCourseEmail60DayText
	end property
	public property let CourseEmail60DayText(p_sCourseEmail60DayText)
		g_sCourseEmail60DayText = p_sCourseEmail60DayText
	end property

	
	'This is the body text of the email we send 90 days before the course 
	'expires.
	public property get CourseEmail90DayText()
		CourseEmail90DayText = g_sCourseEmail90DayText
	end property
	public property let CourseEmail90DayText(p_sCourseEmail90DayText)
		g_sCourseEmail90DayText = p_sCourseEmail90DayText
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
		
		g_sCompanyName = ""
		g_sCompanyL2Name = ""
		g_sCompanyL3Name = ""
		g_sCustField1Name = ""
		g_sCustField2Name = ""
		g_sCustField3Name = ""
		g_sCustField4Name = ""
		g_sCustField5Name = ""
		g_sOfficerCustField1Name = ""
		g_sOfficerCustField2Name = ""
		g_sOfficerCustField3Name = ""
		g_sOfficerCustField4Name = ""
		g_sOfficerCustField5Name = ""
		g_sOfficerCustField6Name = ""
		g_sOfficerCustField7Name = ""
		g_sOfficerCustField8Name = ""
		g_sOfficerCustField9Name = ""
		g_sOfficerCustField10Name = ""
		g_sOfficerCustField11Name = ""
		g_sOfficerCustField12Name = ""
		g_sOfficerCustField13Name = ""
		g_sOfficerCustField14Name = ""
		g_sOfficerCustField15Name = ""				
		g_bSend30DayEmail = false
		g_bSend30DayEmailDaily = false
		g_bSend60DayEmail = false
		g_bSend90DayEmail = false
		g_bSendCourseEmail = false
		g_bSend30DayCourseEmail = false
		g_bSend30DayCourseEmailDaily = false		
		g_bSend60DayCourseEmail = false
		g_bSend90DayCourseEmail = false
		g_bCcBrAdmin30Day = false
		g_bCcCoAdmin30Day = false
		g_bCcL2Admin30Day = false
		g_bCcL3Admin30Day = false
		g_bCcBrAdmin60Day = false
		g_bCcCoAdmin60Day = false
		g_bCcL2Admin60Day = false
		g_bCcL3Admin60Day = false
		g_bCcBrAdmin90Day = false
		g_bCcCoAdmin90Day = false
		g_bCcL2Admin90Day = false
		g_bCcL3Admin90Day = false
		
		g_bCcBrAdmin30DayCourse = false
		g_bCcCoAdmin30DayCourse = false
		g_bCcL2Admin30DayCourse = false
		g_bCcL3Admin30DayCourse = false
		g_bCcBrAdmin60DayCourse = false
		g_bCcCoAdmin60DayCourse = false
		g_bCcL2Admin60DayCourse = false
		g_bCcL3Admin60DayCourse = false
		g_bCcBrAdmin90DayCourse = false
		g_bCcCoAdmin90DayCourse = false
		g_bCcL2Admin90DayCourse = false
		g_bCcL3Admin90DayCourse = false
		
		g_bSendCoAdminCourseEmail = false
		g_bSendBrAdminCourseEmail = false
		g_bSendL2AdminCourseEmail = false
		g_bSendL3AdminCourseEmail = false
		g_bSendLoCourseEmail = false
		g_bSendCoAdminEventEmail = false
		g_bSendBrAdminEventEmail = false
		g_bSendL2AdminEventEmail = false
		g_bSendL3AdminEventEmail = false
		g_bSendLoEventEmail = false
		
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	



	' PUBLIC METHODS ==========================================================
	
'	- GetOfficerCustFieldName(p_iNo)
'	- SetOfficerCustFieldName(p_iNo,p_sValue)	
	
	' -------------------------------------------------------------------------
	' Name: GetOfficerCustFieldName
	' Desc: Returns the value of the specified OfficerCustFieldName
	' Preconditions: none
	' Inputs: p_iNo - the number of the Officer Custom Field Name
	' Returns: string - the value of the Officer Custom Field Name
	' -------------------------------------------------------------------------
	public function	GetOfficerCustFieldName(p_iNo)
		if not checkintparam(p_iNo, false, "Number") then
			exit function
		end if
		
		select case p_iNo
			case 1
				GetOfficerCustFieldName = g_sOfficerCustField1Name
			case 2
				GetOfficerCustFieldName = g_sOfficerCustField2Name
			case 3
				GetOfficerCustFieldName = g_sOfficerCustField3Name
			case 4
				GetOfficerCustFieldName = g_sOfficerCustField4Name
			case 5
				GetOfficerCustFieldName = g_sOfficerCustField5Name
			case 6
				GetOfficerCustFieldName = g_sOfficerCustField6Name
			case 7
				GetOfficerCustFieldName = g_sOfficerCustField7Name
			case 8
				GetOfficerCustFieldName = g_sOfficerCustField8Name
			case 9
				GetOfficerCustFieldName = g_sOfficerCustField9Name
			case 10
				GetOfficerCustFieldName = g_sOfficerCustField10Name
			case 11
				GetOfficerCustFieldName = g_sOfficerCustField11Name
			case 12
				GetOfficerCustFieldName = g_sOfficerCustField12Name
			case 13
				GetOfficerCustFieldName = g_sOfficerCustField13Name
			case 14
				GetOfficerCustFieldName = g_sOfficerCustField14Name
			case 15
				GetOfficerCustFieldName = g_sOfficerCustField15Name		
			case else
				GetOfficerCustField = ""
		end select
	end function
	
	' -------------------------------------------------------------------------
	' Name: SetOfficerCustFieldName
	' Desc: Returns the value of the specified OfficerCustFieldName
	' Preconditions: none
	' Inputs: p_iNo - the number of the Officer Custom Field Name
	'		  p_sValue - the value to set the Officer Custom Field Name to
	' Returns: n/a
	' -------------------------------------------------------------------------
	public sub SetOfficerCustFieldName(p_iNo,p_sValue)
		if not checkintparam(p_iNo, false, "Number") then
			exit sub
		end if
		
		select case p_iNo
			case 1
				g_sOfficerCustField1Name = p_sValue
			case 2
				g_sOfficerCustField2Name = p_sValue
			case 3
				g_sOfficerCustField3Name = p_sValue
			case 4
				g_sOfficerCustField4Name = p_sValue
			case 5
				g_sOfficerCustField5Name = p_sValue
			case 6
				g_sOfficerCustField6Name = p_sValue
			case 7
				g_sOfficerCustField7Name = p_sValue
			case 8
				g_sOfficerCustField8Name = p_sValue
			case 9
				g_sOfficerCustField9Name = p_sValue
			case 10
				g_sOfficerCustField10Name = p_sValue
			case 11
				g_sOfficerCustField11Name = p_sValue
			case 12
				g_sOfficerCustField12Name = p_sValue
			case 13
				g_sOfficerCustField13Name = p_sValue
			case 14
				g_sOfficerCustField14Name = p_sValue
			case 15
				g_sOfficerCustField15Name = p_sValue
		end select
	end sub
	
	' -------------------------------------------------------------------------
	' Name: LoadSettingsById
	' Description: Load a settings group based on the passed SettingsId
	' Preconditions: ConnectionString must be set
	' Inputs: p_iSettingsId - ID of the settings to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadSettingsById(p_iSettingsId)
		
		LoadSettingsById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iSettingsId, 0, "Settings ID") then
			exit function
		end if 


		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM CompanySettings " & _
			"WHERE SettingsId = '" & p_iSettingsId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the Settings properties
			g_iSettingsId = oRs("SettingsId")
			g_iCompanyId = oRs("CompanyId")
			g_bActive = oRs("Active")
			g_bAllowOfficerNoteEditing = oRs("AllowOfficerNoteEditing")
			g_sCompanyName = oRs("CompanyName")
			g_sCompanyL2Name = oRs("CompanyL2Name")
			g_sCompanyL3Name = oRs("CompanyL3Name")
			g_sCustField1Name = oRs("CustField1Name")
			g_sCustField2Name = oRs("CustField2Name")
			g_sCustField3Name = oRs("CustField3Name")
			g_sCustField4Name = oRs("CustField4Name")
			g_sCustField5Name = oRs("CustField5Name")
			g_sOfficerCustField1Name = oRs("OfficerCustField1Name")
			g_sOfficerCustField2Name = oRs("OfficerCustField2Name")
			g_sOfficerCustField3Name = oRs("OfficerCustField3Name")
			g_sOfficerCustField4Name = oRs("OfficerCustField4Name")
			g_sOfficerCustField5Name = oRs("OfficerCustField5Name")
			g_sOfficerCustField6Name = oRs("OfficerCustField6Name")
			g_sOfficerCustField7Name = oRs("OfficerCustField7Name")
			g_sOfficerCustField8Name = oRs("OfficerCustField8Name")
			g_sOfficerCustField9Name = oRs("OfficerCustField9Name")
			g_sOfficerCustField10Name = oRs("OfficerCustField10Name")
			g_sOfficerCustField11Name = oRs("OfficerCustField11Name")
			g_sOfficerCustField12Name = oRs("OfficerCustField12Name")
			g_sOfficerCustField13Name = oRs("OfficerCustField13Name")
			g_sOfficerCustField14Name = oRs("OfficerCustField14Name")
			g_sOfficerCustField15Name = oRs("OfficerCustField15Name")						
			g_bSend30DayEmail = oRs("Send30DayEmail")
			g_bSend30DayEmailDaily = oRs("Send30DayEmailDaily")
			g_bSend60DayEmail = oRs("Send60DayEmail")
			g_bSend90DayEmail = oRs("Send90DayEmail")
			g_bSendCourseEmail = oRs("SendCourseEmail")
			g_bSend30DayCourseEmail = oRs("Send30DayCourseEmail")
			g_bSend30DayCourseEmailDaily = oRs("Send30DayCourseEmailDaily")	
			g_bSend60DayCourseEmail = oRs("Send60DayCourseEmail")
			g_bSend90DayCourseEmail = oRs("Send90DayCourseEmail")
			g_bCcBrAdmin30Day = oRs("CcBrAdmin30Day")
			g_bCcCoAdmin30Day = oRs("CcCoAdmin30Day")
			g_bCcL2Admin30Day = oRs("CcL2Admin30Day")
			g_bCcL3Admin30Day = oRs("CcL3Admin30Day")
			g_bCcBrAdmin60Day = oRs("CcBrAdmin60Day")
			g_bCcCoAdmin60Day = oRs("CcCoAdmin60Day")
			g_bCcL2Admin60Day = oRs("CcL2Admin60Day")
			g_bCcL3Admin60Day = oRs("CcL3Admin60Day")
			g_bCcBrAdmin90Day = oRs("CcBrAdmin90Day")
			g_bCcCoAdmin90Day = oRs("CcCoAdmin90Day")
			g_bCcL2Admin90Day = oRs("CcL2Admin90Day")
			g_bCcL3Admin90Day = oRs("CcL3Admin90Day")
			
			g_bCcBrAdmin30DayCourse = oRs("CcBrAdmin30DayCourse")
			g_bCcCoAdmin30DayCourse = oRs("CcCoAdmin30DayCourse")
			g_bCcL2Admin30DayCourse = oRs("CcL2Admin30DayCourse")
			g_bCcL3Admin30DayCourse = oRs("CcL3Admin30DayCourse")
			g_bCcBrAdmin60DayCourse = oRs("CcBrAdmin60DayCourse")
			g_bCcCoAdmin60DayCourse = oRs("CcCoAdmin60DayCourse")
			g_bCcL2Admin60DayCourse = oRs("CcL2Admin60DayCourse")
			g_bCcL3Admin60DayCourse = oRs("CcL3Admin60DayCourse")
			g_bCcBrAdmin90DayCourse = oRs("CcBrAdmin90DayCourse")
			g_bCcCoAdmin90DayCourse = oRs("CcCoAdmin90DayCourse")
			g_bCcL2Admin90DayCourse = oRs("CcL2Admin90DayCourse")
			g_bCcL3Admin90DayCourse = oRs("CcL3Admin90DayCourse")
			
			g_bSendCoAdminCourseEmail = oRs("SendCoAdminCourseEmail")
			g_bSendBrAdminCourseEmail = oRs("SendBrAdminCourseEmail")
			g_bSendL2AdminCourseEmail = oRs("SendL2AdminCourseEmail")
			g_bSendL3AdminCourseEmail = oRs("SendL3AdminCourseEmail")
			g_bSendLoCourseEmail = oRs("SendLoCourseEmail")
			g_bSendCoAdminEventEmail = oRs("SendCoAdminEventEmail")
			g_bSendBrAdminEventEmail = oRs("SendBrAdminEventEmail")
			g_bSendL2AdminEventEmail = oRs("SendL2AdminEventEmail")
			g_bSendL3AdminEventEmail = oRs("SendL3AdminEventEmail")
			g_bSendLoEventEmail = oRs("SendLoEventEmail")

			g_sEmail30DayText = oRs("Email30DayText")
			g_sEmail60DayText = oRs("Email60DayText")
			g_sEmail90DayText = oRs("Email90DayText")
			g_sCourseEmailText = oRs("CourseEmailText")
			g_sCourseEmail30DayText = oRs("CourseEmail30DayText")
			g_sCourseEmail60DayText = oRs("CourseEmail60DayText")
			g_sCourseEmail90DayText = oRs("CourseEmail90DayText")

			g_bSendCoAdminFingerprintDeadlineEmail = oRs("SendCoAdminFingerprintDeadlineEmail")
			g_bSendBrAdminFingerprintDeadlineEmail = oRs("SendBrAdminFingerprintDeadlineEmail")
			g_bSendL2AdminFingerprintDeadlineEmail = oRs("SendL2AdminFingerprintDeadlineEmail")
			g_bSendL3AdminFingerprintDeadlineEmail = oRs("SendL3AdminFingerprintDeadlineEmail")
			g_bSendLoFingerprintDeadlineEmail = oRs("SendLoFingerprintDeadlineEmail")		

			g_bSendLO30DayEmail = oRs("SendLO30DayEmail")
			g_bSendLO60DayEmail = oRs("SendLO60DayEmail")
			g_bSendLO90DayEmail = oRs("SendLO90DayEmail")						
			
			g_bUseRenewalAppDeadlineLicExpEmails = oRs("UseRenewalAppDeadlineLicExpEmails")	
			
			LoadSettingsById = g_iSettingsId
			
		else
			
			ReportError("Unable to load the passed Settings ID.")
			exit function
		
		end if 
		
		set oRs = nothing
		
	end function
	

	' -------------------------------------------------------------------------
	' Name: LoadSettingsByCompanyId
	' Description: Load a settings group based on the passed CompanyId
	' Preconditions: ConnectionString must be set
	' Inputs: p_iCompanyId - ID of the Company whose Settings we'll load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadSettingsByCompanyId(p_iCompanyId)
		
		LoadSettingsByCompanyId = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if 


		dim oRs 'ADODB Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM CompanySettings " & _
			"WHERE CompanyId = '" & p_iCompanyId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the Settings properties
			g_iSettingsId = oRs("SettingsId")
			g_iCompanyId = oRs("CompanyId")
			g_bActive = oRs("Active")
			g_bAllowOfficerNoteEditing = oRs("AllowOfficerNoteEditing")
			g_sCompanyName = oRs("CompanyName")
			g_sCompanyL2Name = oRs("CompanyL2Name")
			g_sCompanyL3Name = oRs("CompanyL3Name")
			g_sCustField1Name = oRs("CustField1Name")
			g_sCustField2Name = oRs("CustField2Name")
			g_sCustField3Name = oRs("CustField3Name")
			g_sCustField4Name = oRs("CustField4Name")
			g_sCustField5Name = oRs("CustField5Name")
			g_sOfficerCustField1Name = oRs("OfficerCustField1Name")
			g_sOfficerCustField2Name = oRs("OfficerCustField2Name")
			g_sOfficerCustField3Name = oRs("OfficerCustField3Name")
			g_sOfficerCustField4Name = oRs("OfficerCustField4Name")
			g_sOfficerCustField5Name = oRs("OfficerCustField5Name")
			g_sOfficerCustField6Name = oRs("OfficerCustField6Name")
			g_sOfficerCustField7Name = oRs("OfficerCustField7Name")
			g_sOfficerCustField8Name = oRs("OfficerCustField8Name")
			g_sOfficerCustField9Name = oRs("OfficerCustField9Name")
			g_sOfficerCustField10Name = oRs("OfficerCustField10Name")
			g_sOfficerCustField11Name = oRs("OfficerCustField11Name")
			g_sOfficerCustField12Name = oRs("OfficerCustField12Name")
			g_sOfficerCustField13Name = oRs("OfficerCustField13Name")
			g_sOfficerCustField14Name = oRs("OfficerCustField14Name")
			g_sOfficerCustField15Name = oRs("OfficerCustField15Name")						
			g_bSend30DayEmail = oRs("Send30DayEmail")
			g_bSend30DayEmailDaily = oRs("Send30DayEmailDaily")
			g_bSend60DayEmail = oRs("Send60DayEmail")
			g_bSend90DayEmail = oRs("Send90DayEmail")
			g_bSendCourseEmail = oRs("SendCourseEmail")
			g_bSend30DayCourseEmail = oRs("Send30DayCourseEmail")
			g_bSend30DayCourseEmailDaily = oRs("Send30DayCourseEmailDaily")
			g_bSend60DayCourseEmail = oRs("Send60DayCourseEmail")
			g_bSend90DayCourseEmail = oRs("Send90DayCourseEmail")
			g_bCcBrAdmin30Day = oRs("CcBrAdmin30Day")
			g_bCcCoAdmin30Day = oRs("CcCoAdmin30Day")
			g_bCcL2Admin30Day = oRs("CcL2Admin30Day")
			g_bCcL3Admin30Day = oRs("CcL3Admin30Day")
			g_bCcBrAdmin60Day = oRs("CcBrAdmin60Day")
			g_bCcCoAdmin60Day = oRs("CcCoAdmin60Day")
			g_bCcL2Admin60Day = oRs("CcL2Admin60Day")
			g_bCcL3Admin60Day = oRs("CcL3Admin60Day")
			g_bCcBrAdmin90Day = oRs("CcBrAdmin90Day")
			g_bCcCoAdmin90Day = oRs("CcCoAdmin90Day")
			g_bCcL2Admin90Day = oRs("CcL2Admin90Day")
			g_bCcL3Admin90Day = oRs("CcL3Admin90Day")
			
			g_bCcBrAdmin30DayCourse = oRs("CcBrAdmin30DayCourse")
			g_bCcCoAdmin30DayCourse = oRs("CcCoAdmin30DayCourse")
			g_bCcL2Admin30DayCourse = oRs("CcL2Admin30DayCourse")
			g_bCcL3Admin30DayCourse = oRs("CcL3Admin30DayCourse")
			g_bCcBrAdmin60DayCourse = oRs("CcBrAdmin60DayCourse")
			g_bCcCoAdmin60DayCourse = oRs("CcCoAdmin60DayCourse")
			g_bCcL2Admin60DayCourse = oRs("CcL2Admin60DayCourse")
			g_bCcL3Admin60DayCourse = oRs("CcL3Admin60DayCourse")
			g_bCcBrAdmin90DayCourse = oRs("CcBrAdmin90DayCourse")
			g_bCcCoAdmin90DayCourse = oRs("CcCoAdmin90DayCourse")
			g_bCcL2Admin90DayCourse = oRs("CcL2Admin90DayCourse")
			g_bCcL3Admin90DayCourse = oRs("CcL3Admin90DayCourse")
			
			g_bSendCoAdminCourseEmail = oRs("SendCoAdminCourseEmail")
			g_bSendBrAdminCourseEmail = oRs("SendBrAdminCourseEmail")
			g_bSendL2AdminCourseEmail = oRs("SendL2AdminCourseEmail")
			g_bSendL3AdminCourseEmail = oRs("SendL3AdminCourseEmail")
			g_bSendLoCourseEmail = oRs("SendLoCourseEmail")
			g_bSendCoAdminEventEmail = oRs("SendCoAdminEventEmail")
			g_bSendBrAdminEventEmail = oRs("SendBrAdminEventEmail")
			g_bSendL2AdminEventEmail = oRs("SendL2AdminEventEmail")
			g_bSendL3AdminEventEmail = oRs("SendL3AdminEventEmail")
			g_bSendLoEventEmail = oRs("SendLoEventEmail")
			g_sEmail30DayText = oRs("Email30DayText")
			g_sEmail60DayText = oRs("Email60DayText")
			g_sEmail90DayText = oRs("Email90DayText")
			g_sCourseEmailText = oRs("CourseEmailText")
			g_sCourseEmail30DayText = oRs("CourseEmail30DayText")
			g_sCourseEmail60DayText = oRs("CourseEmail60DayText")
			g_sCourseEmail90DayText = oRs("CourseEmail90DayText")
			
			g_bSendCoAdminFingerprintDeadlineEmail = oRs("SendCoAdminFingerprintDeadlineEmail")
			g_bSendBrAdminFingerprintDeadlineEmail = oRs("SendBrAdminFingerprintDeadlineEmail")
			g_bSendL2AdminFingerprintDeadlineEmail = oRs("SendL2AdminFingerprintDeadlineEmail")
			g_bSendL3AdminFingerprintDeadlineEmail = oRs("SendL3AdminFingerprintDeadlineEmail")
			g_bSendLoFingerprintDeadlineEmail = oRs("SendLoFingerprintDeadlineEmail")					
			
			g_bSendLO30DayEmail = oRs("SendLO30DayEmail")
			g_bSendLO60DayEmail = oRs("SendLO60DayEmail")
			g_bSendLO90DayEmail = oRs("SendLO90DayEmail")			
			
			g_bUseRenewalAppDeadlineLicExpEmails = oRs("UseRenewalAppDeadlineLicExpEmails")	
			
			LoadSettingsByCompanyId = g_iSettingsId
			
		else
			
			ReportError("Unable to load the passed Settings ID.")
			exit function
		
		end if 
		
		set oRs = nothing
		
	end function

	
	' -------------------------------------------------------------------------
	' Name: SaveSettings
	' Description: Saves a Settings object's properties to the database.
	' Preconditions: ConnectionString
	' Returns: If successful, returns the saved/new ID, othewise 0.
	' -------------------------------------------------------------------------
	public function SaveSettings()
	
		SaveSettings = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iCompanyId, 0, "Company ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a SettingsId property assigned already, we're working on a
		'loaded Settings, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iSettingsId <> "" then
			
			bIsNew = false
			sSql = "UPDATE CompanySettings SET " & _
				"CompanyId = '" & g_iCompanyId & "', " & _
				"Active = '" & abs(cint(g_bActive)) & "', " & _
				"AllowOfficerNoteEditing = '" & abs(cint(g_bAllowOfficerNoteEditing)) & "', " & _
				"CompanyName = '" & g_sCompanyName & "', " & _
				"CompanyL2Name = '" & g_sCompanyL2Name & "', " & _
				"CompanyL3Name = '" & g_sCompanyL3Name & "', " & _
				"CustField1Name = '" & g_sCustField1Name & "', " & _
				"CustField2Name = '" & g_sCustField2Name & "', " & _
				"CustField3Name = '" & g_sCustField3Name & "', " & _
				"CustField4Name = '" & g_sCustField4Name & "', " & _
				"CustField5Name = '" & g_sCustField5Name & "', " & _
				"OfficerCustField1Name = '" & g_sOfficerCustField1Name & "', " & _
				"OfficerCustField2Name = '" & g_sOfficerCustField2Name & "', " & _
				"OfficerCustField3Name = '" & g_sOfficerCustField3Name & "', " & _
				"OfficerCustField4Name = '" & g_sOfficerCustField4Name & "', " & _
				"OfficerCustField5Name = '" & g_sOfficerCustField5Name & "', " & _
				"OfficerCustField6Name = '" & g_sOfficerCustField6Name & "', " & _
				"OfficerCustField7Name = '" & g_sOfficerCustField7Name & "', " & _
				"OfficerCustField8Name = '" & g_sOfficerCustField8Name & "', " & _
				"OfficerCustField9Name = '" & g_sOfficerCustField9Name & "', " & _
				"OfficerCustField10Name = '" & g_sOfficerCustField10Name & "', " & _
				"OfficerCustField11Name = '" & g_sOfficerCustField11Name & "', " & _
				"OfficerCustField12Name = '" & g_sOfficerCustField12Name & "', " & _
				"OfficerCustField13Name = '" & g_sOfficerCustField13Name & "', " & _
				"OfficerCustField14Name = '" & g_sOfficerCustField14Name & "', " & _
				"OfficerCustField15Name = '" & g_sOfficerCustField15Name & "', " & _								
				"Send30DayEmail = '" & abs(cint(g_bSend30DayEmail)) & "', " & _				
				"Send30DayEmailDaily = '" & abs(cint(g_bSend30DayEmailDaily)) & "', " & _
				"Send60DayEmail = '" & abs(cint(g_bSend60DayEmail)) & "', " & _
				"Send90DayEmail = '" & abs(cint(g_bSend90DayEmail)) & "', " & _
				"Send30DayCourseEmail = '" & abs(cint(g_bSend30DayCourseEmail)) & "', " & _
				"Send30DayCourseEmailDaily = '" & abs(cint(g_bSend30DayCourseEmailDaily)) & "', " & _
				"Send60DayCourseEmail = '" & abs(cint(g_bSend60DayCourseEmail)) & "', " & _
				"Send90DayCourseEmail = '" & abs(cint(g_bSend90DayCourseEmail)) & "', " & _
				"CcBrAdmin30Day = '" & abs(cint(g_bCcBrAdmin30Day)) & "', " & _
				"CcCoAdmin30Day = '" & abs(cint(g_bCcCoAdmin30Day)) & "', " & _
				"CcL2Admin30Day = '" & abs(cint(g_bCcL2Admin30Day)) & "', " & _
				"CcL3Admin30Day = '" & abs(cint(g_bCcL3Admin30Day)) & "', " & _				
				"CcBrAdmin60Day = '" & abs(cint(g_bCcBrAdmin60Day)) & "', " & _
				"CcCoAdmin60Day = '" & abs(cint(g_bCcCoAdmin60Day)) & "', " & _
				"CcL2Admin60Day = '" & abs(cint(g_bCcL2Admin60Day)) & "', " & _
				"CcL3Admin60Day = '" & abs(cint(g_bCcL3Admin60Day)) & "', " & _	
				"CcBrAdmin90Day = '" & abs(cint(g_bCcBrAdmin90Day)) & "', " & _
				"CcCoAdmin90Day = '" & abs(cint(g_bCcCoAdmin90Day)) & "', " & _
				"CcL2Admin90Day = '" & abs(cint(g_bCcL2Admin90Day)) & "', " & _
				"CcL3Admin90Day = '" & abs(cint(g_bCcL3Admin90Day)) & "', " & _
				"CcBrAdmin30DayCourse = '" & abs(cint(g_bCcBrAdmin30DayCourse)) & "', " & _
				"CcCoAdmin30DayCourse = '" & abs(cint(g_bCcCoAdmin30DayCourse)) & "', " & _
				"CcL2Admin30DayCourse = '" & abs(cint(g_bCcL2Admin30DayCourse)) & "', " & _
				"CcL3Admin30DayCourse = '" & abs(cint(g_bCcL3Admin30DayCourse)) & "', " & _				
				"CcBrAdmin60DayCourse = '" & abs(cint(g_bCcBrAdmin60DayCourse)) & "', " & _
				"CcCoAdmin60DayCourse = '" & abs(cint(g_bCcCoAdmin60DayCourse)) & "', " & _
				"CcL2Admin60DayCourse = '" & abs(cint(g_bCcL2Admin60DayCourse)) & "', " & _
				"CcL3Admin60DayCourse = '" & abs(cint(g_bCcL3Admin60DayCourse)) & "', " & _	
				"CcBrAdmin90DayCourse = '" & abs(cint(g_bCcBrAdmin90DayCourse)) & "', " & _
				"CcCoAdmin90DayCourse = '" & abs(cint(g_bCcCoAdmin90DayCourse)) & "', " & _
				"CcL2Admin90DayCourse = '" & abs(cint(g_bCcL2Admin90DayCourse)) & "', " & _
				"CcL3Admin90DayCourse = '" & abs(cint(g_bCcL3Admin90DayCourse)) & "', " & _		
				"SendCourseEmail = '" & _
				abs(cint(g_bSendCourseEmail)) & "', " & _
				"SendCoAdminCourseEmail = '" & _
				abs(cint(g_bSendCoAdminCourseEmail)) & "', " & _
				"SendBrAdminCourseEmail = '" & _
				abs(cint(g_bSendBrAdminCourseEmail)) & "', " & _
				"SendL2AdminCourseEmail = '" & _
				abs(cint(g_bSendL2AdminCourseEmail)) & "', " & _
				"SendL3AdminCourseEmail = '" & _
				abs(cint(g_bSendL3AdminCourseEmail)) & "', " & _
				"SendLoCourseEmail = '" & _
				abs(cint(g_bSendLoCourseEmail)) & "', " & _
				"SendCoAdminEventEmail = '" & abs(cint(g_bSendCoAdminEventEmail)) & "', " & _
				"SendBrAdminEventEmail = '" & abs(cint(g_bSendBrAdminEventEmail)) & "', " & _
				"SendL2AdminEventEmail = '" & abs(cint(g_bSendL2AdminEventEmail)) & "', " & _
				"SendL3AdminEventEmail = '" & abs(cint(g_bSendL3AdminEventEmail)) & "', " & _
				"SendLoEventEmail = '" & abs(cint(g_bSendLoEventEmail)) & "', " & _
				"SendCoAdminFingerprintDeadlineEmail = '" & abs(cint(g_bSendCoAdminFingerprintDeadlineEmail)) & "', " & _
				"SendBrAdminFingerprintDeadlineEmail = '" & abs(cint(g_bSendBrAdminFingerprintDeadlineEmail)) & "', " & _
				"SendL2AdminFingerprintDeadlineEmail = '" & abs(cint(g_bSendL2AdminFingerprintDeadlineEmail)) & "', " & _
				"SendL3AdminFingerprintDeadlineEmail = '" & abs(cint(g_bSendL3AdminFingerprintDeadlineEmail)) & "', " & _
				"SendLoFingerprintDeadlineEmail = '" & abs(cint(g_bSendLoFingerprintDeadlineEmail)) & "', " & _
				"SendLO30DayEmail = '" & abs(cint(g_bSendLO30DayEmail)) & "', " & _				
				"SendLO60DayEmail = '" & abs(cint(g_bSendLO60DayEmail)) & "', " & _				
				"SendLO90DayEmail = '" & abs(cint(g_bSendLO90DayEmail)) & "', " & _		
				"UseRenewalAppDeadlineLicExpEmails = '" & abs(cint(g_bUseRenewalAppDeadlineLicExpEmails)) & "', " & _														
				"Email30DayText = '" & g_sEmail30DayText & "', " & _
				"Email60DayText = '" & g_sEmail60DayText & "', " & _
				"Email90DayText = '" & g_sEmail90DayText & "', " & _
				"CourseEmail30DayText = '" & g_sCourseEmail30DayText & "', " & _
				"CourseEmail60DayText = '" & g_sCourseEmail60DayText & "', " & _
				"CourseEmail90DayText = '" & g_sCourseEmail90DayText & "', " & _
				"CourseEmailText = '" & g_sCourseEmailText & "' " & _
				"WHERE SettingsId = '" & g_iSettingsId & "'"

			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
			
				SaveSettings = g_iSettingsId
			
			else
			
				ReportError("Unable to save existing Settings group.")
				exit function
			
			end if 
				
		else 
			
			bIsNew = true
			sSql = "INSERT INTO CompanySettings (" & _
				"CompanyId, " & _
				"Active, " & _
				"AllowOfficerNoteEditing, " & _
				"CompanyName, " & _
				"CompanyL2Name, " & _
				"CompanyL3Name, " & _
				"CustField1Name, " & _
				"CustField2Name, " & _
				"CustField3Name, " & _
				"CustField4Name, " & _
				"CustField5Name, " & _
				"OfficerCustField1Name, " & _
				"OfficerCustField2Name, " & _
				"OfficerCustField3Name, " & _
				"OfficerCustField4Name, " & _
				"OfficerCustField5Name, " & _
				"OfficerCustField6Name, " & _
				"OfficerCustField7Name, " & _
				"OfficerCustField8Name, " & _
				"OfficerCustField9Name, " & _
				"OfficerCustField10Name, " & _
				"OfficerCustField11Name, " & _
				"OfficerCustField12Name, " & _
				"OfficerCustField13Name, " & _
				"OfficerCustField14Name, " & _
				"OfficerCustField15Name, " & _
				"Send30DayEmail, " & _
				"Send30DayEmailDaily, " & _
				"Send60DayEmail, " & _
				"Send90DayEmail, " & _
				"SendCourseEmail, " & _
				"Send30DayCourseEmail, " & _
				"Send30DayCourseEmailDaily, " & _				
				"Send60DayCourseEmail, " & _
				"Send90DayCourseEmail, " & _
				"CcBrAdmin30Day, " & _
				"CcCoAdmin30Day, " & _
				"CcL2Admin30Day, " & _
				"CcL3Admin30Day, " & _
				"CcBrAdmin60Day, " & _
				"CcCoAdmin60Day, " & _
				"CcL2Admin60Day, " & _
				"CcL3Admin60Day, " & _
				"CcBrAdmin90Day, " & _
				"CcCoAdmin90Day, " & _
				"CcL2Admin90Day, " & _
				"CcL3Admin90Day, " & _
				"CcBrAdmin30DayCourse, " & _
				"CcCoAdmin30DayCourse, " & _
				"CcL2Admin30DayCourse, " & _
				"CcL3Admin30DayCourse, " & _
				"CcBrAdmin60DayCourse, " & _
				"CcCoAdmin60DayCourse, " & _
				"CcL2Admin60DayCourse, " & _
				"CcL3Admin60DayCourse, " & _
				"CcBrAdmin90DayCourse, " & _
				"CcCoAdmin90DayCourse, " & _
				"CcL2Admin90DayCourse, " & _
				"CcL3Admin90DayCourse, " & _
				"SendCoAdminCourseEmail, " & _
				"SendBrAdminCourseEmail, " & _
				"SendL2AdminCourseEmail, " & _
				"SendL3AdminCourseEmail, " & _
				"SendLoCourseEmail, " & _
				"SendCoAdminEventEmail, " & _
				"SendBrAdminEventEmail, " & _
				"SendL2AdminEventEmail, " & _
				"SendL3AdminEventEmail, " & _
				"SendLoEventEmail, " & _
				"SendCoAdminFingerprintDeadlineEmail, " & _
				"SendBrAdminFingerprintDeadlineEmail, " & _
				"SendL2AdminFingerprintDeadlineEmail, " & _
				"SendL3AdminFingerprintDeadlineEmail, " & _
				"SendLoFingerprintDeadlineEmail, " & _				
				"SendLO30DayEmail, " & _
				"SendLO60DayEmail, " & _
				"SendLO90DayEmail, " & _				
				"UseRenewalAppDeadlineLicExpEmails, " & _							
				"Email30DayText, " & _
				"Email60DayText, " & _
				"Email90DayText, " & _
				"CourseEmail30DayText, " & _
				"CourseEmail60DayText, " & _
				"CourseEmail90DayText, " & _
				"CourseEmailText " & _
				") VALUES (" & _
				"'" & g_iCompanyId & "', " & _
				"'" & abs(cint(g_bActive)) & "', " & _				
				"'" & abs(cint(g_bAllowOfficerNoteEditing)) & "', " & _				
				"'" & g_sCompanyName & "', " & _
				"'" & g_sCompanyL2Name & "', " & _
				"'" & g_sCompanyL3Name & "', " & _
				"'" & g_sCustField1Name & "', " & _
				"'" & g_sCustField2Name & "', " & _
				"'" & g_sCustField3Name & "', " & _
				"'" & g_sCustField4Name & "', " & _
				"'" & g_sCustField5Name & "', " & _
				"'" & g_sOfficerCustField1Name & "', " & _
				"'" & g_sOfficerCustField2Name & "', " & _
				"'" & g_sOfficerCustField3Name & "', " & _
				"'" & g_sOfficerCustField4Name & "', " & _
				"'" & g_sOfficerCustField5Name & "', " & _
				"'" & g_sOfficerCustField6Name & "', " & _
				"'" & g_sOfficerCustField7Name & "', " & _
				"'" & g_sOfficerCustField8Name & "', " & _
				"'" & g_sOfficerCustField9Name & "', " & _
				"'" & g_sOfficerCustField10Name & "', " & _
				"'" & g_sOfficerCustField11Name & "', " & _
				"'" & g_sOfficerCustField12Name & "', " & _
				"'" & g_sOfficerCustField13Name & "', " & _
				"'" & g_sOfficerCustField14Name & "', " & _
				"'" & g_sOfficerCustField15Name & "', " & _								
				"'" & abs(cint(g_bSend30DayEmail)) & "', " & _				
				"'" & abs(cint(g_bSend30DayEmailDaily)) & "', " & _
				"'" & abs(cint(g_bSend60DayEmail)) & "', " & _
				"'" & abs(cint(g_bSend90DayEmail)) & "', " & _
				"'" & abs(cint(g_bSendCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSend30DayCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSend30DayCourseEmailDaily)) & "', " & _				
				"'" & abs(cint(g_bSend60DayCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSend90DayCourseEmail)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin30Day)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin30Day)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin30Day)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin30Day)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin60Day)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin60Day)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin60Day)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin60Day)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin90Day)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin90Day)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin90Day)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin90Day)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin30DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin30DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin30DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin30DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin60DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin60DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin60DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin60DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcBrAdmin90DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcCoAdmin90DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL2Admin90DayCourse)) & "', " & _
				"'" & abs(cint(g_bCcL3Admin90DayCourse)) & "', " & _
				"'" & abs(cint(g_bSendCoAdminCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSendBrAdminCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSendL2AdminCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSendL3AdminCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSendLoCourseEmail)) & "', " & _
				"'" & abs(cint(g_bSendCoAdminEventEmail)) & "', " & _
				"'" & abs(cint(g_bSendBrAdminEventEmail)) & "', " & _
				"'" & abs(cint(g_bSendL2AdminEventEmail)) & "', " & _
				"'" & abs(cint(g_bSendL3AdminEventEmail)) & "', " & _
				"'" & abs(cint(g_bSendLoEventEmail)) & "', " & _
				"'" & abs(cint(g_bSendCoAdminFingerprintDeadlineEmail)) & "', " & _
				"'" & abs(cint(g_bSendBrAdminFingerprintDeadlineEmail)) & "', " & _
				"'" & abs(cint(g_bSendL2AdminFingerprintDeadlineEmail)) & "', " & _
				"'" & abs(cint(g_bSendL3AdminFingerprintDeadlineEmail)) & "', " & _
				"'" & abs(cint(g_bSendLoFingerprintDeadlineEmail)) & "', " & _				
				"'" & abs(cint(g_bSendLO30DayEmail)) & "', " & _				
				"'" & abs(cint(g_bSendLO60DayEmail)) & "', " & _				
				"'" & abs(cint(g_bSendLO90DayEmail)) & "', " & _
				"'" & abs(cint(g_bUseRenewalAppDeadlineLicExpEmails)) & "', " & _					
				"'" & g_sEmail30DayText & "', " & _
				"'" & g_sEmail60DayText & "', " & _
				"'" & g_sEmail90DayText & "', " & _
				"'" & g_sCourseEmail30DayText & "', " & _
				"'" & g_sCourseEmail60DayText & "', " & _
				"'" & g_sCourseEmail90DayText & "', " & _
				"'" & g_sCourseEmailText & "' " & _
				")"
			'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected 
			
			'Retrieve the new License
			sSql = "SELECT SettingsId FROM CompanySettings " & _
				"WHERE CompanyId = '" & g_iCompanyId & "'" & _
				"ORDER BY SettingsId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				
				g_iSettingsId = oRs("SettingsId")
				SaveSettings = g_iSettingsId
			
			else
			
				'The record could not be retrieved.
				ReportError("Failed to save new Settings group.")
				exit function
			
			end if 
			
		end if 
		
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			
end class
%>
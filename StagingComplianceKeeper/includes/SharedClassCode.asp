<%
' Standard Class Methods
'
' This file includes a group of common functions and properties used in many 
' classes.  It is intended to be included in class files.
'-----------------------
' Properties
'	- ConnectionString
'	- MaxRecords
'	- VocalErrors
' Private Methods
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- CheckDataParam
'	- QueryDb
'	- ReportError
'	- Safe
'-----------------------


' GLOBAL VARIABLE DECLARATIONS ================================================
dim g_sConnectionString
dim g_iMaxRecords
dim g_bVocalErrors


' PUBLIC PROPERTIES ===========================================================

'Database connection string
public property let ConnectionString(p_sConnectionString)
	g_sConnectionString = p_sConnectionString
end property
public property get ConnectionString()
	ConnectionString = g_sConnectionString
end property

'Used to restrict the recordset 
public property let MaxRecords(p_iMaxRecords)
	g_iMaxRecords = p_iMaxRecords
end property
public property get MaxRecords()
	MaxRecords = g_iMaxRecords
end property
	
'Do we report errors or just return error codes?  Setting this flag affects
'the way the ReportErrors function works.
public property get VocalErrors()
	VocalErrors = g_bVocalErrors()
end property
public property let VocalErrors(p_bVocalErrors)
	if isnumeric(p_bVocalErrors) then
		g_bVocalErrors = cbool(p_bVocalErrors)
	else
		ReportError("Invalid VocalErrors value.  Boolean required.")
	end if
end property


' PRIVATE METHODS =============================================================

' -------------------------------------------------------------------------
' Name:	CheckConnectionString
' Desc:	Checks for a valid database connectionstring
' Inputs: None
' Outputs: None
' Returns: TRUE if present, or FALSE
' Pre-conditions: None
' -------------------------------------------------------------------------
private function CheckConnectionString
	
	if isnull(g_sConnectionString) then
				
		ReportError("ConnectionString property must be set.")		
		CheckConnectionString = false
		
	else
		
		CheckConnectionString = true
		
	end if
	
end function
	
	
' -------------------------------------------------------------------------
' Name: CheckIntParam
' Desc: Verifies that a value actually is, or can be interpreted as, a
'       valid integer
' Preconditions: None
' Inputs: p_iInt - Integer to check
'         p_bAllowNull - Allow blank values?
'         p_sName - Name of the variable for error reporting purposes
' Returns: True if present, false if not
' -------------------------------------------------------------------------
private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
	CheckIntParam = true
		
	'Make sure this is an int
	if isnumeric(p_iInt) then
			
		if p_iInt = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckIntParam = false

		end if 
			
	else
		
		ReportError(p_sName & " must be a valid integer for this " & _
					"operation.")
		CheckIntParam = false
			
	end if
			
end function  
	
	
' -------------------------------------------------------------------------
' Name: CheckStrParam
' Desc: Verifies that a value actually is, or can be interpreted as, a
'       valid string
' Preconditions: None
' Inputs: p_sStr - Integer to check
'         p_bAllowNull - Allow blank values?
'         p_sName - Name of the variable for error reporting purposes
' Returns: True if present, false if not
' -------------------------------------------------------------------------
private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
	CheckStrParam = true
		
	if p_sStr = "" and not p_bAllowNull then
				
		ReportError(p_sName & " must not be blank for this operation.")
		CheckStrParam = false

	end if
		
end function  
	
	
' -------------------------------------------------------------------------
' Name: CheckDateParam
' Desc: Verifies that a value actually is, or can be interpreted as, a
'		valid date.
' Inputs: p_dDate - Date to check
'		  p_bAllowNull - Allow blank or null values?
'		  p_sName - Name of the value for error reporting purposes
' Returns: True if present, false if not
' -------------------------------------------------------------------------
private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
	CheckDateParam = true
		
	if isnull(p_dDate) and not p_bAllowNull then
		ReportError(p_sName & " must not be null for this operation.")
		CheckDateParam = false
	elseif p_dDate = "" and not p_bAllowNull then
		ReportError(p_sName & " must not be blank for this operation.")
		CheckDateParam = false
	elseif not isdate(p_dDate) then
		ReportError(p_sName & " must be a valid date for this operation.")
		CheckDateParam = false
	end if 
		
end function


' -------------------------------------------------------------------------
' Name:					QueryDB
' Purpose:				executes the query in the database
' Required Properties:  ConnectionString
' Optional Properties:	n/a
' Parameters:			sSQL - the query to execute
' Outputs:				a recordset of the executed query is returned
' -------------------------------------------------------------------------
private function QueryDB(sSQL)
	
	set QueryDb = Server.CreateObject("ADODB.Recordset")
		
	'Verify preconditions
	if not CheckConnectionString then
		exit function
	end if
		
		
	Dim Connect	'as connection
	Dim objRS 'as recordset
    
	set Connect = CreateObject("ADODB.Connection")
	set objRS = CreateObject("ADODB.Recordset")
	    
    Connect.Open g_sConnectionString
	objRs.cursorlocation = 3
	objRs.CursorType = 3
	
	if trim(g_iMaxRecords) <> "" then
		objRS.Maxrecords = g_iMaxRecords
	end if
	
    objRS.Open sSQL, connect
    objRs.ActiveConnection = nothing
	
	Set QueryDB = objRS

	set Connect = Nothing	
	set objRS = Nothing
end function


' -------------------------------------------------------------------------
' Name: ReportError
' Description: Depending on the current settings, optionally prints an
'			   error to the current response.
' Input: p_sError - Error text to print
' Output: Prints to response
' Pre-conditions: None.
' -------------------------------------------------------------------------
private sub ReportError(p_sError)
	
	'if the Vocal Errors switch is activated, report errors.
	if g_bVocalErrors then

		Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
	end if
	
end sub	


' -------------------------------------------------------------------------
' Name:	Safe
' Description: Escapes/scrubs characters to make content safe for
'	submission into the database.
' Inputs: p_sText - String to make database-safe.
' Returns: Edited string
' -------------------------------------------------------------------------
private function Safe(p_sText)
	
	if p_sText <> "" then
			
		p_sText = replace(p_sText, "''", "'")
		p_sText = replace(p_sText, "'", "''")
		
		Safe = p_sText
			
	end if
	
end function
%>
<%
'==============================================================================
' Class: State
' Purpose: Class used for retrieving and storing state regulation information.
' Created: 4/20/05
' Author: Mark Silvia
' -------------------
' Properties:
'	- ConnectionString
'	- VocalErrors
'
' State Properties:
'	- StateId
'	- CompanyId
'	- StateName
'	- StateAbbrev
'	- ContactName
'	- ContactPhone
'	- ContactPhoneExt
'	- ContactPhone2
'	- ContactPhone2Ext
'	- ContactFax
'	- ContactEmail
'	- ContactEmail2
'	- ContactAddress
'	- ContactAddress2
'	- ContactCity
'	- ContactStateId
'	- ContactZipcode
'	- ContactZipcodeExt
'	- MailingAddress
'	- MailingAddress2
'	- MailingCity
'	- MailingStateId
'	- MailingZipcode
'	- MailingZipcodeExt
'	- RegulatedBy
'	- CeRequirements
'	- Hours
'	- ExpireDate
'	- Licenses
'	- LicenseRequirements
'	- Fingerprints
'	- Renewals
'	- Website
'	- LastEdit
'
' Methods
'	- LoadStateById
'	- SaveState
'==============================================================================

Class State

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'State properties
	dim g_iStateId
	dim g_iCompanyId
	dim g_sStateName
	dim g_sStateAbbrev
	dim g_sRequirementsText
	dim g_sContactName
	dim g_sContactPhone
	dim g_sContactPhoneExt
	dim g_sContactPhone2
	dim g_sContactPhone2Ext
	dim g_sContactFax
	dim g_sContactEmail
	dim g_sContactEmail2
	dim g_sContactAddress
	dim g_sContactAddress2
	dim g_sContactCity
	dim g_iContactStateId
	dim g_sContactZipcode
	dim g_sContactZipcodeExt
	dim g_sMailingAddress
	dim g_sMailingAddress2
	dim g_sMailingCity
	dim g_iMailingStateId
	dim g_sMailingZipcode
	dim g_sMailingZipcodeExt
	dim g_sRegulatedBy
	dim g_sCeRequirements
	dim g_iHours
	dim g_dExpireDate
	dim g_sLicenses
	dim g_sLicenseRequirements
	dim g_sFingerprints
	dim g_sRenewals
	dim g_sWebsite
	dim g_dLastEdit
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property	
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	
	'Unique ID of the state.  Read only.
	public property get StateId()
		StateId = g_iStateId
	end property
	
	
	'Company Id - the company that created and maintains this particular record
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = p_iCompanyId
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'Full name of the state.
	public property get StateName()
		StateName = g_sStateName
	end property
	public property let StateName(p_sStateName)
		g_sStateName = left(p_sStateName, 50)
	end property
	
	
	'Abbreviation for the state
	public property get StateAbbrev()
		StateAbbrev = g_sStateAbbrev
	end property
	public property let StateAbbrev(p_sStateAbbrev)
		g_sStateAbbrev = left(p_sStateAbbrev, 2)
	end property
	
	
	'Text holding a long description of the state licensing regulations and
	'requirements.  This is typically going to be pasted from a Word file and
	'will take the place of most of the other state attributes.  I'm leaving the
	'others in just in case the client changes its mind.
	public property get RequirementsText()
		RequirementsText = g_sRequirementsText
	end property
	public property let RequirementsText(p_sRequirementsText)
		g_sRequirementsText = p_sRequirementsText
	end property	
	
	
	'Contact full name
	public property get ContactName()
		ContactName = g_sContactName
	end property
	public property let ContactName(p_sContactName)
		g_sContactName = left(p_sContactName, 2)
	end property
	
	
	'Contact Phone number
	public property get ContactPhone()
		ContactPhone = g_sContactPhone
	end property
	public property let ContactPhone(p_sContactPhone)
		g_sContactPhone = left(p_sContactPhone, 20)
	end property
	
	
	'Contact Phone number extension
	public property get ContactPhoneExt()
		ContactPhoneExt = g_sContactPhoneExt
	end property
	public property let ContactPhoneExt(p_sContactPhoneExt)
		g_sContactPhoneExt = left(p_sContactPhoneExt, 10)
	end property


	'Contact Phone secondary number
	public property get ContactPhone2()
		ContactPhone2 = g_sContactPhone2
	end property
	public property let ContactPhone2(p_sContactPhone2)
		g_sContactPhone2 = left(p_sContactPhone2, 20)
	end property


	'Contact Phone secondary number extension
	public property get ContactPhone2Ext()
		ContactPhone2Ext = g_sContactPhone2Ext
	end property
	public property let ContactPhone2Ext(p_sContactPhone2Ext)
		g_sContactPhone2Ext = left(p_sContactPhone2Ext, 20)
	end property


	'Contact fax number
	public property get ContactFax()
		ContactFax = g_sContactFax
	end property
	public property let ContactFax(p_sContactFax)
		g_sContactFax = left(p_sContactFax, 20)
	end property


	'Contact email
	public property get ContactEmail()
		ContactEmail = g_sContactEmail
	end property
	public property let ContactEmail(p_sContactEmail)
		g_sContactEmail = left(p_sContactEmail, 50)
	end property

	
	'Contact secondary email
	public property get ContactEmail2()
		ContactEmail2 = g_sContactEmail2
	end property
	public property let ContactEmail2(p_sContactEmail2)
		g_sContactEmail2 = left(p_sContactEmail2, 50)
	end property


	'Contact address
	public property get ContactAddress()
		ContactAddress = g_sContactAddress
	end property
	public property let ContactAddress(p_sContactAddress)
		g_sContactAddress = left(p_sContactAddress, 50)
	end property


	'Contact address second line
	public property get ContactAddress2()
		ContactAddress2 = g_sContactAddress2
	end property
	public property let ContactAddress2(p_sContactAddress2)
		g_sContactAddress2 = left(p_sContactAddress2, 50)
	end property


	'Contact address city
	public property get ContactCity()
		ContactCity = g_sContactCity
	end property
	public property let ContactCity(p_sContactCity)
		g_sContactCity = left(p_sContactCity, 50)
	end property


	'Contact state ID
	public property get ContactStateId()
		ContactStateId = g_iContactStateId
	end property
	public property let ContactStateId(p_iContactStateId)
		if isnumeric(p_iContactStateId) then
			g_iContactStateId = cint(p_iContactStateId)
		elseif p_iContactStateId = "" then
			g_iContactStateId = ""
		else
			ReportError("Invalid ContactStateId value.  Integer required.")
		end if
	end property


	'Contact address zipcode
	public property get ContactZipcode()
		ContactZipcode = g_sContactZipcode
	end property
	public property let ContactZipcode(p_sContactZipcode)
		g_sContactZipcode = left(p_sContactZipcode, 5)
	end property

	
	'Contact address zipcode extension
	public property get ContactZipcodeExt()
		ContactZipcodeExt = g_sContactZipcodeExt
	end property
	public property let ContactZipcodeExt(p_sContactZipcodeExt)
		g_sContactZipcodeExt = left(p_sContactZipcodeExt, 4)
	end property


	'Mailing address
	public property get MailingAddress()
		MailingAddress = g_sMailingAddress
	end property
	public property let MailingAddress(p_sMailingAddress)
		g_sMailingAddress = left(p_sMailingAddress, 50)
	end property


	'Mailing address second line
	public property get MailingAddress2()
		MailingAddress2 = g_sMailingAddress2
	end property
	public property let MailingAddress2(p_sMailingAddress2)
		g_sMailingAddress2 = left(p_sMailingAddress2, 50)
	end property


	'Mailing address city
	public property get MailingCity()
		MailingCity = g_sMailingCity
	end property
	public property let MailingCity(p_sMailingCity)
		g_sMailingCity = left(p_sMailingCity, 50)
	end property


	'Mailing state ID
	public property get MailingStateId()
		MailingStateId = g_iMailingStateId
	end property
	public property let MailingStateId(p_iMailingStateId)
		if isnumeric(p_iMailingStateId) then
			g_iMailingStateId = cint(p_iMailingStateId)
		elseif p_iMailingStateId = "" then
			g_iMailingStateId = ""
		else
			ReportError("Invalid MailingStateId value.  Integer required.")
		end if
	end property


	'Mailing address zipcode
	public property get MailingZipcode()
		MailingZipcode = g_sMailingZipcode
	end property
	public property let MailingZipcode(p_sMailingZipcode)
		g_sMailingZipcode = left(p_sMailingZipcode, 5)
	end property

	
	'Mailing address zipcode extension
	public property get MailingZipcodeExt()
		MailingZipcodeExt = g_sMailingZipcodeExt
	end property
	public property let MailingZipcodeExt(p_sMailingZipcodeExt)
		g_sMailingZipcodeExt = left(p_sMailingZipcodeExt, 4)
	end property
	
	
	'State regulating agency
	public property get RegulatedBy()
		RegulatedBy = g_sRegulatedBy
	end property
	public property let RegulatedBy(p_sRegulatedBy)
		g_sRegulatedBy = p_sRegulatedBy
	end property
	
	
	'Continuing education requirements
	public property get CeRequirements()
		CeRequirements = g_sCeRequirements
	end property
	public property let CeRequirements(p_sCeRequirements)
		g_sCeRequirements = p_sCeRequirements
	end property
	
	
	'Hours required
	public property get Hours()
		Hours = g_iHours
	end property
	public property let Hours(p_iHours)
		if isnumeric(p_iHours) then
			g_iHours = cint(p_iHours)
		elseif p_iHours = "" then
			g_iHours = ""
		else
			ReportError("Invalid Hours value.  Integer required.")
		end if
	end property
	
	
	'Expire Date
	public property get ExpireDate()
		ExpireDate = g_dExpireDate
	end property
	public property let ExpireDate(p_dExpireDate)
		if isdate(p_dExpireDate) then
			g_dExpireDate = cdate(p_dExpireDate)
		elseif p_dExpireDate = "" then
			g_dExpireDate = ""
		else
			ReportError("Invalid ExpireDate value.  Date required.")
		end if
	end property
	
	
	'Description of licenses granted by the state
	public property get Licenses()
		Licenses = g_sLicenses
	end property
	public property let Licenses(p_sLicenses)
		g_sLicenses = p_sLicenses
	end property
	
	
	'Description of requirements for licensing
	public property get LicenseRequirements()
		LicenseRequirements = g_sLicenseRequirements
	end property
	public property let LicenseRequirements(p_sLicenseRequirements)
		g_sLicenseRequirements = p_sLicenseRequirements
	end property
	
	
	'Does the state require fingerprints?
	public property get Fingerprints()
		Fingerprints = g_sFingerprints
	end property
	public property let Fingerprints(p_sFingerprints)
		g_sFingerprints = left(p_sFingerprints, 50)
	end property
	
	
	'Description of the renewal deadlines/process
	public property get Renewals()
		Renewals = g_sRenewals
	end property
	public property let Renewals(p_sRenewals)
		g_sRenewals = p_sRenewals
	end property
	
	
	'Website address for state information
	public property get Website()
		Website = g_sWebsite
	end property
	public property let Website(p_sWebsite)
		g_sWebsite = left(p_sWebsite, 50)
	end property
	
	
	'Last edit - date/time of the last time that an admin updated this entry
	public property get LastEdit()
		LastEdit = g_dLastEdit
	end property
	public property let LastEdit(p_dLastEdit)
		if isdate(p_dLastEdit) then
			g_dLastEdit = cdate(p_dLastEdit)
		elseif p_dLastEdit = "" then
			g_dLastEdit = ""
		else
			ReportError("Invalid LastEdit value.  Date required.")
		end if
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
			
		elseif g_sConnectionString = "" then

			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	



	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name:				LoadStateById
	' Description:		This method attempts to load the properties of a stored
	'					State based on the passed ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_iStateId - the ID to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadStateById(p_iStateId)
	
		LoadStateById = 0
	
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function		
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")


		sSql = "SELECT * FROM StateRequirements " & _
			"WHERE StateId = '" & g_iStateId & "' "
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so load the object properties.
			g_iStateId = oRs("StateId")
			g_iCompanyId = oRs("CompanyId")
			g_sStateName = oRs("StateName")
			g_sStateAbbrev = oRs("StateAbbrev")
			g_sRequirementsText = oRs("RequirementsText")
			g_sContactName = oRs("ContactName")
			g_sContactPhone = oRs("ContactPhone")
			g_sContactPhoneExt = oRs("ContactPhoneExt")
			g_sContactPhone2 = oRs("ContactPhone2")
			g_sContactPhone2Ext = oRs("ContactPhone2Ext")
			g_sContactFax = oRs("ContactFax")
			g_sContactEmail = oRs("ContactEmail")
			g_sContactEmail2 = oRs("ContactEmail2")
			g_sContactAddress = oRs("ContactAddress")
			g_sContactAddress2 = oRs("ContactAddress2")
			g_sContactCity = oRs("ContactCity")
			g_iContactStateId = oRs("ContactStateId")
			g_sContactZipcode = oRs("ContactZipcode")
			g_sContactZipcodeExt = oRs("ContactZipcodeExt")
			g_sMailingAddress = oRs("MailingAddress")
			g_sMailingAddress2 = oRs("MailingAddress2")
			g_sMailingCity = oRs("MailingCity")
			g_iMailingStateId = oRs("MailingStateId")
			g_sMailingZipcode = oRs("MailingZipcode")
			g_sMailingZipcodeExt = oRs("MailingZipcodeExt")
			g_sRegulatedBy = oRs("RegulatedBy")
			g_sCeRequirements = oRs("CeRequirements")
			g_iHours = oRs("Hours")
			g_dExpireDate = oRs("ExpireDate")
			g_sLicenses = oRs("Licenses")
			g_sLicenseRequirements = oRs("LicenseRequirements")
			g_sFingerprints = oRs("Fingerprints")
			g_sRenewals = oRs("Renewals")
			g_sWebsite = oRs("Website")
			g_dLastEdit = oRs("LastEdit")
			
			LoadStateById = g_iStateId
			
		else
		
			'We didn't get a recordset.  Load failed.
			ReportError("Failed to load the passed State.")
			exit function
			
		end if 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LoadStateByAbbrev
	' Description:		This method attempts to load the properties of a stored
	'					State based on the passed abbreviation and company ID.
	' Pre-conditions:	ConnectionString must be set.
	' Inputs:			p_sStateAbbrev - the abbreviation of the state to load.
	' Outputs:			None.
	' Returns:			If successful, returns the loaded ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadStateByAbbrev(p_sStateAbbrev)
	
		LoadStateByAbbrev = 0
	
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sStateAbbrev, 0, "State Abbreviation") then
			exit function		
		end if
		
		
		dim oRs 'ADODB Recordset
		dim sSQL 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.RecordSet")


		sSql = "SELECT * FROM StateRequirements " & _
			"WHERE StateAbbrev LIKE '%" & p_sStateAbbrev & "%' "
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a recordset back, so load the object properties.
			g_iStateId = oRs("StateId")
			g_iCompanyId = oRs("CompanyId")
			g_sStateName = oRs("StateName")
			g_sStateAbbrev = oRs("StateAbbrev")
			g_sRequirementsText = oRs("RequirementsText")
			g_sContactName = oRs("ContactName")
			g_sContactPhone = oRs("ContactPhone")
			g_sContactPhoneExt = oRs("ContactPhoneExt")
			g_sContactPhone2 = oRs("ContactPhone2")
			g_sContactPhone2Ext = oRs("ContactPhone2Ext")
			g_sContactFax = oRs("ContactFax")
			g_sContactEmail = oRs("ContactEmail")
			g_sContactEmail2 = oRs("ContactEmail2")
			g_sContactAddress = oRs("ContactAddress")
			g_sContactAddress2 = oRs("ContactAddress2")
			g_sContactCity = oRs("ContactCity")
			g_iContactStateId = oRs("ContactStateId")
			g_sContactZipcode = oRs("ContactZipcode")
			g_sContactZipcodeExt = oRs("ContactZipcodeExt")
			g_sMailingAddress = oRs("MailingAddress")
			g_sMailingAddress2 = oRs("MailingAddress2")
			g_sMailingCity = oRs("MailingCity")
			g_iMailingStateId = oRs("MailingStateId")
			g_sMailingZipcode = oRs("MailingZipcode")
			g_sMailingZipcodeExt = oRs("MailingZipcodeExt")
			g_sRegulatedBy = oRs("RegulatedBy")
			g_sCeRequirements = oRs("CeRequirements")
			g_iHours = oRs("Hours")
			g_dExpireDate = oRs("ExpireDate")
			g_sLicenses = oRs("Licenses")
			g_sLicenseRequirements = oRs("LicenseRequirements")
			g_sFingerprints = oRs("Fingerprints")
			g_sRenewals = oRs("Renewals")
			g_sWebsite = oRs("Website")
			g_dLastEdit = oRs("LastEdit")
						
			LoadStateByAbbrev = g_iStateId
			
		else
		
			'We didn't get a recordset.  Load failed.
			ReportError("Failed to load the passed State.")
			exit function
			
		end if 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				SaveState
	' Description:		Save the State object in the database, and assign it
	'					the resulting State ID.
	' Pre-conditions:	ConnectionString, StateId, StateName, StateAbbrev
	' Inputs:			None. 
	' Outputs:			None. 
	' Returns:			If successful, returns the ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveState()
	
		SaveState = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckStrParam(g_sStateName, 0, "State Name") then
			exit function				
		elseif not CheckStrParam(g_sStateAbbrev, 0, "State Abbrev") then
			exit function		
		end if


		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSQL 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute

		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open

		sSql = "UPDATE StateRequirements SET " & _
			"StateName = '" & g_sStateName & "', " & _
			"StateAbbrev = '" & g_sStateAbbrev & "', " & _
			"RequirementsText = '" & g_sRequirementsText & "', " & _
			"ContactName = '" & g_sContactName & "', " & _
			"ContactPhone = '" & g_sContactPhone & "', " & _
			"ContactPhoneExt = '" & g_sContactPhoneExt & "', " & _
			"ContactPhone2 = '" & g_sContactPhone2 & "', " & _
			"ContactPhone2Ext = '" & g_sContactPhone2Ext & "', " & _
			"ContactFax = '" & g_sContactFax & "', " & _
			"ContactEmail = '" & g_sContactEmail & "', " & _
			"ContactEmail2 = '" & g_sContactEmail2 & "', " & _
			"ContactAddress = '" & g_sContactAddress & "', " & _
			"ContactAddress2 = '" & g_sContactAddress2 & "', " & _
			"ContactCity = '" & g_sContactCity & "', " & _
			"ContactStateId = '" & g_iContactStateId & "', " & _
			"ContactZipcode = '" & g_sContactZipcode & "', " & _
			"ContactZipcodeExt = '" & g_sContactZipcodeExt & "', " & _
			"MailingAddress = '" & g_sMailingAddress & "', " & _
			"MailingAddress2 = '" & g_sMailingAddress2 & "', " & _
			"MailingCity = '" & g_sMailingCity & "', " & _
			"MailingStateId = '" & g_iMailingStateId & "', " & _
			"MailingZipcode = '" & g_sMailingZipcode & "', " & _
			"MailingZipcodeExt = '" & g_sMailingZipcodeExt & "', " & _
			"RegulatedBy = '" & g_sRegulatedBy & "', " & _
			"CeRequirements = '" & g_sCeRequirements & "', " & _
			"Hours = '" & g_iHours & "', "
			
		if g_dExpireDate <> "" then 
			sSql = sSql & "ExpireDate = '" & g_dExpireDate & "', "
		end if
		
		if g_dLastEdit <> "" then
			sSql = sSql & "LastEdit = '" & g_dLastEdit & "', "
		end if
		
		sSql = sSql & "Licenses = '" & g_sLicenses & "', " & _
			"LicenseRequirements = '" & g_sLicenseRequirements & "', " & _
			"Fingerprints = '" & g_sFingerprints & "', " & _
			"Renewals = '" & g_sRenewals & "', " & _
			"Website = '" & g_sWebsite & "' " & _		
			"WHERE StateId = '" & g_iStateId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected = 0 then
		
			ReportError("Unable to update the state record.")
			exit function
		
		'
		'	'We need to create a new record for this company's data for 
		'	'this state
		'	sSql = "INSERT INTO StateRequirements (" & _
		'		"CompanyId, " & _
		'		"StateName, " & _
		'		"StateAbbrev, " & _
		'		"ContactName, " & _
		'		"ContactPhone, " & _
		'		"ContactPhoneExt, " & _
		'		"ContactPhone2, " & _
		'		"ContactPhone2Ext, " & _
		'		"ContactFax, " & _
		'		"ContactEmail, " & _
		'		"ContactEmail2, " & _
		'		"ContactAddress, " & _
		'		"ContactAddress2, " & _
		'		"ContactCity, " & _
		'		"ContactStateId, " & _
		'		"ContactZipcode, " & _
		'		"ContactZipcodeExt, " & _
		'		"MailingAddress, " & _
		'		"MailingAddress2, " & _
		'		"MailingCity, " & _
		'		"MailingStateId, " & _
		'		"MailingZipcode, " & _
		'		"MailingZipcodeExt, " & _
		'		"RegulatedBy, " & _
		'		"CeRequirements, " & _
		'		"Hours, " & _
		'		"ExpireDate " & _
		'		") VALUES (" & _
		'		"'" & g_iCompanyId & "', " & _
		'		"'" & g_sStateName & "', " & _
		'		"'" & g_sStateAbbrev & "', " & _
		'		"'" & g_sContactName & "', " & _
		'		"'" & g_sContactPhone & "', " & _
		'		"'" & g_sContactPhoneExt & "', " & _
		'		"'" & g_sContactPhone2 & "', " & _
		'		"'" & g_sContactPhone2Ext & "', " & _
		'		"'" & g_sContactFax & "', " & _
		'		"'" & g_sContactEmail & "', " & _
		'		"'" & g_sContactEmail2 & "', " & _
		'		"'" & g_sContactAddress & "', " & _
		'		"'" & g_sContactAddress2 & "', " & _
		'		"'" & g_sContactCity & "', " & _
		'		"'" & g_iContactStateId & "', " & _
		'		"'" & g_sContactZipcode & "', " & _
		'		"'" & g_sContactZipcodeExt & "', " & _
		'		"'" & g_sMailingAddress & "', " & _
		'		"'" & g_sMailingAddress2 & "', " & _
		'		"'" & g_sMailingCity & "', " & _
		'		"'" & g_iMailingStateId & "', " & _
		'		"'" & g_sMailingZipcode & "', " & _
		'		"'" & g_sMailingZipcodeExt & "', " & _
		'		"'" & g_sRegulatedBy & "', " & _
		'		"'" & g_sCeRequirements & "', " & _
		'		"'" & g_iHours & "', " & _
		'		"'" & g_dExpireDate & "' " & _
		'		")"
		'	oConn.Execute sSql, lRecordsAffected
		'	
		'	if lRecordsAffected = 0 then
		'	
		'		ReportError("Failed to correctly save State Requirements.")
		'		exit function
		'		
		'	else
		'		
		'		SaveState = 1
		'		
		'	end if 
		'				
		else
			
			SaveState = g_iStateId
			
		end if
		
	end function
	
end class
%>
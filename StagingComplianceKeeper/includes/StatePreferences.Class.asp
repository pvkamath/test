<%
'==============================================================================
' Class: State
' Purpose: Class used for managing the addition and modification of state
'	configurations.
' Create Date: 12/02/2004
' Current Version: 1.0
' ----------------------
' Properties:
'	- ConnectionString
'   - VocalErrors
'	- PreferenceId
'   - StateId
'	- LogoFileName
'	- LicensingTypeId

' Private Methods:
'	- CheckConnectionString()
'	- CheckIntParam(p_iInt, p_bAllowNull, p_sName)
'	- ReportError(p_sError)
'	- QueryDB(sSQL)

' Public Methods:
'	- LoadPreferencesByStateId(p_iStateId)
'	- SavePreferences
'	- LookupLicensingType
'==============================================================================
Class StatePreference


	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	dim g_sConnectionString 'as string
	dim g_bVocalErrors 'Flag to control error reporting method

	dim g_iPreferenceId 'Unique DB identifier
	dim g_iStateId 'Unique State DB identifier
	dim g_sLogoFileName 'Path to custom logo
	dim g_bShowCalendar 'Show the calendar on the frontend
	dim g_iLicensingTypeId 'LO/Branch/Corporate licensing
	dim g_bLicensingCompanies 'Are we licensing companies?
	dim g_bLicensingBranches 'Are we licensing branches?
	dim g_bLicensingOfficers 'Are we licensing officers?
	
	

	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
		'Nothing
	
	end sub
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
		'Nothing
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	Private Function QueryDB(sSQL)
		'heckParam(sSQL)
		CheckConnectionString	

		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Verbose Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name:				LoadPreferencesByStateId(p_iStateId)
	' Description:		Load the state profile using the specified ID
	' Preconditions:	ConnectionString
	' Inputs:			p_iStateId - Unique ID for the State
	' Outputs:			None
	' Returns:			If successful, returns State ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadPreferencesByStateId(p_iStateId)
	
		LoadPreferencesByStateId = 0
	
		'Verify preconditions
		if not CheckConnectionString then
			LoadStateById = 0
			exit function
		end if
		
		dim sSql  'SQL String
		dim oRs  'Recordset object
		
		sSql = "SELECT * FROM StatePreferences " & _
			   "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.EOF and oRs.BOF) then
			
			'We got a record back.  Assign the properties.
			g_iPreferenceId = oRs("PreferenceId")
			g_iStateId = oRs("StateId")
			g_sLogoFileName = oRs("LogoFileName")
			g_bShowCalendar = oRs("ShowCalendar")
			g_iLicensingTypeId = oRs("LicensingTypeId")
			
			'Return the ID
			LoadPreferencesByStateId = oRs("StateId")
		
		else
		
			ReportError("Cannot load passed ID: " & p_iStateId & ".")
			LoadStateById = 0
		
		end if 
		
		set oRs = nothing
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				SavePreferences
	' Description:		Save the state information to the database
	' Pre-conditions:	ConnectionString, StateID
	' Inputs:			None
	' Outputs:			None
	' Returns:			If successful, returns Preferences ID.  Otherwise zero.
	' -------------------------------------------------------------------------
	public function SavePreferences()
	
		SavePreferences = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			SavePreferences = 0
			exit function
		elseif not CheckIntParam(g_iStateId, 1, "State ID") then
			SavePreferences = 0
			exit function		
		end if
		
			
		dim oConn 'ADODB connection object
		dim sSql 'SQL string
		dim oRs 'ADODB recordset object
		dim lRecordsAffected 'Number of records affected by a DB command
		
		set oConn = server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'Update if we have a preference ID 
		if g_iPreferenceId <> "" then
		
			sSql = "UPDATE StatePreferences SET " & _
				   "StateId = '" & g_iStateId & "', " & _
				   "LogoFileName = '" & g_sLogoFileName & "', " & _
				   "ShowCalendar = '" & abs(cint(g_bShowCalendar)) & "', " & _
				   "LicensingTypeId = '" & g_iLicensingTypeId & "' " & _
				   "WHERE PreferenceId = '" & g_iPreferenceId & "'"
			oConn.Execute sSql, lRecordsAffected
		
			if lRecordsAffected < 1 then 
		
				'This should have affected a record.  Report the error and 
				'quit.
				ReportError("No records affected on save.")
				SavePreferences = 0
				exit function
		
			end if 
			
		'Otherwise do an insert
		else
		
			sSql = "INSERT INTO StatePreferences (" & _
				   "StateId, " & _
				   "LogoFileName, " & _
				   "ShowCalendar, " & _
				   "LicensingTypeID " & _
				   ") VALUES (" & _
				   "'" & g_iStateId & "', " & _
				   "'" & g_sLogoFileName & "', " & _
				   "'" & abs(cint(g_bShowCalendar)) & "', " & _
				   "'" & g_iLicensingTypeId & "' " & _
				   ")"
			oConn.Execute(sSql)

			sSql = "SELECT * FROM StatePreferences " & _
				   "WHERE StateId = '" & g_iStateId & "' " & _
				   "ORDER BY PreferenceId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				'No record.  
				ReportError("Unable to insert to database.")
				SavePreferences = 0
				exit function
			
			else
			
				g_iPreferenceId = oRs("PreferenceId")
			
			end if 
					
			
		end if 


		oConn.Close
		set oConn = nothing
		
		SavePreferences = g_iPreferenceId
		
	end function



	' PROPERTIES ==============================================================
	
	'Get/set connection string.
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid value.  Boolean required.")
		end if 
	end property
	
	
	'Get StateId.
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		g_iStateId = p_iStateId
	end property
	
	
	'Get/set logo file name.
	public property get LogoFileName()
		LogoFileName = g_sLogoFileName
	end property
	public property let LogoFileName(p_sLogoFileName)
		g_sLogoFileName = p_sLogoFileName
	end property 
	
	
	'Show calendar boolean
	public property get ShowCalendar()
		ShowCalendar = g_bShowCalendar
	end property
	public property let ShowCalendar(p_bShowCalendar)
		g_bShowCalendar = p_bShowCalendar
	end property
	
	
	'Licensing Type - Are loan officers, branches, or companies licensed?
	public property get LicensingTypeId()
		LicensingTypeId = g_iLicensingTypeId
	end property
	public property let LicensingTypeId(p_iLicensingTypeId)
		if isnumeric(p_iLicensingTypeId) then
			g_iLicensingTypeId = abs(cint(p_iLicensingTypeId))
		else
			ReportError("Invalid LicensingTypeId value.  Integer required.")
		end if 
	end property
	
	
	'Are we licensing Companies?  This is a read-only value which depends on
	'the current licensing type.
	public property get LicensingCompanies
		if g_iLicensingTypeId = 3 or _
			g_iLicensingTypeId = 5 or _
			g_iLicensingTypeId = 6 or _
			g_iLicensingTypeId = 7 then
			
			LicensingCompanies = true
		
		else
		
			LicensingCompanies = false
			
		end if
		
	end property
	
	
	'Are we licensing Branches?  This is a read-only value which depends on
	'the current licensing type.
	public property get LicensingBranches
		if g_iLicensingTypeId = 2 or _
			g_iLicensingTypeId = 4 or _
			g_iLicensingTypeId = 6 or _
			g_iLicensingTypeId = 7 then
			
			LicensingBranches = true
		
		else
		
			LicensingBranches = false
			
		end if
		
	end property
	
	
	'Are we licensing Officers?  This is a read-only value which depends on
	'the current licensing type.
	public property get LicensingOfficers
		if g_iLicensingTypeId = 1 or _
			g_iLicensingTypeId = 4 or _
			g_iLicensingTypeId = 5 or _
			g_iLicensingTypeId = 7 then
			
			LicensingOfficers = true
			
		else
			
			LicensingOfficers = false
			
		end if 
		
	end property
		
	
	'Get Preference ID.  Read Only.
	public property get PreferenceId()
		PreferenceId = g_iPreferenceId
	end property


end class
%>

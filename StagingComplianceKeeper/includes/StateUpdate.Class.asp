<%
'==============================================================================
' Class: StateUpdate
' Controls the creation, modification, and removal of state record update 
'	notices.  This allows the superadmin to post descriptions of when a state
'	requirements profile has been updated, and what has changed.
' Created: 4/10/06
' Authors: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- UpdateId
'	- StateId
'	- UpdateDate
'	- Text
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- CheckDateParam
'	- QueryDb
'	- ReportError
' Public Methods
'	- LoadUpdateById
'	- SaveUpdates
'	- SearchUpdates
'==============================================================================

class StateUpdate

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Settings properties
	dim g_iUpdateId
	dim g_iStateId
	dim g_dUpdateDate
	dim g_sText
	dim g_bDeleted
	dim g_dSearchDateStart
	dim g_dSearchDateEnd
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property


	'Unique ID for this saved Update.  Read only.
	public property get UpdateId()
		UpdateId = g_iUpdateId
	end property
	
	
	'State ID
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then	
			g_iStateId = clng(p_iStateId)
		elseif isnull(p_iStateId) or p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Date the update was posted
	public property get UpdateDate()
		UpdateDate = g_dUpdateDate
	end property
	public property let UpdateDate(p_dUpdateDate)
		if isdate(p_dUpdateDate) then
			g_dUpdateDate = cdate(p_dUpdateDate)
		elseif isnull(p_dUpdateDate) or p_dUpdateDate = "" then
			g_dUpdateDate = ""
		else
			ReportError("Invalid UpdateDate value.  Date required.")
		end if
	end property	
	
	
	'Update description text
	public property get Text()
		Text = g_sText 
	end property
	public property let Text(p_sText)
		g_sText = p_sText
	end property
	
	
	'Has this deadline been deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif isnull(p_bDeleted) or p_bDeleted = "" then
			g_bDeleted = "" 
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property


	'Search start date
	public property get SearchDateStart()
		SearchDateStart = g_dSearchDateStart
	end property
	public property let SearchDateStart(p_dSearchDateStart)
		if isdate(p_dSearchDateStart) then
			g_dSearchDateStart = cdate(p_dSearchDateStart)
		elseif isnull(p_dSearchDateStart) or p_dSearchDateStart = "" then
			g_dSearchDateStart = ""
		else
			ReportError("Invalid SearchDateStart value.  Date required.")
		end if
	end property	


	'Search end date
	public property get SearchDateEnd()
		SearchDateEnd = g_dSearchDateEnd
	end property
	public property let SearchDateEnd(p_dSearchDateEnd)
		if isdate(p_dSearchDateEnd) then
			g_dSearchDateEnd = cdate(p_dSearchDateEnd)
		elseif isnull(p_dSearchDateEnd) or p_dSearchDateEnd = "" then
			g_dSearchDateEnd = ""
		else
			ReportError("Invalid SearchDateEnd value.  Date required.")
		end if
	end property	
	
	
	
	' PRIVATE METHODS =========================================================

	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	



	' PUBLIC METHODS ==========================================================

	' -------------------------------------------------------------------------
	' Name: LoadUpdateById
	' Desc: Load an update based on the passed UpdateId
	' Preconditions: ConnectionString
	' Inputs: p_iUpdateId - ID to load
	' Returns: Loaded ID if successful, 0 if not
	' -------------------------------------------------------------------------
	public function LoadUpdateById(p_iUpdateId)
	
		LoadUpdateById = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iUpdateId, 0, "Update ID") then
			exit function
		end if
		
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM StateUpdates " & _
			"WHERE UpdateId = '" & p_iUpdateId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the properties
			g_iUpdateId = oRs("UpdateId")
			g_iStateId = oRs("StateId")
			g_dUpdateDate = oRs("UpdateDate")
			g_sText = oRs("Text")
			g_bDeleted = oRs("Deleted")
			
			LoadUpdateById = g_iUpdateId
			
		else
			
			ReportError("Unable to load the passed Update ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveUpdate
	' Desc: Saves the object's properties to the database
	' Preconditions: ConnectionString, StateId, UpdateDate
	' Returns: If successful, returns the new/saved ID, otherwise 0
	' -------------------------------------------------------------------------
	public function SaveUpdate()
		
		SaveUpdate = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iStateId, 0, "State ID") or _
			not CheckDateParam(g_dUpdateDate, 0, "Update Date") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.Recordset")
	
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have an ID assigned, we'll UPDATE, otherwise we want to INSERT
		if g_iUpdateId <> "" then
		
			sSql = "UPDATE StateUpdates SET " & _
				"StateId = '" & g_iStateId & "', " & _
				"UpdateDate = '" & g_dUpdateDate & "', " & _
				"Text = '" & g_sText & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE UpdateId = '" & g_iUpdateId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected = 1 then
				SaveUpdate = g_iUpdateId
			else
				ReportError("Unable to save existing Update.")
				exit function
			end if
			
		else
		
			sSql = "INSERT INTO StateUpdates (" & _
				"StateId, " & _
				"UpdateDate, " & _
				"Text, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_iStateId & "', " & _
				"'" & g_dUpdateDate & "', " & _
				"'" & g_sText & "', " & _
				"'" & abs(cint(g_bDeleted)) & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new update
			sSql = "SELECT UpdateId FROM StateUpdates " & _
				"WHERE StateId = '" & g_iStateId & "' " & _
				"AND UpdateDate = '" & g_dUpdateDate & "' " & _
				"ORDER BY UpdateId DESC "
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				g_iUpdateId = oRs("UpdateId")
				SaveUpdate = g_iUpdateId
			else
				'Record could not be retrieved
				ReportError("Failed to save the new Update.")
				exit function
			end if
			
		end if
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchUpdates
	' Desc: Retrieves a collection of Update IDs that match the properties of
	'	the current object instance.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchUpdates()
		
		set SearchUpdates = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL statement
		dim oRs 'Recordset object
			
		sSql = "SELECT DISTINCT SUs.UpdateId, SUs.UpdateDate, " & _
			"SUs.StateId " & _
			"FROM StateUpdates AS SUs " & _
			"WHERE (NOT SUs.Deleted = '1') " 
			
		'If StateId search
		if g_iStateId <> "" then
			sSql = sSql & "AND SUs.StateId = '" & g_iStateId & "' "
		end if
			
		'If SearchDateStart search
		if g_dSearchDateStart <> "" then
			sSql = sSql & "AND SUs.UpdateDate >= '" & g_dSearchDateStart & "' "
		end if
		
		'If SearchDateEnd search
		if g_dSearchDateEnd <> "" then
			sSql = sSql & "AND SUs.UpdateDate <= '" & g_dSearchDateEnd & "' "
		end if
		
		sSql = sSql & "ORDER BY SUs.UpdateDate DESC"
		
		set SearchUpdates = QueryDb(sSql)
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		elseif not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupState = oRs("State")
		end if 
		
		set oRs = nothing
		
	end function
	
end class
%>
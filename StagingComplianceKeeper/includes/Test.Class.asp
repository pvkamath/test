<%
'===============================================================================================================
' Class: Test
' Purpose: Class used for retrieving and storing Associate Tests
' Create Date: 7/1/2010
' ----------------------
' Properties:
'	- ConnectionString
'	- Name
'	- SearchBranchIDList
'	- SearchCompanyID
'	- SearchCompanyL2IDList
'	- SearchCompanyL3IDList
'   - SearchName
'   - SearchStateIDList
'   - SearchStatusIDs
'   - SearchTestDateFrom
'   - SearchTestDateTo
'   - StateID
'   - StatusID
'   - TestDate
'   - TestTypeID
'	- TestID
'	- UserID
' Methods:
'	- CheckConnectionString()
'	- CheckParam(p_sParam)
'	- QueryDB(sSQL)
'
'	- DeleteTest()
'	- GetStatuses()
'	- GetTests()
'	- LoadTest()
'	- SaveTest()
'	- SearchTests()
'
'===============================================================================================================
Class Test
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString 'as string
	dim g_sName 'as string
	dim g_iStateID 'as integer
	dim g_iStatusID 'as integer	
	dim g_sSearchBranchIDList 'as string
	dim g_iSearchCompanyID 'as integer
	dim g_sSearchCompanyL2IDList 'as string
	dim g_sSearchCompanyL3IDList 'as string
	dim g_sSearchName 'as string
	dim g_sSearchStateIDList 'as string	
	dim g_sSearchStatusIDs 'as stromg
	dim g_sSearchTestDateFrom 'as string
	dim g_sSearchTestDateTo 'as string
    dim g_sSearchTestTypeID 'as string
	dim g_sTestDate 'as string
    dim g_iTestTypeID 'as integer
	dim g_iTestID 'as integer		
	dim g_iUserID 'as integer
	
	' PRIVATE METHODS ===========================================================================================
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:  CheckConnectionString
	' Purpose:	Check to make sure the connectionstring has been set
	' Required Properties:  n/a
	' Optional Properties:	n/a
	' Parameters:  sSQL - the query to execute
	' Outputs:  recordset - a recordset of the executed query is returned
	' --------------------------------------------------------------------------------------------------------------------------------	
	Private Sub CheckConnectionString()
		if isNull(g_sConnectionString) then
			response.write "You must specify the value of the ConnectionString Property value before proceeding."
		end if
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:  CheckParam
	' Purpose:  Checks to make sure the passed in value is not null
	' Required Properties:  n/a
	' Optional Properties:	n/a
	' Parameters:  p_sParam - the required parameter
	' Outputs:  n/a
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Sub CheckParam(p_sParam)
		if ((isNull(p_sParam)) or (trim(p_sParam) = "")) then
			response.write "Processing Error!!!<br>You must specify the value before proceeding."
			response.end
		end if
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:  QueryDB
	' Purpose:  executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:  sSQL - the query to execute
	' Outputs:  recordset - a recordset of the executed query is returned
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Function QueryDB(sSQL)
		CheckParam(sSQL)
		CheckConnectionString	
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		'    objRS.ActiveConnection = Connect
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function
	
	' PUBLIC METHODS ===========================================================================================
	
	' ----------------------------------------------------------------------------------------------------------
	' Name:  DeleteTest()
	' Purpose:  Deletes a Test from the system
	' Required Properties:  ConnectionString, p_iTestID
	' Optional Properties:  n/a
	' Parameters:  n/a
	' Outputs:  boolean - true if successfuly, false fi not
	' ----------------------------------------------------------------------------------------------------------		
	Function DeleteTest()
		dim sSQL 'as string
		dim rs 'as object
		
		checkParam(g_iTestID)
		
		DeleteTest = false 'default
		
		sSQL = "apDeleteAssociatesTests " & g_iTestID
		set rs = QueryDB(sSQL)	
		
		if err.number = 0 then 
			DeleteTest = true
		end if
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name:  GetStatuses()
	' Purpose:  Retrieves all Test Statuses
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:  n/a
	' Outputs: recordset of all Test Statuses
	' ----------------------------------------------------------------------------------------------------------			
	Function GetStatuses()
		dim sSQL 'as string
		dim rs 'as object
			
		sSQL = "SELECT * FROM AssociateTestStatuses ORDER BY Status"
		set rs = QueryDB(sSQL)		

		set GetStatuses = rs

		set rs = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name:  GetTests()
	' Purpose:  Retrieves all Tests for the specified Associate
	' Required Properties:  ConnectionString, g_iUserID
	' Optional Properties:	n/a
	' Parameters:  n/a
	' Outputs: recordset of all Tests for the specified Associate
	' ----------------------------------------------------------------------------------------------------------		
	Function GetTests()
		dim sSQL 'as string
		dim rs 'as object
		
		checkParam(g_iUserID)
		
		sSQL = "apGetAssociatesTests " & g_iUserID
		set rs = QueryDB(sSQL)		

		set GetTests = rs

		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name:  LoadTest()
	' Purpose:  Loads the specified Test
	' Required Properties:  ConnectionString, g_iUserID, g_iTestID
	' Optional Properties:  n/a
	' Parameters:  n/a
	' Outputs:  boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------				
	Function LoadTest()
		dim sSQL 'as string
		dim rs 'as object
		
		checkParam(g_iUserID)
		checkParam(g_iTestID)
		
		LoadTest = false 'default
		
		sSQL = "apGetAssociatesTests " & g_iUserID & ", " & g_iTestID
		set rs = QueryDB(sSQL)
		
		if err.number = 0 then 
			if not rs.eof then
				g_sName = rs("Name")
				g_iStateID = rs("StateID")
				g_sTestDate = rs("TestDate")
				g_iStatusID = rs("StatusID")
				g_iTestTypeID = rs("AssociateTestTypeID")

				LoadTest = true
			end if
		end if
			
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name:  SaveTest()
	' Purpose:  Inserts or updates an Associate Test
	' Required Properties: ConnectionString, g_sName, g_iStateID, g_sTestDate, g_iStatusID,  g_iUserID (for adds), g_iTestID (for updates)
	' Optional Properties:	n/a
	' Parameters:  n/a
	' Outputs:  integer - the TestID if successful, or -1 if not
	' ----------------------------------------------------------------------------------------------------------		
	Function SaveTest
		dim sSQL 'as string
		dim rs 'as object
		dim iNextSort 'as integer
		dim objConn 'as object
		
		checkParam(g_sName)
		checkParam(g_iStateID)
		checkParam(g_sTestDate)	
		checkParam(g_iStatusID)
        checkParam(g_iTestTypeID)

		SaveTest = -1 'Default
	
		if trim(g_iTestID) = "" then				
			checkParam(g_iUserID)
			
			'Add Test
			sSQL = "apInsertAssociatesTests " & g_iUserID & ", '" & g_sName & "', " & g_iStateID & ", '" & g_sTestDate & "', " & g_iStatusID & ", " & g_iTestTypeID
			set rs = QueryDB(sSQL)		

			'Saved Successfully
			if err.number = 0 and not rs.eof then
				'Get the ID
				SaveTest = clng(rs(0))

				rs.Close
			end if				
		else
			'Update Test		
			sSQL = "apUpdateAssociatesTests " & g_iTestID & ", '" & g_sName & "', " & g_iStateID & ", '" & g_sTestDate & "', " & g_iStatusID & ", " & g_iTestTypeID
			set rs = QueryDB(sSQL)		
			
			if err.number = 0 and not rs.eof then
				'Saved successfully
				if cint(rs(0)) > 0 then 
					SaveTest = g_iTestID
				end if
			end if	
		end if
		
		set rs = nothing
	End Function		
	
	' ----------------------------------------------------------------------------------------------------------
	' Name:  SearchTests()
	' Purpose:  Searches the test information based on the set criteria.
	' Required Properties: ConnectionString
	' Optional Properties:	SearchName, SearchStatusID, SearchStateIDList, SearchTestDateFrom, SearchTestDateTo, SearchBranchIDList, SearchCompanyL2IDList, SearchCompanyL3IDList, SearchCompanyID
	' Parameters:  n/a
	' Outputs:  recordset of the search results 
	' ----------------------------------------------------------------------------------------------------------		
	Function SearchTests()
		dim sSQL 'as string
		dim rs 'as object
		dim sStatus 'as string
		dim bFirstClause 'as boolean
		dim iStatusID 'as integer
		dim bFirstStatus 'as boolean
		
		'Headers		
		SearchTests = """First Name"",""Middle Name"",""Last Name"",""Test Name"",""State"",""Test Type"",""Test Date"",""Status""" & vbcrlf
		
		sSQL = "SELECT A.FirstName, A.MiddleName, A.LastName, AT.*, ST.State, S.Status, ATT.TestTypeName FROM AssociatesTests AS AT " & _
				      "INNER JOIN Associates AS A ON (A.UserID = AT.UserID) " & _
					  "INNER JOIN AssociateTestStatuses AS S ON (At.StatusID = S.StatusID) " & _
                      "LEFT JOIN AssociateTestTypes AS ATT ON (AT.AssociateTestTypeID = ATT.AssociateTestTypeID) " & _
					  "LEFT OUTER JOIN States AS ST ON (ST.StateID = AT.StateID)  " 
		
		bFirstClause = true
							  
		'If CompanyId search
		if g_iSearchCompanyId <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			sSQL = sSQL & "A.CompanyId = '" & g_iSearchCompanyId & "' "
		end if 					  
					  
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true		
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSQL = sSQL & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSQL = sSQL & " OR "
				elseif not bFirstTierList then
					sSQL = sSQL & " OR ("
				else
					sSQL = sSQL & " AND (("
					bFirstTierList = false
				end if
				
				sSQL = sSQL & "A.CompanyL2Id = '" & iCompanyL2Id & "' " 
							
				bFirstCompanyL2 = false				
			next 
			sSQL = sSQL & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSQL = sSQL & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSQL = sSQL & " OR "
				elseif not bFirstTierList then
					sSQL = sSQL & " OR ("
				else
					sSQL = sSQL & " AND (("
					bFirstTierList = false
				end if
				
				sSQL = sSQL & "A.CompanyL3Id = '" & iCompanyL3Id & "' "
							
				bFirstCompanyL3 = false				
			next 
			sSQL = sSQL & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSQL = sSQL & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSQL = sSQL & " OR "
				elseif not bFirstTierList then
					sSQL = sSQL & " OR ("
				else
					sSQL = sSQL & " AND (("
					bFirstTierList = false
				end if
				
				sSQL = sSQL & "A.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false			
			next 
			sSQL = sSQL & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSQL = sSQL & ") "
		end if

		'Last Name Search
		if 	g_sSearchName <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			
			sSQL = sSQL & " A.LastName like '%" & g_sSearchName & "%' "
		end if	

		'Test Date From Search
		if g_sSearchTestDateFrom <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			
			sSQL = sSQL & " AT.TestDate >=  '" & g_sSearchTestDateFrom & " 00:00' "		
		end if
					
		'Test Date To Search
		if g_sSearchTestDateTo <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			
			sSQL = sSQL & " AT.TestDate <=  '" & g_sSearchTestDateTo & " 23:59' "		
		end if					
							
		'StateID List Search
		if g_sSearchStateIDList <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			sSQL = sSQL & "(AT.StateID in (" & g_sSearchStateIDList & "))"
		end if	
							
		'StatusIDs Search
		if g_sSearchStatusIDs <> "" then
			if bFirstClause then
				sSQL = sSQL & " WHERE "
				bFirstClause = false
			else
				sSQL = sSQL & " AND "
			end if 
			
			sSQL = sSQL & "AT.StatusID in ("
			
			bFirstStatus = true
			for each iStatusID in split(g_sSearchStatusIDs, ", ")
				'Make sure only valid values are passed
				if isnumeric(iStatusID) then 
					if not bFirstStatus then
						sSQL = sSQL & ", "
					else
						bFirstStatus = false
					end if
					
					sSQL = sSQL & iStatusID
				end if
			next

			sSQL = sSQL & ") "
		end if								
		
        if g_sSearchTestTypeID <> "" then
            if bFirstClause then
                sSQL = sSQL & " WHERE "
                bFirstClause = false
            else
                sSQL = sSQL & " AND "
            end if

            sSQL = sSQL & "AT.AssociateTestTypeID = " & g_sSearchTestTypeID
        end if
        
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			do while not rs.eof
				SearchTests = SearchTests & """" & rs("FirstName") & """," & _
																   """" & rs("MiddleName") & """," & _
																   """" & rs("LastName") & """," & _
																   """" & rs("Name") & ""","
																   
				if isnull(rs("State")) then
					SearchTests = SearchTests & """National"","				
				else																				   
					SearchTests = SearchTests & """" & rs("State") & ""","
				end if
																   
				SearchTests = SearchTests & """" & rs("TestTypeName") & """," & rs("TestDate") & """," & _
																   """" & rs("Status") & """" & vbcrlf
				rs.MoveNext
			loop
		else
			SearchTests = SearchTests & "There are currently no results."
		end if
		
		set rs = nothing
	End Function
	
	' PROPERTY DEFINITION ======================================================================================
	'Connection String
	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property	

	'Name
	Public Property Let Name(p_sName)
		g_sName = p_sName
	End Property

	Public Property Get Name()
		Name = g_sName
	End Property	
	
	'SearchBranchIDList
	Public Property Let SearchBranchIDList(p_sSearchBranchIDList)
		g_sSearchBranchIDList = p_sSearchBranchIDList
	End Property

	Public Property Get SearchBranchIDList()
		SearchBranchIDList = g_sSearchBranchIDList
	End Property		
	
	'SearchCompanyID
	Public Property Let SearchCompanyID(p_iSearchCompanyID)
		g_iSearchCompanyID = p_iSearchCompanyID
	End Property

	Public Property Get SearchCompanyID()
		SearchCompanyID = g_iSearchCompanyID
	End Property		
	
	'SearchCompanyL2IDList
	Public Property Let SearchCompanyL2IDList(p_sSearchCompanyL2IDList)
		g_sSearchCompanyL2IDList = p_sSearchCompanyL2IDList
	End Property

	Public Property Get SearchCompanyL2IDList()
		SearchCompanyL2IDList = g_sSearchCompanyL2IDList
	End Property		
	
	'SearchCompanyL3IDList
	Public Property Let SearchCompanyL3IDList(p_sSearchCompanyL3IDList)
		g_sSearchCompanyL3IDList = p_sSearchCompanyL3IDList
	End Property

	Public Property Get SearchCompanyL3IDList()
		SearchCompanyL3IDList = g_sSearchCompanyL3IDList
	End Property				
	
	'SearchName
	Public Property Let SearchName(p_sSearchName)
		g_sSearchName = p_sSearchName
	End Property

	Public Property Get SearchName()
		SearchName = g_sSearchName
	End Property	
	
	'SearchStatusIDs
	Public Property Let SearchStatusIDs(p_sSearchStatusIDs)
		g_sSearchStatusIDs = p_sSearchStatusIDs
	End Property

	Public Property Get SearchStatusIDs()
		SearchStatusIDs = g_sSearchStatusIDs
	End Property		
	
	'SearchStateIDList
	Public Property Let SearchStateIDList(p_sSearchStateIDList)
		g_sSearchStateIDList = p_sSearchStateIDList
	End Property

	Public Property Get SearchStateIDList()
		SearchStateIDList = g_sSearchStateIDList
	End Property	
	
	'SearchTestDateFrom
	Public Property Let SearchTestDateFrom(p_sSearchTestDateFrom)
		g_sSearchTestDateFrom = p_sSearchTestDateFrom
	End Property

	Public Property Get SearchTestDateFrom()
		SearchTestDateFrom = g_sSearchTestDateFrom
	End Property	
	
	'SearchTestDateTo
	Public Property Let SearchTestDateTo(p_sSearchTestDateTo)
		g_sSearchTestDateTo = p_sSearchTestDateTo
	End Property

	Public Property Get SearchTestDateTo()
		SearchTestDateTo = g_sSearchTestDateTo
	End Property	
	
    'SearchTestTypeID
    Public Property Let SearchTestTypeID(p_sSearchTestTypeID)
        g_sSearchTestTypeID = p_sSearchTestTypeID
    End Property
    Public Property Get SearchTestTypeID
        SearchTestTypeID = g_sSearchTestTypeID
    End Property

	'StateID
	Public Property Let StateID(p_iStateID)
		g_iStateID = p_iStateID
	End Property

	Public Property Get StateID()
		StateID = g_iStateID
	End Property		
	
	'StatusID
	Public Property Let StatusID(p_iStatusID)
		g_iStatusID = p_iStatusID
	End Property

	Public Property Get StatusID()
		StatusID = g_iStatusID
	End Property			
	
	'TestDate
	Public Property Let TestDate(p_sTestDate)
		g_sTestDate = p_sTestDate
	End Property

	Public Property Get TestDate()
		TestDate = g_sTestDate
	End Property		
	
	'TestID
	Public Property Let TestID(p_iTestID)
		g_iTestID = p_iTestID
	End Property

	Public Property Get TestID()
		TestID = g_iTestID
	End Property		
	
    'TestTypeID
    Public Property Let TestTypeID(p_iTestTypeID)
        g_iTestTypeID = p_iTestTypeID
    End Property
    Public Property Get TestTypeID()
        TestTypeID = g_iTestTypeID
    End Property

	'UserID
	Public Property Let UserID(p_iUserID)
		g_iUserID = p_iUserID
	End Property

	Public Property Get UserID()
		UserID = g_iUserID
	End Property			
End Class
%>
<%
dim sTempVal
dim sTrackReferrer
dim iTrackUserLessonID
dim iTrackSectionID
dim iTrackUserID
dim bTrackSuccess

iTrackUserID = Session("User_ID")
sTrackReferrer  = request.servervariables("SCRIPT_NAME")
sTrackReferrer = mid(sTrackReferrer,instrrev(sTrackReferrer, "/")+1)

iTrackUserLessonID = trim(replace(request("UserLessonID"), "'", "''"))
iTrackSectionID = trim(replace(request("SectionID"), "'", "''"))

'if not empty, than this is not the 1st hit so Calculate Previous Course Hit
if session("COUSEPAGEVIEWED") then
	'if not on course page, then the Lesson and Section must be retrieved from sessions
	if UCase(sTrackReferrer) <> "COURSEPAGE.ASP" then	
		iTrackUserLessonID = left(session("COURSEHITINFO"),instr(session("COURSEHITINFO"),"/")-1)
		iTrackSectionID = mid(session("COURSEHITINFO"),instr(session("COURSEHITINFO"),"/")+1)
	end if
	
	bTrackSuccess = TrackCoursePageHit(iTrackUserID,iTrackUserLessonID,iTrackSectionID,true)
	
	session("COUSEPAGEVIEWED") = false
	session("COURSEHITINFO") = ""

	if not bTrackSuccess then
		response.redirect("/error.asp?message=The course hit was not tracked successfully.")
	end if
	
'if true, than this is the first hit so do not calculate Previous Course Hit
elseif UCase(sTrackReferrer) = "COURSEPAGE.ASP" then
	'Retrieve values
	bTrackSuccess = TrackCoursePageHit(iTrackUserID,iTrackUserLessonID,iTrackSectionID,false)
	session("COUSEPAGEVIEWED") = true

	if not bTrackSuccess then
		response.redirect("/error.asp?message=The course hit was not tracked successfully.")
	end if
end if

Function TrackCoursePageHit(p_iUserID,p_iUserLessonID,p_iSectionID,p_bCalcPreviousCourseHit)
	dim objConn 'as object
	dim sSQL 'as string
	dim rs 'as object
	dim iHitID 'as integer
	dim iPreviousSectionID 'as integer
	dim sCurrentHitDate, sPreviousHitDate 'as string
	dim lSecDiff 'as long
	dim lNewTotal 'as long
	
	set rs = server.createObject("ADODB.RecordSet")
	set objConn = server.CreateObject("ADODB.Connection")
	objConn.ConnectionString = application("sDataSourceName")
	objConn.Open
		
	'set the Last Activity date for the Entire Lesson
	sSQL = "UPDATE UsersLessons " & _
		   "SET LastActivityDate = '" & now() & "' " & _
		   "WHERE ID = '" & p_iUserLessonID & "' "
	objConn.execute(sSQL)
		
	'Track hit
	sSQL = "INSERT INTO TimeCourseHits (UserID,Lesson,SectionID) " & _
		   "VALUES ('" & p_iUserID & "','" & p_iUserLessonID & "','" & p_iSectionID & "')"
			   
	objConn.execute(sSQL)

	if p_bCalcPreviousCourseHit then				
		'Get the HitID for this record
		sSQL = "SELECT Max(HitID) FROM TimeCourseHits " & _
			   "WHERE UserID = '" & p_iUserID & "' " & _
			   "AND Lesson = '" & p_iUserLessonID & "' " & _
			   "AND SectionID = '" & p_iSectionID & "' "		
		rs.Open sSQL, objConn,3,3
		iHitID = rs(0)
		rs.Close
		
		if err.number <> 0 then
			set objConn = nothing
			set rs = nothing
			TrackCoursePageHit = false
			Exit Function
		end if 
		
		'Get the SectionID and Hit Date for the previous course hit
		sSQL = "SELECT SectionID, HitDate FROM TimeCourseHits " & _
			   "WHERE UserID = '" & p_iUserID & "' " & _
			   "AND Lesson = '" & p_iUserLessonID & "' " & _
			   "AND HitID < '" & iHitID & "' " & _
			   "ORDER BY HitID desc "
		rs.Open sSQL, objConn,3,3
		
		'if no value, then this was the first course hit
		if rs.eof then
			'don't do anything
			TrackCoursePageHit = true
		else
			iPreviousSectionID = rs("SectionID")
			sPreviousHitDate = rs("HitDate")
			rs.Close
			
			'Get the hit date for the current section
			sSQL = "SELECT HitDate FROM TimeCourseHits " & _
				   "WHERE UserID = '" & p_iUserID & "' " & _
				   "AND Lesson = '" & p_iUserLessonID & "' " & _
				   "AND HitID = '" & iHitID & "' " 
			rs.Open sSQL, objConn,3,3
			
			sCurrentHitDate = rs("HitDate")
			rs.Close
					
			'Determine the time difference between the two dates
			lSecDiff = DateDiff("s", sPreviousHitDate, sCurrentHitDate) 
			
			'the max difference is 10 minutes (60 * 20 = 1200)
			if clng(lSecDiff) > 1200 then
				lSecDiff = 1200
			end if
			
			'insert or update the Total time for the previous section

			'Get the current total time if any
			sSQL = "SELECT Total FROM TimeCourseTotals " & _
				   "WHERE UserID = '" & p_iUserID & "' " & _
				   "AND Lesson = '" & p_iUserLessonID & "' " & _
				   "AND SectionID = '" & iPreviousSectionID & "' "
			rs.Open sSQL, objConn,3,3
	
			if rs.eof then
				'no record, insert new record
				sSQL = "INSERT INTO TimeCourseTotals (UserID,Lesson,SectionID,Total) " & _
					   "VALUES ('" & p_iUserID & "','" & p_iUserLessonID & "','" & iPreviousSectionID & "','" & lSecDiff & "')"
			else
				'update existing record
				lNewTotal = clng(rs("Total")) + clng(lSecDiff)
				
				sSQL = "UPDATE TimeCourseTotals " & _
					   "Set Total = '" & lNewTotal & "' " & _
					   "WHERE UserID = '" & p_iUserID & "' " & _
					   "AND Lesson = '" & p_iUserLessonID & "' " & _
					   "AND SectionID = '" & iPreviousSectionID & "' "
			end if
			
			objConn.execute(sSQL)
			
			if err.number = 0 then
				TrackCoursePageHit = true
			else
				TrackCoursePageHit = False
			end if			
		end if
	else
		TrackCoursePageHit = true		
	end if
	
	set objConn = nothing
	set rs = nothing
End Function
%>
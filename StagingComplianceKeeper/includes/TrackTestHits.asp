<%
dim sTemp
dim sTrackTestReferrer
dim iTrackTestUserTestID
dim iTrackTestAttemptID
dim iTrackTestPageNo
dim iTrackTestUserID
dim bTrackTestSuccess

iTrackTestUserID = Session("User_ID")
sTrackTestReferrer  = request.servervariables("SCRIPT_NAME")
sTrackTestReferrer = mid(sTrackTestReferrer,instrrev(sTrackTestReferrer, "/")+1)

iTrackTestUserTestID = trim(replace(request("UserTestID"), "'", "''"))
iTrackTestAttemptID = session("TESTATTEMPT" & iTrackTestUserTestID)
iTrackTestPageNo = trim(request("PageNo"))

if iTrackTestPageNo = "" then
	iTrackTestPageNo = 1
end if

'if not empty, than this is not the 1st hit so Calculate Previous test Hit
if session("TESTPAGEVIEWED") then
	'if not on course page, then the Lesson and Section must be retrieved from sessions
	if UCase(sTrackTestReferrer) <> "TEST.ASP" then	
		iTrackTestUserTestID = left(session("TESTHITINFO"),instr(session("TESTHITINFO"),"/")-1)
		iTrackTestAttemptID = session("TESTATTEMPT" & iTrackTestUserTestID)
		iTrackTestPageNo = mid(session("TESTHITINFO"),instr(session("TESTHITINFO"),"/")+1)
	end if
	
	bTrackTestSuccess = TrackTestHit(iTrackTestUserID,iTrackTestUserTestID,iTrackTestAttemptID,iTrackTestPageNo,true)
	
	session("TESTPAGEVIEWED") = false
	session("TESTHITINFO") = ""

	if not bTrackTestSuccess then
		response.redirect("/error.asp?message=The test hit was not tracked successfully.")
	end if
	
'if true, than this is the first hit so do not calculate Previous Course Hit
elseif UCase(sTrackTestReferrer) = "TEST.ASP" then
	'Retrieve values
	bTrackTestSuccess = TrackTestHit(iTrackTestUserID,iTrackTestUserTestID,iTrackTestAttemptID,iTrackTestPageNo,false)
	session("TESTPAGEVIEWED") = true

	if not bTrackTestSuccess then
		response.redirect("/error.asp?message=The test hit was not tracked successfully.")
	end if
end if

Function TrackTestHit(p_iUserID,p_iUserTestID,p_iAttemptID,p_iPageNo,p_CalcPreviousTestHit)
	dim objConn 'as object
	dim sSQL 'as string
	dim rs 'as object
	dim iUserTestID, iTestID 'as integer
	dim iHitID 'as integer
	dim iPreviousTestPageNo 'as integer
	dim sCurrentHitDate, sPreviousHitDate 'as string
	dim lSecDiff 'as long
	dim lNewTotal 'as long
		
	set rs = server.createobject("ADODB.RecordSet")
	set objConn = server.CreateObject("ADODB.Connection")
	objConn.ConnectionString = application("sDataSourceName")
	objConn.Open
		
	'get the UserLessonID and TestID of the test
	sSQL = "SELECT Lesson, TestID FROM UsersTestsInfo " & _
		   "WHERE UserID = '" & p_iUserID & "' " & _
		   "AND ID = '" & p_iUserTestID & "' "
	rs.Open sSQL, objConn, 3, 3
		
	if not rs.eof then
		iUserLessonID = rs("Lesson")
		iTestID = rs("TestID")
		rs.Close

		'set the Last Test Page submitted for the Entire Test
		sSQL = "UPDATE UsersTestsAttempts " & _
			   "SET LastTestPage = '" & p_iPageNo & "' " & _
			   "WHERE UserID = '" & p_iUserID & "' " & _
			   "AND AttemptID = '" & p_iAttemptID & "' " & _
			   "AND UserTestID = '" & p_iUserTestID & "' "
		objConn.execute(sSQL)
			
		sSQL = "INSERT INTO TimeTestHits(UserID,AttemptID,TestID,Lesson,TestPageNo) " & _
			   "VALUES ('" & p_iUserID & "','" & p_iAttemptID & "','" & iTestID & "','" & iUserLessonID & "','" & p_iPageNo & "') "
		objConn.execute(sSQL)
	
		if p_CalcPreviousTestHit then			
			'Get the HitID for this record
			sSQL = "SELECT Max(HitID) FROM TimeTestHits " & _
				   "WHERE UserID = '" & p_iUserID & "' " & _
				   "AND AttemptID = '" & p_iAttemptID & "' " & _
				   "AND Lesson = '" & iUserLessonID & "' " & _
				   "AND TestID = '" & iTestID & "' " & _
				   "AND TestPageNo = '" & p_iPageNo & "' "
			rs.Open sSQL, objConn, 3, 3
			
			iHitID = rs(0)
			rs.close

			'Get the PageNo and Hit Date for the previous course hit
			sSQL = "SELECT TestPageNo, HitDate FROM TimeTestHits " & _
				   "WHERE UserID = '" & p_iUserID & "' " & _
				   "AND AttemptID = '" & p_iAttemptID & "' " & _				   
				   "AND Lesson = '" & iUserLessonID & "' " & _
				   "AND TestID = '" & iTestID & "' " & _
				   "AND HitID < '" & iHitID & "' " & _
				   "ORDER BY HitID desc "
			rs.Open sSQL, objConn, 3, 3
		
			'if no value, then this was the first course hit
			if rs.eof then
				'don't do anything
				TrackTestHit = true
			else
				iPreviousTestPageNo = rs("TestPageNo")
				sPreviousHitDate = rs("HitDate")
				rs.Close
				
				'Get the hit date for the current section
				sSQL = "SELECT HitDate FROM TimeTestHits " & _
					   "WHERE UserID = '" & p_iUserID & "' " & _
					   "AND HitID = '" & iHitID & "' " 
				rs.Open sSQL, objConn, 3, 3
			
				sCurrentHitDate = rs("HitDate")
				rs.Close
				
				'Determine the time difference between the two dates
				lSecDiff = DateDiff("s", sPreviousHitDate, sCurrentHitDate) 
			
				'the max difference is 10 minutes (60 * 10 = 600)
				if clng(lSecDiff) > 600 then
					lSecDiff = 600
				end if
			
				'insert or update the Total time for the previous section

				'Get the current total time if any
				sSQL = "SELECT Total FROM TimeTestTotals " & _
					   "WHERE UserID = '" & p_iUserID & "' " & _
					   "AND AttemptID = '" & p_iAttemptID & "' " & _					   
					   "AND Lesson = '" & iUserLessonID & "' " & _
					   "AND TestID = '" & iTestID & "' " & _
					   "AND TestPageNo = '" & iPreviousTestPageNo & "' "
				rs.Open sSQL, objConn, 3, 3
			
				if rs.eof then
					'no record, insert new record
					sSQL = "INSERT INTO TimeTestTotals (UserID,AttemptID,TestID,Lesson,TestPageNo,Total) " & _
						   "VALUES ('" & p_iUserID & "','" & p_iAttemptID & "','" & iTestID & "','" & iUserLessonID & "','" & iPreviousTestPageNo & "','" & lSecDiff & "')"
				else
					'update existing record
					lNewTotal = clng(rs("Total")) + clng(lSecDiff)
			
					sSQL = "UPDATE TimeTestTotals " & _
						   "SET Total = '" & lNewTotal & "' " & _
						   "WHERE UserID = '" & p_iUserID & "' " & _
						   "AND AttemptID = '" & p_iAttemptID & "' " & _						   
						   "AND Lesson = '" & iUserLessonID & "' " & _
						   "AND TestID = '" & iTestID & "' " & _
						   "AND TestPageNo = '" & iPreviousTestPageNo & "' " 
				end if
			
				objConn.execute(sSQL)
			
				if err.number = 0 then
					TrackTestHit = true
				else
					TrackTestHit = False
				end if
			end if
		else
			TrackTestHit = true
		end if
	else
		TrackTestHit = false		
	end if
			
	set objConn = nothing
	set rs = nothing	
End Function
%>
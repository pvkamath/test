<%
'===============================================================================================================
' Class: User
' Purpose: Class used for retrieving and storing User info
' Create Date: 8/30/2004
' Current Version: 1.0
' ----------------------
' Properties:
'	- ConnectionString
' Methods:
'	- CheckConnectionString()
'	- CheckParam(p_sParam)
'	- QueryDB(sSQL)
'	- AddAnswer(p_iAttemptID,p_iQuestionID,p_iAnswerID)
'	- AddCompanySA(p_iCompanyID,p_iBranchID)
'	- AddCourse(p_iCourseID,p_fPrice)
'	- AddCrossCertCourse(p_iID,p_iCrossCertCourseID,p_fPrice)
'	- AddNote(p_sTitle,p_sText)
'	- AddTestAttempt(p_iUserTestID)
'	- AddUser()
'	- ChangeStatus()
'   - ChangeMembership(p_iUserId,p_bMembership,p_sMemberNumber))
'   - ChangeSubscription(p_iUserId,p_bSubscribe))
'	- CheckTestAnswer(p_iAttemptID,p_iQuestionID,p_iCorrectAnswerID)
'	- CourseCompleted(p_iUserCourseID)
'	- DeleteCourse(p_iID)
'	- DeleteNote(p_iNoteID)
'	- DisplayCourse(p_iID)
'	- DisplayLesson(p_iUserCourseID,p_iUserLessonID)
'	- EditNote(p_iNoteID,p_sTitle,p_sText)
' 	- EditTestAttempts(p_iUserTestID,p_iAttemptsLeft)
'	- EditUserCourse(p_iUserCourseID,p_sCourseExpirationDate,p_iCourseStatus,p_sCertificateExpirationDate,p_iCertificateReleased)
'	- EmailExists(p_sEmail)
'	- FailTestAttempt(p_iAttemptID)
'	- GetAdminCourseActions(p_iUserCourseID,p_sSort)
'	- GetAnswer(p_iQuestionID, p_iAttemptID)
'	- GetCertificate(p_iID)
'	- GetCourse(p_iID)
'	- GetCourseActions(p_iUserCourseID,p_sSort)
'	- GetCourses(p_iCurExp,p_iStatus,p_iCrossCertified,p_iCoreUserCourseID,p_sOrder)
'	- GetCourseTestScore(p_iUserCourseID)
' 	- GetLastTestAttempt(p_iUserTestID)
'	- GetLastTestPageViewed(p_iAttemptID)
'	- GetLesson(p_iUserLessonID)
'	- GetLessons(p_iID)
'	- GetNextLesson(p_iUserCourseID,p_iUserLessonID)
'	- GetNoOfTestAttempts(p_iUserCourseID)
'	- GetNote(p_iNoteID)
'	- GetNotes(p_sSort)
'	- GetPassedTestAttempt(p_iUserTestID)
'	- GetPreviousLesson(p_iUserCourseID,p_iUserLessonID)
'	- GetRandomVerificationQuestion(p_iVerificationMethod)
'	- GetSection(p_iSectionID)
'	- GetSections(p_iUserLessonID)
'	- GetTestAnswer(p_iAttemptID,p_iQuestionID)
'	- GetTestInfo(p_iUserTestID)
'	- GetTestInfoByID(p_iUserTestID)
'	- GetTestPosition(p_iUserTestID,p_iPageNo)
'	- GetTests(p_iUserCourseID,p_iUserLessonID,p_iSectionID)
'	- GetTotalLessonTime(p_iUserLessonID)
'	- GetTotalTestTime(p_iAttemptID)
'	- GetTotalTestPageTime(p_iAttemptID,p_iPageNo)
'	- GetTotalSectionTime(p_iUserLessonID,p_iSectionID)
'	- GetUser()
'	- HasAdminCourseChanges(p_iUserCourseID)
'	- HasCourses(p_iUserID)
'	- HasCrossCertifications(p_iID)
'	- InsertTransactionInfo(p_fTotalPrice,p_fShippingCost,p_fCouponDiscount,p_fMemberDiscount,p_iPaid)
'	- InsertTransactionCourse(p_iTransactionID,p_iCourseID,p_sCourseName,p_fPrice)
'	- IsCorporateUser()
'	- IsProctorDisclaimerSet(p_iUserCourseID)
'	- IsSupplementCourse(p_iUserCourseID)
'	- IsTestPassed(p_iUserTestID)
'	- LessonCompleted(p_iUserLessonID)
'	- LoginExists(p_sLogin)
'	- SetCourseToCompleted(p_iUserCourseID,p_sCompletionDate)
'	- SetDisclaimerFlag(p_iUserCourseID)
'	- SetLessonToCompleted(p_iUserLessonID)
'	- SetTestToCompleted(p_iUserTestID,p_iAttemptID,p_bPassed)
'	- TestCompleted(p_iUserTestID)
'	- TrackAdminCourseAction(p_iAdminUserID,p_iUserCourseID,p_sAction)
'	- TrackCourseAction(p_iUserCourseID,p_sAction)
'	- TrackCoursePageHit(p_iUserLessonID,p_iSectionID,,p_CalcPreviousCourseHit)
'	- TrackTestHit(p_iUserTestID,p_iAttemptID,p_iPageNo,p_CalcPreviousTestHit)
'	- TrackTestVerification(iUserTestID,iQuestionID,bSuccess)
'
'===============================================================================================================
Class User
	' GLOBAL VARIABLE DECLARATIONS =============================================================================
	dim g_sConnectionString 'as string
	dim iUserID 'as integer
	dim sEmail 'as string
	dim sPassword 'as string
	dim sFirstName 'as string
	dim sMiddleName 'as string
	dim sLastName 'as string
	dim iGroupID 'as integer
	dim iCompanyID 'as integer
	dim iBranchID 'as integer
	dim sCompanyL2IdList 'as string
	dim sCompanyL3IdList 'as string
	dim sBranchIdList 'as string	
	dim iProviderId
	dim sCertCompanyName 'as string
	dim sLicenseNo 'as string
	dim sSSN 'as string
	dim sDriversLicenseNo 'as string
	dim g_iDriversLicenseStateId 'as integer
	dim sCity 'as string
	dim iStateID 'as integer
	dim sAddress 'as string
	dim sZipcode 'as string
	dim sPhone 'as string
	dim sFax 'as string
	dim iUserStatus 'as integer
	dim sMarketWareConfirmNo 'as string
	dim sNotes 'as string
	dim iCrossCertEligible 'as integer
	dim iNewsletter 'as integer
	dim iMembership 'as integer	
	dim iHowHearType 'as integer
	dim sHowHearOther 'as string
	dim iAllowedToAddUsers 'as integer
	dim dLastLoginDate 'as date
	dim iCConAlertEmails 'as ineteger
	
	' PRIVATE METHODS ===========================================================================================
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:		CheckConnectionString
	' Purpose:	Check to make sure the connectionstring has been set
	' Required Properties:  n/a
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				recordset - a recordset of the executed query is returned
	' --------------------------------------------------------------------------------------------------------------------------------	
	Private Sub CheckConnectionString()
		if isNull(g_sConnectionString) then
			response.write "You must specify the value of the ConnectionString Property value before proceeding."
		end if
	End Sub

	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					CheckParam
	' Purpose:				Checks to make sure the passed in value is not null
	' Required Properties:  n/a
	' Optional Properties:	n/a
	' Parameters:			p_sParam - the required parameter
	' Outputs:				n/a
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Sub CheckParam(p_sParam)
		if ((isNull(p_sParam)) or (trim(p_sParam) = "")) then
			response.write "Processing Error!!!<br>You must specify the value before proceeding."
			response.end
		end if
	End Sub
	
	' --------------------------------------------------------------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				recordset - a recordset of the executed query is returned
	' --------------------------------------------------------------------------------------------------------------------------------
	Private Function QueryDB(sSQL)
		CheckParam(sSQL)
		CheckConnectionString	
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		'    objRS.ActiveConnection = Connect
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	End Function

	' PUBLIC METHODS ===========================================================================================
	' -------------------------------------------
	' Name: RecordLoginDate
	' Desc: Records in the database the datetime that a user logs in.
	' Prerequisits: ConnectionString, iUserId
	' Outputs: Boolean - true if successful
	' -------------------------------------------
	function RecordLoginDate()

		dim oConn
		dim sSql
		dim lRecordsAffected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = application("sDataSourceName")
		oConn.Open

		sSql = "UPDATE Users SET " & _
			"LastLoginDate = '" & now() & "' " & _
			"WHERE UserId = '" & iUserId & "' " 
		oConn.Execute sSql, lRecordsAffected

		if lRecordsAffected <> 1 then
			RecordLoginDate = false
		else
			RecordLoginDate = true
		end if

		set oConn = nothing		
	
	end function


	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddAnswer()
	' Purpose:				adds a answer for the specified user's test
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iAttemptID - the id of the attempt
	'						p_iQuestionID - the id of the Question
	'						p_iAnswerID - the id of the answer
	' Outputs:				boolean - true if added successfully, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function AddAnswer(p_iAttemptID,p_iQuestionID,p_iAnswerID)	
		dim objConn 'as object
		dim sSQL 'as string

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'Delete any previous values
		sSQL = "DELETE FROM UsersTestsAnswers " & _
			   "WHERE AttemptID = '" & p_iAttemptID & "' " & _
			   "AND QuestionID = '" & p_iQuestionID & "' " 
		objConn.execute(sSQL)
		
		'add new value
		sSQL = "INSERT INTO UsersTestsAnswers(AttemptID,QuestionID,AnswerID) " & _
			   "VALUES ('" & p_iAttemptID & "','" & p_iQuestionID & "','" & p_iAnswerID & "') "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			AddAnswer = true
		else
			AddAnswer = false
		end if
		
		set objConn = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddCompanySA()
	' Purpose:				adds a user as a company or branch SA
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iCompanyID the ID of the Company
	'						p_iBranchID the ID of the Branch
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function AddCompanySA(p_iCompanyID,p_iBranchID)
		dim objConn 'as object
		dim sSQL 'as string

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open

		'Delete any old info
		sSQL = "DELETE FROM CompanyAdmins WHERE UserID = '" & iUserID & "' "
		objConn.execute(sSQL)

		if err.number = 0 then
			'Add new rec
			sSQL = "INSERT INTO CompanyAdmins(CompanyID,UserID"
			
			'if branch is set
			if (trim(p_iBranchID) <> "") then
				sSQL = sSQL & ",BranchID"
			end if
			sSQL = sSQL & ") "
			
			sSQL = sSQL & "VALUES ('" & p_iCompanyID & "','" & iUserID & "'"
			
			'if branch is set
			if (trim(p_iBranchID) <> "") then
				sSQL = sSQL & ",' & p_iBranchID & "'"
			end if
			sSQL = sSQL & ") "
			
			objConn.execute(sSQL)
			
			if err.number = 0 then
				AddCompanySA = true
			else
				AddCompanySA = false
			end if
		else
			AddCompanySA = false
		end if
			
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddCourse()
	' Purpose:				adds a course for the specified user
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iCourseID the id of the main course
	'						p_fPrice the price of the course
	' Outputs:				integer - the id of the course if successful, -1 if not
	' ----------------------------------------------------------------------------------------------------------	
	function AddCourse(p_iCourseID,p_fPrice)
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		dim iCertificateReleased ' as integer
		dim sCourseExpirationDate, sCertExpirationDate 'as string
		dim iReleaseType 'as integer
		dim iID 'integer
		dim lessonRS 'as object
		dim iUserLessonID 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open

		'Get the expiration date of the Course
		sSQL = "SELECT ExpirationDate FROM Courses WHERE CourseID = '" & p_iCourseID & "' "
		set rs = QueryDB(sSQL)
		if not rs.EOF then
			sCourseExpirationDate = rs("ExpirationDate")	
		else
			sCourseExpirationDate = ""
		end if 
		
		'Get the ReleaseType and ExpirationDate
		sSQL = "SELECT ReleaseType, ExpirationDate FROM Certificates WHERE CourseID = '" & p_iCourseID & "'"
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			iReleaseType = rs("ReleaseType")	
			'if true, the certificate is release on purchase
			if cint(iReleaseType) = 1 then
				iCertificateReleased = 1
			else
				iCertificateReleased = 0		
			end if
			sCertExpirationDate = rs("ExpirationDate")
		else
			iCertificateReleased = 0
			sCertExpirationDate = ""
		end if
			
		'insert course info
		sSQL = "INSERT INTO UsersCourses(UserID,CourseID,CourseStatus,CourseExpirationDate,CertificateReleased, Price "
		
		'if Certificate Expiration Date not blank
		if sCertExpirationDate <> "" then
			sSQL = sSQL & ",CertificateExpirationDate "
		end if
		
		sSQL =  sSQL & ") VALUES ('" & iUserID & "','" & p_iCourseID & "',1,'" & sCourseExpirationDate & "','" & iCertificateReleased & "'," & p_fPrice & " "

		'if Certificate Expiration Date not blank
		if sCertExpirationDate <> "" then
			sSQL = sSQL & ",'" & sCertExpirationDate & "'"
		end if
		sSQL = sSQL & ")"
		
		objConn.execute(sSQL)
		
		if err.number = 0 then		
			'Get the ID
			sSQL = "SELECT MAX(ID) FROM UsersCourses"
			set rs = QueryDB(sSQL)
			iID = rs(0)			
		
			'get the Lesson info for this course
			sSQL = "SELECT LessonID FROM Lessons WHERE CourseID = '" & p_iCourseID & "' ORDER BY Sort"
			set rs = QueryDB(sSQL)
			
			do while not rs.eof
				'insert lessons for the user
				sSQL = "INSERT INTO UsersLessons(UserID,LessonID,Course) " & _
					   "VALUES ('" & iUserID & "','" & rs("LessonID") & "','" & iID & "')"
				objConn.execute(sSQL)
				
				'Get the ID of the lesson in the usersLessons table
				sSQL = "SELECT MAX(ID) FROM UsersLessons " & _
					   "WHERE UserID = '" & iUserID & "' "
				set lessonRS = QueryDB(sSQL)
			
				if err.number = 0 then
					iUserLessonID = lessonRS(0)
					
					'Make a record for each test and quiz of the current lesson for the user
					sSQL = "INSERT INTO UsersTestsInfo (UserID,TestID,Lesson,AttemptsLeft) " & _
						   "SELECT '" & iUserID & "', TestID, '" & iUserLessonID & "', MaxAttempts FROM Tests WHERE LessonID = '" & rs("LessonID") & "' "
					objConn.execute(sSQL)
					
					if err.number <> 0 then
						set lessonRS = nothing
						set rs = nothing
						set objConn = nothing
						AddCourse = false			
						Exit Function		
					end if
				else
					set lessonRS = nothing
					set rs = nothing
					set objConn = nothing
					AddCourse = false
					Exit Function					
				end if
				
				rs.MoveNext
			loop
			AddCourse = iID
		else
			AddCourse = -1
		end if
		
		set lessonRS = nothing
		set objConn = nothing
		set rs = nothing
	end function
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddCrossCertCourse()
	' Purpose:				adds a cross cert course for the specified user
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	'						p_iCrossCertCourseID - Cross Cert ID
	'						p_fPrice the price of the course
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	function AddCrossCertCourse(p_iID,p_iCrossCertCourseID,p_fPrice)
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		dim iCrossCertType 'as integer
		dim iSupplementCourseID 'as integer
		dim sCourseExpirationDate, sCertExpirationDate 'as string
		dim iID 'as integer
		dim bCompleted 'as integer
		dim sCompletionDate 'as string
		dim iCertificateReleased 'as integer
		dim iCourseID 'as integer
		dim iSupplementUserCourseID 'as integer
		dim iReleaseType 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'Get the CourseID
		set rs = GetCourse(p_iID)
		iCourseID = rs("CourseID")
	
		'Get Cross CertType and SupplementCourseID, if any
		sSQL = "SELECT * FROM CrossCertifications " & _
			   "WHERE CoreCourseID = '" & iCourseID & "' AND CrossCertCourseID = '" & p_iCrossCertCourseID & "' "
		set rs = QueryDB(sSQL)
		
		iCrossCertType = rs("CrossCertType")
		iSupplementCourseID = rs("SupplementCourseID")		
		
		'if this is a Supplement Cross cert, then you add the Supplement
		if (trim(iSupplementCourseID) <> "" and not isnull(iSupplementCourseID)) then
			iID = AddCourse(iSupplementCourseID,0)
			iSupplementUserCourseID = iID
		else
			iID = AddCourse(p_iCrossCertCourseID,p_fPrice)
		end if
				
		if err.number = 0 then
			'Add the Cross Certified flag, Cross Cert Type and Core Course ID
			sSQL = "UPDATE UsersCourses " & _
				   "SET CrossCertified = 1, " & _
				   "	CrossCertifiedCoreCourse = '" & p_iID & "', " & _
				   "	CrossCertType = '" & iCrossCertType & "' " & _
				   "WHERE ID = '" & iID & "' "
			objConn.execute(sSQL)
			
			if err.number = 0 then
				'if certificate crosscerttype, add the necessary fields if the Core Course is completed
				if (cint(iCrossCertType) = 1) then
					'Get Completion info of Core Course
					sSQL = "SELECT Completed, CompletionDate, CertificateReleased FROM UsersCourses " & _
						   "WHERE ID = '" & p_iID & "' AND UserID = '" & iUserID & "' "
					set rs = QueryDB(sSQL)
					
					bCompleted = rs("Completed")
					sCompletionDate = rs("CompletionDate")
					rs.close
					
					'Get the Certification release type of cross cert
					set rs = GetCertificate(iID)
					
					if not rs.eof then
						iReleaseType = rs("ReleaseType")
						
						'if release type is set to Proctor Form Received, don't release it
						if cint(iReleaseType) = 3 then
							iCertificateReleased = 0
						else
							iCertificateReleased = 1 
						end if
					else
						iCertificateReleased = 0
					end if
					
					'if completed, add data to cross cert
					if bCompleted then
						sSQL = "UPDATE UsersCourses " & _
							   "SET Completed = 1, " & _
							   "	CompletionDate = '" & now() & "', " & _
							   "	CertificateReleased = '" & iCertificateReleased & "' " & _
							   "WHERE ID = '" & iID & "' "
						objConn.execute(sSQL)
						
						if err.number = 0 then
							AddCrossCertCourse = true
						else
							AddCrossCertCourse = false
						end if
					else
						'not completed
						AddCrossCertCourse = true
					end if
				elseif (cint(iCrossCertType) = 2) then
					'the initial cross cert is now added for Supplement Cross Certs
					iID = AddCourse(p_iCrossCertCourseID,p_fPrice)
					
					if err.number = 0 then
						'Add the Cross Certified flag, Cross Cert Type and Core Course ID
						sSQL = "UPDATE UsersCourses " & _
							   "SET CrossCertified = 1, " & _
							   "	CrossCertifiedCoreCourse = '" & p_iID & "', " & _
							   "	CrossCertType = '" & iCrossCertType & "', " & _
							   "	SupplementCourseID = '" & iSupplementUserCourseID & "' " & _
							   "WHERE ID = '" & iID & "' "
						objConn.execute(sSQL)
					
						if err.number = 0 then
							AddCrossCertCourse = true			
						else
							AddCrossCertCourse = false			
						end if
					else
						AddCrossCertCourse = false			
					end if
				end if
			else
				AddCrossCertCourse = false			
			end if
		else
			set objConn = nothing
			set rs = nothing
			AddCrossCertCourse = false
		end if
		
		set objConn = nothing
		set rs = nothing
	end function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddNote()
	' Purpose:				adds a new note for the user
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_sTitle the title of the note
	'						p_sText the text of the note
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function AddNote(p_sTitle, p_sText)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open	
		
		sSQL = "INSERT INTO Notes(UserID,Title,Text) " & _
			   "VALUES ('" & iUserID & "','" & p_sTitle & "','" & p_sText & "')"
		objConn.execute(sSQL)
		
		if err.number = 0 then
			AddNote = true
		else
			AddNote = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddTestAttempt()
	' Purpose:				adds a new test attempt for the user
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID  the id of the test in the UsersTestss table
	' Outputs:				integer - the ID of the attempt, or -1 if not successfuly
	' ----------------------------------------------------------------------------------------------------------	
	Function AddTestAttempt(p_iUserTestID)
		dim sSQL 'as string
		dim rs 'as object
		dim iUserLessonID 'as integer
		dim iTestID 'as integer
		dim iAttemptID 'as integer
		dim objConn 'as object
		dim iAttemptsLeft 'as integer
		dim iAttemptNo 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'Get the UserLessonID and TestID
		sSQL = "SELECT * FROM UsersTestsInfo WHERE ID = '" & p_iUserTestID & "' "
		set rs = QueryDB(sSQL)

		iUserLessonID = rs("Lesson")
		iTestID = rs("TestID")
		
		'get the AttemptNo
		sSQL = "SELECT MAX(AttemptNo) FROM UsersTestsAttempts WHERE UserTestID = '" & p_iUserTestID & "' "
		set rs = QueryDB(sSQL)	
		
		'if true, no attempts so set to 1
		if trim(rs(0)) = "" or isnull(rs(0)) then
			iAttemptNo = 1
		else
			iAttemptNo = rs(0) + 1
		end if	

		'add attempt entry
		sSQL = "INSERT INTO UsersTestsAttempts(UserID,UserTestID,Lesson,TestID,AttemptNo) " & _
			   "VALUES ('" & iUserID & "','" & p_iUserTestID & "','" & iUserLessonID & "','" & iTestID & "','" & iAttemptNo & "')"
		objConn.execute(sSQL)
		
		if err.number = 0 then
			sSQL = "SELECT MAX(AttemptID) FROM UsersTestsAttempts " & _
				   "WHERE UserID = '" & iUserID & "' " & _
				   "AND UserTestID = '" & p_iUserTestID & "' " 
			set rs = QueryDB(sSQL)
			iAttemptID = rs(0)
			
			AddTestAttempt = iAttemptID
		else
			AddTestAttempt = -1
		end if
		
		set rs = nothing
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				AddUser()
	' Purpose:				adds a new user
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			n/a
	' Outputs:				integer - the user ID of the newly added user or -1 if it was not added
	' ----------------------------------------------------------------------------------------------------------	
	Function AddUser()
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		dim iUserID 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open

		sSQL = "INSERT INTO Users (UserName, Password, FirstName, MiddleName, LastName, Email, GroupID, " & _
			   "Address, City, Zipcode, Phone, Fax, UserStatus, " & _
			   "Notes"
		
		'if CompanyID, not blank
		if (trim(iCompanyID) <> "") then
			sSQL = sSQL & ", CompanyID"
		end if

		'if BranchID, not blank
		'if (trim(iBranchID) <> "") then
		'	sSQL = sSQL & ", BranchID"		
		'end if

		'if ProviderId not blank
		if (trim(iProviderId) <> "") then
			sSql = sSql & ", ProviderID"
		end if

		'if StateID, not blank
		if (trim(iStateID) <> "") then
			sSQL = sSQL & ", StateID"		
		end if
		
		'if CConAlertEmails, not blank
		if (trim(iCConAlertEmails) <> "") then
			sSQL = sSQL & ", CConAlertEmails"		
		end if
		
		sSQL = sSQL & ") VALUES ('" & sLogin & "','" & sPassword & "','" & sFirstName & "','" & sMiddleName & "','" & sLastName & "','" & sEmail & "','" & iGroupID & "'," & _
					  "'" & sAddress & "','" & sCity & "','" & sZipcode & "','" & Phone & "','" & Fax & "','" & UserStatus & "'," & _
					  "'" & Notes & "'" 
					  
		'if CompanyID, not blank
		if (trim(iCompanyID) <> "") then
			sSQL = sSQL & ",'" & iCompanyID & "'"
		end if

		'if BranchID, not blank
		'if (trim(iBranchID) <> "") then
		'	sSQL = sSQL & ",'" & iBranchID & "'"
		'end if
		
		'if ProviderID not blank
		if (trim(iProviderId) <> "") then
			sSql = sSql & ",'" & iProviderId & "'"
		end if

		'if StateID, not blank
		if (trim(iStateID) <> "") then
			sSQL = sSQL & ",'" & iStateID & "'"
		end if

		'if CConAlertEmails, not blank
		if (trim(iCConAlertEmails) <> "") then
			sSQL = sSQL & "," & iCConAlertEmails & " "
		end if
		
		sSQL = sSQL & ")"

		objConn.execute(sSQL)
		
		if err.number = 0 then		
			'Get the QuestionID
			sSQL = "SELECT MAX(UserID) FROM Users"
			set rs = QueryDB(sSQL)
			iUserID = rs(0)
			
			'Membership info
			'if cint(iMembership) = 1 then
			'	bSuccess = ChangeMembership(iUserID, true, "TPM" & iUserID)		
			'else 
			'	bSuccess = ChangeMembership(iUserID, false, "")
			'end if
			
			'Make record in the afxUsersGroupsX XREF table
			sSQL = "INSERT INTO afxUsersGroupsX (UserID, GroupID) " & _
				   "VALUES ('" & rs(0) & "','" & iGroupID & "') "
			objConn.execute(sSQL)
			
			
			'Clear the list of User to CompanyL2 associations for this user.  Then
			'we'll fill the list from scratch.
			sSql = "DELETE FROM UsersCompaniesL2 WHERE UserId = '" & rs(0) & "' "
			objConn.Execute(sSql)
			
			dim iIndivCompanyL2Id
			dim aCompanyL2Ids
			aCompanyL2Ids = split(sCompanyL2IdList, ", ")
			for each iIndivCompanyL2Id in aCompanyL2Ids
			
				sSql = "INSERT INTO UsersCompaniesL2 (UserId, CompanyL2Id) " & _
					"VALUES (" & _
					"'" & rs(0) & "', " & _
					"'" & iIndivCompanyL2Id & "' " & _
					")"
				objConn.Execute(sSql)
				
			next
			
			
			'Clear the list of User to CompanyL3 associations for this user.  Then
			'we'll fill the list from scratch.
			sSql = "DELETE FROM UsersCompaniesL3 WHERE UserId = '" & rs(0) & "' "
			objConn.Execute(sSql)
			
			dim iIndivCompanyL3Id
			dim aCompanyL3Ids
			aCompanyL3Ids = split(sCompanyL3IdList, ", ")
			for each iIndivCompanyL3Id in aCompanyL3Ids
			
				sSql = "INSERT INTO UsersCompaniesL3 (UserId, CompanyL3Id) " & _
					"VALUES (" & _
					"'" & rs(0) & "', " & _
					"'" & iIndivCompanyL3Id & "' " & _
					")"
				objConn.Execute(sSql)
				
			next
			
			
			'Clear the list of User to Branch associations for this user.  Then
			'well fill the list from scratch.
			sSql = "DELETE FROM UsersBranches WHERE UserId = '" & rs(0) & "' "
			objConn.Execute(sSql)
			
			dim iIndivBranchId
			dim aBranchIds
			aBranchIds = split(sBranchIdList, ", ")
			for each iIndivBranchId in aBranchIds
			
				sSql = "INSERT INTO UsersBranches (UserId, BranchId) " & _
					"VALUES (" & _
					"'" & rs(0) & "', " & _
					"'" & iIndivBranchId & "' " & _
					")"
				objConn.Execute(sSql)						
			
			next
			

			'Company SA processing
			'Delete any old info
			'sSQL = "DELETE FROM CompanyAdmins WHERE UserID = '" & iUserID & "' "
			'objConn.execute(sSQL)

			'if this user is a Company SA, add to XRef table
			'if cint(iGroupID) = 4 then
			'	'Add new rec
			'	sSQL = "INSERT INTO CompanyAdmins(CompanyID,UserID,AllowedToAddUsers"
			'
			'	'if branch is set
			'	if (trim(iBranchID) <> "") then
			'		sSQL = sSQL & ",BranchID"
			'	end if
			'	sSQL = sSQL & ") "
			'
			'	sSQL = sSQL & "VALUES ('" & iCompanyID & "','" & iUserID & "','" & iAllowedToAddUsers & "'"
			'
			'	'if branch is set
			'	if (trim(iBranchID) <> "") then
			'		sSQL = sSQL & ",'" & iBranchID & "'"
			'	end if
			'	sSQL = sSQL & ") "
			'
			'	objConn.execute(sSQL)
			'end if
			
			AddUser = iUserID
		else
			AddUser = -1
		end if
		
		set objConn = nothing
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				ChangeStatus()
	' Purpose:				changes the status of a user
	' Required Properties:  ConnectionString, iUserID, iUserStatus
	' Optional Properties:	n/a
	' Parameters:			n/a
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function ChangeStatus()
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE Users " & _
			   "SET UserStatus = '" & iUserStatus & "' "
			   
		'if deleteion, set deletiondate
		if cint(iUserStatus) = 3 then
			sSQL = sSQL & ", DeletionDate = '" & now() & "' "
		end if
		
		sSQL = sSQL & "WHERE UserID = '" & iUserID & "' "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			ChangeStatus = true
		else
			ChangeStatus = false		
		end if
		
		set objConn = nothing
		set rs = nothing
	End Function		

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				ChangeMembership()
	' Purpose:				Changes the users membership status.
	' Required Properties:  ConnectionString
	' Inputs:				p_iUserId - User ID
	'						p_bMembership - Boolean, true to subscribe, false to unsubscribe
	'						p_sMemberNumber - string, the member number
	' Optional Properties:	n/a
	' Parameters:			n/a
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function ChangeMembership(p_iUserId,p_bMembership,p_sMemberNumber)
	
		err.Clear
		
		CheckConnectionString
		CheckParam(p_iUserId)
		CheckParam(p_bMembership)
	
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString 'application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE Users "
		
		if p_bMembership then
		
			sSql = sSql & "SET Membership = '1',MemberNumber='" & p_sMemberNumber & "' "
			
		else
		
			sSql = sSql & "SET Membership = '0',MemberNumber=''"
			
		end if 
		
		sSQL = sSQL & "WHERE UserID = '" & p_iUserID & "' "
		
		objConn.execute(sSQL)
	
		if err.number = 0 then		
			ChangeMembership = true
		else		
			ChangeMembership = false
		end if
		
		set objConn = nothing
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				ChangeSubscription()
	' Purpose:				Changes the users newsletter subscription status.
	' Required Properties:  ConnectionString
	' Inputs:				p_iUserId - User ID
	'						p_bSubscribe - Boolean, true to subscribe, false to unsubscribe
	' Optional Properties:	n/a
	' Parameters:			n/a
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function ChangeSubscription(p_iUserId,p_bSubscribe)
	
		err.Clear
		
		CheckConnectionString
		CheckParam(p_iUserId)
		CheckParam(p_bSubscribe)
	
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = g_sConnectionString 'application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE Users "
		
		if p_bSubscribe then
		
			sSql = sSql & "SET Newsletter = '1'"
			
		else
		
			sSql = sSql & "SET Newsletter = '0'"
			
		end if 
		
		sSQL = sSQL & "WHERE UserID = '" & p_iUserID & "' "
		
		objConn.execute(sSQL)
	
		if err.number = 0 then		
			ChangeSubscription = true
		else		
			ChangeSubscription = false
		end if
		
		set objConn = nothing
		set rs = nothing
	End Function

	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				CheckTestAnswer()
	' Purpose:				checks to see if the user got this question right
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iAttemptID - the attempt id
	'						p_iQuestionID - the id of the question
	'						p_iCorrectAnswerID - the id of the correct answer
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function CheckTestAnswer(p_iAttemptID,p_iQuestionID,p_iCorrectAnswerID)
		dim sSQL
		dim rs
		
		sSQL = "SELECT count(*) FROM UsersTestsAnswers " & _
			   "WHERE AttemptID = '" & p_iAttemptID & "' " & _
			   "AND QuestionID = '" & p_iQuestionID & "' " & _
			   "AND AnswerID = '" & p_iCorrectAnswerID & "' "
		set rs = QueryDB(sSQL)
		
		if rs(0) = 1 then
			CheckTestAnswer = true
		else
			CheckTestAnswer = false
		end if
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				CourseCompleted()
	' Purpose:				checks to see if a Course has been completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the course has been completed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function CourseCompleted(p_iUserCourseID)
		dim rs 'as object
		dim sSQL 'as string
		
		sSQL = "SELECT Completed FROM UsersCourses WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserCourseID & "' "
		set rs = QueryDB(sSQL)
		
		CourseCompleted = rs("Completed")
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DeleteCourse()
	' Purpose:				deletes a user course and any cross certs of that course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DeleteCourse(p_iID)
		dim sSQL 'as string
		dim objConn 'as object

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'Delete the Course from the database
'		sSQL = "ap_DeleteUserCourse " & p_iID 
'		objConn.execute(sSQL)
		
		sSQL = "UPDATE UsersCourses " & _
			   "SET CourseStatus = 3 " & _
			   "WHERE ID = '" & p_iID & "' or CrossCertifiedCoreCourse = '" & p_iID & "' "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			DeleteCourse = true
		else
			DeleteCourse = false
		end if
		
		set objConn = nothing
	End function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DeleteNote()
	' Purpose:				deletes a User's Note
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iNoteID  the id of the Note
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DeleteNote(p_iNoteID)
		dim sSQL 'as string
		dim objConn 'as object

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'Delete the Course
		sSQL = "DELETE FROM Notes " & _
			   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iNoteID & "' "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			DeleteNote = true
		else
			DeleteNote = false
		end if
		
		set objConn = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DisplayCourse()
	' Purpose:				determines whether or not a course should display on the front end
	'						only display core courses and cross certifed courses whose Core course and Supplement Course with applicaple are complete
	'						Cross Cert Courses that require a Supplement should not be shown
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the course should get displayed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DisplayCourse(p_iID)	
		dim sSQL 'as string
		dim rs 'as object
		dim iCrossCertified 'as integer
		dim iCoreCourse 'as integer
		dim iSupplementUserCourseID 'as integer
		
		sSQL = "SELECT CrossCertified, CrossCertifiedCoreCourse, SupplementCourseID FROM UsersCourses " & _
			   "WHERE ID = '" & p_iID & "' "
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			iCrossCertified = rs("CrossCertified")
			iCoreCourse = rs("CrossCertifiedCoreCourse")			
			iSupplementUserCourseID = rs("SupplementCourseID")
		else
			set rs = nothing
			DisplayCourse = false
			Exit Function
		end if
		
		'core course
		if cint(iCrossCertified) = 0 then
			DisplayCourse = true
		else
			'Cross Certified course
			'Get the completed value of the core course						
			sSQL = "SELECT Completed FROM UsersCourses " & _
				   "WHERE ID = '" & iCoreCourse & "' "
				   
			set rs = QueryDB(sSQL)
			
			'if true, the core course is completed so the cross cert can be displayed
			if rs("Completed") then
				'if course has Supplement course, make sure this has been completed also
				if (iSupplementUserCourseID <> "" or not isnull(iSupplementUserCourseID)) then
					'Get the completed value of the core course						
					sSQL = "SELECT Completed FROM UsersCourses " & _
						   "WHERE ID = '" & iSupplementUserCourseID & "' "
				   
					set rs = QueryDB(sSQL)
					
					if rs("Completed") then
						DisplayCourse = true
					else 
						DisplayCourse = false
					end if
				else
					DisplayCourse = true
				end if
			else
				DisplayCourse = false
			end if
		end if
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				DisplayLesson()
	' Purpose:				determines whether or not a lesson should display on the front end
	'						A lesson should only display if the previous lessons does not have a test or the test has been passed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID the id of the current course in the UsersCourses table
	'						p_iUserLessonID  the id of the lesson in the UsersLessons table
	' Outputs:				boolean - true if the lesson should get displayed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function DisplayLesson(p_iUserCourseID,p_iUserLessonID)	
		dim sSQL 'as string
		dim rs 'as object
		dim iSort 'as integer
		dim iPreviousUserLessonID 'as integer
		dim iPreviousLessonID 'as integer

		'Get the Previous UserLessonID
		iPreviousUserLessonID = GetPreviousLesson(p_iUserCourseID,p_iUserLessonID)

		'if no PreviousLessonID, then show because this is the first lesson
		if trim(iPreviousUserLessonID) = "" then
			DisplayLesson = true
		else
			set rs = GetLesson(iPreviousUserLessonID)
			iPreviousLessonID = rs("LessonID")

			'Check to see if the lesson has been completed
			if LessonCompleted(iPreviousUserLessonID) then
				'Check to see if the Previous lesson has a test
				sSQL = "SELECT * FROM Tests AS T " & _
					   "INNER JOIN Lessons AS L ON (L.LessonID = T.LessonID) " & _
					   "INNER JOIN UsersTestsInfo AS UTI ON (UTI.TestID = T.TestID) " & _
					   "WHERE UTI.UserID = '" & iUserID & "' " & _
					   "AND UTI.Lesson = '" & iPreviousUserLessonID & "' " & _
					   "AND L.LessonID = '" & iPreviousLessonID & "' " & _
					   "AND T.TestType = '1'"

				set rs = QueryDb(sSQL)

				'if no test, then show Lesson
				if cint(rs.recordcount) = 0 then
					DisplayLesson = true
				else
					'if the Test has been passed, show lesson
					if cbool(rs("Passed")) then
						DisplayLesson = true
					else
						DisplayLesson = false
					end if
				end if			
			else
				'The lesson has not been completed
				DisplayLesson = false
			end if
		end if
	
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				EditUser()
	' Purpose:				edits a new user
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			n/a
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function EditUser()
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE Users " & _
			   "SET UserName = '" & sLogin & "', " & _
			   "	Password = '" & sPassword & "', " & _
			   "	FirstName = '" & sFirstName & "', " & _
			   "	MiddleName = '" & sMiddleName & "', " & _
			   "	LastName = '" & sLastName & "', " & _
			   "	Email = '" & sEmail & "', " & _
			   "	GroupId = '" & iGroupId & "', " & _
			   "	Address = '" & sAddress & "', " & _
			   "	City = '" & sCity & "', " & _
			   "	Zipcode = '" & sZipcode & "', " & _
			   "	Phone = '" & sPhone & "', " & _
			   "	Fax = '" & sFax & "', " & _
			   "	UserStatus = '" & iUserStatus & "', " & _
			   "	CConAlertEmails = " & iCConAlertEmails & ", " & _
			   "	Notes = '" & sNotes & "' "

		'if CompanyID, not blank
		if (trim(iCompanyID) <> "") then
			sSQL = sSQL & ", CompanyID = '" & iCompanyID & "' "
		else
			sSQL = sSQL & ", CompanyID = null "
		end if

		'if BranchID, not blank
		'if (trim(iBranchID) <> "") then
		'	sSQL = sSQL & ", BranchID = '" & iBranchID & "' "
		'else
		'	sSQL = sSQL & ", BranchID = null "			
		'end if
		
		'if ProviderId not blank
		if (trim(iProviderId) <> "") then
			sSql = sSql & ", ProviderID = '" & iProviderId & "' "
		else
			sSql = sSql & ", ProviderID = null "
		end if

		'if StateID, not blank
		if (trim(iStateID) <> "") then
			sSQL = sSQL & ", StateID = '" & iStateID & "' "
		else
			sSQL = sSQL & ", StateID = null "			
		end if
		
		sSQL = sSQL & "WHERE UserID = '" & iUserID & "' "
		
		objConn.execute(sSQL)
	
		if err.number = 0 then		
			'Membership info
			'if cint(iMembership) = 1 then
			'	bSuccess = ChangeMembership(iUserID, true, "TPM" & iUserID)		
			'else 
			'	bSuccess = ChangeMembership(iUserID, false, "")
			'end if
			'		
			sSQL = "UPDATE afxUsersGroupsX " & _
				   "SET GroupID = '" & iGroupID & "' " & _
				   "WHERE UserID = '" & iUserID & "' "
			objConn.execute(sSQL)
		
		
		
			'Clear the list of User to CompanyL2 associations for this user.  Then
			'we'll fill the list from scratch.
			sSql = "DELETE FROM UsersCompaniesL2 WHERE UserId = '" & iUserId & "' "
			objConn.Execute(sSql)
			
			dim iIndivCompanyL2Id
			dim aCompanyL2Ids
			aCompanyL2Ids = split(sCompanyL2IdList, ", ")
			for each iIndivCompanyL2Id in aCompanyL2Ids
			
				sSql = "INSERT INTO UsersCompaniesL2 (UserId, CompanyL2Id) " & _
					"VALUES (" & _
					"'" & iUserId & "', " & _
					"'" & iIndivCompanyL2Id & "' " & _
					")"
				objConn.Execute(sSql)
				
			next
			
			
			'Clear the list of User to CompanyL3 associations for this user.  Then
			'we'll fill the list from scratch.
			sSql = "DELETE FROM UsersCompaniesL3 WHERE UserId = '" & iUserId & "' "
			objConn.Execute(sSql)
			
			dim iIndivCompanyL3Id
			dim aCompanyL3Ids
			aCompanyL3Ids = split(sCompanyL3IdList, ", ")
			for each iIndivCompanyL3Id in aCompanyL3Ids
			
				sSql = "INSERT INTO UsersCompaniesL3 (UserId, CompanyL3Id) " & _
					"VALUES (" & _
					"'" & iUserId & "', " & _
					"'" & iIndivCompanyL3Id & "' " & _
					")"
				objConn.Execute(sSql)
				
			next
			
		
		
			'Clear the list of User to Branch associations for this user.  Then
			'well fill the list from scratch.
			sSql = "DELETE FROM UsersBranches WHERE UserId = '" & iUserId & "' "
			objConn.Execute(sSql)
			
			dim iIndivBranchId
			dim aBranchIds
			aBranchIds = split(sBranchIdList, ", ")
			for each iIndivBranchId in aBranchIds
			
				sSql = "INSERT INTO UsersBranches (UserId, BranchId) " & _
					"VALUES (" & _
					"'" & iUserId & "', " & _
					"'" & iIndivBranchId & "' " & _
					")"
				objConn.Execute(sSql)						
			
			next
		
		
			'Company SA processing
			'Delete any old info
			'sSQL = "DELETE FROM CompanyAdmins WHERE UserID = '" & iUserID & "' "
			'objConn.execute(sSQL)

			'if this user is a Company SA, add to XRef table
			'if cint(iGroupID) = 4 then
			'	'Add new rec
			'	sSQL = "INSERT INTO CompanyAdmins(CompanyID,UserID,AllowedToAddUsers"
			''
			'	'if branch is set
			'	if (trim(iBranchID) <> "") then
			'		sSQL = sSQL & ",BranchID"
			'	end if
			'	sSQL = sSQL & ") "
			'
			'	sSQL = sSQL & "VALUES ('" & iCompanyID & "','" & iUserID & "','" & iAllowedToAddUsers & "'"
			'
			'	'if branch is set
			'	if (trim(iBranchID) <> "") then
			'		sSQL = sSQL & ",'" & iBranchID & "'"
			'	end if
			'	sSQL = sSQL & ") "
			'
			'	objConn.execute(sSQL)
			'end if
			
			EditUser = true
		else		
			EditUser = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				EditNote()
	' Purpose:				Edits a User's Note
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_sTitle the title of the note
	'						p_sText the text of the note
	' Outputs:				boolean - true if successfully edited, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function EditNote(p_iNoteID,p_sTitle,p_sText)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open	
		
		sSQL = "UPDATE Notes " & _
			   "SET Title = '" & p_sTitle & "', " & _
			   "	Text = '" & p_sText & "' " & _
			   "WHERE ID = '" & p_iNoteID & "' "
	   
		objConn.execute(sSQL)
		
		if err.number = 0 then
			EditNote = true
		else
			EditNote = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				EditTestAttempts()
	' Purpose:				Edits a User's Test/Quiz attempts left
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID the id of the test in the UsersTestss table
	'						p_iAttemptsLeft the number of attempts a user has left to complete a test or quiz
	' Outputs:				boolean - true if successfully edited, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function EditTestAttempts(p_iUserTestID,p_iAttemptsLeft)
		dim objConn 'as object
		dim sSQL 'as string
		dim iAttemptsLeft 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open

		'if blank, change to null (unlimited)
		if trim(p_iAttemptsLeft) = "" then
			iAttemptsLeft = "NULL"
		else
			iAttemptsLeft = p_iAttemptsLeft
		end if
		
		sSQL = "UPDATE UsersTestsInfo " & _
			   "SET AttemptsLeft = " & iAttemptsLeft & " " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND ID = '" & p_iUserTestID & "' "

		objConn.execute(sSQL)
		
		if err.number = 0 then
			EditTestAttempts = true
		else 
			EditTestAttempts = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				EditUserCourse()
	' Purpose:				Edits a User's Course Information
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID the id of the course in the UserCourses table
	'						p_iCompleted the Completed flag
	'						p_sCompletionDate the course completion date
	'						p_sCourseExpirationDate the course expiration date
	'						p_iCourseStatus the status of the course
	'						p_sCertificateExpirationDate the certificate expiration date
	'						p_iCertificateReleased the certificate release type
	' Outputs:				boolean - true if successfully edited, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function EditUserCourse(p_iUserCourseID,p_iCompleted,p_sCompletionDate,p_sCourseExpirationDate,p_iCourseStatus,p_sCertificateExpirationDate,p_iCertificateReleased)
		dim objConn 'as object
		dim sSQL 'as string
		dim bSuccess 'as boolean
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open

		sSQL = "UPDATE UsersCourses " & _
			   "SET CourseExpirationDate = '" & p_sCourseExpirationDate & "', " & _
			    "	CourseStatus = '" & p_iCourseStatus & "' "
			   
		if cint(p_iCompleted) = 1 then
			bSuccess = SetCourseToCompleted(p_iUserCourseID,p_sCompletionDate)
		
			if not bSuccess then
				EditUserCourse = false
				Exit Function
			end if
		else
			sSQL = sSQL & ", Completed = '" & p_iCompleted & "' " & _
			              ",  CompletionDate = null "
		end if
			  
		'if certificate values set, update
		if trim(p_sCertificateExpirationDate) <> "" then
			sSQL = sSQL & ", CertificateExpirationDate = '" & p_sCertificateExpirationDate & "' "
		end if
		
		if trim(p_iCertificateReleased) <> "" then
			sSQL = sSQL & ", CertificateReleased = '" & p_iCertificateReleased & "' "
		end if
		
		sSQL = sSQL & "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserCourseID & "' "

		objConn.execute(sSQL)
		
		if err.number = 0 then
			EditUserCourse = true
		else 
			EditUserCourse = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				EmailExists()
	' Purpose:				determines if an email address is in the system
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_sEmail
	' Outputs:				boolean - true if the email exist, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function EmailExists(p_sEmail)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Count(*) FROM Users AS U " & _
			   "INNER JOIN UserStatusTypes AS UST ON (UST.StatusID = U.UserStatus) " & _
			   "WHERE U.Email = '" & p_sEmail & "' " & _
			   "AND NOT U.UserStatus = '3' "
			   

		set rs = QueryDB(sSQL)

		if cint(rs(0)) > 0 then
			EmailExists = true
		else
			EmailExists = false
		end if		

		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				FailTestAttempt()
	' Purpose:				Fails the specified test attempt
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iAttemptID the id of the attempt
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------
	Function FailTestAttempt(p_iAttemptID)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open		
		
		sSQL = "UPDATE UsersTestsAttempts " & _
			   "SET Completed = 1 " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND AttemptID = '" & p_iAttemptID & "' "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			FailTestAttempt = true
		else
			FailTestAttempt = false
		end if
		
		set objConn = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetAdminCourseActions()
	' Purpose:				Retrieves the actions a user has performed on a specifiecd course or all courses
	' Required Properties:  ConnectionString
	' Optional Properties:
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table, optional
	'						p_sSort the sort value
	' Outputs:				recordset of the course actions
	' ----------------------------------------------------------------------------------------------------------
	Function GetAdminCourseActions(p_iUserCourseID,p_sSort)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM TrackAdminCourseActions AS TCA " & _
			   "INNER JOIN UsersCourses AS UC ON (UC.ID = TCA.Course) " & _
			   "INNER JOIN Courses AS C ON (UC.CourseID = C.CourseID) " & _
			   "INNER JOIN Users AS U ON (U.UserID = TCA.AdminUserID) " & _
			   "WHERE Course = '" & p_iUserCourseID & "' "
		
		if p_sSort <> "" then
			sSQL = sSQL & "ORDER BY " & p_sSort
		else
			sSQL = sSQL & "ORDER BY ActionDate desc"
		end if
	
		set GetAdminCourseActions = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetAnswer()
	' Purpose:				Retrieves the Answer to the specified question 
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iQuestionID the id of the question
	'						p_iAttemptID the id of the attempt
	' Outputs:				recordset of the answer information
	' ----------------------------------------------------------------------------------------------------------
	Function GetAnswer(p_iQuestionID, p_iAttemptID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT * FROM UsersTestsAnswers AS UTA " & _
			   "INNER JOIN UsersTestsAttempts AS UTAT ON (UTAT.AttemptID = UTA.AttemptID) " & _
			   "INNER JOIN TestsAnswers AS TA ON (UTA.AnswerID = TA.AnswerID) " & _
			   "WHERE UTAT.UserID = '" & iUserID & "' " & _
			   "AND UTA.QuestionID = '" & p_iQuestionID & "' " & _
			   "AND UTA.AttemptID = '" & p_iAttemptID & "' "
		   
		set GetAnswer = QueryDB(sSQL)
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCertificate()
	' Purpose:				Retrieves the Certificate Information for the specified course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				recordset of the Certificate information
	' ----------------------------------------------------------------------------------------------------------
	Function GetCertificate(p_iID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM Certificates AS CE " & _
			   "INNER JOIN Courses AS C ON (C.CourseID = CE.CourseID) " & _
			   "INNER JOIN UsersCourses AS UC ON (UC.CourseID = C.CourseID) " & _
			   "WHERE UC.ID = '" & p_iID & "' " & _
			   "AND UC.UserID = '"& iUserID & "' "
		
		set GetCertificate = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCourse()
	' Purpose:				Retrieves the Course Information for the specified course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				recordset of the course information
	' ----------------------------------------------------------------------------------------------------------
	Function GetCourse(p_iID)
		dim sSQL 'as string
		
		sSQL = "SELECT C.*, UC.CourseExpirationDate, UC.CertificateExpirationDate, UC.CourseStatus, UC.CertificateReleased, UC.Completed, UC.CompletionDate, CTX.TypeID, S.State FROM Courses AS C " & _
			   "INNER JOIN UsersCourses AS UC ON (UC.CourseID = C.CourseID) " & _
			   "INNER JOIN CoursesTypesX AS CTX ON (CTX.CourseID = C.CourseID) " & _
			   "LEFT OUTER JOIN States AS S ON (S.StateID = C.StateID) " & _
			   "WHERE UC.ID = '" & p_iID & "' " & _
			   "AND UC.UserID = '"& iUserID & "' " & _
			   "ORDER BY CTX.TypeID asc"
	
		set GetCourse = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCourseActions()
	' Purpose:				Retrieves the actions a user has performed on a specifiecd course or all courses
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table, optional
	'						p_sSort the sort value
	' Outputs:				recordset of the course actions
	' ----------------------------------------------------------------------------------------------------------
	Function GetCourseActions(p_iUserCourseID,p_sSort)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM TrackCourseActions AS TCA " & _
			   "LEFT OUTER JOIN UsersCourses AS UC ON (UC.ID = TCA.Course) " & _
			   "LEFT OUTER JOIN Courses AS C ON (UC.CourseID = C.CourseID) " & _
			   "WHERE TCA.UserID = '" & iUserID & "' "
		
		if trim(p_iUserCourseID) <> "" then
			sSQL = sSQL & " AND Course = '" & p_iUserCourseID & "' "
		end if
		
		if p_sSort <> "" then
			sSQL = sSQL & "ORDER BY " & p_sSort
		else
			sSQL = sSQL & "ORDER BY ActionDate desc"
		end if
	
		set GetCourseActions = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCourses()
	' Purpose:				Retrieves the User's Course information
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iCurExp - if set, 1 is used to show only current, 0 is used to show only expired
	'						p_iStatus - if set, 1 is used to show only Active, 2 is used to show disabled
	'						p_iCrossCertified - if set, 0 is to show only Core Courses, 1 is to show only Cross Cert Courses
	'						p_iCoreUserCourseID - the id of the core user course id in the userscourses table (only for cross certs)
	'						p_sOrder - how the data should be sorted, optional
	' Outputs:				recordset of the User's Course Info 
	' ----------------------------------------------------------------------------------------------------------
	Function GetCourses(p_iCurExp,p_iStatus,p_iCrossCertified,p_iCoreUserCourseID,p_sOrder)
		dim sSQL 'as string
		dim sDate 'as string
		
		sSQL = "SELECT * FROM UsersCourses AS UC " & _
			   "INNER JOIN Courses AS C ON (UC.CourseID = C.CourseID) " & _
			   "WHERE UC.UserID = '" & iUserID & "' "

		'Current or expired flag
		if trim(p_iCurExp) <> "" then
			'get current date
			sDate = date()
		
			'if only current
			if cint(p_iCurExp) = 1 then
				sSQL = sSQL & " AND UC.CourseExpirationDate >= '" & sDate & " 00:00' "
			else
				'if only expired
				sSQL = sSQL & " AND UC.CourseExpirationDate < '" & sDate & " 00:00' "
			end if
		end if
		
		'Status flag
		if trim(p_iStatus) <> "" then
			sSQL = sSQL & " AND UC.CourseStatus = '" & p_iStatus & "' "
		end if 
			   
		'CrossCertified flag
		if trim(p_iCrossCertified) <> "" then
			sSQL = sSQL & " AND UC.CrossCertified = '" & p_iCrossCertified & "' "
			
			'Core Course
			if trim(p_iCoreUserCourseID) <> "" then
				sSQL = sSQL & " AND UC.CrossCertifiedCoreCourse = '" & p_iCoreUserCourseID & "' "
			end if
		end if
	
		if trim(p_sOrder) <> "" then
			sSQL = sSQL & " ORDER BY " & p_sOrder
		else
			sSQL = sSQL & " ORDER BY Name "
		end if
	
		set GetCourses = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCourseTestScore()
	' Purpose:				Combines the total passed test scores of a course into one percentage
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserCourseID the ID of the course in the UsersCourses table
	' Outputs:				string - the percentage of the combined passed tests or N/A if no results
	' ----------------------------------------------------------------------------------------------------------
	Function GetCourseTestScore(p_iUserCourseID)
		dim sSQL 'as string
		dim rs1,correctAnsRS 'as objects
		dim iTestID 'as integer
		dim iAttemptID 'as integer
		dim iNoOfCorrect 'as integer
		dim	iNoOfQuestions 'as integer
		
		'Get Passed test attempts
		sSQL = "SELECT UTA.AttemptID, UTA.TestID FROM UsersLessons AS UL " & _
			   "INNER JOIN UsersTestsAttempts AS UTA ON (UTA.Lesson = UL.ID) " & _
			   "WHERE (UL.Course = '" & p_iUserCourseID & "') AND (UTA.Passed = 1) "
		set rs1 = QueryDB(sSQL)
		
		if not rs1.eof then
			iNoOfCorrect = 0
			iNoOfQuestions = 0

			do while not rs1.eof
				iTestID = rs1("TestID")
				iAttemptID = rs1("AttemptID")
			
				'Get the correct answers
				sSQL = "SELECT * FROM TestsAnswers AS TA " & _
					   "INNER JOIN TestsQuestions AS TQ ON (TA.QuestionID = TQ.QuestionID) " & _
					   "WHERE TQ.TestID = '" & iTestID & "' AND TA.CorrectAnswer = 1 " & _
					   "ORDER BY TQ.Sort"
				set correctAnsRS = QueryDB(sSQL)
			
				do while not correctAnsRS.eof
					'Check to see if the user's answer was correct
					if CheckTestAnswer(iAttemptID,correctAnsRS("QuestionID"),correctAnsRS("AnswerID")) then
					 	iNoOfCorrect = iNoOfCorrect + 1
					end if
				
					correctAnsRS.MoveNext
					iNoOfQuestions = iNoOfQuestions + 1
				loop
				correctAnsRS.Close
				rs1.MoveNext
			loop
			
			'Compute combined test score
			GetCourseTestScore = round((iNoOfCorrect / iNoOfQuestions) * 100) & "%"
		else
			GetCourseTestScore = "N/A"
		end if
		
		set rs1 = nothing
		set correctAnsRS = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLastTestAttempt()
	' Purpose:				Retrieves the ID of the last test Attempt
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserTestID  the id of the last test attempt in the UsersTestsAttempts table
	' Outputs:				integer - the ID of the last test attempt, or 0 if none
	' ----------------------------------------------------------------------------------------------------------
	Function GetLastTestAttempt(p_iUserTestID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Max(AttemptID) FROM UsersTestsAttempts " & _		
			   "WHERE UserTestID = '" & p_iUserTestID & "' AND Completed = 0"
		set rs = QueryDB(sSQL)
		
		if (isnull(rs(0))) then
			GetLastTestAttempt = 0
		else
			GetLastTestAttempt = rs(0)
		end if
	
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLastTestPageViewed()
	' Purpose:				Retrieves the Page No of the last test page viewed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iAttemtpID  the id of attempt
	' Outputs:				integer - the page no , or 0 if null
	' ----------------------------------------------------------------------------------------------------------
	Function GetLastTestPageViewed(p_iAttemptID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT LastTestPage FROM UsersTestsAttempts " & _		
			   "WHERE AttemptID = '" & p_iAttemptID & "' "
		set rs = QueryDB(sSQL)
		
		if (isnull(rs(0))) then
			GetLastTestPageViewed = 0
		else
			GetLastTestPageViewed = rs(0)
		end if
	
		set rs = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLesson()
	' Purpose:				Retrieves the information for a specific Lesson
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersLessons table
	' Outputs:				recordset of the User's Lesson Info for the specified course
	' ----------------------------------------------------------------------------------------------------------
	Function GetLesson(p_iUserLessonID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM UsersLessons AS UL " & _
			   "INNER JOIN Lessons AS L ON (UL.LessonID = L.LessonID) " & _
			   "WHERE UL.ID = '" & p_iUserLessonID & "' "
		
		set GetLesson = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetLessons()
	' Purpose:				Retrieves the User's Lesson Info for the specified course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				recordset of the User's Lesson Info for the specified course
	' ----------------------------------------------------------------------------------------------------------
	Function GetLessons(p_iID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM UsersLessons AS UL " & _
			   "INNER JOIN Lessons AS L ON (UL.LessonID = L.LessonID) " & _
			   "WHERE UL.Course = '" & p_iID & "' " & _
			   "ORDER BY L.Sort "
		
		set GetLessons = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetNextLesson()
	' Purpose:				Retrieves the next lesson of a course a user has to take
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserCourseID the id of the current course in the UsersCourses table
	'						p_iUserLessonID  the id of the current lesson in the UsersLessons table
	' Outputs:				integer - the UserLessonID of a user's next lesson to take or 0 if none
	' ----------------------------------------------------------------------------------------------------------
	Function GetNextLesson(p_iUserCourseID,p_iUserLessonID)
		dim sSQL 'as string
		dim iSort 'as integer
		dim iCourseID 'as string
		dim rs 'as object
		
		'Get the CourseID and sort of the lesson
		sSQL = "SELECT L.CourseID, L.Sort FROM Lessons AS L " & _
			   "INNER JOIN UsersLessons AS UL ON (UL.LessonID = L.LessonID) " & _
			   "WHERE UL.UserID = '" & iUserID & "' AND UL.ID = '" & p_iUserLessonID & "' AND UL.Course = '" & p_iUserCourseID & "' "
		set rs = QueryDB(sSQL)
		
		iCourseID = rs("CourseID")
		iSort = rs("sort")
		
		'Get the next lesson, if any
		sSQL = "SELECT ID FROM UsersLessons AS UL " & _
			   "INNER JOIN Lessons AS L ON (L.LessonID = UL.LessonID) " & _
			   "WHERE UL.UserID = '" & iUserID & "' AND UL.Course = '" & p_iUserCourseID & "' " & _
			   "AND L.CourseID = '" & iCourseID & "' AND L.Sort = '" & iSort + 1 & "' "		
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			GetNextLesson = rs("ID")
		else
			GetNextLesson = ""
		end if			   
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetNoOfTestAttempts()
	' Purpose:				Retrieves the number of test attempts a user had on a Course
	' Required Properties:  ConnectionString
	' Optional Properties:	
	' Parameters:			p_iUserCourseID the id of the course in the UsersCourses table
	' Outputs:				integer - the number of attempts or 0 if none
	' ----------------------------------------------------------------------------------------------------------
	Function GetNoOfTestAttempts(p_iUserCourseID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT COUNT(*) FROM UsersLessons AS UL " & _ 
			   "INNER JOIN UsersTestsAttempts AS UTA ON (UTA.Lesson = UL.ID) " & _
			   "WHERE (UL.Course = '" & p_iUserCourseID & "')"
		set rs = QueryDB(sSQL)
		
		if not isnull(rs(0)) then
			GetNoOfTestAttempts = rs(0)
		else
			GetNoOfTestAttempts = 0
		end if			  
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetNote()
	' Purpose:				Retrieves the User's stored Note
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iNoteID the id of the note
	' Outputs:				recordset of the User's Note
	' ----------------------------------------------------------------------------------------------------------
	Function GetNote(p_iNoteID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM Notes " & _
			   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iNoteID & "' "
		
		set GetNote = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetNotes()
	' Purpose:				Retrieves the User's stored Notes
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	p_sSort - how the notes should be sorted
	' Parameters:			n/a
	' Outputs:				recordset of the User's Notes
	' ----------------------------------------------------------------------------------------------------------
	Function GetNotes(p_sSort)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM Notes " & _
			   "WHERE UserID = '" & iUserID & "' "
			   
		if trim(p_sSort) <> "" then
			sSQL = sSQL & "ORDER BY " & p_sSort
		else
			sSQL = sSQL & "ORDER BY UpdatedDate desc"
		end if
	
		set GetNotes = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetPassedTestAttempt()
	' Purpose:				Retrieves the Attempt ID of the Test/Quiz Attempt that was passed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserTestID  the id of the test
	' Outputs:				integer - the Attempt ID of the Test/Quiz Attempt that was passed
	' ----------------------------------------------------------------------------------------------------------
	Function GetPassedTestAttempt(p_iUserTestID)
		dim sSQL 'as string
		dim rs
		
		sSQL = "SELECT AttemptID FROM UsersTestsAttempts WHERE UserTestID = '" & p_iUserTestID & "' AND Passed = 1"
		set rs = QueryDB(sSQL)
		
		GetPassedTestAttempt = rs("AttemptID")
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetPreviousLesson()
	' Purpose:				Retrieves the Previous lesson of a course 
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserCourseID the id of the current course in the UsersCourses table
	'						p_iUserLessonID  the id of the current lesson in the UsersLessons table
	' Outputs:				integer - the UserLessonID of a user's Previous lesson or 0 if none
	' ----------------------------------------------------------------------------------------------------------
	Function GetPreviousLesson(p_iUserCourseID,p_iUserLessonID)
		dim sSQL 'as string
		dim iSort 'as integer
		dim iCourseID 'as string
		dim rs 'as object
		
		'Get the CourseID and sort of the lesson
		sSQL = "SELECT L.CourseID, L.Sort FROM Lessons AS L " & _
			   "INNER JOIN UsersLessons AS UL ON (UL.LessonID = L.LessonID) " & _
			   "WHERE UL.ID = '" & p_iUserLessonID & "' "
		set rs = QueryDB(sSQL)
		
		iCourseID = rs("CourseID")
		iSort = rs("sort")
		
		'Get the Previous lesson, if any
		sSQL = "SELECT ID FROM UsersLessons AS UL " & _
			   "INNER JOIN Lessons AS L ON (L.LessonID = UL.LessonID) " & _
			   "WHERE UL.UserID = '" & iUserID & "' " & _
			   "AND UL.Course = '" & p_iUserCourseID & "' " & _
			   "AND L.CourseID = '" & iCourseID & "' AND L.Sort = '" & iSort - 1 & "' "		
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			GetPreviousLesson = rs("ID")
		else
			GetPreviousLesson = ""
		end if			   
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetSection()
	' Purpose:				Retrieves Section Info
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iSectionID  the id of the section
	' Outputs:				recordset of the section
	' ----------------------------------------------------------------------------------------------------------
	Function GetSection(p_iSectionID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM LessonSections " & _
			   "WHERE SectionID = '" & p_iSectionID & "' " 
		
		set GetSection = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetSections()
	' Purpose:				Retrieves the User's Section Info for the specified Lesson
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersCourses table
	' Outputs:				recordset of the User's Section Info for the specified Lesson
	' ----------------------------------------------------------------------------------------------------------
	Function GetSections(p_iUserLessonID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM LessonSections AS LS " & _
			   "INNER JOIN UsersLessons AS UL ON (UL.LessonID = LS.LessonID) " & _
			   "WHERE UserID = '" & iUserID & "' AND UL.ID = '" & p_iUserLessonID & "' " & _
			   "ORDER BY LS.Sort "
		
		set GetSections = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTestAnswer()
	' Purpose:				Retrieves the user's answer to the specified question, if any
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iAttemptID the id of the test attempt
	'						p_iQuestionID the id of the question
	' Outputs:				integer - the id of the user's answer or 0 if none
	' ----------------------------------------------------------------------------------------------------------
	Function GetTestAnswer(p_iAttemptID,p_iQuestionID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT AnswerID FROM UsersTestsAnswers AS UT " & _
			   "INNER JOIN UsersTestsAttempts AS UTA ON (UTA.AttemptID = UT.AttemptID) " & _
			   "WHERE UTA.UserID = '" & iUserID & "' " & _
			   "AND UT.AttemptID = '" & p_iAttemptID & "' " & _
			   "AND UT.QuestionID = '" & p_iQuestionID & "' " 
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			GetTestAnswer = rs("AnswerID")
		else
			GetTestAnswer = 0
		end if
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTestInfo()
	' Purpose:				Retrieves the Test Information for the specified lesson
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersCourses table
	' Outputs:				recordset of the Test information
	' ----------------------------------------------------------------------------------------------------------
	Function GetTestInfo(p_iUserLessonID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM UsersTestsInfo AS UTI " & _
			   "INNER JOIN Tests AS T ON (UTI.TestID = T.TestID) " & _
			   "WHERE UserID = '" & iUserID & "' AND UTI.Lesson = '" & p_iUserLessonID & "' AND T.TestType = 1"
	
		set  GetTestInfo = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTestInfoByID()
	' Purpose:				Retrieves the Test Information for the specified test
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserTestID - the id of the test in the UsersTestsInfo table
	' Outputs:				recordset of the Test information
	' ----------------------------------------------------------------------------------------------------------
	Function GetTestInfoByID(p_iUserTestID)
		dim sSQL 'as string
		
		sSQL = "SELECT * FROM UsersTestsInfo AS UTI " & _
			   "INNER JOIN Tests AS T ON (UTI.TestID = T.TestID) " & _
			   "WHERE UserID = '" & iUserID & "' AND UTI.ID = '" & p_iUserTestID & "' "
	
		set  GetTestInfoByID = QueryDB(sSQL)
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTestPosition()
	' Purpose:				Retrieves the Position of the first Question in a test page
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			p_iUserTestID - the id of the test in the UsersTestInfo table
	'						p_iPageNo - the page number
	' Outputs:				integer - the position of the first Question in a test page
	' ----------------------------------------------------------------------------------------------------------
	Function GetTestPosition(p_iUserTestID,p_iPageNo)
		dim sSQL 'as string
		dim rs 'as object
		dim iPageBreak 'as integer
		dim iTestID 'as integer
		dim iNoOfPages 'as integer
		dim iPosition 'as integer
		
		set rs = GetTestInfoByID(p_iUserTestID)
		iTestID = rs("TestID")

		sSQL = "SELECT * FROM TestsQuestions WHERE TestID = '" & iTestID & "' " & _
			   "ORDER BY Sort "
		set rs = QueryDB(sSQL)

		iNoOfPages = 1
		iPosition = 0

		do while (not rs.eof) and (cint(iNoOfPages) < cint(p_iPageNo))
			'if item is Page Break, increment Page count
			if (cint(rs("QuestionType")) = 2) then
				iNoOfPages = iNoOfPages + 1
			end if
			
			rs.MoveNext
			iPosition = iPosition + 1
		loop

		GetTestPosition = iPosition
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTests()
	' Purpose:				Retrieves the Test/Quizzes for the specified lesson
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserCourseID the id of the course in the UserCourses table
	'						p_iUserLessonID  the id of the lesson in the UsersLessons table
	'						p_iSectionID the section id (for quizzes) the quiz should display on
	' Outputs:				recordset of the Tests 
	' ----------------------------------------------------------------------------------------------------------
	Function GetTests(p_iUserCourseID,p_iUserLessonID,p_iSectionID)
		dim sSQL 'as string
		
		sSQL = "SELECT UTI.*,T.Name FROM UsersTestsInfo AS UTI " & _
			   "INNER JOIN Tests AS T ON (UTI.TestID = T.TestID) " & _
			   "INNER JOIN UsersLessons AS UL ON (UL.ID = UTI.Lesson) " & _
			   "WHERE UTI.UserID = '" & iUserID & "' " & _
			   "AND UL.Course = '" & p_iUserCourseID & "' "
		
		'if User Lesson ID set
		if trim(p_iUserLessonID) <> "" then
			sSQL = sSQL & "AND UTI.Lesson = '" & p_iUserLessonID & "' "
		end if
		
		'true for quizzes	   
		if trim(p_iSectionID) <> "" then
			sSQL = sSQL & "AND T.SectionID = '" & p_iSectionID & "' "
		end if
		
		sSQL = sSQL & "ORDER BY T.TestType, T.TestID "

		set GetTests = QueryDB(sSQL)
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTotalLessonTime()
	' Purpose:				Retrieves the Total Time a user has spent on a lesson
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersCourses table
	' Outputs:				long - the total time a user has spent on a lesson
	' ----------------------------------------------------------------------------------------------------------
	Function GetTotalLessonTime(p_iUserLessonID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT sum(Total) FROM TimeCourseTotals " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND Lesson = '" & p_iUserLessonID & "' "
		set rs = QueryDB(sSQL)
		
		if rs.eof then
			GetTotalLessonTime = 0
		else
			if not isnull(rs(0)) then
				GetTotalLessonTime = rs(0)
			else
				GetTotalLessonTime = 0
			end if
		end if
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTotalTestTime()	
	' Purpose:				Retrieves the Total Time a user has spent on a page
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iAttemptID the attempid of the attempt
	' Outputs:				long - the total time a user has spent on a test
	' ----------------------------------------------------------------------------------------------------------
	Function  GetTotalTestTime(p_iAttemptID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Sum(Total) FROM TimeTestTotals " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND AttemptID = '" & p_iAttemptID & "' " 
			   
		set rs = QueryDB(sSQL)
		
		if rs.eof then
			GetTotalTestTime = 0
		else
			if not isnull(rs(0)) then
				GetTotalTestTime = clng(rs(0))
			else
				GetTotalTestTime = clng(0)
			end if
		end if
		
		set rs = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTotalTestPageTime()	
	' Purpose:				Retrieves the Total Time a user has spent on a test page
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iAttemptID the attempid of the attempt
	'						p_iPageNo the page number of the test
	' Outputs:				long - the total time a user has spent on a test page
	' ----------------------------------------------------------------------------------------------------------
	Function GetTotalTestPageTime(p_iAttemptID,p_iPageNo)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Total FROM TimeTestTotals " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND AttemptID = '" & p_iAttemptID & "' " & _
			   "AND TestPageNo = '" & p_iPageNo & "' "
		set rs = QueryDB(sSQL)
		
		if rs.eof then
			GetTotalTestPageTime = clng(0)
		else
			GetTotalTestPageTime = clng(rs("Total"))
		end if
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTotalSectionTime()	
	' Purpose:				Retrieves the Total Time a user has spent on a section
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersLessons table
	' 						p_iSectionID the section id of the section
	' Outputs:				long - the total time a user has spent on a section
	' ----------------------------------------------------------------------------------------------------------
	Function GetTotalSectionTime(p_iUserLessonID,p_iSectionID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Total FROM TimeCourseTotals " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND Lesson = '" & p_iUserLessonID & "' " & _
			   "AND SectionID = '" & p_iSectionID & "' "
		set rs = QueryDB(sSQL)
		
		if rs.eof then
			GetTotalSectionTime = 0
		else
			GetTotalSectionTime = rs("Total")
		end if
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetUser()
	' Purpose:				Retrieves the User information
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' Outputs:				recordset of the User Info 
	' ----------------------------------------------------------------------------------------------------------
	Function GetUser()
		dim sSQL 'as string
		
		sSQL = "SELECT U.*, C.Name AS CompanyName, CA.AllowedToAddUsers FROM Users AS U " & _
			   "INNER JOIN UserStatusTypes AS UST ON (UST.StatusID = U.UserStatus) " & _
			   "LEFT OUTER JOIN Companies AS C ON (C.CompanyID = U.CompanyID) " & _
			   "LEFT OUTER JOIN CompanyBranches AS CB ON (CB.BranchID = U.BranchID) " & _
			   "LEFT OUTER JOIN CompanyAdmins AS CA ON (U.UserID = CA.UserID) " & _
			   "WHERE U.UserID = '" & iUserID & "' "

		set GetUser = QueryDB(sSQL)
	End Function	


	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCompanyL2IdList()
	' Purpose:				Retrieves the User's CompanyL2 associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' ----------------------------------------------------------------------------------------------------------
	Function GetCompanyL2IdList()
		dim sSQL 'as string
		
		sSql = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & iUserId & "'"
		'Response.Write(sSql)

		set GetCompanyL2IdList = QueryDB(sSQL)
	End Function		
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCompanyL2IdListString()
	' Purpose:				Retrieves the User's CompanyL2 associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' ----------------------------------------------------------------------------------------------------------
	Function GetCompanyL2IdListString()
		dim sSQL 'as string
		dim oRs 'as object
		
		GetCompanyL2IdListString = ""
		
		sSql = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & iUserId & "'"
		set oRs = QueryDb(sSql)
		
		do while not oRs.EOF
			GetCompanyL2IdListString = GetCompanyL2IdListString & oRs("CompanyL2Id") & ", "
			oRs.MoveNext
		loop
		if GetCompanyL2IdListString <> "" then
			GetCompanyL2IdListString = left(GetCompanyL2IdListString, len(GetCompanyL2IdListString) - 2)
		end if 		
	End Function	


	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCompanyL3IdList()
	' Purpose:				Retrieves the User's CompanyL3 associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' ----------------------------------------------------------------------------------------------------------
	Function GetCompanyL3IdList()
		dim sSQL 'as string
		
		sSql = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & iUserId & "'"
		'Response.Write(sSql)

		set GetCompanyL3IdList = QueryDB(sSQL)
	End Function		
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetCompanyL3IdListString()
	' Purpose:				Retrieves the User's CompanyL3 associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' ----------------------------------------------------------------------------------------------------------
	Function GetCompanyL3IdListString()
		dim sSQL 'as string
		dim oRs 'as object
		
		GetCompanyL3IdListString = ""
		
		sSql = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & iUserId & "'"
		set oRs = QueryDb(sSql)
		
		do while not oRs.EOF
			GetCompanyL3IdListString = GetCompanyL3IdListString & oRs("CompanyL3Id") & ", "
			oRs.MoveNext
		loop
		if GetCompanyL3IdListString <> "" then
			GetCompanyL3IdListString = left(GetCompanyL3IdListString, len(GetCompanyL3IdListString) - 2)
		end if 		
	End Function	
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetBranchIdList()
	' Purpose:				Retrieves the User's Branch associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' Outputs:				recordset of the User Info 
	' ----------------------------------------------------------------------------------------------------------
	Function GetBranchIdList()
		dim sSQL 'as string
		
		sSql = "SELECT BranchId FROM UsersBranches WHERE UserId = '" & iUserId & "'"
		'Response.Write(sSql)

		set GetBranchIdList = QueryDB(sSQL)
	End Function		
	
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetBranchIdListString()
	' Purpose:				Retrieves the User's Branch associations
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	
	' Parameters:			n/a
	' Outputs:				recordset of the User Info 
	' ----------------------------------------------------------------------------------------------------------
	Function GetBranchIdListString()
		dim sSQL 'as string
		dim oRs 'as object
		
		GetBranchIdListString = ""
		
		sSql = "SELECT BranchId FROM UsersBranches WHERE UserId = '" & iUserId & "'"
		set oRs = QueryDb(sSql)
		
		do while not oRs.EOF
			GetBranchIdListString = GetBranchIdListString & oRs("BranchId") & ", "
			oRs.MoveNext
		loop
		if GetBranchIdListString <> "" then
			GetBranchIdListString = left(GetBranchIdListString, len(GetBranchIdListString) - 2)
		end if 
		
		
		
	End Function		
	

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				HasAdminCourseActions
	' Purpose: 				determines whether or not a Course has had actions performed by an Admin
	' Required Properties:  ConnectionString
	' Optional Properties:
	' Parameters:			p_iUserCourseID the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the user has courses, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function HasAdminCourseActions(p_iUserCourseID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT count(*) FROM TrackAdminCourseActions WHERE Course = '" & p_iUserCourseID & "' "
		set rs = QueryDB(sSQL)
		
		if cint(rs(0)) > 0 then
			HasAdminCourseActions = true
		else
			HasAdminCourseActions = false	
		end if
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				HasCourses
	' Purpose: 				determines whether or not a User currently has Courses
	' Required Properties:  ConnectionString
	' Optional Properties:
	' Parameters:			p_iUserID - User ID
	' Outputs:				boolean - true if the user has courses, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function HasCourses(p_iUserID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT count(*) FROM UsersCourses WHERE UserID = '" & p_iUserID & "' AND CourseStatus <> 3 "
		set rs = QueryDB(sSQL)
		
		if cint(rs(0)) > 0 then
			HasCourses = true
		else
			HasCourses = false	
		end if
		
		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				HasCrossCertifications
	' Purpose: 				determines whether or not a course has cross certifications
	' Required Properties:  ConnectionString
	' Optional Properties:
	' Parameters:			p_iUserID - User ID
	'					    p_iID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the user has courses, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function HasCrossCertifications(p_iUserID, p_iID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT count(*) FROM UsersCourses WHERE UserID = '" & p_iUserID & "' AND CrossCertifiedCoreCourse = '" & p_iID & "' "
		set rs = QueryDB(sSQL)
		
		if cint(rs(0)) > 0 then
			HasCrossCertifications = true
		else
			HasCrossCertifications = false	
		end if
		
		set rs = nothing
	End Function	

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				InsertTransactionInfo
	' Purpose: 				inserts a Transaction record into the database
	' Required Properties:  ConnectionString,iUserID
	' Optional Properties:  iCompanyID
	' Parameters:			p_fTotalPrice - the total price of the transaction
	'						p_fShippingCost - the shipping cost, if any
	'						p_fCouponDiscount - the coupon discount, if any
	'						p_fMemberDiscount - the member discount, if any
	'					    p_iPaid - whether or not the transaction was paid
	' Outputs:				integer - the id of the transaction if successful, -1 if not
	' ----------------------------------------------------------------------------------------------------------	
	Function InsertTransactionInfo(p_fTotalPrice,p_fShippingCost,p_fCouponDiscount,p_fMemberDiscount,p_iPaid)
		dim objConn 'as object
		dim sSQL 'as string

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		if (p_fTotalPrice = "") or (isnull(p_fTotalPrice)) then
			p_fTotalPrice = 0
		end if

		if (p_fShippingCost = "") or (isnull(p_fShippingCost)) then
			p_fShippingCost = 0
		end if

		if (p_fCouponDiscount = "") or (isnull(p_fCouponDiscount)) then
			p_fCouponDiscount = 0
		end if
		
		if (p_fMemberDiscount = "") or (isnull(p_fMemberDiscount)) then
			p_fMemberDiscount = 0
		end if
		
		'add new value
		sSQL = "INSERT INTO Transactions(UserID,CompanyID,TotalPrice,ShippingCost,CouponDiscount,MemberDiscount,Paid) " & _
			   "VALUES ('" & iUserID & "','" & iCompanyID & "'," & p_fTotalPrice & "," & p_fShippingCost & "," & p_fCouponDiscount & "," & p_fMemberDiscount & ",'" & p_iPaid & "') "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			sSQL = "SELECT MAX(TransactionID) FROM Transactions " & _
				   "WHERE UserID = '" & iUserID & "' "
			set rs = QueryDB(sSQL)
			
			InsertTransactionInfo = rs(0)
		else
			InsertTransactionInfo = -1
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				InsertTransactionCourse
	' Purpose: 				inserts a Transaction Course record into the database
	' Required Properties:  ConnectionString
	' Optional Properties:  
	' Parameters:			p_iTransactionID - the ID of the transaction
	'						p_iCourseID - the id of the course
	'					    p_sCourseName - the name of the course
	'						p_fPrice - the purchase price of the course
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function InsertTransactionCourse(p_iTransactionID,p_iCourseID,p_sCourseName,p_fPrice)
		dim objConn 'as object
		dim sSQL 'as string

		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'add new value
		sSQL = "INSERT INTO TransactionsCoursesX(TransactionID,CourseID,CourseName,Price) " & _
			   "VALUES ('" & p_iTransactionID & "','" & p_iCourseID & "','" & p_sCourseName & "'," & p_fPrice & ") "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			InsertTransactionCourse = true
		else
			InsertTransactionCourse = false
		end if
		
		set objConn = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				IsCorporateUser()
	' Purpose:				checks to see if a user is a corporate user
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			
	' Outputs:				boolean - true if the user is a corporate user, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function IsCorporateUser()
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT CompanyID FROM Users " & _
			   "WHERE UserID = '" & iUserID & "' "
		set rs = QueryDB(sSQL)
		
		if (trim(rs("CompanyID")) = "" or isnull(rs("CompanyID"))) then
			IsCorporateUser = false
		else
			IsCorporateUser = true
		end if
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				IsProctorDisclaimerSet()
	' Purpose:				checks to see if the ProctorDisclaimer flag is set for the specified course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the course is a supplement, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function IsProctorDisclaimerSet(p_iUserCourseID)
		dim sSQL 'as string
		dim rs 'as object

		sSQL = "SELECT count(*) FROM UsersCourses " & _ 
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND ID = '" & p_iUserCourseID & "' " & _
			   "AND ProctorDisclaimer = 1 "

		set rs = QueryDb(sSQL)

		if cint(rs(0)) > 0 then
			IsProctorDisclaimerSet = true
		else
			IsProctorDisclaimerSet = false
		end if

		set rs = nothing	
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				IsSupplementCourse()
	' Purpose:				checks to see if a course is a Supplement for another course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if the course is a supplement, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function IsSupplementCourse(p_iUserCourseID)
		dim sSQL 'as string
		dim rs 'as object

		sSQL = "SELECT count(*) FROM UsersCourses " & _ 
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND CrossCertified = 1 " & _
			   "AND SupplementCourseID = '" & p_iUserCourseID & "' "

		set rs = QueryDb(sSQL)

		if cint(rs(0)) > 0 then
			IsSupplementCourse = true
		else
			IsSupplementCourse = false
		end if

		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				IsTestPassed()
	' Purpose:				checks to see if a test has been completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID  the id of the test in the UsersTestsInfo table
	' Outputs:				boolean - true if the test has been completed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function IsTestPassed(p_iUserTestID)	
		dim sSQL 'as string
		dim rs 'as object

		sSQL = "SELECT count(*) FROM UsersTestsInfo " & _
			   "WHERE ID = '" & p_iUserTestID & "' AND Passed = 1"

		set rs = QueryDb(sSQL)

		if cint(rs(0)) > 0 then
			IsTestPassed = true
		else
			IsTestPassed = false
		end if

		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				LessonCompleted()
	' Purpose:				checks to see if a Lesson has been completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersLessons table
	' Outputs:				boolean - true if the lesson has been completed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function LessonCompleted(p_iUserLessonID)
		dim sSQL 'as string
		dim rs 'as object
		dim iTimeType 'as integer
		dim iDuration 'as integer
		dim iLessonID 'as integer
		dim iNoOfSections, iNoOfHitSections 'as integer
		
		'determine if the Lesson is timed		
		sSQL = "SELECT L.LessonID, L.TimeType, L.TimedMinutes FROM Lessons AS L " & _
			   "INNER JOIN UsersLessons AS UL ON (L.LessonID = UL.LessonID) " & _
			   "WHERE UL.UserID = '" & iUserID & "' AND UL.ID = '" & p_iUserLessonID & "' "
		set rs = QueryDB(sSQL)
		
		iLessonID = rs("LessonID")
		iTimeType = rs("TimeType")
		iDuration = rs("TimedMinutes") * 60 'convert to seconds
		
		'Make sure all Lesson Sections have been hit once
		
		'get # of sections
		sSQL = "SELECT count(*) FROM LessonSections " & _
			   "WHERE LessonID = '" & iLessonID & "' "
		set rs = QueryDB(sSQL)
		iNoOfSections = rs(0)
		
		'Get the # of sections that have been hit
		sSQL = "SELECT COUNT(DISTINCT SectionID) FROM TimeCourseHits " & _
			   "WHERE UserID = '" & iUserID & "' AND Lesson = '" & p_iUserLessonID & "' "
		set rs = QueryDB(sSQL)
		iNoOfHitSections = rs(0)
		
		if cint(iNoOfSections) = cint(iNoOfHitSections) then
			'if untimed, then the lesson is completed
			if cint(iTimeType) = 1 then
				LessonCompleted = true
			else
				'timed lesson, make sure the time reqs have been met
			
				if cint(iTimeType) = 2 then 'Per Page
					'Get the number of sections that have passed the time requirement
					sSQL = "SELECT COUNT(*) FROM TimeCourseTotals " & _
						   "WHERE UserID = '" & iUserID & "' AND Lesson = '" & p_iUserLessonID & "' " & _
						   "AND Total >= '" & iDuration & "' "

					set rs = QueryDB(sSQL)
					
					'if all section have passed the requirement, then lesson passed
					if (cint(rs(0)) = cint(iNoOfSections)) then
						LessonCompleted = true
					else
						LessonCompleted = false
					end if
				elseif cint(iTimeType = 3) then ' Total Lesson
					'Get the number of sections that have passed the time requirement
					sSQL = "SELECT SUM(total) FROM TimeCourseTotals " & _
						   "WHERE UserID = '" & iUserID & "' AND Lesson = '" & p_iUserLessonID & "' "						  

					set rs = QueryDB(sSQL)
					
					if not isnull(rs(0)) then
						if (clng(rs(0)) >= clng(iDuration)) then
							LessonCompleted = true
						else
							LessonCompleted = false
						end if
					else
						LessonCompleted = false					
					end if
				end if
			end if
		else 
			LessonCompleted = false
		end if

		set rs =  nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				LoginExists()
	' Purpose:				determines if a login is in the system
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_sLogin
	' Outputs:				boolean - true if the login exists, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function LoginExists(p_sLogin)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Count(*) FROM Users AS U " & _
			   "INNER JOIN UserStatusTypes AS UST ON (UST.StatusID = U.UserStatus) " & _
			   "WHERE U.Username = '" & p_sLogin & "' " & _
			   "AND NOT U.UserStatus = '3' "
			   

		set rs = QueryDB(sSQL)

		if cint(rs(0)) > 0 then
			LoginExists = true
		else
			LoginExists = false
		end if		

		set rs = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SetCourseToCompleted()
	' Purpose:				sets the specified course to completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	'						p_sCompletionDate the completion date
	' Outputs:				boolean - true if it was successfully set, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function SetCourseToCompleted(p_iUserCourseID,p_sCompletionDate)
		dim objConn 'as object
		dim sSQL 'as string
		dim iCrossCertType 'as integer
		dim iCertificateReleaseType 'as integer
		dim bSuccess 'as boolean
		dim iUserCourseID 'as integer
		dim rs 'as object
		dim bSupplement 'as boolean
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'determine if this was a Supplement Course
		bSupplement = IsSupplementCourse(p_iUserCourseID)
		
		if bSupplement then
			'set Supplement to inactive
			sSQL = "UPDATE UsersCourses " & _
				   "SET Completed = 1, " & _
				   "	CompletionDate = '" & p_sCompletionDate & "', " & _
				   "	CourseStatus = 2 " & _
				   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserCourseID & "' "

			objConn.execute(sSQL)
		
			if err.number = 0 then
				'retrieve all the courses that have this course as a supplement
				sSQL = "SELECT * FROM UsersCourses " & _ 
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND CrossCertified = 1 " & _
					   "AND SupplementCourseID = '" & p_iUserCourseID & "' "
				set rs = QueryDB(sSQL)			
			
				if not rs.eof then
					'if there are courses, set those course to completed since the supplement is now completed
					do while not rs.eof
						iUserCourseID = rs("ID")
'						sSQL = "UPDATE UsersCourses " & _		
'							   "SET Completed = 1, " & _
'							   "	CompletionDate = '" & now() & "' " & _
'							   "WHERE UserID = '" & iUserID & "' AND ID = '" & rs("ID") & "' " 
'						objConn.execute(sSQL)
						
'						if err.number <> 0 then
'							SetCourseToCompleted = false
'						end if

						bSuccess = SetCourseToCompleted(iUserCourseID,p_sCompletionDate)
						
						if not bSuccess then
							SetCourseToCompleted = false
						end if

						rs.MoveNext
					loop
				end if
			else
				SetCourseToCompleted = false
			end if
		else	
			sSQL = "UPDATE UsersCourses " & _
				   "SET Completed = 1, " & _
				   "	CompletionDate = '" & p_sCompletionDate & "' " & _
				   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserCourseID & "' "
			   
			objConn.execute(sSQL)
		
			if err.number = 0 then
				'check to see if the Certificate should be released
				set rs = GetCertificate(p_iUserCourseID)
			
				if not rs.eof then
					iCertificateReleaseType = rs("ReleaseType")
					'if CertificateReleaseType = 2 (On Course Completion), set CertificateReleased
					if cint(iCertificateReleaseType) = 2 then
						sSQL = "UPDATE UsersCourses " & _
							   "SET CertificateReleased = 1 " & _
							   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserCourseID & "' "

						objConn.execute(sSQL)
					end if
				end if

				'check to see if the Course has Cross Certs that do not require a Supplement
				sSQL = "SELECT * FROM UsersCourses " & _ 
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND CrossCertified = 1 " & _
					   "AND CrossCertifiedCoreCourse = '" & p_iUserCourseID & "' " & _
					   "AND (SupplementCourseID = '' or SupplementCourseID is null) "
				set rs = QueryDB(sSQL)
			
				if not rs.eof then
					do while not rs.eof
						iCrossCertType = rs("CrossCertType")
					
						'if Certificate cross cert type, set cross cert to complete
						if cint(iCrossCertType) = 1 then
							iUserCourseID = rs("ID")
'							sSQL = "UPDATE UsersCourses " & _		
'								   "SET Completed = 1, " & _
'								   "	CompletionDate = '" & now() & "' " & _
'								   "WHERE UserID = '" & iUserID & "' AND ID = '" & rs("ID") & "' " 
'							objConn.execute(sSQL)
							
'							if err.number <> 0 then
'								SetCourseToCompleted = false
'							end if

							bSuccess = SetCourseToCompleted(iUserCourseID,p_sCompletionDate)
						
							if not bSuccess then
								SetCourseToCompleted = false
							end if
						end if
					
						rs.MoveNext
					loop
				end if
			else
				SetCourseToCompleted = false
			end if
		end if
		
		SetCourseToCompleted = true
		
		set rs = nothing
		set objConn = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SetDisclaimerFlag()
	' Purpose:				sets the disclaimer flag for the specified course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	' Outputs:				boolean - true if it was successfully set, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function SetDisclaimerFlag(p_iUserCourseID)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE UsersCourses " & _
			   "SET ProctorDisclaimer = 1 " & _
			   "WHERE ID = '" & p_iUserCourseID & "' AND UserID = '" & iUserID & "' "
   
		objConn.execute(sSQL)

		if err.number = 0 then
			SetDisclaimerFlag = true
		else	
			SetDisclaimerFlag = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SetLessonToCompleted()
	' Purpose:				sets the specified lesson to completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserLessonID  the id of the lesson in the UsersLessons table
	' Outputs:				boolean - true if it was successfully set, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function SetLessonToCompleted(p_iUserLessonID)	
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE UsersLessons " & _
			   "SET Completed = 1, " & _
			   "	CompletionDate = '" & now() & "' " & _
			   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserLessonID & "' "
			   
		objConn.execute(sSQL)
		
		if err.number = 0 then
			SetLessonToCompleted = true
		else
			SetLessonToCompleted = false
		end if
		
		set objConn = nothing
	End Function
	
	
	' -------------------------------------------------------------------------
	' Name: SetAgreementSigned
	' Purpose: Marks the EULA as having been signed, and records the date.
	' Prerequisites: ConnectionString, UserId
	' Returns: Boolean - true if successful
	' -------------------------------------------------------------------------
	function SetAgreementSigned()

		SetAgreementSigned = false
		
		if iUserId = "" then
			exit function
		end if 
		
		dim sSql 'SQL
		dim lRecordsAffected
		dim oConn
	
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = application("sDataSourceName")
		oConn.Open
		
		sSql = "UPDATE Users SET " & _
			   "AcceptedAgreement = '1', " & _
			   "AgreementDate = '" & Now() & "' " & _
			   "WHERE UserId = '" & iUserId & "'"
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected = 1 then
		
			SetAgreementSigned = true
		
		end if
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: IsAgreementSigned
	' Purpose: Verifies that an EULA has been accepted by the user.
	' Prerequisites: ConnectionString, UserId
	' Returns: Boolean, true if successful
	' -------------------------------------------------------------------------
	function IsAgreementSigned()
	
		IsAgreementSigned = false
		
		if iUserId = "" then
			exit function
		end if
		
		dim sSql
		dim oConn
		dim oRs
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = application("sDataSourceName")
		oConn.Open
			
		sSql = "SELECT AcceptedAgreement FROM Users " & _
			"WHERE UserId = '" & iUserId & "'"
		set oRs = oConn.Execute(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			IsAgreementSigned = oRs("AcceptedAgreement")
		
		end if 
	
	end function
	

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				SetTestToCompleted()
	' Purpose:				sets a test to completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID - the id of the test in the UsersTestsInfo table
	'						p_iAttemptID - the attempt id
	'						p_bPassed - whether or not the attempt was successful
	' Outputs:				boolean - true if successful, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function SetTestToCompleted(p_iUserTestID, p_iAttemptID,p_bPassed)
		dim sSQL 'as string
		dim objConn 'as object
		dim iAttemptsLeft 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "UPDATE UsersTestsAttempts " & _
			   "SET Completed = 1, " & _
			   "	CompletionDate ='" & now() & "' "			   
		
		'if passed
		if p_bPassed then
			sSQL = sSQL & ", Passed = 1 "
		end if
		
		sSQL = sSQL & "WHERE AttemptID = '" & p_iAttemptID & "' "		
	
		objConn.execute(sSQL)

		if err.number = 0 then
			'if passed
			if p_bPassed then			
				sSQL = "UPDATE UsersTestsInfo " & _
					   "SET Passed = 1, " & _
				   	   "PassingDate ='" & now() & "' " & _
					   "WHERE ID = '" & p_iUserTestID & "' "

				objConn.execute(sSQL)
			end if

			'deduct 1 from the AttemptsLeft and pass attemp if necessary, if not set to unlimited(null)
			sSQL = "SELECT AttemptsLeft FROM UsersTestsInfo WHERE ID = '" & p_iUserTestID & "' " 
			set rs = QueryDB(sSQL)
			iAttemptsLeft = rs("AttemptsLeft")
			
			if not isnull(iAttemptsLeft) then
				iAttemptsLeft = iAttemptsLeft - 1
				
				'Update Attempts Left field
				sSQL = "UPDATE UsersTestsInfo " & _
					   "SET AttemptsLeft = '" & iAttemptsLeft & "' " & _
					   "WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserTestID & "' " 
				objConn.execute(sSQL)
			end if
					
			if err.number = 0 then
				SetTestToCompleted = True
			else
				SetTestToCompleted = false
			end if
		else
			SetTestToCompleted = false
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TestCompleted()
	' Purpose:				checks to see if a Test has been completed
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID  the id of the test in the UsersTestsInfotable
	' Outputs:				boolean - true if the test has been completed, false if not
	' ----------------------------------------------------------------------------------------------------------	
	Function TestCompleted(p_iUserTestID)
		dim sSQL 'as string
		dim rs 'as object
		
		sSQL = "SELECT Passed FROM UsersTestsInfo WHERE UserID = '" & iUserID & "' AND ID = '" & p_iUserTestID & "' "
		set rs = QueryDB(sSQL)
		
		TestCompleted = rs("Passed")
		
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TrackAdminCourseAction()
	' Purpose:				tracks the admin actions on a course
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			p_iAdminUserID the Admin's UserID
	'						p_iUserCourseID  the id of the course in the UsersCourses table
	' 						p_sAction the action the user performed
	' Outputs:				boolean - true if tracked successfully, false if not
	' ---------------------------------------------------------------------------------------------------------	
	Function TrackAdminCourseAction(p_iAdminUserID,p_iUserCourseID,p_sAction)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "INSERT INTO TrackAdminCourseActions(AdminUserID,Course,CourseAction) " & _
			   "VALUES ('" & p_iAdminUserID & "','" & p_iUserCourseID & "','" & p_sAction & "')"
		objConn.execute(sSQL)
		
		if err.number = 0 then
			TrackAdminCourseAction = true
		else
			TrackAdminCourseAction = false		
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TrackCourseAction()
	' Purpose:				tracks the user action on a course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserCourseID  the id of the course in the UsersCourses table
	' 						p_sAction the action the user performed
	' Outputs:				boolean - true if tracked successfully, false if not
	' ---------------------------------------------------------------------------------------------------------	
	Function TrackCourseAction(p_iUserCourseID,p_sAction)
		dim objConn 'as object
		dim sSQL 'as string
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		sSQL = "INSERT INTO TrackCourseActions(UserID,Course,CourseAction) " & _
			   "VALUES ('" & iUserID & "','" & p_iUserCourseID & "','" & p_sAction & "')"
		objConn.execute(sSQL)
		
		if err.number = 0 then
			TrackCourseAction = true
		else
			TrackCourseAction = false		
		end if
		
		set objConn = nothing
	End Function
	
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TrackCoursePageHit()
	' Purpose:				tracks the user viewing a section of a course
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserLessonID the id of the lesson in the UsersLessons table
	' 						p_iSectionID  the section id
	'						p_CalcPreviousCourseHit whether or not the previous hit should be calculated	
	' Outputs:				boolean - true if tracked successfully, false if not
	' ---------------------------------------------------------------------------------------------------------	
	Function TrackCoursePageHit(p_iUserLessonID,p_iSectionID,p_CalcPreviousCourseHit)
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		dim iHitID 'as integer
		dim iPreviousSectionID 'as integer
		dim sCurrentHitDate, sPreviousHitDate 'as string
		dim lSecDiff 'as long
		dim lNewTotal 'as long
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'set the Last Activity date for the Entire Lesson
		sSQL = "UPDATE UsersLessons " & _
			   "SET LastActivityDate = '" & now() & "' " & _
			   "WHERE ID = '" & p_iUserLessonID & "' "
		objConn.execute(sSQL)
		
		'Track hit
		sSQL = "INSERT INTO TimeCourseHits (UserID,Lesson,SectionID) " & _
			   "VALUES ('" & iUserID & "','" & p_iUserLessonID & "','" & p_iSectionID & "')"
			   
		objConn.execute(sSQL)

		if p_CalcPreviousCourseHit then				
			'Get the HitID for this record
			sSQL = "SELECT Max(HitID) FROM TimeCourseHits " & _
				   "WHERE UserID = '" & iUserID & "' " & _
				   "AND Lesson = '" & p_iUserLessonID & "' " & _
				   "AND SectionID = '" & p_iSectionID & "' "		
			set rs = QueryDB(sSQL)
			iHitID = rs(0)

			if err.number <> 0 then
				set objConn = nothing
				set rs = nothing
				TrackCoursePageHit = false
				Exit Function
			end if 
		
			'Get the SectionID and Hit Date for the previous course hit
			sSQL = "SELECT SectionID, HitDate FROM TimeCourseHits " & _
				   "WHERE UserID = '" & iUserID & "' " & _
				   "AND Lesson = '" & p_iUserLessonID & "' " & _
				   "AND HitID < '" & iHitID & "' " & _
				   "ORDER BY HitID desc "
			set rs = QueryDB(sSQL)
		
			'if no value, then this was the first course hit
			if rs.eof then
				'don't do anything
				TrackCoursePageHit = true
			else
				iPreviousSectionID = rs("SectionID")
				sPreviousHitDate = rs("HitDate")
			
				'Get the hit date for the current section
				sSQL = "SELECT HitDate FROM TimeCourseHits " & _
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND Lesson = '" & p_iUserLessonID & "' " & _
					   "AND HitID = '" & iHitID & "' " 
				set rs = QueryDB(sSQL)
			
				sCurrentHitDate = rs("HitDate")
			
				'Determine the time difference between the two dates
				lSecDiff = DateDiff("s", sPreviousHitDate, sCurrentHitDate) 
			
				'the max difference is 10 minutes (60 * 10 = 600)
				if clng(lSecDiff) > 600 then
					lSecDiff = 600
				end if
			
				'insert or update the Total time for the previous section

				'Get the current total time if any
				sSQL = "SELECT Total FROM TimeCourseTotals " & _
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND Lesson = '" & p_iUserLessonID & "' " & _
					   "AND SectionID = '" & iPreviousSectionID & "' "
				set rs = QueryDB(sSQL)
	
				if rs.eof then
					'no record, insert new record
					sSQL = "INSERT INTO TimeCourseTotals (UserID,Lesson,SectionID,Total) " & _
						   "VALUES ('" & iUserID & "','" & p_iUserLessonID & "','" & iPreviousSectionID & "','" & lSecDiff & "')"
				else
					'update existing record
					lNewTotal = clng(rs("Total")) + clng(lSecDiff)
				
					sSQL = "UPDATE TimeCourseTotals " & _
						   "Set Total = '" & lNewTotal & "' " & _
						   "WHERE UserID = '" & iUserID & "' " & _
						   "AND Lesson = '" & p_iUserLessonID & "' " & _
						   "AND SectionID = '" & iPreviousSectionID & "' "
				end if
			
				objConn.execute(sSQL)
			
				if err.number = 0 then
					TrackCoursePageHit = true
				else
					TrackCoursePageHit = False
				end if
			end if
		else
			TrackCoursePageHit = true
		end if
		
		set objConn = nothing
		set rs = nothing
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TrackTestHit()
	' Purpose:				tracks the user submission of a particular test/quiz page
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID the id of the test in the UsersTestsInfo table
	'						p_iAttemptID the id of the test attempt
	' 						p_iPageNo the page number of the test/quiz that got the hit
	'						p_CalcPreviousTestHit whether or not the previous hit should be calculated
	' Outputs:				boolean - true if tracked successfully, false if not
	' ---------------------------------------------------------------------------------------------------------		
	Function TrackTestHit(p_iUserTestID,p_iAttemptID,p_iPageNo,p_CalcPreviousTestHit)
		dim objConn 'as object
		dim sSQL 'as string
		dim rs 'as object
		dim iUserTestID, iTestID 'as integer
		dim iHitID 'as integer
		dim iPreviousTestPageNo 'as integer
		dim sCurrentHitDate, sPreviousHitDate 'as string
		dim lSecDiff 'as long
		dim lNewTotal 'as long
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		'get the UserLessonID and TestID of the test
		sSQL = "SELECT Lesson, TestID FROM UsersTestsInfo " & _
			   "WHERE UserID = '" & iUserID & "' " & _
			   "AND ID = '" & p_iUserTestID & "' "
		set rs = QueryDB(sSQL)
		
		if not rs.eof then
			iUserLessonID = rs("Lesson")
			iTestID = rs("TestID")


			'set the Last Test Page submitted for the Entire Test
			sSQL = "UPDATE UsersTestsAttempts " & _
				   "SET LastTestPage = '" & p_iPageNo & "' " & _
				   "WHERE UserID = '" & iUserID & "' " & _
				   "AND AttemptID = '" & p_iAttemptID & "' " & _
				   "AND UserTestID = '" & p_iUserTestID & "' "
			objConn.execute(sSQL)
			
			sSQL = "INSERT INTO TimeTestHits(UserID,AttemptID,TestID,Lesson,TestPageNo) " & _
				   "VALUES ('" & iUserID & "','" & p_iAttemptID & "','" & iTestID & "','" & iUserLessonID & "','" & p_iPageNo & "') "
			objConn.execute(sSQL)
	
			if p_CalcPreviousTestHit then			
				'Get the HitID for this record
				sSQL = "SELECT Max(HitID) FROM TimeTestHits " & _
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND AttemptID = '" & p_iAttemptID & "' " & _
					   "AND Lesson = '" & iUserLessonID & "' " & _
					   "AND TestID = '" & iTestID & "' " & _
					   "AND TestPageNo = '" & p_iPageNo & "' "
				set rs = QueryDB(sSQL)
				iHitID = rs(0)
			
				'Get the PageNo and Hit Date for the previous course hit
				sSQL = "SELECT TestPageNo, HitDate FROM TimeTestHits " & _
					   "WHERE UserID = '" & iUserID & "' " & _
					   "AND AttemptID = '" & p_iAttemptID & "' " & _				   
					   "AND Lesson = '" & iUserLessonID & "' " & _
					   "AND TestID = '" & iTestID & "' " & _
					   "AND HitID < '" & iHitID & "' " & _
					   "ORDER BY HitID desc "
				set rs = QueryDB(sSQL)
		
				'if no value, then this was the first course hit
				if rs.eof then
					'don't do anything
					TrackTestHit = true
				else
					iPreviousTestPageNo = rs("TestPageNo")
					sPreviousHitDate = rs("HitDate")

					'Get the hit date for the current section
					sSQL = "SELECT HitDate FROM TimeTestHits " & _
						   "WHERE UserID = '" & iUserID & "' " & _
						   "AND HitID = '" & iHitID & "' " 
					set rs = QueryDB(sSQL)
			
					sCurrentHitDate = rs("HitDate")
			
					'Determine the time difference between the two dates
					lSecDiff = DateDiff("s", sPreviousHitDate, sCurrentHitDate) 
			
					'the max difference is 10 minutes (60 * 10 = 600)
					if clng(lSecDiff) > 600 then
						lSecDiff = 600
					end if
			
					'insert or update the Total time for the previous section

					'Get the current total time if any
					sSQL = "SELECT Total FROM TimeTestTotals " & _
						   "WHERE UserID = '" & iUserID & "' " & _
						   "AND AttemptID = '" & p_iAttemptID & "' " & _					   
						   "AND Lesson = '" & iUserLessonID & "' " & _
						   "AND TestID = '" & iTestID & "' " & _
						   "AND TestPageNo = '" & iPreviousTestPageNo & "' "
					set rs = QueryDB(sSQL)
			
					if rs.eof then
						'no record, insert new record
						sSQL = "INSERT INTO TimeTestTotals (UserID,AttemptID,TestID,Lesson,TestPageNo,Total) " & _
							   "VALUES ('" & iUserID & "','" & p_iAttemptID & "','" & iTestID & "','" & iUserLessonID & "','" & iPreviousTestPageNo & "','" & lSecDiff & "')"
					else
						'update existing record
						lNewTotal = clng(rs("Total")) + clng(lSecDiff)
			
						sSQL = "UPDATE TimeTestTotals " & _
							   "SET Total = '" & lNewTotal & "' " & _
							   "WHERE UserID = '" & iUserID & "' " & _
							   "AND AttemptID = '" & p_iAttemptID & "' " & _						   
							   "AND Lesson = '" & iUserLessonID & "' " & _
							   "AND TestID = '" & iTestID & "' " & _
							   "AND TestPageNo = '" & iPreviousTestPageNo & "' " 
					end if
			
					objConn.execute(sSQL)
			
					if err.number = 0 then
						TrackTestHit = true
					else
						TrackTestHit = False
					end if
				end if
			else
				TrackTestHit = true
			end if
		else
			TrackTestHit = false		
		end if
			
		set objConn = nothing
		set rs = nothing	
	End Function

	' ----------------------------------------------------------------------------------------------------------
	' Name: 				TrackTestVerification()
	' Purpose:				tracks the test verification step
	' Required Properties:  ConnectionString, iUserID
	' Optional Properties:	n/a
	' Parameters:			p_iUserTestID the id of the test in the UsersTestsInfo table
	'						p_iVerificationQuestionID the id of the verification that was asked
	' 						p_bSuccess whether or not the question was answered successfully
	' Outputs:				boolean - true if tracked successfully, false if not
	' ---------------------------------------------------------------------------------------------------------		
	Function TrackTestVerification(p_iUserTestID, p_iVerificationQuestionID, p_bSuccess)
		dim objConn 'as object
		dim sSQL 'as string
		dim iAnsweredCorrect 'as integer
		
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open
		
		if p_bSuccess then
			iAnsweredCorrect = 1
		else
			iAnsweredCorrect = 0
		end if
		
		sSQL = "INSERT INTO VerificationTracking(UserID,UserTestID,VerificationQuestionID,AnsweredCorrect) " & _
			   "VALUES('" & iUserID & "','" & p_iUserTestID & "','" & p_iVerificationQuestionID & "','" & iAnsweredCorrect & "') "
		objConn.execute(sSQL)
		
		if err.number = 0 then
			TrackTestVerification = true
		else
			TrackTestVerification = false
		end if
		
		set objConn = nothing
	End Function
	
	' PROPERTY DEFINITION ======================================================================================
	' No Properties Defined
	Public Property Let Address(p_sAddress)
		sAddress = p_sAddress
	End Property
	
	Public Property Get Address()
		Address = sAddress
	End Property

	Public Property Let AllowedToAddUsers(p_iAllowedToAddUsers)
		iAllowedToAddUsers = p_iAllowedToAddUsers
	End Property
	
	Public Property Get AllowedToAddUsers()
		AllowedToAddUsers = iAllowedToAddUsers
	End Property
	
	Public Property Let BranchID(p_iBranchID)
		iBranchID = p_iBranchID
	End Property
	
	Public Property Get BranchID()
		BranchID = iBranchID
	End Property
	
	
	public property get CompanyL2IdList()
		CompanyL2IdList = sCompanyL2IdList
	end property
	public property let CompanyL2IdList(p_sCompanyL2IdList)
		sCompanyL2IdList = p_sCompanyL2IdList
	end property

	public property get CompanyL3IdList()
		CompanyL3IdList = sCompanyL3IdList
	end property
	public property let CompanyL3IdList(p_sCompanyL3IdList)
		sCompanyL3IdList = p_sCompanyL3IdList
	end property
	
	public property get BranchIdList()
		BranchIdList = sBranchIdList
	end property
	public property let BranchIdList(p_sBranchIdList)
		sBranchIdList = p_sBranchIdList
	end property
	
	
	Public Property Let ProviderID(p_iProviderID)
		iProviderID = p_iProviderID
	End Property
	
	Public Property Get ProviderID()
		ProviderID = iProviderID
	End Property
	
	Public Property Let CConAlertEmails(p_iCConAlertEmails)
		iCConAlertEmails = p_iCConAlertEmails
	End Property
	
	Public Property Get CConAlertEmails()
		CConAlertEmails = iCConAlertEmails
	End Property
	
	Public Property Let CertCompanyName(p_sCertCompanyName)
		sCertCompanyName = p_sCertCompanyName
	End Property
	
	Public Property Get CertCompanyName()
		CertCompanyName = sCertCompanyName
	End Property

	Public Property Let City(p_sCity)
		sCity = p_sCity
	End Property
	
	Public Property Get City()
		City = sCity
	End Property

	Public Property Let CompanyID(p_iCompanyID)
		iCompanyID = p_iCompanyID
	End Property
	
	Public Property Get CompanyID()
		CompanyID = iCompanyID
	End Property

	Public Property Let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	End Property

	Public Property Get ConnectionString()
		ConnectionString = g_sConnectionString
	End Property

	Public Property Let CrossCertEligible(p_iCrossCertEligible)
		iCrossCertEligible = p_iCrossCertEligible
	End Property
	
	Public Property Get CrossCertEligible()
		CrossCertEligible = iCrossCertEligible
	End Property
	
	Public Property Let DriversLicenseNo(p_sDriversLicenseNo)
		sDriversLicenseNo = p_sDriversLicenseNo
	End Property
	
	Public Property Get DriversLicenseNo()
		DriversLicenseNo = sDriversLicenseNo
	End Property
	
	Public Property Let DriversLicenseStateId(p_iDriversLicenseStateId)
		g_iDriversLicenseStateId = p_iDriversLicenseStateId
	End Property
	
	Public Property Get DriversLicenseStateId()
		DriversLicenseStateId = g_iDriversLicenseStateId
	End Property
	
	Public Property Let Email(p_sEmail)
		sEmail = p_sEmail
	End Property
	
	Public Property Get Email()
		Email = sEmail
	End Property
	
	Public Property Let Fax(p_sFax)
		sFax = p_sFax
	End Property
	
	Public Property Get Fax()
		Fax = sFax
	End Property
	
	Public Property Let FirstName(p_sFirstName)
		sFirstName = p_sFirstName
	End Property
	
	Public Property Get FirstName()
		FirstName = sFirstName
	End Property
	
	Public Property Let GroupID(p_iGroupID)
		iGroupID = p_iGroupID
	End Property
	
	Public Property Get GroupID()
		GroupID = iGroupID
	End Property

	Public Property Let HowHearType(p_iHowHearType)
		iHowHearType = p_iHowHearType
	End Property
	
	Public Property Get HowHearType()
		HowHearType = iHowHearType
	End Property

	Public Property Let HowHearOther(p_sHowHearOther)
		sHowHearOther = p_sHowHearOther
	End Property
	
	Public Property Get HowHearOther()
		HowHearOther = sHowHearOther
	End Property
	
	Public Property Let LastName(p_sLastName)
		sLastName = p_sLastName
	End Property
	
	Public Property Get LastName()
		LastName = sLastName
	End Property
	
	Public Property Let LicenseNo(p_sLicenseNo)
		sLicenseNo = p_sLicenseNo
	End Property
	
	Public Property Get LicenseNo()
		LicenseNo = sLicenseNo
	End Property
	
	public property get Login()
		Login = sLogin
	end property 
	
	public property let Login(p_sLogin)
		sLogin = p_sLogin
	end property
	
	Public Property Let MarketWareConfirmNo(p_sMarketWareConfirmNo)
		sMarketWareConfirmNo = p_sMarketWareConfirmNo
	End Property
	
	Public Property Get MarketWareConfirmNo()
		MarketWareConfirmNo = sMarketWareConfirmNo
	End Property

	Public Property Let Membership(p_iMembership)
		iMembership = p_iMembership
	End Property
	
	Public Property Get Membership()
		Membership = iMembership
	End Property
	
	Public Property Let MiddleName(p_sMiddleName)
		sMiddleName = p_sMiddleName
	End Property
	
	Public Property Get MiddleName()
		MiddleName = sMiddleName
	End Property
	
	Public Property Let Newsletter(p_iNewsletter)
		iNewsletter = p_iNewsletter
	End Property
	
	Public Property Get Newsletter()
		Newsletter = iNewsletter
	End Property
	
	Public Property Let Notes(p_sNotes)
		sNotes = p_sNotes
	End Property
	
	Public Property Get Notes()
		Notes = sNotes
	End Property
	
	Public Property Let Password(p_sPassword)
		sPassword = p_sPassword
	End Property
	
	Public Property Get Password()
		Password = sPassword
	End Property
	
	Public Property Let Phone(p_sPhone)
		sPhone = p_sPhone
	End Property
	
	Public Property Get Phone()
		Phone = sPhone
	End Property
	
	Public Property Let SSN(p_sSSN)
		sSSN = p_sSSN
	End Property
	
	Public Property Get SSN()
		SSN = sSSN
	End Property
	
	Public Property Let StateID(p_iStateID)
		iStateID = p_iStateID
	End Property
	
	Public Property Get StateID()
		StateID = iStateID
	End Property
	
	Public Property Let UserStatus(p_iUserStatus)
		iUserStatus = p_iUserStatus
	End Property
	
	Public Property Get UserStatus()
		UserStatus = iUserStatus
	End Property
	
	Public Property Let UserID(p_iUserID)
		iUserID = p_iUserID
	End Property
	
	Public Property Get UserID()
		UserID = iUserID
	End Property
	
	Public Property Let Zipcode(p_sZipcode)
		sZipcode = p_sZipcode
	End Property
	
	Public Property Get Zipcode()
		Zipcode = sZipcode
	End Property
end class
%>

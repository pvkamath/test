<table width=760 align="center" bgcolor="D8CAA9" border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td colspan="2">
			<table width=760 border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/hdrLogo.jpg" width="171" height="142" alt="TrainingPro" border="0"></td>
					<td><img src="/Media/Images/hdrTagline.jpg" width="372" height="142" alt="National Leader in Mortgage Education" border="0"></td>
					<td background="/media/images/hdrSearchBgr.jpg"><img src="/Media/Images/spacer.gif" width="217" height="142" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<% Select Case iPage
			Case 1	%>
			<script language="javascript" src="/includes/FormCheck2.js"></script>
			<script TYPE="text/javascript">
			function ValidateLogin(FORM)
			{
				if (!checkString(FORM.User_Login,"Username"))
					return false;
				
				if (!checkEmail(FORM.User_Login))
					return false;				
				
				if (!checkString(FORM.User_Password,"Password"))
					return false;
				
				return true;
			}
			</script>
			<!-- Login Table  -->
			<table width=760 border=0 cellpadding=0 cellspacing=0 background="/media/images/hdrLoginBgr.jpg">
			<form name="LoginForm" method="POST" action="login.asp" onSubmit="return ValidateLogin(this)">
			<input type="hidden" name="referrer" value="<%=request("Referrer")%>">
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="15" height="27" alt="" border="0"></td>
					<td>Username&nbsp;&nbsp;</td>
					<td><input type="text" size="12" name="User_Login" height="10"></td>
					<td><img src="/Media/Images/spacer.gif" width="15" height="27" alt="" border="0"></td>
					<td>Password&nbsp;&nbsp;</td>
					<td><input type="password" size="12" name="User_Password"></td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="27" alt="" border="0"></td>
					<td width="100%" valign="middle"><input type="image" src="/media/images/bttnLogin.gif" width="49" height="19" alt="Login" border="0" vspace="0" hspace="0" border="0"></td>
				</tr>
			</form>
			</table>
			<% case 3 %>
			<table width=760 border=0 cellpadding=0 cellspacing=0 background="/media/images/hdrLoginBgr.jpg">
				<tr>
					<td><img src="<% = application("sDynMediaPath") %>clear.gif" width="1" height="27" alt="" border="0"></td>
				</tr>
			</table>
			<% case else %>
			<!-- Interior Nav  -->
			<table width=760 border=0 cellpadding=0 cellspacing=0 background="/media/images/hdrLoginBgr.jpg">
				<tr>
					<td><a href="/displayContent.asp?keywords=tips"
						onmouseover="changeImages('tips', '/media/images/navTips-h.gif'); return true;"
						onmouseout="changeImages('tips', '/media/images/navTips.gif'); return true;">
						<img name="tips" src="/media/images/navTips.gif" width="43" height="27" alt="Tips" border="0"></a></td>
					<td><a href="/displayContent.asp?keywords=definitions"
						onmouseover="changeImages('definitions', '/media/images/navDefinitions-h.gif'); return true;"
						onmouseout="changeImages('definitions', '/media/images/navDefinitions.gif'); return true;">
						<img name="definitions" src="/media/images/navDefinitions.gif" width="80" height="27" alt="Definitions" border="0"></a></td>
					<td><a href="#"
						onmouseover="changeImages('notes', '/media/images/navNotes-h.gif'); return true;"
						onmouseout="changeImages('notes', '/media/images/navNotes.gif'); return true;">
						<img name="notes" src="/media/images/navNotes.gif" width="74" height="27" alt="Notes" border="0"></a></td>
					<td><a href="#"
						onmouseover="changeImages('courses', '/media/images/navCourses-h.gif'); return true;"
						onmouseout="changeImages('courses', '/media/images/navCourses.gif'); return true;">
						<img name="courses" src="/media/images/navCourses.gif" width="115" height="27" alt="Courses" border="0"></a></td>
					<td width="100%">&nbsp;</td>
				</tr>
			</table>
			<% End Select %>
			
		</td>
	</tr>
	<tr>
		<td colspan="2"><img src="/Media/Images/hdrBlueDivision.jpg" width="760" height="6" alt="" border="0"></td>
	</tr>
	<!-- Body Table  -->
	<tr>
		<!-- Nav  -->
		<td width="180" height="100%" background="/media/images/navBgr.jpg" style="background-repeat: no-repeat" valign="top"><img src="/Media/Images/spacer.gif" width="10" height="240" alt="" border="0"><!--#include virtual=/includes/navMenu.asp--></td>
		<!-- Main Content Cell  -->
		<td width="580" valign="top">
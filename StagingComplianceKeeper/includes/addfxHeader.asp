<%
dim bMCheckIsAdmin
dim bMCheckIsCompanyL2Admin
dim bMCheckIsCompanyL2Viewer
dim bMCheckIsCompanyL3Admin
dim bMCheckIsCompanyL3Viewer
dim bMCheckIsBranchAdmin
dim bMCheckIsBranchViewer
bMCheckIsAdmin = CheckIsAdmin()
bMCheckIsCompanyL2Admin = CheckIsCompanyL2Admin
bMCheckIsCompanyL2Viewer = CheckIsCompanyL2Viewer
bMCheckIsCompanyL3Admin = CheckIsCompanyL3Admin
bMCheckIsCompanyL3Viewer = CheckIsCompanyL3Viewer
bMCheckIsBranchAdmin = CheckIsBranchAdmin
bMCheckIsBranchViewer = CheckIsBranchViewer
%>
<table width=760 align="center" class="bckMain" border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td colspan="3" bgcolor="#ffffff">
			<table width=760 bgcolor="FFFFFF" border=0 cellpadding=10 cellspacing=0>
				<tr>
					<td><a href="<% = application("URL") %>"><img align="right" src="/Media/Images/logo.gif" width="225" height="140" alt="ComplianceKeeper.com" border="0"></a></td>
					<td width="100%"><img src="/media/images/spacer.gif" width="1" height="119" alt="" border="0"></td>
					<td>
					<% if session("CorpLogoFileName") <> "" then 'oPreference.LogoFileName <> "" then %>
					<img src="/usermedia/images/uploads/AssocLogos/<% = session("CorpLogoFileName") %>" alt="" border="0">
					<% end if %>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="bckGray"><img src="/media/images/spacer.gif" width="1" height="2" alt="" border="0"></td>
	</tr>
	<% if session("User_ID") <> "" and session("afxSecurityGroupId") <= 11 then %>
	<tr>
		<td width="760" height="28" background="/media/images/nav_fullbar.gif" align="center" valign="middle">
			<a href=/default.asp class=nav target=_top><img src=/media/images/navIcon-home.gif width=17 height=17 alt=Home border=0 align=absmiddle>&nbsp;&nbsp;Home</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/calendar/calendar.asp class=nav><img src=/media/images/navIcon-events.gif width=17 height=17 alt=Events border=0 align="absmiddle">&nbsp;&nbsp;Events</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/company/companydetail.asp class=nav><img src=/media/images/navIcon-brokers.gif width=17 height=17 alt=Brokers border=0 align=absmiddle>&nbsp;&nbsp;<% = session("CompanyName") %></a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/branches/branchsearch.asp?action=new class=nav><img src=/media/images/navIcon-branches.gif width=17 height=17 alt=Branches border=0 align=absmiddle>&nbsp;&nbsp;Structure</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/officers/officerlist.asp?action=new class=nav><img src=/media/images/navIcon-officers.gif width=17 height=17 alt=Officers border=0 align=absmiddle>&nbsp;&nbsp;Officers</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/reports/officerreport.asp class=nav><img src=/media/images/navIcon-home.gif width=17 height=17 alt=Logout border=0 align=absmiddle>&nbsp;&nbsp;Reports</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href="http://mortgage.nationwidelicensingsystem.org/slr/Pages/default.aspx" target="_blank" class=nav><img src=/media/images/navIcon-courses.gif width=17 height=17 alt=States border=0 align=absmiddle>&nbsp;&nbsp;States</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/logout.asp class=nav><img src=/media/images/navIcon-logout.gif width=17 height=17 alt=Logout border=0 align=absmiddle>&nbsp;&nbsp;Logout</a>
		</td>
	</tr>
	<% elseif session("User_Id") <> "" and session("afxSecurityGroupId") = 30 then %>
	<tr>
		<td width="760" height="28" background="/media/images/nav_fullbar.gif" align="center" valign="middle">
			<a href=/default.asp class=nav target=_top><img src=/media/images/navIcon-home.gif width=17 height=17 alt=Home border=0 align=absmiddle>&nbsp;&nbsp;Home</a>
			<img src="/media/images/spacer.gif" width="20" height="1" alt="">
			<a href=/logout.asp class=nav><img src=/media/images/navIcon-logout.gif width=17 height=17 alt=Logout border=0 align=absmiddle>&nbsp;&nbsp;Logout</a>
		</td>
	</tr>	
	<% else %>
	<tr>
		<td width="150"><img src="/media/images/nav_left.gif" width="150" height="28" alt=""></td>
		<td width="460" class="bckNav"><img src="/media/images/spacer.gif" width="460" height="28" alt=""></td>
		<td width="150"><img src="/media/images/nav_right.gif" width="150" height="28" alt=""></td>
	</tr>
	<% end if %>
	<tr>
		<td colspan="3" class="bckGray"><img src="/media/images/spacer.gif" width="1" height="1" alt="" border="0"></td>
	</tr>
	<% if (bMCheckIsAdmin or bMCheckIsBranchAdmin or bMCheckIsBranchViewer or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL2Viewer or _
		bMCheckIsCompanyL3Admin or bMCheckIsCompanyL3Viewer) and bAdminSubmenu then %>
	<tr>
		<td colspan="3" align="center" bgcolor="#ffffff">
			<table width="760" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="1" height="28" alt="" border="0"></td>
								<% if sAdminSubmenuType = "OFFICERS" then %>
								<td><a href="officerlist.asp?action=new" class="nav">List Officers</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% if bMCheckIsAdmin or bMCheckIsBranchAdmin or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL3Admin then %>
								<td><a href="modofficer.asp" class="nav">Add New Officer</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><a href="addofficerlist_V2.asp" class="nav">Import Officers</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><a href="deactivateofficers.asp" class="nav">Deactivate Officers</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><a href="addofficerlicense.asp" class="nav">Import NMLS Officer Licenses</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <TR>
                               
                               <td><img src="/Media/Images/spacer.gif" width="1" height="28" alt="" border="0"></td>
                                 
                                <td align="center"><a href="addofficercourse.asp" class="nav">Import Courses</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="center"><a href="grouplist.asp" class="nav">List Groups</a></td>
 								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="center"><a href="modgroup.asp" class="nav">Add Group</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="center"><a href="employeelist.asp?action=new" class="nav">List Employees</a></td>
 								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td align="center"><a href="modemployee.asp" class="nav">Add New Employee</a></td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </TR>
									<% end if %>
								<% elseif sAdminSubmenuType = "BROKERS" then %>
								<td><a href="companysearch.asp" class="nav">List Companies</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><a href="modcompany.asp" class="nav">Add Company</a></td>
								<% elseif sAdminSubmenuType = "BRANCHES" then %>
                                
									<% if bMCheckIsAdmin or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL3Admin or bMCheckIsBranchAdmin _
										or bMCheckIsCompanyL2Viewer or bMCheckIsCompanyL3Viewer or bMCheckIsBranchViewer then %>
								<td>&nbsp;&nbsp;<a href="/branches/companymap.asp" class="nav">Organization &nbsp;&nbsp;Map</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL2Viewer then %>
								<td><a href="/branches/companyl2search.asp" class="nav"><% = session("CompanyL2Name") %> List</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin then %>
								<td><a href="/branches/modcompanyl2.asp" class="nav">Add <% = session("CompanyL2Name") %></a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL2Viewer or bMCheckIsCompanyL3Admin _
										or CheckIsCompanyL3Viewer then %>
								<td><a href="/branches/companyl3search.asp" class="nav"><% = session("CompanyL3Name") %> List</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin or bMCheckIsCompanyL2Admin then %>
								<td><a href="/branches/modcompanyl3.asp" class="nav">Add <% = session("CompanyL3Name") %></a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin or bMCheckIsCompanyL2Admin or bMCheckIsCompanyL3Admin or bMCheckIsBranchAdmin _
										or bMCheckIsCompanyL2Viewer or bMCheckIsCompanyL3Viewer or bMCheckIsBranchViewer then %>
								<td><a href="/branches/branchsearch.asp?action=new" class="nav">Branch List</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<% end if %>
									<% if bMCheckIsAdmin or bMCheckisCompanyL2Admin or bMCheckIsCompanyL3Admin then %>
								<td><a href="/branches/modbranch.asp" class="nav">Add Branch</a></td>
                                <td><a href="/branches/addBranches.asp" class="nav">Import Branches</a></td>
                                <td><a href="/branches/addBranchLicenses.asp" class="nav">Import NMLS Branch Licenses</a></td>
									<% end if %>
								<% elseif sAdminSubmenuType = "COMPANY" then %>
									<% if (CheckIsAdmin() or CheckIsCompanyL2Admin() or CheckIsCompanyL3Admin() or CheckIsBranchAdmin()) then %>
								<td><a href="/courses/modcourse.asp" class="nav">Add Course</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="/courses/courselist.asp" class="nav">List Courses</a></td>	
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="/courses/modprovider.asp" class="nav">Add Provider</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="/courses/providerlist.asp" class="nav">List Providers</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="/company/moduser.asp" class="nav">Add Administrator</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="/company/userlist.asp" class="nav">List Administrators</a></td>

							<tr>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="1" height="28" alt="" border="0"></td>
								<td colspan="13" align="center">
									<a href="/company/modsettings.asp" class="nav">System Settings</a>
									&nbsp;
									<a href="/company/emaillist.asp" class="nav">View Email Logs</a>
                                    &nbsp;
									<a href="/company/modagencytype.asp" class="nav">Add Agency Type</a>
                                    &nbsp;
									<a href="/company/listagencytypes.asp" class="nav">List Agency Types</a>
									&nbsp;
									<a href="/company/modemployeetype.asp" class="nav">Add Employee Type</a>
                                    &nbsp;
									<a href="/company/listemployeetypes.asp" class="nav">List Employee Types</a>
								</td>
									<% end if %>
								<% elseif sAdminSubmenuType = "EVENTS" then %>
									<% if (CheckIsAdmin() or CheckIsCompanyL2Admin() or CheckIsCompanyL3Admin() or CheckIsBranchAdmin()) then %>
								<td><a href="editevent.asp?editmode=Add" class="nav">Add Event</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><a href="editevent.asp?editmode=Add&EventGroup=1" class="nav">Add Event Group</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td><a href="calendar.asp" class="nav">List Events</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td><a href="EventGroupList.asp" class="nav">List Event Groups</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>								
                                <td><a href="list.asp?mode=coming&newsearch=YES" class="nav">Edit Current Events</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>								
								<td><a href="list.asp?mode=history&newsearch=YES" class="nav">Edit Past Events</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>								
								<td><a href="/Calendar/EventsReport.asp" class="nav">Events Report</a></td>								
									<% end if %>
								<% elseif sAdminSubmenuType = "COURSES" then %>
									<% if CheckIsAdmin() then %>
								<td><a href="modcourse.asp" class="nav">Add Course</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="courselist.asp" class="nav">List Courses</a><br>				
									<% end if %>
								<% elseif sAdminSubmenuType = "DOCUMENTS" then %>
								<td><a href="moddocument.asp" class="nav">Add Document</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>	
								<td><a href="documentsearch.asp" class="nav">List Documents</a><br>				
								<% elseif sAdminSubmenuType = "REPORTS" then %>
								<% if session("UserCompanyId") = -8 then %>
								<td nowrap><a href="branchlicreport.asp?action=NEW" class="nav">Branch Licensing Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap><a href="officerlicreport.asp?action=NEW" class="nav">Officer Licensing Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap><a href="officercoursereport.asp?action=NEW" class="nav">Course Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="1" height="28" alt="" border="0"></td>							
								<% end if %>
								<td nowrap><a href="officerreport.asp?action=NEW" class="nav">New Officer Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap><a href="groupreport.asp?action=NEW" class="nav">New Group Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td nowrap><a href="officertestreport.asp?action=NEW" class="nav">Officer Test Report</a></td>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>								
								<td nowrap><a href="emailreport.asp" class="nav">Email Officer List</a></td>
								<% end if %>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="border" colspan="3"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<% end if %>
	<tr>
		<td class="border" colspan="3"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
	</tr>
	<!-- Body Table  -->
	<tr>
		<td colspan="3" height="100%" valign="top" class="bckLeft">
			<% Select Case iPage
			Case 1	%>
			<% case else %>
			<br><img src="/media/images/spacer.gif" width="1" height="3" alt=""><br>
			<% End Select %>

<%

'Shamelessly stolen from the autofx finance calc
function calculatePayment(intPrinciple, intAnnualInterestRate, intInterestPeriods)
	dim intMonthlyInterestRate
	
	if intAnnualInterestRate <> 0 then
		intMonthlyInterestRate = intAnnualInterestRate / 12
		calculatePayment = (intMonthlyInterestRate * intPrinciple) / (1 - ((1 + intMonthlyInterestRate) ^ -intInterestPeriods))
	else
		calculatePayment = intPrinciple / intInterestPeriods
	end if
end function


dim nPrinciple
dim nInterest
dim nYears
dim nPayment

'Assign based on any submitted information
nPrinciple = trim(request("Principle"))
nInterest = trim(request("Interest"))
nYears = trim(request("Time"))


'Clear out any non-numeric junk data.
if not isNumeric(nPrinciple) then nPrinciple = ""
if not isNumeric(nInterest) then nInterest = ""
if not isNumeric(nYears) then nYears = ""


'Get the payment
if not nPrinciple = "" and not nInterest = "" and not nYears = "" then
	
	nPayment = round(calculatePayment(nPrinciple, nInterest / 100, nYears * 12), 2)
	
else

	nPayment = ""
	
end if	

%>
									
									
									
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="sectionTitle">Mortgage Calculator</span>
												<table border=0 cellpadding=0 cellspacing=0>
													<form name="CalcForm" method="POST" action="#">
													<tr>
														<td colspan="3"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
													</tr>
													<tr>
														<td valign="top"><img src="/Media/Images/photoCalculator.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td width="100%" valign="top">
															<table width="100%" border=0 cellpadding=10 cellspacing=0>
																<tr>
																	<td>
						 												<table border=0 cellpadding=4 cellspacing=4>
																			<tr>
																				<td nowrap><b>Principal:</b></td>
																				<td width="100%"><input type="text" size="10" name="Principle" height="10" value="<% = nPrinciple %>"></td>
																			</tr>
																			<tr>
																				<td nowrap><b>Interest Rate:</b></td>
																				<td><input type="text" size="3" name="Interest" height="10" value="<% = nInterest %>">&nbsp;&nbsp;<b>%</b></td>
																			</tr>
																			<tr>
																				<td nowrap><b>Length:</b></td>
																				<td><input type="text" size="2" name="Time" height="10" value="<% = nYears %>">&nbsp;&nbsp;<b>Years</b></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<%
																if nPayment <> "" then
																%>
																<tr>
																	<td align="center">
																		<b>Principal and Interest Payment: $<% = nPayment %></b>
																	</td>
																</tr>
																	<td>
																		<em>*The results of this calculator should only be used as an estimate. Actual financing plans require more complex calculations than the simple compounded interest used in this program.</em>
																	</td>
																</tr>
																<%
																end if
																%>
																<tr>
																	<td colspan="2"><input type="image" src="/Media/Images/bttnCalculate.gif" alt="Calculate Mortgage" border="0"></td>
																</tr>
															</table>
														</td>
													</tr>
												</form>
												</table>
											</td>
										</tr>
									</table>
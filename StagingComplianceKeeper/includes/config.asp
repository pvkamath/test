<%
' Option Explicit
' Declare Variables
dim bShowAutoID, bShowJobsFX, bShowWebCoupons, bShowLeadTracker, bShowShopFX, bshowNewsFX, bshowContentManager, bshowDealerPromo, bshowFormManager, bshowUserSecurity, bshowCalendar
dim bShowNewUsed, bShowMake, bShowModel, bShowColor, bShowPrice
dim headerGif, submitGif, searchGif, viewGif, againGif, nextGif, prevGif, calcGif, printGif, helpGif
dim colspan, cellpadding
dim blackbarjpg, uparrowgif, downarrowgif, blackbar2jpg, autoIDgif, toprightaidjpg, centeraidjpg, lowerleftaidjpg, lowerrightaidjpg
dim spacergif, nophotogif, partsgif, servicespgif, vweekgif
dim partsheight, partswidth, serviceheight, servicewidth, vweekheight, vweekwidth
dim titlepartsgif, titleservicegif, titlevehiclegif, titlewebgif, bulletsgif
dim outline1gif, outline2gif, outline3gif, outline4gif, outline5gif, outline6gif, outline7gif, outline8gif
dim webspacer
dim strRecipientEmail, strRecipientName, strSenderEmail, strSubject, strSuccessURL, strprofilepath, strSenderName
dim Leadtracker
dim WebCouponsPath
dim intRecordsPerPage
dim bDisplayPromotion, bDisplayEffectiveDate, bDisplayExpirationDate, bDisplayPrice, bDisplayType
dim strHeaderColor, strRow1Color, strRow2Color, strHeaderTextColor, strRowTextColor
dim bResultsShowVehicle, bResultsShowYear, bResultsShowMileage, bResultsShowColor, bResultsShowPhotoLink, bResultsUsedRightColumnPrice
dim bResultsNewRightColumnPrice, bResultsUsedLeftColumnPrice, bResultsNewLeftColumnPrice
dim sResultsNewRightHeader, sResultsUsedRightHeader, sResultsNewLeftHeader, sResultsUsedLeftHeader
dim sNoPriceLeftText, sNoPriceRightText, PriceText, sLeftUsedPriceText, sRightUsedPriceText, sLeftNewPriceText, sRightNewPriceText
dim strAutoHeaderColor, bClipboard, bFontDecoration, bLists, bForecolor, bLink, bImage, bStyle, bFont, bSize, bAlignment, bIndents, TextFXGif 
dim sshowpricetext, sRow1TextColor, sRow2TextColor, sCouponTextColor, sCouponHeadingColor
dim strBlack, bAutoBlack, bCouponBlack, strCouponBlack
'Admin Products -- True to display, False to hide
    bShowAutoID = application("bShowAutoID")
    bShowWebCoupons = application("bShowWebCoupons")
    bShowJobsFX = application("bShowJobsFX")
    bShowLeadTracker = application("bShowLeadTracker")
    bShowShopFX = application("bShowShopFX")
    bShowNewsFX = application("bShowNewsFX")
    bShowContentManager = application("bShowContentManager")
    bShowDealerPromo = application("bShowDealerPromo")
    bShowFormManager = application("bShowFormManager")
    bShowUserSecurity = application("bShowUserSecurity")
    bShowCalendar = application("bShowCalendar")
'Leadtracker -- If the site has a leadtracker and AutoID make this variable True
	Leadtracker = application("bLeadtracker")
'AutoID Display
	 'Search options
	     bShowNewUsed = application("bShowNewUsed")
	     bShowMake = application("bShowMake")
	     bShowModel = application("bShowModel")
	     bShowColor = application("bShowColor")
	     bShowPrice = application("bShowPrice")
	 'Results Options
	     bResultsShowVehicle = application("bResultsShowVehicle")
	     bResultsShowYear = application("bResultsShowYear")
	     bResultsShowMileage = application("bResultsShowMileage")
	     bResultsShowColor = application("bResultsShowColor")
	     bResultsShowPhotoLink = application("bResultsShowPhotoLink")
	     bResultsUsedRightColumnPrice = application("bResultsUsedRightColumnPrice")
	     bResultsNewRightColumnPrice = application("bResultsNewRightColumnPrice")
	     bResultsUsedLeftColumnPrice = application("bResultsUsedLeftColumnPrice")
	     bResultsNewLeftColumnPrice = application("bResultsNewLeftColumnPrice")
	     sShowPriceText = application("bsshowpricetext")           'This variable will place text in all price fields if set to true
	     bAutoBlack = application("bAutoBlack")              'Set this variable to true if the site needs black background images for AutoID
	     strAutoHeaderColor =  application("strAutoHeaderColor")	 'This is the color of the column headings
	 'Row Headings
	     sResultsNewLeftHeader = application("sResultsNewLeftHeader")
	     sResultsUsedLeftHeader = application("sResultsUsedLeftHeader")
	     sResultsNewRightHeader = application("sResultsNewRightHeader")
	     sResultsUsedRightHeader = application("sResultsUsedRightHeader")
	     sNoPriceLeftText = application("sNoPriceLeftText")             'This text is displayed if there is no price or the price is 0
	     sNoPriceRightText = application("sNoPriceRightText")		       'This text is displayed if there is no price or the price is 0
	     sLeftUsedPriceText = application("sLeftUsedPriceText")	   'This text is displayed if the sShowPriceText variable is true
	     sRightUsedPriceText = application("sRightUsedPriceText")  'This text is displayed if the sShowPriceText variable is true
	     sLeftNewPriceText = application("sLeftNewPriceText")      'This text is displayed if the sShowPriceText variable is true
	     sRightNewPriceText = application("sRightNewPriceText")    'This text is displayed if the sShowPriceText variable is true
'Web Coupons Display
	     bDisplayExpirationDate = application("bDisplayExpirationDate")
	 'Colors
	     intRecordsPerPage = application("intRecordsPerPage")		 'Number of Coupons to display per page in the Coupon List
	     bCouponBlack = application("bCouponBlack")					 'Set this variable to true if the site needs black background images for Web Coupons
'XMail fields 
	     strRecipientEmail = application("strRecipientEmail")
	     strRecipientName =  application("strRecipientName")
	     strSubject = 		application("strSubject")
	     strSuccessURL = 	application("strSuccessURL")
	     strProfilePath = 	application("strprofilepath") 'Only when used with leadtracker
	     strSenderEmail = 	application("strSenderEmail")
	     strSenderName = 	application("strSenderName")
	 '*******************************************************************************************************
	 '***                                                                                                 ***
	 '***                          YOU SHOULD NOT HAVE TO CHANGE BELOW THIS LINE                          ***
	 '***                                                                                                 ***
	 '*******************************************************************************************************
	 'AUTOID IMAGES -- FOR A SITE WITH A BLACK BACKGROUND USE /autoid/Media/Images/black/imagename
	                  'FOR A SITE WITH A WHITE BACKGROUND USE /autoid/Media/Images/imagename
	     if bAutoBlack then
	 	strBlack="/Black"
	     else
	 	strBlack=""
	     end if

		 logoEDS = "/autoid/media/images"&strblack&"/logo_eds.gif"
		 exclamationGif = "/autoid/Media/Images"&strblack&"/icon_exclamation.gif"
	     stripeGif = "/autoid/Media/Images"&strblack&"/bgr_bar.gif"
		 iconHome = "/autoid/Media/Images"&strblack&"/icon_home.gif"
		 iconCalculator = "/autoid/Media/Images"&strblack&"/icon_calculator.gif"
		 iconSearch = "/autoid/Media/Images"&strblack&"/icon_search.gif"
		 iconPrint = "/autoid/Media/Images"&strblack&"/icon_print.gif"
		 iconHelp = "/autoid/Media/Images"&strblack&"/icon_help.gif"
		 iconBack = "/autoid/Media/Images"&strblack&"/icon_back.gif"
		 star = "/autoid/Media/Images"&strblack&"/star.gif"
		 iconEnlarge = "/autoid/Media/Images"&strblack&"/icon_enlarge.gif"
		 iconEmail = "/autoid/Media/Images"&strblack&"/icon_email.gif"
		 iconCall = "/autoid/Media/Images"&strblack&"/icon_call_dealer.gif"
		 iconDetails = "/autoid/Media/Images"&strblack&"/icon_details.gif"
		 iconGallery = "/autoid/media/images"&strblack&"/icon_gallery.gif"
		 iconFeatures = "/autoid/media/images"&strblack&"/icon_features.gif"
		 iconMoreInfo = "/autoid/media/images"&strblack&"/icon_more_info.gif"
		 
		 cornerBL = "/autoid/Media/Images"&strblack&"/corner_bttmleft.gif"
		 cornerBR = "/autoid/Media/Images"&strblack&"/corner_bttmright.gif"
		 cornerTL = "/autoid/Media/Images"&strblack&"/corner_topleft.gif"
		 cornerTR = "/autoid/Media/Images"&strblack&"/corner_topright.gif"
		 
	     headerGif = "/autoid/Media/Images"&strblack&"/RedFindGrade.gif"
	     searchGif = "/autoid/Media/Images"&strblack&"/startsearch.gif"
	     viewGif =   "/autoid/Media/Images"&strblack&"/view.gif"
	     submitGif = "/autoid/Media/Images"&strblack&"/icon_submit.gif"
	     againGif =  "/autoid/Media/Images"&strblack&"/NewSearch.gif"
	     nextGif =   "/autoid/Media/Images"&strblack&"/next.gif"
	     prevGif =   "/autoid/Media/Images"&strblack&"/previous.gif"
	     helpGif =   "/autoid/Media/Images"&strblack&"/help.gif"
	     blackbarjpg = "/autoid/Media/Images"&strblack&"/blackbar.jpg"
	     downarrowgif = "/autoid/Media/Images"&strblack&"/downarrow.gif"
	     uparrowgif = "/autoid/Media/Images"&strblack&"/uparrow.gif"
	     blackbar2jpg = "/autoid/Media/Images"&strblack&"/blackbar2.jpg"
	     autoIDgif = "/autoid/Media/Images"&strblack&"/logo_addfxInventory.gif"
	     centeraidjpg = "/autoid/Media/Images"&strblack&"/centeraid.jpg"
	     lowerleftaidjpg = "/autoid/Media/Images"&strblack&"/lowerleftaid.jpg"
	     lowerrightaidjpg = "/autoid/Media/Images"&strblack&"/lowerrightaid.jpg"
	     spacergif = "/autoid/Media/Images"&strblack&"/spacer.gif"
	     nophotogif = "/autoid/Media/Images"&strblack&"/nophoto.gif"
		 
	 'WEBCOUPONS IMAGES -- FOR A SIGHT WITH A BLACK BACKGROUND USE "/webcoupons/Media/Images/black/imagename"
	 	 	      'FOR A SIGHT WITH A WHITE BACKGROUND USE "/webcoupons/Media/Images/imagename"
	     if bCouponBlack then
	 	strCouponBlack="/Black"
	     else
	 	strCouponBlack=""
	     end if
	     titlepartsgif = "/webcoupons/Media/Images"&strCouponblack&"/title_parts.gif"
	     titleservicegif =  "/webcoupons/Media/Images"&strCouponblack&"/title_service.gif"
	     titlevehiclegif =   "/webcoupons/Media/Images"&strCouponblack&"/title_vehicle.gif"
	     titlewebgif =   "/webcoupons/Media/Images"&strCouponblack&"/title_web.gif"
	     bulletsgif =   "/webcoupons/Media/Images"&strCouponblack&"/bullets.gif"
	     outline1gif =   "/webcoupons/Media/Images"&strCouponblack&"/outline1.gif"
	     outline2gif =  "/webcoupons/Media/Images"&strCouponblack&"/outline2.gif"
	     outline3gif = "/webcoupons/Media/Images"&strCouponblack&"/outline3.gif"
	     outline4gif = "/webcoupons/Media/Images"&strCouponblack&"/outline4.gif"
	     outline5gif = "/webcoupons/Media/Images"&strCouponblack&"/outline5.gif"
	     outline6gif = "/webcoupons/Media/Images"&strCouponblack&"/outline6.gif"
	     outline7gif = "/webcoupons/Media/Images"&strCouponblack&"/outline7.gif"
	     outline8gif = "/webcoupons/Media/Images"&strCouponblack&"/outline8.gif"
	     webspacer = "/webcoupons/Media/Images"&strCouponblack&"/spacer.gif"
	 ' Additions added for Thumbnails:
	     Const THUMBNAIL_BLOCK_WIDTH = 400
	     Const MAX_THUMBNAILS_PER_ROW = 2
	     Const MAX_THUMBNAIL_WIDTH = 200
	     Const MAX_THUMBNAIL_HEIGHT = 200
	     Const MAX_THUMBNAIL_WIDTH_IF_ONLY_ONE_IMAGE = 350
	     Const MAX_THUMBNAIL_HEIGHT_IF_ONLY_ONE_IMAGE = 350
	 'Additional web coupons colors
 		 sCouponHeadingColor = application("sCouponHeadingColor")
		 cellpadding = application("cellpadding")
	 'TextFX Options
		 bClipboard = application("bShowClipboardIcons")
		 bFontDecoration = application("bShowFontDecorationIcons")
		 bLists = application("bShowListsIcons")
		 bForeColor = application("bShowForeColorIcon")
		 bLink = application("bShowLinkIcon")
		 bImage = application("bShowImageIcon")
		 bStyle = False 
		 bFont = False 
	     bSize = False 
 		 bAlignment = False 
	     bIndents = False 
	     TextFXGif = "TextFX1.0/images/"
%>

<!-- #include virtual = "/admin/includes/contentReport.Class.asp" ------------------------>
<% if false then %>
<script language=javascript>
	function OpenNewsWindow(NewsID){
		window.open("/popupNews.asp?NewsID=" + NewsID,"NewsWindow","Width=350,Height=350,Scrollbars=1,Resizable=yes")
	}
</script>
<% end if %>
<script language="vbscript" runat="server">
dim g_iContentID 'as integer
dim g_sText 'as string
dim g_sHeadline 'as string
dim g_sImagePath 'as string
dim g_sImageName 'as string
</script>
<script language="vbscript" runat="server">
sub setGlobalContentData(p_sKeywords)
on error resume next
dim oContentReport 'as object
dim rsContentList 'as object
dim rsContentDetail 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
	
	g_iContentID = ""
	g_sHeadline = ""
	g_sText = ""
	g_sImagePath = ""
	g_sImagePath = ""
	g_sImageName = ""
	
	
	set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
	oContentReport.ConnectionString = Application("sDataSourceName")

	oContentReport.Keywords = p_sKeywords
	
	bDataReturned = oContentReport.getContentList("Active","")
	if bDataReturned then

		set rsContentList = oContentReport.ContentListRS
		rsContentList.Open
				
		if not rsContentList.EOF then

			iContentID = rsContentList.fields("NewsItemID").Value
			bDataReturned = oContentReport.getContent(iContentID)
			if bDataReturned then
			
				set rsContentDetail = oContentReport.ContentRS
				rsContentDetail.Open
			
				g_iContentID = rsContentDetail.fields("NewsItemID").Value
				g_sHeadline = rsContentDetail.fields("headline").Value
				g_sText = replace(rsContentDetail.fields("BodyText").Value,"''","""")
				g_sImagePath = rsContentDetail.fields("mediaPath").Value
				if isNull(g_sImagePath) then
					g_sImagePath = ""
				end if
				g_sImageName = rsContentDetail.fields("mediaName").Value
				if isNull(g_sImageName) then
					g_sImageName = ""
				end if				
				rsContentDetail.Close
			end if				
		end if
	end if	

end sub

sub printNewsSection(p_sKeywords)
on error resume next
dim oContentReport 'as object
dim rsContentList 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
dim iContentID 'as integer
dim	sHeadline 'as string
	
	
	set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
	oContentReport.ConnectionString = Application("sDataSourceName")

	oContentReport.Keywords = p_sKeywords
	
	bDataReturned = oContentReport.getContentList("Active","")
	if bDataReturned then
		set rsContentList = oContentReport.ContentListRS
		rsContentList.Open
				
		do while not rsContentList.EOF 
		
			iContentID = rsContentList.fields("NewsItemID").Value
			sHeadline = rsContentList.fields("headline").Value

			Response.Write(sHeadline & "<br><img src=""media/images/clear.gif"" width=""10"" height=""10"" alt="""" border=""0""><br>")

			rsContentList.MoveNext
		loop
	end if	

end sub


sub printIncentivesSection(p_sKeywords)
on error resume next
dim oContentReport 'as object
dim rsContentList 'as object
dim dPostingDate 'as date
dim bDataReturned 'as boolean
dim iContentID 'as integer
dim	sHeadline 'as string
dim sBody 'as string

	
	set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
	oContentReport.ConnectionString = Application("sDataSourceName")

	oContentReport.Keywords = p_sKeywords
	
	bDataReturned = oContentReport.getContentList("Active","")
	response.Write oContentReport.ErrorDescription
	if bDataReturned then
		set rsContentList = oContentReport.ContentListRS

		rsContentList.Open
				
		do while not rsContentList.EOF 

			sBody = rsContentList.fields("BodyText").Value
			iContentID = rsContentList.fields("NewsItemID").Value
			sHeadline = rsContentList.fields("headline").Value

			
			Response.Write("<b class=subhead>" & sHeadline & "</b><br>")
			Response.Write(sBody & "<p>")

			rsContentList.MoveNext
		loop
	end if	

end sub


sub getContentByID(p_iContentID)
on error resume next
dim oContentReport 'as object
dim oRSContent 'as object
dim bDataReturned 'as boolean

	bDataReturned = false
	g_iContentID = ""
	g_sHeadline = ""
	g_sText = ""
	g_sImagePath = ""
	g_sImagePath = ""
	g_sImageName = ""
	
	set oContentReport = New ContentReport 'Found in include contentManager.Class.asp
	oContentReport.ConnectionString = Application("sDataSourceName")

	bDataReturned = oContentReport.getContent(p_iContentID)
	
	if bDataReturned then
		set oRSContent = oContentReport.ContentRS
		oRSContent.Open
				
		g_iContentID = oRSContent.fields("NewsItemID").Value
		g_sHeadline = oRSContent.fields("headline").Value
		g_sText = replace(oRSContent.fields("BodyText").Value,"''","""")
		g_sImagePath = oRSContent.fields("mediaPath").Value
		if isNull(g_sImagePath) then
			g_sImagePath = ""
		end if
		g_sImageName = oRSContent.fields("mediaName").Value
		if isNull(g_sImageName) then
			g_sImageName = ""
		end if				
		oRSContent.Close
	end if

	set oRSContent = Nothing
end sub
</script>
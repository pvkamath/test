<%
	'--------------------------------------------------------------------------------------------------
	sub BeginHead(p_sTitle, p_bUsesFormValidation, p_nRefreshTime, p_sRefreshURL)
		response.write("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">" & vbCrLf)
		response.write("<!-- Site Developed by XIGroup -->" & vbCrLf)
		response.write("<!-- www.xigroup.com -->" & vbCrLf)
		
		response.write("<html>")
		
		' Begin head tag:
		response.write("<head>" & vbCrLf)
		response.write PrintTextFile(Application("sDynWebroot") & "includes/designShellHeadInformation.asp")
		response.write("<title>" & Application("sSiteName") & " " & p_sTitle & "</title>" & vbCrLf)
				
		' Redirect if refresh url and refresh time are specified parameters
		if p_nRefreshTime <> "" and p_sRefreshURL <> "" then
			' Print Meta Refresh Info
			response.write("<META HTTP-EQUIV=""Refresh"" " &_
							"CONTENT=""" & p_nRefreshTime & ";" &_
							"URL=" & p_sRefreshURL & """>" & vbCrLf)
		end if
		
		response.write("<meta NAME=""Xchange Interactive Group"" Content=""XIG"">" & vbCrLf)
		Response.Write("<meta NAME=""Title"" Content=""Compliance Keeper: Manage Mortgage Licensing � Mortgage License Tracking � Mortgage Licensing Data Solutions"">" & vbCrLf)
		response.write("<meta NAME=""Keywords"" Content=""compliancekeeper, Data tracking solutions, compliance manager, system security solutions, privacy security, secure client login, compliancekeeper com, send data, streamline data, customized distribution"">" & vbCrLF)
		response.write("<meta NAME=""Description"" Content=""ComplianceKeeper is a Data Tracking Solutions.  A Comprehensive Solution for Your Compliance Manager."">" & vbCrLf)
	
		response.write("<meta HTTP-EQUIV=""Content-Type"" CONTENT=""text/html;CHARSET=iso-8859-1"">" & vbCrLf)
		
		response.write("<LINK REL=""stylesheet"" TYPE=""text/css"" HREF=""" & Application("sDynWebroot") & "includes/style.css"">" & vbCrLf)
				
		response.write("<script language=""JavaScript"" src=""" & Application("sDynWebroot") & "includes/common.js""></script>" & vbCrLf)
		
		if p_bUsesFormValidation then response.write("<script language=""JavaScript"" src=""" & Application("sStandardFormValidationLibrary") & """></script>")
		
	end sub
	
	
	'--------------------------------------------------------------------------------------------------
	sub FinishHeadBeginBody(bShowBackground, bTrimMargins, bShowMenus)
		response.write("</head>" & vbCrLf)
		response.write("<body bgcolor=""#EAEAEA""")

		' Print Javascript References:
		response.write(" onLoad=""JavaScript:")
		'if bShowMenus then response.write("writeMenus();")
		response.write("InitializePage();")
		'if bShowMenus then response.write("return true;")
		response.write("""")
		'if bShowMenus then response.write(" onResize=""Javascript:if (isNS4) nsResizeHandler()""")


		if bShowBackground then
			response.write(" background=""" & Application("sDefaultSiteBackground") & """")
		end if
		
		if bTrimMargins then
			'response.write("  style=""margin-top:0;margin-left:1;margin-right:1"" ")
			response.write(" class=""" & Application("sDefaultBodyStylesheetClass") & """")
			response.write(" topmargin=""0"" leftmargin=""0""  marginwidth=""0"" marginheight=""0""")
		end if

		response.write(">")
	end sub
	

	'--------------------------------------------------------------------------------------------------
	sub EndPage()
		response.write("</body>" & vbCrLf & "</html>")
	end sub
%>
<SCRIPT LANGUAGE=VBSCRIPT RUNAT=SERVER>

	'================================================================
	' force the user to sign on again if the session expires or if 
	' the user has not yet logged on
	'================================================================

	sub checkSecurity()
		If Session("access_level") = "" Then
			session.Abandon
			'response.Write "Access level not set"
			Response.Redirect("/admin/login/login.asp?referer=admin.asp&db="&Request("db"))
		End If	
	end sub

	'================================================================
	' Get user id
	'================================================================
	sub getUserId()
		sNTId = request.serverVariables("REMOTE_USER")
		getUserId = Right(sNTId, Len(sNTId) - InStr(1, sNTId, "\"))
	end sub
	
	'================================================================
	' Get domain id
	'================================================================
	sub getDomainId()
		sNTId = request.serverVariables("REMOTE_USER")
		getDomainId = Left(sNTId, InStr(1, sNTId, "\") - 1)
	end sub
		
</SCRIPT>
<%
'A.M 12/12/2001
'function for check security when user visit pages associated with admin
function CheckSecurity1()
	
	' Verify the user has admin access to this site
	' A user with a siteID of '0' has admin access to all sites
	If session("siteID") <> application("siteID") and session("siteID") <> 0 then
		Response.Redirect(Application("sDynWebrootAdmin") & "login.asp")
	End If

	If (session("AccessLevel") = "") then
		Response.Redirect(Application("sDynWebrootAdmin") & "login.asp")
	End If

End function

'this function is used for the popup windows in the admin section
function CheckSecurity2()	
	' Verify the user has admin access to this site
	' A user with a siteID of '0' has admin access to all sites
	If session("siteID") <> application("siteID") and session("siteID") <> 0 then
		Response.Redirect(Application("sDynWebrootAdmin") & "session_timeout.asp")
	End If

	If (session("AccessLevel") = "") then
		Response.Redirect(Application("sDynWebrootAdmin") & "session_timeout.asp")
	End If

End function

'--------------------------------------------------------------
'set siteID to session property
'input: sUserID -- from table afxUserLoging
'input: connectString -- string for connecting database
'result: if user is a super user, session("siteID") set to 0, 
'else session("siteID") set to associated application site

Function setSessionSiteID(sUserID, connectString)

dim objRS, DataConn, rsCount, appID, bFindSiteID
appID = application("siteID") 
bFindSiteID = false

Set DataConn = Server.CreateObject("ADODB.Connection")
DataConn.ConnectionString = application(connectString)
DataConn.Open
  
Set objRS  = Server.CreateObject("ADODB.Recordset")
sSql = "SELECT * FROM afxUsersSitesX where UserID =" & sUserID

objRS.Open sSQL, DataConn, 3, 3
rsCount = objRS.RecordCount 

If rsCount > 0 Then 'have siteIDs for this user
	objRS.MoveFirst
	objRS.Find "siteID = 0" 'try ti find if this user is a super user
	If (objRS.BOF = True) OR (objRS.EOF = True) Then 'not a super user
		objRS.MoveFirst
		while not objRS.EOF and bFindSiteID = false
			If objRS("siteID") = appID Then
				session("SiteID") = objRS("siteID") 'find a siteID match the siteID in application setting
				bFindSiteID = true
			Else
				objRS.MoveNext 
			End If			
		WEnd
	Else
		session("SiteID") = 0 'super user
	End If
End If

	objRS.Close
	Set objRS =  Nothing
	DataConn.Close
	Set DataConn = Nothing

End Function

%>
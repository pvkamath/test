<%
'==============================================================================
' Class: Associate
' Purpose: Class used for retrieving and storing loan officer information.
'	During early stages these officers were being referred to as Associates, 
'	and the name was implemented for this class before "Officers" became more
'	common.  In TrainingPro Officers are stored in the general Users pool, and
'	managed by the users class.  
' Created: 1/13/05
' Author: Mark Silvia
' -------------------
' Properties:
'	- ConnectionString
'	- TpConnectionString
'	- VocalErrors
'
' Associate Properties:
'	- UserId
'	- CompanyId
'	- CompanyL2Id
'	- CompanyL3Id
'	- BranchId
'	- Email
'	- FirstName
'	- MidInitial
'	- LastName
'	- SSN
'	- GenderId
'	- RaceId
'	- DateOfBirth
'	- Title
'	- Department
'	- DriversLicenseNo
'	- DriversLicenseStateId
'	- HireDate
'	- TerminationDate
'	- Inactive
'	- EmployeeId
'	- Address
'	- Address2
'	- City
'	- StateId
'	- Zipcode
'	- ZipcodeExt
'	- Country
'	- Phone
'	- Phone2
'	- Phone3
'	- PhoneExt
'	- Phone2Ext
'	- Phone3Ext
'	- Fax
'	- CellPhone
'	- HomePhone
'	- HomeEmail
'	- Active
'	- UserStatus?
'	- Comments
'	- SearchStateIdList
'	- SearchCourseName
'	- SearchCourseProviderId
'	- SearchCourseCreditHours
'	- SearchLicenseStatusId
'	- SearchLicenseNumber
'	- SearchLicenseExpDateFrom
'	- SearchLicenseExpDateTo
'
' Methods:
'	- LoadAssociateById
'	- SaveAssociate
'	- DeleteAssociate
'	- SearchAssociates
'	- LookupGenderId
'	- LookupRaceId
'
'==============================================================================

class Associate

	' GLOBAL VARIABLE DECLARATIONS ============================================
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	dim g_iUserId
	dim g_iCompanyId
	dim g_iTpLinkCompanyId
	dim g_iCompanyL2Id
	dim g_iCompanyL3Id
	dim g_iBranchid
	dim g_sEmail
	dim g_sFirstName
	dim g_sMidInitial
	dim g_sLastName
	dim g_sSsn
	dim g_iGenderId
	dim g_iRaceId
	dim g_dDateOfBirth
	dim g_sTitle
	'dim g_sAssistantName
	dim g_sDepartment
	dim g_sDriversLicenseNo
	dim g_iDriversLicenseStateId
	dim g_dHireDate
	dim g_dTerminationDate
	dim g_bInactive
	dim g_sEmployeeId
	dim g_sAddress
	dim g_sAddress2
	dim g_sCity
	dim g_iStateId
	dim g_sZipcode
	dim g_sZipcodeExt
	dim g_iCountryId
	dim g_sPhone
	dim g_sPhone2
	dim g_sPhone3
	dim g_sPhoneExt
	dim g_sPhone2Ext
	dim g_sPhone3Ext
	dim g_sFax
	dim g_sCellPhone
	dim g_sHomePhone
	dim g_sHomeEmail
	dim g_sManager
	dim g_bOutOfStateOrig
	dim g_bActive
	dim g_iUserStatus
	dim g_sSearchStateIdList
	dim g_sSearchCompanyL2IdList
	dim g_sSearchCompanyL3IdList
	dim g_sSearchBranchIdList
	dim g_sCourseName
	dim g_iCourseProviderId
	dim g_iCourseCreditHours
	dim g_iLicenseStatusId
	dim g_sLicenseNumber
	dim g_dLicenseExpDateFrom
	dim g_dLicenseExpDateTo



	' PUBLIC PROPERTIES =======================================================

	'Stores/retrieves the database connection string.
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property


	'Stores/retrieves the trainingpro database connection string.
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.  
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if 
	end property
	
	
	'Unique User ID
	public property get UserId()
		UserId = g_iUserId
	end property
	public property let UserId(p_iUserId)
		if isnumeric(p_iUserId) then
			g_iUserId = cint(p_iUserId)
		elseif p_iUserId = "" then
			g_iUserId = ""
		else
			ReportError("Invalid UserId value.  Integer required.")
		end if
	end property
	
	
	'CompanyId
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = cint(p_iCompanyId)
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if 
	end property
	
	
	'CompanyL2Id - This, CompanyL3Id, and BranchId are all used to determine
	'which tier owns this officer.  CompanyId is always filled to distinguish 
	'the company the officer belongs to, but if the officer is assigned to one
	'of the lower tiers, one of the three properties will be assigned the value
	'and the others will be assigned NULL.
	public property get CompanyL2Id()
		CompanyL2Id = g_iCompanyL2Id
	end property
	public property let CompanyL2Id(p_iCompanyL2Id)
		if isnumeric(p_iCompanyL2Id) then
			g_iCompanyL2Id = clng(p_iCompanyL2Id)
			g_iCompanyL3Id = ""
			g_iBranchId = ""
		elseif p_iCompanyL2Id = "" then
			g_iCompanyL2Id = ""
		else
			ReportError("Invalid CompanyL2Id value.  Integer required.")
		end if
	end property


	'CompanyL3Id
	public property get CompanyL3Id()
		CompanyL3Id = g_iCompanyL3Id
	end property
	public property let CompanyL3Id(p_iCompanyL3Id)
		if isnumeric(p_iCompanyL3Id) then
			g_iCompanyL3Id = clng(p_iCompanyL3Id)
			g_iCompanyL2Id = ""
			g_iBranchId = ""
		elseif p_iCompanyL3Id = "" then
			g_iCompanyL3Id = ""
		else
			ReportError("Invalid CompanyL3Id value.  Integer required.")
		end if
	end property
	
		
	'BranchId
	public property get BranchId()
		BranchId = g_iBranchId
	end property
	public property let BranchId(p_iBranchId)
		if isnumeric(p_iBranchId) then
			g_iBranchId = clng(p_iBranchId)
			g_iCompanyL2Id = ""
			g_iCompanyL3Id = ""
		elseif p_iBranchId = "" then
			g_iBranchId = ""
		else
			ReportError("Invalid BranchId value.  Integer required.")
		end if 		
	end property
	
	
	'OwnerId - returns the ID of whichever Company, L2, L3, or Branch owns the
	'officer record.  Read-only.
	public property get OwnerId()
		if g_iBranchId <> "" then
			OwnerId = g_iBranchId
		elseif g_iCompanyL3Id <> "" then
			OwnerId = g_iCompanyL3Id
		elseif g_iCompanyL2Id <> "" then
			OwnerId = g_iCompanyL2Id
		else
			OwnerId = g_iCompanyId
		end if
	end property
	
	
	'OwnerTypeId - Returns an integer for which type of hierarchy object owns
	'this officer record.  Read-only.
	'2 - Branch
	'3 - Company
	'4 - CompanyL2
	'5 - CompanyL3
	public property get OwnerTypeId()
		if g_iBranchId <> "" then
			OwnerTypeId = 2
		elseif g_iCompanyL3Id <> "" then
			OwnerTypeId = 5
		elseif g_iCompanyL2Id <> "" then
			OwnerTypeId = 4
		else
			OwnerTypeId = 3
		end if
	end property	
	
	
	'User email address
	public property get Email()
		Email = g_sEmail
	end property
	public property let Email(p_sEmail)
		g_sEmail = left(trim(p_sEmail), 50)
	end property
	
	
	'User first name
	public property get FirstName()
		FirstName = g_sFirstName
	end property
	public property let FirstName(p_sFirstName)
		g_sFirstName = left(trim(p_sFirstname), 50)
	end property
	
	
	'User middle name
	public property get MidInitial()
		MidInitial = g_sMidInitial
	end property
	public property let MidInitial(p_sMidInitial)
		g_sMidInitial = left(trim(p_sMidInitial), 1)
	end property
	
	
	'User last name
	public property get LastName()
		LastName = g_sLastName
	end property
	public property let LastName(p_sLastName)
		g_sLastName = left(trim(p_sLastName), 50)
	end property
	

	'Combines first/middle/last name values into one returned value.
	public property get FullName()
		FullName = g_sFirstName & " "
		if trim(g_sMidInitial) <> "" then 
			FullName = FullName & g_sMidInitial & ". "
		end if
		FullName = FullName & g_sLastName
	end property
		
	
	'User social security number
	public property get Ssn()
		Ssn = g_sSsn
	end property
	public property let Ssn(p_sSsn)
		g_sSsn = left(trim(p_sSsn), 11)
	end property
	
	
	'Gender index
	public property get GenderId()
		GenderId = g_iGenderId
	end property
	public property let GenderId(p_iGenderId)
		if isnumeric(p_iGenderId) then
			g_iGenderId = cint(p_iGenderId)
		else
			ReportError("Invalid GenderId value.  Integer required.")
		end if
	end property
	
	
	'Race index
	public property get RaceId()
		RaceId = g_iRaceId
	end property
	public property let RaceId(p_iRaceId)
		if isnumeric(p_iRaceId) then
			g_iRaceId = cint(p_iRaceId)
		else
			ReportError("Invalid RaceId value.  Integer required.")
		end if
	end property
	
	
	'Date of birth
	public property get DateOfBirth()
		DateOfBirth = g_dDateOfBirth
	end property
	public property let DateOfBirth(p_dDateOfBirth)
		if isdate(p_dDateOfBirth) then
			g_dDateOfBirth = cdate(p_dDateOfBirth)
		elseif p_dDateOfBirth = "" then
			g_dDateOfBirth = ""
		else
			ReportError("Invalid DateOfBirth value.  Date required.")
		end if
	end property
	
	
	'User job title
	public property get Title()
		Title = g_sTitle
	end property
	public property let Title(p_sTitle)
		g_sTitle = left(trim(p_sTitle), 50)
	end property
	
	
	'User job department
	public property get Department()
		Department = g_sDepartment
	end property
	public property let Department(p_sDepartment)
		g_sDepartment = left(p_sDepartment, 50)
	end property
	
	
	'Drivers license number
	public property get DriversLicenseNo()
		DriversLicenseNo = g_sDriversLicenseNo
	end property
	public property let DriversLicenseNo(p_sDriversLicenseNo)
		g_sDriversLicenseNo = left(trim(p_sDriversLicenseNo), 25)
	end property
	
	
	'Drivers license state
	public property get DriversLicenseStateId()
		DriversLicenseStateId = g_iDriversLicenseStateId
	end property
	public property let DriversLicenseStateId(p_iDriversLicenseStateId)
		if isnumeric(p_iDriversLicenseStateId) then
			g_iDriversLicenseStateId = cint(p_iDriversLicenseStateId)
		elseif p_iDriversLicenseStateId = "" then
			g_iDriversLicenseStateId = ""
		else
			ReportError("Invalid DriversLicenseStateId.  Integer required.")
		end if 
	end property
	
	
	'Date the employee was hired by the company
	public property get HireDate()
		HireDate = g_dHireDate
	end property
	public property let HireDate(p_dHireDate)
		if isdate(p_dHireDate) then
			g_dHireDate = cdate(p_dHireDate)
		elseif p_dHireDate = "" then
			g_dHireDate = ""
		else
			ReportError("Invalid HireDate value.  Date required.")
		end if
	end property


	'Date the employee's term w/ the company ended
	public property get TerminationDate()
		TerminationDate = g_dTerminationDate
	end property
	public property let TerminationDate(p_dTerminationDate)
		if isdate(p_dTerminationDate) then
			g_dTerminationDate = cdate(p_dTerminationDate)
		elseif p_dTerminationDate = "" then
			g_dTerminationDate = ""
		else
			ReportError("Invalid TerminationDate value.  Date required.")
		end if
	end property
	
	
	'Is the user flagged as terminated/inactive?
	public property get Inactive()
		Inactive = g_bInactive
	end property
	public property let Inactive(p_bInactive)
		if isnumeric(p_bInactive) then
			g_bInactive = cbool(p_bInactive)
		elseif p_bInactive = "" then
			g_bInactive = ""
		else
			ReportError("Invalid Inactive value.  Boolean required.")
		end if 
	end property


	'Is the user flagged as a manager?
	public property get Manager()
		Manager = g_sManager
	end property
	public property let Manager(p_sManager)
		g_sManager = p_sManager
	end property
	
	
	'Is the user flagged as Out of state origin?
	public property get OutOfStateOrig()
		OutOfStateOrig = g_bOutOfStateOrig
	end property
	public property let OutOfStateOrig(p_bOutOfStateOrig)
		if isnumeric(p_bOutOfStateOrig) then
			g_bOutOfStateOrig = cbool(p_bOutOfStateOrig)
		elseif p_bOutOfStateOrig = "" then
			g_bOutOfStateOrig = ""
		else
			ReportError("Invalid OutOfStateOrig value.  Boolean required.")
		end if 
	end property
		
	
	'Employee ID field.  Not a keyfield or anything.
	public property get EmployeeId()
		EmployeeId = g_sEmployeeId
	end property
	public property let EmployeeId(p_sEmployeeId)
		g_sEmployeeId = left(p_sEmployeeId, 25)
	end property
	
	
	'Address line 1
	public property get Address()
		Address = g_sAddress
	end property
	public property let Address(p_sAddress)
		g_sAddress = left(trim(p_sAddress), 100)
	end property
	
	
	'Address line 2
	public property get Address2()
		Address2 = g_sAddress2
	end property
	public property let Address2(p_sAddress2)
		g_sAddress2 = left(trim(p_sAddress2), 100)
	end property
	
	
	'City
	public property get City()
		City = g_sCity
	end property
	public property let City(p_sCity)
		g_sCity = left(trim(p_sCity), 50)
	end property
	
	
	'State index
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then
			g_iStateId = cint(p_iStateId)
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Zipcode
	public property get Zipcode()
		Zipcode = g_sZipcode
	end property
	public property let Zipcode(p_sZipcode)
		g_sZipcode = left(trim(p_sZipcode), 10)
	end property
	
	
	'Optional zipcode extension
	public property get ZipcodeExt()
		ZipcodeExt = g_sZipcodeExt
	end property
	public property let ZipcodeExt(p_sZipcodeExt)
		if len(p_sZipcodeExt) >= 4 then
			g_sZipcodeExt = left(p_sZipcodeExt, 4)
		else
			g_sZipcodeExt = ""
		end if
	end property
	
	
	'Country ID Assigned to this address
	public property get CountryId
		CountryId = g_iCountryId
	end property
	public property let CountryId(p_iCountryId)
		if isnumeric(p_iCountryId) then
			g_iCountryId = p_iCountryId
		elseif p_iCountryId = "" then
			g_iCountryId = ""
		else
			ReportError("Invalid CountryId value.  Integer required.")
		end if 
	end property
	
	
	'Phone number
	public property get Phone()
		Phone = g_sPhone
	end property
	public property let Phone(p_sPhone)
		g_sPhone = left(trim(p_sPhone), 30)
	end property
	
	
	'Phone number extension
	public property get PhoneExt()
		PhoneExt = g_sPhoneExt
	end property
	public property let PhoneExt(p_sPhoneExt)
		g_sPhoneExt = left(trim(p_sPhoneExt), 10)
	end property
	
	
	'Phone number two
	public property get Phone2()
		Phone2 = g_sPhone2
	end property
	public property let Phone2(p_sPhone2)
		g_sPhone2 = left(trim(p_sPhone2), 30)
	end property
	
	
	'Phone number two extension
	public property get Phone2Ext()
		Phone2Ext = g_sPhone2Ext
	end property
	public property let Phone2Ext(p_sPhone2Ext)
		g_sPhone2Ext = left(trim(p_sPhone2Ext), 10)
	end property
	
	
	'Phone number three
	public property get Phone3()
		Phone3 = g_sPhone3
	end property
	public property let Phone3(p_sPhone3)
		g_sPhone3 = left(trim(p_sPhone3), 30)
	end property
	
	
	'Phone number three extension
	public property get Phone3Ext()
		Phone3Ext = g_sPhone3Ext
	end property
	public property let Phone3Ext(p_sPhone3Ext)
		g_sPhone3Ext = left(trim(p_sPhone3Ext), 10)
	end property
	
	
	'Fax number
	public property get Fax()
		Fax = g_sFax
	end property
	public property let Fax(p_sFax)
		g_sFax = left(trim(p_sFax), 30)
	end property
	
	
	'Cell Phone Number
	public property get CellPhone()
		CellPhone = g_sCellPhone
	end property
	public property let CellPhone(p_sCellPhone)
		g_sCellPhone = left(trim(p_sCellPhone), 30)
	end property
	
	
	'Home Phone number
	public property get HomePhone()
		HomePhone = g_sHomePhone
	end property
	public property let HomePhone(p_sHomePhone)
		g_sHomePhone = left(trim(p_sHomePhone), 30)
	end property
	
	
	'Home email address
	public property get HomeEmail()
		HomeEmail = g_sHomeEmail
	end property
	public property let HomeEmail(p_sHomeEmail)
		g_sHomeEmail = left(trim(p_sHomeEmail), 30)
	end property
	
	
	'Active boolean.  Describes whether this entry is to be used in the site.
	'public property get Active()
	'	Active = g_bActive
	'end property
	'public property let Active(p_bActive)
	'	if isnumeric(p_bActive) then
	'		g_bActive = cbool(p_bActive)
	'	else
	'		ReportError("Invalid Active value.  Boolean required.")
	'	end if 
	'end property
	
	
	'UserStatus property.
	public property get UserStatus()
		UserStatus = g_iUserStatus
	end property
	public property let UserStatus(p_iUserStatus)
		if isnumeric(p_iUserStatus) then
			g_iUserStatus = cint(p_iUserStatus)
		else
			ReportError("Invalid UserStatus value.  Integer required.")
		end if
	end property
	
	
	'List of State IDs to search within.  Used to find associates from multiple
	'states rather than just one.
	public property get SearchStateIdList()
		SearchStateIdList = g_sSearchStateIdList
	end property
	public property let SearchStateIdList(p_sSearchStateIdList)
		g_sSearchStateIdList = p_sSearchStateIdList
	end property
	
	
	'List of Branch IDs to search within.  Used to find associates from
	'multiple branches rather than just one.
	public property get SearchBranchIdList()
		SearchBranchIdList = g_sSearchBranchIdList
	end property
	public property let SearchBranchIdList(p_sSearchBranchIdList)
		g_sSearchBranchIdList = p_sSearchBranchIdList
	end property


	'List of CompanyL2 IDs to search within.  Used to find associates from
	'multiple CL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property
	

	'List of CompanyL3 IDs to search within.  Used to find associates from
	'multiple CL3s rather than just one.
	public property get SearchCompanyL3IdList()
		SearchCompanyL3IdList = g_sSearchCompanyL3IdList
	end property
	public property let SearchCompanyL3IdList(p_sSearchCompanyL3IdList)
		g_sSearchCompanyL3IdList = p_sSearchCompanyL3IdList
	end property	
		
	
	'Search CourseName property
	public property get SearchCourseName()
		SearchCourseName = g_sCourseName
	end property
	public property let SearchCourseName(p_sCourseName)
		g_sCourseName = p_sCourseName
	end property
	
	
	'Search Course Provider ID 
	public property get SearchCourseProviderId()
		SearchCourseProviderId = g_iCourseProviderId
	end property
	public property let SearchCourseProviderId(p_iCourseProviderId)
		if isnumeric(p_iCourseProviderId) then
			g_iCourseProviderId = cint(p_iCourseProviderId)
		elseif p_iCourseProviderId = "" then
			g_iCourseProviderId = ""
		else
			ReportError("Invalid SearchCourseProviderId value.  " & _
				"Integer required.")
		end if
	end property
	
	
	'Search Course Credit Hours
	public property get SearchCourseCreditHours()
		SearchCourseCreditHours = g_iCourseCreditHours
	end property
	public property let SearchCourseCreditHours(p_iCourseCreditHours)
		if isnumeric(p_iCourseCreditHours) then
			g_iCourseCreditHours = cint(p_iCourseCreditHours)
		elseif p_iCourseCreditHours = "" then
			g_iCourseCreditHours = ""
		else
			ReportError("Invalid SearchCourseCreditHours value.  " & _
				"Integer required.")
		end if
	end property
	
	
	'Search License Status 
	public property get SearchLicenseStatusId()
		SearchLicenseStatusId = g_iLicenseStatusId
	end property
	public property let SearchLicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = cint(p_iLicenseStatusId)
		elseif p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid SearchLicenseStatusId value.  " & _
				"Integer required.")
		end if
	end property
	
	
	'Search License Number
	public property get SearchLicenseNumber()
		SearchLicenseNumber = g_sLicenseNumber
	end property
	public property let SearchLicenseNumber(p_sLicenseNumber)
		g_sLicenseNumber = p_sLicenseNumber
	end property	
	
	
	'Search License Expiration from
	public property get SearchLicenseExpDateFrom()
		SearchLicenseExpDateFrom = g_dLicenseExpDateFrom
	end property
	public property let SearchLicenseExpDateFrom(p_dLicenseExpDateFrom)
		if isdate(p_dLicenseExpDateFrom) then
			g_dLicenseExpDateFrom = cdate(p_dLicenseExpDateFrom)
		elseif isnull(p_dLicenseExpDateFrom) or p_dLicenseExpDateFrom = "" then
			g_dLicenseExpDateFrom = ""
		else
			ReportError("Invalid SearchLicenseExpDateFrom value.  " & _
				"Date required.")
				
		end if
	end property


	'Search License Expiration from
	public property get SearchLicenseExpDateTo()
		SearchLicenseExpDateTo = g_dLicenseExpDateTo
	end property
	public property let SearchLicenseExpDateTo(p_dLicenseExpDateTo)
		if isdate(p_dLicenseExpDateTo) then
			g_dLicenseExpDateTo = cdate(p_dLicenseExpDateTo)
		elseif isnull(p_dLicenseExpDateTo) or p_dLicenseExpDateTo = "" then
			g_dLicenseExpDateTo = ""
		else
			ReportError("Invalid SearchLicenseExpDateTo value.  " & _
				"Date required.")
		end if
	end property
	
	
	'Is there a TrainingPro company ID which we should link with this company?
	'This is used to share course data between CMS companies and a users of a 
	'specific TrainingPro company.  This value is pulled from the Companies
	'table when the user is loaded, and is thus read-only.  It can be changed
	'by updating the CMS Company record.
	public property get TpLinkCompanyId()
		TpLinkCompanyId = g_iTpLinkCompanyId
	end property

	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub run when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub run when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) or p_iInt = "" then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDb(sSQL)
	
		'Response.Write(sSql)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
		
		'Response.Write("<p>" & sSql & "<p>")
		
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	'This function should no longer be necessary as this class no longer
	'directly queries the TP database.  The only TP information is the course
	'info, which is taken from Views on the CK DB.
	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	'private function QueryTpDb(sSQL)
	'
	'	set QueryTpDb = Server.CreateObject("ADODB.Recordset")
	'	
	'	'Verify preconditions
	'	if not CheckConnectionString then
	'		exit function
	'	end if
	'	
	'	
	'	Dim Connect	'as connection
	'	Dim objRS 'as recordset
    '
	'	set Connect = CreateObject("ADODB.Connection")
	'	set objRS = CreateObject("ADODB.Recordset")
	'    
	'   Connect.Open g_sTpConnectionString
	'	objRs.cursorlocation = 3
	'	objRs.CursorType = 3
	'   objRS.Open sSQL, connect
	'   objRs.ActiveConnection = nothing
	'
	'	Set QueryTpDb = objRS
	'
	'	set Connect = Nothing	
	'	set objRS = Nothing
	'end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	

	
	' -------------------------------------------------------------------------
	' Name: DeleteId
	' Description: Delete the specified Associate entry from the relevant 
	'			   database tables.
	' Preconditions: Connectionstring
	' Inputs: p_iUserId - User ID to delete
	' Returns: If successful, 1, otherwise 0.
	' -------------------------------------------------------------------------
	private function DeleteId(p_iUserId)
		
		DeleteId = 0 
		
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iUserId, false, "User ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		
'		if p_iUserId >= 0 then
'
'			oConn.ConnectionString = g_sTpConnectionString
''			oConn.Open				
'
'			'Mark the user as deleted in the trainingpro database
'			sSql = "UPDATE Users SET " & _
'				   "UserStatus = '3' " & _
'				   "WHERE UserId = '" & p_iUserId & "'" 
'			oConn.Execute sSql, lRecordsAffected
'			
'			'Verify that at least one record was affected
'			if not lRecordsAffected > 0 then
'			
'				ReportError("Failure deleting the user.")
'				exit function
'			
'			end if 
'				
'		elseif p_iUserId < 0 then
		
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
			
			'Mark the user as deleted in the cms database
			sSql = "UPDATE Associates SET " & _
				   "UserStatus = '3' " & _
				   "WHERE UserId = '" & p_iUserId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			'Verify that at least one record was affected
			if not lRecordsAffected > 0 then
				
				ReportError("Failure deleting the user.")
				exit function
			
			end if 
			
'		end if 
		
		
		'We need to remove the Associate's licenses and note that they were
		'deleted via the Associate.
		oConn.Close
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE Licenses SET " & _
			"Deleted = '1', " & _
			"DeletedWithOwner = '1' " & _
			"WHERE Deleted = '0' " & _
			"AND OwnerId = '" & p_iUserId & "' " & _
			"AND OwnerTypeId = '1' "
		oConn.Execute sSql, lRecordsAffected
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
		
		DeleteId = 1
		
	end function
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadAssociateById
	' Description: This method loads an associate's information into the
	'			   associate object.
	' Preconditions: ConnectionString
	' Inputs: p_iUserId - ID of the associate to load.
	' Returns: Returns the user ID if successful, otherwise 0.
	' -------------------------------------------------------------------------
	public function LoadAssociateById(p_iUserId)
	
		LoadAssociateById = 0
	
		'Verify preconditions/inputs
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iUserId, false, "UserID") then
			exit function
		end if
		
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")

		
		'Select the user from the combined TP/CMS Associates view			
		sSql = "SELECT Us.*, Cs.TpLinkCompanyId FROM Associates AS Us " & _
			"LEFT JOIN Companies AS Cs " & _
			"ON (Cs.CompanyId = Us.CompanyId) " & _
			"WHERE Us.UserId = '" & p_iUserId & "' "
		set oRs = QueryDb(sSql)
			
		
		if not (oRs.BOF and oRs.EOF) then
		
			g_iUserId = p_iUserId 'oRs("UserId")
			g_iCompanyId = oRs("CompanyId")
			g_iTpLinkCompanyId = oRs("TpLinkCompanyId")
			g_iCompanyL2Id = oRs("CompanyL2Id")
			g_iCompanyL3Id = oRs("CompanyL3Id")
			g_iBranchid = oRs("BranchId")
			g_sEmail = oRs("Email")
			g_sFirstName = oRs("FirstName")
			g_sMidInitial = trim(oRs("MidInitial"))
			g_sLastName = oRs("LastName")
			g_sSsn = oRs("Ssn")
			g_iGenderId = oRs("GenderId")
			g_iRaceId = oRs("RaceId")
			g_dDateOfBirth = oRs("DateOfBirth")
			g_sTitle = oRs("Title")
			g_sDepartment = oRs("Department")
			g_sDriversLicenseNo = oRs("DriversLicenseNo")
			g_iDriversLicenseStateId = oRs("DriversLicenseStateId")
			g_dHireDate = oRs("HireDate")
			g_dTerminationDate = oRs("TerminationDate")
			g_bInactive = oRs("Inactive")
			g_sManager = oRs("Manager")
			g_bOutOfStateOrig = oRs("OutOfStateOrig")
			g_sEmployeeId = oRs("EmployeeId")
			g_sAddress = oRs("Address")
			g_sAddress2 = oRs("Address2")
			g_sCity = oRs("City")
			g_iStateId = oRs("StateId")
			g_sZipcode = oRs("Zipcode")
			g_sZipcodeExt = oRs("ZipcodeExt")
			g_iCountryId = oRs("CountryId")
			g_sPhone = oRs("Phone")
			g_sPhone2 = oRs("Phone2")
			g_sPhone3 = oRs("Phone3")
			g_sPhoneExt = oRs("PhoneExt")
			g_sPhone2Ext = oRs("Phone2Ext")
			g_sPhone3Ext = oRs("Phone3Ext")
			g_sFax = oRs("Fax")
			g_sCellPhone = oRs("CellPhone")
			g_sHomePhone = oRs("HomePhone")
			g_sHomeEmail = oRs("HomeEmail")
			g_iUserStatus = oRs("UserStatus")
			
			LoadAssociateById = g_iUserId
		
		else

			ReportError("Unable to load officer.  Record not found.")
			exit function
		
		end if 
				
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: LoadAssociateByLastNameSsn
	' Desc: Load the associate object that matches the passed SSN and Last
	'	name.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function LoadAssociateByLastNameSsn(p_sSsn, p_sLastName, _
		p_iCompanyId)

		LoadAssociateByLastNameSsn = 0

		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sSsn, 0, "SSN") or _
			not CheckStrParam(p_sLastName, 0, "Last Name") or _
			not CheckIntParam(p_iCompanyId, 0, "Company ID") then
			exit function
		end if

		
		'dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT UserId FROM Associates " & _
			"WHERE SSN = '" & p_sSsn & "' " & _
			"AND LastName = '" & p_sLastName & "' " & _
			"AND CompanyId = '" & p_iCompanyId & "' " & _
			"AND UserStatus = '1' "
		set oRs = QueryDb(sSql)
			

		if not (oRs.BOF and oRs.EOF) then

			LoadAssociateByLastNameSsn = LoadAssociateById(oRs("UserId"))

		else		

			ReportError("Failed to locate the requested officer.")

		end if

		set oRs = nothing

	end function
		
	
	' -------------------------------------------------------------------------
	' Name: SaveAssociate
	' Description: Save the associate object in the database, and assign it the
	'			   resulting ID.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function SaveAssociate()
	
		SaveAssociate = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		if g_iUserId = "" then 
		
			'This is a new user for a CMS Company, so we'll add the user to the
			'CMS database.
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
		
			sSql = "INSERT INTO Associates (" & _
				"CompanyId, " & _
				"CompanyL2Id, " & _
				"CompanyL3Id, " & _
				"BranchId, " & _
				"Email, " & _
				"FirstName, " & _
				"MidInitial, " & _
				"LastName, " & _
				"Ssn, " & _
				"GenderId, " & _
				"RaceId, " & _
				"DateOfBirth, " & _
				"Title, " & _
				"Department, " & _
				"DriversLicenseNo, " & _
				"DriversLicenseStateId, " & _
				"HireDate, " & _
				"TerminationDate, " & _
				"Inactive, " & _
				"Manager, " & _
				"OutOfStateOrig, " & _
				"EmployeeId, " & _
				"Address, " & _
				"Address2, " & _
				"City, " & _
				"StateId, " & _
				"Zipcode, " & _
				"ZipcodeExt, " & _
				"CountryId, " & _
				"Phone, " & _
				"Phone2, " & _
				"Phone3, " & _
				"PhoneExt, " & _
				"Phone2Ext, " & _
				"Phone3Ext, " & _
				"Fax, " & _
				"CellPhone, " & _
				"HomePhone, " & _
				"HomeEmail, " & _
				"UserStatus " & _
				") VALUES (" & _
				"'" & g_iCompanyId & "', "
				
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "'" & g_iCompanyL2Id & "', " 
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "'" & g_iCompanyL3Id & "', " 
			else
				sSql = sSql & "NULL, "
			end if
			
			if g_iBranchId <> "" then
				sSql = sSql & "'" & g_iBranchId & "', " 
			else
				sSql = sSql & "NULL, "
			end if
				
			sSql = sSql & "'" & g_sEmail & "', " & _
				"'" & g_sFirstName & "', " & _
				"'" & g_sMidInitial & "', " & _
				"'" & g_sLastName & "', " & _
				"'" & g_sSsn & "', " & _
				"'" & g_iGenderId & "', " & _
				"'" & g_iRaceId & "', "
			if g_dDateOfBirth <> "" then 
				sSql = sSql & "'" & g_dDateOfBirth & "', "
			else
				sSql = sSql & "NULL, "
			end if
			sSql = sSql & "'" & g_sTitle & "', " & _
				"'" & g_sDepartment & "', " & _
				"'" & g_sDriversLicenseNo & "', " & _
				"'" & g_iDriversLicenseStateId & "', "
			if g_dHireDate <> "" then
				sSql = sSql & "'" & g_dHireDate & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dTerminationDate <> "" then	
				sSql = sSql & "'" & g_dTerminationDate & "', "
			else
				sSql = sSql & "NULL, "
			end if				
			sSql = sSql & "'" & abs(cint(g_bInactive)) & "', " & _
				"'" & g_sManager & "', " & _
				"'" & abs(cint(g_bOutOfStateOrig)) & "', " & _
				"'" & g_sEmployeeId & "', " & _
				"'" & g_sAddress & "', " & _
				"'" & g_sAddress2 & "', " & _
				"'" & g_sCity & "', " & _
				"'" & g_iStateId & "', " & _
				"'" & g_sZipcode & "', " & _
				"'" & g_sZipcodeExt & "', " & _
				"'" & g_iCountryId & "', " & _ 
				"'" & g_sPhone & "', " & _ 
				"'" & g_sPhone2 & "', " & _
				"'" & g_sPhone3 & "', " & _
				"'" & g_sPhoneExt & "', " & _
				"'" & g_sPhone2Ext & "', " & _
				"'" & g_sPhone3Ext & "', " & _
				"'" & g_sFax & "', " & _ 
				"'" & g_sCellPhone & "', " & _
				"'" & g_sHomePhone & "', " & _
				"'" & g_sHomeEmail & "', " & _
				"'" & g_iUserStatus & "' " & _
				")"
			'Response.Write(sSql)	
			oConn.Execute sSql, lRecordsAffected
			
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to create new CMS officer.")
				exit function
			
			end if 
			
			'Attempt to retrieve the index of the newly created user.
			sSql = "SELECT UserId FROM Associates WHERE " & _
				   "CompanyId = '" & g_iCompanyId & "' " & _
				   "AND FirstName = '" & g_sFirstName & "' " & _
				   "AND LastName = '" & g_sLastName & "' " & _
				   "ORDER BY UserId DESC"
				   
			set oRs = oConn.Execute(sSql)
			
			if oRs.EOF then
			
				ReportError("Failed to create new CMS officer.")
				exit function
				
			else
				
				g_iUserId = oRs("UserId")
				SaveAssociate = g_iUserId
			
			end if 
			
			
		'This is an existing record that we'll update.							
		else
		
			oConn.ConnectionString = g_sConnectionString
			oConn.Open
	
			sSql = "UPDATE Associates SET " & _
				"CompanyId = '" & g_iCompanyId & "', "
			if g_iCompanyL2Id <> "" then
				sSql = sSql & "CompanyL2Id = '" & g_iCompanyL2Id & "', "
			else
				sSql = sSql & "CompanyL2Id = NULL, "
			end if
			if g_iCompanyL3Id <> "" then
				sSql = sSql & "CompanyL3Id = '" & g_iCompanyL3Id & "', "
			else
				sSql = sSql & "CompanyL3Id = NULL, "
			end if
			if g_iBranchId <> "" then
				sSql = sSql & "BranchId = '" & g_iBranchId & "', "
			else
				sSql = sSql & "BranchId = NULL, "
			end if
			sSql = sSql & "Email = '" & g_sEmail & "', " & _
				"FirstName = '" & g_sFirstName & "', " & _
				"MidInitial = '" & g_sMidInitial & "', " & _
				"LastName = '" & g_sLastName & "', " & _
				"Ssn = '" & g_sSsn & "', " & _
				"GenderId = '" & g_iGenderId & "', " & _
				"RaceId = '" & g_iRaceId & "', " & _
				"Title = '" & g_sTitle & "', " & _
				"Department = '" & g_sDepartment & "', " & _
				"DriversLicenseNo = '" & g_sDriversLicenseNo & "', " & _
				"DriversLicenseStateId = '" & _
				g_iDriversLicenseStateId & "', " & _
				"EmployeeId = '" & g_sEmployeeId & "', "
			if g_dHireDate <> "" then
				sSql = sSql & "HireDate = '" & g_dHireDate & "', "
			else
				sSql = sSql & "HireDate = NULL, " 
			end if
			if g_dTerminationDate <> "" then	
				sSql = sSql & "TerminationDate = '" & _
					g_dTerminationDate & "', "
			else
				sSql = sSql & "TerminationDate = NULL, "
			end if
			sSql = sSql & "Inactive = '" & abs(cint(1)) & "', " & _
				"Manager = '" & g_sManager & "', " & _
				"OutOfStateOrig = '" & abs(cint(g_bOutOfStateOrig)) & "', " & _
				"Address = '" & g_sAddress & "', " & _
				"Address2 = '" & g_sAddress2 & "', " & _
				"City = '" & g_sCity & "', " & _
				"StateId = '" & g_iStateId & "', " & _
				"Zipcode = '" & g_sZipcode & "', " & _
				"Phone = '" & g_sPhone & "', " & _
				"Phone2 = '" & g_sPhone2 & "', " & _
				"Phone3 = '" & g_sPhone3 & "', " & _
				"PhoneExt = '" & g_sPhoneExt & "', " & _
				"Phone2Ext = '" & g_sPhone2Ext & "', " & _
				"Phone3Ext = '" & g_sPhone3Ext & "', " & _
				"Fax = '" & g_sFax & "', " & _
				"Active = '" & g_bActive & "', " & _
				"UserStatus = '" & g_iUserStatus & "' " & _
				"WHERE UserId = '" & g_iUserId & "'"
				'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected
		
			if not lRecordsAffected > 0 then
			
				ReportError("Failed to save existing user.")
				exit function
			
			end if 
			
			SaveAssociate = g_iUserId
		
		end if
		
		
		oConn.Close
		set oConn = nothing
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: DeleteAssociate
	' Description: Sets the associate UserStatus to deleted.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: If successful, returns the ID, otherwise zero.
	' -------------------------------------------------------------------------
	public function DeleteAssociate()

		DeleteAssociate = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(g_iUserId, false, "User ID") then
			exit function
		end if 

		
		dim iResult
		
		iResult = DeleteId(g_iUserId)
		
		if iResult = 0 then
		
			'Zero means failure
			ReportError("Could not delete the specified associate.")
			DeleteAssociate = 0 
			
		else
		
			'Success
			DeleteAssociate = 1
			
		end if 

	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchAssociates
	' Description: Retrieves a collection of UserIds that match the properties
	'			   of the current object.  I.e., based on the current Name, etc
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchAssociates()
	
		set SearchAssociates = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim sSql2
		dim oRs
		
		bFirstClause = true
		
		sSql = "SELECT VAs.UserId, VAs.LastName, VAs.FirstName " & _
			"FROM Associates AS VAs "

		
		'If FirstName search
		if g_sFirstName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.FirstName LIKE '%" & g_sFirstName & "%'"
		end if 
		
		
		'If LastName search
		if g_sLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName LIKE '%" & g_sLastName & "%'" 
		end if 
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyId = '" & g_iCompanyId & "'"
		end if 


		


		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL3Id = '" & iCompanyL3Id & "' "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if		

		'If CompanyL2Id search
		if g_iCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL2Id = '" & g_iCompanyL2Id & "'"
		end if
		
		
		'If CompanyL3Id search
		if g_iCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL3Id = '" & g_iCompanyL3Id & "'"
		end if

		
		'If BranchId search
		if g_iBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.BranchId = '" & g_iBranchId & "'"
		end if
		
		
		'If Email search
		if g_sEmail <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Email = '" & g_sEmail & "'"
		end if
		
		
		'If SSN search
		if g_sSsn <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Ssn = '" & g_sSsn & "'"
		end if 
		
		
		'If UserStatus search
		if g_iUserStatus <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.UserStatus = '" & g_iUserStatus & "'"
		end if 
		
		
		'If Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
				
		
		'If StateId search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "VAs.StateId = '" & g_iStateId & "'"
		end if
		

		sSql = sSql & " ORDER BY LastName"
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchAssociates = QueryDb(sSql)
			
	end function


	' -------------------------------------------------------------------------
	' Name: SearchAssociatesCoursesLicenses
	' Description: Retrieves a collection of UserIds that match the properties
	'			   of the current object.  I.e., based on the current Name, 
	'			   etc.  This differs from the SearchAssociates function in
	'			   that it searches by course and license information as well,
	'			   and will return the same user multiple times for each
	'			   course or license item on which they match.  It is used in
	'			   the site reports function.
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function SearchAssociatesCoursesLicenses()
	
		set SearchAssociatesCoursesLicenses = _
			Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if 
	
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql
		dim oRs
		
		bFirstClause = true 'false


'			"LEFT OUTER JOIN vAssociatesCourses AS VACs " & _
'			"ON ((VACs.UserId = VAs.UserId AND VACs.UserId < 0) OR (VACs.UserId > 0 AND VAs.Email LIKE VACs.Email AND VAs.SSN LIKE VACs.SSN AND VACs.CompanyId = '" & session("UserTpLinkCompanyId") & "')) AND NOT VACs.Deleted = '1' " & _
		
		
		sSql = "SELECT DISTINCT VAs.UserId, VAs.LastName, VAs.FirstName " & _
			"FROM Associates AS VAs " & _
			"LEFT OUTER JOIN vAssociatesCoursesCKUserId AS VACs " & _
			"ON (VACs.UserId = VAs.UserID " & _
			"AND (VACs.CompanyId = '" & g_iCompanyId & "' "
			
		if session("UserTpLinkCompanyId") <> "" then
			sSql = sSql & "OR VACs.CompanyId = '" & _
				session("UserTpLinkCompanyId") & "') "
		else
			sSql = sSql & ") " 
		end if
			
		sSql = sSql & "AND NOT VACs.Deleted = '1') " & _
			"LEFT OUTER JOIN vCourses AS VCs " & _
			"ON VCs.CourseId = VACs.CourseId " & _
			"LEFT OUTER JOIN Licenses AS Ls " & _
			"ON ((Ls.OwnerId = VAs.UserId) AND (Ls.OwnerTypeId = '1') AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL)) "
		'	"WHERE (VACs.Deleted = '0' OR VACs.Deleted IS NULL) " & _
		'	"AND (Ls.Deleted = '0' OR Ls.Deleted IS NULL) " 
		
		
		'If State Id List search
		dim aStateIdList
		dim iStateId
		dim bFirstState
		if g_sSearchStateIdList <> "" then
			aStateIdList = split(g_sSearchStateIdList, ",")
			bFirstState = true
			for each iStateId in aStateIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE ("
					bFirstClause = false
				elseif not bFirstState then
					sSql = sSql & " OR "
				else
					sSql = sSql & " AND ("
				end if
				
				sSql = sSql & "Ls.LicenseStateId = '" & iStateId & "' " & _
					"OR VCs.StateId = '" & iStateId & "' "
				
				bFirstState = false
			
			next
			sSql = sSql & ")"
		end if 			
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true
		
		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL2Id = '" & iCompanyL2Id & "' " 
				
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.CompanyL3Id = '" & iCompanyL3Id & "' "
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "VAs.BranchId = '" & iBranchId & "' " 
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if
		
			
		'If CourseName search
		if g_sCourseName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.Name LIKE '%" & g_sCourseName & "%'"
		end if 
		
		
		'If CourseProviderId search
		if g_iCourseProviderId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.ProviderId = '" & g_iCourseProviderId & "'"
		end if
		
		
		'If CourseCreditHours search
		if g_iCourseCreditHours <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VCs.ContEdHours = '" & g_iCourseCreditHours & "'"
		end if
		
		
		'If LicenseStatusId search
		if g_iLicenseStatusId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseStatusId = '" & g_iLicenseStatusId & "'"
		end if
		
		
		'If LicenseNumber search
		if g_sLicenseNumber <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "Ls.LicenseNum LIKE '%" & g_sLicenseNumber & "%'"
		end if
		
		
		'If LicenseExpDateFrom search
		if g_dLicenseExpDateFrom <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseExpDate >= '" & g_dLicenseExpDateFrom & "'"
		end if 
		
		
		'If LicenseExpDateTo search
		if g_dLicenseExpDateTo <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ls.LicenseExpDate <= '" & g_dLicenseExpDateTo & "'"
		end if 

		
		'If FirstName search
		if g_sFirstName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.FirstName LIKE '%" & g_sFirstName & "%'"
		end if 
		
		
		'If LastName search
		if g_sLastName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.LastName LIKE '%" & g_sLastName & "%'" 
		end if 
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyId = '" & g_iCompanyId & "'"
		end if 


		'If CompanyL2Id search
		if g_iCompanyL2Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL2Id = '" & g_iCompanyL2Id & "'"
		end if
		
		
		'If CompanyL3Id search
		if g_iCompanyL3Id <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.CompanyL3Id = '" & g_iCompanyL3Id & "'"
		end if
				
		
		'If BranchId search
		if g_iBranchId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.BranchId = '" & g_iBranchId & "'"
		end if
		
		
		'If Email search
		if g_sEmail <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Email = '" & g_sEmail & "'"
		end if
		
		
		'If SSN search
		if g_sSsn <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "VAs.Ssn LIKE '%" & g_sSsn & "%'"
		end if 
		
		
		'If UserStatus search
		if g_iUserStatus <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.UserStatus = '" & g_iUserStatus & "'"
		end if 
		
		
		'If Inactive search
		if g_bInactive <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if 
			sSql = sSql & "VAs.Inactive = '" & abs(cint(g_bInactive)) & "'"
		end if
		
		
		'If StateId search
		if g_iStateId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND " 
			end if
			sSql = sSql & "VAs.StateId = '" & g_iStateId & "'"
		end if
		
		sSql = sSql & " ORDER BY VAs.LastName, VAs.FirstName "
		'Response.Write("<p>" & sSql & "<p>")
		
		set SearchAssociatesCoursesLicenses = QueryDb(sSql)
			
	end function


	' -------------------------------------------------------------------------
	' Name: SearchCourses
	' Input: p_sCourseName - Name text to search for
	'		p_iProviderId - Provider to search fo
	' Desc: Returns a recordset of the matching courses.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Returns: Recordset of CourseIds
	' -------------------------------------------------------------------------
	public function SearchCourses(p_sCourseName, p_iProviderId)
	
		set SearchCourses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iProviderId, true, "Provider ID") or _
			not CheckIntParam(g_iUserId, false, "User ID") then
			exit function
		end if
		
		dim sSql 'SQL statement
		
		sSql = "SELECT * FROM vAssociatesCoursesCKUserId AS ACs " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if
			
			'"AND (ACs.Id < 0 " & _
			'	"AND ACs.UserId = '" & g_iUserId & "' " & _
			'	"OR (ACs.Id > 0 " & _
			'	"AND ACs.Email LIKE '" & g_sEmail & "' " & _
			'	"AND ACs.SSN LIKE '" & g_sSsn & "' " & _
			'	"AND ACs.CompanyId = '" & g_iTpLinkCompanyId & "')) "
		
		if p_iProviderId <> "" then 
			sSql = sSql & "AND ACs.ProviderId = '" & p_iProviderId & "' "
		end if
		
		if p_sCourseName <> "" then
			sSql = sSql & "AND ACs.Name LIKE '%" & p_sCourseName & "%' "
		end if
		
		'Response.Write(sSql)
		
		set SearchCourses = QueryDb(sSql)
		
	end function		


	' -------------------------------------------------------------------------
	' Name: GetCourses
	' Desc: Returns a recordset of the courses this user has completed.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Returns: Recordset of CourseIds
	' -------------------------------------------------------------------------
	public function GetCourses
	
		set GetCourses = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") then		
			exit function
		end if
		
		dim sSql 'SQL Statement
		
		sSql = "SELECT * FROM vAssociatesCoursesCKUserId AS ACs " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if

		sSql = sSql & "ORDER BY ACs.CourseExpirationDate "
			
		'Response.Write("<p>" & sSql & "<p>")
				
		set GetCourses = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name: GetCourse
	' Desc: Returns a recordset of a course this user has completed.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId
	' Returns: Recordset of Course association information
	' -------------------------------------------------------------------------
	public function GetCourse(p_iAssocId)
	
		set GetCourse = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Course Association ID") then
			exit function
		end if
		
		dim sSql 'SQL Statement
		

		sSql = "SELECT * FROM vAssociatesCoursesCKUserId AS ACs " & _
			"WHERE NOT ACs.Deleted = '1' " & _
			"AND ACs.Id = '" & p_iAssocId & "' " & _
			"AND ACs.UserId = '" & g_iUserId & "' " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if g_iTpLinkCompanyId <> "" then
			sSql = sSql & "OR ACs.CompanyId = '" & g_iTpLinkCompanyId & "') "
		else
			sSql = sSql & ") "
		end if
		
		set GetCourse = QueryDb(sSql)
		
		'response.Write(sSql)
		
	end function
	
	
	'We don't need this now that course information is only accessed through the
	'course view. 
	' ----------------------------------------------------------------------------------------------------------
	' Name: 				GetTpCourse()
	' Purpose:				Retrieves the Course Information for the specified course
	' Required Properties:  ConnectionString, g_iUserID
	' Optional Properties:
	' Parameters:			p_iID  the id of the course in the UsersCourses table
	' Outputs:				recordset of the course information
	' ----------------------------------------------------------------------------------------------------------
	'Function GetTpCourse(p_iAssocId)
	'	dim sSQL 'as string
	'	
	'	sSQL = "SELECT C.*, UC.*, CTX.TypeID, S.State FROM Courses AS C " & _
	'		   "INNER JOIN UsersCourses AS UC ON (UC.CourseID = C.CourseID) " & _
	'		   "INNER JOIN CoursesTypesX AS CTX ON (CTX.CourseID = C.CourseID) " & _
	'		   "LEFT OUTER JOIN States AS S ON (S.StateID = C.StateID) " & _
	'		   "WHERE UC.ID = '" & p_iAssocId & "' " & _
	'		   "AND UC.UserID = '"& g_iUserID & "' " & _
	'		   "ORDER BY CTX.TypeID asc"
	'		   
	'	Response.Write("<p>" & sSql & "<p>")
	'
	'	set GetTpCourse = QueryTpDb(sSQL)
	'End Function
	
	
	' -------------------------------------------------------------------------
	' Name: UpdateCourse
	' Desc: Updates a course association with the passed information.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId, CourseId, ExpDate, CompletionDate
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function UpdateCourse(p_iAssocId, p_iCourseId, p_dExpDate, _
		p_dCompletionDate)
	
		UpdateCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Association ID") or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckDateParam(p_dExpDate, false, "Renewal Date") or _
			not CheckDateParam(p_dCompletionDate, false, "Completion Date") _
			then
			exit function
		end if 
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE AssociatesCourses SET " & _
			"CourseId = '" & p_iCourseId & "', " & _
			"CourseExpirationDate = '" & p_dExpDate & "', " & _
			"CompletionDate = '" & p_dCompletionDate & "' " & _
			"WHERE Id = '" & p_iAssocId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to update the course record.")
		
		else
			
			UpdateCourse = 1
			
		end if
		
		oConn.Close
		set oConn = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: AddCourse
	' Desc: Adds a course association with the passed information.
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: CourseId, ExpDate, CompletionDate
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function AddCourse(p_iCourseId, p_dExpDate, _
		p_dCompletionDate)
	
		AddCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckDateParam(p_dExpDate, false, "Renewal Date") or _
			not CheckDateParam(p_dCompletionDate, false, "Completion Date") _
			then
			exit function
		end if 
		
		'Verify that this is a non-TP course we're being asked to add.
		if p_iCourseId > 0 then
					
		end if 
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Num of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "INSERT INTO AssociatesCourses (" & _
			"UserId, " & _
			"CourseId, " & _
			"CourseExpirationDate, " & _
			"CompletionDate " & _
			") VALUES (" & _
			"'" & g_iUserId & "', " & _
			"'" & p_iCourseId & "', " & _
			"'" & p_dExpDate & "', " & _
			"'" & p_dCompletionDate & "' " & _
			")"
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to add the course record.")
		
		else
			
			AddCourse = 1
			
		end if
		
		oConn.Close
		set oConn = nothing
		
	end function

	
	' -------------------------------------------------------------------------
	' Name: RemoveCourse
	' Desc: Removes a course association 
	' Preconditions: ConnectionString, TpConnectionString, UserId
	' Inputs: AssocId
	' Returns: 1 if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function RemoveCourse(p_iAssocId)
	
		RemoveCourse = 0
		
		'Verify preconditions and inputs
		if not CheckConnectionString or _
			not CheckIntParam(g_iUserId, false, "User ID") or _
			not CheckIntParam(p_iAssocId, false, "Association ID") then
			exit function
		end if
		
		dim sSql 'SQL statement
		dim oConn 'Connection object
		dim lRecordsAffected 'Number of records affected by SQL statement
		
		set oConn = Server.CreateObject("ADODB.Connection")
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		sSql = "UPDATE AssociatesCourses SET " & _
			"Deleted = '1' " & _
			"WHERE Id = '" & p_iAssocId & "' " & _
			"AND UserId = '" & g_iUserId & "' "
		oConn.Execute sSql, lRecordsAffected
		
		if lRecordsAffected <> 1 then
		
			ReportError("Failed to delete the course record.")
			
		else
		
			RemoveCourse = 1
			
		end if 
		
		oConn.Close
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: GetTpCourseProgress
	' Desc: Returns a percentage value of the completion of a course based on
	'	the number of completed sections.
	' Preconditions: ConnectionString
	' Inputs: p_iCourseId - ID of the course to check
	'	p_iTpUserId - TrainingPro User ID to look up
	' Returns: Integer value representing the percent completion
	' -------------------------------------------------------------------------
	public function GetTpCourseProgress(p_iCourseId, p_iTpUserId)
	
		GetTpCourseProgress = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iCourseId, false, "Course ID") or _
			not CheckIntParam(p_iTpUserId, false, "User ID") then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		dim iTotalLessons
		dim iCompletedLessons
		
		iTotalLessons = 0
		iCompletedLessons = 0
		
		'Get the total number of courses 
		sSql = "SELECT ID, Completed FROM trainingpro2.dbo.UsersLessons " & _
			"WHERE UserId = '" & p_iTpUserId & "' " & _
			"AND Course = '" & p_iCourseId & "' "
		set oRs = QueryDb(sSql)
			
		do while not oRs.EOF
				
			if oRs("Completed") then
			
				iCompletedLessons = iCompletedLessons + 1
			
			end if
			
			iTotalLessons = iTotalLessons + 1
			
			oRs.MoveNext
				
		loop
		
		if not iTotalLessons = 0 then
			GetTpCourseProgress = round(((iCompletedLessons / iTotalLessons) * 100), 0)
		else
			GetTpCourseProgress = 0
		end if
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupTpUserId
	' Desc: Returns the unique user ID of a TrainingPro user matching the
	'	passed email address
	' Preconditions: Connectionstring
	' Inputs: p_sUserName
	' Returns: Integer UserID
	' -------------------------------------------------------------------------
	public function LookupTpUserId(p_sUserName)
		
		LookupTpUserId = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(p_sUserName, false, "User Name") then
			exit function
		end if
		
		dim sSql 
		dim oRs
		
		sSql = "SELECT UserId FROM trainingpro2.dbo.Users WHERE " & _
			"UserName LIKE '" & p_sUserName & "' " & _
			"AND UserStatus = '1'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupTpUserId = oRs("UserId")
			
		end if
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: LookupGenderId
	' Description: Returns the long form of a GenderType
	' Preconditions: ConnectionString
	' Inputs: p_iGenderId - Gender ID to look up
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function LookupGenderId
	
		LookupGenderId = ""
		
		'Verify preconditions
		if not CheckConnectionstring then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM GenderTypes WHERE " & _
			"GenderId = '" & g_iGenderId & "'" 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
				
			LookupGenderId = oRs("GenderType")
		
		end if 
		
		set oRs = nothing
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupRaceId
	' Description: Returns the long form of a RaceType
	' Preconditions: ConnectionString
	' Inputs: p_iRaceId - Race ID to look up
	' Returns: Returns the results recordset.
	' -------------------------------------------------------------------------
	public function LookupRaceId
	
		LookupRaceId = ""
		
		'Verify preconditions
		if not CheckConnectionstring then
			exit function
		end if
		
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM RaceTypes WHERE " & _
			"RaceId = '" & g_iRaceId & "'" 
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
				
			LookupRaceId = oRs("RaceType")
		
		end if 
		
		set oRs = nothing
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupBranchNumber
	' Desc: Gets the BranchNumber value of the passed BranchId
	' Preconditions: ConnectionString
	' Inputs: p_iBranchId - ID of the Branch to lookup
	' Returns: Value of the BranchNumber of the Branch 
	' -------------------------------------------------------------------------
	public function LookupBranchNumber(p_iBranchId)
	
		LookupBranchNumber = ""
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iBranchId, 0, "Branch ID") then
			exit function
		end if
		
		dim sSql
		dim oRs
		
		sSql = "SELECT * FROM CompanyBranches WHERE " & _
			"BranchId = '" & p_iBranchId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
		
			LookupBranchNumber = oRs("BranchNum")
			
		end if
		
		set oRs = nothing
		
	end function
	
	
end class
%>
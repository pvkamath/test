<%
'==============================================================================
' Class: AssociateDoc
' Controls the creation, modification, and removal of documents and forms that
'	may be uploaded for officer, company, branch, and state records.
' Create Date: 11/7/05
' Author: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- DocId
'	- DocName
'	- CompanyId
'	- OwnerId
'	- OwnerTypeId
'	- FileName
'	- FileExtension
'	- PostDate
'	- Deleted
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'	- DeleteId
'
' Methods
'	- LoadDocById
'	- SaveDoc
'	- SearchDocs
'==============================================================================

class AssociateDoc

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Document properties
	dim g_iDocId
	dim g_sDocName
	dim g_iCompanyId
	dim g_iOwnerId
	dim g_iOwnerTypeId
	dim g_sFileName
	dim g_sFileExtension
	dim g_dPostDate
	dim g_bDeleted
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this Document
	public property get DocId()
		DocId = g_iDocId
	end property
	
	
	'Document name
	public property get DocName()
		DocName = g_sDocName
	end property
	public property let DocName(p_sDocName)
		g_sDocName = left(p_sDocName, 50)
	end property
	
	
	'CompanyId - Unique ID of the company to which this doc belongs.  This
	'would be redundant with the OwnerId/OwnerTypeId combo, except that we 
	'need it to distinguish which company owns a state doc.  The state IDs are
	'shared between companies.
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property 
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'OwnerId - Unique ID of the record this doc belongs to
	public property get OwnerId()
		OwnerId = g_iOwnerId
	end property
	public property let OwnerId(p_iOwnerId)
		if isnumeric(p_iOwnerId) then
			g_iOwnerId = clng(p_iOwnerId)
		elseif p_iOwnerId = "" then
			g_iOwnerId = "" 
		else
			ReportError("Invalid OwnerId value.  Integer required.")
		end if
	end property
	
	
	'Owner Type - Which type of record owns this doc?
	'1 - Officer
	'2 - Branch
	'3 - Company
	'4 - CompanyL2
	'5 - CompanyL3
	'6 - State
	public property get OwnerTypeId()
		OwnerTypeId = g_iOwnerTypeId
	end property
	public property let OwnerTypeId(p_iOwnerTypeId)
		if isnumeric(p_iOwnerTypeId) then
			g_iOwnerTypeId = clng(p_iOwnerTypeId)
		elseif p_iOwnerTypeId = "" then
			g_iOwnerTypeId = "" 
		else
			ReportError("Invalid OwnerTypeId value.  Integer required.")
		end if
	end property
	
	
	'File name of the document
	public property get FileName()
		FileName = g_sFileName
	end property
	public property let FileName(p_sFileName)
		g_sFileName = p_sFileName
	end property
	
	
	'Extension for the file
	public property get FileExtension()
		FileExtension = g_sFileExtension
	end property
	public property let FileExtension(p_sFileExtension)
		g_sFileExtension = p_sFileExtension
	end property
	
	
	'Date the document was posted on the site
	public property get PostDate
		PostDate = g_dPostDate
	end property
	public property let PostDate(p_dPostDate)
		if isdate(p_dPostDate) then
			g_dPostDate = cdate(p_dPostDate)
		elseif p_dPostDate = "" then
			g_dPostDate = ""
		else
			ReportError("Invalid PostDate value.  Date required.")
		end if
	end property

	
	'Deleted - Has this record been marked as deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = ""
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
		
		g_bDeleted = 0
	
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(sSql & "<p>")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadDocById
	' Desc: Load a document based on the passed DocId
	' Preconditions: ConnectionString
	' Inputs: p_iDocId - ID of the document to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadDocById(p_iDocId)
		
		LoadDocById = 0 
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iDocId, 0, "Doc ID") then
			exit function
		end if 
		
		
		dim oRs 'RecordSet
		dim sSql 'SQL Statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM AssociateDocs " & _
			"WHERE DocId = '" & p_iDocId & "'" 
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the document properties
			g_iDocId = oRs("DocId")
			g_sDocName = oRs("DocName")
			g_iCompanyId = oRs("CompanyId")
			g_iOwnerId = oRs("OwnerId")
			g_iOwnerTypeId = oRs("OwnerTypeId")
			g_sFileName = oRs("FileName")
			g_sFileExtension = oRs("FileExtension")
			g_dPostDate = oRs("PostDate")
			g_bDeleted = oRs("Deleted")
			
			LoadDocById = g_iDocId
		
		else
			
			ReportError("Unable to load the passed Doc ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LoadDocFile
	' Desc: Inserts the document into the database.
	' Preconditions: ConnectionString, DocId
	' Returns: Returns the recordset object
	' -------------------------------------------------------------------------
	public function LoadDocFile()
	
		set LoadDocFile = server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iDocId, 0, "Document ID") then
			exit function
		end if
		
		dim sSql 'SQL Statement
		dim oRs 'ADODB Recordset
		
		
		'Load the database record
		sSql = "SELECT DocId, FileContents FROM AssociateDocsFiles " & _
			"WHERE DocId = '" & g_iDocId & "'"
		'Response.Write(sSql)
		'set LoadDocFile = QueryDb(sSql)
		set oRs = QueryDb(sSql)
		
		dim oStream
		set oStream = server.CreateObject("ADODB.Stream")
		oStream.Type = 1
		oStream.Open 
		'Response.Write("<br>z" & oStream.Size & "x")
		if not oRs.EOF then
			oStream.Write oRs("FileContents")
			'Response.Write("<br>z" & oStream.Size & "x")
		end if
		

		oRs.MoveFirst
		
		set LoadDocFile = oRs				
		
	'	do while not LoadDocFile.EOF
		'	'Response.Write("<br>Y" & LoadDocFile.State  & "Y")
	'		Response.Write("<br>X" & LoadDocFile("DocName") & "X")
		'	LoadDocFile.MoveNext
	'	loop
		
		'oConn.Close
		'set oConn = nothing
			
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveDocFile
	' Desc: Inserts the document file into the database.
	' Preconditions: ConnectionString, DocId
	' Returns: If successful, returns the doc ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveDocFile(p_oFileContents)
		
		SaveDocFile = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iDocId, 0, "Document ID") then
			exit function
		end if
		
		'Response.Write("<br>z" & p_oFileContents.Size & "z")
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordet
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		dim bExistingFile 'Is there already a file stored?
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.Recordset")
				
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'See if there is an existing file in the database already
		sSql = "SELECT DocId, FileContents FROM AssociateDocsFiles " & _
			"WHERE DocId = '" & g_iDocId & "'"
		oRs.Open sSql, oConn, adOpenKeyset, adLockOptimistic
		
		if not oRs.EOF then
			bExistingFile = true
		else
			bExistingFile = false
		end if 
		
		if not bExistingFile then 
		
			oRs.AddNew
			oRs("DocId") = g_iDocId
			
			'Response.Write("<p>...")
			p_oFileContents.Position = 0
			'Response.Write(p_oFileContents.Read(adReadAll))
			'Response.Write("<br>...")
			
			oRs("FileContents") = p_oFileContents.Read
			
		else
		
			oRs("DocId") = g_iDocId
			oRs("FileContents") = p_oFileContents.Read
		
		end if 
		
		oRs.Update
		
		
		SaveDocFile = g_iDocId
		
		oRs.Close
		set oRs = nothing
		
	end function		
				
				
	' -------------------------------------------------------------------------
	' Name: SaveDoc
	' Desc: Saves a document object's properties to the database.
	' Preconditions: ConnectionString, OwnerId, OwnerTypeId, DocName
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveDoc()
		
		SaveDoc = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckStrParam(g_sDocName, 0, "Document Name") or _
			not CheckIntParam(g_iOwnerId, 0, "Owner ID") or _
			not CheckIntParam(g_iOwnerTypeId, 0, "Owner Type ID") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL Statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.RecordSet")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a DocId property assigned already, we're working on a
		'loaded document, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iDocId <> "" then
		
			bIsNew = false
			sSql = "UPDATE AssociateDocs SET " & _
				"DocName = '" & g_sDocName & "', " & _ 
				"CompanyId = '" & g_iCompanyId & "', " & _
				"OwnerId = '" & g_iOwnerId & "', " & _
				"OwnerTypeId = '" & g_iOwnerTypeId & "', " & _
				"FileName = '" & g_sFileName & "', " & _
				"FileExtension = '" & g_sFileExtension & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE DocId = '" & g_iDocId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				
				SaveDoc = g_iDocId
				
			else
				
				ReportError("Unable to save existing Document.")
				exit function
				
			end if
			
		else
		
			bIsNew = true
			sSql = "INSERT INTO AssociateDocs (" & _
				"DocName, " & _
				"CompanyId, " & _
				"OwnerId, " & _
				"OwnerTypeId, " & _
				"FileName, " & _
				"FileExtension, " & _
				"PostDate, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_sDocName & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & g_iOwnerId & "', " & _
				"'" & g_iOwnerTypeId & "', " & _
				"'" & g_sFileName & "', " & _
				"'" & g_sFileExtension & "', " & _
				"'" & Now() & "', " & _
				"'" & g_bDeleted & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new document
			sSql = "SELECT DocId FROM AssociateDocs " & _
				"WHERE OwnerId = '" & g_iOwnerId & "' " & _
				"AND DocName = '" & g_sDocName & "' " & _
				"ORDER BY DocId DESC" 
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				g_iDocId  = oRs("DocId")
				SaveDoc = g_iDocId 
				
			else
			
				'The record could not be retrieved
				ReportError("Failed to save new Document.")
				exit function
				
			end if 
			
		end if 
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
			

	' -------------------------------------------------------------------------
	' Name: SearchDocs
	' Desc: Retrieves a collection of DocIds that match the properties of
	'		the current object instance.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchDocs()
		
		set SearchDocs = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim bFirstClause 'Used to direct correct SQL phrasing
		dim sSql 'SQL statement
		dim oRs 'Recordset object

		
		bFirstClause = true
		
		sSql = "SELECT Ds.DocId, " & _
			"Ds.DocName, Ds.PostDate " & _
			"FROM AssociateDocs AS Ds "
			'"WHERE (NOT Ds.Deleted = '1') "
			
		'If DocName search
		if g_sDocName <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ds.DocName LIKE '%" & g_sDocName & "%'"
		end if 
		
		
		'If Deleted search
		if g_bDeleted <> "" then	
			if bFirstClause then
				sSql = sSql & " WHERE " 
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ds.Deleted = '" & g_bDeleted & "'"
		end if
		
		
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ds.CompanyId = '" & g_iCompanyId & "'"
		end if
		
		
		'If OwnerId search
		if g_iOwnerId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ds.OwnerId = '" & g_iOwnerId & "'"
		end if
		
		
		'If OwnerType search
		if g_iOwnerTypeId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Ds.OwnerTypeId = '" & g_iOwnerTypeId & "'"
		end if
		
		
		sSql = sSql & " ORDER BY Ds.DocName"
		
		'Response.Write(sSql)
		
		set SearchDocs = QueryDb(sSql) 
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupAssociateNameById
	' Desc: Gets the name of an associate based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupAssociateNameById(p_iId)
	
		LookupAssociateNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT FirstName, LastName FROM Associates " & _
			"WHERE UserId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupAssociateNameById = oRs("FirstName") & " " & oRs("LastName")
			
		end if
		
		set oRs = nothing
		
	end function


	' -------------------------------------------------------------------------
	' Name: LookupCompanyNameById
	' Desc: Gets the name of a company based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupCompanyNameById(p_iId)
	
		LookupCompanyNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM Companies " & _
			"WHERE CompanyId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupCompanyNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	


	' -------------------------------------------------------------------------
	' Name: LookupBranchNameById
	' Desc: Gets the name of a branch based on the passed ID.
	' Preconditions: ConnectionString
	' Inputs: p_iId
	' Returns: Name string, or empty if none found.
	' -------------------------------------------------------------------------
	public function LookupBranchNameById(p_iId)
	
		LookupBranchNameById = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'Recordset object
		
		sSql = "SELECT Name FROM CompanyBranches " & _
			"WHERE BranchId = '" & p_iId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			
			LookupBranchNameById = oRs("Name") 
			
		end if
		
		set oRs = nothing
		
	end function	
	
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	'					p_bAbbrev - Return two-letter abbreviation
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId, p_bAbbrev)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		elseif not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			if p_bAbbrev then
				LookupState = oRs("Abbrev")
			else
				LookupState = oRs("State")
			end if
		end if 
		
		set oRs = nothing
		
	end function
	
		
end class
%>
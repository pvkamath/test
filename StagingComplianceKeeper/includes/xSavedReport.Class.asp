<%
'==============================================================================
' Class: SavedReport
' Controls the creation, modification, and removal of saved report
'	configurations
' Created: 4/10/06
' Authors: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- ReportId
'	- Name
'	- Deleted
'	- CompanyId
'	- LastName
'	- Ssn
'	- StateId
'	- BranchId
'	- Course
'	- ProviderId
'	- CreditHours
'	- LicenseNumber
'	- LicExpTo
'	- LicExpFrom
'	- LicenseStatusId
'	- ShowOfficers
'	- ShowCompanies
'	- ShowBranches
'	- ShowCompanyL2s
'	- ShowCompanyL3s
'	- ShowCourses
'	- ShowLicenses
'	- ShowInactive
'	- ShowBranchAdmins
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- CheckDateParam
'	- QueryDb
'	- ReportError
' Public Methods
'	- LoadReportById
'	- SaveReport
'	- SearchReports
'==============================================================================

class SavedReport

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_bVocalErrors
	
	'Settings properties
	dim g_iReportId
	dim g_sName
	dim g_iCompanyId
	dim g_bDeleted
	dim g_sLastName
	dim g_sSsn
	dim g_iStateId
	dim g_iBranchId
	dim g_sStateIdList
	dim g_sBranchIdList
	dim g_sCourse
	dim g_iProviderId
	dim g_iCreditHours
	dim g_sLicenseNumber
	dim g_dLicExpTo
	dim g_dLicExpFrom
	dim g_iLicenseStatusId
	dim g_bShowOfficers
	dim g_bShowCompanies
	dim g_bShowBranches
	dim g_bShowCompanyL2s
	dim g_bShowCompanyL3s
	dim g_bShowCourses
	dim g_bShowLicenses
	dim g_bShowInactive
	dim g_iShowBranchAdmins
	
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property


	'Unique ID for this saved Report
	public property get ReportId()
		ReportId = g_iReportId
	end property
	
	
	'Name for this saved Report
	public property get Name()
		Name = g_sName
	end property
	public property let Name(p_sName)
		if p_sName = "" then 
			g_sName = "" 
		else
			g_sName = left(p_sName, 50)
		end if
	end property
	
	
	'ID of the Company that owns this saved report
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = clng(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Boolean required.")
		end if
	end property
	
	
	'Has this saved record been deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = false
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property
	
	
	'Last name search
	public property get LastName()
		LastName = g_sLastName
	end property
	public property let LastName(p_sLastName)
		if p_sLastName = "" then 
			g_sLastName = "" 
		else
			g_sLastName = left(p_sLastName, 50)
		end if
	end property
	
	
	'SSN search
	public property get Ssn()
		Ssn = g_sSsn
	end property
	public property let Ssn(p_sSsn)
		if p_sSsn = "" then
			g_sSsn = ""
		else 
			g_sSsn = left(p_sSsn, 11)
		end if 
	end property
	
	
	'State ID search
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then	
			g_iStateId = clng(p_iStateId)
		elseif p_iStateId = "" then
			g_iStateId = ""
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Branch ID search
	public property get BranchId()
		BranchId = g_iBranchId
	end property 
	public property let BranchId(p_iBranchId)
		if isnumeric(p_iBranchId) then
			g_iBranchId = clng(p_iBranchId)
		elseif p_iBranchId = "" then
			g_iBranchId = ""
		else
			ReportError("Invalid BranchId value.  Integer required.")
		end if
	end property
	
	
	'State ID List search
	public property get StateIdList()
		StateIdList = g_sStateIdList
	end property
	public property let StateIdList(p_sStateIdList)
		g_sStateIdList = p_sStateIdList
	end property


	'Branch ID List search
	public property get BranchIdList()
		BranchIdList = g_sBranchIdList
	end property
	public property let BranchIdList(p_sBranchIdList)
		g_sBranchIdList = p_sBranchIdList
	end property
	
	
	'Course name search
	public property get Course()
		Course = g_sCourse
	end property
	public property let Course(p_sCourse)
		if p_sCourse = "" then
			g_sCourse = "" 
		else
			g_sCourse = left(p_sCourse, 50)
		end if
	end property
	
	
	'Provider ID search
	public property get ProviderId()
		ProviderId = g_iProviderId
	end property
	public property let ProviderId(p_iProviderId)
		if isnumeric(p_iProviderId) then
			g_iProviderId = clng(p_iProviderId)
		elseif p_iProviderId = "" then
			g_iProviderId = ""
		else
			ReportError("Invalid ProviderId value.  Integer required.")
		end if
	end property
	
	
	'Credit Hours search
	public property get CreditHours()
		CreditHours = g_iCreditHours
	end property
	public property let CreditHours(p_iCreditHours)
		if isnumeric(p_iCreditHours) then
			g_iCreditHours = clng(p_iCreditHours)
		elseif p_iCreditHours = "" then
			g_iCreditHours = ""
		else
			ReportError("Invalid CreditHours value.  Integer required.")
		end if
	end property			
	
	
	'License Number search
	public property get LicenseNumber()
		LicenseNumber = g_sLicenseNumber
	end property
	public property let LicenseNumber(p_sLicenseNumber)
		if p_sLicenseNumber = "" then 
			g_sLicenseNumber = "" 
		else
			g_sLicenseNumber = left(p_sLicenseNumber, 50)
		end if
	end property
	
	
	'License Expiration To search
	public property get LicExpTo()
		LicExpTo = g_dLicExpTo
	end property
	public property let LicExpTo(p_dLicExpTo)
		if isdate(p_dLicExpTo) then
			g_dLicExpTo = cdate(p_dLicExpTo)
		elseif p_dLicExpTo = "" then
			g_dLicExpTo = ""
		else
			ReportError("Invalid LicExpTo value.  Date required.")
		end if
	end property


	'License Expiration From search
	public property get LicExpFrom()
		LicExpFrom = g_dLicExpFrom
	end property
	public property let LicExpFrom(p_dLicExpFrom)
		if isdate(p_dLicExpFrom) then
			g_dLicExpFrom = cdate(p_dLicExpFrom)
		elseif p_dLicExpFrom = "" then
			g_dLicExpFrom = ""
		else
			ReportError("Invalid LicExpFrom value.  Date required.")
		end if
	end property	
	
	
	'License Status ID search
	public property get LicenseStatusId()
		LicenseStatusId = g_iLicenseStatusId
	end property
	public property let LicenseStatusId(p_iLicenseStatusId)
		if isnumeric(p_iLicenseStatusId) then
			g_iLicenseStatusId = clng(p_iLicenseStatusId)
		elseif p_iLicenseStatusId = "" then
			g_iLicenseStatusId = ""
		else
			ReportError("Invalid LicenseStatusId value.  Integer required.")
		end if
	end property
	
	
	'Do we show officers in the search?
	public property get ShowOfficers()
		ShowOfficers = g_bShowOfficers
	end property
	public property let ShowOfficers(p_bShowOfficers)
		if isnumeric(p_bShowOfficers) then
			g_bShowOfficers = cbool(p_bShowOfficers)
		elseif p_bShowOfficers = "" then
			g_bShowOfficers = false
		else
			ReportError("Invalid ShowOfficers value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companies in the search?
	public property get ShowCompanies()
		ShowCompanies = g_bShowCompanies
	end property
	public property let ShowCompanies(p_bShowCompanies)
		if isnumeric(p_bShowCompanies) then
			g_bShowCompanies = cbool(p_bShowCompanies)
		elseif p_bShowCompanies = "" then
			g_bShowCompanies = false
		else
			ReportError("Invalid ShowCompanies value.  Boolean required.")
		end if
	end property
	
	
	'Do we show branches in the search?
	public property get ShowBranches()
		ShowBranches = g_bShowBranches
	end property
	public property let ShowBranches(p_bShowBranches)
		if isnumeric(p_bShowBranches) then
			g_bShowBranches = cbool(p_bShowBranches)
		elseif p_bShowBranches = "" then
			g_bShowBranches = false
		else
			ReportError("Invalid ShowBranches value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companyl2s in the search?
	public property get ShowCompanyL2s()
		ShowCompanyL2s = g_bShowCompanyL2s
	end property
	public property let ShowCompanyL2s(p_bShowCompanyL2s)
		if isnumeric(p_bShowCompanyL2s) then
			g_bShowCompanyL2s = cbool(p_bShowCompanyL2s)
		elseif p_bShowCompanyL2s = "" then
			g_bShowCompanyL2s = false
		else
			ReportError("Invalid ShowCompanyL2s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show companyl3s in the search?
	public property get ShowCompanyL3s()
		ShowCompanyL3s = g_bShowCompanyL3s
	end property
	public property let ShowCompanyL3s(p_bShowCompanyL3s)
		if isnumeric(p_bShowCompanyL3s) then
			g_bShowCompanyL3s = cbool(p_bShowCompanyL3s)
		elseif p_bShowCompanyL3s = "" then
			g_bShowCompanyL3s = false
		else
			ReportError("Invalid ShowCompanyL3s value.  Boolean required.")
		end if
	end property
	
	
	'Do we show courses in the search?
	public property get ShowCourses()
		ShowCourses = g_bShowCourses
	end property
	public property let ShowCourses(p_bShowCourses)
		if isnumeric(p_bShowCourses) then
			g_bShowCourses = cbool(p_bShowCourses)
		elseif p_bShowCourses = "" then
			g_bShowCourses = false
		else
			ReportError("Invalid ShowCourses value.  Boolean required.")
		end if
	end property
	
	
	'Do we show licenses in the search?
	public property get ShowLicenses()
		ShowLicenses = g_bShowLicenses
	end property
	public property let ShowLicenses(p_bShowLicenses)
		if isnumeric(p_bShowLicenses) then
			g_bShowLicenses = cbool(p_bShowLicenses)
		elseif p_bShowLicenses = "" then
			g_bShowLicenses = false
		else
			ReportError("Invalid ShowLicenses value.  Boolean required.")
		end if
	end property
	
	
	'Do we show only inactive officers in the search?
	public property get ShowInactive()
		ShowInactive = g_bShowInactive
	end property
	public property let ShowInactive(p_bShowInactive)
		if isnumeric(p_bShowInactive) then
			g_bShowInactive = cbool(p_bShowInactive)
		elseif p_bShowInactive = "" then
			g_bShowInactive = false
		else
			ReportError("Invalid ShowInactive value.  Boolean required.")
		end if
	end property
	

	'Show Branch Admin search
	public property get ShowBranchAdmins()
		ShowBranchAdmins = g_iShowBranchAdmins
	end property
	public property let ShowBranchAdmins(p_iShowBranchAdmins)
		if isnumeric(p_iShowBranchAdmins) then
			g_iShowBranchAdmins = clng(p_iShowBranchAdmins)
		elseif p_iShowBranchAdmins = "" then
			g_iShowBranchAdmins = ""
		else
			ReportError("Invalid ShowBranchAdmins value.  Integer required.")
		end if
	end property
	
	
	' PRIVATE METHODS =========================================================

	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		if isnull(g_sConnectionString) then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		'Response.Write(sSql)
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	



	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadReportById
	' Desc: Load a report object based on the passed ReportId
	' Preconditions: ConnectionString
	' Inputs: p_iReportId - ID of report to load
	' Returns: Loaded ID if successful, 0 if not.
	' -------------------------------------------------------------------------
	public function LoadReportById(p_iReportId)
	
		LoadReportById = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(p_iReportId, 0, "Report ID") then
			exit function
		end if
		
		
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM SavedReports " & _
			"WHERE ReportId = '" & p_iReportId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the properties
			g_iReportId = oRs("ReportId")
			g_sName = oRs("Name")
			g_iCompanyId = oRs("CompanyId")
			g_bDeleted = oRs("Deleted")
			g_sLastName = oRs("LastName")
			g_sSsn = oRs("Ssn")
			g_iStateId = oRs("StateId")
			g_iBranchId = oRs("BranchId")
			g_sStateIdList = oRs("StateIdList")
			g_sBranchIdList = oRs("BranchIdList")
			g_sCourse = oRs("Course")
			g_iProviderId = oRs("ProviderId")
			g_iCreditHours = oRs("CreditHours")
			g_sLicenseNumber = oRs("LicenseNumber")
			g_dLicExpTo = oRs("LicExpTo")
			g_dLicExpFrom = oRs("LicExpFrom")
			g_iLicenseStatusId = oRs("LicenseStatusId")
			g_bShowOfficers = oRs("ShowOfficers")
			g_bShowCompanies = oRs("ShowCompanies")
			g_bShowBranches = oRs("ShowBranches")
			g_bShowCompanyL2s = oRs("ShowCompanyL2s")
			g_bShowCompanyL3s = oRs("ShowCompanyL3s")
			g_bShowCourses = oRs("ShowCourses")
			g_bShowLicenses = oRs("ShowLicenses")
			g_bShowInactive = oRs("ShowInactive")
			g_iShowBranchAdmins = oRs("ShowBranchAdmins")
			
			LoadReportById = g_iReportId
			
		else
			
			ReportError("Unable to load the passed Settings ID.")
			exit function
			
		end if
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SaveReport
	' Desc: Save the object's properties to the database.
	' Preconditions: ConnectionString, g_iCompanyId
	' Returns: If successful, returns the saved/new ID, otherwise 0.
	' -------------------------------------------------------------------------
	public function SaveReport()
		
		SaveReport = 0
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckIntParam(g_iCompanyId, 0, "Company ID") then
			exit function
		end if
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset object
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = Server.CreateObject("ADODB.Connection")
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		'Find out if a Report with this name already exists
		sSql = "SELECT ReportId FROM SavedReports " & _
			"WHERE Name LIKE '" & g_sName & "' " & _
			"AND CompanyId = '" & g_iCompanyId & "'"
		set oRs = oConn.Execute(sSql)
		if not (oRs.EOF and oRs.BOF) then
			g_iReportId = oRs("ReportId")
		end if 
		
		
		'If we have a ReportId property assigned already, we're working with a
		'loaded Report, so we'll UPDATE.  Otherwise, we INSERT.
		if g_iReportId <> "" then
			
			sSql = "UPDATE SavedReports SET " & _
				"Name = '" & g_sName & "', " & _
				"CompanyId = '" & g_iCompanyId & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "', " & _
				"LastName = '" & g_sLastName & "', " & _
				"Ssn = '" & g_sSsn & "', " & _
				"StateId = '" & g_iStateId & "', " & _
				"BranchId = '" & g_iBranchId & "', " & _
				"StateIdList = '" & g_sStateIdList & "', " & _
				"BranchIdList = '" & g_sBranchIdList & "', " & _
				"Course = '" & g_sCourse & "', " & _
				"ProviderId = '" & g_iProviderId & "', " & _
				"CreditHours = '" & g_iCreditHours & "', " & _
				"LicenseNumber = '" & g_sLicenseNumber & "', "
			if g_dLicExpTo <> "" then
				sSql = sSql & "LicExpTo = '" & g_dLicExpTo & "', "
			else
				sSql = sSql & "LicExpTo = NULL, "
			end if
			if g_dLicExpFrom <> "" then	
				sSql = sSql & "LicExpFrom = '" & g_dLicExpFrom & "', "
			else
				sSql = sSql & "LicexpFrom = NULL, "
			end if
			sSql = sSql & "LicenseStatusId = '" & g_iLicenseStatusId & _
				"', " & _
				"ShowOfficers = '" & abs(cint(g_bShowOfficers)) & "', " & _
				"ShowCompanies = '" & abs(cint(g_bShowCompanies)) & "', " & _
				"ShowBranches = '" & abs(cint(g_bShowBranches)) & "', " & _
				"ShowCompanyL2s = '" & abs(cint(g_bShowCompanyL2s)) & "', " & _
				"ShowCompanyL3s = '" & abs(cint(g_bShowCompanyL3s)) & "', " & _
				"ShowCourses = '" & abs(cint(g_bShowCourses)) & "', " & _
				"ShowLicenses = '" & abs(cint(g_bShowLicenses)) & "', " & _
				"ShowInactive = '" & abs(cint(g_bShowInactive)) & "', " & _
				"ShowBranchAdmins = '" & g_iShowBranchAdmins & "' " & _
				"WHERE ReportId = '" & g_iReportId & "'"
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected > 0 then
				SaveReport = g_iReportId
			else
				ReportError("Unable to save existing Report.")
				exit function
			end if
			
		else
		
			sSql = "INSERT INTO SavedReports (" & _
				"Name, " & _
				"CompanyId, " & _
				"Deleted, " & _
				"LastName, " & _
				"Ssn, " & _
				"StateId, " & _
				"BranchId, " & _
				"StateIdList, " & _
				"BranchIdList, " & _
				"Course, " & _
				"ProviderId, " & _
				"CreditHours, " & _
				"LicenseNumber, " & _
				"LicExpTo, " & _
				"LicExpFrom, " & _
				"LicenseStatusId, " & _
				"ShowOfficers, " & _
				"ShowCompanies, " & _
				"ShowBranches, " & _
				"ShowCompanyL2s, " & _
				"ShowCompanyL3s, " & _
				"ShowCourses, " & _
				"ShowLicenses, " & _
				"ShowInactive, " & _
				"ShowBranchAdmins " & _
				") VALUES (" & _
				"'" & g_sName & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & abs(cint(g_bDeleted)) & "', " & _
				"'" & g_sLastName & "', " & _
				"'" & g_sSsn & "', " & _
				"'" & g_iStateId & "', " & _
				"'" & g_iBranchId & "', " & _
				"'" & g_sStateIdList & "', " & _
				"'" & g_sBranchIdList & "', " & _
				"'" & g_sCourse & "', " & _
				"'" & g_iProviderId & "', " & _
				"'" & g_iCreditHours & "', " & _
				"'" & g_sLicenseNumber & "', "
			if g_dLicExpTo <> "" then
				sSql = sSql & "'" & g_dLicExpTo & "', "
			else
				sSql = sSql & "NULL, " 
			end if
			if g_dLicExpFrom <> "" then 
				sSql = sSql & "'" & g_dLicExpFrom & "', "
			else
				sSql = sSql & "NULL, "
			end if
			sSql = sSql & "'" & g_iLicenseStatusId & "', " & _
				"'" & abs(cint(g_bShowOfficers)) & "', " & _
				"'" & abs(cint(g_bShowCompanies)) & "', " & _
				"'" & abs(cint(g_bShowBranches)) & "', " & _
				"'" & abs(cint(g_bShowCompanyL2s)) & "', " & _
				"'" & abs(cint(g_bShowCompanyL3s)) & "', " & _
				"'" & abs(cint(g_bShowCourses)) & "', " & _
				"'" & abs(cint(g_bShowLicenses)) & "', " & _
				"'" & abs(cint(g_bShowInactive)) & "', " & _
				"'" & g_iShowBranchAdmins & "' " & _
				")"
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new Report
			sSql = "SELECT ReportId FROM SavedReports " & _
				"WHERE Name = '" & g_sName & "'" & _
				"ORDER BY ReportId DESC"
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
				g_iReportId = oRs("ReportId")
				SaveReport = g_iReportId
			else
				'Record could not be retrieved
				ReportError("Failed to save new Report.")
				exit function
			end if
		
		end if
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchReports
	' Desc: Retrieves a collection of ReportIds that match the properties of 
	'	the current object.
	' Preconditions: ConnectionString
	' Returns: Recordset of the results
	' -------------------------------------------------------------------------
	public function SearchReports()
	
		set SearchReports = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		dim bFirstClause 'Used to direct SQL phrasing
		dim sSql
		
		sSql = "SELECT SRs.ReportId, SRs.Name " & _
			"FROM SavedReports AS SRs "
		
			
		'If CompanyId search
		if g_iCompanyId <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "Bs.CompanyId = '" & g_iCompanyId & "'"
		end if 	
		
		
		'If Deleted search
		if g_bDeleted <> "" then
			if bFirstClause then
				sSql = sSql & " WHERE "
				bFirstClause = false
			else
				sSql = sSql & " AND "
			end if
			sSql = sSql & "SRs.Deleted = '" & abs(cint(g_bDeleted)) & "'"
		end if 	
			
		
		sSql = sSql & " ORDER BY SRs.Name"
		
		set SearchReports = QueryDb(sSql)
		
	end function
		
	
end class
%>

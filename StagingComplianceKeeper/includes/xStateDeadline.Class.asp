<%
'==============================================================================
' Class: StateDeadline
' Controls the creation, modification, and removal of state course expirations
' Create Date: 7/25/05
' Author: Mark Silvia
' --------------------
' Properties
'	- ConnectionString
'	- VocalErrors
'	- DeadlineId
'	- CompanyId
'	- StateId
'	- ExpDate
'	- Description
'	- Deleted
'	- SearchExpDateStart
'	- SearchExpDateEnd
'
' Private Methods
'	- Class_Initialize
'	- Class_Terminate
'	- CheckConnectionString
'	- CheckIntParam
'	- CheckStrParam
'	- QueryDb
'	- ReportError
'
' Methods
'	- LoadDeadlineById
'	- SaveDeadline
'	- SearchDeadlines
'	- LookupState
'==============================================================================

class StateDeadline

	' GLOBAL VARIABLE DECLARATIONS ============================================
	
	'Misc properties
	dim g_sConnectionString
	dim g_sTpConnectionString
	dim g_bVocalErrors
	
	'Deadline properties
	dim g_iDeadlineId
	dim g_iCompanyId
	dim g_iCourseId
	dim g_iStateId
	dim g_dExpDate
	dim g_sDescription
	dim g_bDeleted
	dim g_bStatewide
	dim g_dSearchExpDateStart
	dim g_dSearchExpDateEnd
	dim g_sSearchBranchIdList
	dim g_sSearchCompanyL2IdList
	dim g_sSearchCompanyL3IdList
	
	
	' PUBLIC PROPERTIES =======================================================
	
	'Database connection string
	public property let ConnectionString(p_sConnectionString)
		g_sConnectionString = p_sConnectionString
	end property
	public property get ConnectionString()
		ConnectionString = g_sConnectionString
	end property
	
	
	'Stores/retrieves the trainingpro database connection string
	public property let TpConnectionString(p_sTpConnectionString)
		g_sTpConnectionString = p_sTpConnectionString
	end property
	public property get TpConnectionString()
		TpConnectionString = g_sTpConnectionString
	end property
	
	
	'Do we report errors or just return error codes?  Setting this flag affects
	'the way the ReportErrors function works.
	public property get VocalErrors()
		VocalErrors = g_bVocalErrors()
	end property
	public property let VocalErrors(p_bVocalErrors)
		if isnumeric(p_bVocalErrors) then
			g_bVocalErrors = cbool(p_bVocalErrors)
		else
			ReportError("Invalid VocalErrors value.  Boolean required.")
		end if
	end property
	
	
	'Unique ID for this deadline.  Read only.
	public property get DeadlineId()
		DeadlineId = g_iDeadlineId
	end property
	
	
	'Company ID of the company that owns this deadline
	public property get CompanyId()
		CompanyId = g_iCompanyId
	end property
	public property let CompanyId(p_iCompanyId)
		if isnumeric(p_iCompanyId) then
			g_iCompanyId = cint(p_iCompanyId)
		elseif p_iCompanyId = "" then
			g_iCompanyId = ""
		else
			ReportError("Invalid CompanyId value.  Integer required.")
		end if
	end property
	
	
	'Course ID to which this deadline applies.
	public property get CourseId()
		CourseId = g_iCourseId
	end property
	public property let CourseId(p_iCourseId)
		if isnumeric(p_iCourseId) then
			g_iCourseId = cint(p_iCourseId)
		elseif p_iCourseId = "" then
			g_iCourseId = ""
		else
			ReportError("Invalid CourseId value.  Integer required.")
		end if
	end property
	
	
	'State ID to which this deadline applies.
	public property get StateId()
		StateId = g_iStateId
	end property
	public property let StateId(p_iStateId)
		if isnumeric(p_iStateId) then
			g_iStateId = cint(p_iStateId)
		elseif p_iStateId = "" then
			g_iStateId = "" 
		else
			ReportError("Invalid StateId value.  Integer required.")
		end if
	end property
	
	
	'Use state rather than courses
	public property get Statewide()
		Statewide = g_bStatewide
	end property
	public property let Statewide(p_bStatewide)
		if isnumeric(p_bStatewide) then
			g_bStatewide = cbool(p_bStatewide)
		elseif p_bStatewide = "" then
			g_bStatewide = ""
		else
			ReportError("Invalid Statewide value.  Boolean required.")
		end if
	end property
	
	
	'Expiration date
	public property get ExpDate()
		ExpDate = g_dExpDate
	end property
	public property let ExpDate(p_dExpDate)
		if isdate(p_dExpDate) then 
			g_dExpDate = cdate(p_dExpDate)
		elseif p_dExpDate = "" then
			g_dExpDate = ""
		else
			ReportError("Invalid ExpDate value.  Date required.")
		end if
	end property
	
	
	'Has this deadline been deleted?
	public property get Deleted()
		Deleted = g_bDeleted
	end property
	public property let Deleted(p_bDeleted)
		if isnumeric(p_bDeleted) then
			g_bDeleted = cbool(p_bDeleted)
		elseif p_bDeleted = "" then
			g_bDeleted = "" 
		else
			ReportError("Invalid Deleted value.  Boolean required.")
		end if
	end property


	'Date value used as the date to search from when searching expiration dates
	public property get SearchExpDateStart()
		SearchExpDateStart = g_dSearchExpDateStart
	end property
	public property let SearchExpDateStart(p_dSearchExpDateStart)
		if isdate(p_dSearchExpDateStart) then 
			g_dSearchExpDateStart = cdate(p_dSearchExpDateStart)
		elseif p_dSearchExpDateStart = "" then
			g_dSearchExpDateStart = ""
		else
			ReportError("Invalid SeachExpDateStart value.  Date required.")
		end if
	end property


	'Date value used as the date to search to when searching expiration dates
	public property get SearchExpDateEnd()
		SearchExpDateEnd = g_dSearchExpDateEnd
	end property
	public property let SearchExpDateEnd(p_dSearchExpDateEnd)
		if isdate(p_dSearchExpDateEnd) then 
			g_dSearchExpDateEnd = cdate(p_dSearchExpDateEnd)
		elseif p_dSearchExpDateEnd = "" then
			g_dSearchExpDateEnd = ""
		else
			ReportError("Invalid SeachExpDateEnd value.  Date required.")
		end if
	end property		
	
	
	'List of CompanyL2 IDs to search within.  Used to find associates from
	'multiple companyL2s rather than just one.
	public property get SearchCompanyL2IdList()
		SearchCompanyL2IdList = g_sSearchCompanyL2IdList
	end property
	public property let SearchCompanyL2IdList(p_sSearchCompanyL2IdList)
		g_sSearchCompanyL2IdList = p_sSearchCompanyL2IdList
	end property


	'List of CompanyL3 IDs to search within.  Used to find associates from
	'multiple companyL3s rather than just one.
	public property get SearchCompanyL3IdList()
		SearchCompanyL3IdList = g_sSearchCompanyL3IdList
	end property
	public property let SearchCompanyL3IdList(p_sSearchCompanyL3IdList)
		g_sSearchCompanyL3IdList = p_sSearchCompanyL3IdList
	end property
	
	
	'List of Branch IDs to search within.  Used to find associates from
	'multiple branches rather than just one.
	public property get SearchBranchIdList()
		SearchBranchIdList = g_sSearchBranchIdList
	end property
	public property let SearchBranchIdList(p_sSearchBranchIdList)
		g_sSearchBranchIdList = p_sSearchBranchIdList
	end property
	
	
	
	' PRIVATE METHODS =========================================================
	
	' -------------------------------------------------------------------------
	' Name:				Class_Initialize
	' Description:		Sub that runs when object is instantiated.
	' -------------------------------------------------------------------------
	private sub Class_Initialize()
	end sub
	
	
	' -------------------------------------------------------------------------
	' Name:				Class_Terminate
	' Description:		Sub that runs when object is terminated.
	' -------------------------------------------------------------------------
	private sub Class_Terminate()
	end sub
	

	' -------------------------------------------------------------------------
	' Name:	CheckConnectionString
	' Desc:	Checks for a valid database connectionstring
	' Inputs: None
	' Outputs: None
	' Returns: TRUE if present, or FALSE
	' Pre-conditions: None
	' -------------------------------------------------------------------------
	private function CheckConnectionString
	
		CheckConnectionString = false
	
		if isnull(g_sConnectionString) or _
			g_sConnectionString = "" then
				
			ReportError("ConnectionString property must be set.")		
			CheckConnectionString = false
		
		elseif isnull(g_sTpConnectionString) or _
			g_sTpConnectionString = "" then 
			
			ReportError("Alternate ConnectionString property must be set.")
			CheckConnectionString = false
		
		else
		
			CheckConnectionString = true
		
		end if
	
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: CheckIntParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid integer
	' Preconditions: None
	' Inputs: p_iInt - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckIntParam(p_iInt, p_bAllowNull, p_sName)
	
		CheckIntParam = true
		
		'Make sure this is an int
		if isnumeric(p_iInt) then
			
			if p_iInt = "" and not p_bAllowNull then
				
				ReportError(p_sName & " must not be blank for this operation.")
				CheckIntParam = false

			end if 
			
		else
		
			ReportError(p_sName & " must be a valid integer for this " & _
						"operation.")
			CheckIntParam = false
			
		end if
			
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckStrParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'       valid string
	' Preconditions: None
	' Inputs: p_sStr - Integer to check
	'         p_bAllowNull - Allow blank values?
	'         p_sName - Name of the variable for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckStrParam(p_sStr, p_bAllowNull, p_sName)
	
		CheckStrParam = true
		
		if p_sStr = "" and not p_bAllowNull then
				
			ReportError(p_sName & " must not be blank for this operation.")
			CheckStrParam = false

		end if
		
	end function  
	
	
	' -------------------------------------------------------------------------
	' Name: CheckDateParam
	' Desc: Verifies that a value actually is, or can be interpreted as, a
	'		valid date.
	' Inputs: p_dDate - Date to check
	'		  p_bAllowNull - Allow blank or null values?
	'		  p_sName - Name of the value for error reporting purposes
	' Returns: True if present, false if not
	' -------------------------------------------------------------------------
	private function CheckDateParam(p_dDate, p_bAllowNull, p_sName)
			
		CheckDateParam = true
		
		if isnull(p_dDate) and not p_bAllowNull then
			ReportError(p_sName & " must not be null for this operation.")
			CheckDateParam = false
		elseif p_dDate = "" and not p_bAllowNull then
			ReportError(p_sName & " must not be blank for this operation.")
			CheckDateParam = false
		elseif not isdate(p_dDate) then
			ReportError(p_sName & " must be a valid date for this operation.")
			CheckDateParam = false
		end if 
		
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryDB
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryDB(sSQL)
	
		set QueryDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
		
		'Response.Write(vbCrLf & "<!--" & sSql & "-->" & vbCrLf)
	    
	    Connect.Open g_sConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryDB = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name:					QueryTpDb
	' Purpose:				executes the query in the database
	' Required Properties:  ConnectionString
	' Optional Properties:	n/a
	' Parameters:			sSQL - the query to execute
	' Outputs:				a recordset of the executed query is returned
	' -------------------------------------------------------------------------
	private function QueryTpDb(sSQL)
	
		set QueryTpDb = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		Dim Connect	'as connection
		Dim objRS 'as recordset
    
		set Connect = CreateObject("ADODB.Connection")
		set objRS = CreateObject("ADODB.Recordset")
	    
	    Connect.Open g_sTpConnectionString
		objRs.cursorlocation = 3
		objRs.CursorType = 3
	    objRS.Open sSQL, connect
	    objRs.ActiveConnection = nothing
	
		Set QueryTpDb = objRS

		set Connect = Nothing	
		set objRS = Nothing
	end function


	' -------------------------------------------------------------------------
	' Name: ReportError
	' Description: Depending on the current settings, optionally prints an
	'			   error to the current response.
	' Input: p_sError - Error text to print
    ' Output: Prints to response
    ' Pre-conditions: None.
	' -------------------------------------------------------------------------
	private sub ReportError(p_sError)
	
		'if the Vocal Errors switch is activated, report errors.
		if g_bVocalErrors then

			Response.Write("Error: " & p_sError & "<br>" & vbCrLf) 
		
		end if
	
	end sub	
	
	
	
	' PUBLIC METHODS ==========================================================
	
	' -------------------------------------------------------------------------
	' Name: LoadDeadlineById
	' Desc: Load a deadline based on the passed ID
	' Preconditions: ConnectionString
	' Inputs: p_iDeadlineId - Unique ID to load
	' Returns: Loaded ID if successful, 0 if not
	' -------------------------------------------------------------------------
	public function LoadDeadlineById(p_iDeadlineId)
	
		LoadDeadlineById = 0
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iDeadlineId, 0, "Deadline ID") then
			exit function
		end if
		
		
		dim oRs 'Recordset
		dim sSql 'SQL statement
		
		set oRs = Server.CreateObject("ADODB.Recordset")
		
		sSql = "SELECT * FROM StateDeadlines " & _
			"WHERE DeadlineId = '" & p_iDeadlineId & "'"
		set oRs = QueryDb(sSql)
		
		if not (oRs.BOF and oRs.EOF) then
		
			'We got a record back, so load the object properties
			g_iDeadlineId = oRs("DeadlineId")
			g_iStateId = oRs("StateId")
			g_iCompanyId = oRs("CompanyId")
			g_iCourseId = oRs("CourseId")
			g_bStatewide = oRs("Statewide")
			g_dExpDate = oRs("ExpDate")
			g_bDeleted = oRs("Deleted") 

			LoadDeadlineById = g_iDeadlineId
			
		else
			
			ReportError("Unable to load the passed Deadline ID.")
			exit function
			
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: Save Deadline
	' Desc: Saves the object's properties to the database
	' Preconditions: ConnectionString, DeadlineId, StateId, ExpDate
	' Returns: If successful, returns the saved/new ID, otherwise 0
	' -------------------------------------------------------------------------
	public function SaveDeadline()
	
		SaveDeadline = 0 
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckDateParam(g_dExpDate, 0, "Expiration Date") then
			exit function
		end if 
		
		
		dim oConn 'DB Connection
		dim oRs 'Recordset
		dim sSql 'SQL statement
		dim lRecordsAffected 'Number of records affected by an execute
		
		set oConn = server.CreateObject("ADODB.Connection")
		set oRs = server.CreateObject("ADODB.Recordset")
	
		oConn.ConnectionString = g_sConnectionString
		oConn.Open
		
		
		'If we have a DeadlineId property assigned already, we're working on a 
		'loaded document, so we'll do an UPDATE.  Otherwise we do an INSERT.
		dim bIsNew
		if g_iDeadlineId <> "" then
		
			bIsNew = false
			sSql = "UPDATE StateDeadlines SET " & _
				"StateId = '" & g_iStateId & "', " & _
				"CourseId = '" & g_iCourseId & "', " & _
				"CompanyId = '" & g_iCompanyId & "', " & _
				"Statewide = '" & abs(cint(g_bStatewide)) & "', " & _
				"ExpDate = '" & g_dExpDate & "', " & _
				"Deleted = '" & abs(cint(g_bDeleted)) & "' " & _
				"WHERE DeadlineId = '" & g_iDeadlineId & "'"
			'Response.Write(sSql)
			'Response.End
			oConn.Execute sSql, lRecordsAffected
			
			if lRecordsAffected = 1 then
				
				SaveDeadline = g_iDeadlineId 
				
			else
				
				ReportError("Unable to save existing Deadline.")
				exit function
				
			end if
			
		else
		
			bIsNew = true
			sSql = "INSERT INTO StateDeadlines (" & _
				"StateId, " & _
				"CompanyId, " & _
				"CourseId, " & _
				"Statewide, " & _
				"ExpDate, " & _
				"Deleted " & _
				") VALUES (" & _
				"'" & g_iStateId & "', " & _
				"'" & g_iCompanyId & "', " & _
				"'" & g_iCourseId & "', " & _
				"'" & abs(cint(g_bStatewide)) & "', " & _
				"'" & g_dExpDate & "', " & _
				"'" & abs(cint(g_bDeleted)) & "' " & _
				")"
			'Response.Write(sSql)
			oConn.Execute sSql, lRecordsAffected
			
			'Retrieve the new deadline
			sSql = "SELECT DeadlineId FROM StateDeadlines " & _
				"WHERE StateId = '" & g_iStateId & "' " & _
				"AND CompanyId = '" & g_iCompanyId & "' " & _
				"AND CourseId = '" & g_iCourseId & "' " & _
				"AND ExpDate = '" & g_dExpDate & "' " & _
				"ORDER BY DeadlineId DESC"
			'Response.Write(sSql)
			set oRs = oConn.Execute(sSql)
			
			if not (oRs.EOF and oRs.BOF) then
			
				g_iDeadlineId = oRs("DeadlineId")
				SaveDeadline = g_iDeadlineId
				
			else
				
				'The record could not be retrieved 
				ReportError("Failed to save the new Deadline.")
				exit function
				
			end if
			
		end if
		
		oConn.Close
		set oRs = nothing
		set oConn = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: SearchDeadlines 
	' Desc: Retrieves a collection of Deadline IDs that match the properties of
	'	the current object instance.
	' Preconditions: ConnectionString
	' Returns: Resulting recordset
	' -------------------------------------------------------------------------
	public function SearchDeadlines()
		
		set SearchDeadlines = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		end if
		
		
		dim sSql 'SQL statement
		dim oRs 'Recordset object
			
		sSql = "SELECT DISTINCT SDs.DeadlineId, SDs.ExpDate " & _
			"FROM StateDeadlines AS SDs " & _
			"WHERE (NOT SDs.Deleted = '1') " 
		
		'If StateId search
		if g_iStateId <> "" then
			sSql = sSql & "AND SDs.StateId = '" & g_iStateId & "' "
		end if
		
		'If companyId search
		if g_iCompanyId <> "" then
			sSql = sSql & "AND SDs.CompanyId = '" & g_iCompanyId & "' "
		end if
		
		'If SearchExpDateStart search
		if g_dSearchExpDateStart <> "" then
			sSql = sSql & "AND SDs.ExpDate >= '" & g_dSearchExpDateStart & "' "
		end if
		
		'If SearchExpDateEnd search
		if g_dSearchExpDateEnd <> "" then
			sSql = sSql & "AND SDs.ExpDate <= '" & g_dSearchExpDateEnd & "' "
		end if
		
		sSql = sSql & "ORDER BY SDs.ExpDate ASC"
		
		'Response.write(sSql & "<p>")
		
		set SearchDeadlines = QueryDb(sSql)
		
	end function 
	
	
	' -------------------------------------------------------------------------
	' Name: SearchExpirationsByTime
	' Desc: Retrieves the number of loan officers that have courses that will 
	'	expire within the given date range.
	' Input: p_dStartDate, p_dEndDate
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Integer count of the number of loan officers
	' -------------------------------------------------------------------------
	public function SearchExpirationsByTime(p_dStartDate, p_dEndDate, _
		p_iStateId, p_iBranchId, p_sSearchText, p_sLastName)
	
		'SearchExpirationsByTime = 0
		SearchExpirationsByTime = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckDateParam(p_dStartDate, 0, "Start Date") or _
			not CheckDateParam(p_dEndDate, 0, "End Date") then
			exit function
		end if
		
		dim sSql 'SQL statement
		dim oRs 'Recordset object
		dim bFirstClause
		bFirstClause = false
		
		'We need to override the user's search if there are state deadlines
		'set for this state.
		
		
		sSql = "SELECT DISTINCT ACs.UserId " & _
			"FROM vAssociatesCoursesCKUserId AS ACs " & _
			"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
			"LEFT JOIN vCourses AS Cs ON (Cs.CourseId = ACs.CourseId) " & _
			"WHERE ACs.CertificateExpirationDate > '" & p_dStartDate & "' " & _
			"AND ACs.CertificateExpirationDate < '" & p_dEndDate & "' " & _
			"AND NOT ACs.Deleted = '1' " & _
			"AND NOT ACs.UserId IS NULL " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if session("UserTpLinkCompanyId") <> "" then 
			sSql = sSql & "OR ACs.CompanyId = '" & _
				session("UserTpLinkCompanyId") & "') "
		else
			sSql = sSql & ") "
		end if
		
		if p_sSearchText <> "" then
			sSql = sSql & "AND ACs.Name LIKE '%" & p_sSearchText & "%'"
		end if
		
		if p_iBranchId <> "" then
			sSql = sSql & "AND Us.BranchId = '" & p_iBranchId & "' "
		end if		
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true

		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Us.CompanyL2Id = '" & iCompanyL2Id & "' " 
								
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierClause = false
				end if
				
				sSql = sSql & "Us.CompanyL3Id = '" & iCompanyL3Id & "' " 
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierClause = false
				end if
				
				sSql = sSql & "Us.BranchId = '" & iBranchId & "' "
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if

		
		if p_sLastName <> "" then
			sSql = sSql & "AND Us.LastName LIKE '%" & p_sLastName & "%'"
		end if
			
		if p_iStateId <> "" then
			sSql = sSql & "AND Cs.StateId = '" & p_iStateId & "' "
		end if
			
		'Response.Write("<p>" & sSql & "<p>")
		set SearchExpirationsByTime = QueryDb(sSql)
		
	end function


	' -------------------------------------------------------------------------
	' Name: SearchExpirationsByTimeStateId
	' Desc: Retrieves the number of loan officers that have courses that will 
	'	expire within the given date range.
	' Input: p_dStartDate, p_dEndDate
	' Preconditions: ConnectionString, TpConnectionString
	' Returns: Integer count of the number of loan officers
	' -------------------------------------------------------------------------
	public function SearchExpirationsByTimeStateId(p_dStartDate, p_dEndDate, _
		p_iBranchId, p_sSearchText, p_sLastName)
	
		'SearchExpirationsByTime = 0
		SearchExpirationsByTimeStateId = Server.CreateObject("ADODB.Recordset")
		
		'Verify preconditions
		if not CheckConnectionString or _
			not CheckDateParam(p_dStartDate, 0, "Start Date") or _
			not CheckDateParam(p_dEndDate, 0, "End Date") then
			exit function
		end if
		
		dim sSql 'SQL statement
		dim oRs 'Recordset object
		dim bFirstClause
		bFirstClause = false
		
		'We need to override the user's search if there are state deadlines
		'set for this state.
		
		
		sSql = "SELECT DISTINCT Cs.StateId, " & _
			"COUNT(ACs.UserId) AS OfficerCount " & _
			"FROM vAssociatesCoursesCKUserId AS ACs " & _
			"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
			"LEFT JOIN vCourses AS Cs ON (Cs.CourseId = ACs.CourseId) " & _
			"WHERE ACs.CertificateExpirationDate > '" & p_dStartDate & "' " & _
			"AND ACs.CertificateExpirationDate < '" & p_dEndDate & "' " & _
			"AND NOT ACs.Deleted = '1' " & _
			"AND NOT ACs.UserID IS NULL " & _
			"AND (ACs.CompanyId = '" & g_iCompanyId & "' "
			
		if session("UserTpLinkCompanyId") <> "" then 
			sSql = sSql & "OR ACs.CompanyId = '" & _
				session("UserTpLinkCompanyId") & "') "
		else
			sSql = sSql & ") "
		end if
		
		if p_sSearchText <> "" then
			sSql = sSql & "AND ACs.Name LIKE '%" & p_sSearchText & "%'"
		end if
		
		if p_iBranchId <> "" then
			sSql = sSql & "AND Us.BranchId = '" & p_iBranchId & "' "
		end if		
		
		
		'We need to combine the different tier lists together, because we need
		'to search from several together for users that may have admin access
		'over several L2s and L3s and Branches at once.  
		dim bFirstTierList
		bFirstTierList = true

		'If CompanyL2 ID List search
		dim aCompanyL2IdList
		dim iCompanyL2Id
		dim bFirstCompanyL2
		if g_sSearchCompanyL2IdList <> "" then
			aCompanyL2IdList = split(g_sSearchCompanyL2IdList, ",")
			bFirstCompanyL2 = true
			for each iCompanyL2Id in aCompanyL2IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL2 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierList = false
				end if
				
				sSql = sSql & "Us.CompanyL2Id = '" & iCompanyL2Id & "' " 
								
				bFirstCompanyL2 = false
				
			next 
			sSql = sSql & ")"
		end if

		'If CompanyL3 ID List search
		dim aCompanyL3IdList
		dim iCompanyL3Id
		dim bFirstCompanyL3
		if g_sSearchCompanyL3IdList <> "" then
			aCompanyL3IdList = split(g_sSearchCompanyL3IdList, ",")
			bFirstCompanyL3 = true
			for each iCompanyL3Id in aCompanyL3IdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstCompanyL3 then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierClause = false
				end if
				
				sSql = sSql & "Us.CompanyL3Id = '" & iCompanyL3Id & "' " 
				
				bFirstCompanyL3 = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'If Branch ID List search
		dim aBranchIdList
		dim iBranchId
		dim bFirstBranch
		if g_sSearchBranchIdList <> "" then
			aBranchIdList = split(g_sSearchBranchIdList, ",")
			bFirstBranch = true
			for each iBranchId in aBranchIdList
			
				if bFirstClause then
					sSql = sSql & " WHERE (("
					bFirstClause = false
					bFirstTierList = false
				elseif not bFirstBranch then
					sSql = sSql & " OR "
				elseif not bFirstTierList then
					sSql = sSql & " OR ("
				else
					sSql = sSql & " AND (("
					bFirstTierClause = false
				end if
				
				sSql = sSql & "Us.BranchId = '" & iBranchId & "' "
				
				bFirstBranch = false
				
			next 
			sSql = sSql & ")"
		end if
		
		'Close out the tier clause
		if not bFirstTierList then
			sSql = sSql & ")"
		end if

		
		if p_sLastName <> "" then
			sSql = sSql & "AND Us.LastName LIKE '%" & p_sLastName & "%'"
		end if
			
		'if p_iStateId <> "" then
		'	sSql = sSql & "AND Cs.StateId = '" & p_iStateId & "' "
		'end if
		sSql = sSql & "GROUP BY Cs.StateId "
			
		'Response.Write("<p>" & sSql & "<p>")
		set SearchExpirationsByTimeStateId = QueryDb(sSql)
		
	end function
	
			
	
	' -------------------------------------------------------------------------
	' Name:				LookupState
	' Description:		Gets the name of the state from the passed State ID
	' Pre-conditions:	ConnectionString
	' Inputs:			p_iStateId - State ID to look up
	' Outputs:			None.
	' Returns:			String w/ State Name, or empty
	' -------------------------------------------------------------------------
	public function LookupState(p_iStateId)
	
		LookupState = ""
	
		'Verify preconditions
		if not CheckConnectionString then
			exit function		
		elseif not CheckIntParam(p_iStateId, 0, "State ID") then
			exit function
		end if
		
		
		dim sSql 'SQL Query
		dim oRs 'ADODB Recordset
		
		sSql = "SELECT * FROM States " & _
		       "WHERE StateId = '" & p_iStateId & "'"
		set oRs = QueryDb(sSql)
		
		if not oRs.EOF then
			LookupState = oRs("State")
		end if 
		
		set oRs = nothing
		
	end function
	
	
	' -------------------------------------------------------------------------
	' Name: LookupCourseName
	' Description: Gets the name of a course from the passed course ID
	' Preconditions: ConnectionString
	' Inputs: p_iCourseId - ID of the course to look up
	' Returns: String with Course name, or empty string.
	' -------------------------------------------------------------------------
	public function LookupCourseName(p_iCourseId)
	
		LookupCourseName = ""
		
		'Verify preconditions
		if not CheckConnectionString then
			exit function
		elseif not CheckIntParam(p_iCourseId, 0, "Course ID") then
			exit function
		elseif p_iCourseId = 0 then 
			exit function
		end if
		
		dim sSql 'SQL query
		dim oRs 'ADODB recordset

				
		if p_iCourseId > 0 then
		
			sSql = "SELECT Name FROM Courses AS Cs " & _
				"WHERE Cs.CourseId = '" & p_iCourseId & "'"
			set oRs = QueryTpDb(sSql)
		
		elseif p_iCourseID < 0 then
			
			sSql = "SELECT Name, Number FROM Courses AS Cs " & _
				"WHERE Cs.CourseId = '" & p_iCourseId & "'"
			set oRs = QueryDb(sSql)
		
		end if 
		
		if not (oRs.BOF and oRs.EOF) then
		
			if p_iCourseId < 0 then
				
				if oRs("Number") <> "" then
			
					LookupCourseName = oRs("Number") & " - " & oRs("Name")
					
				end if
		
			else
			
				LookupCourseName = oRs("Name")
			
			end if
			
		end if 
		
		set oRs = nothing
		
	end function
	
end class
%>
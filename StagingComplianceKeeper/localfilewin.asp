<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/AssociateDoc.Class.asp" --------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Branch.Class.asp" --------------------------->
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/CompanyL2.Class.asp" ------------------------>
<!-- #include virtual="/includes/CompanyL3.Class.asp" ------------------------>


<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
'Verify that the user has logged in.
CheckIsLoggedIn()
	

'Verify that we have a valid company ID by loading the company object
dim oCompany 
set oCompany = new Company
oCompany.VocalErrors = application("bVocalErrors")
oCompany.ConnectionString = application("sDataSourceName")
oCompany.TpConnectionString = application("sTpDataSourceName")

if oCompany.LoadCompanyById(session("UserCompanyId")) = 0 then

	AlertError("This page requires a valid Company ID.")
	Response.End

end if 


'Get the passed type value to see which type of files we're looking for.
dim sPageType
dim sLetter
sPageType = request("type")
sLetter = request("let")
if sLetter = "" then
	sLetter = "A"
end if 

dim oCompanyL2
set oCompanyL2 = new CompanyL2
oCompanyL2.ConnectionString = application("sDataSourceName")
oCompanyL2.VocalErrors = application("bVocalErrors")
oCompanyL2.CompanyId = session("UserCompanyId")

dim oCompanyL3
set oCompanyL3 = new CompanyL3
oCompanyL3.ConnectionString = application("sDataSourceName")
oCompanyL3.VocalErrors = application("bVocalErrors")
oCompanyL3.CompanyId = session("UserCompanyId")

dim oBranch
set oBranch = new Branch
oBranch.ConnectionString = application("sDataSourceName")
oBranch.VocalErrors = application("bVocalErrors")
oBranch.CompanyId = session("UserCompanyId")

dim oAssociate
set oAssociate = new Associate
oAssociate.ConnectionString = application("sDataSourceName")
oAssociate.VocalErrors = application("bVocalErrors")
oAssociate.CompanyId = session("UserCompanyId")
oAssociate.SearchLastNameStart = sLetter

'Restrict the list if the user does not have company-wide access.
if CheckIsCompanyL2Admin() or CheckIsCompanyL2Viewer() then
	oAssociate.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin() or CheckIsCompanyL3Viewer() then
	oAssociate.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin() or CheckIsBranchViewer() then
	oAssociate.SearchBranchIdList = GetUserBranchIdList()
end if



dim oAssociateDoc
set oAssociateDoc = new AssociateDoc
oAssociateDoc.ConnectionString = application("sDataSourceName")
oAssociateDoc.VocalErrors = application("bVocalErrors")
oAssociateDoc.CompanyId = session("UserCompanyId")
oAssociateDoc.OwnerId = oCompany.CompanyId
oAssociateDoc.OwnerTypeId = 3

dim bFoundFiles
dim iStateId
dim oRs
dim oDocRs
set oDocRs = oAssociateDoc.SearchDocs

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
		
	function ReturnAnswer(p_iFileId, p_sFileName) {
		window.opener.AddLibraryFile(p_iFileId, p_sFileName);
		window.close();
	}
		
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	'PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>


			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="100%">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" alt="Select a File" align="absmiddle" vspace="10"> Select a File</td>
				</tr>
				<tr>
					<td class="bckWhiteBottomBorder">
						<table border="0" cellpadding="5" cellspacing="0" width="100%">
							<form name="90DaySearch" method="POST" action="dllist.asp">
							<tr>
								<td class="newstitle"><a href="localfilewin.asp">Company</a></td>
								<td class="newstitle"><a href="localfilewin.asp?type=l2"><% = session("CompanyL2Name") %></a></td>
								<td class="newstitle"><a href="localfilewin.asp?type=l3"><% = session("CompanyL3Name") %></a></td>
								<td class="newstitle"><a href="localfilewin.asp?type=br">Branches</a></td>
								<td class="newstitle"><a href="localfilewin.asp?type=of">Officers</a></td>
								<td class="newstitle"><a href="localfilewin.asp?type=st">States</a></td>
							</tr>
							</form>
						</table>
					</td>
				</tr>
				<tr>
					<td class="bckRight">

<%
if sPageType = "" then 
%>

	<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyName") %> Files</td>
		</tr>
	<%
	if not oDocRs.EOF then 
	%>
		
		<tr>
			<td colspan="2"><b><% = oCompany.Name %></b></td>
		</tr>
		<%
		do while not oDocRs.EOF 
		
			if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then
		
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
			<% 
				bFoundFiles = true
			end if
			
			oDocRs.MoveNext %>
		<% 
		loop
		%>
	
	<%
	end if
	if not bFoundFiles then
		%>
	<tr>
		<td>&nbsp;&nbsp;&nbsp;</td>
		<td>No files were found in this category.</td>
	</tr>			
		<%
	end if 
	%>
	</table>
	
	
<%
elseif sPageType = "l2" then


	'Find the files for any second-tier companies.
	set oRs = oCompanyL2.SearchCompanies()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyL2Name") %> Files</td>
		</tr>
		<%
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisCompanyL2Admin(oRs("CompanyL2Id")) or CheckIsThisCompanyL2Viewer(oRs("CompanyL2Id")) then
	
			iResult = oCompanyL2.LoadCompanyById(oRs("CompanyL2Id"))
			
				oAssociateDoc.DocName = ""
				oAssociateDoc.OwnerId = oCompanyL2.CompanyL2Id
				oAssociateDoc.OwnerTypeId = 4
				set oDocRs = oAssociateDoc.SearchDocs

				if not oDocRs.EOF then
			
				%>	
		<tr>
			<td colspan="2"><b><% = oCompanyL2.Name %></b></td>
		</tr>
				<%

					do while not oDocRs.EOF
						
						if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then	
						%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
						<% 
							bFoundFiles = true
						end if 
						
						oDocRs.MoveNext
					loop			
				end if
			end if
			oRs.MoveNext
		loop
		if not bFoundFiles then
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>No files were found in this category.</td>
		</tr>			
			<%
		end if 
		%></table><%
	end if


elseif sPageType = "l3" then


	'Find the files for any third-tier companies.
	set oRs = oCompanyL3.SearchCompanies()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle"><% = session("CompanyL3Name") %> Files</td>
		</tr>
		<%
		
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisCompanyL2Admin(oRs("CompanyL3Id")) or CheckIsThisCompanyL3Viewer(oRs("CompanyL3Id")) then
	
			iResult = oCompanyL3.LoadCompanyById(oRs("CompanyL3Id"))
			
				oAssociateDoc.DocName = ""
				oAssociateDoc.OwnerId = oCompanyL3.CompanyL3Id
				oAssociateDoc.OwnerTypeId = 5
				set oDocRs = oAssociateDoc.SearchDocs

				if not oDocRs.EOF then
				
				%>	
		<tr>
			<td colspan="2"><b><% = oCompanyL3.Name %></b></td>
		</tr>
				<%

					do while not oDocRs.EOF
						
						if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then	
						%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
						<% 
							bFoundFiles = true
						end if 
						
						oDocRs.MoveNext
					loop			
				end if
			end if
			oRs.MoveNext
		loop
		if not bFoundFiles then
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>No files were found in this category.</td>
		</tr>			
			<%
		end if 
		%></table><%
	end if
	
	
elseif sPageType = "br" then
	
	
	'Find the files for any branches.
	set oRs = oBranch.SearchBranches()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle">Branch Files</td>
		</tr>
		<%	
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisBranchAdmin(oRs("BranchId")) or CheckIsThisBranchViewer(oRs("BranchId")) then
	
			iResult = oBranch.LoadBranchById(oRs("BranchId"))
			
				oAssociateDoc.DocName = ""
				oAssociateDoc.OwnerId = oBranch.BranchId
				oAssociateDoc.OwnerTypeId = 2
				set oDocRs = oAssociateDoc.SearchDocs

				if not oDocRs.EOF then
				
				%>	
		<tr>
			<td colspan="2"><b><% = oBranch.Name %></b></td>
		</tr>
				<%

					do while not oDocRs.EOF
						
						if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then	
						%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
						<% 
							bFoundFiles = true
						end if 
						
						oDocRs.MoveNext
					loop			
				end if
			end if
			oRs.MoveNext
		loop
		if not bFoundFiles then
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>No files were found in this category.</td>
		</tr>			
			<%
		end if 
		%></table><%
	end if




elseif sPageType = "of" then
	
	
	'Find the files for any branches.
	set oRs = oAssociate.SearchAssociates()
	
	if not oRs.EOF then
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">
			<tr>
				<td>
				<a href="localfilewin.asp?type=of&let=a"<% if sLetter = "a" then %> class="newsTitle"<% end if %>>A</a>
				<a href="localfilewin.asp?type=of&let=b"<% if sLetter = "b" then %> class="newsTitle"<% end if %>>B</a>
				<a href="localfilewin.asp?type=of&let=c"<% if sLetter = "c" then %> class="newsTitle"<% end if %>>C</a>
				<a href="localfilewin.asp?type=of&let=d"<% if sLetter = "d" then %> class="newsTitle"<% end if %>>D</a>
				<a href="localfilewin.asp?type=of&let=e"<% if sLetter = "e" then %> class="newsTitle"<% end if %>>E</a>
				<a href="localfilewin.asp?type=of&let=f"<% if sLetter = "f" then %> class="newsTitle"<% end if %>>F</a>
				<a href="localfilewin.asp?type=of&let=g"<% if sLetter = "g" then %> class="newsTitle"<% end if %>>G</a>
				<a href="localfilewin.asp?type=of&let=h"<% if sLetter = "h" then %> class="newsTitle"<% end if %>>H</a>
				<a href="localfilewin.asp?type=of&let=i"<% if sLetter = "i" then %> class="newsTitle"<% end if %>>I</a>
				<a href="localfilewin.asp?type=of&let=j"<% if sLetter = "j" then %> class="newsTitle"<% end if %>>J</a>
				<a href="localfilewin.asp?type=of&let=k"<% if sLetter = "k" then %> class="newsTitle"<% end if %>>K</a>
				<a href="localfilewin.asp?type=of&let=l"<% if sLetter = "l" then %> class="newsTitle"<% end if %>>L</a>
				<a href="localfilewin.asp?type=of&let=m"<% if sLetter = "m" then %> class="newsTitle"<% end if %>>M</a>
				<a href="localfilewin.asp?type=of&let=n"<% if sLetter = "n" then %> class="newsTitle"<% end if %>>N</a>
				<a href="localfilewin.asp?type=of&let=o"<% if sLetter = "o" then %> class="newsTitle"<% end if %>>O</a>
				<a href="localfilewin.asp?type=of&let=p"<% if sLetter = "p" then %> class="newsTitle"<% end if %>>P</a>
				<a href="localfilewin.asp?type=of&let=q"<% if sLetter = "q" then %> class="newsTitle"<% end if %>>Q</a>
				<a href="localfilewin.asp?type=of&let=r"<% if sLetter = "r" then %> class="newsTitle"<% end if %>>R</a>
				<a href="localfilewin.asp?type=of&let=s"<% if sLetter = "s" then %> class="newsTitle"<% end if %>>S</a>
				<a href="localfilewin.asp?type=of&let=t"<% if sLetter = "t" then %> class="newsTitle"<% end if %>>T</a>
				<a href="localfilewin.asp?type=of&let=u"<% if sLetter = "u" then %> class="newsTitle"<% end if %>>U</a>
				<a href="localfilewin.asp?type=of&let=v"<% if sLetter = "v" then %> class="newsTitle"<% end if %>>V</a>
				<a href="localfilewin.asp?type=of&let=w"<% if sLetter = "w" then %> class="newsTitle"<% end if %>>W</a>
				<a href="localfilewin.asp?type=of&let=x"<% if sLetter = "x" then %> class="newsTitle"<% end if %>>X</a>
				<a href="localfilewin.asp?type=of&let=y"<% if sLetter = "y" then %> class="newsTitle"<% end if %>>Y</a>
				<a href="localfilewin.asp?type=of&let=z"<% if sLetter = "z" then %> class="newsTitle"<% end if %>>Z</a>
				</td>
			</tr>
		</table>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle">Officer Files</td>
		</tr>
		<%	
	
		do while not oRs.EOF
		
			if CheckIsAdmin or CheckIsThisAssociateAdmin(oRs("UserId")) or CheckIsThisAssociateViewer(oRs("UserId")) then
	
			if oAssociate.LoadAssociateById(oRs("UserId")) <> 0 then
			
				oAssociateDoc.DocName = ""
				oAssociateDoc.OwnerId = oAssociate.UserId
				oAssociateDoc.OwnerTypeId = 1
				set oDocRs = oAssociateDoc.SearchDocs

				if not oDocRs.EOF then
				
				%>	
		<tr>
			<td colspan="2"><b><% = oAssociate.FullName %></b></td>
		</tr>
				<%

					do while not oDocRs.EOF
						
						if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then	
						%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
						<% 
							bFoundFiles = true
						end if 
						
						oDocRs.MoveNext
					loop			
				end if
			end if
			end if 
			oRs.MoveNext
		loop
		if not bFoundFiles then
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>No files were found in this category.</td>
		</tr>			
			<%
		end if 
		%></table><%
	end if
	
	
	
elseif sPageType = "st" then
	
	
		%>
		<table cellpadding="5" cellspacing="0" border="0">	
		<tr>
			<td colspan="2" class="activeTitle">State Files</td>
		</tr>
		<%	
	
		for iStateId = 1 to 51
		
				oAssociateDoc.DocName = ""
				oAssociateDoc.OwnerId = iStateId
				oAssociateDoc.OwnerTypeId = 6
				set oDocRs = oAssociateDoc.SearchDocs

				if not oDocRs.EOF then
				
				%>	
		<tr>
			<td colspan="2"><b><% = oAssociateDoc.LookupState(iStateId, 0) %></b></td>
		</tr>
				<%

					do while not oDocRs.EOF
						
						if oAssociateDoc.LoadDocById(oDocRs("DocId")) <> 0 then	
						%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td><a href="#" onClick="javascript:ReturnAnswer(<% = oAssociateDoc.DocId %>,'<% = oAssociateDoc.DocName %>');"><img src="/Media/Images/bttnNew.gif" align="top" border="0"> <% = oAssociateDoc.DocName %></a> (<a href="/officerdocs/<% = oAssociateDoc.DocId %>/<% = oAssociateDoc.FileName %>">view</a>)</td>
		</tr>
						<% 
							bFoundFiles = true
						end if 
						
						oDocRs.MoveNext
					loop			
				end if
		next
		if not bFoundFiles then
			%>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
			<td>No files were found in this category.</td>
		</tr>			
			<%
		end if 
		%></table><%
	
	

end if
%>
				<p><br>
			</td>
		</tr>
	</table>
</body>
</html>
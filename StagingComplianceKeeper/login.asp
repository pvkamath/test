<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- include virtual="/includes/StateCandidate.Class.asp" ------------------->
<!-- include virtual="/includes/StatePreferences.Class.asp" ------------------>

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	
	'iPage = 1
	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
 

<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

		<td>
			<table width=760 bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
					<td width="100%" valign="top">
						<table bgcolor="FFFFFF" border=0 cellpadding=10 cellspacing=0>
							<tr bgcolor="#8A99B6">
								<td><img src="/Media/Images/photo-tasks.jpg" width="40" height="40" alt="" border="0"></td>
								<td width="100%" class="sectiontitle">Administrative Login</td>
							</tr>
							<tr>
								<td colspan="2" valign="top"><span class="date">
									<table bgcolor="FFFFFF" border=0 cellpadding=0 cellspacing=0>
										<tr>
									<td class="newstitle" nowrap>Login</td>
									<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
									<form name="Login" method="POST" action="loginproc.asp">
									<td><input type="text" name="User_Login" size="20" value=""></td>
									<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
									<td width="100%" valign="middle"><input type="image" src="/media/images/bttnGo.gif" width="22" height="17" alt="Go" border="0" vspace="0" hspace="0" border="0" id=image1 name=image1></td>
										</tr>
										<tr>
									<td colspan="5">&nbsp;</td>
										</tr>
										<tr>
									<td class="newstitle" nowrap>Password</td>
									<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
									<td><input type="password" name="User_Password" size="20" value=""></td>
									<td><img src="/Media/Images/spacer.gif" width="10" height="17" alt="" border="0"></td>
									<td></td>
									</form>
										</tr>
									</table>
									
								</td>
							</tr>
						</table>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>					
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"><br>
						<!-- space-->
						<table width="100%" bgcolor="dadada" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="16" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="6" height="10" alt="" border="0"></td>
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/Media/Images/spacer.gif" width="10" height="15" alt="" border="0"></td>
				</tr>
			</table>
		</td>
	</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/User.Class.asp" ------------------------------------------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Settings.Class.asp" ----------------------->


<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------



	'Check the user's login.
	if not request("User_Password") = "" then
	
		dim oRs 'as object
		dim sUserName 'as string
		dim sPassword 'as string
		dim sFullName 'as string
		dim oRs2 'as object
		dim oUserObj 'as object
		dim objConn 'as object
		dim bIsAggreementSigned 'as boolean
		
		oRs = Server.CreateObject("ADODB.Recordset")
		set objConn = server.CreateObject("ADODB.Connection")
		objConn.ConnectionString = application("sDataSourceName")
		objConn.Open		
						
		sUserName = replace(request("User_Login"),"'","''")
		sPassword = replace(request("User_Password"),"'","''")
		sReferrer = trim(request("Referrer"))
		
		sSql = "SELECT * FROM Users AS Us " & _
			"LEFT JOIN afxSecurityGroups AS SGs " & _
			"ON SGs.GroupId = Us.GroupId " & _
			"LEFT JOIN afxSecurityGroupsRolesX AS SGRX " & _
			"ON SGRX.GroupId = SGs.GroupId " & _
			"LEFT JOIN afxSecurityRoles AS SRs " & _
			"ON SRs.RoleId = SGRX.RoleId " & _
			"WHERE Us.UserName = '" & sUserName & "' " & _
			"AND Us.Password = '" & sPassword & "' "
		'sSQL = "SELECT * FROM Users WHERE UserName = '" & sUserName & "' AND Password = '" & sPassword & "' "

	   
       set oRs = objConn.execute(sSql)
       		

		if not oRs.EOF then
		
			if oRs("UserStatus") = 1 then
		
				session("afxSecurityAccessLevel") = oRs("AccessLevel")
				session("afxSecurityGroupId") = oRs("GroupId")
				session("User_ID") = oRs("UserID")

				'Get fullname and store it in session var
				set oUserObj = New User
				oUserObj.ConnectionString = application("sDataSourceName")
				oUserObj.UserID = oRs("UserID")

				dim oBranchRs 
				set oBranchRs = oUserObj.GetBranchIdList()
			
				set oRs2 = oUserObj.GetUser
				if trim(oRs2("MiddleName")) <> "" then
					sFullName = oRs2("FirstName") & " " & oRs2("MiddleName") & ". " & oRs2("LastName")
				else
					sFullName = oRs2("FirstName") & " " & oRs2("LastName")
				end if			
				session("User_FullName") = sFullName
				session("UserEmail") = oRs("Email")
				session("UserCompanyId") = oRs2("CompanyId")
				session("UserBranchId") = oRs2("BranchId")
				session("UserBranchIdList") = oUserObj.GetBranchIdListString
				session("UserCompanyL2IdList") = oUserObj.GetCompanyL2IdListString
				session("UserCompanyL3IdList") = oUserObj.GetCompanyL3IdListString
				session("UserProviderId") = oRs2("ProviderId")
				session("dUserLastLoginDate") = oRs2("LastLoginDate")
				
				'We don't want the LastLoginDate to be null; if it is, we'll
				'set it to the current date.
				if isnull(session("dUserLastLoginDate")) or session("dUserLastLoginDate") = "" then
					session("dUserLastLoginDate") = now()
				end if 
			
				set oRs2 = nothing
			
			
				dim oCompany
				set oCompany = new Company
				oCompany.ConnectionString = application("sDataSourceName")
				oCompany.TpConnectionString = application("sTpDataSourceName")
				oCompany.VocalErrors = application("bVocalErrors")
			
				oCompany.LoadCompanyById(session("UserCompanyId"))
				
				if oCompany.Active = false then
					session.Abandon
					AlertError("This company is no longer available.")
					Response.End
				end if
				
				
				dim oSettings
				set oSettings = new Settings
				oSettings.ConnectionString = application("sDataSourceName")
				oSettings.VocalErrors = application("bVocalErrors")
				oSettings.LoadSettingsByCompanyId(oCompany.CompanyId)
				
		
				session("CorpLogoFileName") = oCompany.LogoFileName		'.LookupCorporateLogo
				session("UserTpLinkCompanyId") = oCompany.TpLinkCompanyId
				if oSettings.CompanyName <> "" then
					session("CompanyName") = oSettings.CompanyName
				else
					session("CompanyName") = "Company"
				end if
				if oSettings.CompanyL2Name <> "" then
					session("CompanyL2Name") = oSettings.CompanyL2Name
				else
					session("CompanyL2Name") = "Division"
				end if
				if oSettings.CompanyL3Name <> "" then
					session("CompanyL3Name") = oSettings.CompanyL3Name
				else
					session("CompanyL3Name") = "Region"
				end if
				
				session("AllowOfficerNoteEditing") = oSettings.AllowOfficerNoteEditing
				
				if oSettings.CustField1Name <> "" then
					session("CustField1Name") = oSettings.CustField1Name
				else
					Session.Contents.Remove("CustField1Name")
				end if
				if oSettings.CustField2Name <> "" then
					session("CustField2Name") = oSettings.CustField2Name
				else
					Session.Contents.Remove("CustField2Name")
				end if
				if oSettings.CustField3Name <> "" then
					session("CustField3Name") = oSettings.CustField3Name
				else
					Session.Contents.Remove("CustField3Name")
				end if
				if oSettings.CustField4Name <> "" then
					session("CustField4Name") = oSettings.CustField4Name
				else
					Session.Contents.Remove("CustField4Name")
				end if
				if oSettings.CustField5Name <> "" then
					session("CustField5Name") = oSettings.CustField5Name
				else
					Session.Contents.Remove("CustField5Name")
				end if
				
				for i = 1 to 15
					if oSettings.GetOfficerCustFieldName(i) <> "" then
						session("OfficerCustField" & i & "Name") = oSettings.GetOfficerCustFieldName(i)
					else
						Session.Contents.Remove("OfficerCustField" & i & "Name") 
					end if
				next
				
				oUserObj.RecordLoginDate
				bIsAggreementSigned = oUserObj.IsAgreementSigned
				
				set oRs = nothing
				set objConn = nothing
				set oUserObj = nothing
				set oCompany = nothing
				set oSettings = nothing
								
				'If this user hasn't signed the EULA, redirect them to that page now.
				if not bIsAggreementSigned  then 
					session("NeedsAgreement") = true
					Response.Redirect "agreement.asp?id=" & request("id") & "&lid=" & request("lid") & "&EventID=" & request("EventID") & "&referrer=" & request("referrer")
				end if 
			
				'Redirect back to the default page.
				Response.Redirect "companyhomeupdate.asp?id=" & request("id") & "&lid=" & request("lid") & "&EventID=" & request("EventID") & "&referrer=" & request("referrer")
				
			end if
					
		end if 
	
		set oRs = nothing
		set objConn = nothing
	end if
	
	'an error occurred
	response.redirect("/error.asp?message=<b>Invalid Login</b><br><br>The login/password provided is invalid.")
%>
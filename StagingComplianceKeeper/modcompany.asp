<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/includes/Company.Class.asp" ------>
<!-- #include virtual = "/includes/User.Class.asp" --------->

<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Company Profile"
	
	sKeywords = getFormElement("keywords")
	
	iPage = 3
	
	dim oCompany
	dim oUser
	dim oRs

	set oCompany = new Company
	set oUser = new User
	
	oUser.ConnectionString = application("sDataSourceName")
	oCompany.ConnectionString = application("sDataSourceName")
	
	oUser.UserID = session("User_ID")
	set oRs = oUser.GetUser

	'Get the company ID
	if not (oRs.EOF and oRs.BOF) then
	
		iCompanyId = oRs("CompanyID")
	
	else
	
		Response.Redirect("error.asp?message=Error retrieving your associated company.  Please login again.")
		Response.End
	
	end if
	
	set oUser = nothing
	set oRs = nothing


	'Get the company information.
	if iCompanyId <> "" then

		if oCompany.LoadId(iCompanyId) = 0 then

			'The load was unsuccessful.  Die.
			Response.Redirect("error.asp?message=Error retrieving your company information.  Please try again.")
			Response.end

		end if

	end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="JavaScript">
	function Validate(FORM)
	{
		if (!checkString(FORM.Name, "Name")
			|| !checkString(FORM.Address, "Address")
			|| !checkString(FORM.City, "City")
			|| !checkString(FORM.State, "State")
			|| !checkZIPCode(FORM.Zipcode, 0)
		   )
			return false;

		return true;
	}
</script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"></span></td>
										</tr>
										<tr>
											<%setGlobalContentData(sKeywords)%>
											<td><span class="pagetitle">Update Company Profile</span>
											<p>
<form name="CompanyForm" action="modcompanyproc.asp" method="POST" onSubmit="return Validate(this)">
<table border="0" cellpadding="5" cellspacing="5">
	<tr>
		<td align="left" valign="top">
			<b>Name: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Name" value="<% = oCompany.Name %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>TIN: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="TIN" value="<% = oCompany.Tin %>" size="12" maxlength="11">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Address: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Address" value="<% = oCompany.Address %>" size="50" maxlength="100">
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>City: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="City" value="<% = oCompany.City %>" size="50" maxlength="50">
		</td>
	</tr>
	<tr>		
		<td><b>State:</b></td>
		<td>
			<% call DisplayStatesDropDown(oCompany.StateId,1,1) 'located in functions.asp %>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<b>Zipcode: </b>
		</td>
		<td align="left" valign="top">
			<input type="text" name="Zipcode" value="<% = oCompany.Zipcode %>" size="12" maxlength="10">
		</td>
	</tr>	
	<tr>
		<td></td>
		<td align="left" valign="top">
			<p><br>
			<input type="image" src="<% = application("sDynMediaPath") %>bttnContinue.gif">&nbsp;
		</td>
	</tr>

</table>
</form>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

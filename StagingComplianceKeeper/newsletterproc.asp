<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual = "/includes/User.Class.asp" --------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro"
	
	sKeywords = "Newsletter" 'getFormElement("keywords")

	iPage = 1
	
	if session("User_ID") = "" then
		Response.Redirect("default.asp")
	else
		iPage = 0
	end if

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle">User Registration</span>
<%

dim oRs
dim oUser
set oUser = new User

dim iUserID
dim bNewsletter

dim sMessage
dim bSuccess

bSuccess = true

oUser.ConnectionString = application("sDataSourceName")

if (Ucase(trim(request("subscribe"))) = "ON") then
	bNewsletter = true
else
	bNewsletter = false
end if

iUserID = session("User_ID")

bSuccess = oUser.ChangeSubscription(iUserID, bNewsletter)

if not bSuccess then
	sMessage = "There was a problem updating your account.  Please try again at another time."
else
	sMessage = "Your account has been successfully updated.  Thank you."
end if 
	

%>

<p>
<b><% = sMessage %></b>
<p>

<% if bError then %>
<a href="javascript:history.back()"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" alt="Go Back" border="0"></a>
<% else %>
<a href="userPage.asp"><img src="<% = application("sDynMediaPath") %>bttnContinue.gif" alt="Proceed to Your User Page" border="0"></a>
<% end if %>

<!-- Page Extension -->
<br>
<img src="<% = application("sDynMediaPath") %>clear.gif" width="1" height="150" alt="" border="0">

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
				
					</td>					


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->

<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro Event Calendar"
	
	if session("User_ID") = "" then
		iPage = 1
	else
		iPage = 0
	end if

	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ 
dim sCalendarType
dim iPassedMonth
iPassedMonth = ScrubForSQL(getFormElement("SearchMonth"))	
if iPassedMonth = "" or not (isNumeric(iPassedMonth)) then
	iPassedMonth = 0
end if

sEventType = ScrubForSQL(getFormElement("SearchEventtype"))
iSearchStateID = ScrubForSQL(getFormElement("SearchState"))

if sEventType <> "" then
	sCalendarType = sEventType
else
	sCalendarType = "Event"
end if
%>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"></span></td>
										</tr>
										<tr>
											<td>											
											<span class="pagetitle"><%=sCalendarType %> Calendar</span><p>

											<form name="monthform" method="post" action="calendar.asp">
											<input type="hidden" name="SearchEventtype" value="<%=sEventType%>">
											<table cellpadding="4">
												<tr>
													<td>
														<select name="SearchMonth" id="SearchMonth" onChange="this.form.submit();">
															<option value="0">All Months</option>
<%
				
				dim bNextYear
				dim iMonth
				dim iYear
				dim iCount
				
				
				bNextYear = false
				for iCount = 0 to 11
				
					iMonth = Month(Date) + iCount
					
					'Move to next year if we're past month 12
					if cint(iMonth) > 12 then
					
						iMonth = iMonth - 12
						bNextYear = true
						
					end if
					
					'If this is the month we were passed, make it selected
					if cint(iMonth) = cint(iPassedMonth) then
						
						sSelected = " SELECTED"
						
					else
						
						sSelected = ""
						
					end if
					
					if bNextYear then
					
						iYear = Year(Date) + 1
						
					else
						
						iYear = Year(Date)
						
					end if
					
					Response.Write("<option value=""" & iMonth & """" & sSelected & ">" & MonthName(iMonth) & " " & iYear & "</option>" & vbCrLf)
				
				next
				%>			
															</select>
														</td>
													</tr>
													<tr>
														<td>
															<% call DisplayStatesDropDown(iSearchStateID,3,0) %>	
														</td>
													</tr>
												</table>
												</form>											


<p>

<%

dim oConn
dim sCalendarRow

set oConn = new HandyADO

sSql = "SELECT Es.*, Cs.Name AS CourseName, ET.EventType " & _
	   "FROM afxEvents AS Es " & _
	   "LEFT JOIN Courses AS Cs ON (Cs.CourseID = Es.CourseID) " & _
	   "LEFT OUTER JOIN afxEventsEventTypeX as EET on (Es.EventID = EET.EventID) " & _	   
	   "LEFT OUTER JOIN afxEventType as ET on (EET. EventTypeID = ET.EventTypeID) " & _
	   "LEFT OUTER JOIN States as S on (Es.StateID = S.StateID) "  & _
	   "WHERE Es.EndDate > '" & Now & "' "
	   
if iPassedMonth = 0 then
	sSql = sSql & "AND Active = '1' "
else
	sSql = sSql & "AND (DATEPART(mm, StartDate) = '" & iPassedMonth & "' OR DATEPART(mm, EndDate) = '" & iPassedMonth & "') AND Active = '1' "
end if

If sEventType <> "" Then											
	sSql = sSql & " AND ET.EventType = '" & sEventType & "'"
End If

'if true, filter by state
If trim(iSearchStateID) <> "" Then											
	sSQL = sSQL & " AND S.StateID = '" & iSearchStateID & "'"
End If

sSql = sSql & "ORDER BY Es.StartDate ASC"
	   
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

%>

<table width="100%" border="0" cellpadding="4" cellspacing="0">

<%
if oRS.EOF then
%>
	<tr>
		<td>There are currently no events scheduled.</td>
	</tr>
<%
else
do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""EAE3D3"""
	else
		sCalendarRow = ""
	end if

	Response.Write("<tr>" & vbCrLf)
	Response.Write("	<td bgcolor=""D8CBAA"" colspan=""4"">" & oRs("StartDate"))
	if oRs("EndDate") <> "" and oRs("StartDate") <> "" then
		Response.Write(" - " & oRs("EndDate"))
	elseif oRs("EndDate") <> "" then
		Response.Write(oRs("EndDate"))
	end if
	Response.Write("	</td>" & vbCrLf)
	
	Response.Write("	</tr><tr>" & vbCrLf)
	
	Response.Write("	<td bgcolor=""EAE3D3""><b>Event</b></td>")
	
	if UCase(trim(oRS("EventType"))) = "LIVE COURSE" then	
		Response.Write("	<td bgcolor=""EAE3D3""><b>Course</b></td>")
	else
		Response.Write("	<td bgcolor=""EAE3D3"">&nbsp;</td>")	
	end if
	Response.Write("	<td bgcolor=""EAE3D3""><b>City</b></td>")
	Response.Write("	<td bgcolor=""EAE3D3""><b>State</b></td>" & vbCrLf)
	
	Response.Write("	</tr><tr>" & vbCrLf)
	
	Response.Write("	<td><a href=""event.asp?id=" & oRs("EventID") & "&SearchMonth=" & iPassedMonth & "&SearchEventType=" & sEventType & """>" & oRs("Event") & "</a></td>")
	Response.Write("	<td>" & oRs("CourseName") & "</td>")
	Response.Write("	<td>" & oRs("City") & "</td>")
	Response.Write("	<td>" & GetStateName(oRs("StateID"), true) & "</td>" & vbCrLf)	

	Response.Write("	</tr><tr>" & vbCrLf)
	Response.Write("	<td colspan=""4"">&nbsp;</td>" & vbCrLf)
	
	Response.Write("</tr>")		

	oRs.MoveNext

loop
end if
%>

</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

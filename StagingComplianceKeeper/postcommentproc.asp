<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual = "/includes/Blog.Class.asp" -->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
		
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	iPage = 1


	'Verify that the user has logged in.
	CheckIsLoggedIn()
	
	
dim oBlog 'as object
dim iBlogID 'as integer
dim iPostID 'as integer
dim sText 'as string
dim iCommentID 'as integer

iMemberID = Session("User_Id")
iBlogID = 0 'GetFormElementAndScrub("BlogID")
iPostID = ScrubForSql(request("PostId"))
sText  = ScrubForSql(request("Text"))


set oBlog = New Blog
oBlog.Connectionstring = application("sDataSourceName")

oBlog.MemberID = iMemberID
oBlog.PostID = iPostID
oBlog.CommentText = sText
iCommentID = oBlog.SaveComment()
set oBlog = nothing

if clng(iCommentID) > 0 then
	response.redirect("viewpost.asp?id=" & iPostID)
else 
	AlertError("Your comment was not added successfully.  Please try again in a few moments.")
end if

%>
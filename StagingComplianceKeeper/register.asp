<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/User.Class.asp" --------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Registratrion"
	
	if session("User_ID") = "" then
		iPage = 1
	else
		iPage = 0
	end if
	
	sHost = UCase(Request.ServerVariables("HTTP_HOST"))
	
	sKeywords = getFormElement("keywords")

	'Make registrationform secure if on the live site
	if sHost = "WWW.TRAININGPRO.COM" OR sHOST = "TRAININGPRO.COM" then
		if UCase(Request.ServerVariables("HTTPS")) <> "ON" then
			response.redirect(application("SECUREURL") & "/register.asp?Keywords=" & sKeywords & "&referrer=" & request("referrer"))
		end if		
	end if
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%>
<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		
	function Validate(FORM)
	{   	

		if (!checkString(FORM.FirstName,"First Name") || !checkString(FORM.LastName,"Last Name"))
			return false;	
	
		if(!checkEmail(FORM.Email) || !checkString(FORM.Password,"Password") || !checkString(FORM.ConfirmPassword,"Confirm Password"))
			return false;
	
		if (FORM.Password.value != FORM.ConfirmPassword.value)
		{
			alert("The Password and Confirm Password fields do not match.")
			FORM.Password.focus();
			return false;
		}

	
		if (!checkSSN(FORM.SSN))
			return false;


		//check address
		if (!checkString(FORM.Address,"Address"))
			return false;

		
		//if zipcode is not blank, make sure it's in the proper format
		//if (FORM.Zipcode.value != "")
		//{
			if (!checkZIPCode(FORM.Zipcode))
				return false;
		//}


		//if phone # is not blank, make sure it's in the proper format
		if (!checkUSPhone(FORM.Phone))
			return false;
		

		//if fax # is not blank, make sure it's in the proper format
		if (FORM.Fax.value != "")
		{
			if (!checkUSPhone(FORM.Fax))
				return false;
		}	

		//if the state is OHIO, the Drivers License is required
		if (FORM.State[FORM.State.selectedIndex].value == '36')
		{
			if (!checkString(FORM.DriversLicenseNo,"Driver's License No."))
				return false;
		}		
		
		return true;
	}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%

dim oUser 
dim oRs

dim sMemberNumber
dim sEmail
dim sPassword
dim sFirstName
dim sMidInitial
dim sLastName
dim sSSN
dim sLicenseNo
dim sDriversLicenseNo
dim sAddress
dim sCity
dim iState
dim sZipcode
dim sPhoneNo
dim sFaxNo
dim sCertCompanyName
dim bNewsletter
dim sReferrer

set oUser = new User 
oUser.ConnectionString = application("sDataSourceName")

if session("User_ID") <> "" then

	oUser.UserID = session("User_ID")
	set oRs = oUser.GetUser
	
	sMemberNumber = oRs("MemberNumber")
	sEmail = oRs("Email")
	sPassword = oRs("Password")
	sFirstName = oRs("FirstName")
	sMidInitial = oRs("MidInitial")
	sLastName = oRs("LastName")
	sSSN = oRs("SSN")
	sLicenseNo = oRs("LicenseNo")
	sDriversLicenseNo = oRs("DriversLicenseNo")
	sAddress = oRs("Address")
	sCity = oRs("City")
	iState = oRs("StateID")
	sZipcode = oRs("Zipcode")
	sPhoneNo = oRs("Phone")
	sFaxNo = oRs("Fax")
	sCertCompanyName = oRs("CertCompanyName")
	bNewsletter = oRs("Newsletter")
	
	sMode = "EDIT"
else
	sMode = "ADD"
end if

sReferrer = trim(request("referrer"))

	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td background="/media/images/courseBgr.gif" width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td>
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle">User Registration Form</span>
												<br>
												<br><span class="newsTitle">If you have an account, use the login boxes above to sign in.</span>
												<form name="RegistrationForm" method="POST" action="registerproc.asp" onSubmit="return Validate(this)">
												<input type="hidden" name="referrer" value="<%=sReferrer%>">
												
<table cellpadding="4" cellspacing="4">
	<tr>
		<td colspan="2">
			<span class="formstar">*</span> Required Fields
		</td>
	</tr>
<% if trim(sMemberNumber) <> "" then %>	
	<tr>
		<td><b>Membership Number:</b></td>
		<td><%= sMemberNumber %></td>
	</tr>	
<% end if %>	
	<tr>
		<td><b>First Name:</b><span class="formstar">*</span></td>
		<td>
<% if UCase(sMode) = "ADD" then %>
			<input type="text" name="FirstName" value="<% = sFirstName %>" size="30" maxlength="50">
<% else %>
			<%=sFirstName%><input type="hidden" name="FirstName" value="<% = sFirstName %>">
<% end if %>			
		</td>				
	</tr>
	<tr>
		<td><b>Mid. Initial:</b></td>
		<td>
<% if UCase(sMode) = "ADD" then %>		
			<input type="text" name="MidInitial" value="<% = sMidInitial %>" size="1" maxlength="1">
<% else %>	
			<%=sMidInitial%><input type="hidden" name="MidInitial" value="<% = sMidInitial %>">
<% end if %>
		</td>				
	</tr>
	<tr>
		<td><b>Last Name:</b><span class="formstar">*</span></td>
		<td>
<% if UCase(sMode) = "ADD" then %>			
			<input type="text" name="LastName" value="<% = sLastName %>" size="30" maxlength="50">
<% else %>
			<%=sLastname%><input type="hidden" name="LastName" value="<%=sLastName%>">
<% end if %>			
		</td>				
	</tr>
	<tr>
		<td><b>Email:</b><span class="formstar">*</span></td>
		<td><input type="text" name="Email" value="<% = sEmail %>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>Password:</b><span class="formstar">*</span></td>
		<td><input type="password" name="Password" value="<% = sPassword %>" size="30" maxlength="20"></td>				
	</tr>
	<tr>
		<td><b>Confirm Password:</b><span class="formstar">*</span></td>
		<td><input type="password" name="ConfirmPassword" value="<% = sPassword %>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>SSN:</b><span class="formstar">*</span></td>
		<td><input type="text" name="SSN" value="<% = sSSN %>" size="11" maxlength="11"></td>				
	</tr>			
	<tr>
		<td><b>License No:</b></td>
		<td><input type="text" name="LicenseNo" value="<% = sLicenseNo %>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>Driver's License No.:</b></td>
		<td><input type="text" name="DriversLicenseNo" value="<% = sDriversLicenseNo %>" size="30" maxlength="25"></td>				
	</tr>		
	<tr>
		<td><b>Address:</b><span class="formstar">*</span></td>
		<td><input type="text" name="Address" value="<% = sAddress %>" size="30" maxlength="100"></td>				
	</tr>				
	<tr>
		<td><b>City:</b></td>
		<td><input type="text" name="City" value="<% = sCity %>" size="30" maxlength="50"></td>				
	</tr>	
	<tr>
		<td><b>State:</b></td>
		<td><% call DisplayStatesDropDown(iStateID,1,0) %></td>
	</tr>
	<tr>
		<td><b>Zipcode:</b><span class="formstar">*</span></td>
		<td><input type="text" name="Zipcode" value="<% = sZipcode %>" size="10" maxlength="10"></td>				
	</tr>
	<tr>
		<td><b>Phone No.:</b><span class="formstar">*</span></td>
		<td><input type="text" name="Phone" value="<% = sPhoneNo %>" size="30" maxlength="20"></td>				
	</tr>
	<tr>
		<td><b>Fax No.:</b></td>
		<td><input type="text" name="Fax" value="<% = sFaxNo %>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>Company Name<br>on Certificate:</b></td>
		<td><input type="text" name="CertCompanyName" value="<% = sCertCompanyName %>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>Subscribe to<br>Newsletter:</b></td>
<% if bNewsletter then %>
		<td valign="middle"><input type="checkbox" name="Newsletter" CHECKED></td>
<% else %>
		<td valign="middle"><input type="checkbox" name="Newsletter"></td>
<% end if %>
	</tr>				
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="<% = application("sDynMediaPath") %>bttnContinue.gif">&nbsp;
			<a href="javascript:history.back()"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" border=0></a>
		</td>
	</tr>
</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>					

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

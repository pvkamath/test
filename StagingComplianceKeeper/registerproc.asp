<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" ----->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual = "/includes/User.Class.asp" --------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable

	' Set Page Title:
	sPgeTitle = "TrainingPro"

	if session("User_ID") = "" then
		iPage = 1
	else
		iPage = 0
	end if

	sKeywords = getFormElement("keywords")
	
	'set current date
	sCurrentDate = dayName(Weekday(Now)) & " " & date() & " at " & time()

' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}

		var preloadFlag = true;

		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>

<table width=100% height="400" border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Course Column  -->
					<td width=100% valign="top">
						<table width=100% border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td><img src="/Media/Images/courseTop.gif" width="572" height="20" alt="" border="0"></td>
							</tr>
							<tr>
								<td background="/media/images/courseBgrColor.gif">
									<table width=100% border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="date"><%=sCurrentDate%></span></td>
										</tr>
										<tr>
											<td><span class="pagetitle">User Registration</span>
<%

dim oRs
dim oUser
set oUser = new User

dim iUserID
dim sEmail, sOldEmail, sPassword, sConfirmPassword
dim sFirstName, sMid, sLastName
dim iGroupID
'dim iGroupID, iCompanyID, iBranchID
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo
dim sCity, sAddress, sZipcode, sPhone, sFax
dim iStateID
dim iUserStatus
dim iCrossCertEligible
dim iNewsletter
'dim sMarketWareConfirmNo
'dim sNotes
dim sMessage
'dim bSuccess
dim bError
dim sFullName 'Stores the full user name after registration
dim sReferrer

bError = false

oUser.ConnectionString = application("sDataSourceName")

sReferrer = trim(request("Referrer"))
sEmail = ScrubForSQL(request("Email"))
sOldEmail = ScrubForSQL(request("OldEmail"))
sPassword = ScrubForSQL(request("Password"))
sConfirmPassword = ScrubForSQL(request("ConfirmPassword"))
sFirstName = ScrubForSQL(request("FirstName"))
sMid = ScrubForSQL(request("MidInitial"))
sLastName = ScrubForSQL(request("LastName"))
iGroupID = 5 'Student
'iCompanyID = ScrubForSQL(request("Company"))
'iBranchID = ScrubForSQL(request("Branch"))
sCertCompanyName = ScrubForSQL(request("CertCompanyName"))
sLicenseNo = ScrubForSQL(request("LicenseNo"))
sSSN = ScrubForSQL(request("SSN"))
sDriversLicenseNo = ScrubForSQL(request("DriversLicenseNo"))
sCity = ScrubForSQL(request("City"))
iStateID = ScrubForSQL(request("State"))
sAddress = ScrubForSQL(request("Address"))
sZipcode = ScrubForSQL(request("Zipcode"))
sPhone = ScrubForSQL(request("Phone"))
sFax = ScrubForSQL(request("Fax"))
'iUserStatus = ScrubForSQL(request("UserStatus"))
'sMarketWareConfirmNo = ScrubForSQL(request("MarketWareConfirmNo"))
'sNotes = ScrubForSQL(request("Notes"))

if (Ucase(trim(request("Newsletter"))) = "ON") then
	iNewsLetter = 1
else
	iNewsLetter = 0
end if

'Make Sure this Email Address is not in the system
if (oUser.EmailExists(sEmail)) then

	oUser.UserID = session("User_ID")
	set oRs = oUser.GetUser()
	if not (oRs.BOF and oRs.EOF) then

		if oRs("Email") = sEmail then

			bError = false

		else

			sMessage = "Sorry, the email address """ & sEmail & """ is already taken.  Please try a new address."
			bError = true

		end if

	else

		sMessage = "Sorry, the email address """ & sEmail & """ is already taken.  Please try a new address."
		bError = true

	end if 

else



end if

if bError = false then

	oUser.Email = sEmail
	oUser.Password = sPassword
	oUser.FirstName = sFirstName
	oUser.MidInitial = sMid
	oUser.LastName = sLastName
	oUser.GroupID = iGroupID
'	oUserObj.CompanyID = iCompanyID
'	oUserObj.BranchID = iBranchID
	oUser.CertCompanyName = sCertCompanyName
	oUser.LicenseNo = sLicenseNo
	oUser.SSN = sSSN
	oUser.DriversLicenseNo = sDriversLicenseNo
	oUser.City = sCity
	oUser.StateID = iStateID
	oUser.Address = sAddress
	oUser.Zipcode = sZipcode
	oUser.Phone = sPhone
	oUser.Fax = sFax
	oUser.UserStatus = 1 'Active
'	oUserObj.MarketWareConfirmNo = sMarketWareConfirmNo
'	oUserObj.Notes = sNotes
'	oUserObj.CrossCertEligible = iCrossCertEligible
	oUser.Newsletter = iNewsletter
	
	if session("User_ID") <> "" then

		oUser.UserID = session("User_ID")

		if not oUser.EditUser then

			sMessage = "There was a problem updating your account.  Please try again at another time."
			bError = true

		else

			sMessage = "Your account has been successfully updated.  Thank you."
			if oUser.MidInitial <> "" then
				sFullName = oUser.FirstName & " " & oUser.MidInitial & ". " & oUser.LastName
			else
				sFullName = oUser.FirstName & " " & oUser.LastName
			end if			
			session("User_FullName") = sFullName
			
			bError = false

		end if 

	else

		iUserID = oUser.AddUser()

		if iUserID <> -1 then

			sMessage = "Your new account """ & sEmail & """ has been successfully added.  Thank you."

			session("Access_Level") = iGroupID
			session("User_ID") = iUserID
			session("email") = sEmail
			if oUser.MidInitial <> "" then
				sFullName = oUser.FirstName & " " & oUser.MidInitial & ". " & oUser.LastName
			else
				sFullName = oUser.FirstName & " " & oUser.LastName
			end if			
			session("User_FullName") = sFullName

			bError = false

		else

			sMessage = "There was an error creating the new user.  Please try again at another time."
			bError = true		

		end if

	end if

end if

%>

<p>
<b><% = sMessage %></b>
<p>

<% if bError then %>
<a href="javascript:history.back()"><img src="<% = application("sDynMediaPath") %>bttnBack.gif" alt="Go Back" border="0"></a>
<%
else
	if UCase(sReferrer) = "PURCHASE" then
%>
<a href="/Purchase/BillingInfo.asp"><img src="<% = application("sDynMediaPath") %>bttnContinue.gif" alt="Continue Your Purchase" border="0"></a>
<%
	else
%>
<a href="/userPage.asp"><img src="<% = application("sDynMediaPath") %>bttnContinue.gif" alt="Proceed to Your User Page" border="0"></a>
<%
	end if
end if
%>

<!-- Page Extension -->
<br>
<img src="<% = application("sDynMediaPath") %>clear.gif" width="1" height="150" alt="" border="0">

											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/courseBttm.gif" width="572" height="10" alt="" border="0"></td>
							</tr>
						</table>
				
					</td>					


<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

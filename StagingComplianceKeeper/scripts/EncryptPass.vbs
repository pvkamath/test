dim sConnectionString
dim rs
dim objconn
dim sSQL
dim sSSN
Dim sbox(255)
Dim key(255)
   
'sConnectionString = "Provider=SQLOLEDB; Data Source=devdb1; Initial Catalog=ComplianceKeeperCorp; User Id=sa; Password=x1gr0up"
'sConnectionString = "Provider=SQLOLEDB; Data Source=STAGEWEB; Initial Catalog=ComplianceKeeperCorp; User Id=sa; Password=0verl0ad"
sConnectionString = "Provider=SQLOLEDB; Data Source=192.168.1.11; Initial Catalog=ComplianceKeeperCorp; User Id=sa; Password=Gr3nad3$"

Set objConn = CreateObject("ADODB.Connection")
Set rs = CreateObject("ADODB.RecordSet")
objConn.Open sConnectionString
rs.cursorlocation = 3
rs.CursorType = 3

sSQL = "SELECT UserID, SSN FROM Users ORDER By UserID  "

rs.Open sSQL, objConn
rs.ActiveConnection = nothing

do while not rs.eof
	wscript.echo(rs("UserID"))
	sSSN = rs("SSN")
	
	if isnull(sSSN) or trim(sSSN) = "" then
		sSSN = "111-11-1111"
	end if

	sSSN = EnDeCrypt(sSSN,"HGSDYGDSLWREIUCJD938439402342") 'located below
	
	objConn.execute("UPDATE Users SET SSN = '" & sSSN & "' WHERE UserID = " & rs("UserID"))
	rs.MoveNext
loop
rs.Close

sSQL = "SELECT UserID, SSN FROM Associates ORDER By UserID  "
rs.Open sSQL, objConn
rs.ActiveConnection = nothing

do while not rs.eof
	wscript.echo(rs("UserID"))
	sSSN = rs("SSN")
	
	if isnull(sSSN) or trim(sSSN) = "" then
		sSSN = "111-11-1111"
	end if

	sSSN = EnDeCrypt(sSSN,"HGSDYGDSLWREIUCJD938439402342") 'located below
	
	objConn.execute("UPDATE Associates SET SSN = '" & sSSN & "' WHERE UserID = " & rs("UserID"))
	rs.MoveNext
loop

set rs = nothing
set objConn = nothing

'RC4 Functions
'-------------------------------------------------------------------------------------------------------------------------------
' Comersus 3.51
' e-commerce ASP Open Source
' CopyRight Rodrigo S. Alhadeff, Comersus
' Buenos Aires, 05/2001
' http://www.comersus.com 
' Details: RC4 encryption functions

   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   ':::                                                             :::
   ':::  This script performs 'RC4' Stream Encryption               :::
   ':::  (Based on what is widely thought to be RSA's RC4           :::
   ':::  algorithm. It produces output streams that are identical   :::
   ':::  to the commercial products)                                :::
   ':::                                                             :::
   ':::  This script is Copyright � 1999 by Mike Shaffer            :::
   ':::  ALL RIGHTS RESERVED WORLDWIDE                              :::
   ':::                                                             :::
   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


   Sub RC4Initialize(strPwd)
   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   ':::  This routine called by EnDeCrypt function. Initializes the :::
   ':::  sbox and the key array)                                    :::
   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

      dim tempSwap
      dim a
      dim b
	  dim intLength
	  
      intLength = len(strPwd)
      For a = 0 To 255
         key(a) = asc(mid(strpwd, (a mod intLength)+1, 1))
         sbox(a) = a
      next

      b = 0
      For a = 0 To 255
         b = (b + sbox(a) + key(a)) Mod 256
		 
         tempSwap = sbox(a)
         sbox(a) = sbox(b)
         sbox(b) = tempSwap
      Next
   
   End Sub
   
   Function EnDeCrypt(plaintxt, psw)
   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   ':::  This routine does all the work. Call it both to ENcrypt    :::
   ':::  and to DEcrypt your data.                                  :::
   ':::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

      dim temp
      dim a
      dim i
      dim j
      dim k
      dim cipherby
      dim cipher

      i = 0
      j = 0

      RC4Initialize psw

      For a = 1 To Len(plaintxt)
         i = (i + 1) Mod 256
         j = (j + sbox(i)) Mod 256
         temp = sbox(i)
         sbox(i) = sbox(j)
         sbox(j) = temp
   
         k = sbox((sbox(i) + sbox(j)) Mod 256)

         cipherby = Asc(Mid(plaintxt, a, 1)) Xor k
         cipher = cipher & Chr(cipherby)
      Next

      EnDeCrypt = cipher
      ' remove quotes 
      EnDeCrypt	= replace(EnDeCrypt,"'","''")      
   End Function
'Send Calendar Event Alerts to Company Admins 1 month prior to the event

dim objConn, rs 'as object
dim sEventTitle 'as string
dim iEventID 'as integer
dim iCompanyID 'as integer
dim s30DayAlertDate 'as string
dim sCompanyAdminEmails 'as string
dim sStartDate 'as string

'sConnectionString = "Provider=SQLOLEDB; Data Source=devdb1; Initial Catalog=compliancekeepercorp; User Id=sa; Password=x1gr0up"
'sEmailServer = "smtp.xigroup.com"
'sConnectionString = "Provider=SQLOLEDB; Data Source=STAGEWEB; Initial Catalog=compliancekeepercorp; User Id=sa; Password=0verl0ad"
'sEmailServer = "smtp.xigroup.com"
sConnectionString = "Provider=SQLOLEDB; Data Source=VM-DEV-01\SQL2008; Initial Catalog=compliancekeepercorp; User Id=sa; Password=Educat10n"
sEmailServer = "192.168.100.58:225"

sUrl = "https://www.compliancekeeper.com"
'sUrl = "http://dev.compliancekeepercorp"

Set objConn = CreateObject("ADODB.Connection")
Set rs = CreateObject("ADODB.RecordSet")
objConn.ConnectionString = sConnectionString
objConn.Open		

'Compute the event date to check for (1 month from current date)
s30DayAlertDate = dateadd("M", 1, date())

sSQL = "SELECT EventID, Event, StartDate, CompanyID FROM afxEvents WHERE UseTracker = 1 AND StartDate >= '" & s30DayAlertDate & " 00:00' AND StartDate <= '" & s30DayAlertDate & " 23:59'"
rs.Open sSQL, objConn, 3, 3

'wscript.echo "Start"
do while not rs.eof
	iEventID = rs("EventID")
	sEventTitle = rs("Event")
	sStartDate = rs("StartDate")
	iCompanyID = rs("CompanyID")

	'Get the Company Admin Email addresses
	sCompanyAdminEmails = BuildCompanyAdminEmailList(iCompanyID, iEventID)

	'Send Email
	if sCompanyAdminEmails <> "" then
		Call SendAlertMessage(iEventID, sEventTitle, sStartDate, sCompanyAdminEmails)
	end if
	
	rs.MoveNext
loop
'wscript.echo "End"

set objConn = nothing
set rs = nothing

'Creates a Comma Delimited String of company admin email addresses
Function BuildCompanyAdminEmailList(p_iCompanyID, p_iEventID)
	dim objConn, rs, rs2 'as object
	dim sSQL 'as string
	dim sEmailList 'as string
	dim iUserID 'as integer
	
	Set objConn = CreateObject("ADODB.Connection")
	Set rs = CreateObject("ADODB.RecordSet")
	Set rs2 = CreateObject("ADODB.RecordSet")
	objConn.ConnectionString = sConnectionString
	objConn.Open			
	
	sSQL = "SELECT UserID, Email, GroupID FROM Users WHERE CompanyID = " & p_iCompanyID & " AND UserStatus = 1 AND CConAlertEmails = 1"
	rs.Open sSQL, objConn, 3, 3
	
	sEmailList = ""
	do while not rs.eof
		iUserID = rs("UserID")
		
		'User Access Lists
		sCompanyL2IdList = GetUserCompanyL2IdList(iUserID, iCompanyID, rs("GroupID"))
		sCompanyL3IdList = GetUserCompanyL3IdList(iUserID, iCompanyID, rs("GroupID"))
		sBranchIdList = GetUserBranchIdList(iUserID, iCompanyID, rs("GroupID"))

		'Determine if this user should get the alert of the Event		
		sSQL = "SELECT COUNT(*) FROM afxEvents WHERE EventID = " & p_iEventID & " "
	
		'Division Clause
		sSQL = sSQL & "AND (CompanyL2Id is null"
		if trim(sCompanyL2IdList) <> "" then
			sSQL = sSQL & " OR CompanyL2Id in (" & sCompanyL2IdList & ")"
		end if
		sSQL = sSQL & ") "

		'Region Clause
		sSQL = sSQL & "AND (CompanyL3Id is null"
		if trim(sCompanyL3IdList) <> "" then
			sSQL = sSQL & " OR CompanyL3Id in (" & sCompanyL3IdList & ")"
		end if
		sSQL = sSQL & ") "

		'Branch Clause
		sSQL = sSQL & "AND (BranchId is null"
		if trim(sBranchIdList) <> "" then
			sSQL = sSQL & " OR BranchID in (" & sBranchIdList & ")"
		end if
		sSQL = sSQL & ") "	

		rs2.Open sSQL, objConn, 3, 3
		
		'The User has access to this event
		if cint(rs2(0)) > 0 then			
			if trim(sEmailList) = "" then
				sEmailList = rs("Email")
			else
				sEmailList = sEmailList & ", " & rs("Email")
			end if
		end if
		rs2.Close
		
		rs.MoveNext
	loop	
	
	BuildCompanyAdminEmailList = sEmailList
	
	set objConn = nothing
	set rs = nothing
	set rs2 = nothing	
End Function

'Creates and Sends The Alert Emails to the Company Admins using Jmail
Sub SendAlertMessage(p_iEventID, p_sEventTitle, p_sStartDate, p_sCompanyAdminEmails)
	dim strMailServerAddress	'The SMTP server that Jmail will use
	dim objJmail

	'Create an instance of the Jmail object and set it's initial values
	Set objJMail = CreateObject("JMail.Message")
	objJMail.ContentType = "text/html"

	'set the Jmail values
	objJmail.From = "alerts@compliancekeeper.com"
	objJmail.FromName = "ComplianceKeeper.com Alerts"
	objJmail.Subject = p_sEventTitle
	
	objJmail.Body = "The event listed below will occur in 30 Days.  If you need to reschedule this event, you should go to the Edit events page to change the start date.<br><br>" & p_sEventTitle & " - " & p_sStartDate & "<br>" &_
		            "<a href=""" & sURL & "/calendar/EditEvent.asp?eventid=" & p_iEventID & "&referrer=EVENTALERTEMAIL"">Click here to Edit the Event</a>"
					
	for each sEmail in split(p_sCompanyAdminEmails, ", ")
		objJmail.AddRecipient sEmail
	next
	
	'mail it
	objJmail.Send(sEmailServer)
	objJmail.Close		
	
	set objJmail = nothing	
End Sub

'Get the list of BranchIds to which the user has view or admin access.  
'These are not necessarily directly assigned, but may instead be assigned 
'to an L2 or L3 which itself belongs to the user.
function GetUserBranchIdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim oCompanyL3Rs
	dim sUserBranchAccessList 'as string
	dim sUserCompanyL2AccessList 'as string
	dim sUserCompanyL3AccessList 'as string
		
	'Default Value
	GetUserBranchIdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	set oCompanyL3Rs = CreateObject("ADODB.Recordset")
	oCompanyL3Rs.CursorLocation = 3
	oCompanyL3Rs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every branch assigned to the company.
		sSQL = "SELECT BranchId FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		do while not oRs.EOF
			if trim(GetUserBranchIdList) <> "" then
				GetUserBranchIdList = GetUserBranchIdList & ", "
			end if				
			GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close	
	
		sSQL = "SELECT BranchId, CompanyL2Id, CompanyL3Id FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		do while not oRs.EOF
			if oRs("CompanyL2Id") <> "" then
				if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then
					'User has access to the branch if it is assigned to one of his own CL2s.
					if trim(GetUserBranchIdList) <> "" then
						GetUserBranchIdList = GetUserBranchIdList & ", "
					end if						
					GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
				end if
			elseif oRs("CompanyL3Id") <> "" then
				'User has access if the branch is assigned to a CL3 which is itself assigned to one of his CL2s.
				sSQL = "SELECT CompanyL3Id, CompanyL2Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 " & _
						"AND NOT CompanyL2Id IS NULL"
				oCompanyL3Rs.Open sSQL, oConn
				oCompanyL3Rs.ActiveConnection = nothing
				do while not oCompanyL3Rs.EOF 
					if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oCompanyL3Rs("CompanyL2Id") & ",") then
						if trim(GetUserBranchIdList) <> "" then
							GetUserBranchIdList = GetUserBranchIdList & ", "
						end if								
					
						GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
					end if

					oCompanyL3Rs.MoveNext
				loop
				oCompanyL3Rs.Close
			end if
			oRs.MoveNext
		loop
	
	elseif p_iGroupID = 5 or p_iGroupID = 6 then
		'Get the CompanyL3s this user has access to
		sUserCompanyL3AccessList = ""
		
		sSQL = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL3AccessList) <> "" then
				sUserCompanyL3AccessList = sUserCompanyL3AccessList & ", "
			end if
			
			sUserCompanyL3AccessList = sUserCompanyL3AccessList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
		oRs.Close
	
		'User has access if the branch is assigned to one of his CL3s.
		sSQL = "SELECT BranchId, CompanyL3Id FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 " & _
				"AND NOT CompanyL3Id IS NULL"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL3AccessList & ",", ", " & oRs("CompanyL3Id") & ",") then
				if trim(GetUserBranchIdList) <> "" then
					GetUserBranchIdList = GetUserBranchIdList & ", "
				end if					
				GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			end if
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 10 or p_iGroupID = 11 then
		'Get the Branches this user has access to
		sUserBranchAccessList = ""
		
		sSQL = "SELECT BranchId FROM UsersBranches WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserBranchAccessList) <> "" then
				sUserBranchAccessList = sUserBranchAccessList & ", "
			end if
			
			sUserBranchAccessList = sUserBranchAccessList & oRs("BranchId")
			oRs.MoveNext
		loop
		oRs.Close	
	
		'User has access if the branch is one of his branches.  
		sSQL = "SELECT BranchId FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		do while not oRs.EOF
			if instr(1, ", " & sUserBranchAccessList & ",", ", " & oRs("BranchId") & ",") then
				if trim(GetUserBranchIdList) <> "" then
					GetUserBranchIdList = GetUserBranchIdList & ", "
				end if						
				GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			end if
			oRs.MoveNext
		loop
	end if
		
	set oConn = nothing
	set oRs = nothing			
end function
	

'Get the list of CompanyL2Ids to which the user has view or admin access.  
function GetUserCompanyL2IdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim sUserCompanyL2AccessList 'as string
	
	'Default value
	GetUserCompanyL2IdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every company L2 assigned to the
		'company.
		sSQL = "SELECT CompanyL2Id FROM CompaniesL2 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(GetUserCompanyL2IdList) <> "" then
				GetUserCompanyL2IdList = GetUserCompanyL2IdList & ", "
			end if		
			GetUserCompanyL2IdList = GetUserCompanyL2IdList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close
	
		'CompanyL2 admins/viewers have access if the CompanyL2 is in their list of assigned CL2s.
		sSQL = "SELECT CompanyL2Id FROM CompaniesL2 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then
				if trim(GetUserCompanyL2IdList) <> "" then
					GetUserCompanyL2IdList = GetUserCompanyL2IdList & ", "
				end if
				GetUserCompanyL2IdList = GetUserCompanyL2IdList & oRs("CompanyL2Id")
			end if
			oRs.MoveNext
		loop
	end if
	
	set oConn = nothing
	set oRs = nothing	
end function

'Get the list of CompanyL3Ids to which the user has view or admin access.  
function GetUserCompanyL3IdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim sUserCompanyL2AccessList, sUserCompanyL3AccessList 'as string
	
	'Default value
	GetUserCompanyL3IdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every company L3 assigned to the
		'company.
		sSQL = "SELECT CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(GetUserCompanyL3IdList) <> "" then
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
			end if		
			GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close	
	
		'CompanyL2 admins/viewers have access if the company L3 is assigned to one
		'of their company L2s.
		sSQL = "SELECT CompanyL2Id, CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' " & _
				"AND NOT CompanyL2Id IS NULL"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then			
				'User has access to the companyL3 is assigned to one of his own CL2s.
				if trim(GetUserCompanyL3IdList) <> "" then
					GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
				end if						
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			end if
			oRs.MoveNext
		loop	
	elseif p_iGroupID = 5 or p_iGroupID = 6 then
		'Get the CompanyL3s this user has access to
		sUserCompanyL3AccessList = ""
		
		sSQL = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL3AccessList) <> "" then
				sUserCompanyL3AccessList = sUserCompanyL3AccessList & ", "
			end if
			
			sUserCompanyL3AccessList = sUserCompanyL3AccessList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
		oRs.Close
		
		'CompanyL3 admins/viewers have access if the CompanyL3 is in their list of assigned CL3s.
		sSQL = "SELECT CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL3AccessList & ",", ", " & oRs("CompanyL3Id") & ",") then
				if trim(GetUserCompanyL3IdList) <> "" then
					GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
				end if				
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			end if
			oRs.MoveNext
		loop
	end if
	
	set oConn = nothing
	set oRs = nothing	
end function
'Send Fingerprint Deadline Alerts to Officers and Administrators 30, 60, and 90 days from the date

dim objConn, rs 'as object
dim s30DayAlertDate 'as string
dim s60DayAlertDate 'as string
dim s90DayAlertDate 'as string
dim sCompanyAdminEmails 'as string
dim sCompanyL2IdList, sCompanyL3IdList, sBranchIdList 'as string
		
'sConnectionString = "Provider=SQLOLEDB; Data Source=devdb1; Initial Catalog=compliancekeepercorp; User Id=sa; Password=x1gr0up"
'sEmailServer = "smtp.xigroup.com"
'sConnectionString = "Provider=SQLOLEDB; Data Source=STAGEWEB; Initial Catalog=compliancekeepercorp; User Id=sa; Password=0verl0ad"
'sEmailServer = "smtp.xigroup.com"
sConnectionString = "Provider=SQLOLEDB; Data Source=192.168.100.59; Initial Catalog=compliancekeepercorp; User Id=sa; Password=Educat10n"
sEmailServer = "192.168.100.58:25"

sUrl = "https://www.compliancekeeper.com"
'sUrl = "http://dev.compliancekeepercorp"

'Compute the Alert Dates
s30DayAlertDate = dateadd("M", 1, date())
s60DayAlertDate = dateadd("M", 2, date())
s90DayAlertDate = dateadd("M", 3, date())

Call SendAlerts(s30DayAlertDate, "30")
Call SendAlerts(s60DayAlertDate, "60")
Call SendAlerts(s90DayAlertDate, "90")

'----------------------------------------------------------------------------------
Sub SendAlerts(p_sAlertDate, p_sDaycount)
	dim objConn, rs, rs2 'as object
	dim sSQL 'as string
	dim iCompanyID, iCompanyL2ID, iCompanyL3ID, iBranchID 'as integer
	dim iUserID 'as integer
	dim sFullName 'as string
	
	Set objConn = CreateObject("ADODB.Connection")
	Set rs = CreateObject("ADODB.RecordSet")
	Set rs2 = CreateObject("ADODB.RecordSet")
	objConn.ConnectionString = sConnectionString
	objConn.Open	

	sSQL = "SELECT UserID, Firstname, Lastname, Email, CompanyID, CompanyL2ID, CompanyL3ID, BranchID, FingerprintDeadlineDate FROM Associates " & _
				  "WHERE FingerprintDeadlineDate >= '" & p_sAlertDate & " 00:00' AND FingerprintDeadlineDate <= '" & p_sAlertDate & " 23:59' " & _
				  "AND UserStatus = 1 " & _
				  "ORDER BY CompanyID, Email "
				  
	rs.Open sSQL, objConn, 3, 3
	
	do while not rs.eof
		iUserID = rs("UserID")
		sFullName = rs("Firstname") & " " & rs("LastName")
		iCompanyID = rs("CompanyID")
		iCompanyL2ID = rs("CompanyL2ID")
		iCompanyL3ID = rs("CompanyL3ID")
		iBranchID = rs("BranchID")
		sFingerprintDeadlineDate = rs("FingerprintDeadlineDate")
		
		'Get the company email settings
		sSQL2 = "SELECT CASE WHEN SendCoAdminFingerprintDeadlineEmail Is Null THEN 0 ELSE SendCoAdminFingerprintDeadlineEmail END AS SendCoAdminFingerprintDeadlineEmail, " & _
									  " CASE WHEN SendBrAdminFingerprintDeadlineEmail Is Null THEN 0 ELSE SendBrAdminFingerprintDeadlineEmail END AS SendBrAdminFingerprintDeadlineEmail, " & _
									  " CASE WHEN SendL2AdminFingerprintDeadlineEmail Is Null THEN 0 ELSE SendL2AdminFingerprintDeadlineEmail END AS SendL2AdminFingerprintDeadlineEmail, " & _
									  " CASE WHEN SendL3AdminFingerprintDeadlineEmail Is Null THEN 0 ELSE SendL3AdminFingerprintDeadlineEmail END AS SendL3AdminFingerprintDeadlineEmail, " & _
									  " CASE WHEN SendLoFingerprintDeadlineEmail Is Null THEN 0 ELSE SendLoFingerprintDeadlineEmail END AS SendLoFingerprintDeadlineEmail " & _												  
						"FROM CompanySettings " & _
				        "WHERE CompanyID = " & iCompanyID
		rs2.Open sSQL2, objConn, 3, 3

		'Get the Company Admin Email addresses
		sCompanyAdminEmails = BuildCompanyAdminEmailList(iCompanyID, iCompanyL2ID, iCompanyL3ID, iBranchID, cbool(rs2("SendCoAdminFingerprintDeadlineEmail")), cbool(rs2("SendBrAdminFingerprintDeadlineEmail")), cbool(rs2("SendL2AdminFingerprintDeadlineEmail")), cbool(rs2("SendL3AdminFingerprintDeadlineEmail")))

		'Determine if the Associate should receive the email
		sEmail = ""
		if cbool(rs2("SendLoFingerprintDeadlineEmail")) then
			sEmail = rs("Email")
		end if

		rs2.Close()
		
		'Send Email if the Associate or Email lists are not empty
		if trim(sEmail) <> "" or trim(sCompanyAdminEmails) <> "" then
			Call SendEmail(iUserID, sFullName, sEmail, iCompanyID, sCompanyAdminEmails, sFingerprintDeadlineDate, p_sDaycount)
		end if
		
		rs.MoveNext
	loop
	
	set objConn = nothing	
	set rs = nothing
	set rs2 = nothing
End Sub

'Creates a Comma Delimited String of company admin email addresses
Function BuildCompanyAdminEmailList(p_iAssociateCompanyID, p_iAssociateCompanyL2ID, p_iAssociateCompanyL3ID, p_iAssociateBranchID, p_bSendCOAdminAlerts, p_bSendBrAdminAlerts, p_bSendL2AdminAlerts, p_bSendL3AdminAlerts)
	dim objConn, rs 'as object
	dim sSQL 'as string
	dim sEmailList 'as string
	dim iUserID 'as integer
	dim bFirstGroup 'as string
	
	BuildCompanyAdminEmailList = "" 'default

	'If all Admin settings are off, exit
	if not (p_bSendCOAdminAlerts or p_bSendBrAdminAlerts or p_bSendL2AdminAlerts or p_bSendL3AdminAlerts) then
		exit function
	end if

	Set objConn = CreateObject("ADODB.Connection")
	Set rs = CreateObject("ADODB.RecordSet")

	objConn.ConnectionString = sConnectionString
	objConn.Open			
	
	sSQL = "SELECT UserID, Email, GroupID FROM Users WHERE CompanyID = " & p_iAssociateCompanyID & " AND UserStatus = 1 AND CConAlertEmails = 1 AND GroupID in ("
	
	bFirstGroup = true
	
	'Pull Company Admins
	if p_bSendCOAdminAlerts then
		sSQL = sSQL & "2"
		bFirstGroup = false
	end if
	
	'Pull L2 Admins
	if p_bSendL2AdminAlerts then
		if bFirstGroup then
			bFirstGroup = false			
		else
			sSQL = sSQL & ","
		end if
		sSQL = sSQL & "3,4"
	end if
		
	'Pull L3 Admins
	if p_bSendL3AdminAlerts then
		if bFirstGroup then
			bFirstGroup = false			
		else
			sSQL = sSQL & ","
		end if
		sSQL = sSQL & "5,6"
	end if
		
	'Pull Branch Admins
	if p_bSendBrAdminAlerts then
		if bFirstGroup then
			bFirstGroup = false			
		else
			sSQL = sSQL & ","
		end if
		sSQL = sSQL & "10,11"
	end if
	sSQL = sSQL & ")"

	rs.Open sSQL, objConn, 3, 3
	
	sEmailList = ""
	do while not rs.eof
		'Get the User Access Lists (The Divisions/Regions/Branches this administrator has access to)
		sCompanyL2IdList = GetUserCompanyL2IdList(rs("UserID"), p_iAssociateCompanyID, rs("GroupID"))
		sCompanyL3IdList = GetUserCompanyL3IdList(rs("UserID"), p_iAssociateCompanyID, rs("GroupID"))
		sBranchIdList = GetUserBranchIdList(rs("UserID"), p_iAssociateCompanyID, rs("GroupID"))
				
		'Determine if the administrator has access to the current Associates info
		if ValueInDelString(p_iAssociateCompanyL2ID, sCompanyL2IdList) or ValueInDelString(p_iAssociateCompanyL3ID, sCompanyL3IdList) or ValueInDelString(p_iAssociateBranchID, sBranchIdList) then
			'Add to list
			if trim(BuildCompanyAdminEmailList) <> "" then
				BuildCompanyAdminEmailList = BuildCompanyAdminEmailList & ", "
			end if
			BuildCompanyAdminEmailList = BuildCompanyAdminEmailList & rs("email")
		end if		
				
		rs.MoveNext
	loop
	
	set rs = nothing
	set objConn = nothing
End Function 

'Get the list of BranchIds to which the user has view or admin access.  
'These are not necessarily directly assigned, but may instead be assigned 
'to an L2 or L3 which itself belongs to the user.
function GetUserBranchIdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim oCompanyL3Rs
	dim sUserBranchAccessList 'as string
	dim sUserCompanyL2AccessList 'as string
	dim sUserCompanyL3AccessList 'as string
		
	'Default Value
	GetUserBranchIdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	set oCompanyL3Rs = CreateObject("ADODB.Recordset")
	oCompanyL3Rs.CursorLocation = 3
	oCompanyL3Rs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every branch assigned to the company.
		sSQL = "SELECT BranchId FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		do while not oRs.EOF
			if trim(GetUserBranchIdList) <> "" then
				GetUserBranchIdList = GetUserBranchIdList & ", "
			end if				
			GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close	
	
		sSQL = "SELECT BranchId, CompanyL2Id, CompanyL3Id FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		do while not oRs.EOF
			if oRs("CompanyL2Id") <> "" then
				if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then
					'User has access to the branch if it is assigned to one of his own CL2s.
					if trim(GetUserBranchIdList) <> "" then
						GetUserBranchIdList = GetUserBranchIdList & ", "
					end if						
					GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
				end if
			elseif oRs("CompanyL3Id") <> "" then
				'User has access if the branch is assigned to a CL3 which is itself assigned to one of his CL2s.
				sSQL = "SELECT CompanyL3Id, CompanyL2Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 " & _
						"AND NOT CompanyL2Id IS NULL"
				oCompanyL3Rs.Open sSQL, oConn
				oCompanyL3Rs.ActiveConnection = nothing
				do while not oCompanyL3Rs.EOF 
					if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oCompanyL3Rs("CompanyL2Id") & ",") then
						if trim(GetUserBranchIdList) <> "" then
							GetUserBranchIdList = GetUserBranchIdList & ", "
						end if								
					
						GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
					end if

					oCompanyL3Rs.MoveNext
				loop
				oCompanyL3Rs.Close
			end if
			oRs.MoveNext
		loop
	
	elseif p_iGroupID = 5 or p_iGroupID = 6 then
		'Get the CompanyL3s this user has access to
		sUserCompanyL3AccessList = ""
		
		sSQL = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL3AccessList) <> "" then
				sUserCompanyL3AccessList = sUserCompanyL3AccessList & ", "
			end if
			
			sUserCompanyL3AccessList = sUserCompanyL3AccessList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
		oRs.Close
	
		'User has access if the branch is assigned to one of his CL3s.
		sSQL = "SELECT BranchId, CompanyL3Id FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 " & _
				"AND NOT CompanyL3Id IS NULL"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL3AccessList & ",", ", " & oRs("CompanyL3Id") & ",") then
				if trim(GetUserBranchIdList) <> "" then
					GetUserBranchIdList = GetUserBranchIdList & ", "
				end if					
				GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			end if
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 10 or p_iGroupID = 11 then
		'Get the Branches this user has access to
		sUserBranchAccessList = ""
		
		sSQL = "SELECT BranchId FROM UsersBranches WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserBranchAccessList) <> "" then
				sUserBranchAccessList = sUserBranchAccessList & ", "
			end if
			
			sUserBranchAccessList = sUserBranchAccessList & oRs("BranchId")
			oRs.MoveNext
		loop
		oRs.Close	
	
		'User has access if the branch is one of his branches.  
		sSQL = "SELECT BranchId FROM CompanyBranches WHERE CompanyId = '" & p_iCompanyID & "' AND Deleted = 0 "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		do while not oRs.EOF
			if instr(1, ", " & sUserBranchAccessList & ",", ", " & oRs("BranchId") & ",") then
				if trim(GetUserBranchIdList) <> "" then
					GetUserBranchIdList = GetUserBranchIdList & ", "
				end if						
				GetUserBranchIdList = GetUserBranchIdList & oRs("BranchId")
			end if
			oRs.MoveNext
		loop
	end if
		
	set oConn = nothing
	set oRs = nothing			
end function
	

'Get the list of CompanyL2Ids to which the user has view or admin access.  
function GetUserCompanyL2IdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim sUserCompanyL2AccessList 'as string
	
	'Default value
	GetUserCompanyL2IdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every company L2 assigned to the
		'company.
		sSQL = "SELECT CompanyL2Id FROM CompaniesL2 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(GetUserCompanyL2IdList) <> "" then
				GetUserCompanyL2IdList = GetUserCompanyL2IdList & ", "
			end if		
			GetUserCompanyL2IdList = GetUserCompanyL2IdList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close
	
		'CompanyL2 admins/viewers have access if the CompanyL2 is in their list of assigned CL2s.
		sSQL = "SELECT CompanyL2Id FROM CompaniesL2 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then
				if trim(GetUserCompanyL2IdList) <> "" then
					GetUserCompanyL2IdList = GetUserCompanyL2IdList & ", "
				end if
				GetUserCompanyL2IdList = GetUserCompanyL2IdList & oRs("CompanyL2Id")
			end if
			oRs.MoveNext
		loop
	end if
	
	set oConn = nothing
	set oRs = nothing	
end function

'Get the list of CompanyL3Ids to which the user has view or admin access.  
function GetUserCompanyL3IdList(p_iUserID, p_iCompanyID, p_iGroupID)
	dim oConn
	dim sSQL 'as string
	dim oRs 'as object
	dim sUserCompanyL2AccessList, sUserCompanyL3AccessList 'as string
	
	'Default value
	GetUserCompanyL3IdList = ""
	
	set oConn = CreateObject("ADODB.Connection")
	oConn.ConnectionString = sConnectionString
	oConn.Open
		
	set oRs = CreateObject("ADODB.Recordset")
	oRs.CursorLocation = 3
	oRs.CursorType = 3
		
	if p_iGroupID = 2 then
		'Company admins have access to every company L3 assigned to the
		'company.
		sSQL = "SELECT CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(GetUserCompanyL3IdList) <> "" then
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
			end if		
			GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
			
	elseif p_iGroupID = 3 or p_iGroupID = 4 then
		'Get the CompanyL2s this user has access to
		sUserCompanyL2AccessList = ""
		
		sSQL = "SELECT CompanyL2Id FROM UsersCompaniesL2 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL2AccessList) <> "" then
				sUserCompanyL2AccessList = sUserCompanyL2AccessList & ", "
			end if
			
			sUserCompanyL2AccessList = sUserCompanyL2AccessList & oRs("CompanyL2Id")
			oRs.MoveNext
		loop
		oRs.Close	
	
		'CompanyL2 admins/viewers have access if the company L3 is assigned to one
		'of their company L2s.
		sSQL = "SELECT CompanyL2Id, CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' " & _
				"AND NOT CompanyL2Id IS NULL"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if instr(1, ", " & sUserCompanyL2AccessList & ",", ", " & oRs("CompanyL2Id") & ",") then			
				'User has access to the companyL3 is assigned to one of his own CL2s.
				if trim(GetUserCompanyL3IdList) <> "" then
					GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
				end if						
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			end if
			oRs.MoveNext
		loop	
	elseif p_iGroupID = 5 or p_iGroupID = 6 then
		'Get the CompanyL3s this user has access to
		sUserCompanyL3AccessList = ""
		
		sSQL = "SELECT CompanyL3Id FROM UsersCompaniesL3 WHERE UserId = '" & p_iUserID & "'"
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing	
		
		do while not oRs.EOF
			if trim(sUserCompanyL3AccessList) <> "" then
				sUserCompanyL3AccessList = sUserCompanyL3AccessList & ", "
			end if
			
			sUserCompanyL3AccessList = sUserCompanyL3AccessList & oRs("CompanyL3Id")
			oRs.MoveNext
		loop
		oRs.Close
		
		'CompanyL3 admins/viewers have access if the CompanyL3 is in their list of assigned CL3s.
		sSQL = "SELECT CompanyL3Id FROM CompaniesL3 WHERE CompanyId = '" & p_iCompanyID & "' "
		oRs.Open sSQL, oConn
		set oRs.ActiveConnection = nothing
		
		do while not oRs.EOF 
			if instr(1, ", " & sUserCompanyL3AccessList & ",", ", " & oRs("CompanyL3Id") & ",") then
				if trim(GetUserCompanyL3IdList) <> "" then
					GetUserCompanyL3IdList = GetUserCompanyL3IdList & ", "
				end if				
				GetUserCompanyL3IdList = GetUserCompanyL3IdList & oRs("CompanyL3Id")
			end if
			oRs.MoveNext
		loop
	end if
	
	set oConn = nothing
	set oRs = nothing	
end function

Sub SendEmail(p_iUserID, p_sName, p_sEmail, p_iCompanyID, p_sCompanyAdminEmails, p_sFingerprintDeadlineDate, p_sDayCount)
	dim strMailServerAddress	'The SMTP server that Jmail will use
	dim objJmail
	dim sAdminEmail
	dim sSQL
	dim objConn
	dim sRecipients
	
	'Create an instance of the Jmail object and set it's initial values
	Set objJMail = CreateObject("JMail.Message")
	objJMail.ContentType = "text/html"

	'set the Jmail values
	objJmail.From = "alerts@compliancekeeper.com"
	objJmail.FromName = "ComplianceKeeper.com Alerts"
	sSubject = p_sDayCount & " Day Fingerprint Deadline Alert: " & p_sName
	objJmail.Subject = sSubject
	
	sBody = "The Fingerprint Deadline for the Officer listed below will occur in " & p_sDayCount & " Days.  If you need to edit this date, you should go to the Edit Officer page to change the date.<br><br>" & p_sName & " - " & p_sFingerprintDeadlineDate & "<br>" &_
		            "<a href=""" & sURL & "/officers/modofficer.asp?id=" & p_iUserID & "&referrer=FINGERPRINTALERTEMAIL"">Click here to Edit the date</a>"
	
	objJmail.Body = sBody
		
	'Add the user to the alert email			
	if trim(p_sEmail) <> "" then
		objJmail.AddRecipient p_sEmail		
		sRecipients = p_sEmail
	end if					
					
	'add each admin to the alert email
	if trim(p_sCompanyAdminEmails) <> "" then
		for each sAdminEmail in split(p_sCompanyAdminEmails, ", ")
			objJmail.AddRecipient sAdminEmail 
		next
		
		if trim(sRecipients) <> "" then
			sRecipients = sRecipients & ", "
		end if
		sRecipients = sRecipients & p_sCompanyAdminEmails
	end if
	
	'mail it
	if objJmail.Send(sEmailServer) then
		Set objConn = CreateObject("ADODB.Connection")
		objConn.ConnectionString = sConnectionString
		objConn.Open		
		
		'Log the email
		sSQL = "INSERT INTO EmailLog (" & _
			"SenderId, " & _
			"CompanyID, " & _
			"RecipientList, " & _
			"Subject, " & _
			"BodyText, " & _
			"SendDate, " & _
			"SendLog " & _
			") VALUES (" & _
			"'0', " & _
			"'" & p_iCompanyID & "', " & _
			"'" & replace(sRecipients,"'","''") & "', " & _
			"'" & replace(sSubject,"'","''") & "', " & _
			"'" & replace(sBody,"'","''") & "', " & _
			"'" & Now() & "', " & _
			"'" & replace(objJmail.Log,"'","''") & "' " & _
			")"
		objConn.Execute sSQL	
		
		set objConn = nothing
	end if
	
	objJmail.Close		
	
	set objJmail = nothing	
End sub

' ----------------------------------------------------------------------------------------------------------
' Name : ValueInDelString
' Purpose : determines if a value is contained in a string of comma delimited elements
' Inputs : p_sValue - the value to check for
'		 : p_sElementString - string of elements delimited by commas
' Outputs : true if the value was in the string, false if not
' ----------------------------------------------------------------------------------------------------------
function ValueInDelString(p_sValue,p_sElementString)
	dim sCurrentValue
	dim aElementArray
	
	aElementArray = split(p_sElementString, ", ")
	
	for each sCurrentValue in aElementArray
		if (trim(sCurrentValue) = trim(p_sValue)) then
			ValueInDelString = true
			exit function
		end if
	next
	
	ValueInDelString = false
end function

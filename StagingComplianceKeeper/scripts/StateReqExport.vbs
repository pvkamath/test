'---------------------------------------------------------------------------------------------------
'				Function 5000 - NightlyQuickenReport()
'---------------------------------------------------------------------------------------------------
sub NightlyQuickenReport(p_iCompanyID, p_sEmail)

	
	
	dim sSQL, sSQL2, sField 'as string
	dim reqRS 'as object
	dim p_objConn 'as object
	
	
	set reqRS = server.createobject("ADODB.Recordset")
	set p_objConn = server.CreateObject("ADODB.Connection")
    	p_objConn.Open application("sDataSourceName")

	

'Set the report Parameters
	dim iCompDateRange, iRegDateRange 'as integer
	dim iUserID 'as integer
	dim sStudent, sCourse 'as string



sSQL =	"SELECT StateAbbrev as StName, RequirementsText as ReqTxt, LastEdit From StateRequirements"

	reqRS.Open sSQL, p_objConn, 3, 2
	reqRS.MoveFirst
' -- Output the query as an XML/txt file
	response.ContentType = "text/xml"
	response.write("<?xml version='1.0' encoding='ISO-8859-1'?>")
	response.write("<CK_State_Reqs>")
	
' -- Now output the contents of the Recordset
	
	Do While Not (reqRS.EOF) AND Not (reqRS.BOF) 
		

	sState = "<State Name = "
		
	sState = sState & chr(34) & reqRS.Fields("StName").value & chr(34)
	sState = sState & ">"
	response.write(sState)


		' -- output the contents
		sReq = "<Requirement Text> "
		sReq = sReq & reqRS.Fields("StName").value & "</Requirement Text>"
		response.write(sReq)
		

		sEdit = "<Edit Date> "
		sEdit = sEdit & reqRS.Fields("LastEdit").value & "</Edit Date>"
		response.write(sEdit)
			
																								
						sCourse = sCourse & " TimeLogged=" & chr(34) & p_oUser.GetTotalCourseTime(courseRS.Fields(i)) & chr(34) 
						sCourse = sCourse & " TestAttempts=" & chr(34) & p_oUser.GetNoOfTestAttempts(courseRS.Fields(i)) & chr(34) 
						sCourse = sCourse & " CompletionPercentage=" & chr(34)
						
						'If this is a Cross Cert Course, display the Core Course or Supplemental courses Completion % based upon Cross Cert Type
						if courseRS("CrossCertified") then
							if cint(courseRS("CrossCertType")) = 1 then 'Certificate Cross Cert Type
								sCourse = sCourse & p_oReport.GetCourseCompletionPercentage(courseRS("CrossCertifiedCoreCourse"))
							elseif not isnull(courseRS("SupplementCourseID")) then 'Supplement Cross Cert
								sCourse = sCourse & p_oReport.GetCourseCompletionPercentage(courseRS("SupplementCourseID"))
							else 'This is the Supplement course so get this course's completion %
								sCourse = sCourse & p_oReport.GetCourseCompletionPercentage(courseRS.Fields(i))
							end if
						else
							sCourse = sCourse & p_oReport.GetCourseCompletionPercentage(courseRS.Fields(i))
						end if
						
						sCourse = sCourse & chr(34)



					Else
						sCourse = sCourse & " TimeLogged=" & chr(34) & "0" & chr(34) 

						If courseRS.Fields("Completed").value = 1 then						
							sCourse = sCourse & " TestAttempts=" & chr(34) & "1" & chr(34) & " CompletionPercentage=" & chr(34) & "1" & chr(34)
						Else
							sCourse = sCourse & " TestAttempts=" & chr(34) & "0" & chr(34) & " CompletionPercentage=" & chr(34) & "0" & chr(34) 
						End if

					End IF		
				
			Else
				
				sCourse = sCourse & " " & courseRS.Fields(i).Name & "=" & chr(34) & courseRS.Fields(i) & chr(34) 
				
				
			End IF
							
			Next
		
			sCourse = sCourse & "></Course_Record>"
			Response.write(sCourse)
			
			' -- move to the next course record
			courseRS.MoveNext
		Loop
			
			
			courseRS.Close
			set courseRS = Nothing
			response.write("</Student_Record>")
		End IF
	
		
		studentRS.MoveNext	
	
	Loop
	
	response.write("</Student_Course_Report>")

ELSE
	
	response.write(" The User Could not be found.")
	'response.redirect("error.asp?message=The User Could not be found.")
End IF
	
	
	set studentRS = Nothing
	set p_objConn = Nothing
	set p_oUser = Nothing
	set p_oReport = Nothing
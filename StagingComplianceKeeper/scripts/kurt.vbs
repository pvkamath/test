'Send Calendar Event Alerts to Company Admins 1 month prior to the event

dim objConn, rs 'as object
dim sEventTitle 'as string
dim iEventID 'as integer
dim iCompanyID 'as integer
dim s30DayAlertDate 'as string
dim sCompanyAdminEmails 'as string
dim sStartDate 'as string

'sConnectionString = "Provider=SQLOLEDB; Data Source=devdb1; Initial Catalog=compliancekeepercorp; User Id=sa; Password=x1gr0up"
'sEmailServer = "smtp.xigroup.com"
'sConnectionString = "Provider=SQLOLEDB; Data Source=STAGEWEB; Initial Catalog=compliancekeepercorp; User Id=sa; Password=0verl0ad"
'sEmailServer = "smtp.xigroup.com"
sConnectionString = "Provider=SQLOLEDB; Data Source=192.168.100.59; Initial Catalog=compliancekeepercorp; User Id=sa; Password=Educat10n"
sEmailServer = "192.168.100.58:25"

sUrl = "https://www.compliancekeeper.com"
'sUrl = "http://dev.compliancekeepercorp"

Set objConn = CreateObject("ADODB.Connection")
Set rs = CreateObject("ADODB.RecordSet")
objConn.ConnectionString = sConnectionString
objConn.Open		

'Compute the event date to check for (1 month from current date)
s30DayAlertDate = "3/15/2010"

sSQL = "SELECT EventID, Event, StartDate, CompanyID FROM afxEvents WHERE CompanyID = '-136' AND UseTracker = 1 AND StartDate >= '" & s30DayAlertDate & " 00:00' AND StartDate <= '" & s30DayAlertDate & " 23:59'"
wscript.echo(sSQL)
rs.Open sSQL, objConn, 3, 3

'wscript.echo "Start"
do while not rs.eof
	iEventID = rs("EventID")
	sEventTitle = rs("Event")
	sStartDate = rs("StartDate")
	iCompanyID = rs("CompanyID")

	'Get the Company Admin Email addresses
	sCompanyAdminEmails = BuildCompanyAdminEmailList(iCompanyID)
	
	'Send Email
	if sCompanyAdminEmails <> "" then
		Call SendAlertMessage(iEventID, sEventTitle, sStartDate, sCompanyAdminEmails)
	end if
	
	rs.MoveNext
loop
'wscript.echo "End"

set objConn = nothing
set rs = nothing

'Creates a Comma Delimited String of company admin email addresses
Function BuildCompanyAdminEmailList(p_iCompanyID)
	dim objConn, rs 'as object
	dim sSQL 'as string
	dim sEmailList 'as string
	
	Set objConn = CreateObject("ADODB.Connection")
	Set rs = CreateObject("ADODB.RecordSet")
	objConn.ConnectionString = sConnectionString
	objConn.Open			
	
	sSQL = "SELECT Email FROM Users WHERE CompanyID = " & p_iCompanyID & " AND UserStatus = 1 AND CConAlertEmails = 1"
	rs.Open sSQL, objConn, 3, 3
	
	sEmailList = ""
	do while not rs.eof
		if trim(sEmailList) = "" then
			sEmailList = rs("Email")
		else
			sEmailList = sEmailList & ", " & rs("Email")
		end if
			
		rs.MoveNext
	loop	
	
	BuildCompanyAdminEmailList = sEmailList
	
	set objConn = nothing
	set rs = nothing
End Function

'Creates and Sends The Alert Emails to the Company Admins using Jmail
Sub SendAlertMessage(p_iEventID, p_sEventTitle, p_sStartDate, p_sCompanyAdminEmails)
	dim strMailServerAddress	'The SMTP server that Jmail will use
	dim objJmail

	'Create an instance of the Jmail object and set it's initial values
	Set objJMail = CreateObject("JMail.Message")
	objJMail.ContentType = "text/html"

	'set the Jmail values
	objJmail.From = "alerts@compliancekeeper.com"
	objJmail.FromName = "ComplianceKeeper.com Alerts"
	objJmail.Subject = p_sEventTitle
	
	objJmail.Body = "The event listed below will occur in 30 Days.  If you need to reschedule this event, you should go to the Edit events page to change the start date.<br><br>" & p_sEventTitle & " - " & p_sStartDate & "<br>" &_
		            "<a href=""" & sURL & "/calendar/EditEvent.asp?eventid=" & p_iEventID & "&referrer=EVENTALERTEMAIL"">Click here to Edit the Event</a>"
					
	for each sEmail in split(p_sCompanyAdminEmails, ", ")
		objJmail.AddRecipient sEmail
	next
	objJmail.AddRecipient "kchambers@xigroup.com"

	'mail it
	objJmail.Send(sEmailServer)
	objJmail.Close		
	
	set objJmail = nothing	
End Sub
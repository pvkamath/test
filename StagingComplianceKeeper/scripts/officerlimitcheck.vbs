'Checks number of listed officers for each company against the allowed amounts 
'for its respective pricing plan, and emails an alert if appropriate.

dim iCompanyId 'Current company ID
dim sConnectionString 'DB connection string
dim oConn 'DB connection object
dim oRs 'Company search recordset object
dim oCompanyRs 'Company info recordset object
dim oPlanRs 'Pricing plan info recordset object
dim sSql 'SQL statement
dim oOfficerRs 'Officer search recordset object
dim iOfficerCount 'Number of active officers for a company
dim iOfficerLimit 'Max number of officer for a company's plan


set oConn = CreateObject("ADODB.Connection")
set oRs = CreateObject("ADODB.Recordset")
set oCompanyRs = CreateObject("ADODB.Recordset")
set oPlanRs = CreateObject("ADODB.Recordset")
set oOfficerRs = CreateObject("ADODB.Recordset")

sConnectionString = "driver={SQL server};server=devdb;uid=sa;pwd=l'user;database=compliancekeepercorp"
oConn.ConnectionString = sConnectionString
oConn.Open


'Get a list of all active companies in the system
sSql = "SELECT Cs.CompanyId, Cs.Name FROM Companies AS Cs WHERE NOT Active = '0'"
oRs.Open sSql, oConn
do while not oRs.EOF 

	
	'Retrieve the record for the current company.
	iCompanyId = oRs("CompanyId")
	sSql = "SELECT * FROM Companies " & _
		"WHERE CompanyId = '" & iCompanyId & "'"
	oCompanyRs.Open sSql, oConn

	if not oCompanyRs.EOF then

		'Get the plan information
		sSql = "SELECT * FROM PricingPlans " & _
			"WHERE PricingPlanId = '" & oCompanyRs("PricingPlanId") & "'"
		oPlanRs.Open sSql, oConn
		
		if not oPlanRs.EOF then
			
			iOfficerLimit = oPlanRs("MaxOfficers")
			
			if iOfficerLimit <> "" then
			
				'Search the current number of loan officers for this company
				sSql = "SELECT VAs.UserId, VAs.LastName, VAs.FirstName FROM Associates AS VAs " & _
					"WHERE VAs.CompanyId = '" & iCompanyId & "' AND VAs.UserStatus = '1'"
				oOfficerRs.CursorLocation = 3
				oOfficerRs.CursorType = 3
				oOfficerRs.Open sSql, oConn	
				
				iOfficerCount = oOfficerRs.RecordCount
				
				oOfficerRs.Close

				'Are we over the plan limit?
				if iOfficerCount > iOfficerLimit then
			
					'Send an alert email.
					HandleOverLimit iCompanyId, iOfficerLimit, iOfficerCount
			
				end if			
				
			end if
					
		end if	
		
		oPlanRs.Close
	
	end if
	
	oCompanyRs.Close
	oRs.MoveNext
	
loop

oConn.Close
set oConn = nothing
set oRs = nothing
set oOfficerRs = nothing
set oPlanRs = nothing




'Respond to an instance of a company exceeding its plan limit.  We'll send an
'email to the CK administrators with the details.
sub HandleOverLimit(p_iCompanyId, p_iOfficerLimit, p_iOfficerCount)

	dim oJmail 'JMail object
	dim sEmailText 'Body text for the email
	dim sSubject 'Subject line 
	dim sSender 'Sender email address
	dim sRecipient 'Recipient email address
	dim sMailServerAddress 'Address of mail server
	
	sSender = "msilvia@xigroup.com"
	sRecipient = "msilvia@xigroup.com"
	
	sSubject = "Officer Limit Alert: " & oCompanyRs("Name")
	
	sEmailText = "This is a ComplianceKeeper Officer Limit Alert." & vbCrLf & vbCrLf 
	sEmailText = sEmailText & oCompanyRs("Name") & " has exceeded its plan limit of " & p_iOfficerLimit & " officers.  " 
	sEmailText = sEmailText & "It currently has " & p_iOfficerCount & " officer records in the system."
	
	sMailServerAddress = "192.168.1.16:25"
	
	'Create an instance of the jmail object and set its initial values
	set oJmail = CreateObject("Jmail.Message")
	oJmail.ContentType = "text/plain"
	oJmail.Silent = false 'true
	oJmail.Logging = false

	'Set the Jmail values with the information collected
	oJmail.Body = sEmailText
	oJmail.AddRecipient(sRecipient)
	oJmail.From = sSender
	oJmail.FromName = "ComplianceKeeper Alerts"
	oJmail.Subject = sSubject
	
	'Mail it
	oJmail.Send(sMailServerAddress)
	
	'Response.Write("<p>" & sSubject)
	'Response.Write("<br>" & sEmailText)
	'Response.Write("<br>" & oJmail.From)
	
	oJmail.Close
	set oJmail = nothing	
	
end sub

<%
'Sends license and course expiration alert emails

dim iCompanyId 'Current company ID
dim sCompanyName 'Current company name
dim sConnectionString 'DB connection string
dim oConn 'DB connection object
dim oRs 'Company search recordset object
dim oCompanyRs 'Company info recordset object
dim oSettingsRs 'Company settings recordset object
dim oAlertsRs 'Found liceneses recordset object
'dim oPlanRs 'Pricing plan info recordset object

dim asAdmins()
dim asAdminsEmails()

redim asAdmins(0)
redim asAdminsEmails(0)

dim sSql 'SQL statement
dim oOfficerRs 'Officer search recordset object
dim iOfficerCount 'Number of active officers for a company
dim iOfficerLimit 'Max number of officer for a company's plan

dim oJmail
dim sRecipientList
dim sUrl

set oConn = CreateObject("ADODB.Connection")
set oRs = CreateObject("ADODB.Recordset")
set oCompanyRs = CreateObject("ADODB.Recordset")
set oOfficerRs = CreateObject("ADODB.Recordset")
set oSettingsRs = CreateObject("ADODB.Recordset")
oSettingsRs.CursorType = 3
set oAlertsRs = CreateObject("ADODB.Recordset")

sUrl = "https://www.compliancekeeper.com"
'sUrl = "http://dev.compliancekeepercorp"

'sConnectionString = "driver={SQL server};server=devdb1;uid=sa;pwd=x1gr0up;database=compliancekeepercorp"
'sEmailServer = "smtp.xigroup.com"
'sConnectionString = "driver={SQL server};server=cmdb1;uid=sa;pwd=0verl0ad;database=compliancekeepercorp"
'sEmailServer = "smtp.xigroup.com"
'sConnectionString = "Provider=SQLOLEDB; Data Source=192.168.100.59; Initial Catalog=compliancekeepercorp; User Id=sa; Password=Educat10n"
'sConnectionString = "Provider=SQLOLEDB; Data Source=starbase1.starfleet.local; Initial Catalog=compliancekeeper; User Id=trainingpro; Password=Educat10n"
sConnectionString = application("sDataSourceName")
sEmailServer = application("MAILSERVER")

sErrorFile = "C:\ErrorLogs\SendAlertMailsNew.txt"
WriteToLogFile("Started running " & now())

oConn.ConnectionString = sConnectionString
oConn.Open


'Get a list of all active companies in the system
sSql = "SELECT Cs.CompanyId, Cs.Name FROM Companies AS Cs WHERE NOT Active = '0'"
oRs.Open sSql, oConn
do while not oRs.EOF 

	
	'Retrieve the record for the current company.
	iCompanyId = oRs("CompanyId")
	sSql = "SELECT * FROM Companies " & _
		"WHERE CompanyId = '" & iCompanyId & "'"
	oCompanyRs.Open sSql, oConn

	if not oCompanyRs.EOF then
	
		'Set the company name
		sCompanyName = oCompanyRs("Name")
	
		'See which alerts are activated
		sSql = "SELECT * FROM CompanySettings " & _	
			"WHERE CompanyId = '" & iCompanyId & "'"
		oSettingsRs.Open sSql, oConn

		if not oSettingsRs.EOF then

			'Set which Expiration Date to use License Expiration Date or Renewal Application Deadline field
			if oSettingsRs("UseRenewalAppDeadlineLicExpEmails") then
				sLicenseExpDate = "LicenseAppDeadline"
			else
				sLicenseExpDate = "LicenseExpDate"	
			end if		
		
			'Send 30 day license expiration email
			if oSettingsRs("Send30DayEmail") then 										
				sSql = "SELECT DISTINCT Ls.LicenseId, " & _
					"Ls." & sLicenseExpDate & ", " & _
					"Ls.LicenseStateId, " & _
					"Ls.LicenseNum, " & _
					"Ls.OwnerTypeId, " & _
					"Ls.OwnerId, " & _
					"ACs.LastName, " & _
					"Bs.Name AS BranchName " & _
					"FROM Licenses AS Ls " & _
					"LEFT JOIN Associates AS ACs ON (ACs.UserId = Ls.OwnerId AND Ls.OwnerTypeId = 1) " & _
					"LEFT JOIN CompanyBranches AS Bs ON (Bs.BranchId = Ls.OwnerId AND Ls.OwnerTypeId = 2) " & _
					"LEFT JOIN CompaniesL2 AS L2s ON (L2s.CompanyL2Id = Ls.OwnerId AND Ls.OwnerTypeId = 4) " & _
					"LEFT JOIN CompaniesL3 AS L3s ON (L3s.CompanyL3Id = Ls.OwnerId AND Ls.OwnerTypeId = 5) " & _
					"WHERE (NOT (Ls.Deleted = '1')) " & _
					"AND Ls.OwnerCompanyId = '" & iCompanyId & "' " & _
					"AND (ACs.Inactive = '0' OR Bs.Inactive = '0' OR L2s.Inactive = '0' OR L3s.Inactive = '0' OR Ls.OwnerTypeId = '3') " & _
					"AND (Ls.LicenseStatusId = '1' OR Ls.LicenseStatusId = '2' OR Ls.LicenseStatusId = '4' OR Ls.LicenseStatusId = '5' OR Ls.LicenseStatusId = '10' OR Ls.LicenseStatusId = '11' OR Ls.LicenseStatusId = '12' OR Ls.LicenseStatusId = '18') " & _			
					"AND (Ls." & sLicenseExpDate & " >= '" & formatdatetime(date() + 1, 2) & "' AND Ls." & sLicenseExpDate & " <= '" & formatdatetime(date() + 31, 2) & "') "
					
				'If send daily reminder is not set, only send the 30 Day email reminder if not previously sent.
				if not oSettingsRs("Send30DayEmailDaily") then 
					sSql = sSql & "AND (Ls.Triggered30 = '0') "
				end if
				
				sSql = sSql & "ORDER BY Ls.OwnerTypeId DESC, ACs.LastName, Bs.BranchName, Ls." & sLicenseExpDate & " ASC "
					
				'Response.Write("<br>" & sSql & "<p>")
				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF
					SendLicenseAlertsEmail 1, oAlertsRs("LicenseId")
					MarkLicenseTriggered 1, oAlertsRs("LicenseId")
					oAlertsRs.MoveNext
				loop
				oAlertsRs.Close
			
			end if
			
			'Send 60 day license expiration email
			if oSettingsRs("Send60DayEmail") then						
				sSql = "SELECT DISTINCT Ls.LicenseId, " & _
					"Ls." & sLicenseExpDate & ", " & _
					"Ls.LicenseStateId, " & _
					"Ls.LicenseNum, " & _
					"Ls.OwnerTypeId, " & _
					"Ls.OwnerId, " & _
					"ACs.LastName, " & _
					"Bs.Name AS BranchName " & _
					"FROM Licenses AS Ls " & _
					"LEFT JOIN Associates AS ACs ON (ACs.UserId = Ls.OwnerId AND Ls.OwnerTypeId = 1) " & _
					"LEFT JOIN CompanyBranches AS Bs ON (Bs.BranchId = Ls.OwnerId AND Ls.OwnerTypeId = 2) " & _
					"LEFT JOIN CompaniesL2 AS L2s ON (L2s.CompanyL2Id = Ls.OwnerId AND Ls.OwnerTypeId = 4) " & _
					"LEFT JOIN CompaniesL3 AS L3s ON (L3s.CompanyL3Id = Ls.OwnerId AND Ls.OwnerTypeId = 5) " & _
					"WHERE (NOT (Ls.Deleted = '1')) " & _
					"AND Ls.OwnerCompanyId = '" & iCompanyId & "' " & _
					"AND (ACs.Inactive = '0' OR Bs.Inactive = '0' OR L2s.Inactive = '0' OR L3s.Inactive = '0' OR Ls.OwnerTypeId = '3') " & _
					"AND (Ls.LicenseStatusId = '1' OR Ls.LicenseStatusId = '2' OR Ls.LicenseStatusId = '4' OR Ls.LicenseStatusId = '5' OR Ls.LicenseStatusId = '10' OR Ls.LicenseStatusId = '11' OR Ls.LicenseStatusId = '12' OR Ls.LicenseStatusId = '18') " & _			
					"AND (Ls." & sLicenseExpDate & " >= '" & formatdatetime(date() + 31, 2) & "' AND Ls." & sLicenseExpDate & " <= '" & formatdatetime(date() + 61, 2) & "') " & _
					"AND (Ls.Triggered60 = '0') " & _
					"ORDER BY Ls.OwnerTypeId DESC, ACs.LastName, Bs.BranchName, Ls." & sLicenseExpDate & " ASC "
			
				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF
					SendLicenseAlertsEmail 2, oAlertsRs("LicenseId")
					MarkLicenseTriggered 2, oAlertsRs("LicenseId")
					oAlertsRs.MoveNext
				loop
				oAlertsRs.Close
				
			end if
			
				
			'Send 90 day license expiration email
			if oSettingsRs("Send90DayEmail") then
				sSql = "SELECT DISTINCT Ls.LicenseId, " & _
					"Ls." & sLicenseExpDate & ", " & _
					"Ls.LicenseStateId, " & _
					"Ls.LicenseNum, " & _
					"Ls.OwnerTypeId, " & _
					"Ls.OwnerId, " & _
					"ACs.LastName, " & _
					"Bs.Name AS BranchName " & _
					"FROM Licenses AS Ls " & _
					"LEFT JOIN Associates AS ACs ON (ACs.UserId = Ls.OwnerId AND Ls.OwnerTypeId = 1) " & _
					"LEFT JOIN CompanyBranches AS Bs ON (Bs.BranchId = Ls.OwnerId AND Ls.OwnerTypeId = 2) " & _
					"LEFT JOIN CompaniesL2 AS L2s ON (L2s.CompanyL2Id = Ls.OwnerId AND Ls.OwnerTypeId = 4) " & _
					"LEFT JOIN CompaniesL3 AS L3s ON (L3s.CompanyL3Id = Ls.OwnerId AND Ls.OwnerTypeId = 5) " & _
					"WHERE (NOT (Ls.Deleted = '1')) " & _
					"AND Ls.OwnerCompanyId = '" & iCompanyId & "' " & _
					"AND (ACs.Inactive = '0' OR Bs.Inactive = '0' OR L2s.Inactive = '0' OR L3s.Inactive = '0' OR Ls.OwnerTypeId = '3') " & _
					"AND (Ls.LicenseStatusId = '1' OR Ls.LicenseStatusId = '2' OR Ls.LicenseStatusId = '4' OR Ls.LicenseStatusId = '5' OR Ls.LicenseStatusId = '10' OR Ls.LicenseStatusId = '11' OR Ls.LicenseStatusId = '12' OR Ls.LicenseStatusId = '18') " & _			
					"AND (Ls." & sLicenseExpDate & " >= '" & formatdatetime(date() + 61, 2) & "' AND Ls." & sLicenseExpDate & " <= '" & formatdatetime(date() + 91, 2) & "') " & _
					"AND (Ls.Triggered90 = '0') " & _
					"ORDER BY Ls.OwnerTypeId DESC, ACs.LastName, Bs.BranchName, Ls." & sLicenseExpDate & " ASC "

				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF
					SendLicenseAlertsEmail 3, oAlertsRs("LicenseId")
					MarkLicenseTriggered 3, oAlertsRs("LicenseId")
					oAlertsRs.MoveNext
				loop
				oAlertsRs.Close
				
			end if
			
			
			if oSettingsRs("Send30DayCourseEmail") then
			
				'Send 30 day course expiration email
				sSql = "SELECT DISTINCT ACs.Id, ACs.UserId, ACs.CourseId " & _
					"FROM vAssociatesCoursesCKUserId AS ACs " & _
					"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
					"LEFT JOIN vCourses AS Cs ON (Cs.CourseId = ACs.CourseId) " & _
					"LEFT JOIN AssociatesCoursesTriggers AS ACTs ON (ACTs.Id = ACs.Id) " & _
					"WHERE ((ACs.CertificateExpirationDate >= '" & formatdatetime(date() + 1, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate >= '" & formatdatetime(date() + 1, 2) & "')) " & _
					"AND ((ACs.CertificateExpirationDate < '" & formatdatetime(date() + 31, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate < '" & formatdatetime(date() + 31, 2) & "')) " & _
					"AND NOT Us.Inactive = '1' " & _
					"AND NOT Us.UserStatus = '3' " & _
					"AND NOT ACs.Deleted = '1' " & _
					"AND NOT ACs.UserId IS NULL "
					
				'If send daily reminder is not set, only send the 30 Day course email reminder if not previously sent.
				if not oSettingsRs("Send30DayCourseEmailDaily") then 
					sSql = sSql & "AND (ACTs.Triggered30 = '0' OR ACTs.Triggered30 IS NULL) "
				end if
				
				sSql = sSql & "AND (ACs.CompanyId = '" & iCompanyId & "' " 
				if oCompanyRs("TpLinkCompanyId") <> "" then
					sSql = sSql & "OR ACs.CompanyId = '" & _
						oCompanyRs("TpLinkCompanyId") & "') "
				else
					sSql = sSql & ") "
				end if

				sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) OR (Us.BeginningEducationDate IS NULL) OR (ACs.PurchaseDate IS NULL)) "

				'Response.Write("<p>" & sSql & "</p>")
				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF 
					SendCourseAlertsEmail 1, oAlertsRs("UserId"), oAlertsRs("CourseId"), oAlertsRs("Id")
					MarkCourseTriggered 1, oAlertsRs("Id")
					oAlertsRs.MoveNext
				loop
				oAlertsRs.Close
				
			end if

			
			if oSettingsRs("Send60DayCourseEmail") then
			
				'Send 60 day course expiration email
				sSql = "SELECT DISTINCT ACs.Id, ACs.UserId, ACs.CourseId " & _
					"FROM vAssociatesCoursesCKUserId AS ACs " & _
					"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
					"LEFT JOIN vCourses AS Cs ON (Cs.CourseId = ACs.CourseId) " & _
					"LEFT JOIN AssociatesCoursesTriggers AS ACTs ON (ACTs.Id = ACs.Id) " & _
					"WHERE ((ACs.CertificateExpirationDate >= '" & formatdatetime(date() + 31, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate >= '" & formatdatetime(date() + 31, 2) & "')) " & _
					"AND ((ACs.CertificateExpirationDate < '" & formatdatetime(date() + 61, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate < '" & formatdatetime(date() + 61, 2) & "')) " & _
					"AND NOT Us.Inactive = '1' " & _
					"AND NOT Us.UserStatus = '3' " & _
					"AND NOT ACs.Deleted = '1' " & _
					"AND NOT ACs.UserId IS NULL " & _
					"AND (ACTs.Triggered60 = '0' OR ACTs.Triggered60 IS NULL) " & _
					"AND (ACs.CompanyId = '" & iCompanyId & "' " 
				if oCompanyRs("TpLinkCompanyId") <> "" then
					sSql = sSql & "OR ACs.CompanyId = '" & _
						oCompanyRs("TpLinkCompanyId") & "') "
				else
					sSql = sSql & ") "
				end if

				sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) OR (Us.BeginningEducationDate IS NULL) OR (ACs.PurchaseDate IS NULL)) "

				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF 
					SendCourseAlertsEmail 2, oAlertsRs("UserId"), oAlertsRs("CourseId"), oAlertsRs("Id")
					MarkCourseTriggered 2, oAlertsRs("Id")
					oAlertsRs.MoveNext
				loop		
				oAlertsRs.Close		
				
			end if
			
			
			if oSettingsRs("Send90DayCourseEmail") then				
				
				'Send 90 day course expiration email
				sSql = "SELECT DISTINCT ACs.Id, ACs.UserId, ACs.CourseId " & _
					"FROM vAssociatesCoursesCKUserId AS ACs " & _
					"LEFT JOIN Associates AS Us ON (Us.UserId = ACs.UserId) " & _
					"LEFT JOIN vCourses AS Cs ON (Cs.CourseId = ACs.CourseId) " & _
					"LEFT JOIN AssociatesCoursesTriggers AS ACTs ON (ACTs.Id = ACs.Id) " & _
					"WHERE ((ACs.CertificateExpirationDate >= '" & formatdatetime(date() + 61, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate >= '" & formatdatetime(date() + 61, 2) & "')) " & _
					"AND ((ACs.CertificateExpirationDate < '" & formatdatetime(date() + 91, 2) & "' " & _
					"AND ACs.UserSpecRenewalDate IS NULL) OR " & _
					"(ACs.UserSpecRenewalDate < '" & formatdatetime(date() + 91, 2) & "')) " & _
					"AND NOT Us.Inactive = '1' " & _
					"AND NOT Us.UserStatus = '3' " & _
					"AND NOT ACs.Deleted = '1' " & _
					"AND NOT ACs.UserId IS NULL " & _
					"AND (ACTs.Triggered90 = '0' OR ACTs.Triggered90 IS NULL) " & _
					"AND (ACs.CompanyId = '" & iCompanyId & "' " 
				if oCompanyRs("TpLinkCompanyId") <> "" then
					sSql = sSql & "OR ACs.CompanyId = '" & _
						oCompanyRs("TpLinkCompanyId") & "') "
				else
					sSql = sSql & ") "
				end if

				sSql = sSql & "AND ((Us.BeginningEducationDate < ACs.PurchaseDate) OR (Us.BeginningEducationDate IS NULL) OR (ACs.PurchaseDate IS NULL)) "

				oAlertsRs.Open sSql, oConn
				
				do while not oAlertsRs.EOF 
					SendCourseAlertsEmail 3, oAlertsRs("UserId"), oAlertsRs("CourseId"), oAlertsRs("Id")
					MarkCourseTriggered 3, oAlertsRs("Id")
					oAlertsRs.MoveNext
				loop
				oAlertsRs.Close
				
			end if
			
		end if
		oSettingsRs.Close
		
	end if 
	oCompanyRs.Close
	
	oRs.MoveNext
	
loop		
	
	
'Send the Admin Summary emails

'Loop through the Admin email addresses array
for i = 0 to ubound(asAdmins)

	if trim(asAdmins(i)) <> "" then

		'Response.Write("<p><b>" & asAdmins(i) & "</b><br>")
		'Response.Write("<pre>" & asAdminsEmails(i) & "</pre></p>")

		'Create an instance of the jmail object and set its initial values
		set oJmail = CreateObject("Jmail.Message")
		oJmail.ContentType = "text/html"
		oJmail.Silent = false 'true
		oJmail.Logging = true

        if application("TESTING") then
            oJmail.AddRecipient application("TestSendToEmailAddress")
        else
            oJmail.AddRecipient trim(asAdmins(i))  
		end if
        	
		'Setup the email
		sSubject = "ComplianceKeeper Alert Summary "
		sSender = "alerts@compliancekeeper.com"
		sEmailText = "The following are your administrator email alerts for " & date() & ". " & "<br>" & "<br>" & asAdminsEmails(i)

		sMailServerAddress = "smtp.xigroup.com"

		'Set the Jmail values with the information collected
		oJmail.Body = sEmailText
		oJmail.From = sSender
		oJmail.FromName = "ComplianceKeeper.com Alerts"
		oJmail.Subject = sSubject

		'Mail it
		on error resume next
		oJmail.Send(sEmailServer)
		if err.number <> 0 then
            WriteToLogFile("Error Sending Email " & err.Description)            
        end if
		'Log the email
		sSql = "INSERT INTO EmailLog (" & _
			"SenderId, " & _
			"CompanyID, " & _
			"RecipientList, " & _
			"Subject, " & _
			"BodyText, " & _
			"SendDate, " & _
			"SendLog " & _
			") VALUES (" & _
			"'0', " & _
			"'" & iCompanyId & "', " & _
			"'" & replace(asAdmins(i),"'","''") & "', " & _
			"'" & replace(sSubject,"'","''") & "', " & _
			"'" & replace(sEmailText,"'","''") & "', " & _
			"'" & Now() & "', " & _
			"'" & replace(oJmail.Log,"'","''") & "' " & _
			")"

		'oConn.Execute sSql
			
		oJmail.Close
		set oJmail = nothing
		WriteToLogFile("Closed and existed routine, all objectes cleaned up.")
	end if 
	
next

	
	
	
oConn.Close
set oConn = nothing
set oRs = nothing
set oOfficerRs = nothing
set oPlanRs = nothing

WriteToLogFile("Stopped running " & now())
Response.Write("<script type='text/javascript'> window.opener='Self';  window.open('','_parent','');  window.close();</script>")

'Mark a specific license as having triggered a certain alert range
sub MarkLicenseTriggered(p_iAlertType, p_iLicenseId)

	dim sSql
	dim sTriggerRange
	
	if p_iAlertType = 1 then
		sTriggerRange = "30"
	elseif p_iAlertType = 2 then
		sTriggerRange = "60"
	else
		sTriggerRange = "90"
	end if
	
	sSql = "UPDATE Licenses " & _
		"SET Triggered" & sTriggerRange & " = '1' " & _
		"WHERE LicenseId = '" & p_iLicenseId & "'"
	oConn.Execute sSql
	
end sub



'Mark a specific course association as having triggered a certain alert range
sub MarkCourseTriggered(p_iAlertType, p_iAssocId)

	dim sSql
	dim oRs
	dim sTriggerRange
	
	if p_iAlertType = 1 then
		sTriggerRange = "30"
	elseif p_iAlertType = 2 then
		sTriggerRange = "60"
	else
		sTriggerRange = "90"
	end if
	
	sSql = "SELECT Id FROM AssociatesCoursesTriggers WHERE Id = '" & p_iAssocId & "' "
	set oRs = oConn.Execute(sSql)
	
	if not oRs.EOF then
	
		sSql = "UPDATE AssociatesCoursesTriggers " & _
			"SET Triggered" & sTriggerRange & " = '1' " & _
			"WHERE Id = '" & p_iAssocId & "'"
		'Response.Write(sSql)
		oConn.Execute sSql
		
	else
	
		sSql = "INSERT INTO AssociatesCoursesTriggers (Id, Triggered" & sTriggerRange & ") " & _
			"VALUES ('" & p_iAssocId & "', '1') "
		'Response.Write(sSql)
		oConn.Execute sSql
	
	end if
	
end sub



'Send off a license alert email
sub SendLicenseAlertsEmail(p_iAlertType, p_iLicenseId)

	dim sSql
	dim oLicAlertRs
	dim oRecipientsRs
	dim oJmailRs
	
	dim bCcCoAdmins
	dim bCcBrAdmins
	
	dim iBranchId
	dim iCompanyL2Id
	dim iCompanyL3Id
	
	dim sOwnerName
	dim sDayCount
	'dim sRecipientList
	sRecipientList = ""
	dim asRecipientList()
	dim bFoundRecipient
	
	'dim oJmail
	dim sEmailText
	dim sSubject
	dim sSender
	dim sRecipient
	dim sMailServerAddress
	
	set oLicAlertRs = CreateObject("ADODB.Recordset")
	set oRecipientsRs = CreateObject("ADODB.Recordset")
	set oJmailRs = CreateObject("ADODB.Recordset")
	
	'Get the officer and license information
	sSql = "SELECT DISTINCT Ls.LicenseId, " & _
		"Ls.LicenseStateId, " & _
		"Ls.LicenseNum, " & _
		"Ls.OwnerTypeId, " & _
		"Ls.OwnerId, " & _
		"ACs.FirstName, " & _
		"ACs.LastName, " & _
		"ACs.Email, " & _
		"ACs.BranchId, " & _
		"ACs.CompanyL2Id, " & _
		"ACs.CompanyL3Id, " & _
		"Bs.Name AS BranchName, " & _
		"S.State " & _
		"FROM Licenses AS Ls " & _
		"INNER JOIN States AS S " & _
		"ON (S.StateID = Ls.LicenseStateId) " & _
		"LEFT JOIN Associates AS ACs " & _
		"ON (ACs.UserId = Ls.OwnerId AND Ls.OwnerTypeId = 1) " & _
		"LEFT JOIN CompanyBranches AS Bs " & _
		"ON (Bs.BranchId = Ls.OwnerId AND Ls.OwnerTypeId = 2) " & _
		"WHERE Ls.LicenseId = '" & p_iLicenseId & "' "
	oLicAlertRs.Open sSql, oConn
	
	if not oLicAlertRs.EOF then
	
		'Create an instance of the jmail object and set its initial values
		set oJmail = CreateObject("Jmail.Message")
		oJmail.ContentType = "text/html"
		oJmail.Silent = false 'true
		oJmail.Logging = true

		iBranchId = oLicAlertRs("BranchId")
		iCompanyL2Id = oLicAlertRs("CompanyL2Id")
		iCompanyL3Id = oLicAlertRs("CompanyL3Id")
		
		'Set the owner name
		if oLicAlertRs("OwnerTypeId") = 1 then
			sOwnerName = oLicAlertRs("FirstName") & " " & oLicAlertRs("LastName")
		elseif oLicAlertRs("OwnerTypeId") = 2 then
			sOwnerName = oLicAlertRs("BranchName")
		else
			sOwnerName = sCompanyName
		end if
	
	
		'Determine our recipients and the email text
		if p_iAlertType = 1 then
		
			'Set the email text
			sDayCount = "30"
			sEmailText = oSettingsRs("Email30DayText")
			
			'BCC the Branch Admins if the option is checked (and this is an officer rather than company lic), or if this is a Branch License
			if (oSettingsRs("CcBrAdmin30Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iBranchId = "0" or iBranchId = "")) or oLicAlertRs("OwnerTypeId") = 2 then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin30Day"), oSettingsRs("CcL3Admin30Day")
			end if
			
			
			'BCC the CompanyL2 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL2 license
			if (oSettingsRs("CcL2Admin30Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL2Id = "0" or iCompanyL2Id = "")) or oLicAlertRs("OwnerTypeId") = 4 then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if
			
			
			'BCC the CompanyL3 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL3 license
			if (oSettingsRs("CcL3Admin30Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL3Id = "0" or iCompanyL3Id = "")) or oLicAlertRs("OwnerTypeId") = 5 then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin30Day")
			end if
			
			
			'BCC the Company Admins if the option is checked, or if this is a Company license
			if oSettingsRs("CcCoAdmin30Day") or oLicAlertRs("OwnerTypeId") = 3 then
				
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"				
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'Response.Write("<br>Adding " & oRecipientsRs("Email") & " from Company")
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
				
			end if
			
		elseif p_iAlertType = 2 then
		
			'Set the email text
			sDayCount = "60"
			sEmailText = oSettingsRs("Email60DayText")
			
			
			'BCC the Branch Admins if the option is checked (and this is an officer rather than company lic), or if this is a Branch License
			if (oSettingsRs("CcBrAdmin60Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iBranchId = "0" or iBranchId = "")) or oLicAlertRs("OwnerTypeId") = 2 then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin60Day"), oSettingsRs("CcL3Admin60Day")
			end if
			
			
			'BCC the CompanyL2 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL2 license
			if (oSettingsRs("CcL2Admin60Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL2Id = "0" or iCompanyL2Id = "")) or oLicAlertRs("OwnerTypeId") = 4 then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if
			
			
			'BCC the CompanyL3 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL3 license
			if (oSettingsRs("CcL3Admin60Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL3Id = "0" or iCompanyL3Id = "")) or oLicAlertRs("OwnerTypeId") = 5 then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin60Day")
			end if
			
						
			'BCC the Company Admins if the option is checked, or if this is a Company license
			if oSettingsRs("CcCoAdmin60Day") or oLicAlertRs("OwnerTypeId") = 3 then
			
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"					
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
			
			end if
			
		elseif p_iAlertType = 3 then
		
			'Set the email text
			sDayCount = "90"
			sEmailText = oSettingsRs("Email90DayText")
			
			
			'BCC the Branch Admins if the option is checked (and this is an officer rather than company lic), or if this is a Branch License
			if (oSettingsRs("CcBrAdmin90Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iBranchId = "0" or iBranchId = "")) or oLicAlertRs("OwnerTypeId") = 2 then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin90Day"), oSettingsRs("CcL3Admin90Day")
			end if
			
			
			'BCC the CompanyL2 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL2 license
			if (oSettingsRs("CcL2Admin90Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL2Id = "0" or iCompanyL2Id = "")) or oLicAlertRs("OwnerTypeId") = 4 then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if
			
			
			'BCC the CompanyL3 Admins if the option is checked (and this is an
			'officer rather than company lic), or if this is a CompanyL3 license
			if (oSettingsRs("CcL3Admin90Day") and oLicAlertRs("OwnerTypeId") = 1 and not (iCompanyL3Id = "0" or iCompanyL3Id = "")) or oLicAlertRs("OwnerTypeId") = 5 then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin90Day")
			end if
			
		
			'BCC the Company Admins if the option is checked, or if this is a Company license
			if oSettingsRs("CcCoAdmin90Day") or oLicAlertRs("OwnerTypeId") = 3 then
			
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"					
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
			
			end if
			
		end if
		
		
		'Add the actual officer recpient if this is an office license and the Send Loan Officer Flag is set
		if oLicAlertRs("OwnerTypeId") = 1 then
			if (p_iAlertType = 1 and oSettingsRs("SendLO30DayEmail")) or  (p_iAlertType = 2 and oSettingsRs("SendLO60DayEmail")) or (p_iAlertType = 3 and oSettingsRs("SendLO90DayEmail")) then
'				oJmail.AddRecipient "kchambers@xigroup.com"		
                if application("TESTING") then
                    oJmail.AddRecipient application("TestSendToEmailAddress")
                else
	    			oJmail.AddRecipient oLicAlertRs("Email")   
                end if
    			
				'sRecipientList = sRecipientList & ", " & oLicAlertRs("Email")
			end if
		end if
		
		'Trim inital comma from recipient list
		sRecipientList = mid(sRecipientList, 3)
		
		
		'Setup the email
		sSubject = sDayCount & " Day License Expiration Alert: " & sOwnerName
		sSender = "alerts@compliancekeeper.com"
		sEmailText = sEmailText & "<br>" & "<br>" & sOwnerName & ": License " 
		if not isnull(oLicAlertRs("LastName")) then
			sEmailText = sEmailText & oLicAlertRs("LastName") & " "
		end if
		sEmailText = sEmailText & oLicAlertRs("State") & " " & oLicAlertRs("LicenseNum") & "<br>" & _
					 "<a href=""" & sUrl & "/officers/modlicense.asp?lid=" & oLicAlertRs("LicenseID") & "&referrer=LICENSEALERTEMAIL"">Click here to Edit this License</a>"
		
		
		'Add the recipients and their email texts to the respective arrays.
		asRecipients = split(sRecipientList, ", ")
		for each sRecipient in asRecipients
			'Verify whether the recipient is already in the array.
			for i = 0 to ubound(asAdmins)
				if trim(asAdmins(i)) = trim(sRecipient) then
					bFoundRecipient = true
					exit for
				end if
			next
			if not bFoundRecipient then
				'i = i + 1
				redim preserve asAdmins(i)
				redim preserve asAdminsEmails(i)
				asAdmins(i) = sRecipient
			end if
			asAdminsEmails(i) = asAdminsEmails(i) & "<br>" & "<br>" & "<br>" & "----------" & "<br>" & sEmailText
		next
		
		
		sMailServerAddress = "smtp.xigroup.com"

		'Set the Jmail values with the information collected
		oJmail.Body = sEmailText
		'oJmail.AddRecipient(sRecipient)
		oJmail.From = sSender
		oJmail.FromName = "ComplianceKeeper.com Alerts"
		oJmail.Subject = sSubject

		'Mail it
		on error resume next
		oJmail.Send(sEmailServer)
		    if err.number <> 0 then
                WriteToLogFile("Error Sending Email " & err.Description)        
            end if
		'Log the email
		sSql = "INSERT INTO EmailLog (" & _
			"SenderId, " & _
			"CompanyID, " & _
			"RecipientList, " & _
			"Subject, " & _
			"BodyText, " & _
			"SendDate, " & _
			"SendLog " & _
			") VALUES (" & _
			"'0', " & _
			"'" & iCompanyId & "', " & _
			"'" & replace(sRecipientList,"'","''") & "', " & _
			"'" & replace(sSubject,"'","''") & "', " & _
			"'" & replace(sEmailText,"'","''") & "', " & _
			"'" & Now() & "', " & _
			"'" & replace(oJmail.Log,"'","''") & "' " & _
			")"
		'Response.Write("<p>" & sSql & "<p>")
		oConn.Execute sSql
		
		'Response.Write("<p><b>" & sCompanyName & "</b>")
		'Response.Write("<br>" & sSubject)
		'Response.Write("<br>" & sRecipientList)
		'Response.Write("<br>" & oLicAlertRs("Email"))
		'Response.Write("<br>" & sEmailText)
		'Response.Write("<br>" & oJmail.From)
	
		oJmail.Close
		set oJmail = nothing
		WriteToLogFile("Closed and existed routine, all objectes cleaned up.")
	end if
end sub


'sub SendCourseAlertEmails(p_iAlertType, p_iUserId, p_iCompanyId)
'end sub

sub SendCourseAlertsEmail(p_iAlertType, p_iUserId, p_iCourseId, p_iAssocId)

	dim sSql
	dim oCourseAlertRs
	dim oRecipientsRs
	dim oJmailRs
	
	dim bCcCoAdmins
	dim bCcBrAdmins
	
	dim iBranchId
	dim iCompanyL2Id
	dim iCompanyL3Id
	
	dim sOwnerName
	dim sDayCount
	sRecipientList = ""
	
	'dim oJmail
	dim sEmailText
	dim sSubject
	dim sSender
	dim sRecipient
	dim sMailServerAddress
	
	set oCourseAlertRs = CreateObject("ADODB.Recordset")
	set oRecipientsRs = CreateObject("ADODB.Recordset")
	set oJmailRs = CreateObject("ADODB.Recordset")
	
	'Get the officer and course information
	sSql = "SELECT DISTINCT VCs.Name, " & _
		"VCs.Number, " & _
		"VCs.StateId, " & _
		"Ss.State, " & _
		"Ss.Abbrev, " & _
		"ACs.FirstName, " & _
		"ACs.LastName, " & _
		"ACs.Email, " & _
		"ACs.BranchId, " & _
		"ACs.CompanyL2Id, " & _
		"ACs.CompanyL3Id, " & _
		"VACs.CertificateExpirationDate, " & _
		"VACs.UserSpecRenewalDate " & _
		"FROM vAssociatesCoursesCkUserId AS VACs " & _
		"LEFT JOIN Associates AS ACs " & _
		"ON (VACs.UserId = ACs.UserId) " & _
		"LEFT JOIN vCourses AS VCs " & _
		"ON (VCs.CourseId = VACs.CourseId) " & _
		"LEFT JOIN States AS Ss " & _
		"ON (Ss.StateId = VCs.StateId) " & _
		"WHERE VACs.Id = '" & p_iAssocId & "' "
	oCourseAlertRs.Open sSql, oConn
	

	if not oCourseAlertRs.EOF then
	
		'Create an instance of the jmail object and set its initial values
		set oJmail = CreateObject("Jmail.Message")
		oJmail.ContentType = "text/html"
		oJmail.Silent = false 'true
		oJmail.Logging = true

		iBranchId = oCourseAlertRs("BranchId")
		iCompanyL2Id = oCourseAlertRs("CompanyL2Id")
		iCompanyL3Id = oCourseAlertRs("CompanyL3Id")
		
		'Set the owner name
		sOwnerName = oCourseAlertRs("FirstName") & " " & oCourseAlertRs("LastName")
		
	
		'Determine our recipients and the email text
		if p_iAlertType = 1 then
		
			'Set the email text
			sDayCount = "30"
			sEmailText = oSettingsRs("CourseEmail30DayText")
			
			'BCC the Branch Admins if the option is checked 
			if oSettingsRs("CcBrAdmin30DayCourse") then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin30DayCourse"), oSettingsRs("CcL3Admin30DayCourse")
			end if
			
			'BCC the CompanyL3 Admins if the option is checked
			if oSettingsRs("CcL3Admin30DayCourse") then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin30DayCourse")
			end if
			
			'BCC the CompanyL2 Admins if the option is checked
			if oSettingsRs("CcL2Admin30DayCourse") then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if			
			
			'BCC the Company Admins if the option is checked
			if oSettingsRs("CcCoAdmin30DayCourse") then
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"					
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'Response.Write("<br>Adding " & oRecipientsRs("Email") & " from Company")
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
			end if
			
		elseif p_iAlertType = 2 then
		
			'Set the email text
			sDayCount = "60"
			sEmailText = oSettingsRs("CourseEmail60DayText")
			
			'BCC the Branch Admins if the option is checked 
			if oSettingsRs("CcBrAdmin60DayCourse") then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin60DayCourse"), oSettingsRs("CcL3Admin60DayCourse")
			end if
			
			'BCC the CompanyL3 Admins if the option is checked
			if oSettingsRs("CcL3Admin60DayCourse") then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin60DayCourse")
			end if
			
			'BCC the CompanyL2 Admins if the option is checked
			if oSettingsRs("CcL2Admin60DayCourse") then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if			
			
			'BCC the Company Admins if the option is checked
			if oSettingsRs("CcCoAdmin60DayCourse") then
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"					
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'Response.Write("<br>Adding " & oRecipientsRs("Email") & " from Company")
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
			end if
						
		elseif p_iAlertType = 3 then
		
			'Set the email text
			sDayCount = "90"
			sEmailText = oSettingsRs("CourseEmail90DayText")
			
			'BCC the Branch Admins if the option is checked 
			if oSettingsRs("CcBrAdmin90DayCourse") then
				AddBranchAdmins iBranchId, iCompanyId, oSettingsRs("CcL2Admin90DayCourse"), oSettingsRs("CcL3Admin90DayCourse")
			end if
			
			'BCC the CompanyL3 Admins if the option is checked
			if oSettingsRs("CcL3Admin90DayCourse") then
				AddL3Admins iCompanyL3Id, iCompanyId, oSettingsRs("CcL2Admin90DayCourse")
			end if
			
			'BCC the CompanyL2 Admins if the option is checked
			if oSettingsRs("CcL2Admin90DayCourse") then
				AddL2Admins iCompanyL2Id, iCompanyId
			end if			
			
			'BCC the Company Admins if the option is checked
			if oSettingsRs("CcCoAdmin90DayCourse") then
				sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
					"WHERE Us.CompanyId = '" & iCompanyId & "' " & _
					"AND Us.GroupId = '2' " & _
					"AND Us.UserStatus = '1' " & _
					"AND Us.CConAlertEmails = 1"
				oRecipientsRs.Open sSql, oConn
				
				do while not oRecipientsRs.EOF 
					'Response.Write("<br>Adding " & oRecipientsRs("Email") & " from Company")
					'oJmail.AddRecipientBcc oRecipientsRs("Email")
					sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
					oRecipientsRs.MoveNext
				loop
				oRecipientsRs.Close
			end if
						
		end if
		
		
		'Add the actual course owner recipient
        if application("TESTING") then
            oJmail.AddRecipient application("TestSendToEmailAddress")
        else
		    oJmail.AddRecipient oCourseAlertRs("Email")   
        end if		

		'sRecipientList = sRecipientList & ", " & oCourseAlertRs("Email")
		
		'Trim inital comma from recipient list
		sRecipientList = mid(sRecipientList, 3)
	
	
		'Setup the email
		sSubject = sDayCount & " Day Course Expiration Alert: " & sOwnerName
		sSender = "alerts@compliancekeeper.com"
		sEmailText = sEmailText & "<br>" & "<br>" & sOwnerName & ": " & oCourseAlertRs("Name") & " (for " & oCourseAlertRs("State") & ") "
		
		
		'Add the recipients and their email texts to the respective arrays.
		asRecipients = split(sRecipientList, ", ")
		for each sRecipient in asRecipients
			'Verify whether the recipient is already in the array.
			for iCount = 0 to ubound(asAdmins)
				if trim(asAdmins(iCount)) = trim(sRecipient) then
					bFoundRecipient = true
					exit for
				end if
			next
			if not bFoundRecipient then
				'iCount = iCount + 1
				redim preserve asAdmins(iCount)
				redim preserve asAdminsEmails(iCount)
				asAdmins(iCount) = sRecipient
			end if
			asAdminsEmails(iCount) = asAdminsEmails(iCount) & "<br>" & "<br>" & "<br>" & "----------" & "<br>" & sEmailText
		next
		
		
		sMailServerAddress = "smtp.xigroup.com"

		'Set the Jmail values with the information collected
		oJmail.Body = sEmailText
		'oJmail.AddRecipient(sRecipient)
		oJmail.From = sSender
		oJmail.FromName = "ComplianceKeeper.com Alerts"
		oJmail.Subject = sSubject
	
		'Mail it<br>
		on error resume next
		oJmail.Send(sEmailServer)
		 if err.number <> 0 then
             WriteToLogFile("Error Sending Email " & err.Description)
        
         end if
		'Log the email
		sSql = "INSERT INTO EmailLog (" & _
			"SenderId, " & _
			"CompanyID, " & _
			"RecipientList, " & _
			"Subject, " & _
			"BodyText, " & _
			"SendDate, " & _
			"SendLog " & _
			") VALUES (" & _
			"'0', " & _
			"'" & iCompanyId & "', " & _
			"'" & replace(sRecipientList,"'","''") & "', " & _
			"'" & replace(sSubject,"'","''") & "', " & _
			"'" & replace(sEmailText,"'","''") & "', " & _
			"'" & Now() & "', " & _
			"'" & replace(oJmail.Log,"'","''") & "' " & _
			")"
		'Response.Write("<p>" & sSql & "<p>")
		oConn.Execute sSql
		
		'Response.Write("<p><b>" & sCompanyName & "</b>")
		'Response.Write("<br>" & sSubject)
		'Response.Write("<br>" & sRecipientList)
		'Response.Write("<br>" & oCourseAlertRs("Email"))
		'Response.Write("<br>" & sEmailText)
		'Response.Write("<br>" & oJmail.From)
		
	
		oJmail.Close
		set oJmail = nothing
		WriteToLogFile("Closed and existed routine, all objectes cleaned up.")
	end if
end sub


'Add the Company L2 Admins to the recipient list.
sub AddL2Admins(p_iCompanyL2Id, p_iCompanyId)

	dim oRecipientsRs
	dim sSql
	
	set oRecipientsRs = CreateObject("ADODB.Recordset")

	sSql = "SELECT DISTINCT Us.Email FROM Users AS Us " & _
		"LEFT JOIN UsersCompaniesL2 AS UCL2s " & _
		"ON (Us.UserId = UCL2s.UserId) " & _
		"WHERE Us.CompanyId = '" & p_iCompanyId & "' " & _
		"AND UCL2s.CompanyL2Id = '" & p_iCompanyL2Id & "' " & _
		"AND Us.GroupId = '3' " & _
		"AND Us.UserStatus = '1' " & _
		"AND Us.CConAlertEmails = 1"	 
	oRecipientsRs.Open sSql, oConn
	
	'Response.Write("<br>" & sSql)
				
	do while not oRecipientsRs.EOF 
		'Response.write("Adding: " & oRecipientsRs("Email") & " from AddL2Admins<br>")
		'oJmail.AddRecipientBcc oRecipientsRs("Email")
		sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
		oRecipientsRs.MoveNext
	loop
	oRecipientsRs.Close
	
	set oRecipientsRs = nothing
				
end sub


'Add the Company L3 Admins to the recipient list.
sub AddL3Admins(p_iCompanyL3Id, p_iCompanyId, p_bNotifyL2s)

	dim oRecipientsRs
	dim sSql

	set oRecipientsRs = CreateObject("ADODB.Recordset")

	sSql = "SELECT DISTINCT Us.Email, CL3s.CompanyL3Id, CL3s.CompanyL2Id FROM Users AS Us " & _
		"LEFT JOIN UsersCompaniesL3 AS UCL3s " & _
		"ON (Us.UserId = UCL3s.UserId) " & _
		"LEFT JOIN CompaniesL3 AS CL3s " & _
		"ON (UCL3s.CompanyL3Id = CL3s.CompanyL3Id) " & _
		"WHERE Us.CompanyId = '" & p_iCompanyId & "' " & _
		"AND UCL3s.CompanyL3Id = '" & p_iCompanyL3Id & "' " & _
		"AND Us.GroupId = '5' " & _
		"AND Us.UserStatus = '1' " & _
		"AND Us.CConAlertEmails = 1"	 
	oRecipientsRs.Open sSql, oConn

	'We'll want to notify the Company L2 admins as well if this L3 is assigned
	'to one and we are set to notify L2 admins.
	if not oRecipientsRs.EOF then
		if oRecipientsRs("CompanyL2Id") <> "" and p_bNotifyL2s then
			AddL2Admins oRecipientsRs("CompanyL2Id"), p_iCompanyId
		end if
	end if
				
	do while not oRecipientsRs.EOF 
		'Response.write("Adding: " & oRecipientsRs("Email") & " from AddL3Admins<br>")
		'oJmail.AddRecipientBcc oRecipientsRs("Email")
		sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
		oRecipientsRs.MoveNext
	loop
	
	oRecipientsRs.Close
	set oRecipientsRs = nothing
				
end sub


'Add the Branch admins to the recipient list.
sub AddBranchAdmins(p_iBranchId, p_iCompanyId, p_bNotifyL2s, p_bNotifyL3s)

	dim oRecipientsRs
	dim sSql
	
	set oRecipientsRs = CreateObject("ADODB.Recordset")

	sSql = "SELECT DISTINCT Us.Email, CBs.CompanyL2Id, CBs.CompanyL3Id FROM Users AS Us " & _
		"LEFT JOIN UsersBranches AS UBs " & _
		"ON (UBs.UserId = Us.UserId) " & _
		"LEFT JOIN CompanyBranches AS CBs " & _
		"ON (CBs.BranchId = UBs.BranchId) " & _
		"WHERE Us.CompanyId = '" & p_iCompanyId & "' " & _
		"AND UBs.BranchId = '" & p_iBranchId & "' " & _
		"AND Us.GroupId = '10' " & _
		"AND Us.UserStatus = '1' " & _
		"AND Us.CConAlertEmails = 1"	 
	oRecipientsRs.Open sSql, oConn
	
	'We'll want to notify the Company L3 or L2 admins as well if this branch is
	'assigned to one of them and we are set to notify the admins of that tier.
	if not oRecipientsRs.EOF then
		if oRecipientsRs("CompanyL2Id") <> "" and p_bNotifyL2s then
			AddL2Admins oRecipientsRs("CompanyL2Id"), p_iCompanyId
		elseif oRecipientsRs("CompanyL3Id") <> "" and p_bNotifyL3s then
			AddL3Admins oRecipientsRs("CompanyL3Id"), p_iCompanyId, p_bNotifyL2s
		end if
	end if
				
	do while not oRecipientsRs.EOF 
		'Response.write("Adding: " & oRecipientsRs("Email") & " from AddBranchAdmins<br>")
		'oJmail.AddRecipientBcc oRecipientsRs("Email")
		sRecipientList = sRecipientList & ", " & oRecipientsRs("Email")
		oRecipientsRs.MoveNext
	loop
	oRecipientsRs.Close
	set oRecipientsRs = nothing

end sub


Sub WriteToLogFile(MyText)
    Set FSOobj = Server.CreateObject("Scripting.FileSystemObject")
    Dim FilePath
    FilePath = sErrorFile
    if FSOobj.fileExists(FilePath) Then 
        Set OpenFileobj = FSOobj.OpenTextFile(FilePath, 8, 0)
        OpenFileobj.WriteLine(MyText)
        OpenFileobj.Close
        Set OpenFileobj = Nothing
    Else
        Response.Write "File does not exist"
    End if 
    
    Set FSOobj = Nothing
    
End Sub

 %>
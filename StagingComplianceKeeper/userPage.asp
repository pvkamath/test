<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/HandyAdo.Class.asp" -->
<!-- #include virtual = "/admin/includes/functions.asp" ---->
<!-- #include virtual = "/includes/User.Class.asp" --------->
<!-- #include virtual = "/includes/Course.Class.asp" --------->
<!-- #include virtual = "/includes/NewsKnowledgeMethods.asp" -->
<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------

	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	
	' Set Page Title:
	sPgeTitle = "TrainingPro - Home"

	if UCase(Request.ServerVariables("HTTPS")) = "ON" then
		response.redirect(application("URL") & "/UserPage.asp")
	end if	
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		
		var preloadFlag = true;
		
		function changeImages() {
			if (document.images && (preloadFlag == true)) {
				for (var i=0; i<changeImages.arguments.length; i+=2) {
					document[changeImages.arguments[i]].src = changeImages.arguments[i+1];
				}
			}
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
CheckIsLoggedIn()

dim oUserObj, oCourseObj
dim oRs
dim sClass 'Used for alternating rows
dim iCounter 'Counter var
dim bHadCourses
dim sFullName
dim sCompany
dim sAddress
dim sAddress2
dim sPhone
dim sCourseExpirationDate
dim sThumbnail
dim sCertificateExpirationDate
dim bCertificateReleased

set oUserObj = new User
oUserObj.ConnectionString = application("sDataSourceName")

set oCourseObj = new Course
oCourseObj.ConnectionString = application("sDataSourceName")

oUserObj.UserID = session("User_ID")
sFullName = session("User_FullName") 

set oRs = oUserObj.GetUser
sMemberNumber = oRS("MemberNumber")
sCompany = oRs("CompanyName")
sAddress = oRs("Address")

if trim(oRs("City")) <> "" and GetStateName(oRs("StateID"), true) <> "" then 
	sAddress2 = oRs("City") & ", " & GetStateName(oRs("StateID"), true) & " "
elseif GetStateName(oRs("StateID"), true) <> "" then
	sAddress2 = GetStateName(oRs("StateID"), true) & " "
elseif trim(oRs("City")) <> "" then
	sAddress2 = oRs("City") & " "
end if 

if trim(oRs("Zipcode")) <> "" then
	sAddress2 = sAddress2 & oRs("Zipcode")
end if

sPhone = trim(oRs("Phone"))

	
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
			
			<table width=100% border=0 cellpadding=0 cellspacing=0>
				<tr>
					<!-- Left Column  -->
					<td width=283 valign="top">
						<!-- Intro Table  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/introBgr.gif">
									<table border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="date"><% = DayName(Weekday(now)) & " " & date & " at " & time() %> (Eastern)</span>
												<!--<span class="date">Thursday 09/2704 at 3:26PM (Eastern)</span>-->
												<p class="userInfo">
												<% if trim(sMemberNumber) <> "" then %>
												<%= sMemberNumber %><br>
												<% end if %>
												<% = sFullName %><br>
												<% = sCompany %><br>
												<% = sAddress %><br>
												<% = sAddress2 %><br>
												<% = sPhone %></p>
												
												<p><a href="register.asp"><img src="Media/Images/bttnUpdate.gif" alt="Update" border="0"></a></p></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
						<!-- Calendar of Events  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/columnBgr2.gif">
									<table border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="sectionTitle">Calendar of Events</span>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
													</tr>
													<tr>
														<td valign="top"><img src="/Media/Images/photoEvents.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td width="100%" valign="top">
<%

dim oConn
dim sCalendarRow

set oConn = new HandyADO

sSql = "SELECT Es.EventID, Es.Event, Es.StartDate " & _
	   "FROM afxEvents AS Es " & _
	   "WHERE Es.EndDate > '" & Now & "' " & _
	   "AND Es.EndDate < '" & Now + 30 & "' " & _
	   "ORDER BY Es.StartDate ASC"
	   
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)

%>

<table border=0 cellpadding=10 cellspacing=0>

<%

do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""F6F4EE"""
	else
		sCalendarRow = ""
	end if

	Response.Write("<tr>" & vbCrLf)
	Response.Write("	<td valign=""top""" & sCalendarRow & "><a href=""event.asp?id=" & oRs("EventID") & """ class=""newstitle"">" & oRs("StartDate") & " - " & oRs("Event") & "</a></td>" & vbCrLf)
	Response.Write("</tr>")		

	oRs.MoveNext

loop

%>
<tr>
	<td valign="top"><a href="calendar.asp"><img src="<% = application("sDynMediaPath") %>bttnViewCalendar.gif" border="0"></a></td>
</tr>
</table>

														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
						<!-- News Table  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/newsBgr.gif">
									<table border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td colspan="2"><img src="/Media/Images/titleDailyNews.gif" width="234" height="27" alt="TrainingPro News" border="0"></td>
										</tr>
										<% Call PrintNewsKnowledgeNews() 'located in /includes/NewsKnowledgeMethods.asp %>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/newsBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
							<tr>
								<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<td><img src="/Media/Images/spacer.gif" width="6" height="10" alt="" border="0"></td>
					<!-- Right Courses  -->
					<td valign="top">
						<!-- Active Courses  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/columnBgr1.gif">
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/spacer.gif" width="30" height="10" alt="" border="0"><br>
												<span class="sectionTitle">Active Courses</span>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="3"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
													</tr>												
													<tr>
														<td valign="top" colspan="2">&nbsp;</td>
														<td width="100%" valign="top">																												
															<table width="100%" border=0 cellpadding=0 cellspacing=0>
																<tr>
																	<td><b>Key:</b>&nbsp;&nbsp;</td>
																	<td><img src="/Media/Images/bulletGreen.jpg" width="10" height="12" alt="" border="0" hspace="10" vspace="3"></td>
																	<td>Completed</td>
																	<td><img src="/Media/Images/bulletRed.jpg" width="10" height="12" alt="" border="0" hspace="10" vspace="3"></td>
																	<td width="100%">Incomplete</td>
																</tr>
																<tr>
																	<td colspan="4"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>	
																</tr>
															</table>
														</td>
													</tr>	
													<tr>
														<td valign="top"><img src="/Media/Images/photoActive.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td width="100%" valign="top">
															<table width="100%" border=0 cellpadding=10 cellspacing=0>
<%
	'Show All Non Completed Courses
	set oRs = oUserObj.GetCourses(1, 1, "","","")
	iCounter = 0
	sClass = ""
	bHadCourses = false
	
	if not (oRs.BOF and oRs.EOF) then
	
		do while not oRs.EOF and iCounter < 3
			if (oUserObj.DisplayCourse(oRs("ID"))) and (not oUserObj.CourseCompleted(oRs("ID"))) then
				bHadCourses = true
			
				if sClass = "" then	
					sClass = " bgcolor=""F6F4EE"""
				else
					sClass = ""
				end if
			
%>
																<tr>
																	<td<% = sClass %>>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td valign="top"><img src="/Media/Images/bulletRed.jpg" width="10" height="12" alt="" border="0" hspace="0" vspace="3"></td>
																				<td><img src="/Media/Images/spacer.gif" width="6" height="10" alt="" border="0"></td>
																				<td width="100%" valign="top"><a href="/Course/CourseLessons.asp?UserCourseID=<%=oRs("ID")%>" class="newstitle"><% = oRs("Name") %></a><br>
																					<%= formatdatetime(oRs("PurchaseDate"),2) %></td>
																			</tr>
																		</table>
																	</td>
																</tr>
<%		
				iCounter = iCounter + 1
			end if

			oRs.MoveNext
		loop	
	else
		bHadCourses = false
	end if
	
	if bHadCourses then
%>
																<tr>
																	<td><a href="/Course/ShowActiveCourses.asp"><img src="/Media/Images/bttnViewAll.gif" alt="View All" border="0"></a></td>
																</tr>
<%
	else
%>
																<tr>
																	<td bgcolor="F6F4EE">
																		<b>You have no courses in progress.</b>
																	</td>
																</tr>
<%	
	end if 

%>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
						<!-- Course History  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/columnBgr2.gif">
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="sectionTitle">Course History</span>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="3"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
													</tr>
													<tr>
														<td valign="top" colspan="2">&nbsp;</td>
														<td width="100%" valign="top">																												
															<table width="100%" border=0 cellpadding=0 cellspacing=0>
																<tr>
																	<td><b>Key:</b>&nbsp;&nbsp;</td>
																	<td><img src="/Media/Images/bulletGreen.jpg" width="10" height="12" alt="" border="0" hspace="10" vspace="3"></td>
																	<td>Completed</td>
																	<td><img src="/Media/Images/bulletRed.jpg" width="10" height="12" alt="" border="0" hspace="10" vspace="3"></td>
																	<td width="100%">Expired</td>
																</tr>
																<tr>
																	<td colspan="4"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>	
																</tr>
															</table>
														</td>
													</tr>													
													<tr>
														<td valign="top"><img src="/Media/Images/photoHistory.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td width="100%" valign="top">
															<table width="100%" border=0 cellpadding=10 cellspacing=0>
<%
	'Show all Completed Courses
	set oRs = oUserObj.GetCourses("", 1,"","","CompletionDate DESC, ID DESC")
	iCounter = 0
	bHadCourses = false
	sClass = ""
	
	if not (oRs.BOF and oRs.EOF) then
	
		do while not oRs.EOF and iCounter < 3			
			if oRs("Completed") then
				iCourseID = oRs("CourseID")
				
				'determine if the course has expired
				sCourseExpirationDate = oRs("CourseExpirationDate")

				if (datediff("d",date(),sCourseExpirationDate) >= 0) then
					sThumbnail = "bulletGreen.jpg"
					sLink = "<a class=""newsTitle"" href=""/Course/CourseLessons.asp?UserCourseID=" & oRs("ID") & """ class=""newstitle"">" & oRs("Name") & "</a>"
				else
					sThumbnail ="bulletRed.jpg"
					sLink = "<b>" & oRs("Name") & "</b>"
				end if
				
				'Certificate Expiration
				sCertificateExpirationDate = oRs("CertificateExpirationDate")
				bCertificateReleased = oRs("CertificateReleased")				
				
				bHadCourses = true
				
				if sClass = "" then	
					sClass = " bgcolor=""F6F4EE"""
				else
					sClass = ""
				end if
			
%>
																<tr>
																	<td<% = sClass %>>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td valign="top"><img src="/Media/Images/<%=sThumbnail%>" width="10" height="12" alt="" border="0" hspace="0" vspace="3"></td>
																				<td><img src="/Media/Images/spacer.gif" width="6" height="10" alt="" border="0"></td>
																				<td width="100%" valign="top"><%= sLink %><br>
																					<%= formatdatetime(oRs("CompletionDate"),2) %><br>
<%
			'show certificate if released and the course has a certificate			
			if oCourseObj.HasCertificate(iCourseID) then			
				if bCertificateReleased then							
%>
															<a class="newstitle" href="/Course/ViewCertificate.asp?UserCourseID=<%= oRs("ID") %>" target=_blank>View Certificate</a>
<%
				else
%>
															Certificate Pending<br>
															<a href="<%=application("sDynDocumentsFolder")%>ProctorForm.doc" class="newsTitle" target=_blank>Proctor Form</a>
<%
				end if
			end if
%>																																									
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
<%
				iCounter = iCounter + 1
			end if
			
			oRs.MoveNext
			
		loop
	
	else
	
		bHadCourses = false
		
	end if
	
	if bHadCourses then
%>
																<tr>
																	<td><a href="/Course/ShowCourseHistory.asp"><img src="/Media/Images/bttnViewAll.gif" alt="View All" border="0"></a></td>
																</tr>
<%
	else
%>
																<tr>
																	<td bgcolor="F6F4EE">
																		<b>You have no completed courses.</b>
																	</td>
																</tr>
<%
	end if
%>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
						<!-- Market Update  -->
						<!--
						<table background="/media/images/columnBgr2.gif" width="100%" border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/columnBgr2.gif">
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><img src="/Media/Images/photoMarket.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
											<td width="100%"><span class="sectionTitle">Market Update</span></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td colspan="2"><b>Market Direction</b></td>
										</tr>
										<tr>
											<td>
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr>
														<td bgcolor="F6F4EE" nowrap><a href="" class="newstitle">Rates Are</a></td>
														<td width="100%" bgcolor="F6F4EE" align="right">Moving Slightly Lower</td>
													</tr>
												</table>
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr>
														<td bgcolor="F6F4EE" nowrap><a href="" class="newstitle">Rate Volatility</a></td>
														<td width="100%" bgcolor="F6F4EE" align="right">Low</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2"><b>Interest Rate Trends</b></td>
										</tr>
										<tr>
											<td>
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr>
														<td bgcolor="F6F4EE" nowrap><a href="" class="newstitle">Long Term (4-6 weeks)</a></td>
														<td width="100%" bgcolor="F6F4EE" align="right">Flat</td>
													</tr>
												</table>
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr>
														<td bgcolor="F6F4EE" nowrap><a href="" class="newstitle">Short Term (1-2 weeks)</a></td>
														<td width="100%" bgcolor="F6F4EE" align="right">Flat</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2"><b>Market Data</b></td>
										</tr>
										<tr>
											<td>
												<table width="100%" border=0 cellpadding=10 cellspacing=0>
													<tr>
														<td bgcolor="F6F4EE" nowrap>third party product here</td></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"><a href=""><img src="/Media/Images/bttnViewAll.gif" alt="View All" border="0"></a></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
						-->
						<!-- Course History  -->
						<table border=0 cellpadding=0 cellspacing=0>
							<tr>
								<td background="/media/images/columnBgr2.gif">
									<!-- #include virtual="/includes/calc.asp" -->
								    <!--
									<table width="100%" border=0 cellpadding=10 cellspacing=0>
										<tr>
											<td><span class="sectionTitle">Mortgage Calculator</span>
												<table border=0 cellpadding=0 cellspacing=0>
													<tr>
														<td colspan="3"><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
													</tr>
													<tr>
														<td valign="top"><img src="/Media/Images/photoCalculator.gif" width="40" height="40" alt="" border="0" vspace="0"></td>
														<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
														<td width="100%" valign="top">
															<table width="100%" border=0 cellpadding=10 cellspacing=0>
																<tr>
																	<td>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td nowrap><b>Principal Loan<br>Balance</b></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td width="100%"><input type="text" size="10" name="LoanBalance" height="10"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td nowrap><b>Interest Rate</b></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td><input type="text" size="3" name="LoanBalance" height="10"></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td nowrap><b>%</b></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td nowrap><b>Amortization<br>Length</b></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td><input type="text" size="2" name="LoanBalance" height="10"></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td nowrap><b>Years</b></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td colspan="5"><b>Starting:</b></td>
																			</tr>
																			<tr>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="6" alt="" border="0"></td>
																			</tr>
																			<tr>
																				<td>Month:&nbsp;&nbsp;</td>
																				<td><select size="" name="Starting Month">
																					<option value="January">Jan
																					<option value="February">Feb
																					<option value="March">Mar
																					<option value="April">Apr
																					<option value="May">May
																					<option value="June">Jun
																					<option value="July">Jul
																					<option value="August">Aug
																					<option value="September">Sep
																					<option value="October">Oct
																					<option value="November">Nov
																					<option value="December">Dec</td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td>Year:&nbsp;&nbsp;</td>
																				<td><input type="text" size="2" name="Starting Year" height="10"></td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td>
																		<table border=0 cellpadding=0 cellspacing=0>
																			<tr>
																				<td nowrap><b>Show Full<br>Amortization Table</b></td>
																				<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
																				<td><select size="" name="Show Full Amortization Table">
																					<option value="Yes">Yes
																					<option value="No">No</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td colspan="2"><a href=""><img src="/Media/Images/bttnCalculate.gif" alt="Calculate Mortgage" border="0"></a></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									-->
								</td>
							</tr>
							<tr>
								<td><img src="/Media/Images/columnBttm2.gif" width="283" height="10" alt="" border="0"></td>
							</tr>
						</table>
					</td>
					<!-- Right Border  -->
					<td><img src="/Media/Images/spacer.gif" width="10" height="10" alt="" border="0"></td>
				</tr>

<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------

	set oRs = nothing
	set oUserObj = nothing
	set oCourseObj = nothing

	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp
%>

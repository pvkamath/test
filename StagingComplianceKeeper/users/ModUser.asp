<%
option explicit
%>
<!-- #include virtual = "/admin/security/includes/Security.asp" ------------------------------------------->
<!-- #include virtual = "/admin/includes/headHtml.asp" ---------------------------------------------------->
<!-- #include virtual = "/admin/includes/topHtml.asp" ----------------------------------------------------->
<!-- #include virtual = "/admin/includes/functions.asp" --------------------------------------------------->
<!-- #include virtual = "/admin/includes/Misc.asp" -------------------------------------------------------->
<!-- #include virtual = "/includes/User.Class.asp" ---------------------------------------------------->

<%
dim oUserObj 'as object
dim rs 'as object
dim iUserID 'as integer
dim sMode 'as string
dim sLogin
dim sEmail, sPassword, sConfirmPassword 'as string
dim sFirstName, sMid, sLastName 'as string
dim iGroupID, iProviderID, iBranchID 'as integer
dim bBranchesDropDownDisabled 'as integer
dim iScreenRefreshed 'as integer
dim sCertCompanyName,sLicenseNo, sSSN, sDriversLicenseNo 'as string
dim sCity, sAddress, sZipcode, sPhone, sFax 'as string
dim iStateID, iUserStatus 'as integer
dim bCrossCertEligible, bNewsletter 'as boolean
dim sCrossCertEligibleChecked, sNewsletterChecked 'as string
dim sMarketWareConfirmNo 'as string
dim sNotes 'as string
dim sAllowedToAddUsersChecked, sAllowedToAddUsersDisabled  'as string
dim bAllowedToAddUsers 'as boolean
dim iCompanyId

iUserID = ScrubForSQL(request("UserID"))

iScreenRefreshed = trim(request("ScreenRefresh"))
if trim(iScreenRefreshed) = "" then
	iScreenRefreshed = 0 
end if

if iUserID = "" then
	sMode = "Add"
	
	'if the screen was refreshed, pull the values that were stored on the form
	if (cint(iScreenRefreshed) = 1) then
		sLogin = trim(request("Login"))
		sEmail = trim(request("Email"))
		sPassword = trim(request("Password"))
		sConfirmPassword = trim(request("ConfirmPassword"))
		sFirstName = trim(request("FirstName"))
		sMid = trim(request("MiddleName"))
		sLastName = trim(request("LastName"))
		iGroupID = trim(request("GroupID"))
		iCompanyId = ScrubForSql(request("Company"))
		iBranchId = ScrubForSql(request("BranchId"))
		iProviderID = trim(request("Provider"))
		sSSN = trim(request("SSN"))
		sCity = trim(request("City"))
		iStateID = trim(request("State"))
		sAddress = trim(request("Address"))
		sZipcode = trim(request("Zipcode"))
		sPhone = trim(request("Phone"))
		sFax = trim(request("Fax"))
		iUserStatus = trim(request("UserStatus"))
		sNotes = trim(request("Notes"))
		
	else

		iGroupID = ""

	end if

else
	sMode = "Edit"
	
	'if the screen was refreshed, pull the values that were stored on the form
	if (cint(iScreenRefreshed) = 1) then
		sLogin = trim(request("Login"))
		sEmail = trim(request("Email"))
		sPassword = trim(request("Password"))
		sConfirmPassword = trim(request("ConfirmPassword"))
		sFirstName = trim(request("FirstName"))
		sMid = trim(request("MiddleName"))
		sLastName = trim(request("LastName"))
		iGroupID = trim(request("GroupID"))
		iCompanyId = ScrubForSql(request("Company"))
		iBranchId = ScrubForSql(request("BranchId"))
		sCity = trim(request("City"))
		iStateID = trim(request("State"))
		sAddress = trim(request("Address"))
		sZipcode = trim(request("Zipcode"))
		sPhone = trim(request("Phone"))
		sFax = trim(request("Fax"))
		iUserStatus = trim(request("UserStatus"))
		sNotes = trim(request("Notes"))

	else	

		'Pull Info from db
		set oUserObj = New User
		oUserObj.ConnectionString = application("sDataSourceName")
		oUserObj.UserID = iUserID
		set rs = oUserObj.GetUser()
		
		sLogin = trim(rs("UserName"))
		sEmail = trim(rs("Email"))
		sPassword = trim(rs("Password"))
		sFirstName = trim(rs("FirstName"))
		sMid = trim(rs("MiddleName"))
		sLastName = trim(rs("LastName"))
		iGroupID = trim(rs("GroupID"))
		iCompanyId = trim(rs("CompanyID"))
		iBranchId = trim(rs("BranchId"))
		sCity = trim(rs("City"))
		iStateID = trim(rs("StateID"))
		sAddress = trim(rs("Address"))
		sZipcode = trim(rs("Zipcode"))
		sPhone = trim(rs("Phone"))
		sFax = trim(rs("Fax"))
		iUserStatus = trim(rs("UserStatus"))
		sNotes = trim(rs("Notes"))
		
		set oUserObj = nothing

		sConfirmPassword = sPassword
	
	end if
	
end if

%>

<script language="Javascript" src="/admin/includes/FormCheck2.js" type="text/javascript"></script>
<script language="Javascript">

function CompanyChange()
{
	document.frm1.BranchId.value = '';	
									
	RefreshScreen();
}

function RefreshScreen()
{
	document.frm1.ScreenRefresh.value = 1;
	document.frm1.action = "ModUser.asp";
	
	//EnableFields();
	
	document.frm1.submit();	
}

function Validate(FORM)
{   	
	if(!checkEmail(FORM.Email) || !checkString(FORM.Login, "Login") || !checkString(FORM.Password,"Password") || !checkString(FORM.ConfirmPassword,"Confirm Password"))
		return false;
		
	if (FORM.Password.value != FORM.ConfirmPassword.value)
	{
		alert("The Password and Confirm Password fields do not match.")
		FORM.Password.focus();
		return false;
	}
	
	if (!checkString(FORM.FirstName,"First Name") || !checkString(FORM.LastName,"Last Name") || !checkString(FORM.GroupID,"User Type"))
		return false;

	//if the UserType/GroupID is Company Admin, the Company field must be selected
	if ((FORM.GroupID.selectedIndex == 3) && (FORM.Provider.selectedIndex == 0))
	{
		alert("The Provider field must be set since this user is a Provider Admin.");
		FORM.GroupID.focus();
		return false;
	}
	
	//check address
	if (!checkString(FORM.Address,"Address"))
		return false;
	
	//if zipcode is not blank, make sure it's in the proper format
	if (FORM.Zipcode.value != "")
	{
		if (!checkZIPCode(FORM.Zipcode))
			return false;
	}
	
	//if phone # is not blank, make sure it's in the proper format
	if (!checkUSPhone(FORM.Phone))
		return false;
	
	//if fax # is not blank, make sure it's in the proper format
	if (FORM.Fax.value != "")
	{
		if (!checkUSPhone(FORM.Fax))
			return false;
	}	

	return true;
}
</script>

<table width="100%" bgcolor="FFFFFF" border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td>
			<span class="activeTitle"><%=sMode%>&nbsp;User</span><br><img src="/admin/login/media/images/clear.gif" width="10" height="4" border="0" hspace="0" vspace="0"><br>
		</td>
	</tr>
</table>
<br>

<form name="frm1" action="ModUserProc.asp" method="POST" onSubmit="return Validate(this)">
<input type="hidden" name="UserID" value="<%=iUserID%>">
<input type="hidden" name="Mode" value="<%=sMode%>">
<% if UCase(sMode) = "EDIT" then %>
<input type="hidden" name="OldEmail" value="<%=sEmail%>">
<% end if %>

<input type="hidden" name="ScreenRefresh" value="">
<table cellpadding="4" cellspacing="4">
	<tr>
		<td colspan="2">
			<b>*</b> Required Fields
		</td>
	</tr>
	<tr>
		<td><b>* Login:</b></td>
		<td><input type="text" name="Login" value="<% = sLogin %>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>* Password:</b></td>
		<td><input type="password" name="Password" value="<%=sPassword%>" size="30" maxlength="20"></td>				
	</tr>
	<tr>
		<td><b>* Confirm Password:</b></td>
		<td><input type="password" name="ConfirmPassword" value="<%=sConfirmPassword%>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>* First Name:</b></td>
		<td><input type="text" name="FirstName" value="<%=sFirstName%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>Middle Name:</b></td>
		<td><input type="text" name="MiddleName" value="<%=sMid%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>* Last Name:</b></td>
		<td><input type="text" name="LastName" value="<%=sLastName%>" size="30" maxlength="50"></td>				
	</tr>
	<tr>
		<td><b>* Email:</b></td>
		<td><input type="text" name="Email" value="<%=sEmail%>" size="30" maxlength="100"></td>				
	</tr>
	<tr>
		<td><b>* User Status:</b></td>
		<td><% call DisplayUserStatusDropDown(iUserStatus,1) %></td>				
	</tr>		
	<tr>
		<td><b>* User Type:</b></td>
		<td><% call DisplayUserTypeDropDown(iGroupID,1) %></td>				
	</tr>	
	<tr>
		<td><b>Company: </b></td>
		<td><% call DisplayCompaniesDropDown(iCompanyId, 3) %></td>
	</tr>		
	<tr>
		<td><b>Branch: </b></td>
		<td><% call DisplayBranchesDropDown(iBranchId, iCompanyId, false, 1) %></td>
	</tr>
	<tr>
		<td><b>* Address:</b></td>
		<td><input type="text" name="Address" value="<%=sAddress%>" size="30" maxlength="100"></td>				
	</tr>				
	<tr>
		<td><b>City:</b></td>
		<td><input type="text" name="City" value="<%=sCity%>" size="30" maxlength="50"></td>				
	</tr>	
	<tr>
		<td><b>State:</b></td>
		<td><% call DisplayStatesDropDown(iStateID,1,0) %></td>
	</tr>
	<tr>
		<td><b>Zipcode:</b></td>
		<td><input type="text" name="Zipcode" value="<%=sZipcode%>" size="10" maxlength="10"></td>				
	</tr>
	<tr>
		<td><b>* Phone No.:</b></td>
		<td><input type="text" name="Phone" value="<%=sPhone%>" size="30" maxlength="20"></td>				
	</tr>
	<tr>
		<td><b>Fax No.:</b></td>
		<td><input type="text" name="Fax" value="<%=sFax%>" size="30" maxlength="20"></td>				
	</tr>			
	<tr>
		<td><b>Notes:</b></td>
		<td valign="middle">
			<textarea name="notes" cols="50" rows="5" maxlength="1000"><%=sNotes%></textarea>
		</td>
	</tr>			
	<tr>
		<td align="center" colspan="2">
			<br>
			<input type="image" src="/admin/media/images/blue_bttn_submit.gif">&nbsp;
			<a href="javascript:history.back()"><img src="/admin/media/images/blue_bttn_cancel.gif" border=0></a>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual = "/admin/includes/bottomHtml.asp" --------------------------------------------------->

<%
set rs = nothing
%>
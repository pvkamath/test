<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/CalendarDisplay.Class.asp" ------------------>
<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/Document.Class.asp" ------------------------->
<!-- #include virtual="/includes/StateDeadline.Class.asp" -------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
		
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	iPage = 1


'Verify that the user has logged in.
CheckIsLoggedIn()


'Calendar Object initialization
dim oCalendarDisplay
set oCalendarDisplay = new CalendarDisplay
dim sEventType
dim iSearchStateId

sEventType = ScrubForSql(request("SearchEventType"))
iSearchStateId = ScrubForSql(request("SearchState"))

oCalendarDisplay.TemporaryRecordsetFile = application("sAbsWebroot") & "includes\TempRecordsetDefinition.rst"
oCalendarDisplay.StartOfBusinessDay = "7:00 AM"
oCalendarDisplay.EndOfBusinessDay = "9:00 PM"
oCalendarDisplay.HighlightReferenceDate = false
oCalendarDisplay.DateLink = application("sDynWebroot") & "calendar/calendar.asp?SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & "&View=day&refdate="
if CheckIsAdmin() then
	oCalendarDisplay.EmptyDateLink = application("sDynWebroot") & "calendar/editevent.asp?editmode=Add&refdate="
else
	oCalendarDisplay.EmptyDateLink = ""
end if
oCalendarDisplay.TimeLink = ""
oCalendarDisplay.ShowEmptyDaysInWeekView = true

'Get search criteria
dim dStartDate, dEndDate, dRefDate, sView
dim dRsStartDate, dRsEndDate, dTmpDate
dim bDisplayEventListing
dim sMode

'Show the monthly view if there is not specified view
sView = request("view")
if sView = "" then
	sView = "month"
end if

'Display the event listing if this is a monthly view
if sView = "month" then
	bDisplayEventListing = true
else
	bDisplayEventListing = false
end if

'If we do not have a specified reference date, use the current one
dRefDate = request("refDate")
if dRefDate = "" then
	dRefDate = Date
end if 

sMode = request("mode")

'Depending on our chosen view mode, set the start and end date accordingly
select case sView
	case "month"
		
		'Set startdate to the beginning of the month, then subract one month from that date
		dStartDate = month(dRefDate) & "/1/" & year(dRefDate)
		dStartDate = oCalendarDisplay.SubtractOneMonth(dStartDate)
		
		'Set end date to the last day of the month, then add one month to that date
		dEndDate = getLastDayOfMonth(Month(dRefDate), year(dRefDate))
		dEndDate = oCalendarDisplay.AddOneMonth(dEndDate)
		
	case else
	
		'Assign these the the refdate, which if unspecified is the current date
		dStartDate = dRefDate
		dEndDate = dRefDate
	
end select
	
' -------------- END INITIALIZATION CONTENT -------------------------------------------
	BeginHead sPgeTitle, USES_FORM_VALIDATION, vbBlank, vbBlank				' From htmlElements.asp
%><script TYPE="text/javascript"><!--
		// Function fxInitialize -- REQUIRED ON EVERY PAGE -----------------------
		function InitializePage() {
		}
		// Enter Javascript Validation and Functions below: ----------------------
//--></script>
<%
	FinishHeadBeginBody SHOW_PAGE_BACKGROUND, TRIM_PAGE_MARGINS, SHOW_MENUS	' From htmlElements.asp
	PrintShellHeader SHOW_MENUS, SIDE_MENU_SELECTED						' From shell.asp
	'-------------------- ENTER PAGE CONTENT HERE ------------------------------------------------ %>
<script language="JavaScript">
function ConfirmDocumentDelete(p_iDocId,p_sDocName)
{
	if (confirm("Are you sure you wish to delete the following Document:\n  " + p_sDocName + "\n\nAll information will be deleted."))
		window.location.href = '<% = application("URL") %>/documents/deletedocument.asp?did=' + p_iDocId;
}
</script>

<table width="760" border="0" cellpadding="0" cellspacing="0">
	<tr>
<!--- column #3 --->
		<td class="bckRight" width="460" valign="top" align="center">
			<table border=0 cellpadding=10 cellspacing=0 align="left">
				<tr>
					<td class="date" align="left">Welcome, <% = session("User_FullName") %></td>
				</tr>
			</table>
			<p><br clear="all"><br>
			
			
			<!-- Renewal Alerts -->
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td class="bluetitle" valign="bottom">License Renewal Alerts</td>
					<td align="right" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" align="right">
							<tr>
								<td><span class="small">Search the Renewals:</span></td>
							</tr>
							<form name="90DaySearch" method="POST" action="/alerts/list.asp">
							<tr>
								<td align="right"><input type="text" name="SearchText" style="HEIGHT: 24px; WIDTH: 160px" value="<% = ScrubForSql(request("90DaySearchText")) %>">&nbsp;<input type="submit" value="Go" name="submit" alt="Go"></td>
							</tr>
							</form>
						</table>						
					</td>
				</tr>
				<tr>
					<td colspan="2"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">

<%

'Calculate the difference in days between the current time and the date of the
'user's last login.
dim iTimeDiff
iTimeDiff = datediff("d", cdate(session("dUserLastLoginDate")), now())
if iTimeDiff > 30 then
	iTimeDiff = 30
end if


'License or License*S*?
if session("CKTrackerLicenses30") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if 

%>
							<tr>
								<td width="175" class="alert90days"></b>Expiring in 90 Days</b></td><td width="175"><b><% = session("CKTrackerLicenses90") %></b> License<% = sPlural %></b> (<a href="alerts/list.asp?searchtype=numbered&expiringfrom=<% = 90 - iTimeDiff %>&expiringto=90"><% = session("CKTrackerLicenses90New") %> New</a>)</td><td width="80"><a href="alerts/list.asp?searchtype=numbered&expiringfrom=60&expiringto=90">View List</a></td>
							</tr>
<%


'License or License*S*?
if session("CKTrackerLicenses60") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if

%>
							<tr>
								<td width="175" class="alert60days"></b>Expiring in 60 Days</b></td><td width="175"><b><% = session("CKTrackerLicenses60") %></b> License<% = sPlural %></b> (<a href="alerts/list.asp?searchtype=numbered&expiringfrom=<% = 60 - iTimeDiff %>&expiringto=60"><% = session("CKTrackerLicenses60New") %> New</a>)</td><td width="80"><a href="alerts/list.asp?searchtype=numbered&expiringfrom=30&expiringto=60">View List</a></td>
							</tr>
<%


'License or License*S*?
if session("CKTrackerLicenses30") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if

%>
							<tr>
								<td width="175" class="alert30days"></b>Expiring in 30 Days</b></td><td width="175"><b><% = session("CKTrackerLicenses30") %></b> License<% = sPlural %></b> (<a href="alerts/list.asp?searchtype=numbered&expiringfrom=<% = 30 - iTimeDiff %>&expiringto=30"><% = session("CKTrackerLicenses30New") %> New</a>)</td><td width="80"><a href="alerts/list.asp?searchtype=numbered&expiringto=30">View List</a></td>
							</tr>



						</table>
					</td>
				</tr>
			</table>
			<p><br><p>
			
			
			<!-- Renewal Alerts -->
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td valign="bottom"><span class="bluetitle">Licenses Pending</span></td>
					<td align="right" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" align="right">
							<tr>
								<td><span class="small">Search Pending Renewals:</span></td>
							</tr>
							<form name="PendingSearch" method="POST" action="/alerts/list.asp?searchtype=pendingrenewal">
							<tr>
								<td align="right"><input type="text" name="SearchText" style="HEIGHT: 24px; WIDTH: 160px" value="<% = ScrubForSql(request("PendingSearchText")) %>">&nbsp;<input type="submit" value="Go" name="submit" alt="Go"></td>
							</tr>
							</form>
						</table>						
					</td>
				</tr>
				<tr>
					<td colspan="2"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">		





							<tr>
								<td width="175">Pending</td><td width="175"><b><% = session("CKTrackerLicensesPending") %></b> License<% if session("CKTrackerLicensesPending") <> 1 then %>s<% end if %></td><td width="80"><a href="/alerts/list.asp?searchtype=pendingrenewal">View List</a></td>
							</tr>





						</table>
					</td>
				</tr>
			</table>			
			<p><br><p>
			
			<!-- Individual Brokers -->
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td valign="bottom"><span class="bluetitle">Active Licenses</span></td>
					<td align="right" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" align="right">
							<tr>
								<td><span class="small">Search Active Licensees:</span></td>
							</tr>
							<form name="BrokerSearch" method="POST" action="/alerts/list.asp?searchtype=active">
							<tr>
								<td align="right"><input type="text" name="SearchText" style="HEIGHT: 24px; WIDTH: 160px" value="<% = ScrubForSql(request("BrokerSearchText")) %>">&nbsp;<input type="submit" value="Go" name="submit" alt="Go"></td>
							</tr>
							</form>
						</table>						
					</td>
				</tr>
				<tr>
					<td colspan="2"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">	


							<tr>
								<td width="175">Active</td><td width="175"><b><% = session("CKTrackerLicensesActive") %></b> License<% if session("CKTrackerLicensesActive") <> 1 then %>s<% end if %></td><td width="80"><a href="/alerts/list.asp?searchtype=active">View List</a></td>
							</tr>



						</table>
					</td>
				</tr>
			</table>
			<p><br><p>


			<!-- Education Renewal Alerts -->
			<table border="0" cellpadding="0" cellspacing="0" width="430">
				<tr>
					<td class="bluetitle" valign="bottom">Education Renewal Alerts</td>
					<td align="right" valign="bottom">
						<table cellpadding="0" cellspacing="0" border="0" align="right">
							<tr>
								<td><span class="small">Search Courses:</span></td>
							</tr>
							<form name="SearchCourses" method="POST" action="/alerts/dllist.asp?searchtype=numbered&expiringto=90">
							<tr>
								<td nowrap align="right"><input type="text" name="SearchText" style="HEIGHT: 24px; WIDTH: 160px" value="">&nbsp;<input type="submit" value="Go" name="submit" alt="Go"></td>
							</tr>
							</form>
						</table>						
					</td>					</tr>
				<tr>
					<td colspan="2"><img src="/media/images/spacer.gif" width="1" height="5" alt=""></td>
				</tr>
				<tr>
					<td class="bckBlue" colspan="2"><img src="/media/images/spacer.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td colspan="2">
						<img src="/media/images/spacer.gif" width="1" height="5" alt=""><br>
						<table cellpadding="3" cellspacing="0" border="0" width="100%">
<%


'Course or Courses?
if session("CKTrackerCourses90") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if 

%>
							<tr>
								<td width="175" class="alert90days"></b>Expiring in 90 Days</b></td><td width="175"><b><% = session("CKTrackerCourses90") %></b> Course<% = sPlural %></b> (<a href="alerts/dllist.asp?searchtype=numbered&expiringfrom=<% = 90 - iTimeDiff %>&expiringto=90"><% = session("CKTrackerCourses90New") %> New</a>)</td><td width="80"><a href="alerts/dllist.asp?searchtype=numbered&expiringfrom=60&expiringto=90">View List</a></td>
							</tr>
<%


'Course or Courses?
if session("CKTrackerCourses60") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if

%>
							<tr>
								<td width="175" class="alert60days"></b>Expiring in 60 Days</b></td><td width="175"><b><% = session("CKTrackerCourses60") %></b> Course<% = sPlural %></b> (<a href="alerts/dllist.asp?searchtype=numbered&expiringfrom=<% = 60 - iTimeDiff %>&expiringto=60"><% = session("CKTrackerCourses60New") %> New</a>)</td><td width="80"><a href="alerts/dllist.asp?searchtype=numbered&expiringfrom=30&expiringto=60">View List</a></td>
							</tr>
<%


'Course or Courses?
if session("CKTrackerCourses30") <> 1 then
	sPlural = "s"
else
	sPlural = ""
end if
%>
							<tr>
								<td width="175" class="alert30days"></b>Expiring in 30 Days</b></td><td width="175"><b><% = session("CKTrackerCourses30") %></b> Course<% = sPlural %></b> (<a href="alerts/dllist.asp?searchtype=numbered&expiringfrom=<% = 30 - iTimeDiff %>&expiringto=30"><% = session("CKTrackerCourses30New") %> New</a>)</td><td width="80"><a href="alerts/dllist.asp?searchtype=numbered&expiringto=30">View List</a></td>
							</tr>



						</table>
					</td>
				</tr>
			</table>
			<p><br><p>

<!--- center border column --->			
		<td class="border"><img src="/Media/Images/spacer.gif" width="1" height="10" alt="" border="0"></td>
		

		<td class="bckLeft" width="299" valign="top" align="center">
			<table border=0 cellpadding=10 cellspacing=0 align="right">
				<tr>
					<td class="date" align="left"><% = formatdatetime(Now, 1) %></td>
				</tr>
			</table>
			<br clear="all">
			<img src="/Media/Images/spacer.gif" width="1" height="10" alt="" border="0">
			<br clear="all">
			
			<!-- Event Calendar -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="250">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_calendar.gif" width="17" height="17" alt="Calendar" align="absmiddle" vspace="10"> Event Calendar</td>
				</tr>
				<tr>
<%
'===========================================
'New Caledar code

'Get events
sSql = "SELECT " & _
	   "DISTINCT E.EventId, " & _
	   "E.Event, " & _
	   "E.StartDate, " & _
	   "E.EndDate, " & _
	   "E.Active, " & _
	   "E.ImgPath, " & _
	   "E.Location, " & _
	   "E.Directions, " & _
	   "EET.EventId, " & _
	   "ET.EventType, " & _
	   "E.RedirectUrl " & _
	   "FROM afxEvents AS E " & _
	   "LEFT OUTER JOIN afxEventsEventTypeX EET ON (E.EventId = EET.EventId) " & _
	   "LEFT OUTER JOIN afxEventType ET ON (EET.EventTypeId = ET.EventTypeId) " & _
	   "LEFT OUTER JOIN States AS S ON (E.StateId = S.StateId) " & _
	   "WHERE E.EndDate >= '" & dStartDate & "' AND E.StartDate <= '" & dEndDate & " 23:59:59'" & _
	   "AND E.Active = 1 " & _
	   "AND E.CompanyId = '" & session("UserCompanyId") & "' "

'Filter by search state if provided
if trim(iSearchStateId) <> "" then
	sSql = sSql & " AND S.StateId = '" & iSearchStateId & "'"
end if

'Filter according to specified mode
if sMode = "past" then
	sSql = sSql & " AND E.EndDate < '" & Date & "' " & _
				  "ORDER BY E.StartDate DESC "
else
	sSql = sSql & " AND E.EndDate >= '" & Date & "' " & _
				  "ORDER BY E.StartDate ASC "
end if


dim oConn
set oConn = new HandyAdo
set oRs = oConn.GetDisconnectedRecordset(application("sDataSourceName"), sSql, false)


'Add the retrieved events to the calendar.
dim sEvent
dim sRedirectUrl

if not (oRs.BOF and oRs.EOF) then
	do while not oRs.EOF
		dRsStartDate = oRs("StartDate")
		dRsEndDate = oRs("EndDate")
		sRedirectUrl = oRs("RedirectUrl")
		if isdate(dRsStartDate) and isdate(dRsEndDate) then
			dTmpDate = dRsStartDate
			
			'Loop through the dates from start date to end date
			do while DateDiff("D", FormatDateTime(dTmpDate, 2), FormatDateTime(dRsEndDate, 2)) >= 0
				sEvent = "<a class=""newsTitle"" href=""/event.asp?id=" & oRs("EventId") & "&SearchState=" & iSearchStateId & "&SearchEventType=" & sEventType & """>" & oRs("Event") & "</a>"
				oCalendarDisplay.AddEvent cdate(dTmpDate), sEvent, oRs("Directions")
				dTmpDate = DateAdd("D", 1, dTmpDate)
			loop
		end if 
		oRs.MoveNext
	loop
	oRs.MoveFirst
end if 

'===========================================

dim sCalendarRow

%>
			<td valign="top" class="bckLtGrayBottomBorder"><span class="date">
<%
select case month(now)

case 1
	Response.Write("January")
case 2
	Response.Write("February")
case 3
	Response.Write("March")
case 4
	Response.Write("April")
case 5
	Response.Write("May")
case 6
	Response.Write("June")
case 7
	Response.Write("July")
case 8
	Response.Write("August")
case 9
	Response.Write("September")
case 10
	Response.Write("October")
case 11
	Response.Write("November")
case 12
	Response.Write("December")
case else
	Response.Write("")

end select 
%>  
<% = " " %>
<% = year(now) %>
			</span>
		</td>
	</tr>
	<tr>
		<td>
			<table bgcolor="#ffffff" cellpadding="3" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="center" colspan="2">
<%
if sView = "month" then

	dim sPrevDate, sNextDate
	dim bDisplayPrevMonth, bDisplayNextMonth
	
	sPrevDate = oCalendarDisplay.SubtractOneMonth(dRefDate)
	sNextDate = oCalendarDisplay.AddOneMonth(dRefDate)
	if DateDiff("M", Date, sPrevDate) > 12 or DateDiff("M", Date, sPrevDate) < -12 then
		bDisplayPrevMonth = false
	else
		bDisplayPrevMonth = true
	end if
	if DateDiff("M", Date, sNextDate) > 12 or DateDiff("M", Date, sNextDate) < -12 then
		bDispalyNextMonth = false
	else
		bDisplayPrevMonth = true
	end if 	
	
	'Print mini calendars
	oCalendarDisplay.Mode = "mini"
	oCalendarDisplay.HighlightReferenceDate = false

	oCalendarDisplay.PrintCalendar(dRefDate)	

end if

set oCalendarDisplay = nothing

%>
							</td>
						</tr>
						<tr>
							<td>
<%










if oRs.EOF and oRs.BOF then
%>
No Events Listed
<%
end if 
%>
									<ul class="bullet">
<%

do while not oRs.EOF

	if sCalendarRow = "" then
		sCalendarRow = " bgcolor=""F6F4EE"""
	else
		sCalendarRow = ""
	end if
	
	Response.Write("<li type=""circle""><b>" & FormatDateTime(oRs("StartDate"), 2))
	if (oRs("EndDate") <> "") and (oRs("EndDate") <> oRs("StartDate")) then 
		Response.Write(" - " & FormatDateTime(oRs("EndDate"), 2))
	end if
	Response.Write("</b><br><a href=""calendar/calendardetail.asp?id=" & oRs("EventId") & """>" & oRs("Event") & "</a></li>" & vbCrLf)

	oRs.MoveNext

loop

set oConn = nothing
set oRs = nothing
%>
							</tr>							
						</table>
					</td>
				</tr>
			</table>	
			<br>
			
			<!-- Document Tracker -->
			<table class="bckBox" border="0" cellpadding="5" cellspacing="0" width="250">
				<tr class="bckBlue">
					<td class="sectiontitle"><img src="/media/images/icon_search.gif" width="17" height="17" alt="Search" align="absmiddle" vspace="10"> Document Tracking</td>
				</tr>
				<tr>
					<td valign="top" class="bckLtGrayBottomBorder">
								<table cellpadding="5" cellspacing="0" border="0">
									<tr>
<% if CheckIsAdmin() then %>
<td><a href="<% = application("URL") %>/documents/moddocument.asp" class="nav"><img src="<% = application("sDynMediaPath") %>bttnNew.gif" align="top" border="0" title="Add New Document" alt="Add New Document"> New Document</a></td>
<% else %>
&nbsp;
<% end if %>
									</tr>
								</table>
					</td>
				</tr>
				<tr>
					<td width="100%">
<%
'Declare and initialize the document object
dim oDoc
dim iDocCounter
set oDoc = new Document
oDoc.ConnectionString = application("sDataSourceName")
oDoc.VocalErrors = application("bVocalErrors")
oDoc.OwnerId = session("UserCompanyId")

'Declare and initialize the license object
dim oLicense
set oLicense = new License
oLicense.VocalErrors = application("bVocalErrors")
oLicense.ConnectionString = application("sDataSourceName")

set oRs = oDoc.SearchDocs

if not (oRs.BOF and oRs.EOF) then
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<% if CheckIsAdmin() then %>
		<td></td>
		<td></td>
		<% end if %>
		<td><b>Document</b></td>
		<td><b>Status</b></td>
	</tr>
<%
	iDocCounter = 1
	do while not oRs.EOF and not iDocCounter > 3
	
		if oRs(0) = "DOC" then 
		
			if oDoc.LoadDocById(oRs("DocId")) <> 0 then

				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & " valign=""top""><a href=""" & application("URL") & "/documents/moddocument.asp?did=" & oDoc.DocId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This Document"" alt=""Edit This Document""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & " valign=""top""><a href=""javascript:ConfirmDocumentDelete(" & oDoc.DocId & ",'" & oDoc.DocName & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This Document"" alt=""Delete This Document""></a></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.DocName)
				if oDoc.DocSentDate <> "" then
					Response.Write("<br><b>Sent: </b>" & oDoc.DocSentDate)
				end if
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">" & oDoc.LookupDocStateId(oDoc.DocStateId) & "</td>" & vbCrLf)
				
			end if 
			
		elseif oRs(0) = "LIC" then
		
			if oLicense.LoadLicenseById(oRs("DocId")) <> 0 then
			
				if sColor = "" then
					sColor = " bgcolor=""#ffffff"""
				else
					sColor = ""
				end if
	
				Response.Write("<tr>" & vbCrLf)
				if CheckIsAdmin() then
					Response.Write("<td " & sColor & " valign=""top""><a href=""" & application("URL") & "/officers/modlicense.asp?lid=" & oLicense.LicenseId & """><img src=""" & application("sDynMediaPath") & "bttnEdit.gif"" border=""0"" title=""Edit This License"" alt=""Edit This License""></a></td>" & vbCrLf)
					Response.Write("<td " & sColor & " valign=""top""><a href=""javascript:ConfirmLicenseDelete(" & oLicense.LicenseId & ",'" & oLicense.LicenseNum & "')""><img src=""" & application("sDynMediaPath") & "bttnDelete.gif"" border=""0"" title=""Delete This License"" alt=""Delete This License""></a></td>" & vbCrLf)
				end if 
				Response.Write("<td " & sColor & " valign=""top"">Renewal For " & oLicense.LookupState(oLicense.LicenseStateId) & " License #" & oLicense.LicenseNum)
				Response.Write("<br><b>Sent: </b>" & oLicense.SubmissionDate)
				Response.Write("</td>" & vbCrLf)
				Response.Write("<td " & sColor & " valign=""top"">Pending</td>" & vbCrLf)
			
			end if 		
		
		end if 
		
		
		oRs.MoveNext
		iDocCounter = iDocCounter + 1
	
	loop
%>
	<tr>
		<% if CheckIsAdmin() then %>
		<td colspan="2"></td>
		<% end if %>
		<td colspan="2"><a href="documents/documentsearch.asp">View more documents...</a></td>
</table>
<%

else
%>
<table width="100%" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td>No Documents Found.</td>
	</tr>
</table>
<%
end if 

set oRs = nothing
%>

					</td>
				</tr>
			</table>
				
			<br>
		</td>





<%	'-------------------- END PAGE CONTENT ----------------------------------------------------------
	PrintShellFooter SHOW_MENUS										' From shell.asp
	EndPage														' From htmlElements.asp

	function getLastDayOfMonth(p_month, p_year) 
		on error resume next

		dim aryCalendar, aryLeapCalendar 
		dim sReturn
		dim iYear, iMonth
		dim sTestDate

		if len(p_month) > 0 and len(p_year) > 0 then
			aryCalendar = split("31 28 31 30 31 30 31 31 30 31 30 31")

			if len(p_year) = 2 then
				p_year = "20" & p_year
			end if

			iMonth = cint(p_month) - 1
			iYear = cint(p_year)

			'Check for leap year ..
			'1.Years evenly divisible by four are normally leap years, except for... 
			'2.Years also evenly divisible by 100 are not leap years, except for... 
			'3.Years also evenly divisible by 400 are leap years. 
			if iYear mod 4 = 0 then
				if iYear mod 100 = 0 and iYear mod 400 <> 0 then
					sReturn	= aryCalendar(iMonth)		
				else
					if iMonth = 2 then
						sReturn = "29"
					else
						sReturn	= aryCalendar(iMonth)	
					end if
				end if
			else
				sReturn	= aryCalendar(iMonth)
			end if
		end if

		if err then
			sReturn = ""
		end if

		sTestDate = p_month&"/"&sReturn&"/"&p_year

		if isDate(sTestDate) then
			sReturn = sTestDate
		else
			sReturn = ""
		end if

		getLastDayofmonth = sReturn

	end function
%>
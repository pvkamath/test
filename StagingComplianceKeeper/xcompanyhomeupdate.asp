<% 'response.end %>
<% 'server.ScriptTimeout = 2000 %>
<!-- #include virtual = "/includes/misc.asp" --------------->
<!-- #include virtual = "/includes/htmlElements.asp" ------->
<!-- #include virtual = "/includes/shell.asp" -------------->
<!-- #include virtual = "/admin/includes/functions.asp" ---->

<!-- #include virtual="/includes/Company.Class.asp" -------------------------->
<!-- #include virtual="/includes/License.Class.asp" -------------------------->
<!-- #include virtual="/includes/Associate.Class.asp" ------------------------>
<!-- #include virtual="/includes/StateDeadline.Class.asp" -------------------->

<%' -------------- INITIALIZATION CONTENT HERE -------------------------------------------------------
	
	' Template Constants -- Modify as needed per each page:
	const USES_FORM_VALIDATION = False			' Template Constant
	const SHOW_PAGE_BACKGROUND = True			' Template Constant
	const TRIM_PAGE_MARGINS = True				' Template Constant
	const SHOW_MENUS = False					' Template Constant
	const SIDE_MENU_SELECTED = ""				' Template Constant
	dim sPgeTitle								' Template Variable
	dim iPage
		
	' Set Page Title:
	sPgeTitle = "Compliance Management Solutions"
	iPage = 1


'Verify that the user has logged in.
CheckIsLoggedIn()



dim iNumNew
dim iTimeDiff


'Declare and initialize the License object.
dim oLicense
set oLicense = new License
oLicense.ConnectionString = application("sDataSourceName")
oLicense.VocalErrors = application("bVocalErrors")


'Restrict the search with some general criteria.
oLicense.OwnerCompanyId = session("UserCompanyId") 'Only the user's own company
oLicense.LicenseStatusId = 1 'Only active licenses


'Restrict to the l2/l3/branch list for specific admins/viewers
if CheckIsCompanyL2Admin or CheckIsCompanyL2Viewer then
	oLicense.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oLicense.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oLicense.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin or CheckIsCompanyL3Viewer then
	oLicense.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oLicense.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin or CheckIsBranchViewer then
	oLicense.SearchBranchIdList = GetUserBranchIdList()
end if


'Find all the licenses expiring between 60 and 90 days from now.
oLicense.SearchLicenseExpDateFrom = now() + 60
oLicense.SearchLicenseExpDateTo = now() + 90
set oRs = oLicense.SearchLicenses()
session("CKTrackerLicenses90") = oRs.RecordCount
set oRs = nothing


'Find all the licenses expiring between 30 and 60 days from now.
oLicense.SearchLicenseExpDateFrom = now() + 30
oLicense.SearchLicenseExpDateTo = now() + 60
set oRs = oLicense.SearchLicenses()
session("CKTrackerLicenses60") = oRs.RecordCount
set oRs = nothing


'Find all the licenses expiring between 0 and 30 days from now.
oLicense.SearchLicenseExpDateFrom = now()
oLicense.SearchLicenseExpDateTo = now() + 30
set oRs = oLicense.SearchLicenses()
session("CKTrackerLicenses30") = oRs.RecordCount
set oRs = nothing


'Calculate the difference in days between the current time and the date of the
'user's last login.
iTimeDiff = datediff("d", cdate(session("dUserLastLoginDate")), now())


'Find the number of new expiring licenses in the next 60-90 days: we want to
'know how many licenses are in this count that weren't in the count when the
'user last visited the site.  That is, how many licenses are expiring between
'the date that is 90 days from the last login and the date that is 90 days from
'this login.  In the case that there is no overlap - that 90 days from the last
'login is earlier than 60 days from the current login, meaning the user logged
'in more than 30 days ago - all the records in the normal 60-90 day expiration 
'window are considered new.
if iTimeDiff >= 30 then
	session("CKTrackerLicenses90New") = session("CKTrackerLicenses90")
	iTimeDiff = 30
else
	oLicense.SearchLicenseExpDateFrom = cdate(session("dUserLastLoginDate")) + 90
	oLicense.SearchLicenseExpDateTo = now() + 90
	set oRs = oLicense.SearchLicenses()
	session("CKTrackerLicenses90New") = oRs.RecordCount
	set oRs = nothing
end if


'Find the number of new expiring licenses in the 30-60 days.
if iTimeDiff >= 30 then
	session("CKTrackerLicenses60New") = session("CKTrackerLicenses60")
else
	oLicense.SearchLicenseExpDateFrom = cdate(session("dUserLastLoginDate")) + 60
	oLicense.SearchLicenseExpDateTo = now() + 60
	set oRs = oLicense.SearchLicenses()
	session("CKTrackerLicenses60New") = oRs.RecordCount
	set oRs = nothing
end if


'Find the number of new expiring licenses in the next 30 days.
if iTimeDiff >= 30 then
	session("CKTrackerLicenses30New") = session("CKTrackerLicenses30")
else
	oLicense.SearchLicenseExpDateFrom = cdate(session("dUserLastLoginDate")) + 30
	oLicense.SearchLicenseExpDateTo = now() + 30
	set oRs = oLicense.SearchLicenses()
	session("CKTrackerLicenses30New") = oRs.RecordCount
	set oRs = nothing
end if


'Reset the license object properties to search for pending licenses.
oLicense.LicenseStatusId = 10
oLicense.SearchLicenseExpDateFrom = now()
oLicense.SearchLicenseExpDateTo = ""
set oRs = oLicense.SearchLicenses()
session("CKTrackerLicensesPending") = oRs.RecordCount
set oRs = nothing


'Reset the license object properties to search for all active licenses.
oLicense.LicenseStatusId = 1
oLicense.SearchLicenseExpDateFrom = now()
oLicense.SearchLicenseExpDateTo = ""
set oRs = oLicense.SearchLicenses()
session("CKTrackerLicensesActive") = oRs.RecordCount
set oRs =  nothing









dim oExpiringUsers 'Recordset of expiring users

'Declare and initialize the State Deadline object
dim oDeadline
set oDeadline = new StateDeadline
oDeadline.VocalErrors = application("bVocalErrors")
oDeadline.ConnectionString = application("sDataSourceName")
oDeadline.TpConnectionString = application("sTpDataSourceName")
oDeadline.CompanyId = session("UserCompanyId")


'Restrict to the l2/l3/branch list for specific admins/viewers
if CheckIsCompanyL2Admin or CheckIsCompanyL2Viewer then
	oDeadline.SearchCompanyL2IdList = GetUserCompanyL2IdList()
	oDeadline.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsCompanyL3Admin or CheckIsCompanyL3Viewer then
	oDeadline.SearchCompanyL3IdList = GetUserCompanyL3IdList()
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
elseif CheckIsBranchAdmin or CheckIsBranchViewer then
	oDeadline.SearchBranchIdList = GetUserBranchIdList()
end if

'if CheckIsBranchAdmin() or CheckIsBranchViewer() then
'	oDeadline.SearchBranchIdList = session("UserBranchIdList")
'end if



'Search users with expiring courses in 60-90 days.
set oExpiringUsers = oDeadline.SearchExpirationsByTime(now() + 60, now() + 90, "", "", "", "")
session("CKTrackerCourses90") = oExpiringUsers.RecordCount
set oExpiringUsers = nothing


'Search users with expiring courses in 30-60 days.
set oExpiringUsers = oDeadline.SearchExpirationsByTime(now() + 30, now() + 60, "", "", "", "")
session("CKTrackerCourses60") = oExpiringUsers.RecordCount
set oExpiringUsers = nothing


'Search users with expiring courses in 0-30 days.
set oExpiringUsers = oDeadline.SearchExpirationsByTime(now(), now() + 30, "", iBranchId, "", "")
session("CKTrackerCourses30") = oExpiringUsers.RecordCount
set oExpiringUsers = nothing


'Search users with new expiring courses since last login in 60-90 days.
if iTimeDiff >= 30 then
	session("CKTrackerCourses90New") = session("CKTrackerCourses90")
else
	set oExpiringUsers = oDeadline.SearchExpirationsByTime(cdate(session("dUserLastLoginDate")) + 90, now() + 90, "", "", "", "")
	session("CKTrackerCourses90New") = oExpiringUsers.RecordCount
	set oExpiringUsers = nothing
end if


'Search users with new expiring courses since last login in 30-60 days.
if iTimeDiff >= 30 then
	session("CKTrackerCourses60New") = session("CKTrackerCourses60")
else
	set oExpiringUsers = oDeadline.SearchExpirationsByTime(cdate(session("dUserLastLoginDate")) + 60, now() + 60, "", iBranchId, "", "")
	session("CKTrackerCourses60New") = oExpiringUsers.RecordCount
	set oExpiringUsers = nothing
end if


'Search users with new expiring courses since last login in 0-30 days.
if iTimeDiff >= 30 then
	session("CKTrackerCourses30New") = session("CKTrackerCourses30")
else
	set oExpiringUsers = oDeadline.SearchExpirationsByTime(cdate(session("dUserLastLoginDate")) + 30, now() + 30, "", iBranchId, "", "")
	session("CKTrackerCourses30New") = oExpiringUsers.RecordCount
	set oExpiringUsers = nothing
end if


Response.Redirect("companyhome.asp")
Response.End


%>
<% = now() + 30 %>
<p>
<% = formatdatetime(now() + 30, 2) %>
<p>
Licenses<br>
90: <% = session("CKTrackerLicenses90") %> (<% = session("CKTrackerLicenses90New") %>)<br>
60: <% = session("CKTrackerLicenses60") %> (<% = session("CKTrackerLicenses60New") %>)<br>
30: <% = session("CKTrackerLicenses30") %> (<% = session("CKTrackerLicenses30New") %>)<br>
Pending: <% = session("CKTrackerLicensesPending") %><br>
Active: <% = session("CKTrackerLicensesActive") %><br>
<p>
Courses<br>
90: <% = session("CKTrackerCourses90") %> (<% = session("CKTrackerCourses90New") %>)<br>
60: <% = session("CKTrackerCourses60") %> (<% = session("CKTrackerCourses60New") %>)<br>
30: <% = session("CKTrackerCourses30") %> (<% = session("CKTrackerCourses30New") %>)<br>

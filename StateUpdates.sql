/*
   Thursday, June 16, 201610:45:07 AM
   User: 
   Server: MKE-PKAMATH
   Database: ComplianceKeeper
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_StateUpdates
	(
	UpdateId int NOT NULL IDENTITY (1, 1),
	StateId int NOT NULL,
	UpdateDate datetime NULL,
	Text text NULL,
	Deleted bit NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_StateUpdates SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_StateUpdates ON
GO
IF EXISTS(SELECT * FROM dbo.StateUpdates)
	 EXEC('INSERT INTO dbo.Tmp_StateUpdates (UpdateId, StateId, UpdateDate, Text, Deleted)
		SELECT UpdateId, StateId, UpdateDate, Text, Deleted FROM dbo.StateUpdates WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_StateUpdates OFF
GO
DROP TABLE dbo.StateUpdates
GO
EXECUTE sp_rename N'dbo.Tmp_StateUpdates', N'StateUpdates', 'OBJECT' 
GO
COMMIT
